import Complex as compl


print("Please enter your Complex number equation, using format (real,img)operator(real,img) , available operations: + - * /")
s = input()

lhs = compl.Complex(0, 0)
rhs = compl.Complex(0, 0)

if ')+(' in s:
    lhs, rhs = s.split("+")
    lhs = compl.Complex.fromstring(lhs)
    rhs = compl.Complex.fromstring(rhs)
    print(lhs+rhs)
elif ')-(' in s:
    lhs, rhs = s.split("-")
    lhs = compl.Complex.fromstring(lhs)
    rhs = compl.Complex.fromstring(rhs)
    print(lhs-rhs)
elif ')*(' in s:
    lhs, rhs = s.split("*")
    lhs = compl.Complex.fromstring(lhs)
    rhs = compl.Complex.fromstring(rhs)
    print(lhs*rhs)
elif ')/(' in s:
    lhs, rhs = s.split("/")
    lhs = compl.Complex.fromstring(lhs)
    rhs = compl.Complex.fromstring(rhs)
    print(lhs/rhs)
else:
    print('unknown expression')