from PIL import Image
import os
from os import listdir
from os.path import isfile, join


DIR_FOTO_JPG = '/home/mateuszogonowski/Documents/PythonScripts/PracaZPlikami/fotoJPG'
DIR_FOTO_PNG = '/home/mateuszogonowski/Documents/PythonScripts/PracaZPlikami/fotoPNG'
os.chdir(DIR_FOTO_JPG)
fotos = [f for f in listdir(DIR_FOTO_JPG) if isfile(join(DIR_FOTO_JPG, f))]

for img in fotos:
    base = os. path. splitext(img)
    os.chdir(DIR_FOTO_JPG)
    im = Image.open(img)
    os.chdir(DIR_FOTO_PNG)
    im.save(base[0]+".png")
