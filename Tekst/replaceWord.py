import os


DIR = '/home/mateuszogonowski/Documents/PythonScripts/Tekst/computer'
os.chdir(DIR)

infile = "+41.txt"
outfile = "+412.txt"

delete_list = {'Telephone':'Smartphone', 'numbers':'letters', 'in':'out', 'Switzerland':'Finland'}

with open(infile) as fin, open(outfile, "w+") as fout:
    for line in fin:
        for word in delete_list:
            line = line.replace(word, delete_list[word])
        fout.write(line)
