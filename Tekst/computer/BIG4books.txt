BIG4books Online Accounting Software is a free online accounting software system for small to medium-sized businesses, non-profits, consultancies, freelancers, and other organizations. It is a fully featured, double-entry bookkeeping system with support for various features to support the needs of the user organization. Free and subscription based service options are available.



Basic accounting features include customer and vendor records, accounts payables and receivables, invoicing, order entry, aging reports, financial statements, and general ledger.
Inventory control includes a perpetual inventory system with reorder points, automatic adjustment of COGS and inventory from Sales invoices, multiple per-item vendors, customers price points, general ledger account assignments.
Tax support includes customizable tax accounts and rates, automatic calculation of VAT, Sales tax, Local tax, or other taxation mechanisms. Per customer, vendor, inventory item tax assignment.
Multiple currencies, customizable.
Flexible general ledger account assignment, reporting options, custom report output formats, direct email transmission of Invoices, Orders.
Multiple users per company account and per user feature / data base access controls.
SAAS (online) delivery, accessible over internet worldwide. Automatic online backup.



Integrated payroll (payroll is provided in cooperation with an external partner)



BIG4books.com was deployed as a Saas beginning in January 2009. The software has been developed by BIG4 Systems of North Andover, MA, USA. The underlying accounting software is based on a system that has been widely deployed in an on-premises setting since 2001.
Version 1.3.0 was released in February 2011. The software is undergoing active development to add additional features and functions mainly due to user requests. Several thousand users are working with the system.
BIG4books.com was claimed to be unique with the "availability of a free ( no-charge) option to use the service" that is supported by sponsored advertising, and was introduced in 2009 "to provide a very low cost online accounting software system to existing and emerging small enterprises" during the then "current economic situation" 
90 Day Trial periods were also implemented at the end of 2010 due to "the persistent sluggishness in the economy, and the need for many small businesses to take advantage of more advanced features" 



Freeware
Comparison of accounting software






(English) Online Accounting Software
(English) Alltop
(English) Small Business Accounting Software:17 Targeted Choices
(English) Big4 Releasing Free Online Accounting System
(English) What is the easiest and most efficient accounting software for Macs?