The U Go offensive, or Operation C ( ), was the Japanese offensive launched in March 1944 against forces of the British Empire in the northeast Indian region of Manipur. Aimed at the Brahmaputra valley, through the two towns of Imphal and Kohima, the offensive along with the overlapping Ha Go offensive was one of the last major Japanese offensives during the Second World War. The offensive culminated in the Battles of Imphal and Kohima where the Japanese and their allies were first held and then pushed back.



In 1942, the Japanese Army had driven the British, Indian and Chinese troops out of Burma. When heavy monsoon rains stopped campaigning, the British and Indian troops had occupied Imphal, the capital of Manipur state. This lay in a plain astride one of the few practicable routes over the jungle-covered mountains which separated India and Burma. The Japanese commander in Burma, Lieutenant General Sh jir  Iida, was asked for his opinion on whether a renewed advance should be made into India after the rains ended. After conferring with his divisional commanders, Iida reported that it would be unwise to do so, because of the difficult terrain and supply problems.
During the year and a half which followed, the Allies reconstructed the lines of communication to Assam, in north-east India. The United States Army (with large numbers of Indian labourers) created several airbases in Assam from which supplies were flown to the Nationalist Chinese government under Chiang Kai-shek and American airbases in China. This air route, which crossed several mountain ranges, was known as the hump. The Americans also began constructing the Ledo road, which they intended would form a land link from Assam to China.
In mid-1943, the Japanese command in Burma had been reorganised. General Iida was posted back to Japan and a new headquarters, Burma Area Army, was created under Lieutenant-General Masakasu Kawabe. One of its subordinate formations, responsible for the central part of the front facing Imphal and Assam, was Fifteenth Army, whose new commander was Lieutenant-General Renya Mutaguchi.
From the moment he took command, Mutaguchi forcefully advocated an invasion of India. Rather than seeking a mere tactical victory, he planned to exploit the capture of Imphal by advancing to the Brahmaputra River valley, thereby cutting the Allied supply lines to their front in northern Burma, and to the airfields supplying the Nationalist Chinese. His motives for doing so appear to be complex. In late 1942, when consulted by Lieutenant General Iida about the advisability of continuing the Japanese advance, he had been particularly vocal in his opposition, as the terrain appeared to be too difficult and the logistic problems seemed impossible to overcome. He had thought at the time that this plan originated at a local level, but was ashamed of his earlier caution when he found that Imperial Army HQ had originally advocated it.
By design or chance, Mutaguchi had played a major part in several Japanese victories, ever since the Marco Polo Bridge incident in 1937. He believed it was his destiny to win the decisive battle of the war for Japan. Mutaguchi was also goaded by the first Chindit expedition launched by the British under Orde Wingate early in 1943. Wingate's troops had traversed terrain which Mutaguchi had earlier claimed would be impassable to the Japanese 18th Division which he commanded at the time. The Allies had widely publicised the successful aspects of Wingate's expedition while concealing their losses to disease and exhaustion, misleading Mutaguchi and some of his staff as to the difficulties they would later face.



Between 24 June and 27 June 1943, a planning conference was held in Rangoon. Mutaguchi's Chief of Staff, Major General Todai Kunomura, presented Mutaguchi's plan, but was brusquely overruled. The staff of Burma Area Army objected to Kunomura pre-empting their own limited plans to push the Japanese forward defensive lines a short distance into the mountainous frontier with India.
Mutaguchi's plan was nevertheless examined. Lieutenant General Eitaro Naka, (Burma Area Army's Chief of Staff), Major General Masazumi Inada, (the Vice Chief of Staff of Southern Expeditionary Army Group) and even Lieutenant General Gonpachi Kondo from Imperial General Headquarters all pointed out tactical and logistical weaknesses in Mutaguchi's plan. However, Lieutenant General Kawabe did not expressly forbid Mutaguchi to carry out his ideas.
At subsequent exercises at Fifteenth Army's headquarters in Maymyo and at Southern Expeditionary Army Group's headquarters in Singapore, Lieutenant General Naka appeared to have been won over to Mutaguchi's ideas. Lieutenant General Inada was still opposed, but put forward to Kunomura and Major Iwaichi Fujiwara (one of Mutaguchi's staff officers) the apparently frivolous idea of attacking into the Chinese province of Yunnan instead. However, Inada was removed from Southern Expeditionary Army on 11 October 1943, after being made the scapegoat for failures to comply with an agreement to cede territories to Thailand which, under Field Marshal Plaek Pibulsonggram, was allied to Japan.
After another map exercise in Singapore on 23 December 1943, Field Marshal Hisaichi Terauchi (Commander in Chief of Southern Expeditionary Army) approved the plan. Inada's replacement, Lieutenant General Kitsuju Ayabe, was despatched to Imperial Army HQ to gain approval. Prime Minister Hideki T j  gave final sanction after questioning a staff officer over aspects of the plan from his bath.
Once this decision was taken, neither Lieutenant General Kawabe nor Field Marshal Terauchi were given any opportunity to call off Mutaguchi's attack, codenamed U-GO or Operation C ( ), nor to exercise much control over it once it was launched.



To some extent, Mutaguchi and Tojo were influenced by Subhas Chandra Bose, who led the Azad Hind, a movement which was dedicated to freeing India from British rule. Bose was also commander in chief of the movement's armed forces, the Azad Hind Fauj or Indian National Army (INA). The INA was composed mainly of former prisoners of war from the British Indian Army who had been captured by the Japanese after the fall of Singapore, and Indian expatriates in South East Asia who had decided to join the nationalist movement.
Bose was eager for the INA to participate in any invasion of India, and persuaded several Japanese that a victory such as Mutaguchi anticipated would lead to the collapse of British rule in India. The idea that their western boundary would be controlled by a more friendly government was attractive to the Japanese. It would also have been consistent with the idea that Japanese expansion into Asia was part of an effort to support Asian government of Asia and counter western colonialism.



The Allies were preparing to take the offensive themselves in early 1944. The Indian XV Corps was advancing in the coastal Arakan Province, while the British IV Corps had pushed two Indian infantry divisions almost to the Chindwin River at Tamu and Tiddim. These two divisions were widely separated and vulnerable to being isolated.
The Japanese planned that a division from the Twenty-Eighth Army would launch a diversionary attack in the Arakan, codenamed Ha Go, in the first week of February. This would attract Allied reserves from Assam, and also create the impression that the Japanese intended to attack Bengal through Chittagong.
In the centre, Mutaguchi's Fifteenth Army would launch the main attack into Manipur in the first week in March, aiming to capture Imphal and Kohima, scattering British forces and forestalling any offensive movements against Burma. In detail, the Fifteenth Army plans were:
The Japanese 33rd Infantry Division under Lieutenant-General Motoso Yanagida would destroy the 17th Indian Infantry Division at Tiddim, then attack Imphal from the south.
Yamamoto Force, formed from units detached from the Japanese 33rd and 15th Divisions under Major-General Tsunoru Yamamoto (commander of 33rd Division's Infantry Group), supported by tanks and heavy artillery, would destroy the 20th Indian Infantry Division at Tamu, then attack Imphal from the east.
The Japanese 15th Infantry Division under Lieutenant-General Masafumi Yamauchi would envelop Imphal from the north.
In a separate subsidiary operation, the Japanese 31st Infantry Division under Lieutenant-General K toku Sat  would isolate Imphal by capturing Kohima, then push onwards to capture the vital Allied supply base at Dimapur in the Brahmaputra valley.
At the insistence of Bose, two brigades from the Indian National Army were also assigned to the attacks on Imphal from the south and east. The Japanese had originally intended using the INA only for reconnaissance and propaganda.
The staff at Burma Area Army had originally thought this plan too risky. They believed it was unwise to separate the attacking forces so widely, but several officers who were vocal in their opposition were transferred. Mutaguchi's divisional commanders were also pessimistic. They thought that Mutaguchi was gambling too heavily on gaining early success to solve supply problems. Some of them thought him a "blockhead", or reckless.



In early 1944, the Allied formations in Assam and Arakan were part of the British Fourteenth Army, commanded by Lieutenant General William Slim. Over the preceding year, since the failure of an earlier offensive in the Arakan, he and his predecessor, General George Giffard, had been striving to improve the health, training and morale of the British and Indian units of the army. Through improvements in the lines of communication, better administration in the rear areas, and above all, better supply of fresh rations and medicines, these efforts had been successful. The Allies had also developed methods to counter the standard Japanese tactics of outflanking and isolating formations. In particular, they would increasingly depend upon aircraft to supply cut-off units. The Japanese had not anticipated this, and their attacks would be thwarted several times.
From various intelligence sources, Slim and Lieutenant General Geoffrey Scoones (commanding Indian IV Corps) had learned of the general intentions of the Japanese to launch an offensive, although they did not have specific information on the Japanese objectives and were to be surprised several times when the Japanese did launch their attacks. Rather than anticipate the Japanese by attacking across the Chindwin, or trying to defend the line of the river itself, Slim intended to exploit the Japanese logistical weaknesses by withdrawing into Imphal to fight a defensive battle where the Japanese would be unable to supply their troops.




The diversionary Japanese attack in Arakan began on 5 February. A force from the Japanese 55th Division infiltrated the lines of Indian XV Corps to overrun an Indian divisional headquarters and isolate the Corps' forward divisions. When they tried to press their attacks against a hastily-fortified administrative area known as the "Admin Box", they found that Allied aircraft dropped supplies to the garrison, while the Japanese themselves were cut off from their supply sources and starved. British and Indian tanks and infantry broke through a hill pass to relieve the defenders of the Box. The badly supplied and starving Japanese forces were forced to withdraw.







The main U Go offensive began on 6 March 1944. Slim and Scoones had given their forward divisions orders to withdraw too late. The 20th Indian Division withdrew safely, but the 17th Indian Division was cut off and forced to fight its way back into the Imphal plain. Scoones was forced to commit almost all his reserves to help the 17th Division. Because the diversionary offensive in the Arakan had already failed, the Allies were able to fly a division (including its artillery and front-line transport) from the Arakan front to Imphal, in time to prevent the Japanese 15th Division overrunning Imphal from the north.
During April, the Japanese attacks against the defences at the edge of the Imphal plain were all held. In May, IV Corps began a counter-offensive, pushing northward to link up with a relieving force fighting its way southward from Kohima. Although the Allied progress was slow, the Japanese 15th Division was forced to withdraw through lack of supply, and the Allies reopened the Kohima-Imphal road on 22 June, ending the siege (although the Japanese continued to mount attacks from the south and east of Imphal).




The battle of Kohima took place in two stages. From 3 to 16 April 1944, the Japanese 31st Division attempted to capture Kohima ridge, a feature which dominated the road from Dimapur to Imphal on which IV Corps at Imphal depended for supply. On 16 April the small British force at Kohima was relieved, and from 18 April to 16 May the newly arrived Indian XXXIII Corps counter-attacked to drive the Japanese from the positions they had captured. At this point, with the Japanese starving, Lieutenant General K toku Sat  ordered his division to withdraw. Although a detachment continued to fight rearguard actions to block the road, XXXIII Corps drove south to link up with the defenders of Imphal on 22 June.



Mutaguchi continued to order fresh attacks, but by late June it was clear that the starving and disease-ridden Japanese formations were in no state to obey. When he realised that none of his formations were obeying his orders for a renewed attack, Mutaguchi finally ordered the offensive to be broken off on 3 July. The Japanese, reduced in many cases to a rabble, fell back to the Chindwin, abandoning their artillery, transport, and soldiers too sick to walk.



The defeat at Kohima and Imphal was the largest defeat to that date in Japanese history. The British and Indian forces had lost around 16,987 men, dead, missing and wounded. The Japanese suffered 60,643 casualties, including 13,376 dead. Most of these losses were the result of starvation, disease and exhaustion.
The defeat resulted in sweeping changes in command within the Japanese Army in Burma. Mutaguchi sacked all his division commanders during the operation, before being sacked himself on 30 August. Kawabe, whose health was broken, was also dismissed. Many of the senior staff officers at the headquarters of Fifteenth Army and Burma Area Army were also transferred to divisional or regimental commands.












Battle of Kohima-Imphal animated battle map by Jonathan Webb