The Honeywell Level 6 was a line of 16-bit minicomputers, later upgraded to 32-bits, manufactured by Honeywell, Inc. from the mid 1970s. In 1979 the Level 6 was renamed the DPS 6, subsequently DPS 6 Plus and finally DPS 6000.



As initially introduced the Level 6 consisted of three models: the 6/06, the 6/34, and the 6/36. The CPU featured a real-time clock, a ROM bootstrap loader and 64 interrupt levels. The architecture provided a variety of addressing modes and 18 programmer-visible registers. Rack-mount and tabletop versions were available.
These systems supported up to 64 K words (KW) of MOS memory with a cycle time or 650 nanoseconds.
All three models all featured the Megabus, which was a proprietary asynchronous bus architecture.
By 1978 the line had been extended downwards with the introduction of the 6/23 and 6/33, and upwards with the 6/43, 6/47, 6/53, and 6/57. The 6/23 did not support the Megabus. The 6/33 was the new entry-level upgradable model. The other four models supported up to 1 MW (Mega Words) of memory and 26 registers. A memory management unit (MMU), optional on the 6/43 and 6/47, and standard on the 6/53 and 6/57, supported memory segmentation and four protection rings. An optional Scientific Instruction Processor (SIP) added single- and double-precision hardware floating-point instructions. The 6/47 and 6/57 were enhanced versions of the 6/43 and 6/53 respectively which added a Commercial Instruction Processor (CIP) including 30 additional instructions for character-string manipulation and decimal arithmetic. Among the final developments in the line were the high-end 32-bit 6/95-1, 6/98-1 and dual processor 6/95-2 and 6/98-2 models.
In the 1980s, Honeywell's Datanet 8 line of communications processors, often used as front-end processors for DPS 8 mainframes, shared many hardware components with DPS 6. Another specialised derivative of the Level 6 was the Honeywell Page Printing System.
In June 1986, following Honeywell Information Systems' merger with Bull, Honeywell Bull introduced the DPS 6 Plus line of symmetric multiprocessing 32-bit systems, models 410 and 420 (code named MRX - Medium Range eXtended) with up to four processors.In 1987 they introduced the uniprocessor models 210 and 220 (code named LRX - Low Range eXtended), announced the HRX (High Range eXtended), and Computerworld reported that there were more than 50,000 DPS 6 systems installed worldwide. The HRX was introduced as the DPS 6000 600 series. Recognising the commercial success of Unix, in 1988 Honeywell Bull introduced an 80386-based Unix co-processor for the DPS 6 Plus 400 series.



The operating system for the Level 6 was GCOS 6.
GCOS 6 Mod 200 was an entry version of the system oriented toward interactive data entry.
GCOS 6 Mod 400 was a batch operating system also used to run process-control applications.
GCOS 6 Mod 600 was a time-sharing operating system.
The DPS 6 Plus line ran HVS 6 Plus.



General Comprehensive Operating System






Level 6 documentation at Bitsavers