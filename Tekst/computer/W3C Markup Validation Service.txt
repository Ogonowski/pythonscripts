The Markup Validation Service is a validator by the World Wide Web Consortium (W3C) that allows Internet users to check HTML and XHTML documents for well-formed markup. Markup validation is an important step towards ensuring the technical quality of web pages. However, it is not a complete measure of web standards conformance. Though W3C validation is important for browser compatibility and site usability, it has not been confirmed what effect it has on search engine optimization.



The Markup Validation Service began as The Kinder, Gentler HTML Validator, a project by Gerald Oskoboiny. It was developed to be a more intuitive version of the first online HTML validator written by Dan Connolly and Mark Gaither, which was announced on July 13, 1994.
In September 1997, Oskoboiny began working for the W3C, and on December 18, 1997, the W3C announced its W3C HTML Validator based upon his works.
W3C also offers validation tools for web technologies other than HTML/XHTML, such as CSS, XML schemas, and MathML.



Many major web browsers are often tolerant of certain types of error, and may display a document successfully even if it is not syntactically correct. Certain other XML documents can also be validated if they refer to an internal or external DTD.



All mark-up validators suffer from an inability to see the "big picture" on a web page. However, they excel at picking up missed closing tags and other technicalities. This does not mean that the page will display as the author intended in all browsers.
DTD-based validators are also limited in their ability to check attribute values according to many specification documents. For example, using an HTML 4.01 DOCTYPE, bgcolor="fffff" is accepted as valid for the "body" element even though the value "fffff" is missing a preceding '#' character and contains only five (instead of six) hexadecimal digits. Also, for the "img" element, width="really wide" is also accepted as valid. DTD-based validators are technically not able to test for these types of attribute value problems.
Furthermore, even if validated, all web pages should be tested in as many different browsers as possible to ensure that the limitations of the validator are compensated for and that the page works correctly.



While the W3C and other HTML and XHTML validators will assess pages coded in those formats, a separate validator like the W3C CSS validator is needed to confirm that there are no errors in the associated Cascading Style Sheet. CSS validators work in a similar manner to HTML and XHTML validators in that they apply current CSS standards to referenced CSS documents.



HTML Tidy, an offline markup validation program developed by Dave Raggett of W3C
CSE HTML Validator, an offline HTML and CSS validator
World Wide Web Consortium (W3C)






The W3C Markup Validation Service
The W3C CSS Validation Service
CSS Validation Tool
SonarQube CSS Plugin, a code quality tool that provides a CSS validator as well as CSS good practice checks