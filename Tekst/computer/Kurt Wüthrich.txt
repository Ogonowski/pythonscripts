Kurt W thrich (born October 4, 1938 in Aarberg, Canton of Bern) is a Swiss chemist/biophysicist and Nobel Chemistry laureate, known for developing Nuclear Magnetic Resonance (NMR) methods for studying biological macromolecules. 



Born in Aarberg, Switzerland, W thrich was educated in chemistry, physics, and mathematics at the University of Berne before pursuing his Ph.D. supervised by Silvio Fallab at the University of Basel, awarded in 1964. 



After his PhD, W thrich continued postdoctoral research with Fallab for a short time before leaving to work at the University of California, Berkeley for two years from 1965 with Robert E. Connick. That was followed by a stint working with Robert G. Shulman at the Bell Telephone Laboratories in Murray Hill, New Jersey from 1967 to 1969.
W thrich returned to Switzerland, to Z rich, in 1969, where he began his career there at the ETH Z rich, rising to Professor of Biophysics by 1980. He currently maintains a laboratory both at the ETH Z rich and at The Scripps Research Institute, in La Jolla, California. He has also been a visiting professor at the University of Edinburgh (1997-2000), the Chinese University of Hong Kong (where he was an Honorary Professor) and Yonsei University.
During his graduate studies W thrich started out working with electron paramagnetic resonance spectroscopy and the subject of his Ph. D thesis was "the catalytic activity of copper compounds in autoxidation reactions". During his time as a postdoc in Berkeley he began working with the newly developed and related technique of nuclear magnetic resonance spectroscopy to study the hydration of metal complexes. When W thrich joined the Bell Labs, he was put in charge of one of the first superconducting NMR spectrometers, and started studying the structure and dynamics of proteins. He has pursued this line of research ever since.
After returning to Switzerland, W thrich collaborated with among others nobel laureate Richard R. Ernst on developing the first two-dimensional NMR experiments, and established the nuclear Overhauser effect as a convenient way of measuring distances within proteins. This research later led to the complete assignment of resonances for among others the bovine pancreatic trypsin inhibitor and glucagon.
In October 2010, W thrich participated in the USA Science and Engineering Festival's Lunch with a Laureate program where middle and high school students will get to engage in an informal conversation with a Nobel Prize winning scientist over a brown-bag lunch. W thrich is also a member on the USA Science and Engineering Festival's Advisory Board.



He was awarded the Louisa Gross Horwitz Prize from Columbia University in 1991, the Otto Warburg Medal in 1999 and half of the Nobel Prize in Chemistry in 2002 for "his development of nuclear magnetic resonance spectroscopy for determining the three-dimensional structure of biological macromolecules in solution". He was elected a Foreign Member of the Royal Society (ForMemRS) in 2010.


