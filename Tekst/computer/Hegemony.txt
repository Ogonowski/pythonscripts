Hegemony (UK /h m ni/ or / h d m ni/, US /h d m ni/ or / h d mo ni/; Greek:   h gemon a, "leadership, rule") is the political, economic, or military predominance or control of one state over others. In Ancient Greece (8th century BCE   6th century CE), hegemony denoted the politico military dominance of a city-state over other city-states. The dominant state is known as the hegemon.
In the 19th century, hegemony came to denote the "Social or cultural predominance or ascendancy; predominance by one group within a society or milieu". Later, it could be used to mean "a group or regime which exerts undue influence within a society." Also, it could be used for the geopolitical and the cultural predominance of one country over others; from which was derived hegemonism, as in the idea that the Great Powers meant to establish European hegemony over Asia and Africa.
The Marxist theory of cultural hegemony, associated particularly with Antonio Gramsci, is the idea that the ruling class can manipulate the value system and mores of a society, so that their view becomes the world view (Weltanschauung): in Terry Eagleton's words, "Gramsci normally uses the word hegemony to mean the ways in which a governing power wins consent to its rule from those it subjugates". In contrast to authoritarian rule, cultural hegemony "is hegemonic only if those affected by it also consent to and struggle over its common sense".

In cultural imperialism, the leader state dictates the internal politics and the societal character of the subordinate states that constitute the hegemonic sphere of influence, either by an internal, sponsored government or by an external, installed government.




From the post-classical Latin word hegemonia, from "1513 or earlier"; or the Greek word  , meaning "authority, rule, political supremacy", related to the word   "leader".






In the Greco Roman world of 5th century BCE European classical antiquity, the city-state of Sparta was the hegemon of the Peloponnesian League (6th to 4th centuries BCE) and King Philip II of Macedon was the hegemon of the League of Corinth in 337 BCE (a kingship he willed to his son, Alexander the Great). Likewise, the role of Athens within the short-lived Delian League (478-404 BCE) was that of a "hegemon". Ancient historians such as Xenophon and Ephorus were the first who used the term in its modern sense.
In Ancient East Asia, Chinese hegemony existed during the Spring and Autumn Period (c. 770   480 BCE), when the weakened rule of the Eastern Zhou Dynasty led to the relative autonomy of the Five Hegemons (Ba in Chinese [ ]). They were appointed by feudal lord conferences, and thus were nominally obliged to uphold the imperium of the Zhou Dynasty over the subordinate states.



1st and 2nd century Europe was dominated by the hegemonic peace of the Pax Romana. It was instituted by the emperor Augustus, and was accompanied by a series of brutal military campaigns.
From the 7th century to the 12th century, the Umayyad Caliphate and later Abbasid Caliphate dominated the vast territories they governed, with other states like Byzantine Empire paying tribute.
In 7th century India, Harsha, ruler of a large empire in northern India from 606 to 647 CE, brought most of the north under his hegemony. He preferred not to rule as a central government, but left "conquered kings on their thrones and contenting himself with tribute and homage."
From the late 9th to the early 11th century, the empire developed by Charlemagne achieved hegemony in Europe, with dominance over France, Italy and Burgundy.



Writing in The Politics of International Political Economy, Jayantha Jayman writes "If we consider the Western dominated global system from as early as the 15th century, there have been several hegemonic powers and contenders that have attempted to create the world order in their own images." He lists several contenders for historical hegemony.

Portugal 1494 to 1580 (end of Italian Wars to Spanish invasion of Portugal). Based on Portugal's dominance in navigation.
Holland 1580 to 1688 (1579 Treaty of Utrecht marks the foundation of the Dutch Republic to William of Orange's arrival in England). Based on Dutch control of credit and money.
Britain 1688 to 1792 (Glorious Revolution to Napoleonic Wars). Based on British textiles and command of the high seas.
Britain 1815 to 1914 (Congress of Vienna to World War I). Based on British industrial supremacy and railroads.

To this list could be added the hegemony of Habsburg Spain in 16th century Europe. However, after an attempt by Phillip IV to restore it, by the middle of the 17th century "Spain's pretensions to hegemony had definitely and irremediably failed."
In late 16th and 17th-century Holland, the Dutch Republic's mercantilist dominion was an early instance of commercial hegemony, made feasible with the development of wind power for the efficient production and delivery of goods and services. This, in turn, made possible the Amsterdam stock market and concomitant dominance of world trade.
In France, King Louis XIV (1638 1715) and (Emperor) Napoleon I (1799 1815) attempted French hegemony via economic, cultural and military domination of most of Continental Europe. However, Jeremy Black writes that, because of Britain, France "was unable to enjoy the benefits" of this hegemony.
After the defeat and exile of Napoleon, hegemony largely passed to the British Empire, which became the largest empire in history, with Queen Victoria (1837 1901) ruling over one-quarter of the world's land and population at its zenith. Like the Dutch, the British Empire was primarily seaborne; many British possessions were located around the rim of the Indian Ocean, as well as numerous islands in the Pacific Ocean and the Caribbean Sea. Britain also controlled the Indian subcontinent and large portions of Africa.
In Europe, Germany, rather than Britain, may have been the strongest power after 1871, but Samuel Newland writes:

Bismarck defined the road ahead as   no expansion, no push for hegemony in Europe. Germany was to be the strongest power in Europe but without being a hegemon.   His basic axioms were first, no conflict among major powers in Central Europe; and second German security without German hegemony."



The early 20th century, like the late 19th century was characterized by multiple Great Powers but no global hegemon. World War I weakened the strongest of the Imperial Powers, Great Britain, but also strengthened the United States and, to a lesser extent, Japan. Both of these states' governments pursued policies to expand their regional spheres of influence, the U.S. in Latin America and Japan in East Asia. France, the UK, Italy, the Soviet Union and later Nazi Germany (1933 1945) all either maintained imperialist policies based on spheres of influence or attempted to conquer territory but none achieved the status of a global hegemonic power.
After the Second World War, the United Nations was established and the five strongest global powers (China, France, the UK, the US, and the USSR) were given permanent seats on the U.N. Security Council, the organization's most powerful decision making body. Following the war, the US and the USSR were the two strongest global powers and this created a bi-polar power dynamic in international affairs, commonly referred to as the Cold War. The hegemonic conflict was ideological, between communism and capitalism, as well as geopolitical, between the Warsaw Pact countries (1955 1991) and NATO/SEATO/CENTO countries (1949 present). During the Cold War both hegemons competed against each other directly (during the arms race) and indirectly (via proxy wars). The result was that many countries, no matter how remote, were drawn into the conflict when it was suspected that their governments' policies might destabilise the balance of power. Reinhard Hildebrandt calls this a period of "dual-hegemony", where "two dominant states have been stabilizing their European spheres of influence against and alongside each other." Proxy wars became battle grounds between forces supported either directly or indirectly by the hegemonic powers and included the Korean War, the Laotian Civil War, the Arab Israeli conflict, the Vietnam War, the Afghan War, the Angolan Civil War, and the Central American Civil Wars.
Following the dissolution of the Soviet Union in 1991 the United States was the world's sole hegemonic power.



Various perspectives on whether the US was or continues to be a hegemon have been presented since the end of the Cold War. The American political scientists John Mearsheimer and Joseph Nye have argued that the US is not a true hegemon because it has neither the financial nor the military resources to impose a proper, formal, global hegemony. On the other hand, Anna Cornelia Beyer, in her book about counter-terrorism, argues that global governance is a product of American leadership and describes it as hegemonic governance.
The French Socialist politician Hubert V drine in 1999 described the US as a hegemonic hyperpower, because of its unilateral military actions worldwide.
Pentagon's strategist, Edward Luttwak, in The Grand Strategy of the Roman Empire outlined three stages, with hegemonic being the first and followed by imperial. In his view the transformation proved to be fatal and eventually led to the fall of the Roman Empire. His book is am implicit advice to Washington to continue the present hegemonic strategy and refrain from establishing empire.
Historian Max Ostrovsky in his Y = Arctg X: The Hyperbolae of the World Order demonstrated that the Roman transformation from hegemony into empire led to the apogee of the Roman power and civilization (Pax Romana) rather than crisis while the fall was much later and due to other reasons. In addition, Ostrovsky analyzed Rome's contemporary hegemony of Ch'in the state which conquered the Chinese world between 364 and 221 BC. The analogous transformation never led to a fatal fall. China overcame all crises and preserved its ancient unity until today. The thesis demonstrates that both Rome and Ch'in attempted to preserve hegemony but were forced to turn it into empire due to recurring security crises. In fact, all three powers Rome, Ch'in and the United States began with isolationism and were compelled to advance to hegemony due to security crises (World War II in the US' case). Ostrovsky suggests that hegemony is an intermediate stage between states system and empire. This proved to be the case for the two ancient hegemonies and soon is likely to be for the modern hegemony too.




In the historical writing of the 19th century, the denotation of hegemony extended to describe the predominance of one country upon other countries; and, by extension, hegemonism denoted the Great Power politics (c. 1880s   1914) for establishing hegemony (indirect imperial rule), that then leads to a definition of imperialism (direct foreign rule). In the early 20th century, in the field of international relations, the Italian Marxist philosopher Antonio Gramsci developed the theory of cultural domination (an analysis of economic class) to include social class; hence, the philosophic and sociologic theory of cultural hegemony analysed the social norms that established the social structures (social and economic classes) with which the ruling class establish and exert cultural dominance to impose their Weltanschauung (world view) justifying the social, political, and economic status quo as natural, inevitable, and beneficial to every social class, rather than as artificial social constructs beneficial solely to the ruling class.
From the Gramsci analysis derived the political science denotation of hegemony as leadership; thus, the historical example of Prussia as the militarily and culturally predominant province of the German Empire (Second Reich 1871 1918); and the personal and intellectual predominance of Napoleon Bonaparte upon the French Consulate (1799 1804). Contemporarily, in Hegemony and Socialist Strategy (1985), Ernesto Laclau and Chantal Mouffe defined hegemony as a political relationship of power wherein a sub-ordinate society (collectivity) perform social tasks that are culturally unnatural and not beneficial to them, but that are in exclusive benefit to the imperial interests of the hegemon, the superior, ordinate power; hegemony is a military, political, and economic relationship that occurs as an articulation within political discourse. Beyer analysed the contemporary hegemony of the United States at the example of the Global War on Terrorism and presented the mechanisms and processes of American exercise of power in 'hegemonic governance'.



Academics have argued that in the praxis of hegemony, imperial dominance is established by means of cultural imperialism, whereby the leader state (hegemon) dictates the internal politics and the societal character of the subordinate states that constitute the hegemonic sphere of influence, either by an internal, sponsored government or by an external, installed government. The imposition of the hegemon's way of life an imperial lingua franca and bureaucracies (social, economic, educational, governing) transforms the concrete imperialism of direct military domination into the abstract power of the status quo, indirect imperial domination. Critics have said that this view is "deeply condescending" and "treats people [ ] as blank slates on which global capitalism s moving finger writes its message, leaving behind another cultural automaton as it moves on."
Suggested examples of cultural imperialism include the latter-stage Spanish and British Empires, the 19th and 20th century Reichs of unified Germany (1871 1945), and by the end of the 20th century, the United States.
Culturally, hegemony also is established by means of language, specifically the imposed lingua franca of the hegemon (leader state), which then is the official source of information for the people of the society of the sub-ordinate state. Writing on language and power, Andrea Mayr says, "As a practice of power, hegemony operates largely through language." In contemporary society, an example of the use of language in this way is in the way Western countries set up educational systems in African countries mediated by Western languages.
Another example of this is found in the way language helped "diminish the traditions" of African Americans in the US.









Beyer, Anna Cornelia (2010). Counterterrorism and International Power Relations: The EU, ASEAN and Hegemonic Global Governance. London: IB Tauris. 
DuBois, T. D. (2005). "Hegemony, Imperialism and the Construction of Religion in East and Southeast Asia". History & Theory 44 (4): 113 131. doi:10.1111/j.1468-2303.2005.00345.x. 
Hopper, P. (2007). Understanding Cultural Globalization (1st ed.). Malden, MA: Polity Press. ISBN 978-0-7456-3557-6. 
Howson, Richard, ed. (2008). Hegemony: Studies in Consensus and Coercion. Psychology Press. ISBN 978-0-415-95544-7. 
Joseph, Jonathan (2002). Hegemony: A Realist Analysis. Routledge. ISBN 0-415-26836-2. 
Slack, Jennifer Daryl (1996). "The Theory and Method of Articulation in Cultural Studies". In Morley, David & Chen, Kuan-Hsing. Stuart Hall: Critical Dialogues in Cultural Studies. Routledge. pp. 112 127. 



Hegemonism Hegemony at DMOZ
Mike Dorsher, Ph.D., Hegemony Online: The Quiet Convergence of Power, Culture and Computers
Hegemony and the Hidden Persuaders   the Power of Un-common sense
Parag Khanna, Waving Goodbye to Hegemony