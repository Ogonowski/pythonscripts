The Intel Pentium brand refers to mainstream x86-architecture microprocessors from Intel. Processors branded Pentium Processor with MMX Technology (and referred to as Pentium MMX for brevity) are also listed here.









Based on P5
Steppings: B1, C1, D1



Based on P5 microarchitecture



Based on P5 microarchitecture



Based on P5 microarchitecture



Based on P5 microarchitecture




Desktop processors based on the P6 microarchitecture were marketed as Pentium Pro, Pentium II and Pentium III, as well as variations of these names.




Desktop processors based on the NetBurst microarchitecture were marketed as Pentium 4 and Pentium D.



Earlier E5xxx desktop processors based on the Core microarchitecture were marketed as Pentium Dual-Core, while later E5xxx and all E6xxx models were called Pentium. Note however, that several resellers will still refer to the newer generation processors as Pentium Dual-Core.



The Intel Pentium Dual-Core processors, E2140, E2160, E2180, E2200, and E2220 use the Allendale core, which includes 2 MB of native L2 cache, with half disabled leaving only 1 MB. This compares to the higher end Conroe core which features 4 MB L2 Cache natively. Intel has shifted its product lines having the Core 2 line as Mainstream/Performance, Pentium Dual-Core as Mainstream, and the new Celeron (based on the Conroe-L core) as Budget/Value.
Based on the 64-bit Core microarchitecture.
All models support: MMX, SSE, SSE2, SSE3, SSSE3, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation)
Die size: 77 mm  (Allendale-1M), 111 mm  (Allendale), 143 mm  (Conroe)
Steppings: L2, M0 (Allendale), G0 (Conroe)



 The E5000 series and E6000 series use the same 45 nm Wolfdale-3M core as the E7000 series Core 2s, which has 3 MB L2 cache natively. 1 MB of L2 cache is disabled, for a total of 2 MB L2 cache, or twice the amount in the original Allendale Pentiums. The Wolfdale core is capable of SSE4, but it is disabled in these Pentiums. Pentium E2210 is an OEM processor based on Wolfdale-3M with only 1 MB L2 cache enabled out of the total 3 MB.
All models support: MMX, SSE, SSE2, SSE3, SSSE3, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x
Die size: 82 mm 
Steppings: R0
Based on the Penryn microarchitecture
Part of 3MB L2 Cache Disabled
E2210 is a Wolfdale-3M with 2MB cache disabled unlike all other E22xx, which are Allendale.
E5000-series processors were initially known as Pentium Dual-Core, while all later processors were just Pentium.
E6500K has unlocked multiplier, and is only available in China as limited edition.
Models with a part number ending in "ML" support Intel VT-x.






Note that these are also dual core, but under the Pentium brand.
Based on Westmere microarchitecture
All models support: MMX, SSE, SSE2, SSE3, SSSE3, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x, Smart Cache.
Contains 45 nm "Ironlake" GPU.
G6951 can be unlocked to enable "extra features", which are present in the CPU but deliberately crippled, with the purchase of a $50 upgrade card.






All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x, Smart Cache.
Pentium G8xx supports DDR3-1333 in addition to DDR3-1066.
HD Graphics (Sandy Bridge) contain 6 EUs as well as HD Graphics 2000, but does not support the following technologies: Intel Quick Sync Video, InTru3D, Intel Clear Video HD, Wireless display, Intel insider.
Transistors: 504 million
Die size: 131 mm 



All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x, Smart Cache.
G20xx support up to DDR3-1333 memory while G21xx support up to DDR3-1600.
HD Graphics (Ivy Bridge) contain 6 EUs as well as HD Graphics 2500, but does not support the following technologies: Intel Quick Sync Video, InTru3D, Intel Clear Video HD, Wireless display, Intel insider.






All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x, Smart Cache.
G32xx support up to DDR3-1333 memory while G34xx support up to DDR3-1600.
G3258 (Pentium anniversary edition) has unlocked multiplier.
Haswell Pentiums support QuickSync.
Transistors: 1.4 billion
Die size: 177mm 






All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, Enhanced Intel SpeedStep Technology (EIST), Intel 64, Intel VT-x.
GPU and memory controller are integrated onto the processor die
GPU is based on Ivy Bridge Intel HD Graphics, with 4 execution units, and supports DirectX 11, OpenGL 4.0, OpenGL ES 3.0 and OpenCL 1.1 (on Windows).
Package size: 25 mm   27 mm






All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x, Intel VT-d, AES-NI, Smart Cache.
All models support up to DDR3-1600 or DDR4-2133 memory.
Embedded models support ECC memory.
Transistors: TBD
Die size: TBD









Based on P5 microarchitecture



Based on P5 microarchitecture



Based on P5 microarchitecture



Based on P5 microarchitecture




Mobile processors based on the P6 microarchitecture were marketed as Pentium II, Pentium III, Pentium M and Pentium Dual-Core, as well as variations of these names.




Mobile processors based on the NetBurst microarchitecture were marketed as Pentium 4.



Previous mobile processors based on the Core microarchitecture were marketed as Pentium Dual-Core, while the current models are called Pentium. Note however, that several resellers will still refer to them as Pentium Dual-Core.



Based on the 32-bit Enhanced Pentium M microarchitecture
All models support: MMX, SSE, SSE2, SSE3, Enhanced Intel SpeedStep Technology (EIST), XD bit (an NX bit implementation)
Die size: 90.3 mm 
Steppings: D0
T2060 debuted on January 30, 2007 in notebooks only sold as part of Windows Vista launch bundles; it appears to be OEM-only.
T2060 & T2080 were discovered to be an Intel Core T2050 & T2250 with half the L2 cache (old versions of CPU-Z identified them as T2050 & T2250)



Based on the 64-bit Core microarchitecture
All models support: MMX, SSE, SSE2, SSE3, SSSE3, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation)
Die size: 111 mm 
Steppings: M0



Based on the 64-bit Penryn microarchitecture
All models support: MMX, SSE, SSE2, SSE3, SSSE3, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation)
Die size: 82 mm 
Steppings: R0






Based on Westmere microarchitecture
All models support: MMX, SSE, SSE2, SSE3, SSSE3, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Smart Cache
FSB has been replaced with DMI.
Contains 45 nm "Ironlake" GPU HD Graphics.
Die size: 81 mm 
Transistors: 382 million
Graphics and Integrated Memory Controller die size: 114 mm 
Transistors: 177 million
Stepping: C2, K0






All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Smart Cache.
HD Graphics (Sandy Bridge) contain 6 EUs as well as HD Graphics 2000, but does not support the following technologies: Intel Quick Sync Video, InTru 3D, Clear Video HD, Wireless Display, Intel Insider.
Transistors: 504 million
Die size: 131 mm 



All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x, Smart Cache.
HD Graphics (Ivy Bridge) contain 6 EUs as well as HD Graphics 2500, but does not support the following technologies: Intel Quick Sync Video, InTru 3D, Clear Video HD, Wireless Display, Intel Insider.






All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x, Smart Cache.



All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x, Smart Cache.
3558U also supports Intel Wireless Display.
Transistors: 1.3 billion
Die size: 181 mm 



All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x, Smart Cache.
3561Y also supports Intel Wireless Display.
Transistors: 1.3 billion
Die size: 181 mm 



All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x, Intel VT-d, Smart Cache, Intel Wireless Display, and configurable TDP (cTDP) down
3825U also supports Hyper-threading.






All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x.
GPU and memory controller are integrated onto the processor die
GPU is based on Ivy Bridge Intel HD Graphics, with 4 execution units, and supports DirectX 11, OpenGL 4.0, OpenGL ES 3.0 and OpenCL 1.1 (on Windows).
Package size: 25 mm   27 mm
Transistors: 960 million 
Die size: 130 mm  



All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x, AES-NI.
GPU and memory controller are integrated onto the processor die
GPU is based on Broadwell Intel HD Graphics, with 16 execution units, and supports DirectX 11.2, OpenGL 4.3, OpenGL ES 3.0 and OpenCL 1.2 (on Windows).
Package size: 25 mm   27 mm






All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, Hyper-threading, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x, Intel VT-d, AES-NI, Smart Cache, Intel Wireless Display, and configurable TDP (cTDP) down



All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, Hyper-threading, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x, Intel VT-d, AES-NI, Smart Cache, Intel Wireless Display, and configurable TDP (cTDP) down









All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x, Smart Cache, Hyper-threading.
No models include HD Graphics.
Transistors: 624 or 504 million
Die size: 149 or 131 mm2



Based on Sandy Bridge-E CPU.
All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), TXT, Intel VT-x, Intel VT-d, AES-NI, Smart Cache.






All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, F16C, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), TXT, Intel VT-x, Intel EPT, Intel VT-d, Intel VT-c, Intel x8 SDDC, AES-NI, Smart Cache.
Support for up to 6 DIMMS of DDR3 memory.









All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x, EPT, Hyper-threading, Smart Cache, ECC memory.
Transistors:
Die size:





