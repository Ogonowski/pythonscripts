The Far East Combined Bureau, an outstation of the British Government Code and Cypher School, was set up in Hong Kong in March 1935, to monitor Japanese, and also Chinese and Russian (Soviet) intelligence and radio traffic. Later it moved to Singapore, Colombo (Ceylon) and then Kilindini (Kenya).



The FECB was located in an office block in the Naval dockyard, with an armed guard at the door (which negated any attempt at secrecy). The intercept site was on Stonecutters Island, four miles across the harbour, and manned by a dozen RAF and RN ratings (plus later four Army signallers). The codebreaking or Y section had Japanese, Chinese and Russian interpreters, under RN Paymaster Arthur (Harry) Shaw, with Dick Thatcher and Neil Barnham. The FECB was headed by the Chief of Intelligence Staff (COIS) Captain John Waller, later by Captain F. J. Wylie.
Shaw had been dealing direct with GC&CS and the C-in-C Far East in Shanghai, but found that Waller expected that everything should go through him, and paid little regard to keeping sources secret. So by 1936 the two most senior naval officers were barely on speaking terms. Colonel Valentine Burkhart found when he arrived in 1936 that the bureau was involved in  turf wars , although they eventually accepted that they had no control over the use of intelligence reports.
Initially the Y section was to focus on the three main Japanese Navy codes and cyphers, the Japanese Naval General Cypher, the Flag Officer code and the  tasogare  or basic naval reporting code used to report the sailings of individual ships. In 1938 a section was set up to attack Japanese commercial systems and so to track supply convoys. From 1936 many messages were sent back to London, to be deciphered by John Tiltman, who broke the first version of JN-25 in 1939.



In August 1939, shortly before the outbreak of war with Germany, the FECB moved to Singapore on HMS Birmingham for fear of Japanese attack. A skeleton staff of a codebreaker (Alf Bennett) and four intercept operators were left at Hong Kong, and they were captured by the Japanese on Christmas Day 1941.
FECB went to Seletar Naval Base, and the intercept station to Kranji. An RAF "Y" interception unit, 52 Wireless Unit, arrived in Singapore in early November 1941. As Bletchley Park was concentrating on German Enigma cyphers, many of the Japanese naval section in Hut 7 moved to FECB, Singapore. By May 1940 there were forty people working solely on JN-25, who could read simple messages. The new codebook JN-25B was introduced on 1 December 1940, but was broken immediately as the additives were not changed.
There was interchange with Station CAST at Corregidor, which was better placed to intercept IJN messages, as FECB could only receive the Combined Fleet in home waters at night. FECB also collaborated with the US Army Station 6 intercept site at Fort McKinley near Manila. FECB was sent one of the American Purple machines from Bletchley Park in a warship. Supposed to be sent only by warship or military transport, it was trans-shipped at Durban to the freighter Sussex. The ship s Master said he landed it at the Naval Store Singapore at the end of December 1941, but the Naval Stores Officer denied any knowledge of it; hopefully it was destroyed or dumped in the sea. But the Hollerith tabulating machine (minus a key part, which had to borrowed from the Indian State Railways in Bombay) arrived safely in Colombo.
FECB also cooperated with Kamer 14 (Room 14), the Dutch unit at the Bandung Technical College in Java. Initially some of the FECB people went there after the fall of Singapore. Lieutenant-Commander Leo Brouwer RNN, a Japanese linguist at Kamer 14 was evacuated to Colombo, then Kilindini, and later Hut 7.



With the Japanese advance down the Malay Peninsula, the Army and RAF codebreakers went to the Wireless Experimental Centre in Delhi, India.
The RN codebreakers went to Colombo, Ceylon in January 1942, on the troopship HMS Devonshire (with 12 codebreakers' cars as deck cargo). Pembroke College, an Indian boys school, was requisitioned as a combined codebreaking and wireless interception centre. The FECB worked for Admiral Sir James Somerville, commander-in-chief of the Royal Navy's Eastern Fleet.



In April 1942 most of the RN codebreakers at Colombo moved to Kilindini near Mombasa in Kenya, because of a Japanese task force attack on Colombo. Two codebreakers and the civilian wireless operators were left in Colombo. An Indian boys school at Allidina about a mile outside Mombasa and overlooking the Indian Ocean was requisitioned, hence the name HMS Allidina.
Radio reception was even worse than at Colombo, with only the strongest Japanese signals received. In addition, FRUMEL   the US-Australian-British unit based in Melbourne that replaced CAST   was reportedly reluctant to exchange material. However, in September 1942 Kilindini was able to break the Japanese Merchant Shipping Code, JN-40, because a message was sent twice with extra data. It was a transposition cypher, not a super-enciphered code like JN-25. They also broke JN-152 a simple transposition and substitution cypher for navigation warnings and the previously impenetrable JN-167, another merchant shipping cypher. These successes enabled Allied forces e.g. submarines to attack Japanese supply ships, and resulted in the Japanese merchant marine suffering 90 per cent losses by August 1945.



FECB then moved back to Colombo; the move began in August 1943, with the advance party arriving in Ceylon on 1 September. The location chosen was the Anderson Golf Course six miles from Colombo HQ, hence the name HMS Anderson. Bruce Keith had wanted an up-country site for better reception, but the Chief of Intelligence staff for HQ Eastern Fleet insisted that the codebreakers should be within easy reach of headquarters. While reception was better than at Kilindini, it was affected by a nearby 33 Kv power line and the Racecourse Aerodrome.



Smith wrote that: Only now are the British codebreakers (like John Tiltman, Hugh Foss and Eric Nave) beginning to receive the recognition they deserve for breaking Japanese codes and cyphers.


