Executable compression is any means of compressing an executable file and combining the compressed data with decompression code into a single executable. When this compressed executable is executed, the decompression code recreates the original code from the compressed code before executing it. In most cases this happens transparently so the compressed executable can be used in exactly the same way as the original. Executable compressors are often referred to as "runtime packers", "software packers", "software protectors" (or even "polymorphic packers" and "obfuscating tools").
A compressed executable can be considered a self-extracting archive, where compressed data is packaged along with the relevant decompression code in an executable file. Some compressed executables can be decompressed to reconstruct the original program file without being directly executed. Two programs that can be used to do this are CUP386 and UNP.
Most compressed executables decompress the original code in memory and most require slightly more memory to run (because they need to store the decompressor code, the compressed data and the decompressed code). Moreover, some compressed executables have additional requirements, such as those that write the decompressed executable to the file system before executing it.
Executable compression is not limited to binary executables, but can also be applied to scripts, such as JavaScript. Because most scripting languages are designed to work on human-readable code, which has a high redundancy, compression can be very effective and as simple as replacing long names used to identify variables and functions with shorter versions and/or removing white-space.



Software distributors use executable compression for a variety of reasons, primarily to reduce the secondary storage requirements of their software; as executable compressors are specifically designed to compress executable code, they often achieve better compression ratio than standard data compression facilities such as gzip, zip or bzip2. This allows software distributors to stay within the constraints of their chosen distribution media (such as CD-ROM, DVD-ROM, or Floppy disk), or to reduce the time and bandwidth customers require to access software distributed via the Internet.
Executable compression is also frequently used to deter reverse engineering or to obfuscate the contents of the executable (for example, to hide the presence of malware from antivirus scanners) by proprietary methods of compression and/or added encryption. Executable compression can be used to prevent direct disassembly, mask string literals and modify signatures. Although this does not eliminate the chance of reverse engineering, it can make the process more costly.
A compressed executable requires less storage space in the file system, thus less time to transfer data from the file system into memory. On the other hand, it requires some time to decompress the data before execution begins. However, the speed of various storage media has not kept up with average processor speeds, so the storage is very often the bottleneck. Thus the compressed executable will load faster on most common systems. On modern desktop computers, this is rarely noticeable unless the executable is unusually big, so loading speed is not a primary reason for or against compressing an executable.
On operating systems which read executable images on demand from the disk (see virtual memory), compressed executables make this process less efficient. The decompressor stub allocates a block of memory to hold the decompressed data, which stays allocated as long as the executable stays loaded, whether it is used or not, competing for memory resources with other applications all along. If the operating system uses a swap file, the decompressed data has to be written to it to free up the memory instead of simply discarding unused data blocks and reloading them from the executable image if needed again. This is usually not noticeable, but it becomes a problem when an executable is loaded more than once at the same time the operating system cannot reuse data blocks it has already loaded, the data has to be decompressed into a new memory block, and will be swapped out independently if not used. The additional storage and time requirements mean that it has to be weighed carefully whether to compress executables which are typically run more than once at the same time.
Another disadvantage is that some utilities can no longer identify run-time library dependencies, as only the statically linked extractor stub is visible.
Also, some older virus scanners simply report all compressed executables as viruses because the decompressor stubs share some characteristics with those. Most modern virus scanners can unpack several different executable compression layers to check the actual executable inside, but some popular anti-virus and anti-malware scanners have had troubles with false positive alarms on compressed executables. In an attempt to solve the problem of malware obfuscated with the help of runtime packers the IEEE Industry Connections Security Group has introduced a software taggant system.
Executable compression used to be more popular when computers were limited to the storage capacity of floppy disks, which were both slow and low capacity media, and small hard drives; it allowed the computer to store more software in the same amount of space, without the inconvenience of having to manually unpack an archive file every time the user wanted to use the software. However, executable compression has become less popular because of increased storage capacity on computers. It has its use in the demoscene where demos have to stay within a size limit like 64 kilobytes to enter some competitions. Only very sophisticated compression formats, which add to load time, keep an executable small enough to enter these competitions.






Note: Clients in purple are no longer in development.



PackWin
WinLite



NeLite
LxLite



32LiTE
624
AINEXE
aPACK
DIET
HASP Envelope
LGLZ
LZEXE (by Fabrice Bellard)   First widely publicly used executable compressor for microcomputers.
PKLite
PMWLITE
UCEXE
UPX
WDOSX
WWpack
XE



gzexe
HASP Envelope
UPX



.NETZ
NsPack
Mpress
HASP Envelope
.netshrink
dotBundle
Exepack.NET
DotProtect: Commercial protector/packer for .net and mono. Features on-line verifications and "industry standard encryption".



HASP Envelope
UPX
VMProtect






HASP Envelope
pack200



HASP Envelope



There are two types of compression that can be applied to scripts:
Reduce the redundancy in the script (by removing comments, white space and shorten variable and functions names). This does not alter the behavior of the script.
Compress the original script and create a new script that contains decompression code and compressed data. This is similar to binary executable compression.



These compress the original script and output a new script that has a decompressor and compressed data.
JsSfx
Packify



These remove white space, remove comments, and shorten variable and function names but do not alter the behavior of the script.
Packer
YUI compressor
Shrinksafe
JSMin



Data compression
Disk compression
Executable
Kolmogorov complexity
UPX
Self-extracting archive


