The World Intellectual Property Organization (WIPO) is one of the 17 specialized agencies of the United Nations.
WIPO was created in 1967 "to encourage creative activity, to promote the protection of intellectual property throughout the world."
WIPO currently has 188 member states, administers 26 international treaties, and is headquartered in Geneva, Switzerland. The current Director-General of WIPO is Francis Gurry, who took office on October 1, 2008. 186 of the UN Members as well as the Holy See and Niue are Members of WIPO. Non-members are the states of Marshall Islands, Federated States of Micronesia, Nauru, Palau, Solomon Islands, South Sudan and Timor-Leste. Palestine has observer status.



The predecessor to WIPO was the BIRPI (Bureaux Internationaux R unis pour la Protection de la Propri t  Intellectuelle, French acronym for United International Bureaux for the Protection of Intellectual Property), which had been established in 1893 to administer the Berne Convention for the Protection of Literary and Artistic Works and the Paris Convention for the Protection of Industrial Property.
WIPO was formally created by the Convention Establishing the World Intellectual Property Organization, which entered into force on April 26, 1970. Under Article 3 of this Convention, WIPO seeks to "promote the protection of intellectual property throughout the world." WIPO became a specialized agency of the UN in 1974. The Agreement between the United Nations and the World Intellectual Property Organization notes in Article 1 that WIPO is responsible
"for promoting creative intellectual activity and for facilitating the transfer of technology related to industrial property to the developing countries in order to accelerate economic, social and cultural development, subject to the competence and responsibilities of the United Nations and its organs, particularly the United Nations Conference on Trade and Development, the United Nations Development Programme and the United Nations Industrial Development Organization, as well as of the United Nations Educational, Scientific and Cultural Organization and of other agencies within the United Nations system."
The Agreement marked a transition for WIPO from the mandate it inherited in 1967 from BIRPI, to promote the protection of intellectual property, to one that involved the more complex task of promoting technology transfer and economic development.
Unlike other branches of the United Nations, WIPO has significant financial resources independent of the contributions from its Member States. In 2006, over 90 percent of its income of just over CHF 250 million was expected to be generated from the collection of fees by the International Bureau (IB) under the intellectual property application and registration systems which it administers (the Patent Cooperation Treaty, the Madrid system for trade marks and the Hague system for industrial designs).
In October 2004, WIPO agreed to adopt a proposal offered by Argentina and Brazil, the "Proposal for the Establishment of a Development Agenda for WIPO" - from the Geneva Declaration on the Future of the World Intellectual Property Organization. This proposal was well supported by developing countries. The agreed "WIPO Development Agenda" (composed of over 45 recommendations) was the culmination of a long process of transformation for the organization from one that had historically been primarily aimed at protecting the interests of rightholders, to one that has increasingly incorporated the interests of other stakeholders in the international intellectual property system as well as integrating into the broader corpus of international law on human rights, environment and economic cooperation.
A number of civil society bodies have been working on a draft Access to Knowledge (A2K) treaty which they would like to see introduced.
In December 2011, WIPO published its first World Intellectual Property Report on the Changing Face of Innovation, the first such report of the new Office of the Chief Economist. WIPO is also a co-publisher of the Global Innovation Index.



WIPO has established WIPOnet, a global information network. The project seeks to link over 300 intellectual property offices (IP offices) in all WIPO Member States. In addition to providing a means of secure communication among all connected parties, WIPOnet is the foundation for WIPO's intellectual property services.



1st: Georg Bodenhausen, 1970 1973
2nd:  rp d Bogsch, 1973 1997
3rd: Kamil Idris, 1997 2008
4th: Francis Gurry, 2008 present



Anti-Counterfeiting Trade Agreement
List of parties to international copyright agreements
Member states of the World Intellectual Property Organization
Substantive Patent Law Treaty (SPLT)
Uniform Domain-Name Dispute-Resolution Policy
United States and the United Nations
World Intellectual Property Day (April 26)
World Intellectual Property Organization treaties
World Intellectual Wealth Organisation






Official website
List of member states