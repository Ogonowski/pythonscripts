The software industry includes businesses for development, maintenance and publication of software that are using different business models, mainly either "license/maintenance based" (on-premises) or "Cloud based" (such as SaaS, PaaS, IaaS, MaaS, AaaS, etc.). The industry also includes software services, such as training, documentation, consulting and Data recovery.



The word "software" was coined as a prank as early as 1953, but did not appear in print until the 1960s. Before this time, computers were programmed either by customers, or the few commercial computer vendors of the time, such as UNIVAC and IBM. The first company founded to provide software products and services was Computer Usage Company in 1955.
The software industry expanded in the early 1960s, almost immediately after computers were first sold in mass-produced quantities. Universities, government, and business customers created a demand for software. Many of these programs were written in-house by full-time staff programmers. Some were distributed freely between users of a particular machine for no charge. Others were done on a commercial basis, and other firms such as Computer Sciences Corporation (founded in 1959) started to grow. Other influential or typical software companies begun in the early 1960s included Advanced Computer Techniques, Automatic Data Processing, Applied Data Research, and Informatics General. The computer/hardware makers started bundling operating systems, systems software and programming environments with their machines.
When Digital Equipment Corporation (DEC) brought a relatively low-priced microcomputer to market, it brought computing within the reach of many more companies and universities worldwide, and it spawned great innovation in terms of new, powerful programming languages and methodologies. New software was built for microcomputers, so other manufacturers including IBM, followed DEC's example quickly, resulting in the IBM AS/400 amongst others.
The industry expanded greatly with the rise of the personal computer ("PC") in the mid-1970s, which brought desktop computing to the office worker for the first time. In the following years, it also created a growing market for games, applications, and utilities. DOS, Microsoft's first operating system product, was the dominant operating system at the time.
In the early years of the 21st century, another successful business model has arisen for hosted software, called software-as-a-service, or SaaS; this was at least the third time this model had been attempted. From the point of view of producers of some proprietary software, SaaS reduces the concerns about unauthorized copying, since it can only be accessed through the Web, and by definition no client software is loaded onto the end user's PC.



According to industry analyst Gartner, the size of the worldwide software industry in 2013 was US$407.3 billion, an increase of 4.8% over 2012. As in past years, the largest four software vendors were Microsoft, Oracle Corporation, IBM, and SAP respectively. 



The software industry has been subject to a high degree of consolidation over the past couple of decades. From 1988 to 2010, 41,136 mergers and acquisitions have been announced with a total known value of US$1,451 billion ($1.45 trillion). The highest number and value of deals was set in 2000 during the high times of the dot-com bubble with 6,757 transactions valued at $447 billion. In 2010, 1,628 deals were announced valued at $49 billion. Approaches to successfully acquire and integrate software companies are available 



Business models of software companies have been widely discussed. Network effects in software ecosystems, networks of companies, and their customers are an important element in the strategy of software companies.



Software engineering
World's largest software companies
COCOMO (Constructive Cost Model)
Function point
Software development effort estimation
Comparison of development estimation software






"Software Industry". Words to Avoid (or Use with Care) Because They Are Loaded or Confusing. Free Software Foundation. 2012-11-20. Retrieved 2013-01-31.