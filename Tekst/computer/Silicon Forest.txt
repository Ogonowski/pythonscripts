Silicon Forest is a nickname for the cluster of high-tech companies located in the Portland metropolitan area in the U.S. state of Oregon, and most frequently refers to the industrial corridor between Beaverton and Hillsboro in northwest Oregon.
The name is analogous to Silicon Valley. In the greater Portland area, these companies have traditionally specialized in hardware   specifically test-and-measurement equipment (Tektronix), computer chips (Intel and an array of smaller chip manufacturers), electronic displays (InFocus, Planar and Pixelworks) and printers (Hewlett-Packard Co., Xerox and Epson). There is a small clean technology emphasis in the area.




Silicon Forest can refer to all the technology companies in Oregon, but initially referred to Washington County on Portland s west side. First used in a Japanese company s press release dating to 1981, Lattice Semiconductor trademarked the term in 1984 but does not use the term in its marketing materials. Lattice s founder is sometimes mentioned as the person who came up with the term.
The high-tech industry in the Portland area dates back to at least the 1940s, with Tektronix and Electro Scientific Industries as pioneers. Tektronix and ESI both started out in Portland proper, but moved to Washington County in 1951 and 1962, respectively, and developed sites designed to attract other high-tech companies. These two companies, and later Intel, led to the creation of a number of spin-offs and startups, some of which were remarkably successful. A 2003 dissertation on these spin-offs led to a poster depicting the genealogy of 894 Silicon Forest companies. High-tech employment in the state reached a peak of almost 73,000 in 2001, but has never recovered from the dot-com bust. Statewide, tech employment totaled 57,000 in the spring of 2012.
Unlike other regions with a "silicon" appellation, semiconductors truly are the heart of Oregon's tech industry.
Intel's headquarters remain in Santa Clara, Calif., but in the 1990s the company began moving its most advanced technical operations to Oregon. Its Ronler Acres campus eventually became its most advanced anywhere, and Oregon is now Intel's largest operating hub. As of late 2012, Intel has close to 17,000 employees in Oregon more than anywhere else the company operates.



The following is a sample of past and present notable companies in the Silicon Forest. They may have been founded in the Silicon Forest or have a major subsidiary there. A list of Portland tech startups (technology companies founded in Portland) is provided separately.



Airbnb



BiiN (defunct)
Central Point Software (defunct)
Etec Systems, Inc. (defunct)
Floating Point Systems (defunct)
Fujitsu (factory closed)
MathStar (defunct)
Merix Corporation (acquired by Viasystems)
NEC (factory closed)
Open Source Development Labs (defunct)
Sequent Computer Systems (defunct)


