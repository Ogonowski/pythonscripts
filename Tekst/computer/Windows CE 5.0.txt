Windows CE 5.0 (codenamed "Macallan") is a successor to Windows CE 4.2, the third release in the Windows CE .NET family.
It was first released on July 9, 2004. Like its predecessors, Windows CE 5.0 is marketed towards the embedded device market and independent device vendors. Windows CE 5.0 is billed as a low-cost, compact, fast-to-market, real-time operating system available for x86, ARM, MIPS, and SuperH microprocessor-based systems. Windows CE 5.0 builds on previous Windows CE releases in its adoption of shared source. Since 2001, Microsoft has been steadily expanding the available Windows CE source tree with embedded system developers. Windows CE 5.0 is the most open Microsoft Operating System to date, though not all of the system is available under shared source agreements. Developers have the freedom to modify down to the kernel level, without the need to share their changes with Microsoft or competitors.
Windows CE 5.x is the base OS for Windows Mobile 6.0, 6.1 and 6.5. On the x86 platform, Windows CE 5.0 competes against Microsoft's other embedded Operating Systems, Windows XP Embedded and its predecessor Windows NT Embedded.
Platform Builder IDE for Windows CE 5.0 is the last builder tool available as standalone product.



According to Microsoft, Windows CE is preferable to Windows XP Embedded in situations where demanding wireless and multimedia requirements need to be met. The following are the primary considerations for  choosing the right version :
CPU architecture: Windows CE supports an extensive array of architectures, including x86, whereas Windows XP Embedded only supports the x86 architecture.
Real-time applications: Windows CE is a real-time operating system, while Windows XP Embedded is not by default.
Existing Win32 applications: Windows CE cannot use Win32 binaries, libraries, and drivers without modification.
Memory footprint: The minimum footprint of Windows CE is 350 kilobytes. The minimum footprint of Windows XP Embedded is 8 megabytes, making it over 23 times larger.
There is also a difference in cost.



Handheld PC
Palm-Size PC
Pocket PC
Personal Digital Assistant
Windows Mobile




"Which to Choose: Evaluating Microsoft Windows CE .NET and Windows XP Embedded". Microsoft Corporation. September 2003. 
"Portable CE 5.0".