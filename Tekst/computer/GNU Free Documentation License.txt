The GNU Free Documentation License (GNU FDL or simply GFDL) is a copyleft license for free documentation, designed by the Free Software Foundation (FSF) for the GNU Project. It is similar to the GNU General Public License, giving readers the rights to copy, redistribute, and modify a work and requires all copies and derivatives to be available under the same license. Copies may also be sold commercially, but, if produced in larger quantities (greater than 100), the original document or source code must be made available to the work's recipient.
The GFDL was designed for manuals, textbooks, other reference and instructional materials, and documentation which often accompanies GNU software. However, it can be used for any text-based work, regardless of subject matter. For example, the free online encyclopedia Wikipedia uses the GFDL (coupled with the Creative Commons Attribution Share-Alike License) for all of its text.



The GFDL was released in draft form for feedback in September 1999. After revisions, version 1.1 was issued in March 2000, version 1.2 in November 2002, and version 1.3 in November 2008. The current state of the license is version 1.3.
The first discussion draft of the GNU Free Documentation License version 2 was released on September 26, 2006, along with a draft of the new GNU Simpler Free Documentation License.
On December 1, 2007, Jimmy Wales announced that a long period of discussion and negotiation between and amongst the Free Software Foundation, Creative Commons, the Wikimedia Foundation and others had produced a proposal supported by both the FSF and Creative Commons to modify the Free Documentation License in such a fashion as to allow the possibility for the Wikimedia Foundation to migrate the projects to the similar Creative Commons Attribution Share-Alike (CC BY-SA) license. These changes were implemented on version 1.3 of the license, which includes a new provision allowing certain materials released under the license to be used under a Creative Commons Attribution Share-Alike license also.



Material licensed under the current version of the license can be used for any purpose, as long as the use meets certain conditions.
All previous authors of the work must be attributed.
All changes to the work must be logged.
All derivative works must be licensed under the same license.
The full text of the license, unmodified invariant sections as defined by the author if any, and any other added warranty disclaimers (such as a general disclaimer alerting readers that the document may not be accurate for example) and copyright notices from previous versions must be maintained.
Technical measures such as DRM may not be used to control or obstruct distribution or editing of the document.



The license explicitly separates any kind of "Document" from "Secondary Sections", which may not be integrated with the Document, but exist as front-matter materials or appendices. Secondary sections can contain information regarding the author's or publisher's relationship to the subject matter, but not any subject matter itself. While the Document itself is wholly editable, and is essentially covered by a license equivalent to (but mutually incompatible with) the GNU General Public License, some of the secondary sections have various restrictions designed primarily to deal with proper attribution to previous authors.
Specifically, the authors of prior versions have to be acknowledged and certain "invariant sections" specified by the original author and dealing with his or her relationship to the subject matter may not be changed. If the material is modified, its title has to be changed (unless the prior authors give permission to retain the title).
The license also has provisions for the handling of front-cover and back-cover texts of books, as well as for "History", "Acknowledgements", "Dedications" and "Endorsements" sections. These features were added in part to make the license more financially attractive to commercial publishers of software documentation, some of whom were consulted during the drafting of the GFDL. "Endorsements" sections are intended to be used in official standard documents, where distribution of modified versions should only be permitted if they are not labeled as that standard any more.



The GFDL requires the ability to "copy and distribute the Document in any medium, either commercially or noncommercially" and therefore is incompatible with material that excludes commercial re-use. As mentioned above, the GFDL was designed with commercial publishers in mind, as Stallman explained:

The GFDL is meant as a way to enlist commercial publishers in funding free documentation without surrendering any vital liberty. The 'cover text' feature, and certain other aspects of the license that deal with covers, title page, history, and endorsements, are included to make the license appealing to commercial publishers for books whose authors are paid.

Material that restricts commercial re-use is incompatible with the license and cannot be incorporated into the work. However, incorporating such restricted material may be fair use under United States copyright law (or fair dealing in some other countries) and does not need to be licensed to fall within the GFDL if such fair use is covered by all potential subsequent uses. One example of such liberal and commercial fair use is parody.



Although the two licenses work on similar copyleft principles, the GFDL is not compatible with the Creative Commons Attribution-ShareAlike license.
However, at the request of the Wikimedia Foundation, version 1.3 added a time-limited section allowing specific types of websites using the GFDL to additionally offer their work under the CC BY-SA license. These exemptions allow a GFDL-based collaborative project with multiple authors to transition to the CC BY-SA 3.0 license, without first obtaining the permission of every author, if the work satisfies several conditions:
The work must have been produced on a "Massive Multiauthor Collaboration Site" (MMC), such as a public wiki for example.
If external content originally published on a MMC is present on the site, the work must have been licensed under Version 1.3 of the GNU FDL, or an earlier version but with the "or any later version" declaration, with no cover texts or invariant sections. If it was not originally published on an MMC, it can only be relicensed if it were added to an MMC before November 1, 2008.
To prevent the clause from being used as a general compatibility measure, the license itself only allowed the change to occur before August 1, 2009. At the release of version 1.3, the FSF stated that all content added before November 1, 2008 to Wikipedia as an example satisfied the conditions. The Wikimedia Foundation itself after a public referendum, invoked this process to dual-license content released under the GFDL under the CC BY-SA license in June 2009, and adopted a foundation-wide attribution policy for the use of content from Wikimedia Foundation projects.



There have currently been no cases involving the GFDL in a court of law, although its sister license for software, the GNU General Public License, has been successfully enforced in such a setting. Although the content of Wikipedia has been plagiarized and used in violation of the GFDL by other sites, such as Baidu Baike, no contributors have ever tried to bring an organization to court due to violation of the GFDL. In the case of Baidu, Wikipedia representatives asked the site and its contributors to respect the terms of the licenses and to make proper attributions.



Some critics consider the GFDL a non-free license. Some reasons for this are that the GFDL allows "invariant" text which cannot be modified or removed, and that its prohibition against digital rights management (DRM) systems applies to valid usages, like for "private copies made and not distributed".
Notably, the Debian project and Nathanael Nerode have raised objections. In 2006, Debian developers voted to consider works licensed under the GFDL to comply with their Debian Free Software Guidelines provided the invariant section clauses are not used. The results was GFDL without invariant sections is DFSG compliant. However, their resolution stated that even without invariant sections, GFDL-licensed software documentation "is still not free of trouble", namely because of its incompatibility with the major free software licenses.
Those opposed to the GFDL have recommended the use of alternative licenses such as the BSD Documentation License or the GNU GPL.
The FLOSS Manuals foundation, an organization devoted to creating manuals for free software, decided to eschew the GFDL in favor of the GPL for its texts in 2007, citing the incompatibility between the two, difficulties in implementing the GFDL, and the fact that the GFDL "does not allow for easy duplication and modification", especially for digital documentation.



The GNU FDL contains the statement:

A criticism of this language is that it is too broad, because it applies to private copies made but not distributed. This means that a licensee is not allowed to save document copies "made" in a proprietary file format or using encryption.
In 2003, Richard Stallman said about the above sentence on the debian-legal mailing list:



A GNU FDL work can quickly be encumbered because a new, different title must be given and a list of previous titles must be kept. This could lead to the situation where there are a whole series of title pages, and dedications, in each and every copy of the book if it has a long lineage. These pages cannot be removed until the work enters the public domain after copyright expires.
Richard Stallman said about invariant sections on the debian-legal mailing list:



The GNU FDL is incompatible in both directions with the GPL material under the GNU FDL cannot be put into GPL code and GPL code cannot be put into a GNU FDL manual. At the June 22nd and 23rd 2006 international GPLv3 conference in Barcelona, Eben Moglen hinted that a future version of the GPL could be made suitable for documentation:



The GNU FDL requires that licensees, when printing a document covered by the license, must also include "this License, the copyright notices, and the license notice saying this License applies to the Document". This means that if a licensee prints out a copy of an article whose text is covered under the GNU FDL, he or she must also include a copyright notice and a physical printout of the GNU FDL, which is a significantly large document in itself. Worse, the same is required for the standalone use of just one (for example, Wikipedia) image. Wikivoyage, a web site dedicated to free content travel guides, chose not to use the GFDL because it considers it unsuitable for short printed texts.



Some of these were developed independently of the GNU FDL, while others were developed in response to perceived flaws in the GNU FDL.
Creative Commons licenses
Design Science License
Free Art license
FreeBSD Documentation License
Open Content License
Open Gaming License
Open Publication License
WTFPL




Most projects of the Wikimedia Foundation, including Wikipedia (excluding Wikivoyage and Wikinews) - On June 15, 2009, the Section 11 clauses were used to dual-license the content of these wikis under the Creative Commons Attribution Share-Alike license and GFDL.
An Anarchist FAQ
Citizendium - the project uses GFDL for articles originally from Wikipedia.
Free On-line Dictionary of Computing
Last.fm - artists descriptions are under GFDL
Marxists Internet Archive
PlanetMath
Rosetta Code
SourceWatch
The specification documents that define TRAK, an enterprise architecture framework, are released under the GFDL.
The Virginia Commonwealth University Mathematics Department has begun commercially publishing mathematics texts licensed under the GFDL, including Abstract Algebra by Thomas W. Judson.
the Baseball-Reference's BR Bullpen, a free user-contributed baseball wiki









Guide to the new drafts of documentation licenses
GFDL official text
Free Software and Free Manuals, essay by Richard Stallman
Apple's Common Documentation License, an alternative license