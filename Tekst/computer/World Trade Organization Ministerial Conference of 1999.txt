The WTO Ministerial Conference of 1999 was a meeting of the World Trade Organization, convened at the Washington State Convention and Trade Center in Seattle, Washington, USA, over the course of three days, beginning November 30, 1999. A week before the meeting, delegates admitted failure to agree on the agenda and the presence of deep disagreements with developing countries. Intended as the launch of a new round of multilateral trade negotiations that would have been called "The Millennium Round", the negotiations were marred by poor organization and controversial management of large street protests. Developing country representatives became resentful and uncooperative on being excluded from talks as the United States and the European Union attempted to cement a mutual deal on agriculture. The negotiations collapsed and were reconvened at Doha, Qatar, in November 2001. The Doha venue enabled on-site public protest to be excluded. Necessary agenda concessions were made to include the interests of developing countries, which had by then further established their own negotiation blocs, such as the Non-Aligned Movement and the Shanghai Co-operation Organisation. Thus, the current round is called the Doha Development Round, which has since 2008 remained stalled as a result of diverging perspectives regarding tariffs, agriculture, and non-tariff barriers such as agricultural subsidies.
Anti-globalization activists made headlines around the world in 1999, when they forced the Seattle WTO Ministerial Conference of 1999 to end early with direct action tactics.






The Agricultural Working Group primarily focused on improving the text of the Draft Ministerial Declaration which they intended to utilise as a starting point for further negotiations regarding agricultural trades and tariffs. The text dealt with:
The objectives of the negotiations   whether agricultural products should ultimately be treated the same as industrial products.
Provisions for developing countries (to be discussed on 2 December)
Reductions in subsidies and protection.
"Multifunctionality" (how to deal with non-trade objectives such as environmental protection, food security, etc.) and other issues.
A proposed timetable for the negotiations.



Ministers from developing countries demanded that imperialist economies such as the US and the EU stop controversial agricultural subsidies, which hindered globalisation.
Japan said abusive use of anti-dumping measures should be regarded as a disguised form of protectionism that nullifies tariff reductions overnight. It said that improvement of the AD Agreement is a lynchpin of the new Round, and that many developing countries support this.
Jamaica said that the 71 ACP countries have been marginalised regarding certain issues of the World Trade Organisation. It called for turning S&D into hard commitments, the extension of transition periods for TRIMS and Customs Valuation, and increase in funding and human resources for technical co-operation. It asked that the waiver for preferential trade treatment given to ACP countries must be extended to give time for them to be integrated into the global economy.
Iceland proposed negotiations to remove subsidies on fisheries. It was supported by a number of delegations, including the US, Peru, Indonesia, Norway, Chile and Ecuador.



Disagreements regarding Market Access centred primarily around:
Coverage and scope of the negotiations   whether they should cover all non-agricultural products or whether some could be excluded (agricultural products are negotiated under agriculture).
Overall objective of the negotiations (the current text does not say how much tariffs should be reduced)
Non-tariff measures affecting access to markets (anti-dumping measures, customs valuation, import licensing, rules of origin, safeguard measures, subsidies, etc.). Differences of opinion exist on many of these issues.
How the negotiations should be organised.
How to address developing countries  concerns   one proposal is for exports from least developed countries to be given "bound" zero tariffs in richer countries.



The two other issues considered on the 1st December were investment and competition policy. Ministers contemplated whether or not they could agree to start negotiations on investment and/or competition as part of the round of negotiations that will incorporate agriculture, services and other topics; if not, could they agree to develop elements that might eventually be incorporated in agreements on investment and competition and return to the question of whether or not to undertake negotiations at the Fourth Ministerial Session?






A new draft was proposed by the Singaporean delegation emphasising:
The Integration of Agriculture into mainstream World Trade Organisation policies
The Reduction of so-called export subsidies
Developing Country Issues
Ending the meeting, the Chairman said he was walking a tightrope. He was being pulled equally in both directions, he said. The danger was that if he moved one way or another he would fall off the rope. But he observed that the text was only for launching new negotiations. "The new round is where the real battle will begin," he said. If the round is concluded, it will boost global welfare by tens of billions of dollars, he concluded.



Questions raised in the consultations held by the Chairman focused on the methodology of tariff-cutting negotiations. A number of delegations are proposing a common approach. Unlike in the Uruguay Round where members cut tariffs on a "request-offer" basis, this would be a harmonised approach that would facilitate comparisons of tariff reduction proposals. Another position is using the combination of request-offer and harmonisation in the negotiations. Certain major traders are calling a reference in the text to an effective increase in market access. The Accelerated Tariff Liberalisation initiative for certain product sectors was also raised.



This working group was set up to help create a labour standards working group within the WTO or a body operated jointly by a number of international organisations to look at the issues. Opinions differed, with a number of developing countries opposing the creation of either type of body.



elements raised by member governments in this discussion concerned:
The de-restriction of documents
Improving the WTO's organisational structure to improve transparency and decision-making,
improving information flows and
enhancing public understanding of and participation in the workings of the organisation.



Informal meetings continued through the night of December 2 and into December 3. The main discussions were in meetings in which some 20 40 ministers took part. The people attending these meetings varied according to subject, and the chairpeople did their utmost to ensure that participants represented a cross-section of the members  positions on the relevant subjects.
Progress was reported in a number of areas, but by late afternoon it was clear that there was too little time left to complete the work of narrowing the gaps, bringing the draft declaration back to the plenary working groups, making any additional changes arising from the working groups and then approving the declaration by consensus. The conference had simply run out of time.



Actor/Director Stuart Townsend released a film about the protests surrounding the conference in 2007 entitled Battle in Seattle.
The Seattle-based hip-hop duo Blue Scholars released the song 50 Thousand Deep about their first-hand experience of the protests on their 2007 album Bayani



1999 Seattle WTO protests
1998 defeat of the OECD's MAI by protest action






Seattle Selected to Host WTO, Washington Council on International Trade, 25 January 1999.
Seattle Aftermath, Online NewsHour, 18 January 2000.
Seattle Weekly Editors Answering WTO's big questions, Seattle Weekly, 2 August 2000.



World Trade Organization 1999 Seattle Ministerial Conference Protest Collection. 1993-2000. 43.63 cubic feet. At the Labor Archives of Washington, University of Washington Libraries Special Collections.