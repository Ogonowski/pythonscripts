The Greek postal code system is administered by ELTA (   , Hellenic Post). Each city street or rural region has a unique five-digit number. The first three digits identify the city, municipality or prefecture. In major cities, the final two digits identify streets or portions of streets.
Postal codes beginning with the digits between 100 and 180 are used for the city of Athens; the beginning sequences 180 to 199 are used for other parts of the prefecture of Attica, with the exception of Corfu and Rhodes.
A complicated system relates the numbers used for the second and third digits to the numbers used in the fourth and fifth digits.
In less populated areas, the third digit is always a 0, while the final two digits identify municipalities in the prefecture. Because there are fewer municipalities in such regions, the final two digits tend not to exceed 50 or 60.
In areas with a greater population, the third digit may climb as high as 6 or 8, while the fourth and fifth digits may also be higher.
In larger cities, the third digit is never a 0, but ranges from 1 to 9. If it reaches 8 or 9, the fourth and fifth digits also reach larger numbers such as 80 and 99.
Sequences beginning in the 900s are not used.
All postal codes in Greece are numeric consisting of five digits. Until 1983 local three-digit systems existed in Athens and other cities.



^ Hellenic Post: FAQs Accessed July 13, 2008



List of postal codes in Greece