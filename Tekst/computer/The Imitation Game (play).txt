The Imitation Game is a television play written by Ian McEwan and directed by Richard Eyre, a BBC Play for Today, broadcast on 26 April 1980.



It is 1940 in Frinton and 19-year-old Cathy Raine turns down a job at the local munitions factory and, much to the consternation of her parents and boyfriend Tony, joins the ATS. She is assigned to a wireless listening station, transcribing Enigma coded morse transmissions from Nazi Germany and makes friends with Mary. After an altercation in a pub she is moved to Bletchley Park, the centre of the code-breaking operation, only to find herself cleaning and making tea. She overhears the male staff discussing the eponymous 'Imitation Game', devised by Bletchley Park's Alan Turing. Cathy is befriended by Cambridge mathematics Don Turner (based loosely on Turing) and they end up in bed together but for Turner it's a failure and he accuses her of planning it all to humiliate him. Later Cathy is caught in Turner's room reading documents relating to the Ultra programme, and she is incarcerated in military prison for the remainder of the war. 



Harriet Walter as Cathy Raine
Bernard Gallagher as Mr. Raine
Gillian Martell as Mrs. Raine
Simon Chandler as Tony
Brenda Blethyn as Mary
Nicholas Le Prevost as Turner



Writing in 1980, Ian McEwan states, 'Initially I wanted to write a play about Alan Turing, one of the founding fathers of modern computers', but his researches provided very little material, 'by this time other facts about Bletchley Park interested me more. By the end of the war ten thousand people were working in and around Bletchley. The great majority of them were women doing vital but repetitive jobs...The  need to know  rule meant that the women knew as much as was necessary to do their jobs, which was very little. As far as I could discover, there were virtually no women in at the centre of the Ultra secret. There was a widely held view at the beginning of the war that women could not keep secrets. I had come to think of Ultra as a microcosm, not only of the war but of a whole society...By having a woman at the centre of the film (I no longer thought of it as a play), I could disguise my own ignorance about Ultra as hers. The idea was to have her move from the outermost ring to the very centre, where she would be destroyed.


