In mathematics the p-adic number system for any prime number p extends the ordinary arithmetic of the rational numbers in a way different from the extension of the rational number system to the real and complex number systems. The extension is achieved by an alternative interpretation of the concept of "closeness" or absolute value. In particular, p-adic numbers have the interesting property that they are said to be close when their difference is divisible by a high power of p   the higher the power the closer they are. This property enables p-adic numbers to encode congruence information in a way that turns out to have powerful applications in number theory including, for example, in the famous proof of Fermat's Last Theorem by Andrew Wiles.
p-adic numbers were first described by Kurt Hensel in 1897, though with hindsight some of Kummer's earlier work can be interpreted as implicitly using p-adic numbers. The p-adic numbers were motivated primarily by an attempt to bring the ideas and techniques of power series methods into number theory. Their influence now extends far beyond this. For example, the field of p-adic analysis essentially provides an alternative form of calculus.
More formally, for a given prime p, the field Qp of p-adic numbers is a completion of the rational numbers. The field Qp is also given a topology derived from a metric, which is itself derived from the p-adic order, an alternative valuation on the rational numbers. This metric space is complete in the sense that every Cauchy sequence converges to a point in Qp. This is what allows the development of calculus on Qp, and it is the interaction of this analytic and algebraic structure which gives the p-adic number systems their power and utility.
The p in p-adic is a variable and may be replaced with a prime (yielding, for instance, "the 2-adic numbers") or another placeholder variable (for expressions such as "the  -adic numbers"). The "adic" of "p-adic" comes from the ending found in words such as dyadic or triadic, and the p means a prime number.



This section is an informal introduction to p-adic numbers, using examples from the ring of 10-adic (decadic) numbers. Although for p-adic numbers p should be a prime, base 10 was chosen to highlight the analogy with decimals. The decadic numbers are generally not used in mathematics: since 10 is not prime, the decadics are not a field. More formal constructions and properties are given below.
In the standard decimal representation, almost all real numbers do not have a terminating decimal representation. For example, 1/3 is represented as a non-terminating decimal as follows

Informally, non-terminating decimals are easily understood, because it is clear that a real number can be approximated to any required degree of precision by a terminating decimal. If two decimal expansions differ only after the 10th decimal place, they are quite close to one another; and if they differ only after the 20th decimal place, they are even closer.
10-adic numbers use a similar non-terminating expansion, but with a different concept of "closeness". Whereas two decimal expansions are close to one another if their difference is a large negative power of 10, two 10-adic expansions are close if their difference is a large positive power of 10. Thus 3333 and 4333, which differ by 103, are close in the 10-adic world, and 33333333 and 43333333 are even closer, differing by 107.
More precisely, a rational number r can be expressed as 10e p/q, where p and q are positive integers and q is relatively prime to p and to 10. For each r   0 there exists the maximal e such that this representation is possible. Let the 10-adic norm of r to be
      
      |0|10 = 0.
Closeness in any number system is defined by a metric. Using the 10-adic metric the distance between numbers x and y is given by |x y|10. An interesting consequence of the 10-adic metric (or of a p-adic metric) is that there is no longer a need for the negative sign. As an example, by examining the following sequence we can see how unsigned 10-adics can get progressively closer and closer to the number  1:
        so  .
       so  .
       so  .
       so  .
and taking this sequence to its limit, we can say that the 10-adic expansion of  1 is

In this notation, 10-adic expansions can be extended indefinitely to the left, in contrast to decimal expansions, which can be extended indefinitely to the right. Note that this is not the only way to write p-adic numbers   for alternatives see the Notation section below.
More formally, a 10-adic number can be defined as

where each of the ai is a digit taken from the set {0, 1, , 9} and the initial index n may be positive, negative or 0, but must be finite. From this definition, it is clear that positive integers and positive rational numbers with terminating decimal expansions will have terminating 10-adic expansions that are identical to their decimal expansions. Other numbers may have non-terminating 10-adic expansions.
It is possible to define addition, subtraction, and multiplication on 10-adic numbers in a consistent way, so that the 10-adic numbers form a commutative ring.
We can create 10-adic expansions for negative numbers as follows

and fractions which have non-terminating decimal expansions also have non-terminating 10-adic expansions. For example

Generalizing the last example, we can find a 10-adic expansion with no digits to the right of the decimal point for any rational number p q such that q is co-prime to 10; Euler's theorem guarantees that if q is co-prime to 10, then there is an n such that 10n   1 is a multiple of q. The other rational numbers can be expressed as 10-adic numbers with some digits after the decimal point.
As noted above, 10-adic numbers have a major drawback. It is possible to find pairs of non-zero 10-adic numbers (having an infinite number of digits, and thus not rational) whose product is 0. This means that 10-adic numbers do not always have multiplicative inverses i.e. valid reciprocals, which in turn implies that though 10-adic numbers form a ring they do not form a field, a deficiency that makes them much less useful as an analytical tool. Another way of saying this is that the ring of 10-adic numbers is not an integral domain because they contain zero divisors. The reason for this property turns out to be that 10 is a composite number which is not a power of a prime. This problem is simply avoided by using a prime number p as the base of the number system instead of 10 and indeed for this reason p in p-adic is usually taken to be prime.



When dealing with natural numbers, if we take p to be a fixed prime number, then any positive integer can be written as a base p expansion in the form

where the ai are integers in {0, , p 1}. For example, the binary expansion of 35 is 1 25 + 0 24 + 0 23 + 0 22 + 1 21 + 1 20, often written in the shorthand notation 1000112.
The familiar approach to extending this description to the larger domain of the rationals (and, ultimately, to the reals) is to use sums of the form:

A definite meaning is given to these sums based on Cauchy sequences, using the absolute value as metric. Thus, for example, 1/3 can be expressed in base 5 as the limit of the sequence 0.1313131313...5. In this formulation, the integers are precisely those numbers for which ai = 0 for all i < 0.
With p-adic numbers, on the other hand, we choose to extend the base p expansions in a different way. Unlike traditional integers, where the magnitude is determined by how far they are from zero, the "size" of p-adic numbers is determined by the p-adic Norm, where high positive powers of p are relatively small compared to high negative powers of p. Consider infinite sums of the form:

where k is some (not necessarily positive) integer, and each coefficient  can be called a p-adic digit. With this approach we obtain the p-adic expansions of the p-adic numbers. Those p-adic numbers for which ai = 0 for all i < 0 are also called the p-adic integers.
As opposed to real number expansions which extend to the right as sums of ever smaller, increasingly negative powers of the base p, p-adic numbers may expand to the left forever, a property that can often be true for the p-adic integers. For example, consider the p-adic expansion of 1/3 in base 5. It can be shown to be  13131325, i.e., the limit of the sequence 25, 325, 1325, 31325, 131325, 3131325, 13131325,   :

Multiplying this infinite sum by 3 in base 5 gives  00000015. As there are no negative powers of 5 in this expansion of 1/3 (i.e. no numbers to the right of the decimal point), we see that 1/3 satisfies the definition of being a p-adic integer in base 5.
More formally, the p-adic expansions can be used to define the field Qp of p-adic numbers while the p-adic integers form a subring of Qp, denoted Zp. (Not to be confused with the ring of integers modulo p which is also sometimes written Zp. To avoid ambiguity, Z/pZ or Z/(p) are often used to represent the integers modulo p.)
While it is possible to use the approach above to define p-adic numbers and explore their properties, just as in the case of real numbers other approaches are generally preferred. Hence we want to define a notion of infinite sum which makes these expressions meaningful, and this is most easily accomplished by the introduction of the p-adic metric. Two different but equivalent solutions to this problem are presented in the Constructions section below.



There are several different conventions for writing p-adic expansions. So far this article has used a notation for p-adic expansions in which powers of p increase from right to left. With this right-to-left notation the 3-adic expansion of 1 5, for example, is written as

When performing arithmetic in this notation, digits are carried to the left. It is also possible to write p-adic expansions so that the powers of p increase from left to right, and digits are carried to the right. With this left-to-right notation the 3-adic expansion of 1 5 is

p-adic expansions may be written with other sets of digits instead of {0, 1, , p 1}. For example, the 3-adic expansion of 1/5 can be written using balanced ternary digits {1,0,1} as

In fact any set of p integers which are in distinct residue classes modulo p may be used as p-adic digits. In number theory, Teichm ller representatives are sometimes used as digits.







The real numbers can be defined as equivalence classes of Cauchy sequences of rational numbers; this allows us to, for example, write 1 as 1.000  = 0.999  . The definition of a Cauchy sequence relies on the metric chosen, though, so if we choose a different one, we can construct numbers other than the real numbers. The usual metric which yields the real numbers is called the Euclidean metric.
For a given prime p, we define the p-adic absolute value in Q as follows: for any non-zero rational number x, there is a unique integer n allowing us to write x = pn(a/b), where neither of the integers a and b is divisible by p. Unless the numerator or denominator of x in lowest terms contains p as a factor, n will be 0. Now define |x|p = p n. We also define |0|p = 0.
For example with x = 63/550 = 2 1 32 5 2 7 11 1

This definition of |x|p has the effect that high powers of p become "small". By the fundamental theorem of arithmetic, for a given non-zero rational number x there is a unique finite set of distinct primes  and a corresponding sequence of non-zero integers  such that:

It then follows that  for all , and  for any other prime 
It is a theorem of Ostrowski that each absolute value on Q is equivalent either to the Euclidean absolute value, the trivial absolute value, or to one of the p-adic absolute values for some prime p. So the only norms on Q modulo equivalence are the absolute value, the trivial absolute value and the p-adic absolute value which means that there are only as many completions (with respect to a norm) of Q.
The p-adic absolute value defines a metric dp on Q by setting

The field Qp of p-adic numbers can then be defined as the completion of the metric space (Q, dp); its elements are equivalence classes of Cauchy sequences, where two sequences are called equivalent if their difference converges to zero. In this way, we obtain a complete metric space which is also a field and contains Q.
It can be shown that in Qp, every element x may be written in a unique way as

where k is some integer such that ak   0 and each ai is in {0, , p 1 }. This series converges to x with respect to the metric dp.
With this absolute value, the field Qp is a local field.



In the algebraic approach, we first define the ring of p-adic integers, and then construct the field of fractions of this ring to get the field of p-adic numbers.
We start with the inverse limit of the rings Z/pnZ (see modular arithmetic): a p-adic integer is then a sequence (an)n 1 such that an is in Z/pnZ, and if n   m, then an   am (mod pn).
Every natural number m defines such a sequence (an) by an = m mod pn and can therefore be regarded as a p-adic integer. For example, in this case 35 as a 2-adic integer would be written as the sequence (1, 3, 3, 3, 3, 35, 35, 35,  ).
The operators of the ring amount to pointwise addition and multiplication of such sequences. This is well defined because addition and multiplication commute with the "mod" operator; see modular arithmetic.
Moreover, every sequence (an) where the first element is not 0 has an inverse. In that case, for every n, an and p are coprime, and so an and pn are relatively prime. Therefore, each an has an inverse mod pn, and the sequence of these inverses, (bn), is the sought inverse of (an). For example, consider the p-adic integer corresponding to the natural number 7; as a 2-adic number, it would be written (1, 3, 7, 7, 7, 7, 7, ...). This object's inverse would be written as an ever-increasing sequence that begins (1, 3, 7, 7, 23, 55, 55, 183, 439, 439, 1463 ...). Naturally, this 2-adic integer has no corresponding natural number.
Every such sequence can alternatively be written as a series. For instance, in the 3-adics, the sequence (2, 8, 8, 35, 35, ...) can be written as 2 + 2 3 + 0 32 + 1 33 + 0 34 + ... The partial sums of this latter series are the elements of the given sequence.
The ring of p-adic integers has no zero divisors, so we can take the field of fractions to get the field Qp of p-adic numbers. Note that in this field of fractions, every non-integer p-adic number can be uniquely written as p n u with a natural number n and a unit in the p-adic integers u. This means that

Note that S 1 A, where  is a multiplicative subset (contains the unit and closed under multiplication) of a commutative ring with unit , is an algebraic construction called the ring of fractions or localization of  by .






Zp is the inverse limit of the finite rings Z/pk Z, which is uncountable in fact, has the cardinality of the continuum. Accordingly, the field Qp is uncountable. The endomorphism ring of the Pr fer p-group of rank n, denoted Z(p )n, is the ring of n   n matrices over Zp; this is sometimes referred to as the Tate module.




Define a topology on Zp by taking as a basis of open sets all sets of the form
Ua(n) = {n +  pa :     Zp}.
where a is a non-negative integer and n is an integer in [1, pa]. For example, in the dyadic integers, U1(1) is the set of odd numbers. Ua(n) is the set of all p-adic integers whose difference from n has p-adic absolute value less than p1 a. Then Zp is a compactification of Z, under the derived topology (it is not a compactification of Z with its usual discrete topology). The relative topology on Z as a subset of Zp is called the p-adic topology on Z.
The topology of Zp is that of a Cantor set. For instance, we can make a continuous 1-to-1 mapping between the dyadic integers and the Cantor set expressed in base 3 by mapping  in Z2 to  in C, where . Using a different mapping, in which the integers go to just part of the Cantor set, one can show that the topology of Qp is that of a Cantor set minus a point (such as the right-most point). In particular, Zp is compact while Qp is not; it is only locally compact. As metric spaces, both Zp and Qp are complete.



Qp contains Q and is a field of characteristic 0. This field cannot be turned into an ordered field.
R has only a single proper algebraic extension: C; in other words, this quadratic extension is already algebraically closed. By contrast, the algebraic closure of Qp, denoted Qp, has infinite degree, i.e. Qp has infinitely many inequivalent algebraic extensions. Also contrasting the case of real numbers, although there is a unique extension of the p-adic valuation to Qp, the latter is not (metrically) complete. Its (metric) completion is called Cp or  p. Here an end is reached, as Cp is algebraically closed. However unlike C this field is not locally compact.
Cp and C are isomorphic as fields, so we may regard Cp as C endowed with an exotic metric. It should be noted that the proof of existence of such a field isomorphism relies on the axiom of choice, and does not provide an explicit example of such an isomorphism.
If K is a finite Galois extension of Qp, the Galois group Gal(K/Qp) is solvable. Thus, the Galois group Gal(Qp/Qp) is prosolvable.



Qp contains the n-th cyclotomic field (n > 2) if and only if n | p   1. For instance, the n-th cyclotomic field is a subfield of Q13 if and only if n = 1, 2, 3, 4, 6, or 12. In particular, there is no multiplicative p-torsion in Qp, if p > 2. Also,  1 is the only non-trivial torsion element in Q2.
Given a natural number k, the index of the multiplicative group of the k-th powers of the non-zero elements of Qp in Q p is finite.
The number e, defined as the sum of reciprocals of factorials, is not a member of any p-adic field; but e p   Qp (p   2). For p = 2 one must take at least the fourth power. (Thus a number with similar properties as e   namely a p-th root of e p   is a member of Qp for all p.)



The only real functions whose derivative is zero are the constant functions. This is not true over Qp. For instance, the function

has zero derivative everywhere but is not even locally constant at 0.
If we let R be denoted Q , then given any elements r , r2, r3, r5, r7, ... where rp   Qp, it is possible to find a sequence (xn) in Q such that for all p (including  ), the limit of xn in Qp is rp.



Eric Hehner and Nigel Horspool proposed in 1979 the use of a p-adic representation for rational numbers on computers called Quote notation. The primary advantage of such a representation is that addition, subtraction, and multiplication can be done in a straightforward manner analogous to similar methods for binary integers; and division is even simpler, resembling multiplication. However, it has the disadvantage that representations can be much larger than simply storing the numerator and denominator in binary; for example, if 2n   1 is a Mersenne prime, its reciprocal will require 2n   1 bits to represent.



The reals and the p-adic numbers are the completions of the rationals; it is also possible to complete other fields, for instance general algebraic number fields, in an analogous way. This will be described now.
Suppose D is a Dedekind domain and E is its field of fractions. Pick a non-zero prime ideal P of D. If x is a non-zero element of E, then xD is a fractional ideal and can be uniquely factored as a product of positive and negative powers of non-zero prime ideals of D. We write ordP(x) for the exponent of P in this factorization, and for any choice of number c greater than 1 we can set

Completing with respect to this absolute value |.|P yields a field EP, the proper generalization of the field of p-adic numbers to this setting. The choice of c does not change the completion (different choices yield the same concept of Cauchy sequence, so the same completion). It is convenient, when the residue field D/P is finite, to take for c the size of D/P.
For example, when E is a number field, Ostrowski's theorem says that every non-trivial non-Archimedean absolute value on E arises as some |.|P. The remaining non-trivial absolute values on E arise from the different embeddings of E into the real or complex numbers. (In fact, the non-Archimedean absolute values can be considered as simply the different embeddings of E into the fields Cp, thus putting the description of all the non-trivial absolute values of a number field on a common footing.)
Often, one needs to simultaneously keep track of all the above-mentioned completions when E is a number field (or more generally a global field), which are seen as encoding "local" information. This is accomplished by adele rings and idele groups.



Helmut Hasse's local global principle is said to hold for an equation if it can be solved over the rational numbers if and only if it can be solved over the real numbers and over the p-adic numbers for every prime p. This principle holds e.g. for equations given by quadratic forms, but fails for higher polynomials in several indeterminates.


