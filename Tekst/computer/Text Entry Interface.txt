A text entry interface or text entry device is an interface that is used to enter text information into an electronic device. A commonly used device is a mechanical computer keyboard. Most laptop computers have an integrated mechanical keyboard, and desktop computers are usually operated primarily using a keyboard and mouse. Devices such as smartphones and tablets mean that interfaces such as virtual keyboards and voice recognition are becoming more popular as text entry systems.



With the increasing popularity of mobile electronic information management, there variety in text entry interfaces has developed considerably. Such interfaces are primarily used to communicate and record information and data.



A computer keyboard is a series of electronic switches used to input data into a computer. Each button typically represents one character, but some symbols may only be accessible via a combination of buttons. The layout of the keyboard is like that of a traditional typewriter, although there are some additional keys provided for performing further functions. There are a number of different keyboard layouts available, QWERTY being the standard English-language keyboard layout. The standard English keyboard map is referred to as QWERTY, as the first six keys on the top row of letters are Q, W, E, R, T and Y. Other keyboards layouts include AZERTY and Dvorak. The AZERTY keyboard is a variation of the standard QWERTY keyboard adapted for French-language input. The AZERTY layout modifies the QWERTY layout to add functionality for inputting accented characters. The Dvorak keyboard is designed so that the middle row of keys includes the most common letters, with the goal of allowing greater efficiency and comfort while typing. The keyboard usually contains letters, numbers, punctuation, function and control keys, arrow keys, a keypad, and may include a wristpad.



With the popularity of text messaging, text entry with mobile phones has gained use. Each key contains multiple characters, and these are reached through multiple key presses. This is frequently used in conjunction with predictive text (also known as T9) entry. Although once popular, this system has been mostly displaced with the widespread use of touchscreens on smartphones and is now mostly found on budget feature phones.




Virtual keyboards are similar to mechanical keyboards, but do not make use of physical keys. These may be implemented on systems using a screen or projected onto a surface. The individual letters may be selected by touching them as on a touch screen or surface, or by clicking on them with a classical pointing device (a mouse or trackpad), like in the case of virtual computer keyboards. Multi-touch screens even support virtual chorded keyboards.
On-screen keyboards can be used to type and enter data without using the physical keyboard. An on-screen keyboard may contain all the standard keys including all letters, numbers, symbols, and system keys like Home, End, Insert, Page Up and Page Down, Ctrl, Alt, Caps, and Shift, and can even extend the set of characters available by simulating alternative layouts. These keys can be selected using the mouse or another pointing device, or a single key or small group of keys can be used to cycle through the keys on the screen. The on-screen keyboard is the most common type of virtual keyboard. The accuracy of this keyboard depends only on hitting the right key. The main purpose of an on-screen keyboard is to provide an alternative mechanism for disabled users who cannot use a physical keyboard, or to provide text input on devices lacking a physical keyboard, such as smartphones and tablets.
Virtual keyboards also allow users to enter characters not available on their physical keyboard, enabling support for a number of languages with only one hardware keyboard.
Devices such as smartphones and tablets come with touchscreens and make use of virtual keyboards. Keyboards vary between operating systems but many third-party applications are available to replace the system keyboard. Mobile virtual keyboards are often used alongside predictive text.



Voice recognition is a system that allows using one's voice in order to send messages, make phone calls and more. The most powerful voice entry systems can recognize thousands of words. It usually requires the speaker to speak slowly, distinctly and to separate each word with a short pause. This system can replace or supplement other input devices such as keyboards and different pointing devices. The software has been developed to provide a fast method of writing without using a keyboard and can help people with various disabilities. The system works by analysing sounds and converting them to text. It knows how the language is usually spoken and decides what the speaker is most probably saying. The most powerful systems should recognise around 95% of clear speech correctly. Several voice recognition applications are available. Some of the most well-known systems are Apple Inc.'s Siri and Cortana which is developed by Microsoft. Many voice-recognition programs offer the ability to start and control programs through spoken commands.



This is commonly used on electronic systems where text entry is not important. Examples include television channel naming and text entry in video game systems such as the Sony PSP. Usually directional input devices (arrow keys, joysticks) are used to highlight a letter or number, then an enter key used to select the letter.



Handwriting recognition (or HWR) is the ability of a computer to receive and interpret intelligible handwritten input from sources such as paper documents, photographs, touch-screens and other devices. It allows users to use a touch screen device much like a notepad on which they can write without the need for a keyboard, and the software finds the closest match in its symbol database to replace the handwritten letters with. Handwriting recognition primarily uses either optical character recognition which uses an optical scanner to scan the words written by the user to determine the best suitable match, or by using a pen-based computer interface to track the movements of the tip of the pen as the user is writing.




A light pen is a computer input device used in conjunction with a computer's CRT display. It is used to select a displayed menu item. A light pen can also allow users draw on the screen with great positional accuracy. It consists of a photocell and an optical system placed in a small tube. When the tip of a light pen is moved over the monitor screen and pen button is pressed, its photocell sensing element detects the screen location and sends the corresponding signal to the CPU. The first light pen was created around 1952 as part of the Whirlwind project at MIT. Because the user was required to hold his/her arm in front of the screen for long periods of time, the light pen fell out of use as a general purpose input device.



A digital pen is an input device which captures the handwriting or brush strokes of a user, converts handwritten analog information created using "pen and paper" into digital data, enabling the data to be utilized in various applications. For example, the writing data can be digitized and uploaded to a computer and displayed on its monitor. The data can then be interpreted by handwriting software (OCR) to allow the digital pen to act as a text entry interface and be used in different applications or just as graphics.
A digital pen is generally larger and has more features than a stylus. Digital pens typically contain internal electronics and have features such as touch sensitivity, input buttons, memory, writing data transmission capabilities, and electronic erasers



A graphics tablet or digitizer is a computer input device that enables a user to hand-draw images, animations and graphics, similar to the way a person draws images with a pencil and paper. These tablets may also be used to capture data or handwritten signatures. It can also be used to trace an image from a piece of paper which is taped or otherwise secured to the surface. Capturing data in this way, by tracing or entering the corners of linear poly-lines or shapes, is called digitizing
The device consists of a flat surface upon which the user may "draw" or trace an image using an attached stylus, a pen-like drawing apparatus. The image is displayed on the computer monitor, although some graphics tablets also have a screen.
Some tablets are intended as a replacement for the mouse as the primary pointing and navigation device for desktop computers.
Graphics tablets can be use with handwriting recognition software to input text, using the graphics tablet to write on the handwriting recognition detects the letters and converts it to digital information.



One of the earliest text entry interfaces was the punched card input. A punched card input is a computer input device used to read executable computer programs, source code, and data from punched cards. Most early computers used punched cards as their main input device. Along with a card punch, punched card readers were an earlier method of entering data and running programs before the current generation of input devices existed. A card punch is an output device that punches holes in cards under computer control. Sometimes card readers were combined with card punches and, later, other devices to form multifunction machines.
Punched cards had been in use since the 1890s; their technology was mature and reliable. Card readers and punches developed for punched card machines were readily adaptable for computer use. Businesses were familiar with storing data on punched cards, and keypunch machines and their operators were widely employed. Punched cards were a better fit than other 1950s technologies, such as paper tape, for many computer applications as individual cards could easily be updated without having to reproduce entire files.






Guide to Windows keyboards
BBC Voice recognition software
Microsoft handwriting recognition



Hoste, Lode and Signer, Beat: SpeeG2: A Speech- and Gesture-based Interface for Ef cient Controller-free Text Entry, In Proceedings of the 15th International Conference on Multimodal Interaction (ICMI 2013), Sydney, Australia, December 2013.
"Typing without using the keyboard (On-screen Keyboard)". http://windows.microsoft.com/. Retrieved 2014-11-11. 
"Siri". https://www.apple.com/. Retrieved 2014-11-11. 
Broida, Rick (2014-10-25). "Stack keyboard brings handwriting recognition to iOS". http://www.cnet.com/. CNET. Retrieved 2014-11-11. 
"Dictating text using Speech Recognition". http://windows.microsoft.com. Retrieved 2014-11-11. 
"Definition of:virtual keyboard". http://www.pcmag.com. Retrieved 2014-11-11. 
"Quicktype". https://www.apple.com. Retrieved 2014-11-11. 
MacKenzie, Scott; Tanaka-Ishii, Kumiko (2007). [Text Entry Systems: Mobility, Accessibility, Universality Text Entry Systems: Mobility, Accessibility, Universality] . San Francisco: Diane Cerra. p. 314. ISBN 978-0-12-373591-1. Retrieved 2014-11-11. 
"Top smartphone voice recognition software". http://www.geeksquad.co.uk. Retrieved 2014-11-13.