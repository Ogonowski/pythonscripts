In accounting and finance, equity is the difference between the value of the assets/interest and the cost of the liabilities of something owned. For example, if someone owns a car worth $15,000 but owes $5,000 on that car, the car represents $10,000 equity. Equity can be negative if liability exceeds assets.
In an accounting context, shareholders' equity (or stockholders' equity, shareholders' funds, shareholders' capital or similar terms) represents the equity of a company as divided among individual shareholders of common or preferred stock. Accounting shareholders are the cheapest risk bearers as they deal with the public. Negative shareholders' equity is often referred to as a (positive) shareholders' deficit.
For the purposes of liquidation during bankruptcy, ownership equity is the portion of a business's equity which remains for the owners after all liabilities have been paid and all other creditors have been reimbursed.



When starting a business, the owners put funds into the business to finance various business operations. Under the model of a private limited company, the business and its owners are separate entities, so the business is considered to owe these funds to its owners as a liability in the form of share capital. Throughout the business's existence, the value (equity) of the business will be the difference between its assets (the value it provides) and its liabilities (the costs, such as the initial investments, which its owners and other creditors put into it); this is the accounting equation.
When a business must liquidate during bankruptcy, the proceeds from the assets are used to reimburse creditors. The creditors are ranked by priority, with secured creditors being paid first, other creditors being paid next, and owners being paid last. Ownership equity (also known as risk capital or liable capital) is this remaining or residual claim against assets which is paid only after all other creditors are paid. In such cases where even creditors could not get enough money to pay their bills, the ownership equity is reduced to zero because nothing is left over to reimburse it.



An equity investment generally refers to the buying and holding of shares of stock on a stock market by individuals and firms in anticipation of income from dividends and capital gains, as the value of the stock rises. Typically equity holders receive voting rights, meaning that they can vote on candidates for the board of directors (shown on a diversification of the fund(s) and to obtain the skill of the professional fund managers in charge of the fund(s). An alternative, which is usually employed by large private investors and pension funds, is to hold shares directly; in the institutional environment many clients who own portfolios have what are called segregated funds, as opposed to or in addition to the pooled mutual fund alternatives.
A calculation can be made to assess whether an equity is over or under priced, compared with a long-term government bond. This is called the yield gap or Yield Ratio. It is the ratio of the dividend yield of an equity and that of the long-term bond.



In financial accounting, owner's equity consists of the net assets of an entity. Net assets is the difference between the total assets of the entity and all its liabilities. Equity appear on the balance sheet / statement of financial position, one of the four primary financial statements.
The assets of an entity includes both tangible and intangible items, such as brand names and reputation or goodwill. The types of accounts and their description that comprise the owner's equity depend on the nature of the entity and may include the following:
Share capital (common stock)
Preferred stock
Capital surplus
Retained earnings
Treasury stock
Stock options
Reserve



The book value of equity will change in the case of the following events:
Changes in the firm's assets relative to its liabilities. For example, a profitable firm receives more cash for its products than the cost at which it produced these goods, and so in the act of making a profit, it is increasing its retained earnings, therefore its shareholders' equity.
Depreciation - Equity will decrease, for example, when machinery depreciates, which is registered as a decline in the value of the asset, and on the liabilities side of the firm's balance sheet as a decrease in shareholders' equity.
Issue of new equity in which the firm obtains new capital increases the total shareholders' equity.
Share repurchases, in which a firm gives back money to its investors, reducing on the asset side its financial assets, and on the liability side the shareholders' equity. For practical purposes (except for its tax consequences), share repurchasing is similar to a dividend payment, as both consist of the firm giving money back to investors. Rather than giving money to all shareholders immediately in the form of a dividend payment, a share repurchase reduces the number of shares (increases the size of each share) in future income and distributions.
Dividends paid out to preferred stock owners are considered an expense to be subtracted from net income(from the point of view of the common share owners).
Other reasons - Assets and liabilities can change without any effect being measured in the Income Statement under certain circumstances; for example, changes in accounting rules may be applied retroactively. Sometimes assets bought and held in other countries get translated back into the reporting currency at different exchange rates, resulting in a changed value.



When the owners are shareholders, the interest can be called shareholders' equity; the accounting remains the same, and it is ownership equity spread out among shareholders. If all shareholders are in one and the same class, they share equally in ownership equity from all perspectives. However, shareholders may allow different priority ranking among themselves by the use of share classes and options. This complicates both analysis for stock valuation and accounting.
The individual investor is interested not only in the total changes to equity, but also in the increase or decrease in the value of their own personal share of the equity. This reconciliation of equity should be done both in total and on a per share basis.
Shareholders' equity is obtained by subtracting total liabilities from the total assets of the shareholders. These assets and liabilities can be
Equity (beginning of year)
+ net income inter net money one gained
  dividends how much money one gained or lost so far
+/  gain/loss from changes to the number of shares outstanding.
= Equity (end of year) if one gets more money during the year or less or not anything



In the stock market, market price per share does not correspond to the equity per share calculated in the accounting statements. Stock valuations, which are often much higher, are based on other considerations related to the business' operating cash flow, profits and future prospects; some factors are derived from the accounting statement.



The notion of equity with respect to real estate comes the equity of redemption. This equity is a property right valued at the difference between the market value of the property and the amount of any mortgage or other encumbrance.




Private equity


