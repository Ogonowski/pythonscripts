Horn Island Chemical Warfare Service Quarantine Station, also known as the Horn Island Testing Station, was a U.S. biological weapons testing site during World War II. It was located on Mississippi's Horn Island and opened in 1943. When the war ended, the facility was closed.



Horn Island Chemical Warfare Service Quarantine Station was acquired in March 1943 by the U.S. Army for use as a biological weapons testing site. The site was located on Horn Island, about 10 miles (16 km) south of Pascagoula, Mississippi, and opened on October 29, 1943. The 2,000-acre (8.1 km2) site on Horn Island was managed and built by the Chemical Warfare Service's (CWS) Special Projects Division (SPD). By May 1944 the U.S. bio-weapons program employed 1,500 people between its Horn Island facility and the facilities at Camp Detrick. The work at Horn Island, like all of the work done at SPD facilities during World War II was highly classified and precautions were taken to ensure the work remained secret; during the Army's occupation of Horn Island the public was barred from the island.
Soon after construction at the facility was complete it was found that the area was unsuitable for large-scale testing of biological agents. At the time, shipping traffic on the Mississippi River, near the island, was rising. It was determined that bio-weapons trials in close proximity to human population was undesirable and testing on the island was limited. Shortly before the end of World War II, on August 11, 1945, an order from the CWS declared that the Special Projects Division was to cease its activities. The facility at Horn Island was closed in 1946.



Horn Island was acquired for the sole purpose of becoming a biological weapons test site for the U.S. military. The site was established as one of several designed to assist the newly formed U.S. biological weapons program at Camp Detrick. Horn Island Testing Station was initially established to focus its studies on insects as biological weapons. When conceived and constructed the testing station at Horn Island was meant to be the primary bio-weapons field testing site for the United States.
The U.S. Army built facilities on the island for these purposes which included several buildings, roads, and a narrow gauge railroad. The Army also constructed an incinerator with a tall brick chimney on the island. After the Army abandoned the site, a hurricane destroyed most of the structures, including the incinerator and chimney. The foundations of some of the military buildings are still visible on the island, which is now part of the Gulf Islands National Seashore. Additionally, the remnants of the incinerator chimney were still visible into at least the 1980s.



Because of its proximity to human populations only two lethal agents, both toxins, were ever tested on the island, botulin and ricin. The U.S. Navy used the site during the war to study mosquitoes and flies that were native to the Pacific Islands. In addition, an anthrax simulant, Bacillus globigii was used in aerosol dispersion tests at the station.
Testing at Horn Island with the toxin botulin showed that the agent was not a viable aerosol biological weapon. Tests were undertaken using four pound bombs filled with botulin. These bombs were detonated over confined guinea pigs, just one of the animals died from inhaled botulin and another died after licking the toxin from its fur.



Fort Terry
Granite Peak Installation
Gruinard Island
Plum Island


