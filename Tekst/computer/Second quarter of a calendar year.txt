Generally speaking, a calendar year begins on the New Year's Day of the given calendar system and ends on the day before the following New Year's Day, and thus consists of a whole number of days. To reconcile the calendar year with the astronomical cycle (which has a fractional number of days) certain years contain extra days.
The Gregorian year, which is in use in most of the world, begins on January 1 and ends on December 31. It has a length of 365 days in an ordinary year, with 8,760 hours, 525,600 minutes, and 31,536,000 seconds; but 366 days in a leap year, with 8,784 hours, 527,040 minutes, and 31,622,400 seconds. With 97 leap years every 400 years, the year has an average length of 365.2425 days. Other formula-based calendars can have lengths which are further out of step with the solar cycle: for example, the Julian calendar has an average length of 365.25 days, and the Hebrew calendar has an average length of 365.2468 days.
The astronomer's mean tropical year which is averaged over equinoxes and solstices is currently 365.24219 days, slightly shorter than the average length of the year in most calendars, but the astronomer's value changes over time, so William Herschel's suggested correction to the Gregorian calendar may become unnecessary by the year 4000.



The calendar year can be divided into 4 quarters, often abbreviated Q1, Q2, Q3 and Q4. These are typically of slightly differing lengths and aligned to month boundaries for convenience:
First quarter / Q1: from the beginning of January to the end of March (01/01 - 31/03), 90 days or 91 days on leap years
Second quarter / Q2: from the beginning of April to the end of June (01/04 - 30/06), 91 days
Third quarter / Q3: from the beginning of July to the end of September (01/07 - 30/09), 92 days
Fourth quarter / Q4: from the beginning of October to the end of December (01/10 - 31/12), 92 days






Academic term
Calendar reform
Common year
Fiscal year
ISO 8601
ISO week date
Leap year
Rolling 12-month period
Tropical year
Seasonal year