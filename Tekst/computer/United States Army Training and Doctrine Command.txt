Established 1 July 1973, the United States Army Training and Doctrine Command (TRADOC) is a command of the United States Army headquartered at Fort Eustis, Virginia. It is charged with overseeing training of Army forces and the development of operational doctrine. TRADOC operates 37 schools and centers at 27 different locations. TRADOC schools conduct 1,304 courses and 108 language courses. The 1,304 courses include 516,000 seats (resident, on-site and distributed learning) for 443,231 soldiers; 36,145 other-service personnel; 8,314 international soldiers; and 28,310 civilians.



The official mission statement for TRADOC states:

Training and Doctrine Command develops, educates and trains Soldiers, civilians, and leaders; supports unit training; and designs, builds and integrates a versatile mix of capabilities, formations, and equipment to strengthen the U.S. Army as America's Force of Decisive Action.



TRADOC was established as a major U.S. Army command on 1 July 1973. The new command, along with the U.S. Army Forces Command (FORSCOM), was created from the Continental Army Command (CONARC) located at Fort Monroe, VA. That action was the major innovation in the Army's post-Vietnam reorganization, in the face of realization that CONARC's obligations and span of control were too broad for efficient focus. The new organization functionally realigned the major Army commands in the continental United States. CONARC, and Headquarters, U.S. Army Combat Developments Command (CDC), situated at Fort Belvoir, VA, were discontinued, with TRADOC and FORSCOM at Fort Belvoir assuming the realigned missions. TRADOC assumed the combat developments mission from CDC, took over the individual training mission formerly the responsibility of CONARC, and assumed command from CONARC of the major Army installations in the United States housing Army training center and Army branch schools. FORSCOM assumed CONARC's operational responsibility for the command and readiness of all divisions and corps in the continental U.S. and for the installations where they were based.
Joined under TRADOC, the major Army missions of individual training and combat developments each had its own lineage. The individual training responsibility had belonged, during World War II, to Headquarters Army Ground Forces (AGF). In 1946 numbered army areas were established in the U.S. under AGF command. At that time, the AGF moved from Washington, D.C. to Fort Monroe, VA. In March 1948, the AGF was replaced at Fort Monroe with the new Office, Chief of Army Field Forces (OCAFF). OCAFF, however, did not command the training establishment. That function was exercised by Headquarters, Department of the Army through the numbered armies to the corps, division, and Army Training Centers. In February 1955, HQ Continental Army Command (CONARC) replaced OCAFF, assuming its missions as well as the training missions from DA. In January, HQ CONARC was redesignated U.S. Continental Army Command. Combat developments emerged as a formal Army mission in the early 1950s, and OCAFF assumed that role in 1952. In 1955, CONARC assumed the mission. In 1962, HQ U.S. Army Combat Development Command (CDC) was established to bring the combat developments function under one major Army command.



Army Capabilities Integration Center
Brigade Modernization Command

U.S. Army Cadet Command
Combined Arms Center
Mission Command Center of Excellence
Intelligence Center of Excellence
Cyber Center of Excellence
Aviation Center of Excellence

Fires Center of Excellence
Artillery School
Air Defense Artillery School

Initial Military Training
Maneuver Center of Excellence
Armor
Infantry

Maneuver Support Center of Excellence
Engineer School
Chemical School
Military Police School

U.S. Army Recruiting Command
Sustainment Center of Excellence
Ordnance School
Transportation School
Quartermaster School




The current Commanding General is GEN David G. Perkins [1]. The Command Sergeant Major is currently CSM David S. Davenport [2].



TRADOC COMMAND OVERVIEW VIDEO on YouTube




Command Overview Brief
Fact Sheet
Organization Chart
TRADOC Website
TRADOC history
Joint Base Langley - Eustis