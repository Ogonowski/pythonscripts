DC/OSx (DataCenter/OSx) is a discontinued Unix operating system for MIPS based systems developed by Pyramid Technology. It ran on its Nile series of SMP machines and was a port of AT&T System V Release 4 (SVR4). In 1995, Pyramid Technology was acquired by Siemens Nixdorf Informationssysteme (SNI), and DC/OSx was superseded by the SINIX operating system.



DC/OSx was the first symmetric multiprocessing (SMP) implementation on Unix System V Release 4.
DC/OSx was later superseded by SINIX, a version of the Unix operating system from SNI. Features of DC/OSx were incorporated into SINIX; later versions were branded as Reliant Unix.



BS2000
Timeline of operating systems






List of Unix Variants