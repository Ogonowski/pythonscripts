This is a timeline of events that occurred during World War II in 1943.



1: German 1st Panzer Division withdraws from the Terek River area in southern Russia to prevent encirclement.
2: Americans and Australians recapture Buna, New Guinea.
7: Japanese land more troops at Lae, New Guinea.
9: United States Western Task Force redesignated I Armored Corps.
10: Soviet troops launch an all-out offensive attack on Stalingrad; they also renew attacks in the north (Leningrad) and in the Caucasus.
14: The Casablanca Conference of Allied leaders begins. Winston Churchill and Franklin D. Roosevelt discuss the eventual invasion of mainland Europe, the impending invasion of Sicily and Italy, and the wisdom of the principle of "unconditional surrender".
15: The British start an offensive aimed at taking far-off Tripoli, Libya.
16: Iraq declares war on the Axis powers.
: The Royal Air Force begins a two-night bombing of Berlin.
18: The Jews in the Warsaw Ghetto rise up for the first time, starting the Warsaw Ghetto Uprising.
: Besieged defenders of Leningrad link up with relieving forces.
19: General Georgy Zhukov is promoted to Marshal as the Stalingrad struggle grinds to a close.
20: USS Silversides attacked a Japanese convoy 286 miles from Truk, Caroline Islands en route to the Solomon Islands, sinking transport Meiu Maru and damaging Surabaya Maru.
21: Last airfield at Stalingrad is taken by Red forces, ensuring that the Luftwaffe will be unable to supply German troops any further; Hitler still demands that Friedrich Paulus continue the fight.
: Red Army armies have more victories in the Caucasus.

22: Allies liberate Sanananda, New Guinea.
23: British capture Tripoli, Libya.
: Japanese continue their fight in western Guadalcanal; they now seem to have given up completely on the New Guinea campaign.
24: The Casablanca Conference ends; Allies insist on unconditional surrender from Germany.
: German forces in Stalingrad are in the last phases of collapse.
25: United States XIV Corps arrives in Pacific Theater.
26: Soviet troops retake Voronezh.
27: 50 bombers mount the first all American air raid against Germany. Wilhelmshaven, the large naval base, is the primary target.
28: A new conscription law in Germany: men between 16 and 65 and women between 17 and 50 are open to mobilization.
: George Zhukov awarded the first Order of Suvorov 1st Class.
29: The naval battle of Rennell Island, near Guadalcanal, begins. The Japanese beat the Americans and the USS Chicago is lost.
: Another two-day bombing of Berlin by the RAF.
30: The last Japanese have cleared out of Guadalcanal by a brilliant evacuation plan undetected by the Americans.
31: Friedrich Paulus (Generalfeldmarschall in command of the German 6th Army) and his staff surrender to Soviet troops in Stalingrad.




2: In the Soviet Union, the Battle of Stalingrad comes to an end with the official surrender of the German 6th Army. The German public is informed of this disaster, marking the first time the Nazi government has acknowledged a failure in the war effort.
: Rommel retreats farther into Tunisia, establishing his troops at the Mareth Line. Within two days, Allied troops move into Tunisia for the first time.
5: The Allies now have all of Libya under control.
: Essen is bombed, marking the beginning of a four-month attack on the Ruhr industrial area.
7: In the United States, it is announced that shoe rationing will go into effect in two days.
8: The Chindits (a "long range penetration group") under British General Orde Wingate begin an incursion into Burma.
: Nuremberg is heavily bombed.
: United States' VI Corps arrives in North Africa.
9: Guadalcanal is finally secured; it is the first major achievement of the American offensive in the Pacific war.
: Munich and Vienna are heavily bombed, along with Berlin.
11: U.S. General Dwight D. Eisenhower is selected to command the Allied armies in Europe.
13: Rommel launches a counter-attack against the Americans in western Tunisia; he takes Sidi Bouzid and Gafsa. The Battle of the Kasserine Pass begins: inexperienced American troops are soon forced to retreat.
14: Rostov-on-Don is liberated by the Red Army;
16: Soviet Union reconquers Kharkov, but is later driven out in the Third Battle of Kharkov.
: Prime Minister of Vichy France Pierre Laval and Minister of Justice Joseph Barth lemy formally create the Service du travail obligatoire (STO)
18: In a speech at the Berlin Sportpalast German propaganda minister Joseph Goebbels declares a "Total War" against the Allies; The Nazis arrest the members of the White Rose movement, an anti-Nazi youth group.
: Chindits under Wingate cut the railway line between Mandalay and Myitkyina.
21: Americans take the Russell Islands, part of the Solomons chain.
22: Hans and Sophie Scholl of the White Rose movement are executed.
25: Japanese POWs refuse to work at Featherston prisoner of war camp, this escalates into a deadly clash between the inmates and the guards
26: Rommel retreats northward from the Mareth Line in Tunisia.
28: The SS United Victory, the first Victory ship, is launched; this class of transport will prove to be crucial in hauling men and supplies across the oceans.
: Operation Gunnerside: six Norwegians led by Joachim R nneberg successfully attack the heavy water plant Vemork.




1: Battle of the Bismarck Sea. U.S. and Australian naval forces, over the course of three days, sink eight Japanese troop transports near New Guinea.
: Heinz Guderian becomes the Inspector-General of the Armoured Troops for the German Army.
2: Wingate's Chindits continue their localised strikes in Burma.
5: German advances around Kharkov threaten earlier Red Army gains.
: Continued RAF bombing of the Ruhr valley, particularly Essen.
6: Battle of Medenine, Tunisia. It is Rommel's last battle in Africa as he is forced to retreat.
8: Continuing German counter-attacks around Kharkov.
9: Members of the Calcutta Light Horse carry out a covert attack against a German merchantship, which had been transmitting Allied positions to U-boats from the Mormugao Harbour in Portugal's neutral territory of Goa.
10: The USAAF 14th Air Force is formed in China, under General Claire Lee Chennault, former head of the "Flying Tigers."
: The US House of Representatives votes to extend the Lend-Lease plan.
11: The Germans enter Kharkov and the fierce struggle with the Red Army continues.

12 Greece Karditsa it's the first city in Europe to be liberated from Nazi occupation, the campaign fought by the Greek People's Liberation Army ELAS.
13: German forces liquidate the Jewish ghetto in Krak w.
14: Germans recapture Kharkov.
16: The first reports of the Katyn massacre in Poland seep to the West; reports say that more than 22,000 prisoners of war were killed by the NKVD, who eventually blame the massacre on the Germans.
: Stalin for the ninth time demands a "Second Front," accusing his allies of treachery.
17: Devastating convoy losses in the Atlantic due to increased U-boat activity; the middle of the Atlantic is apparently not sufficiently covered by planes or ships.
18: General George S. Patton leads his tanks of II Corps into Gafsa, Tunisia.
20: Montgomery's forces begin a breakthrough in Tunisia, striking at the Mareth line.
23: American tanks defeat the Germans at El Guettar, Tunisia.
24-25: Seventy-six Allied PoWs escape from Stalag Luft III in Sagan. This becomes known as the "Great Escape". Seventy-three were later recaptured; of these 50 were executed, 23 were sent back to prison camps and three escaped to freedom.
26: The British break through the Mareth line in southern Tunisia, threatening the whole German army. The Germans move north.
: Battle of the Komandorski Islands. In the Aleutian Islands United States Navy forces intercept Japanese attempting to reinforce a garrison at Kiska. Poor leadership on both sides leads to a stalemate of sorts, and the Japanese withdraw without achieving their goal.



1: Allies continue to squeeze the Germans into the corner of Tunisia.
3: Racial tensions between American marines and New Zealand troops of M ori origin result in the Battle of Manners Street, a small-scale riot in which no lives were lost
4: The only large-scale escape of Allied prisoners-of-war from the Japanese in the Pacific takes place when ten American POWs and two Filipino convicts break out of the Davao Penal Colony on the island of Mindanao in the southern Philippines. The escaped POWs were the first to break the news of the infamous Bataan Death March and other atrocities committed by the Japanese to the world.
7: Hitler and Mussolini come together at Salzburg, mostly for the purpose of propping up Mussolini's fading morale.
: Allied forces the Americans from the West, the British from the East link up near Gafsa in Tunisia.
: Bolivia declares war on Germany, Japan, and Italy.
10: The British 8th Army enters Sfax, Tunisia.
13: Radio Berlin announces the discovery by Wehrmacht of mass graves of Poles purportedly killed by Soviets in the Katyn massacre.
15: Finland officially rejects Soviet terms for peace.
: Heavy RAF raid on Stuttgart.
18: Admiral Isoroku Yamamoto, chief architect of Japanese naval strategy, is killed when his plane is shot down by American P38's over Bougainville. He was on an inspection tour.
: The "Palm Sunday massacre": large numbers of German troop-transport aircraft are shot down before reaching Tunisia, where they were to pick up the isolated German troops.
19-30: The Bermuda Conference takes place in Hamilton, Bermuda. U.K. and U.S. leaders discuss the plight of the European Jews.
19: The Warsaw Ghetto uprising: On the Eve of Passover, Jews resist German attempts to deport the Jewish community.
19: In occupied Belgium, partisans attack the a railway convoy transporting Belgian Jews to Auschwitz. It is the largest attack on a Holocaust train of the war and 236 Jews escape.
26: The British finally take "Longstop Hill" in Tunisia, a key position on the breakout road to Tunis.
28: Allies attempt to close the mid-Atlantic gap in the war against the U-boats with long-range bombers.
30: Operation Mincemeat: Lt. Jewell's crew releases Martin's body near the Spanish coast. Later, the body washes up on the Spanish coast and is discovered by a local fisherman.




1: Allies close in on the cornered Germans in the Tunis area.
2: Japanese aircraft again bomb Darwin, Australia.
7: Tunis captured by British First Army. Meanwhile the Americans take Bizerte.
9: The Japanese begin a three-day massacre of civilians; about 30,000 Chinese are killed in the Changjiao massacre.
11: American troops invade Attu Island in the Aleutian Islands in an attempt to expel occupying Japanese forces.
12: The Trident Conference begins in Washington, D.C. with Franklin D. Roosevelt and Winston Churchill taking part. The discussions are mostly on future strategy.
13: Remaining German Afrika Korps and Italian troops in North Africa surrender to Allied forces. The Allies take over 250,000 prisoners.
15: The French form a "Resistance Movement."
16: The Warsaw Ghetto Uprising ends. The ghetto has been destroyed, with about 14,000 Jews killed and about another 40,000 sent to the death camp at Treblinka.
: The Dambuster Raids are carried out by RAF 617 Squadron on two German dams, Mohne and Eder. The Ruhr war industries lose electrical power.
17: The Germans launch their fifth major offensive against Tito's partisans in Yugoslavia.
19: Winston Churchill addresses a joint session of the U.S. Congress. He praises the partnership of the two Allies.
22: Allies bomb Sicily and Sardinia, both possible landing sites.
24: Admiral Karl D nitz orders the majority of U-boats to withdraw from the Atlantic because of heavy losses to new Allied anti-sub tactics. By the end of the month, 43 U-boats are lost, compared to 34 Allied ships sunk. This is referred to as "Black May".
: Josef Mengele becomes Chief Medical Officer in Auschwitz.
29: RAF bombs Wuppertal, causing heavy civilian losses.
30: Attu Island is again under American control.
31: American B-17's bomb Naples.



4: General Henri Giraud becomes Commander-in-Chief of the Free French forces.
8: Japanese forces begin to evacuate Kiska Island in the Aleutians, their last foothold in the Western hemisphere. The event is almost to the year of their landing.
11: British 1st Division takes the Italian island of Pantelleria, between Tunisia and Sicily, capturing 11,000 Italian troops.
12: The Italian island of Lampedusa, between Tunisia and Sicily, surrenders to the Allies.
13: Heavy US aircraft losses over Kiel.
17: Allies bomb Sicily and the Italian mainland, as signs increase of a forthcoming invasion.
21: Operation Cartwheel opens with landings by the United States 4th Marine Raider Battalion at Segi Point on New Georgia in the Solomon Islands, beginning the New Georgia Campaign. It will not be secured until August.
23: American troops land in the Trobriand Islands, close to New Guinea. The American strategy of driving up the Southwest Pacific by "Island Hopping" continues.
24: Continuing attacks against the Ruhr industrial valley. One result is the evacuation of large numbers of German civilians from the area.
30: American troops land on Rendova Island, New Georgia, another part of Operation Cartwheel.




4: Exiled Polish leader General W adys aw Sikorski dies in an air crash in Gibraltar.
5: Operation Citadel (the Battle of Kursk) begins.
: Conclusion of the National Bands Agreement in occupied Greece, which is to coordinate the actions of the Resistance movement in Greece.
6: U.S. and Japanese ships fight the Battle of Kula Gulf in the Solomons.
7: Walter Dornberger briefs the V-2 rocket to Hitler, who approves the project for top priority.
10: Operation Husky (the Allied invasion of Sicily) begins.
11: Ukrainian Insurgent Army massacres Poles at Dominopol.
12:/:13: The Japanese win a tactical victory at the Battle of Kolombangara.
12: The Battle of Prokhorovka begins; the largest tank battle in human history and part of the Battle of Kursk, it is the pivotal battle of Operation Citadel.
13: Hitler calls off the Kursk offensive, but the Soviets continue the battle.
19: The Allies bomb Rome for the first time.
21: The Operation Bellicose targeting of Friedrichshafen W rzburg radars is the first bombing of a V-2 rocket facility.
22: U.S. forces under Patton capture Palermo, Sicily.
24: Hamburg, Germany, is heavily bombed in Operation Gomorrah, which at the time is the heaviest assault in the history of aviation.
25: Mussolini is arrested and relieved of his offices after a meeting with Italian King Victor Emmanuel III, who chooses Marshal Pietro Badoglio to form a new government.



1: Operation Tidal Wave: Oil refineries at Ploie ti, Romania, are bombed by U.S. IX Bomber Command.
: Japan declares independence for the State of Burma under Ba Maw.
2: 2,897 Romani are gassed when their camp at Auschwitz is liquidated.
: John F. Kennedy's PT-109 is rammed in two and sunk off the Solomon Islands.
3: The first of two "George S. Patton slapping incidents" occurs in Sicily.
5: Swedish government announces it will no longer allow German troops and war material to transit Swedish railways.
: Russians recapture Orel and Belgorod.
6/7: The U.S. wins the Battle of Vella Gulf off Kolombangara in the Solomons.
6: German troops start pouring in to take over Italy's defences.
11: German and Italian forces begin to evacuate Sicily.
15: The Land Battle of Vella Lavella island in the Solomons begins.
: US and Canadian troops invade Kiska Island in the Aleutians, not knowing the Japanese have already evacuated.
16: Polish Jews begin a resistance with scant weaponry in Bia ystok. The leaders commit suicide when they run out of ammo.
: U.S. troops enter Messina, Sicily.
17: All of Sicily now controlled by the Allies.
: Heavy loss of Allied bombers in the Schweinfurt Regensburg mission.
: Operation Crossbow begins with Operation Hydra when the RAF bombs the Peenem nde V-2 rocket facility.
17/18: Portugal, referencing the Anglo-Portuguese Treaty of 1373, allows the Allies to use the Azores Islands for air and naval bases.
19: Roosevelt and Churchill signed the Quebec Agreement during the Quebec Conference.
23: Operation Polkovodets Rumyantsev liberates Kharkov, Ukraine. The Battle of Kursk has become the first successful major Soviet summer offensive of the war.
29: During the Occupation of Denmark by Nazi Germany, martial law replaced the Danish government.

31: The Northwest African Air Forces conducts an air raid against the Italian city of Pisa.



1: 22,750,000 British men and women are either in the services or Civil Defence or doing essential war work, according to the U.K. Ministry of labour.
3: A secret Italian Armistice is signed and Italy drops out of the war. Mainland Italy is invaded when the British XXIII Corps lands at Reggio Calabria.
: Nazi Germany begins the evacuation of civilians from Berlin.
4: Soviet Union declares war on Bulgaria.
: The 503rd Parachute Regiment under American General Douglas MacArthur lands and occupies Nadzab, just west of the port city of Lae in northeastern New Guinea. Lae falls into Australian hands and Australian troops take Salamaua.
8: Eisenhower publicly announces the surrender of Italy to the Allies. The Germans enact Operation Achse, the disarmament of Italian armed forces.
:The USAAF bombs the German General Headquarters for the Mediterranean zone at Frascati.
9: The Allies land at Salerno, Italy; meanwhile the British troops take Taranto in the heel of the Italian "boot". Allied strategy aims at a "drive" up the "boot".
: Iran declares war on Germany.
10: German troops occupy Rome. The Italian fleet meanwhile surrenders at Malta and other Mediterranean ports.
11: British troops enter Bari in southeastern Italy.
12: Mussolini is rescued from a mountaintop captivity by German SS troops led by Otto Skorzeny. Mussolini is then set up by Hitler, who remains loyal to his old friend, as the head of the puppet "Italian Social Republic."
13: The Salerno beachhead is in jeopardy, as German counterattacks increase.
14: German troops commence the Holocaust of Viannos in Crete that will continue for two more days.
15: Chiang Kai-shek asks that General Stilwell, American military advisor/commander, be recalled for suggesting an alliance with the Communists.
16: British forces land on various Italian-held Greek islands in the Aegean Sea, beginning the Dodecanese Campaign.
: British and American troops link up near the Salerno beachhead.
19: German troops evacuate Sardinia.
21: The battle of the Solomons can now be considered at an unofficial end.
: The Massacre of the Acqui Division begins: After resisting for a week, the Italian Acqui division on the Greek island of Cephallonia surrenders to the Germans. During the next days, over 4,500 Italians are executed, and further 3,000 lost during transport at sea.
22: Australian forces land at Finschhafen, a small port in New Guinea. The Japanese continue the battle well into October.
: British midget submarines attack the German battleship Tirpitz, at anchor in a Norwegian fjord, crippling her for six months.
25: The Red Army retakes Smolensk.
26: Germans assault the island of Leros, beginning the Battle of Leros.
27: The Germans take over the island of Corfu from the Italians, the previous occupiers.
: Sheng Shicai has Mao Zedong's brother Mao Zemin and Chen Tanqiu, a founder of the Communist Party of China, executed.
28: The people of Naples, sensing the approach of the Allies, rise up against the German occupiers.
30: With the Gestapo starting to round up Danish Jews, certain Danes are secretly sending their Jewish countrymen to Sweden by means of dangerous boat crossings.



Unknown: Ruzagayura famine starts (until December 1944) in the Belgian African colony of Ruanda-Urundi
1: Neapolitans complete their uprising and free Naples from German military occupation.
3: Churchill appoints Lord Louis Mountbatten the commander of South East Asia Command.
3: The Germans conquer the island of Kos.
4: Corsica is liberated by Free French forces.
5: The Allies cross Italy's Volturno Line.
6: The Naval Battle of Vella Lavella completes the second phase of Operation Cartwheel.
7: 98 American civilian prisoners were executed on Wake Island.
9: United States VII Corps arrives in European Theater.
10: Chiang Kai-shek takes the oath of office as chairman of Nationalist Government (China).
12: Operation Cartwheel begins a bombing campaign against Rabaul.
13: Italy declares war on Germany.
14: 229 of 292 B-17s reached the target in the Second Raid on Schweinfurt. Losses are so heavy that the long range daylight bombing campaign is suspended until the bombers can be escorted by P-51 fighters.
Members of the Sobibor extermination camp underground, led by Polish-Jewish prisoner Leon Feldhendler and Soviet-Jewish POW Alexander Pechersky, succeeded in covertly killing eleven German SS officers and a number of camp guards. Although their plan was to kill all the SS and walk out of the main gate of the camp, the killings were discovered and the inmates ran for their lives under fire. About 300 out of the 600 prisoners in the camp escaped into the forests.
18: The Third Moscow Conference convened.
19: The German War Office contracts the Mittelwerk to produce 12,000 V-2 rockets.
22/23: An air raid on Kassel causes a seven day firestorm.
25: The Red Army takes Dnipropetrovsk.
28: Cruiser HMS Charybdis sunk, and destroyer HMS Limbourne damaged, by German torpedo boats off the North coast of Brittany with large loss of life. Bodies of 21 sailors and marines washed up on the Island of Guernsey. Buried with full military honours by the German Occupation authorities, allowing around 5,000 Islanders to attend and lay some 900 wreaths.
29: Troops replace striking London dockworkers.
31: Heavy rains in Italy slow the Allied advance south of Rome.



1: In Operation Goodtime, United States Marines land on Bougainville in the Solomon Islands. The fighting on this island will continue to the end of the war.
2: In the early morning hours, American and Japanese ships fight the inconclusive Battle of Empress Augusta Bay off Bougainville, but the Japanese are unable to land reinforcements.
2: British troops, in Italy, reach the Garigliano River.
5: The Italians bomb the Vatican in a failed attempt to knock out the Vatican radio.
6: The Red Army liberates the city of Kiev. This is an anniversary of the Russian Revolution in 1917.
9: Allies take Castiglione, Italy.
9: General De Gaulle becomes President of the French Committee of National Liberation.
9: Members of the Belgian Resistance publish a fake issue of the collaborationist newspaper Le Soir, mocking the German strategic situation.
11: American air power continues to hit Rabaul.
12: Germans overrun British forces on the Dodecanese islands, off Turkey.
14: Heavy bombers hit Tarawa, in the Gilbert Islands in the Pacific.
15: Allied Expeditionary Force for the invasion of Europe is officially formed.
15: German SS leader Heinrich Himmler orders that Gypsies and "part-Gypsies" are to be put "on the same level as Jews and placed in concentration camps."
16: Anti-German resistance in Italy increases; there are explosions in Milan.
16: The Battle of Leros ends with the surrender of the British and Italian forces to the Germans.
16: 160 American bombers strike a hydro-electric power facility and heavy water factory in German-controlled Vemork, Norway

16: Japanese submarine sinks surfaced submarine USS Corvina near Truk
18: 440 Royal Air Force planes bomb Berlin causing only light damage and killing 131. The RAF lose nine aircraft and 53 aviators.
19: Prisoners of the Janowska concentration camp stage a mass escape/uprising when they are ordered to cover up evidence of a mass-murder. Most are rounded up and killed
20: Battle of Tarawa begins - United States Marines land on Tarawa and Makin atolls in the Gilbert Islands and take heavy fire from Japanese shore guns. The American public is shocked by the heavy losses of life.
20: British troops under Montgomery continue their slow advances on the eastern side of Italy.
22: The Cairo Conference: US President Franklin D. Roosevelt, British Prime Minister Winston Churchill, and ROC leader Chiang Kai-Shek meet in Cairo, Egypt, to discuss ways to defeat Japan.
23: Heavy damage from Allied bombing of Berlin. Notably, the Deutsche Opernhaus on Bismarckstra e in the Berlin district of Charlottenburg is destroyed.
24: Heavy bombing of Berlin continues.
25: Americans and Japanese fight the naval Battle of Cape St. George between Buka and New Ireland. Admiral Arleigh Burke's destroyers distinguish themselves.
25: Rangoon is bombed by American heavy bombers.
26: The Red Army offensive in the Ukraine continues.
27: The Cairo Conference ("Sextant") ends; Roosevelt, Churchill, and Chiang Kai-shek complete the Cairo Declaration, which deals with the overall strategic plan against Japan.
27: Huge civilian losses in Berlin as heavy bombing raids continue.
28: The Tehran Conference . US President Franklin D. Roosevelt, British Prime Minister Winston Churchill and Soviet Leader Joseph Stalin meet in Tehran to discuss war strategy; (on November 30 they establish an agreement concerning a planned June 1944 invasion of Europe codenamed Operation Overlord). Stalin at last has the promise he has been waiting for.
29: Second session of AVNOJ, the Anti-fascist council of national liberation of Yugoslavia, is held in Jajce, Bosnia and Herzegovina, determining the post-war order of the country.
30: In Malaya, Japanese introduce the GOVERNMENT NOTIFICATION No. 41 to encourage families to grow their own food crops and vegetables. Families who are successful will be awarded prizes while family who fail to comply this notification or leave their vacant lands unplanted will be punished. This notification was written by Itami Masakichi (Penang Shu Chokan) on 25 November 2603/1943




2: The Germans conduct a highly successful Air Raid on Bari, Italy. One of the German bombs hits an allied cargo ship carrying mustard gas, releasing the chemical which killed 83 allied soldiers. Over 1000 other soldiers died in the raid.
3: Edward R. Murrow delivers his classic "Orchestrated Hell" broadcast over CBS Radio describing a Royal Air Force nighttime bombing raid on Berlin.
4: Bolivia declares war on all Axis powers.
: In Yugoslavia, resistance leader Marshal Josip Broz Tito proclaims a provisional democratic Yugoslav government in-exile.
12: Rommel is appointed head of "Fortress Europa", chief planner against the expected Allied offensive.
13: German soldiers carry out the Massacre of Kalavryta in southern Greece.
: United States VIII Corps arrives in European Theater.
14: United States XV Corps arrives in European Theater.
16: Kalinin is retaken in a large Red Army offensive.
24: US General Dwight D. Eisenhower becomes the Supreme Allied Commander in Europe.
26: German battleship Scharnhorst is sunk off North Cape (in the Arctic) by an array of British cruisers and destroyer torpedoes.
26: American Marines land on Cape Gloucester, New Britain.
27: General Eisenhower is officially named head of Overlord, the invasion of Normandy.
28: In Burma, Chinese troops have some success against the Japanese.
29: Control of the Andaman Islands is handed over to Azad Hind by the Japanese



Strategic operations of the Red Army in World War II






Timeline For World War 2
Timeline of WWII
Documents of World War II
World War II Timeline