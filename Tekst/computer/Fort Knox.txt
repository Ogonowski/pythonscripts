Fort Knox is a United States Army post in Kentucky south of Louisville and north of Elizabethtown. The 109,000 acre (170 sq mi, 441 km ) base covers parts of Bullitt, Hardin, and Meade counties. It currently holds the Army human resources Center of Excellence to include the Army Human Resources Command, United States Army Cadet Command and the United States Army Accessions Command.
For 60 years, Fort Knox was the home of the U.S. Army Armor Center and the U.S. Army Armor School (now relocated to Fort Benning, Georgia as a result of BRAC action), and was used by both the Army and the Marine Corps to train crews on the M1 Abrams main battle tank. The history of the U.S. Army's Cavalry and Armored forces, and of General George S. Patton's career, can be found at the General George Patton Museum on the grounds of Fort Knox.




The United States Department of the Treasury has maintained the Bullion Depository on the post since 1937.
This facility is operated solely by the Treasury Department.



Parts of the base in Hardin and Meade counties form a census-designated place (CDP), which had a population of 12,377 at the 2000 census and 10,124 at the 2010 census.



The George S. Patton Museum and Center of Leadership at Fort Knox includes an exhibit highlighting leadership issues that arose from the attacks of September 11, 2001, which includes two firetrucks. One of them, designated Foam 161, was partially charred and melted in the attack upon the Pentagon. Fort Knox is also the location of the United States Army's Human Resources Command's Timothy Maude Center of Excellence, which was named in honor of Lieutenant General Timothy Maude, the highest-ranking member of the U.S. military to die in the attacks of September 11, 2001.
In 2012, the U.S. Army Armor School was relocated to "The Maneuver Center of Excellence" at FT Benning, GA.






Fortifications were constructed near the site in 1861, during the Civil War when Fort Duffield was constructed. Fort Duffield was located on what was known as Muldraugh Hill on a strategic point overlooking the confluence of the Salt and Ohio Rivers and the Louisville and Nashville Turnpike. The area was contested by both Union and Confederate forces. Bands of organized guerrillas frequently raided the area during the war. John Hunt Morgan the 2nd Kentucky Cavalry for the Confederate Army raided the area before staging his famous raid on Indiana and Ohio known as Morgan's Raid.



After the war, the area now occupied by the Army was home to various small communities. In October 1903, military maneuvers for the Regular Army and the National Guards of several states were held at West Point, Kentucky and the surrounding area. In April 1918, field artillery units from Camp Zachary Taylor arrived at West Point for training. 20,000 acres (8,100 ha) near the village of Stithton were leased to the government and construction for a permanent training center was started in July 1918.



The new camp was named after Henry Knox, the Continental Army's chief of artillery during the Revolutionary War and the country's first Secretary of War. The camp was extended by the purchase of a further 40,000 acres (16,000 ha) in June 1918 and construction properly began in July 1918. The building program was reduced following the end of the war and reduced further following cuts to the army in 1921 after the National Defense Act of 1920. The camp was greatly reduced and became a semi-permanent training center for the 5th Corps Area for Reserve Officer training, the National Guard, and Citizen's Military Training Camps (CMTC). For a short while, from 1925 to 1928, the area was designated as "Camp Henry Knox National Forest."



The post contains an airfield, called Godman Army Airfield, that was used by the United States Army Air Corps, and its successor, the United States Army Air Forces as a training base during World War II. It was used by the Kentucky Air National Guard for several years after the war until they relocated to Standiford Field in Louisville. The airfield is still in use by the United States Army Aviation Branch.




In 1931 a small force of the mechanized cavalry was assigned to Camp Knox to use it as a training site. The camp was turned into a permanent garrison in January 1932 and renamed Fort Knox. The 1st Cavalry Regiment arrived later in the month to become the 1st Cavalry Regiment (Mechanized).
In 1936 the 1st was joined by the 13th to become the 7th Cavalry Brigade (Mechanized). The site quickly became the center for mechanization tactics and doctrine. The success of the German mechanized units at the start of World War II was a major impetus to operations at the fort. A new Armored Force was established in July 1940 with its headquarters at Fort Knox with the 7th Cavalry Brigade becoming the 1st Armored Division. The Armored Force School and the Armored Force Replacement Center were also sited at Fort Knox in October 1940, and their successors remained there until 2010, when the Armor School moved to Fort Benning, Georgia. The site was expanded to cope with its new role. By 1943, there were 3,820 buildings on 106,861 acres (43,245 ha). A third of the post has been torn down within the last ten years, with another third slated by 2010.



On October 18, 1993, Arthur Hill went on a shooting rampage, killing three and wounding two before attempting suicide, shooting and severely wounding himself. The shooting occurred at Fort Knox's Training Support Center. Prior to the incident, Hill's coworkers claimed they were afraid of a mentally unstable person who was at work. Arthur Hill died on October 21 of his self-inflicted gunshot wound.



On April 3, 2013, a civilian employee was shot and killed in a parking lot on post. Investigators were seeking to question a man in connection with the shooting. The victim was an employee of the United States Army Human Resources Command and was transported to the Ireland Army Community Hospital, where he was pronounced dead. This shooting caused a temporary lock-down that was lifted around 7 p.m. on the same day. Marquinta E. Jacobs, a soldier stationed at Fort Knox, was charged on April 4 with the shooting.




The Army Human Resource Command Center re-located to Fort Knox from the Washington D.C./Virginia area beginning in 2009. New facilities are under construction throughout Fort Knox, such as the new Army Human Resource Center, the largest construction project in the history of Fort Knox. It is a $185 million, three-story, 880,000-square-foot (82,000 m2) complex of six interconnected buildings, sitting on 104 acres (42 ha).
In May 2010, The Human Resource Center of Excellence, the largest office building in the state, opened at Fort Knox. The new center employs nearly 4,300 soldiers and civilians.




Fort Knox is one of only three Army posts (the others being Fort Campbell, Kentucky and Fort Sam Houston, Texas) that still have a high school located on-post. Fort Knox High School, serving grades 9 12, was built in 1958 and has undergone only a handful of renovations since then; but a new building was completed in 2007.






3rd Sustainment Command (Expeditionary)
19th Engineer Battalion
4th Cavalry Brigade, First Army Division East
95th Training Division (formerly 95th Infantry Division)
113th Band
Ireland Army Community Hospital MEDDAC
84th Training Command (UR)
70th Training Division (FT)

United States Army Recruiting Command
3rd Recruiting Brigade
U S Army Medical Recruiting Brigade



3rd Brigade, 1st Infantry Division
16th Cavalry Regiment
1st Squadron
2nd Squadron
3rd Squadron

194th Armored Brigade
1st Cavalry Regiment
7th Squadron (Air)
Troops A, B, C, D, & HHT
235th Aviation Co. (Attack Helicopter)

81st Armored Regiment
1st Battalion
2nd Battalion
3rd Battalion

15th Cavalry Regiment
5th Squadron

46th Infantry Regiment
1st Battalion
2nd Battalion

34th Military Police Detachment
46th Adjutant General Battalion
Source:



Fort Knox is located at 37 54'09.96" North, 85 57'09.11" West, along the Ohio River. The depository itself is located at 37 52'59.59" North, 85 57'55.31" West.
According to the Census Bureau, the base CDP has a total area of 20.94 square miles (54.23 km2), of which 20.92 sq mi (54.18 km2) is land and 0.03 sq mi (0.08 km2) 0.14% is water. Communities near Fort Knox include Brandenburg, Elizabethtown, Hodgenville, Louisville, Radcliff, Shepherdsville, and Vine Grove, Kentucky. The Meade County city of Muldraugh is completely surrounded by Fort Knox.



The climate in this area is characterized by hot, humid summers and generally mild to cool winters. According to the K ppen Climate Classification system, Fort Knox has a humid subtropical climate, abbreviated "Cfa" on climate maps.



As of the census of 2000, there were 12,377 people, 2,748 households, and 2,596 families residing on base. The population density was 591.7 inhabitants per square mile (228.5/km2). There were 3,015 housing units at an average density of 144.1/sq mi (55.6/km2). The racial makeup of the base was 66.3% White, 23.1% African American, 0.7% Native American, 1.7% Asian, 0.4% Pacific Islander, 4.3% from other races, and 3.6% from two or more races. Hispanics or Latinos of any race were 10.4% of the population.
There were 2,748 households out of which 77.7% had children under the age of 18 living with them, 86.0% were married couples living together, 6.1% had a female householder with no husband present, and 5.5% were non-families. 4.9% of all households were made up of individuals and 0.1% had someone living alone who was 65 years of age or older. The average household size was 3.49 and the average family size was 3.60.
The age distribution was 34.9% under the age of 18, 25.5% from 18 to 24, 37.2% from 25 to 44, 2.3% from 45 to 64, and 0.1% who were 65 years of age or older. The median age was 22 years. For every 100 females there were 155.7 males. For every 100 females age 18 and over, there were 190.3 males. These statistics are generally typical for military bases.
The median income for a household on the base was US$34,020, and the median income for a family was $33,588. Males had a median income of $26,011 versus $21,048 for females. The per capita income for the base was $12,410. About 5.8% of the population and 6.6% of the population were below the poverty line, including 7.6% of those under the age of 18 and 100.0% of those 65 and older.



The term "Fort Knox" is used in general discussion as a synonym for a secure location.



Elizabethtown, KY Metropolitan Statistical Area
Goldfinger (film)
Ireland Army Community Hospital
List of attractions and events in the Louisville metropolitan area
List of World War II military service football teams
Louisville-Elizabethtown-Scottsburg, KY-IN Combined Statistical Area
Louisville-Jefferson County, KY-IN Metropolitan Statistical Area






Official website
Fort Knox area booming
Patton Museum, (at Fort Knox)
Ireland Army Community Hospital, (at Fort Knox)
Satellite photo of the Fort Knox Bullion Depository, click to enlarge
Official Base information from the DOD Military Installations website
Fort Knox Morale, Welfare, and Recreation