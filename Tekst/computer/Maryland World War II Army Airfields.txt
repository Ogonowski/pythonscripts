During World War II, the United States Army Air Forces (USAAF) established numerous airfields in Maryland for training pilots and aircrews of USAAF fighters and bombers.
Most of these airfields were under the command of First Air Force or the Army Air Forces Training Command (AAFTC) (A predecessor of the current-day United States Air Force Air Education and Training Command). However the other USAAF support commands (Air Technical Service Command (ATSC); Air Transport Command (ATC) or Troop Carrier Command) commanded a significant number of airfields in a support roles.
It is still possible to find remnants of these wartime airfields. Many were converted into municipal airports, some were returned to agriculture and several were retained as United States Air Force installations and were front-line bases during the Cold War. Hundreds of the temporary buildings that were used survive today, and are being used for other purposes.



First Air Force
Baltimore MAP, Baltimore
Also part of Air Technical Service Command
394th Army Air Force Base Unit
Now: Industrial area (non-aviation use)
Camp Springs/Andrews Field AAF, Camp Springs
Headquarters, Continental Air Forces
463d Army Air Force Base Unit
Now:  Andrews Air Force Base (IATA: ADW, ICAO: KADW, FAA LID: ADW)
Proving Ground Command
Phillips Field AAF, Havre de Grace
Now: active US Army Airfield (IATA: APG, ICAO: KAPG, FAA LID: APG), part of Aberdeen Proving Ground



Maurer, Maurer (1983). Air Force Combat Units Of World War II. Maxwell AFB, Alabama: Office of Air Force History. ISBN 0-89201-092-4.
Ravenstein, Charles A. (1984). Air Force Combat Wings Lineage and Honors Histories 1947-1977. Maxwell AFB, Alabama: Office of Air Force History. ISBN 0-912799-12-9.
Thole, Lou (1999), Forgotten Fields of America : World War II Bases and Training, Then and Now - Vol. 2. Pictorial Histories Pub . ISBN 1-57510-051-7
Military Airfields in World War II - Maryland


