The Persian Corridor was a supply route through Iran into Soviet Azerbaijan by which British aid and American Lend-Lease supplies were transferred to the Soviet Union during World War II.



English-language official documents from the Persian Corridor period continue to make the word "Persia" completely interchangeable with name of the nation-state of "Iran." In correspondence by the government of the United Kingdom, usage of "Persia" over "Iran" was chosen by Winston Churchill to avoid possible confusion with neighboring Iraq.



See main article Anglo-Soviet invasion of Iran

Following Germany's invasion of the USSR in June 1941, Britain and the Soviet Union became allies. Both turned their attention to Iran. Britain and the USSR saw the newly opened Trans-Iranian Railway as an attractive route to transport supplies from the Persian Gulf to the Soviet region. Britain and the USSR used concessions extracted in previous interventions to pressure Iran (and, in Britain's case, Iraq) into allowing the use of their territory for military and logistical purposes. Increased tensions with Britain especially led to pro-German rallies in Tehran. In August 1941, because Reza Shah refused to expel all German nationals and come down clearly on the Allied side, Britain and the USSR invaded Iran, arrested the monarch and sent him into exile to South Africa, taking control of Iran's communications and the coveted railway.

In 1942 the United States, by then an ally of Britain and the USSR in World War II, sent a military force to Iran to help maintain and operate sections of the railway. The British and Soviet authorities allowed Reza Shah's system of government to collapse, and they limited the constitutional government interfaces. They put Reza Shah's son, Mohammad Reza Pahlavi onto the Iranian/Persian throne.
The new Shah soon signed an agreement pledging full non-military logistical cooperation with the British and Soviets, in exchange for full recognition of his country's independence, and also a promise to withdraw from Iran within six months of the war's conclusion. In September 1943, the Shah went further, and he declared war on Germany. He signed the Declaration by the United Nations entitling his country to a seat in the original United Nations. Two months later, he hosted the Tehran Conference between Churchill, Roosevelt, and Stalin.
The presence of so many foreign troops in Iran accelerated social change and it roused nationalist sentiment in the country. In 1946, Hossein Gol-e-Golab published the nationalist song Ey Iran; it was reportedly inspired by an incident during the war in which Golab witnessed an American GI beating up a native Iranian greengrocer in a marketplace dispute.



Meanwhile, Soviet leader Joseph Stalin, under pressure from the British and the Polish Government-in-Exile, began releasing some surviving Polish prisoners-of-war captured in 1939, and also Polish citizens subsequently deported by the occupying Soviets to the Soviet republics, with the aim of forming a Polish army to fight on the Allied side. General W adys aw Anders was released from the Lubyanka Prison, and he began assembling his troops. However, continued friction with the Soviets and their refusal to adequately supply the Polish troops with war equipment and food, as well as the Soviets' insistence on dispersing unprepared Polish units along the front, led to the eventual evacuation of Anders's troops, along with a sizable contingent of Polish civilians, to Iran. These troops formed the basis of what later became 2nd Polish Corps which went on to serve with distinction in the Italian campaign but some civilians settled permanently in Iran. Some Polish refugees who continue living in Iran today were featured in the 2002 independent film by Jagna Wright and Aneta Naszynska, A Forgotten Odyssey. More information is also available through the Kresy-Siberia Group.




The Allies delivered all manner of materiel to the Soviets, from Studebaker US6 trucks to American B-24 bombers. Most supplies in the corridor arrived by ship at Persian Gulf ports, and then were carried north by railway or in truck convoys. Some goods were reloaded onto ships to cross the Caspian Sea, and others continued by truck.
The United States Army forces in the corridor were originally under the Iran-Iraq Service Command - later renamed the Persian Gulf Service Command (PGSC). This was the successor to the original United States Military Iranian Mission, which had been put in place to deliver Lend-Lease supplies before the United States had entered the World War. The mission was originally commanded by Colonel Don G. Shingler, who was then replaced late in 1942 by Brigadier General Donald H. Connolly. Both the Iran-Iraq Service Command and the PGSC were subordinate to the U.S. Army Forces in the Middle East (USAFIME). PGSC was eventually renamed simply the Persian Gulf Command.



The Allied supply efforts were enormous. The Americans alone delivered over 16.3 million tonnes to the Soviets during the war, via three routes, including Arctic convoys of World War II to the ports of Murmansk and Archangelsk. Also, Soviet shipping carried supplies from the west coast of the United States and Canada to Vladivostok in the Far East, since the Soviet Union was not at war with Japan at that time (until August 1945). The Persian Corridor was the route for 4,159,117 long tons (4,225,858 metric tonnes) of this cargo. However, this was not the only American contribution via the Persian Corridor - not to mention the contributions of other Allies like Great Britain, Canada, South Africa, Australia, and numerous other nations, colonies, and protectorates of the Allied nations. All told, about 7,900,000 long tons (8,000,000 metric tonnes) of shipborne cargo from Allied sources were unloaded in the Corridor, most of it bound for Russia - but some of it for British forces under the Middle East Command, or for the Iranian economy, which was sustaining the influx of tens of thousands of foreign troops and Polish refugees. Also, supplies were needed for the development of new transportation and logistics facilities in Persia and in the Soviet Union. The tonnage figure does not include transfers of warplanes via Persia.




Supplies came from as far away as Canada and the United States, and those were unloaded in Persian Gulf ports in Iran and Iraq. Once the Axis powers were cleared from the Mediterranean Sea in 1943 - with the Allied capture of Tunisia, Sicily, and southern Italy - cargo convoys were able to pass through the Mediterranean, the Suez Canal, and the Red Sea to Iran for shipment to the USSR.
The main ports in the Corridor for supplies inbound to Iran were: in Iran,
Bushehr
Bandar Shahpur (now Bandar Imam Khomeini); and
in Iraq,
Basra
Umm Qasr.
The main overland routes were from the ports to Tehran, and then
Tehran   Askhabad or
Tehran   Baku
or, alternatively,
Basra   Kazvin or
Dzhulfa   Beslan.
The main port for outbound supplies (via the Caspian Sea) was Nowshahr. Ships ferried supplies from this port to Baku or Makhachkala.



Important smaller ports and transit points on the routes included:



Lenkoran;



Yerevan;



Tbilisi;



Beslan;




Ports
Bandar Anzali
Bandar Abbas
Chabahar
Noshahr
Bandar-e Shah (now Bandar Torkoman)
Amir Abad port
Khoramshahr
Bushehr
Assalouyeh
Mahshahr
BIK port
Fereydunkenar [1]
Cities



Ports
Krasnovodsk
Cities
Ashgabat
Kizyl Arvat
Kizyl Atrek




Cargo was principally handled by special British and American transportation units from the nations' respective combat service support branches, such as the Royal Army Service Corps and the United States Army Quartermaster Corps. Many Allied civilian workers, such as stevedores and railway engineers, were also employed on the corridor. Many skilled engineers, accountants and other professionals who volunteered or were drafted into the armed services were made warrant officers to help oversee the complex supply operations.
In addition to providing logistical support to the Iranians, the Allies offered other services as well. The Americans in particular were viewed as more neutral since they had no colonial past in the country as did the British and Russians. The Americans contributed special expertise to the young Shah's government. Colonel Norman Schwarzkopf, Sr., who at the outbreak of the war was serving as superintendent of the New Jersey State Police was in August 1942 put in charge of training the Imperial Iranian Gendarmerie. Coincidentally, his son, Norman Schwarzkopf, Jr., would make his own mark on the Middle East almost fifty years later during the Persian Gulf War.



To help operate trains on the demanding Trans-Iranian Railway route, the US supplied large numbers of ALCO diesel locomotives, which were more suitable than steam locomotives. About 3000 pieces of rolling stock of various types were also supplied.


