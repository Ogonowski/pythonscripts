JHDL (Just-Another Hardware Description Language) is a low-level structural hardware description language, focused primarily on building circuits via an Object Oriented approach that bundles collections of gates into Java objects. Implemented as a toolset and class library on top of the Java programming language, its primary use is for the design of digital circuits for implementation in field-programmable gate arrays (FPGAs). Particular attention was paid to supporting the Xilinx series of chips.
When the design is ready to be placed in a fabric, the developer simply generates an Electronic Design Interchange Format (EDIF) netlist and imports it into his favorite toolkit. Once imported, the developer should be able to transfer the circuit via a Joint Test Action Group (JTAG) cable. EDIF netlisting is supported for the XC4000, Virtex, and Virtex-II series of FPGAs.
JHDL was developed at BYU in the Configurable Computing Laboratory, the project initiated in 1997.  As of July 2013, the latest update to the JHDL project was made in May 2006 according to the official JDHL website.



The JHDL language features include:
Structural hardware design
Flexible module generators
Table-generated finite state machines
A graphical "Workbench" toolkit
Behavioral synthesis is not yet fully supported.
The integrated JHDL Workbench environment is designed to allow developers to graphically test and trace their circuit designs. This tool includes:
A graphical schematic viewer
A multiclock cycle-based simulator
A command line interface
A complete list of all wires and gates
A complete status of all values passing through the circuit



Originally, the J in "JHDL" stood for "Java". However, to prevent trademark issues, the name has been backronymed to stand for Just-Another Hardware Description Language.



Official JHDL website
BYU's Configurable Computing Laboratory
Cosmic Horizon JHDL
^ Brent E. Nelson, "The Mythical CCM: In Search of Usable (and Resuable) FPGA-Based General Computing Machines," asap, pp.5-14, IEEE 17th International Conference on Application-specific Systems, Architectures and Processors (ASAP'06), 2006