Cursor movement keys or arrow keys are buttons on a computer keyboard that are either programmed or designated to move the cursor in a specified direction. The term "cursor movement key" is distinct from "arrow key" in that the former term may refer to any of various keys on a computer keyboard designated for cursor movement, whereas "arrow keys" generally refers to one of four specific keys, typically marked with arrows.
Arrow keys are typically located at the bottom of the keyboard to the left side of the numeric keypad, usually arranged in an inverted-T layout but also found in diamond shapes and linear shapes. Arrow keys are commonly used for navigating around documents and for playing games. The inverted-T layout was popularized by the Digital Equipment Corporation LK201 keyboard from 1982.



Before the computer mouse was widespread, arrow keys were the primary way of moving a cursor on screen. Mouse keys is a feature that allows controlling a mouse cursor with arrow keys instead. A feature echoed in the Amiga whereby holding the Amiga key would allow a person to move the pointer with the arrow keys in the Workbench (operating system), but most games require a mouse or joystick. The use of arrow keys in games has come back into fashion from the late 1980s and early 1990s when joysticks were a must, and were usually used in preference to arrow keys with some games not supporting any keys. It can be used instead of WASD keys, to play games using those keys.
The inverted-T layout was popularized by the Digital Equipment Corporation LK201 keyboard from 1982.
Some Commodore 8-bit computers used two keys instead of four, with directions selected using the shift key.

The original Apple Macintosh had no arrow keys at the insistence of Steve Jobs, who felt that people should use the mouse instead. They were deliberately excluded from the Macintosh launch design as a forcing device, acclimating users to the new mouse input device and inducing software developers to conform to mouse-driven design rather than easily porting previous terminal-based software to the new platform. Arrow keys were included in later Apple keyboards. Early models with arrow keys but no middle section (Home, End, etc.) placed them in one line below the right-hand Shift key in an HJKL-like fashion; later versions had a standard inverted-T layout, either in the middle block or as half-height keys at the bottom right of the main keyboard.



Although the "arrow keys" provide one convention for cursor movement on computers, there are also other conventions for cursor movement that use entirely different keys. In Power Point, a selected object may be moved in "micro-increments" by depressing Ctrl whilst using the arrow keys (according to Microsoft on-line answers). This doesn't work in Publisher.



This layout dates back to Sinclair ZX80, Sinclair ZX81 and Sinclair Spectrum software: the original Sinclair machines had cursor keys on the top row, keys 5, 6, 7 and 8. Due to the unusual programming technique adapted by Sinclair, these keys were accessed either by using the   Shift key in conjunction with a numeric key or by the numeric key alone depending on program in use.




WASD (,AOE on Dvorak keyboards; ZQSD on AZERTY keyboards) is a set of four keys on a QWERTY or QWERTZ computer keyboard which mimics the inverted-T configuration of the arrow keys. These keys are often used to control the player character's movement in computer games, most commonly first person games but also in many driving and third person games. W/S control forward and backward, while A/D control strafing left and right. Typically, modifier keys are not used to strafe (as performed using the Alt + arrow keys in older games such as Doom and Duke Nukem 3D).
Primarily, WASD is used to account for the fact that the arrow keys are not ergonomic to use in conjunction with a right-handed mouse. This also allows the user to use the left hand thumb to press the space bar (often the jump command) and the left hand little finger to press the Ctrl or   Shift keys (often the crouch and/or sprint commands). In later games, the usage of the E key to interact with items or open up the inventory was also popularized due to its location next to the WASD keys, allowing players to reach it quickly.
As opposed to the default keyboard-only controls of Doom or Duke Nukem 3D, WASD is usually used in combination with a mouse. The greatest advantage of using a mouse and keyboard combination over a keyboard-only configuration is the ability to use the mouse to look around both vertically and horizontally, referred to as mouselook. Mouselook enables the player to perform techniques such as smooth circle strafing, which, although possible with the keyboard, was difficult to perform and resulted in jagged movement.
The scheme wasn't popularized until competitive play in Quake and subsequently QuakeWorld made clear its advantages over the older arrow key configurations. In the same year that Castle Wolfenstein was released, 1981, the game Wizardry used the AWD keys for movement in a 3D dungeon. Both the programmers of Castle Wolfenstein and Wizardry were users of the earlier PLATO system where the game Moria used the AWD keys.
Most gamers prefer the WASD keys to the arrow keys for other various reasons, including the fact that more keys (and therefore, game commands) are easily accessible with the left hand when placed near WASD. Left-handed mouse users may prefer using the numpad or IJKL with their right hands instead for similar reasons.
After being popularized by first-person shooters, WASD became more common in other computer game genres as well. Many of the games that have adopted this layout use a first-person or over-the-shoulder third-person perspective. However, some games that use overhead camera views also use WASD to move the camera, such as some city-building games and economic simulation games.



The ESDF variation is an alternative to WASD and is sometimes preferred because it provides access to movement independent keys for the little finger (Q, A, Z) which generally allows for more advanced manual binding. Incidentally, it allows the left hand to remain in homerow with the advantage of the F key homerow marker (available on most standard keyboards) to easily return to position with the index finger.
Perhaps the earliest game to use ESDF was Crossfire (1981), which used the keys for firing in multiple directions. It is the default configuration for several games such as Tribes 2. The game Blackthorne used a combination of arrow keys for movement and ESDF for actions. Moreover, these keys are compatible with both QWERTY and AZERTY keyboard layouts, which is a major plus if the game is also released in France or Belgium.
Some players use RDFG or TFGH to give access to even more keys (S and X for the little finger).



Another alternate to the WASD shooter movement style is DCAS (sometimes called ASDC). In this configuration, D and C control forward and backward motion, while A and S control side-stepping (strafing). Typically the Alt key is utilized for crouching instead of the Ctrl key, as it is more easily reached when the hand is positioned for DCAS.
When Bungie's first-person shooter Marathon was released in 1994, it featured up/down look control and the option to fully control turning and aiming by mouse (a feature later popularized by id's Quake as mouselook/freelook). However, it did not include a set of default controls to handle this. With WASD not yet a well-known standard, some gamers devised their own control schemes to handle combined keyboard movement with mouse aiming; DCAS was one such control scheme.
Like WASD, DCAS allows the player to easily utilize the left modifier keys; this is advantageous because on most keyboards, the circuitry is better at tracking multiple key-presses simultaneously if some of them are modifier keys. But unlike WASD, the position of the left hand for DCAS gaming is very similar to the left hand's position on the home row keys. This is very comfortable for right-handed gamers and seen as the primary advantage over using WASD, but it is ill-suited for left-handed mousing.



Two early games that used IJKL were Crossfire (1981) and Lode Runner (1983).
IJKL is used by a growing number of browser games. These games cannot use the arrow keys because many browsers' windows will scroll if the arrow keys are used, thus hindering gameplay. This is a problem specific to DHTML/JavaScript games. IJKL, like WASD, are arranged in an ergonomic inverted T shape, and, since they are used by the right hand, adjustment is easy for people who commonly use the arrow keys.
Also, in many games that also use the WASD keys, the IJKL keys are sometimes used as a secondary player control for games that have multiplayer.
Devil May Cry 4 by Capcom utilizes IJKL as the player's action keys (such as Melee Attack, Ranged Attack, Special Attack etc.) as an alternative to mouse-driven actions.



Some older computer games, especially those on 8-bit platforms, often had the combination IJKM used as the standard control key combination, which was more logically arranged, if far less ergonomic than an inverted-T. In addition, on the Apple II platform, special support existed in ROM for Escape mode. At the Applesoft BASIC prompt, using the right and left arrow keys to move the cursor would add/remove characters the cursor passed over to/from the input buffer. Pressing the Escape key entered a mode where pressing the I, J, K or M keys would move the cursor without altering the input buffer. After exiting this mode by pressing Escape again, normal behavior would resume. This made it easy to edit lines of BASIC code by listing them, then re-inputting them with edits interspersed.



The Apple II and Apple II Plus originally had left and right arrow keys but no up and down arrow keys. Many programs written for these computers used A and Z to substitute for the missing up and down keys. The IJKM combination was also popular on these computers. These keys fell somewhat out of favor after the release of the Apple IIe, which had a full set of arrow keys.



HJKL is a layout used in the Unix computer world, a practice spawned by its use in the vi text editor. The editor was written by Bill Joy for use on an Lear-Siegler ADM-3A terminal, which places arrow symbols on these letters since, like the original Mac shown above, it did not have dedicated arrow keys on the keyboard. These correspond to the functions of the corresponding control characters Ctrl+H, Ctrl+J, Ctrl+K, and Ctrl+L when sent to the terminal, moving the cursor left, down, up, and right, respectively. (The Ctrl+H and Ctrl+J functions were standard, but the interpretations of Ctrl+K and Ctrl+L were unique to the ADM-3A.) This key arrangement is often referred to as "vi keys". HJKL keys are still ubiquitous in newly developed Unix software even though today's keyboards have arrow keys. They have the advantage of letting touch-typists move the cursor without taking their fingers off of the home row. Examples of games that use HJKL are the text-based "graphic" adventures like NetHack, the Rogue series, and Linley's Dungeon Crawl. It is also used by some players of the Dance Dance Revolution clone StepMania, where HJKL corresponds directly to the order of the arrows. Gmail, Google labs' keyboard shortcuts and other websites use J and K for "next" and "previous".



Another old-style variation that spawned from games like Quake was the SDF-SPACE layout. In this layout, S = turn left, D = forward, F = turn right, space = backpedal, E = strafe left and R = strafe right. This layout allows the player to aim with the mouse while strafing (sidestepping), turning and running or backpedalling all at once creating slightly more complex movements. This variation is not favoured any longer for two main reasons. First, because many players deem the turn commands useless because the mouse can act as a turning device, and so they assign S and F to the sidestep commands and leave the turn commands unassigned. The second, and probably more prominent reason is, in assigning both the turn and strafe commands, performing movements and dodges can be much more confusing, so newcomers tend to not prefer this key setup. Though no longer widely used, many FPS veterans and tournament players still employ this key setup.
This variant adopted to newer games using mouse-look doesn't really need worry about the turn left and turn right keys. Instead S = strafe left, D = strafe right, A = backpedal, and space or F = forward. This is a more natural feel on the keyboard as your fingers rest on the home row. The comfort and usability points from ESDF apply here. Notice that jump is left out, that is because in games like Quake jump was usually MOUSE2. With more alternate fire and aim-down-sights oriented games today you may have to play with F, SPACE, and MOUSE2 on what you want them to do.
A similar layout is ASD-SPACE. Commonly used in 2D-based fighting games, ASD-SPACE maps A, S, and D to "left", "down", and "right" movement, while the spacebar is used for "up" (jumping). This allows an easier access to "360 degree" motions than a normal inverted-T layout, as well as being more ergonomic than simply placing all directions in a single row (ex. ASDF). There is even a "stickless" arcade controller based around the concept, called the Hitbox.



Another, close, variation is the WQSE combination, which follows the belief that the index and ring fingers' natural and more ergonomic positions when the middle finger is on W are Q and E rather than A and D, respectively. This can be attested to by the fact that the arrow keys were partly designed in the inverted-T shape in order to avoid having the side buttons possibly directly underneath other keys. It also has the advantage that there is less distance needed to travel to reach the number keys. For similar reasons, some gamers use the WQSD combination (which is WASD with the A key moved up to Q, or WQSE with the E moved down to D). For players who prefer to keep the keyboard centered on the body, this results in less wrist rotation, as it places the index finger naturally over the D key when the left arm rests down to the left of the keyboard. SAZD is a slight variation on WQSE and WQSD, in that it is both ergonomic and rotated, but gives the fingers closer proximity to the   Shift and SPACE keys.



Dating back to Sinclair Spectrum days. The O/P keys were used for left/right movement, and the Q/A keys were used for up/down or forward/backwards movement. With SPACE being a key in the bottom right of the original rubber keyed Spectrum the M or sometimes N key would be used for fire/action; on later models the SPACE bar would be used.
QAOP had its own variations, as ZXKM or WELP. Many BBC Micro games used the keys ZX*? respectively for left, right, up, and down (with "*" being the key above and right of "?", where "@" is on modern keyboards). In each case one hand controls left/right and the other hand controls up/down movement. A further variation is used when two players use the keyboard at the same time, for example, Gauntlet uses the combination 1QSD for Player 1, and 8IKL for Player 2.



The ESDX keys (known as the "cursor movement diamond") were used in the WordStar word processor and related applications, in combination with the Control key.
These keys were also used, and had arrows printed on them, on the TI-99/4(A) computer (1979-1984).



The numpad or number pad keys are used quite often, but is used mostly in driving simulator games. This is mainly because these games usually have quite a large amount of keys needed to control the vehicle properly and the number pad will have plenty of keys for that particular use. Another reason this is commonly used is because right-handed players will find this a more comfortable position than the IJKL keys (see above), and the number pad has less keys around it, thus it is less likely the player will hit the wrong key by mistake.




Another variation is WAXD, using either 4 or 8 keys surrounding the "s" key. Eight directional arrows were printed on the keysets of PLATO terminals developed in the 1960s and 70s, and many games (including Empire, Dogfight, and more than a dozen dungeon games such as Avatar) utilize this layout. The TUTOR language app generator, character set (bitmap) editor, and line set editor also use these keys on that system and its successors. For ballistic targeting, key combinations (e.g. "we", "de", etc.) are used to get angles in multiples of 22.5 degrees. In many programs the "s" key is often used to either "select" or "shoot", depending upon the application.
This layout also appears on the numeric keypads of some keyboards, using 8462 (including 7913).



Vaguely related is the ZXC layout, used in many freeware games, and a common setup for emulation and older 2D gaming using a keyboard.
A few games from the 1980s, such as the Phantasie series, used the "3WES" layout, which forms a diamond on QWERTY keyboards. In this layout, three of the four keys happen to correspond to the compass directions "West", "East" and "South". These games usually assigned both "N" and "3" to "North".
AZERTY users will use the "ZQSD" combination instead of "WASD", since those are the keys in place of WASD on a QWERTY keyboard. Depending on the configuration, "QAOP" may either still work or be vertically inverted. On the Dvorak Simplified Keyboard, "WASD" is ",AOE".
Left-handed players may use the numeric keypad instead.
A somewhat uncommon variant is YGHJ which while requiring the keyboard to be turned slightly clockwise, can result in the thumb resting comfortably upon the right Alt key and the little finger resting on C. This can be useful in games that utilize both jump and sprint functions as it allows the fingers to rest on smaller keys than   Shift and Space. The YGHJ configuration also places the hand closer to the center of the QWERTY section of the keyboard, potentially opening up the entire board to custom keybindings.
The game Qwop uses the control scheme "QWOP" to control Qwop's arms and legs. the Q and W keys control Qwop's thighs while the O and P keys control Qwop's calves making an intentionally difficult control system as a result.
Layouts such as, Shift Z Ctrl X, where   Shift is up and Ctrl is down, allow all direction keys to be used in any combination, without the delay of changing finger position. But the use of modifier keys can be problematic, as some games do not allow mapping of these keys.





