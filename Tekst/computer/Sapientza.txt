Sapientza or Sapienza (Greek:  ) is a Greek island off the southern coast of the Peloponnese, near the city of Meth ni. It is administratively part of the municipality of Pylos-Nestor, in Messenia. The 2011 census reported a population of two inhabitants. Sapientza is the second largest island of Messenian Oinousses, a small group island which consists of three small islands (Schiza, Sapientza and Agia Marina). Its name has Italian origin and means wisdom. South-west of Sapientza is located the Calypso Deep the deepest point of the Mediterranean Sea, with a depth of 5,267 meters.



On the sea there are many shipwrecks because the island was located over the sea road between Italy and Middle East. Some of them contained important shipload such as a roman wreck which carried granite columns from the peristyle of Herod's temple in Caesarea of Palestine. In the south of the island there is an important lighthouse built in 1885. Its height is 8 meters. Sapientza has lush vegetation with dense shrubland and perennial hollies, arbutus and other mediterranean plants. For this reason, Sapientza along with other Messenian Oinousses have been included in the Natura 2000 Network, with code GR2550003.



Sapientza is known for the Treaty of Sapienza, in 1209, between the Republic of Venice and Principality of Achaea. With this treaty Methoni and Corone remained to Venetian and other Peloponnese ceded to Villehardouin. After the Greek War of Independence, Sapienza became a part of Greek state. Nevertheless, British claimed Sapienza during Pacifico case, as part of Ionian Islands.






Treaty of Sapienza


