A network processor is an integrated circuit which has a feature set specifically targeted at the networking application domain.
Network processors are typically software programmable devices and would have generic characteristics similar to general purpose central processing units that are commonly used in many different types of equipment and products.



In modern telecommunications networks, information (voice, video, data) is transferred as packet data (termed packet switching) which is in contrast to older telecommunications networks that carried information as analog signals such as in the public switched telephone network (PSTN) or analog TV/Radio networks. The processing of these packets has resulted in the creation of integrated circuits (IC) that are optimised to deal with this form of packet data. Network Processors have specific features or architectures that are provided to enhance and optimise packet processing within these networks.
Network processors have evolved into ICs with specific functions. This evolution has resulted in more complex and more flexible ICs being created. The newer circuits are programmable and thus allow a single hardware IC design to undertake a number of different functions, where the appropriate software is installed.
Network processors are used in the manufacture of many different types of network equipment such as:
Routers, software routers and switches
Firewalls
Session border controllers
Intrusion detection devices
Intrusion prevention devices
Network monitoring systems



In the generic role as a packet processor, a number of optimised features or functions are typically present in a network processor, these include:
Pattern matching - the ability to find specific patterns of bits or bytes within packets in a packet stream.
Key lookup - the ability to quickly undertake a database lookup using a key (typically an address in a packet) to find a result, typically routing information.
Computation
Data bitfield manipulation - the ability to change certain data fields contained in the packet as it is being processed.
Queue management - as packets are received, processed and scheduled to be sent onwards, they are stored in queues.
Control processing - the micro operations of processing a packet are controlled at a macro level which involves communication and orchestration with other nodes in a system.
Quick allocation and re-circulation of packet buffers.



In order to deal with high data-rates, several architectural paradigms are commonly used:
Pipeline of processors - each stage of the pipeline consisting of a processor performing one of the functions listed above.
Parallel processing with multiple processors, often including multithreading.
Specialized microcoded engines to more efficiently accomplish the tasks at hand.
Recently, multicore architectures are used for higher layer (L4-L7), application processing.
Additionally, traffic management, which is a critical element in L2-L3 network processing and used to be executed by a variety of co-processors, has become an integral part of the network processor architecture, and a substantial part of its silicon area ("real estate") is devoted to the integrated traffic manager



Using the generic function of the network processor, a software program implements an application that the network processor executes, resulting in the piece of physical equipment performing a task or providing a service. Some of the applications types typically implemented as software running on network processors are:
Packet or frame discrimination and forwarding, that is, the basic operation of a router or switch.
Quality of service (QoS) enforcement - identifying different types or classes of packets and providing preferential treatment for some types or classes of packet at the expense of other types or classes of packet.
Access Control functions - determining whether a specific packet or stream of packets should be allowed to traverse the piece of network equipment.
Encryption of data streams - built in hardware-based encryption engines allow individual data flows to be encrypted by the processor.
TCP offload processing


