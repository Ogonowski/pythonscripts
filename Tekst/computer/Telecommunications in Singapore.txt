The telecommunication infrastructure of Singapore spans the entire city-state. Its development level is high, with close accessibility to the infrastructure from nearly all inhabited parts of the island and for all of the population, with exceptions. As of 1998, there were almost 55 million phone lines in Singapore, close to 47 million of which also served other telecommunication devices like computers and facsimile machines. Underwater telephone cables have been laid that lead to Malaysia, the Philippines and Indonesia.
As of March 2009, there are three cellular phone operators in Singapore serving more than 6.4 million cellular phones. As for internet facilities, as of 2009, there are four major internet service providers (ISPs) in Singapore. By February 2009, there were more than 4.8 million broadband users in Singapore. Howevever, due to the small market and possible market collusion, there have been rising concerns that various ISPs' telecommunication infrastructures being highly under-utilised.
Radio and television stations are all government-owned entities. All eight television channels are owned by MediaCorp; its only other competitor, SPH Mediaworks closed its television channel on 1 January 2005. Due to the proximity of Singapore to Malaysia and Indonesia, almost all radios and television sets in Singapore can pick up broadcast signals from both countries. Private ownership of satellite dishes is banned, but most households have access to the StarHub TV and the Singtel IPTV TV(mio TV) network. As of 1997, there were 1.3 million televisions in Singapore.
All radio stations are operated either by MediaCorp, the SAFRA National Service Association (SAFRA) or SPH UnionWorks. As of 1997, there were 2.5 million radios in Singapore.
The print media is dominated by Singapore Press Holdings and MediaCorp. Singapore Press Holdings publishes flagship newspaper The Straits Times. Daily newspapers are published in English, Chinese, Malay, and Tamil.




Telephones - fixed line:
Total Fixed Line Subscriptions: 1,843,000 (March 2009)
Fixed Line Household Penetration: 93.0%
Telephones - mobile market:
Total Mobile Subscriptions (2G+3G+3.5G): 6,414,800 (March 2009)
Mobile Penetration: 132.6%
Operators: 3 (2009)
Singtel
M1
StarHub

Telephone system: Excellent domestic facilities; excellent international servicedomestic: NAinternational: Submarine cables to Malaysia (Sabah and Peninsular Malaysia), Indonesia, and the Philippines; satellite earth stations - 2 Intelsat (1 Indian Ocean and 1 Pacific Ocean), and 1 Inmarsat (Pacific Ocean region)
IDD Country Code: +65




Radio broadcast stations (as of March 2006): AM 0, FM 19, shortwave 5 (Source:Asiawaves.Net )
Radios: 2.55 million (1997)




Television broadcast stations (as of December 2013):
9 free-to-air (9 analogue/digital, 5 high-definition)
Operator:
MediaCorp
StarHub




Singapore has a large number of computer users and most households have computers and Internet access. A survey conducted by Infocomm Development Authority of Singapore indicated that 78% of households own computers at home and 7 in 10 households have Internet access (2006). The CIA's The World Factbook reports that Singapore has 2.422 millions Internet users (2005) and 898,762 Internet hosts (2006).
Internet Service Providers (ISPs): 8 (1999)
Broadband
Subscribers: 4,883,700 (79.5% wireless, 10.3% xDSL, 10.1% cable modem, 0.1% others) as of February 2009
Fiber Internet
Services are provided via Open Net, to the residential and commercial entities. Whilst the services are via one infrastructure, the Fiber itself are provided by Singtel and these are independent of the ISP Equipment. Currently the OLT are provided by Nucleus Connect and Singtel using HuaWei, ZTE and Ericsson ONT. The current licensed Service Providers are Singtel, Starhub, M1, MyRepublic and ViewQwest. Other white labeled ISPs piggy back on these ISPs including SuperInternet, ZoneTEL, Pacific Internet. Singapore is currently aiming 95% Household connection by End 2012 with speeds up to 1Gbit/s, typically where a customer usually subscribes to 100 to 200Mbit/s packages with Voice and IPTV on the platform.
While Nucleus Connect is the Operating Company (OpCo) of the OpenNet infrastructure, it is not the service provider,rather the company that switches the network to the respective ISPs.
Country code (Top level domain): SG



Singapore as a small densely populated island nation is the pioneer, and continues to be one of the few countries in the World in which broadband internet access is readily available to just about any would-be user anywhere in the country, with connectivity over 99%. In a government-led initiative to connect the island in a high-speed broadband network using various mediums such as fibre, DSL and cable, the Singapore ONE project was formally announced in June 1996, and commercially launched in June 1998. By December 1998, Singapore ONE is available nationwide with the completion of the national fibre optics network.
In 1997, commercial trials for Singapore Telecommunications' (Singtel) ADSL-based "SingTel Magix" service were undertaken in March, before being launched in June. Also in June, Singapore Cable Vision commenced trials for its cable modem based services, before being commercially deployed in December 1999. Singtel's ADSL service was subsequently rolled out on a nationwide scale in August 2000.
In January 2001, the Broadband Media Association was formed to promote the broadband industry. By April the same year there were 6 broadband internet providers, with the total number of broadband users exceeding 300,000. Pacific Internet introduced wireless broadband services in October 2001.
In 2007, Infocomm Development Authority(IDA) of Singapore introduced a programme named "Wireless@SG". It is part of its Next Generation National Infocomm Infrastructure initiative. Users can enjoy free, both in-door and outdoor seamless wireless broadband access with speeds of up to 1 Mbit/s at with high human traffic. As at June 2007, there are more than 460,000 subscribers and 4,200 hotspots under the Wireless@SG programme. In the same year, M1 introduced its mobile broadband services.
Due to the rise of OpenNet empire, operators - Singtel and StarHub will all be converted fully to fibre optic by July 2014.
Optical Fiber broadband providers:
StarHub (RSP ; Retail sale *Plans: 100Mbit/s, 300Mbit/s, 500Mbit/s, 1000Mbit/s)
M1 (*Plans: 25 Mbit/s, 50 Mbit/s, 100 Mbit/s, 200Mbit/s, 300Mbit/s, 1000 Mbit/s)
Singtel (RSP ; Retail sale *Plans: 200Mbit/s, 300 Mbit/s, 500Mbit/s)
OpenNet (Passive Infrastructure Company ; NetCo ; Wholesale)
Nucleus Connect (Active Infrastructure Company ; OpCo ; Wholesale)
MyRepublic.com.sg (RSP ; Retail sale *Plans: 100Mbit/s, 150Mbit/s,300Mbit/s,1000Mbit/s)
ViewQwest (RSP ; Retail sale *Plans: 200Mbit/s, 400Mbit/s, 500Mbit/s, 1000Mbit/s, 2000Mbit/s)
Wireless (mobile) broadband providers:
M1 (*Plans: 1 Mbit/s, 2 Mbit/s, 4 Mbit/s, 7.2 Mbit/s, 21 Mbit/s)
StarHub (*,**Plans: 1.2 Mbit/s, 2 Mbit/s, 7.2 Mbit/s, 21 Mbit/s)
Singtel (**Plans: 1 Mbit/s, 1.5 Mbit/s(Only for Youth Plan) 2 Mbit/s, 4 Mbit/s, 7.2 Mbit/s, 14.4 Mbit/s, 21 Mbit/s)
Note:
*Plans refers "unlimited" broadband access plans
** Plans refers to "volume based" broadband access plans
*** Plans refers to "time-based" broadband access plans
Wireless@SG operators (Up to 1 Mbit/s):
iCell
M1
Singtel
StarHub




http://internetinasia.typepad.com/blog/2006/03/singapore_plans.html



Terry Johal, "Controlling the Internet: The use of legislation and its effectiveness in Singapore (pdf file)", Proceedings, 15th Biennial Conference of the Asian Studies Association of Australia, Canberra, 2004.



Ministry Of Information, Communications And The Arts, Singapore
Media Development Authority (MDA), Singapore
Infocomm Development Authority (IDA), Singapore
IDA, Singapore Infocomm Statistics at a Glance, Singapore
Wireless@SG, Singapore
Press in Singapore
Singapore Wireless Guide