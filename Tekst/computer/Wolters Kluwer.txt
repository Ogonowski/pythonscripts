Wolters Kluwer Euronext: WKL is a global information services company. The company is headquartered in Alphen aan den Rijn, Netherlands. Wolters Kluwer in its current form was founded in 1987 with a merger between Kluwer Publishers and Wolters Samson. The company serves legal, business, tax, accounting, finance, audit, risk, compliance, and healthcare markets. It operates in over 150 countries. Nancy McKinstry serves as CEO of the company.






Jan-Berend Wolters founded the Schoolbook publishing house in Groningen, Netherlands in 1836. In 1858, the Noordhoff publishing house was founded alongside the Schoolbook publishing house. The two publishing houses merged in 1968. Wolters-Noordhoff merged with Information and Communications Union (ICU) in 1972 and took the name ICU. ICU changed its name to Wolters-Samson in 1983. The company began serving foreign law firms and multinational companies in China in 1985. In 1987, Elsevier, the largest publishing house in the Netherlands, announced its intentions to buy up Kluwer s stock. Kluwer merged with Wolters-Samson to fend off Elsevier s take-over bid and formed Wolters Kluwer. The merger made Wolters Kluwer the second largest publishing house in the Netherlands.
After the merger, Wolters Kluwer began expanding internationally with the purchase of IPSOA Editore, Kieser Verlag, Technipublicaciones and Tele Consulte in 1989. By the end of the year, Wolters Kluwer expanded its presence to Spain, West Germany and France. The company also launched LEX, its legal information system, in Poland. In 1989, 44% of the company s revenue was earned in foreign markets. The following year, Wolters Kluwer purchased J. B. Lippincott & Co. from HarperCollins. The company acquired Liber, a Swedish publishing company, in 1993. The following year it established its first Eastern European subsidiary, IURA Edition, in Bratislava, Slovakia. The company acquired Jugend & Volk, Dalian, Fateco F rlag and Juristf rlaget, Deutscher Kommunal-Verlag Dr. Naujoks & Behrendt and Colex Data in 1995. Wolters Kluwer was operating in 16 countries and had approximately 8000 employees by the end of that year.



The following year, Wolters Kluwer purchased J. B. Lippincott & Co. from HarperCollins. The company acquired Liber, a Swedish publishing company, in 1993. The following year it established its first Eastern European subsidiary, IURA Edition, in Bratislava, Slovakia. The company acquired Jugend & Volk, Dalian, Fateco F rlag and Juristf rlaget, Deutscher Kommunal-Verlag Dr. Naujoks & Behrendt and Colex Data in 1995. Wolters Kluwer was operating in 16 countries and had approximately 8000 employees by the end of that year.
In 1996, Wolters Kluwer purchased CCH Inc., a tax and business materials publisher, for $1.9 billion. The purchase assisted in expanding the company s business in Asia because of CCH Inc. s involvement in Australia, New Zealand, Japan, Singapore, and Hong Kong. It also purchased Little, Brown and Company s medical and legal division that year. Waverly, Inc., Ovid Technologies, Inc. and Plenum Publishing Corporation were acquired in 1998 with the intention of developing Wolters Kluwer s medical and scientific publishing industry.



The company established its first three-year strategy to deliver sustained value to customers and shareholders in 2003. The New Delhi Wolters Kluwer Health office opened in 2006. In September 2008, Wolters Kluwer acquired UpToDate, an evidence-based electronic clinical information resource. The following month, the company received a multi-year contract to provide prescription and patient-level data to the United States Food and Drug Administration. In 2009, Wolters Kluwer was named the  Best Place to Work  in Spain by the Great Place to Work Institute.
Wolters Kluwer acquired FRSGlobal, financial regulatory reporting and risk management firm in September 2010. The acquisition enabled Wolters Kluwer to provide financial organizations comprehensive compliance and risk solutions. The company acquired SASGAS, a financial reporting software solutions provider, to the foreign and domestic bank market in China in October 2011. That December, Wolters Kluwer acquired Medknow, an open access publisher.
In 2012, Wolters Kluwer acquired Acclipse, an accounting software provider, and Finarch, an integrated finance and risk solutions. The company s health division tested technology to identify and treat sepsis that December. Wolters Kluwer acquired Health Language, a medical terminology management provider, in January 2013. In May 2013, it acquired Prosoft Tecnologia, a Brazilian provider of tax and accounting software. The company acquired CitizenHawk, an American online brand protection and global domain recovery specialist, in September 2013. That month, Wolters Kluwer acquired Svenson, an Austrian regulatory reporting solutions provider. The acquisition enabled both companies to assist Austrian banks and insurance companies in meeting national and international regulatory requirements.
The company became the fifth participant in the AAISalliance, an arrangement of information providers that make their services available for member insurance companies of the American Association of Insurance Services (AAIS) in April 2014. In May 2014, Wolters Kluwer launched UpToDate, a clinical decision support resource, in the United Kingdom. UpToDate was launched throughout western Europe a month later. Wolters Kluwer acquired Datacert, a Houston, Texas-based enterprise legal management software and services provider in April 2014.
The company partnered with Anhembi Morumbi University, a private university in S o Paulo, Brazil, to provide information and resources to healthcare students and professionals in June 2014. That month, the company s CCH eSign solution won the CPA Practice Advisor Magazine's 2014 Tax & Accounting Technology Innovation Award. The solution won the SIIA s  Best Enterprise Mobile Application  award that year. The company partnered with Broadridge Tax Services in August 2014 to facilitate tax reporting and reconciliation. In September, the company s UpToDate resource was released in Latin America. That month the company extended its partnership with the American Internal Revenue Service. 2014 marked the 15th year of their collaboration.



Wolters Kluwer operates under four divisions: Legal & Regulatory, Tax & Accounting, Health and Financial & Compliance Services. The company is active in over 150 countries. Approximately 77% of the company s revenue came from online, software and services in 2013.



Wolters Kluwer is listed on the Dow Jones Sustainability Index. The company received the Bronze Class Sustainability Award 2014 from RobecoSAM. Wolters Kluwer is recognized as one of the  Global 100 Most Sustainable Corporations in the World  by Corporate Knights.




European Business Law Review






Official website
Dutch website
Mary H. Munroe (2004). "Wolters Kluwer Timeline". The Academic Publishing Industry: A Story of Merger and Acquisition   via Northern Illinois University.