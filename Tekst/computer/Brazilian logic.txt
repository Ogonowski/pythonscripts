A paraconsistent logic is a logical system that attempts to deal with contradictions in a discriminating way. Alternatively, paraconsistent logic is the subfield of logic that is concerned with studying and developing paraconsistent (or "inconsistency-tolerant") systems of logic.
Inconsistency-tolerant logics have been discussed since at least 1910 (and arguably much earlier, for example in the writings of Aristotle); however, the term paraconsistent ("beside the consistent") was not coined until 1976, by the Peruvian philosopher Francisco Mir  Quesada.



In classical logic (as well as intuitionistic logic and most other logics), contradictions entail everything. This curious feature, known as the principle of explosion or ex contradictione sequitur quodlibet (Latin, "from a contradiction, anything follows") can be expressed formally as
Which means: if P and its negation  P are both assumed to be true, then P is assumed to be true, from which it follows that at least one of the claims P and some other (arbitrary) claim A is true. However, if we know that either P or A is true, and also that P is not true (that  P is true) we can conclude that A, which could be anything, is true. Thus if a theory contains a single inconsistency, it is trivial that is, it has every sentence as a theorem. The characteristic or defining feature of a paraconsistent logic is that it rejects the principle of explosion. As a result, paraconsistent logics, unlike classical and other logics, can be used to formalize inconsistent but non-trivial theories.



Paraconsistent logics are propositionally weaker than classical logic; that is, they deem fewer propositional inferences valid. The point is that a paraconsistent logic can never be a propositional extension of classical logic, that is, propositionally validate everything that classical logic does. In some sense, then, paraconsistent logic is more conservative or cautious than classical logic. It is due to such conservativeness that paraconsistent languages can be more expressive than their classical counterparts including the hierarchy of metalanguages due to Alfred Tarski et al. According to Solomon Feferman [1984]: " natural language abounds with directly or indirectly self-referential yet apparently harmless expressions all of which are excluded from the Tarskian framework." This expressive limitation can be overcome in paraconsistent logic.



The primary motivation for paraconsistent logic is the conviction that it ought to be possible to reason with inconsistent information in a controlled and discriminating way. The principle of explosion precludes this, and so must be abandoned. In non-paraconsistent logics, there is only one inconsistent theory: the trivial theory that has every sentence as a theorem. Paraconsistent logic makes it possible to distinguish between inconsistent theories and to reason with them.
Research into paraconsistent logic has also led to the establishment of the philosophical school of dialetheism (most notably advocated by Graham Priest), which asserts that true contradictions exist in reality, for example groups of people holding opposing views on various moral issues. Being a dialetheist rationally commits one to some form of paraconsistent logic, on pain of otherwise embracing trivialism, i.e. accepting that all contradictions (and equivalently all statements) are true. However, the study of paraconsistent logics does not necessarily entail a dialetheist viewpoint. For example, one need not commit to either the existence of true theories or true contradictions, but would rather prefer a weaker standard like empirical adequacy, as proposed by Bas van Fraassen.



In classical logic Aristotle's three laws, namely, the excluded middle (p or  p), non-contradiction   (p    p) and identity (p iff p), are regarded as the same, due to the inter-definition of the connectives. Moreover, traditionally contradictoriness (the presence of contradictions in a theory or in a body of knowledge) and triviality (the fact that such a theory entails all possible consequences) are assumed inseparable, granted that negation is available. These views may be philosophically challenged, precisely on the grounds that they fail to distinguish between contradictoriness and other forms of inconsistency.
On the other hand, it is possible to derive triviality from the 'conflict' between consistency and contradictions, once these notions have been properly distinguished. The very notions of consistency and inconsistency may be furthermore internalized at the object language level.



Paraconsistency involves a tradeoff. In particular, abandoning the principle of explosion requires one to abandon at least one of the following three very intuitive principles:
Though each of these principles has been challenged, the most popular approach among logicians is to reject disjunctive syllogism. If one is a dialetheist, it makes perfect sense that disjunctive syllogism should fail. The idea behind this syllogism is that, if   A, then A is excluded, so the only way A   B could be true would be if B were true. However, if A and   A can both be true at the same time, then this reasoning fails.
Another approach is to reject disjunction introduction but keep disjunctive syllogism and transitivity. The disjunction (A   B) is defined as   ( A    B). In this approach all of the rules of natural deduction hold, except for proof by contradiction and disjunction introduction; moreover,  does not mean necessarily that , which is also a difference from natural deduction. Also, the following usual Boolean properties hold: excluded middle and (for conjunction and disjunction) associativity, commutativity, distributivity, De Morgan's laws, and idempotence. Furthermore, by defining the implication (A   B) as   (A    B), there is a Two-Way Deduction Theorem allowing implications to be easily proved. Carl Hewitt favours this approach, claiming that having the usual Boolean properties, Natural Deduction, and Deduction Theorem are huge advantages in software engineering.
Yet another approach is to do both simultaneously. In many systems of relevant logic, as well as linear logic, there are two separate disjunctive connectives. One allows disjunction introduction, and one allows disjunctive syllogism. Of course, this has the disadvantages entailed by separate disjunctive connectives including confusion between them and complexity in relating them.
The three principles below, when taken together, also entail explosion, so at least one must be abandoned:
Both reductio ad absurdum and the rule of weakening have been challenged in this respect, but without much success. Double negation elimination is challenged, but for unrelated reasons. By removing it alone, while upholding the other two one may still be able to prove all negative propositions from a contradiction.



One well-known system of paraconsistent logic is the simple system known as LP ("Logic of Paradox"), first proposed by the Argentinian logician F. G. Asenjo in 1966 and later popularized by Priest and others.
One way of presenting the semantics for LP is to replace the usual functional valuation with a relational one. The binary relation  relates a formula to a truth value:  means that  is true, and  means that  is false. A formula must be assigned at least one truth value, but there is no requirement that it be assigned at most one truth value. The semantic clauses for negation and disjunction are given as follows:

(The other logical connectives are defined in terms of negation and disjunction as usual.) Or to put the same point less symbolically:
not A is true if and only if A is false
not A is false if and only if A is true
A or B is true if and only if A is true or B is true
A or B is false if and only if A is false and B is false
(Semantic) logical consequence is then defined as truth-preservation:
 if and only if  is true whenever every element of  is true.
Now consider a valuation  such that  and  but it is not the case that . It is easy to check that this valuation constitutes a counterexample to both explosion and disjunctive syllogism. However, it is also a counterexample to modus ponens for the material conditional of LP. For this reason, proponents of LP usually advocate expanding the system to include a stronger conditional connective that is not definable in terms of negation and disjunction.
As one can verify, LP preserves most other inference patterns that one would expect to be valid, such as De Morgan's laws and the usual introduction and elimination rules for negation, conjunction, and disjunction. Surprisingly, the logical truths (or tautologies) of LP are precisely those of classical propositional logic. (LP and classical logic differ only in the inferences they deem valid.) Relaxing the requirement that every formula be either true or false yields the weaker paraconsistent logic commonly known as FDE ("First-Degree Entailment"). Unlike LP, FDE contains no logical truths.
It must be emphasized that LP is but one of many paraconsistent logics that have been proposed. It is presented here merely as an illustration of how a paraconsistent logic can work.



One important type of paraconsistent logic is relevance logic. A logic is relevant iff it satisfies the following condition:
if A   B is a theorem, then A and B share a non-logical constant.
It follows that a relevance logic cannot have (p    p)   q as a theorem, and thus (on reasonable assumptions) cannot validate the inference from {p,  p} to q.
Paraconsistent logic has significant overlap with many-valued logic; however, not all paraconsistent logics are many-valued (and, of course, not all many-valued logics are paraconsistent). Dialetheic logics, which are also many-valued, are paraconsistent, but the converse does not hold.
Intuitionistic logic allows A    A not to be equivalent to true, while paraconsistent logic allows A    A not to be equivalent to false. Thus it seems natural to regard paraconsistent logic as the "dual" of intuitionistic logic. However, intuitionistic logic is a specific logical system whereas paraconsistent logic encompasses a large class of systems. Accordingly, the dual notion to paraconsistency is called paracompleteness, and the "dual" of intuitionistic logic (a specific paracomplete logic) is a specific paraconsistent system called anti-intuitionistic or dual-intuitionistic logic (sometimes referred to as Brazilian logic, for historical reasons). The duality between the two systems is best seen within a sequent calculus framework. While in intuitionistic logic the sequent

is not derivable, in dual-intuitionistic logic

is not derivable. Similarly, in intuitionistic logic the sequent

is not derivable, while in dual-intuitionistic logic

is not derivable. Dual-intuitionistic logic contains a connective # known as pseudo-difference which is the dual of intuitionistic implication. Very loosely, A # B can be read as "A but not B". However, # is not truth-functional as one might expect a 'but not' operator to be; similarly, the intuitionistic implication operator cannot be treated like "  (A    B)". Dual-intuitionistic logic also features a basic connective   which is the dual of intuitionistic  : negation may be defined as  A = (  # A)
A full account of the duality between paraconsistent and intuitionistic logic, including an explanation on why dual-intuitionistic and paraconsistent logics do not coincide, can be found in Brunner and Carnielli (2005).



Paraconsistent logic has been applied as a means of managing inconsistency in numerous domains, including:
Semantics. Paraconsistent logic has been proposed as means of providing a simple and intuitive formal account of truth that does not fall prey to paradoxes such as the Liar. However, such systems must also avoid Curry's paradox, which is much more difficult as it does not essentially involve negation.
Set theory and the foundations of mathematics (see paraconsistent mathematics). Some believe that paraconsistent logic has significant ramifications with respect to the significance of Russell's paradox and G del's incompleteness theorems.
Epistemology and belief revision. Paraconsistent logic has been proposed as a means of reasoning with and revising inconsistent theories and belief systems.
Knowledge management and artificial intelligence. Some computer scientists have utilized paraconsistent logic as a means of coping gracefully with inconsistent information.
Deontic logic and metaethics. Paraconsistent logic has been proposed as a means of dealing with ethical and other normative conflicts.
Software engineering. Paraconsistent logic has been proposed as a means for dealing with the pervasive inconsistencies among the documentation, use cases, and code of large software systems.
Electronics design routinely uses a four valued logic, with "hi-impedance (z)" and "don't care (x)" playing similar roles to "don't know" and "both true and false" respectively, in addition to True and False. This logic was developed independently of Philosophical logics.



Some philosophers have argued against dialetheism on the grounds that the counterintuitiveness of giving up any of the three principles above outweighs any counterintuitiveness that the principle of explosion might have.
Others, such as David Lewis, have objected to paraconsistent logic on the ground that it is simply impossible for a statement and its negation to be jointly true. A related objection is that "negation" in paraconsistent logic is not really negation; it is merely a subcontrary-forming operator.



Approaches exist that allow for resolution of inconsistent beliefs without violating any of the intuitive logical principles. Most such systems use multi-valued logic with Bayesian inference and the Dempster-Shafer theory, allowing that no non-tautological belief is completely (100%) irrefutable because it must be based upon incomplete, abstracted, interpreted, likely unconfirmed, potentially uninformed, and possibly incorrect knowledge (of course, this very assumption, if non-tautological, entails its own refutability, if by "refutable" we mean "not completely [100%] irrefutable"). These systems effectively give up several logical principles in practice without rejecting them in theory.



Notable figures in the history and/or modern development of paraconsistent logic include:
Alan Ross Anderson (USA, 1925 1973). One of the founders of relevance logic, a kind of paraconsistent logic.
F. G. Asenjo (Argentina)
Diderik Batens (Belgium)
Nuel Belnap (USA, b. 1930). Worked with Anderson on relevance logic.
Jean-Yves B ziau (France/Switzerland, b. 1965). Has written extensively on the general structural features and philosophical foundations of paraconsistent logics.
Ross Brady (Australia)
Bryson Brown (Canada)
Walter Carnielli (Brazil). The developer of the possible-translations semantics, a new semantics which makes paraconsistent logics applicable and philosophically understood.
Newton da Costa (Brazil, b. 1929). One of the first to develop formal systems of paraconsistent logic.
Itala M. L. D'Ottaviano (Brazil)
J. Michael Dunn (USA). An important figure in relevance logic.
Carl Hewitt
Stanis aw Ja kowski (Poland). One of the first to develop formal systems of paraconsistent logic.
R. E. Jennings (Canada)
David Kellogg Lewis (USA, 1941 2001). Articulate critic of paraconsistent logic.
Jan  ukasiewicz (Poland, 1878 1956)
Robert K. Meyer (USA/Australia)
Chris Mortensen (Australia). Has written extensively on paraconsistent mathematics.
Lorenzo Pe a (Spain, b. 1944). Has developed an original line of paraconsistent logic, gradualistic logic (also known as transitive logic, TL), akin to Fuzzy Logic.
Val Plumwood [formerly Routley] (Australia, b. 1939). Frequent collaborator with Sylvan.
Graham Priest (Australia). Perhaps the most prominent advocate of paraconsistent logic in the world today.
Francisco Mir  Quesada (Peru). Coined the term paraconsistent logic.
B. H. Slater (Australia). Another articulate critic of paraconsistent logic.
Richard Sylvan [formerly Routley] (New Zealand/Australia, 1935 1996). Important figure in relevance logic and a frequent collaborator with Plumwood and Priest.
Nicolai A. Vasiliev (Russia, 1880 1940). First to construct logic tolerant to contradiction (1910).




Deviant logic
Formal logic
Probability logic
Table of logic symbols



^ Priest (2002), p. 288 and  3.3.
^ Carnielli, W. and Marcos, J. (2001) "Ex contradictione non sequitur quodlibet" Proc. 2nd Conf. on Reasoning and Logic (Bucharest, July 2000)
^ Jennifer Fisher (2007). On the Philosophy of Logic. Cengage Learning. pp. 132 134. ISBN 978-0-495-00888-0. 
^ Graham Priest (2007). "Paraconsistency and Dialetheism". In Dov M. Gabbay; John Woods. The Many Valued and Nonmonotonic Turn in Logic. Elsevier. p. 131. ISBN 978-0-444-51623-7. 
^ Ot vio Bueno (2010). "Philosophy of Logic". In Fritz Allhoff. Philosophies of the Sciences: A Guide. John Wiley & Sons. p. 55. ISBN 978-1-4051-9995-7. 
^ See the article on the principle of explosion for more on this.
^ a b c Hewitt (2008b)
^ a b Hewitt (2008a)
^ Priest (2002), p. 306.
^ LP is also commonly presented as a many-valued logic with three truth values (true, false, and both).
^ See, for example, Priest (2002),  5.
^ See Priest (2002), p. 310.
^ Surveys of various approaches to paraconsistent logic can be found in Bremer (2005) and Priest (2002), and a large family of paraconsistent logics is developed in detail in Carnielli, Congilio and Marcos (2007).
^ See Aoyama (2004).
^ Most of these are discussed in Bremer (2005) and Priest (2002).
^ See, for example, Truth maintenance systems or the articles in Bertossi et al. (2004).
^ See Lewis (1982).
^ See Slater (1995), B ziau (2000).



Jean-Yves B ziau, Walter Carnielli and Dov Gabbay, eds. (2007). Handbook of Paraconsistency. London: King's College. ISBN 978-1-904987-73-4. 
Aoyama, Hiroshi (2004). "LK, LJ, Dual Intuitionistic Logic, and Quantum Logic". Notre Dame Journal of Formal Logic 45 (4): 193 213. doi:10.1305/ndjfl/1099238445. 
Bertossi, Leopoldo, eds. (2004). Inconsistency Tolerance. Berlin: Springer. ISBN 3-540-24260-0. 
Brunner, Andreas and Carnielli, Walter (2005). "Anti-intuitionism and paraconsistency". Journal of Applied Logic 3 (1): 161 184. doi:10.1016/j.jal.2004.07.016. 
B ziau, Jean-Yves (2000). "What is Paraconsistent Logic?". In In D. Batens et al. (eds.). Frontiers of Paraconsistent Logic. Baldock: Research Studies Press. pp. 95 111. ISBN 0-86380-253-2. 
Bremer, Manuel (2005). An Introduction to Paraconsistent Logics. Frankfurt: Peter Lang. ISBN 3-631-53413-2. 
Brown, Bryson (2002). "On Paraconsistency". In In Dale Jacquette (ed.). A Companion to Philosophical Logic. Malden, Massachusetts: Blackwell Publishers. pp. 628 650. ISBN 0-631-21671-5. 
Carnielli, Walter; Coniglio, Marcelo E.; Marcos, J (2007). "Logics of Formal Inconsistency". In D. Gabbay; F. Guenthner. Handbook of Philosophical Logic, Volume 14 (2nd ed.). The Netherlands: Kluwer Academic Publishers. pp. 1 93. ISBN 1-4020-6323-7. 
Feferman, Solomon (1984). "Toward Useful Type-Free Theories, I". The Journal of Symbolic Logic 49 (1): 75 111. doi:10.2307/2274093. 
Hewitt, Carl (2008a). "Large-scale Organizational Computing requires Unstratified Reflection and Strong Paraconsistency". In Jaime Sichman, Pablo Noriega, Julian Padget and Sascha Ossowski (ed.). Coordination, Organizations, Institutions, and Norms in Agent Systems III. Lecture Notes in Computer Science 4780. Springer-Verlag. doi:10.1007/978-3-540-79003-7. 
Hewitt, Carl (2008b). "Common sense for concurrency and inconsistency tolerance using Direct LogicTM and the Actor model". arXiv:0812.4852 [cs.LO]. 
Lewis, David (1998) [1982]. "Logic for Equivocators". Papers in Philosophical Logic. Cambridge: Cambridge University Press. pp. 97 110. ISBN 0-521-58788-3. 
Pe a, Lorenzo (1996) [1996]. "Graham Priest's 'Dialetheism': Is it altogether true?". Sorites 7: 28 56. hdl:10261/9714. Retrieved 2009-05-03. 
Priest, Graham (2002). "Paraconsistent Logic.". In In D. Gabbay and F. Guenthner (eds.). Handbook of Philosophical Logic, Volume 6 (2nd ed.). The Netherlands: Kluwer Academic Publishers. pp. 287 393. ISBN 1-4020-0583-0. 
Priest, Graham and Tanaka, Koji (2009) [1996]. "Paraconsistent Logic". Stanford Encyclopedia of Philosophy. Retrieved June 17, 2010.  (First published Tue Sep 24, 1996; substantive revision Fri Mar 20, 2009)
Slater, B. H. (1995). "Paraconsistent Logics?". Journal of Philosophical Logic 24 (4): 451 454. doi:10.1007/BF01048355. 
Woods, John (2003). Paradox and Paraconsistency: Conflict Resolution in the Abstract Sciences. Cambridge: Cambridge University Press. ISBN 0-521-00934-0. 



"Paraconsistent Logic" entry in the Internet Encyclopedia of Philosophy
Stanford Encyclopedia of Philosophy "Paraconsistent Logic"
Stanford Encyclopedia of Philosophy "Inconsistent Mathematics"
"World Congress on Paraconsistency, Ghent 1997, Juquehy 2000, Toulouse, 2003, Melbourne 2008, Kolkata, 2014"
Paraconsistent First-Order Logic with infinite hierarchy levels of contradiction LP#. Axiomatical system HST#, as paraconsistent generalization of Hrbacek set theory HST