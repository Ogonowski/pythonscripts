Othonoi (Greek:  , Italian: Fan ) is an island and a former community of the Ionian Islands, Greece. Since the 2011 local government reform it is part of the municipality of Corfu, of which it is a municipal unit. It is the westernmost point of Greece, located northwest of Corfu. Population 392 (2011). In the 19th century the island used to be the capital of the Diapontia Islands municipality, which also included nearby islands of Ereikoussa and Mathraki. Most of the island's inhabitants can trace their origins to Paxi island, south of Corfu, as people from Paxi were settled in the island beginning in the 1570s by the Venetian Republic. A cave, near the Aspri Ammos (White Sand) beach, is traditionally believed to have been the place where Calypso kept Odysseus captive.



Othonoi is mentioned from antiquity under the name Othronos. The current name is corruption of ancient name. The ancient Greeks were also believed that Othonoi was the legendary island of Calypso. A big sea cave of the island is today called Calypso's cave. During the medieval period the island possibly was depopulated. During the famous Battle of Lepanto, nobody resident is mentioned in the island. Later, people from Epirus settled in the island and from there some of them settled and in the other Diapontia Islands, Ereikoussa and Mathraki. The island became part of Greece in 1864, after concession of Ionian Islands to Greece.





