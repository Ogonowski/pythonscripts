Lithuania is a member of the European Union and the largest economy among the three Baltic states. Lithuania belongs to the group of very high human development countries.
Lithuania was the first country to declare independence from Soviet Union in 1990 and rapidly moved from centrally planned to a market economy, implementing numerous liberal reforms. It enjoyed high growth rates after joining the European Union along with the other Baltic states, leading to the notion of a Baltic Tiger.
GDP growth reached its peak in 2007, increasing by 11.1%, and still growing slightly in 2008. Similar to the other Baltic States, the Lithuanian economy suffered a deep recession in 2009, with GDP falling by almost 15%. GDP growth has resumed in 2010, albeit at a slower pace than before the crisis.
GDP per capita in Lithuania is 70% above the world's average of $10,500. Lithuania is ranked 24th in the world in the Ease of Doing Business Index prepared by the World Bank Group and 15th out of 178 countries in the Index of Economic Freedom, measured by The Heritage Foundation.



The history of Lithuania can be divided into seven major periods. All the periods have some interesting and important facts that had an impact on the economic situation of the country in those times.
Ancient times   Baltic tribes (until the 13th century)
Grand Duchy of Lithuania (13th century 1569)
Polish-Lithuanian Commonwealth (1569 1795)
Pressure of the Russian Empire (1795 1914)
Independent Lithuania during the interwar period (1918 1940)
Lithuanian Soviet Socialist Republic (1944 1990)
Republic of Lithuania (1990 )



The first Lithuanians were a branch of an ancient ethnic group known as the Balts. The tribes maintained close trade contacts with the Roman Empire. Amber was the main good provided to the Roman Empire from Baltic Sea coast, by a long route called the Amber Road.
Consolidation of the Lithuanian lands began in the late 12th century. Mindaugas, the first Lithuanian ruler, was crowned as Catholic King of Lithuania in 1253. The expansion of the Grand Duchy of Lithuania reached its height in the middle of the 14th century under the Grand Duke Gediminas, who created a strong central government which later spread from the Black Sea to the Baltic Sea. The duchy was open to everyone. Grand Duke Gediminas issued letters to the Hanseatic league, offering free access to his domains to men of every order and profession from nobles and knights to tillers of the soil. Economic immigrants had a positive impact, improving the level of handicrafts.
In 1569, the Polish Lithuanian Commonwealth was formed by the union of the Kingdom of Poland and the Grand Duchy of Lithuania. The economy of the Commonwealth was dominated by feudal agriculture based on the exploitation of the agricultural workforce (serfs). Poland-Lithuania played a significant role in the supply of 16th century Western Europe by exporting three sorts of goods, notably grain (rye), cattle (oxen) and fur. These three articles amounted to nearly 90% of the country's exports to western markets by overland and maritime trade.
The Commonwealth was famous for Europe's first and the world's second modern codified national constitution, the so-called Constitution of 3 May, declared on 3 May 1791, after the 1788 ratification of the United States Constitution. Economic and commercial reforms, previously shunned as unimportant by the Szlachta, were introduced, and the development of industries was encouraged.
Following the partitions of the Polish-Lithuanian Commonwealth in 1772, 1793 and 1795, the Russian Empire controlled the majority of Lithuania. One of the most important reforms during the pressure of the Russian Empire that affected economic relations was the Emancipation Reform of 1861 in Russia. The reform amounted to the liquidation of serf dependence previously suffered by peasants and boosted the development of capitalism.



On 16 February 1918, the Council of Lithuania passed a resolution for the re-establishment of the Independent State of Lithuania. Soon, many economic reforms for sustainable economic growth were implemented. A national currency, called the Lithuanian litas, was introduced in 1922. It proved to become one of the most stable currencies in Europe during the inter-war period. During the time of its independence, 1918 1940, Lithuania made substantial progress. For example, Lithuania was the second in the world in exporting flax; Lithuanian farm products such as meat, dairy products, many kinds of grain, potatoes, etc. were of superior quality in the world market.
Having taken advantage of favorable international developments, and driven by its foreign policy aims directed against Lithuanian statehood, the Union of Soviet Socialist Republics (USSR) occupied Lithuania in 1940. Land and the most important objects for the economy were nationalized, and most of the farms collectivized. Later, many inefficient factories and industry companies, highly dependent on other regions of USSR, were established in Lithuania. Despite that, in 1990, GDP per capita of the Lithuanian Soviet Socialist Republic was $8,591, which was above the average for the rest of the Soviet Union of $6,871 but lagging behind developed western countries.
The Soviet era brought Lithuania intensive industrialization and economic integration into the USSR, although the level of technology and state concern for environmental, health, and labor issues lagged far behind Western standards. Urbanization increased from 39% in 1959 to 68% in 1989. From 1949 to 1952 the Soviets abolished private ownership in agriculture, establishing collective and state farms. Production declined and did not reach pre-war levels until the early 1960s. The intensification of agricultural production through intense chemical use and mechanization eventually doubled production but created additional ecological problems. This changed after independence, when farm production dropped due to difficulties in restructuring the agricultural sector.




Reforms since the mid-1990s led to an open and rapidly growing economy. Open to global trade and investment, Lithuania now enjoys high degrees of business, fiscal, and financial freedom. Lithuania is a member of the EU and the WTO, so regulation is relatively transparent and efficient, with foreign and domestic capital subject to the same rules. The financial sector is advanced, regionally integrated, and subject to few intrusive regulations.
One of Lithuania's most important reforms was the privatization of state-owned assets. The first stage of privatization was being implemented between 1991 and 1995. Citizens were given investment vouchers worth  3.1 billion in nominal value, which let them participate in assets selling. By October 1995, they were used as follows: 65% for acquisition of shares; 19% for residential dwellings; 5% for agricultural properties; and 7% remained unused. More than 5,700 enterprises with  2.0 billion worth of state capital in book value were sold using four initial privatization methods: share offerings; auctions; best business plans competitions; and hard currency sales.
The second privatization step began in 1995 by approving a new law that ensured greater diversity of privatization methods and that enabled participation in the selling process without vouchers. Between 1996 and 1998, 526 entities were sold for more than  0.7 billion. Before the reforms, the public sector totally dominated the economy, whereas the share of the private sector in GDP increased to over 70% by the 2000 and 80% in 2011.
Monetary reform was undertaken in early nineties to improve the stability of the economy. Lithuania chose a currency board system controlled by the Bank of Lithuania independent of any government institution. On 25 June 1993, the Lithuanian litas was introduced as a freely convertible currency, but on 1 April 1994 it was pegged to the United States dollar at a rate of 4 to 1. The mechanism of the currency board system enabled Lithuania to stabilize inflation rates to single digits. The stable currency rate helped to establish foreign economic relations, therefore leading to a constant growth of foreign trade.
By 1998, the economy had survived the early years of uncertainty and several setbacks, including a banking crisis. However, the collapse of the Russian ruble in August 1998 shocked the economy into negative growth and forced the reorientation of trade from Russia towards the West.

Lithuania was invited to the Helsinki EU summit in December 1999 to begin EU accession talks in early 2000.
After the Russian financial crisis, the focus of Lithuania's export markets shifted from East to West. In 1997, exports to the Soviet Union's successor entity (the Commonwealth of Independent States) made up 45% of total Lithuanian exports. This share of exports dropped to 21% of the total in 2006, while exports to EU members increased to 63% of the total. Exports to the United States made up 4.3% of all Lithuania's exports in 2006, and imports from the United States comprised 2% of total imports. Foreign direct investment (FDI) in 2005 was  0.8 billion.
On 2 February 2002 the litas was pegged to the euro at a rate of 3.4528 to 1, which remained until Lithuania adopted the euro in 2015. Lithuania was very close to introducing the euro in 2007, but the inflation level exceeded the Maastricht requirements. On January 1, 2015, Lithuania became the 19th country to use the euro.
The Vilnius Stock Exchange, now renamed the NASDAQ OMX Vilnius, started its activity in 1993 and was the first stock exchange in the Baltic states. In 2003, the VSE was acquired by OMX. Since 27 February 2008 the Vilnius Stock Exchange has been a member of NASDAQ OMX Group, which is the world's largest exchange company across six continents, with over 3,800 listed companies. The market cap of Vilnius Stock Exchange was  3.4 billion on 27 November 2009.
During the last decade (1998 2008) the structure of Lithuania's economy has changed significantly. The biggest changes were recorded in the agricultural sector as the share of total employment decreased from 19.2% in 1998 to just 7.9% in 2008. The service sector plays an increasingly important role. The share of GDP in financial intermediation and real estate sectors was 17% in 2008 compared to 11% in 1998. The share of total employment in the financial sector in 2008 has doubled compared with 1998.




The economy of Lithuania was one of the fastest growing in the world during the decade until 2008. Between 2000 and 2008, the Lithuanian GDP grew by 77%.
One of the most important factors contributing to Lithuania's economic growth was its accession to the WTO in 2001 and the EU in 2004, which allows free movement of labour, capital, and trade among EU member states. On the other hand, rapid growth caused some imbalances in inflation and balance of payments. The current account deficit to GDP ratio in 2006 2008 was in the double digits and reached its peak in the first quarter of 2008 at a threatening 18.8%. This was mostly due to rapid loan portfolio growth as Scandinavian banks provided cheap credit in Lithuania. The loans directly related to acquisition and development of real estate constituted around half of outstanding bank loans to the private sector. Consumption was affected by credit expansion as well. This led to high inflation of goods and services, as well as trade deficit.
The global credit crunch which started in 2008 affected the real estate and retail sectors. The construction sector shrank by 46.8% during the first three quarters of 2009 and the slump in retail trade was almost 30%. GDP plunged by 15.7% in the first nine months of 2009.
Lithuania was the last among the Baltic states to be hit by recession because its GDP growth rate in 2008 was still positive, followed by a slump of more than 15% in 2009. In the third quarter of 2009, compared to the previous quarter, GDP again grew by 6.1% after five-quarters with negative numbers.
A heavy shock to consumers helped to balance the current account in 2009. Net external assets of the Bank of Lithuania are at a record height of  5.5 billion. Economic sentiment and confidence of all business activities have rebounded from a record low at the beginning of the year 2009.
Sectors related to domestic consumption and real estate still suffer from the economic crisis, but exporters have started making profits even with lower levels of revenue. The catalysts of growing profit margins are lower raw material prices and staff expense.



On January 1, 2015, Lithuania became the 19th country to adopt the euro. Despite concerns about joining the Eurozone due to the European debt crisis and changeover costs are estimated at  600 million through 2020 (in contributions to the EU budget and costs to commercial lenders), Lithuania stands to benefit from the euro politically and economically because Lithuania has not had an independent monetary policy for the past twenty years because of its currency pegs. Joining the euro would relieve the Bank of Lithuania of defending the value of the litas, and "it would give Lithuania a say in the decision-making of the European Central Bank (ECB), as well as access to the ECB single-resolution fund and cheaper borrowing costs".
Lithuanians have a "marked preference for using cash as opposed to bank cards", which authorities hope will be diminished with the euro's adoption, which would negatively affect its rather large informal economy. According to ECB data, Lithuanians "take twice as much money out in cash as do Estonians or Latvians, and that this cash is mostly used as 'under the counter' money, as interest rates for time deposits in commercial banks are very low".



Lithuania attracts foreign investors because of its low tax, skilled workforce, reliable infrastructure and rule of law. Cumulative foreign direct investment (FDI) in 2009 was  9.2 billion. The manufacturing sector constituted 28% of total FDI, real estate and business activity sector received 20% of total FDI, and financial intermediation received 19%. Four-fifths of FDI came from the EU countries, with Sweden (17% of total FDI) at the top followed by Germany (10%) and Denmark (9%).

Lithuania seeks to become an innovation hub by 2020. To reach this goal, it is putting its efforts into attracting FDI to added-value sectors, especially IT services, software development, consulting, finance, and logistics. Well-known international companies such as Microsoft, IBM, Transcom, Barclays, Siemens, SEB, TeliaSonera, Paroc, Philip Morris established a presence in Lithuania.
Lithuania has created an attractive business environment for startups, especially with its two free economic zones (FEZ) in Kaunas and Klaip da. The FEZs offer developed infrastructure, service support, and tax incentives; specifically, a company set up in an FEZ is exempt from corporate taxation for its first six years, as well as a tax on dividends and real estate tax. There are nine industrial sites in Lithuania, which can also provide additional advantages by having a well-developed infrastructure, offering consultancy service and tax incentives.
On the other hand, regulatory red tape and corruption have been cited as dampening the investment climate in Lithuania, particularly affecting small and medium enterprises.




The number of the population aged 15 years and over is 2.85 million, and 1.52 million of them were employed in 2008. The population with higher education was 0.54 million, or more than 35% of employed people. This ratio demonstrates that workforce in Lithuania is one of the best-educated in Central and Eastern Europe (CEE) and is twice the EU-15 average. About 90% of Lithuanians speak at least one foreign language, half speak two foreign languages, and a third speak English.
Lithuania takes the first position in the EU by the number of students in the country. Compared to the EU's average of 15%, only 7% of 18-to-24 year olds in Lithuania are not occupied with studies, the lowest percentage in the EU, announced the European Commission in the end of 2009. School-leavers can choose from 22 universities or 28 colleges for further studies, so 74% of pupils graduated from an upper secondary school continue studies in schools of higher education. Every year more than 30,000 students graduate from universities or colleges, so the population with higher education is gradually increasing.
During the last decade (1998 2008) salaries have more than doubled in Lithuania. Despite this, labour costs in Lithuania are among the lowest in the EU. Average monthly net salary in the third quarter of 2009 was  482 and decreased by 6% compared to the same quarter in 2008. The sharpest annual decrease in hourly labour costs in the EU of  10.9% was observed in Lithuania in the third quarter of 2009. Although Lithuania s cheap labor helps to make its exports competitive, real wages have grown by 5% in 2014 (compared to 1% growth in the Eurozone), due in part to the 25% minimum wage increase.
Unemployment in Lithuania has been volatile. Since the year 2001, the unemployment rate has decreased from almost 20% to less than 4% in 2007 thanks to two main reasons. Firstly, during the time of rapid economic expansion, numerous work places were established. This caused a decrease in the unemployment rate and a rise in staff expenses. Secondly, emigration has also reduced unemployment problems since accession to the EU. However, the current economic crisis has lowered the need for workers, so the unemployment rate increased to 13.8% and then stabilized in the third quarter of 2009.






The service sector accounts for the largest share of GDP. One of the most important sub-sectors is information and communication technologies (ICT). Around 37,000 employees work for more than 2,000 ICT companies. ICT received 9.5% of total FDI. 11 out of 20 biggest IT companies from Baltic countries are based in Lithuania. Lithuania exported 31% of its IT services in the first quarter of 2009.
Development of shared services and business process outsourcing are some of the most promising fields. Research company Datamonitor forecasted a 60% personnel growth by 2009. Companies that have outsourced their business operations to Lithuania include Barclays, CITCO Group, MIRROR, PricewaterhouseCoopers, Anthill, and Ernst & Young.



Manufacturing constitutes the biggest part of gross value added in Lithuania. More than 57,000 people were employed in food processing in 2008. The food processing sector constitutes 11% of total exports. Dairy products, especially cheese, are well known in neighbouring countries. Another important manufacturing activity is chemical products. 80% of production is exported so chemical products constitute 12.5% of total exports.
Furniture production employs more than 50,000 people and has seen double-digit growth over the last three years. The biggest companies in this field work in cooperation with IKEA, which owns one of the biggest wood processing companies in Lithuania.
Companies in the automotive and engineering sector are relatively small but offer flexible services for small and non-standard orders at competitive prices. The sector employs about 3% of the working population and receives 5.6% of FDI. Vilnius Gediminas Technical University prepares experts for the sector.
Lithuania has over 50% of the world's market for high-energy picosecond lasers, and is a global leader in producing ultra-fast parametric light generators. Lithuanian laser companies were among the first ones in the world to transfer fundamental research into manufacturing. Lithuania's laser producers export laser technologies and devices to nearly 100 countries.




The financial sector concentrates mostly on the domestic market. There are nine commercial banks that hold a license from the Bank of Lithuania and eight foreign bank branches. Most of the banks belong to international corporations, mainly Scandinavian. The financial sector has demonstrated incredible growth in the last decade (1998 2008). Bank assets were only  3.2 billion or 25.5% from GDP in 2000, half of which consisted of loan portfolio.
By the beginning of the year 2009, bank assets grew to  26.0 billion or 80.8% to GDP, the loan portfolio reached  20.7 billion. The loan-to-GDP ratio was 64%. The growth of deposits was not as fast as that of loans. At the end of 2008, the loan portfolio was almost twice as big as that of deposits. It demonstrated high dependence on external financing. Contraction in the loan portfolio has been recorded over the past year, so the loans to deposits ratio are slowly getting back to healthy levels.



Tourism is becoming increasingly important for Lithuania's economy, constituting around 3% of GDP in 2008. Having an untouched ecological countryside with rich natural resources (22,000 rivers and rivulets and 3,000 lakes), a well-developed rural tourism network, a unique coastal area of almost 100 km and four UNESCO World Heritage sites, Lithuania receives more than 2.2 million foreign tourists a year. Poland, Russia, Latvia, and Belarus supply the most tourists, and a significant number arrive from Germany, the UK, Finland, and Italy as well.



Despite a decreased share in GDP, the agricultural sector is still important for Lithuania as it employs almost 8% of the work force and supplies materials for the food processing sector. 44.8% of the land is arable. Total crop area was 1.8 million hectares in 2008. Cereals, wheat, and triticale are the most popular production of farms. The number of livestock and poultry has decreased twofold compared to the 1990s. The number of cattle in Lithuania at the beginning of the year 2009 was 770,000, the number of dairy cows was 395,000, and the number of poultry was 9.1 million.
Lithuanian food consumption has evolved; between 1992 and 2008, consumption of vegetables increased by 30% to 86 kg per capita, and consumption of meat and its products increased by 23% during the same period to 81 kg per capita. On the other hand, consumption of milk and dairy products has decreased to 268 kg per capita by 21%, and the consumption of bread and grain products decreased to 114 kg per capita by 19% as well.




Lithuania is divided into ten counties. There are five cities with a population over 100,000 and twelve cities of over 30,000 people. The gross regional product is concentrated in the three largest counties   Vilnius, Kaunas, and Klaip da. These three counties account for 70% of the GDP with just 59% of the population. Service centers and industry are concentrated there. In five counties (those of Alytus, Marijampole, Panev ys,  iauliai and Taurag ), GDP per capita is still below 80% of the national average.
In order to achieve balanced regional distribution of GDP, nine public industrial parks (Akmene Industrial Park, Alytus Industrial Park, Kedainiai Industrial Park, Marijampole Industrial Park, Pagegiai Industrial Park, Panevzys Industrial Park, Radviliskis Industrial Park, Ramygala Industrial Park and  iauliai Industrial Park) and three private industrial parks (Taurag  Private Industrial Park, Sitkunai Private Industrial Park, Ramu iai Private Logistic and Industrial Park) were established to provide some tax incentives and prepared physical infrastructure.



The transport, storage, and communication sector has increased its importance to the economy of Lithuania. In 2008, it accounted for 12.1% of GDP compared to 9.1% in 1996.




Lithuania has a broadly developed radio, television, landline and mobile phone, as well as broadband internet networks.
Lithuanian National Radio and Television, the public broadcaster in Lithuania operates 3 television channels, including a satellite channel, as well as 3 radio stations. Privately owned commercial TV and Radio broadcasters operate a multitude national, regional and local channels.
The fixed landline network connects 625 thousand households and businesses (down from the record 845 thousand in 2005). The decline in subscription and utilization of the landline network has been driven by increased availability of mobile phone services. The mobile telephony penetration rate in Lithuania (of 151 per 100 population in 2013) has been one of the highest in the world. In 2013, there were 13 providers of mobile phone services, with the three largest ones - BIT  Lietuva, Omnitel, and Tele2 - operating their own cellular networks.
Lithuanian retail internet sector is competitive, with more than 100 service providers. Retail internet connectivity in Lithuania was among the cheapest in Europe; however, the internet penetration rate (64% of households using internet in 2013) was lower than in other EU countries in the region - Estonia (79%), Latvia (70%) and Poland (69%). Lithuanian internet connection speeds have been claimed to be among the fastest in the world based on user-initiated tests at Speedtest.net.




The utilities sector accounts for more than 3% of gross value added in Lithuania. Electricity production exceeded 12 billion kWh in 2007, and consumption exceeded 9.6 billion kWh. Surplus electricity is exported.
Lithuania operated a nuclear power plant in Visaginas, which produced 72% of electricity in Lithuania. The plant was shut down on 31 December 2009 in line with the commitments made when Lithuania joined EU in 2004. New nuclear power plant in Visaginas has been proposed but the status of the project is uncertain after it was rejected by the voters in a referendum in 2012.
The supply of heating energy has been modernized during the last decade (1998 2008). Technological loss in the heat energy system has decreased significantly from 26.2% in the year 2000 to 16.7% in 2008. The amount of air pollution was reduced by one-third. The share of renewable energy resources in the total fuel balance for heat production increased to almost 20%.




Lithuania forms part of the transport corridor between the East and the West. The volume of goods transported by road transport has increased fivefold since 1996. The total length of roadways is more than 80,000 km, and 90% of them are paved. The government spending on road infrastructure exceeded  0.5 billion in 2008. Via Baltica highway passes through Kaunas, while membership in the Schengen Agreement allows for smooth border crossing to Poland and Latvia.
Railway transport in Lithuania provides long-distance passenger and cargo services. Railways carry approximately 50 million tons of cargo and 7 million passengers a year. Direct rail routes link Lithuania with Russia, Belarus, Latvia, Poland, and Germany. Also, the main transit route between Russia and Russia's Kaliningrad Region passes through Lithuania. JSC Lithuanian Railways transports about 44% of the freight carried through Lithuania. This is a very high indicator compared to other EU countries, where freight transportation by rail amounts to only 10% of the total.

An ice-free seaport of Klaipeda is located in the western part of Lithuania. The port is an important regional transport hub connecting the sea, land and railway routes from east and west. It handles roughly 7,000 ships and 30 million tons of cargo every year, and accepts large-tonnage vessels (dry-cargo vessels up to 70,000 DWT, tankers up to 100,000 DWT and cruise ships up to 270 meters long). The seaport of Klaip da is able to receive Panamax-type vessels. One of the fastest growing segments of sea transport is passenger traffic, which has increased fourfold since 2002.
There are a few commercial airports; scheduled international services use the facilities at Vilnius, Kaunas, and Palanga.



There are more than 600,000 m2 of modern logistics and warehousing facilities in Lithuania. The biggest supply of new, modern warehousing facilities is in the capital city Vilnius (after the completion of several new projects in the third quarter of 2009, the supply of modern warehousing premises has increased by nearly 12% in Vilnius and currently reaches 334,400 m2 of the rentable area). Kaunas is in the second place (around 200,000 m2), and Klaip da in the third (122,500 m2). Since the beginning of the year 2009, prices for warehousing premises have dropped by 20 25% in Vilnius, Kaunas, and Klaip da, and the current level of rents has reached the level of 2003. The costs for renting new warehouses in Vilnius, Kaunas, and Klaip da are similar and reach 0.75 to 1.42 EUR/m2, while the rents of old warehouses are 0.35 to 0.67 EUR/m2.




Lithuanian economy is highly open and International trade is crucial. As a result, the ratio of foreign trade to GDP for Lithuania has often exceeded 100%.
The EU is the biggest trade partner of Lithuania with a 64% of total imports and 55% of total exports during 2013. The Commonwealth of Independent States is the second economic union that Lithuania trades the most with, with a share of imports of 28% and a share of exports of 32% during the same period. The vast majority of commodities, including oil, gas, and metals have to be imported, mainly from Russia. For this reason, Russia is the biggest import partner of Lithuania.
Mineral products constitute 25% of imports and 18% of exports, mainly driven by the presence of ORLEN Lietuva oil refinery with a refining capacity of 9 million tons a year, owned by Polish concern PKN Orlen. Orlen Lietuva sold over  3.5 billion worth of products outside Lithuania, compared to the total Lithuanian exports of  24 billion in 2014.
Some sectors are directed mainly at export markets. Transport and logistics export 2/3 of their products and/or services; the biotechnology industry exports 80%; plastics export 52%; laser technologies export 86%; metal processing, machinery and electric equipment export 64%; furniture and wood processing export 55%; textile and clothing export 76%; and the food industry exports 36%.



The total value of natural resources in Lithuania reaches  16.8 billion or 50% of Lithuania's GDP in 2008. The most valuable natural resource in Lithuania is subterranean water, which constitutes more than a half of the total value of natural resources.



Lithuania's Department of Statistics
Public Organization "Invest Lithuania"
Investing in Lithuania
Economy of Europe
Lithuania




 This article incorporates public domain material from the United States Department of State document "Background Note: Lithuania" by Bureau of European and Eurasian Affairs (retrieved on 2009-10-17).