AT&T Inc. is an American multinational telecommunications corporation, headquartered at Whitacre Tower in downtown Dallas, Texas. AT&T is the second largest provider of mobile telephone and the largest provider of fixed telephone in the United States, and also provides broadband subscription television services. AT&T is the third-largest company in Texas (the largest non-oil company, behind only ExxonMobil and ConocoPhillips, and also the largest Dallas company). As of May 2014, AT&T is the 23rd-largest company in the world as measured by a composite of revenues, profits, assets and market value, and the 16th-largest non-oil company. As of 2015, it is also the 20th-largest mobile telecom operator in the world, with over 123.9 million mobile customers.
AT&T Inc. began its existence as Southwestern Bell Corporation, one of seven Regional Bell Operating Companies (RBOC's) created in 1983 in the divestiture of the American Telephone and Telegraph Company (founded 1885, later AT&T Corp.) following the 1982 United States v. AT&T antitrust lawsuit. Southwestern Bell changed its name to SBC Communications Inc. in 1995. In 2005, SBC purchased former parent AT&T Corp. and took on its branding, with the merged entity naming itself AT&T Inc. and using the iconic AT&T Corp. logo and stock-trading symbol.
The current AT&T reconstitutes much of the former Bell System and includes ten of the original 22 Bell Operating Companies, along with the original long distance division.
As of July 24, 2015, AT&T became the largest pay-TV provider with AT&T U-verse and DirecTV combined. Comcast fell to second place with 22 million after AT&T completed its acquisition of DirecTV.
On August 12, 2015, AT&T announced that it will cease investing in its U-verse platform and create a new in-home TV product that will display content from DirecTV.
On September 10, 2015, AT&T selected Ericsson to integrate both U-verse TV and DirecTV pay-TV services into one.
On September 16, 2015, AT&T hired former Sirius XM, Cisco Systems and Microsoft executive VP Enrique Rodriguez to combine both the U-verse TV and DirecTV pay-TV services.







AT&T can trace its origin back to the original Bell Telephone Company founded by Alexander Graham Bell after his invention of the telephone. One of that company's subsidiaries was American Telephone and Telegraph Company (AT&T), established in 1885, which acquired the Bell Company on December 31, 1899 for legal reasons, leaving AT&T as the main company. AT&T established a network of subsidiaries in the United States and Canada that held a government-authorized phone service monopoly, formalized with the Kingsbury Commitment, throughout most of the twentieth century. This monopoly was known as the Bell System, and during this period, AT&T was also known by the nickname Ma Bell. For periods of time, the former AT&T was the world's largest phone company.
In 1982, US regulators broke up the AT&T monopoly, requiring AT&T to divest its regional subsidiaries and turning them each into individual companies. These new companies were known as Regional Bell Operating Companies, or more informally, Baby Bells. AT&T continued to operate long distance services, but as a result of this breakup, faced competition from new competitors such as MCI and Sprint.
Southwestern Bell was one of the companies created by the breakup of AT&T. The company soon started a series of acquisitions. This includes the 1987 acquisition of Metromedia mobile business, and the acquisition of several cable companies in the early 1990s. In the later half of the 1990s, the company acquired several other telecommunications companies, including some Baby Bells, while selling its cable business. During this time, the company changed its name to SBC Communications. By 1998, the company was in the top 15 of the Fortune 500, and by 1999 the company was part of the Dow Jones Industrial Average (lasting through 2015).



In 2005, SBC purchased AT&T for $16 billion. After this purchase, SBC adopted the AT&T name and brand. The original 1885 AT&T still exists as the long-distance phone subsidiary of this company.
In September 2013, AT&T announced it would expand into Latin America through a collaboration with Carlos Slim's Am rica M vil. On December 17, 2013, AT&T announced plans to sell its Connecticut wireline operations to Stamford-based Frontier Communications. Roughly 2,700 wireline employees supporting AT&T s operations in Connecticut will transfer with the business to Frontier, as well as 900,000 voice connections, 415,000 broadband connections, and 180,000 U-verse video subscribers.
On May 18, 2014, AT&T announced it had agreed to purchase DirecTV. In the deal, which has been approved by boards of both companies, DirecTV stockholders will receive $95 a share in cash and stock, valuing the deal at $48.5 billion. Including assumed debt, the total purchase price is about $67.1 billion. The deal was aimed at increasing AT&T's market share in the pay-TV sector; its existing U-Verse brand has modest market share (5.7 million users compared to DirecTV's 20 million US customers as of 2014) and operates in only 22 states. It will also give AT&T access to fast-growing Latin American markets, where DirecTV has 18 million subscribers. Additionally, the purchase will allow the AT&T to offer TV through both fiber-optic lines and satellites by maintaining the DirecTV brand as a separate subsidiary, and give the company greater flexibility in creating TV/phone/Internet bundles. The deal was subject to regulatory approval by the U.S. Federal Communications Commission (FCC), the U.S. Department of Justice, and some Latin American governments.
The merger was approved by the FCC on July 24, 2015. The deal is subject to conditions for four years, including a requirement for AT&T to expand its fiberoptic broadband service to at least 12.5 million customer locations, not to discriminate against other online video services using bandwidth caps, submit any "interconnection agreements" for government review, and offer low-cost internet services for low-income households.
It was announced on November 7, 2014, that Mexican carrier Iusacell would be acquired by AT&T to create a wider North American network.
In January 2015, AT&T announced it would be acquiring the bankrupt Mexican wireless business of NII Holdings for around $1.875 billion.
On May 14, 2015, AT&T announced that it struck a deal with Hulu that would give its customers access to the streaming service on both regular and premium tiers.



According to the Center for Responsive Politics, AT&T is the second-largest donor to United States political campaigns, and the top American corporate donor, having contributed more than US$47.7 million since 1990, 56% and 44% of which went to Republican and Democratic recipients, respectively. Also, during the period of 1998 to 2010, the company expended US$130 million on lobbying in the United States. A key political issue for AT&T has been the question of which businesses win the right to profit by providing broadband internet access in the United States.
In 2005, AT&T was among 53 entities that contributed the maximum of $250,000 to the second inauguration of President George W. Bush.



Bill Leahy, representing AT&T, sits on the Private Enterprise Board of the American Legislative Exchange Council (ALEC).



AT&T supported the Federal Communications Commission Process Reform Act of 2013 (H.R. 3675; 113th Congress), a bill that would make a number of changes to procedures that the U.S. Federal Communications Commission (FCC) follows in its rulemaking processes. The FCC would have to act in a more transparent way as a result of this bill, forced to accept public input about regulations. Executive Vice President - Federal Relations Tim McKone said that the bill's "much needed institutional reforms will help arm the agency with the tools to keep pace with the Internet speed of today's marketplace. It will also ensure that outmoded regulatory practices for today's competitive marketplace are properly placed in the dustbin of history."



Of the twenty-four companies that were part of the Bell System, ten are a part of the current AT&T:
BellSouth Telecommunications (formerly known as Southern Bell; includes former South Central Bell)
Illinois Bell
Indiana Bell
Michigan Bell
Ohio Bell
Pacific Bell (formerly Pacific Telephone & Telegraph)
Nevada Bell (formerly known as Bell Telephone Company of Nevada)

Southwestern Bell
Wisconsin Bell (formerly Wisconsin Telephone)



The following companies have become defunct or were sold under SBC/AT&T ownership:
Southwestern Bell Texas   a separate operating company created by SBC, absorbed operations of original SWBT on December 30, 2001 and became Southwestern Bell Telephone, L.P.; merged into SWBT Inc. in 2007 which became the current Southwestern Bell
Southern New England Telephone - sold to Frontier Communications in 2014
Woodbury Telephone   merged into Southern New England Telephone on June 1, 2007.



AT&T stated that it would declare its intentions for its rural landlines on November 7, 2012. AT&T had previously announced that it was considering a sale of its rural landlines, which are not wired for AT&T's U-verse service; however, it has also stated that it may keep the business after all.
AT&T was not the first Baby Bell to sell off rural landlines. Ameritech sold some of its Wisconsin lines to CenturyTel in 1998; BellSouth sold some of its lines to MebTel in the 2000s; U S WEST sold many historically Bell landlines to Lynch Communications and Pacific Telecom in the 1990s; Verizon sold many of its New England lines to FairPoint in 2008 and its West Virginia operations to Frontier Communications in 2010.
It was announced on October 24, 2014 that Frontier Communications would take over control of the AT&T landline network in Connecticut On October 25, 2014 after being approved by state utility regulators in early October 2014. The deal worth about $2 billion included Frontier inheriting about 2,500 of AT&T's employees and many of AT&T's buildings.




AT&T Inc. has retained the holding companies it has acquired over the years resulting in the following corporate structure:
AT&T Inc., publicly traded holding company
Southwestern Bell Telephone Company d/b/a AT&T Arkansas, AT&T Kansas, AT&T Missouri, AT&T Oklahoma, AT&T Southwest, AT&T Texas
AT&T Teleholdings, Inc. d/b/a AT&T East, AT&T Midwest, AT&T West; formerly Ameritech, acquired in 1999:
Illinois Bell Telephone Company d/b/a AT&T Illinois
Indiana Bell Telephone Company d/b/a AT&T Indiana
Michigan Bell Telephone Company d/b/a AT&T Michigan
The Ohio Bell Telephone Company d/b/a AT&T Ohio
Pacific Bell Telephone Company d/b/a AT&T California
Nevada Bell Telephone Company d/b/a AT&T Nevada

Wisconsin Bell, Inc. d/b/a AT&T Wisconsin

AT&T Corp., acquired 2005
Alascom, Inc. d/b/a AT&T Alaska

BellSouth Corporation d/b/a AT&T South, acquired 2006
BellSouth Telecommunications, LLC d/b/a AT&T Alabama, AT&T Florida, AT&T Georgia, AT&T Louisiana, AT&T Kentucky, AT&T Mississippi, AT&T North Carolina, AT&T South Carolina, AT&T Southeast, AT&T Tennessee

AT&T Mobility
Cricket Wireless
AT&T Mexico
Iusacell
Nextel Mexico




AT&T's current board of directors:
Randall L. Stephenson   Chairman and Chief Executive Officer
James H. Blanchard
Gilbert F. Amelio
Reuben V. Anderson
Jaime Chico Pardo
James P. Kelly
Jon C. Madonna
Lynn M. Martin
John B. McCoy
Joyce M. Roch 
Matthew K. Rose
Laura D'Andrea Tyson



The financial performance of the company is reported to shareholders on an annual basis and a matter of public record. The unit (except where noted) is millions of US dollars. Where performance has been restated, the most recent statement of performance from an annual report is used.







The company maintains a database of call detail records of all telephone calls that have passed through its network since 1987. AT&T employees work at High Intensity Drug Trafficking Area offices (operated by the Office of National Drug Control Policy) in Los Angeles, Atlanta, and Houston so data can be quickly turned over to law enforcement agencies. Records are requested via administrative subpoena, without the involvement of a court or grand jury.



In September 2007, AT&T changed its legal policy to state that "AT&T may immediately terminate or suspend all or a portion of your Service, any Member ID, electronic mail address, IP address, Universal Resource Locator or domain name used by you, without notice for conduct that AT&T believes"..."(c) tends to damage the name or reputation of AT&T, or its parents, affiliates and subsidiaries." By October 10, 2007 AT&T had altered the terms and conditions for its Internet service to explicitly support freedom of expression by its subscribers, after an outcry claiming the company had given itself the right to censor its subscribers' transmissions. Section 5.1 of AT&T's new terms of service now reads "AT&T respects freedom of expression and believes it is a foundation of our free society to express differing points of view. AT&T will not terminate, disconnect or suspend service because of the views you or we express on public policy matters, political issues or political campaigns."




In 2006, the Electronic Frontier Foundation lodged a class action lawsuit, Hepting v. AT&T, which alleged that AT&T had allowed agents of the National Security Agency (NSA) to monitor phone and Internet communications of AT&T customers without warrants. If true, this would violate the Foreign Intelligence Surveillance Act of 1978 and the First and Fourth Amendments of the U.S. Constitution. AT&T has yet to confirm or deny that monitoring by the NSA is occurring. In April 2006, a retired former AT&T technician, Mark Klein, lodged an affidavit supporting this allegation. The Department of Justice has stated it will intervene in this lawsuit by means of State Secrets Privilege.
In July 2006, the United States District Court for the Northern District of California   in which the suit was filed   rejected a federal government motion to dismiss the case. The motion to dismiss, which invoked the State Secrets Privilege, had argued that any court review of the alleged partnership between the federal government and AT&T would harm national security. The case was immediately appealed to the Ninth Circuit. It was dismissed on June 3, 2009, citing retroactive legislation in the Foreign Intelligence Surveillance Act.
In May 2006, USA Today reported that all international and domestic calling records had been handed over to the National Security Agency by AT&T, Verizon, SBC, and BellSouth for the purpose of creating a massive calling database. The portions of the new AT&T that had been part of SBC Communications before November 18, 2005 were not mentioned.
On June 21, 2006, the San Francisco Chronicle reported that AT&T had rewritten rules on its privacy policy. The policy, which took effect June 23, 2006, says that "AT&T   not customers   owns customers' confidential info and can use it 'to protect its legitimate business interests, safeguard others, or respond to legal process.'"
On August 22, 2007, National Intelligence Director Mike McConnell confirmed that AT&T was one of the telecommunications companies that assisted with the government's warrantless wire-tapping program on calls between foreign and domestic sources.
On November 8, 2007, Mark Klein, a former AT&T technician, told Keith Olbermann of MSNBC that all Internet traffic passing over AT&T lines was copied into a locked room at the company's San Francisco office   to which only employees with National Security Agency clearance had access.
AT&T keeps for five to seven years a record of who text messages whom and the date and time, but not the content of the messages.



In January 2008, the company reported plans to begin filtering all Internet traffic which passes through its network for intellectual property violations. Commentators in the media have speculated that if this plan is implemented, it would lead to a mass exodus of subscribers leaving AT&T, although this is misleading as Internet traffic may go through the company's network anyway. Internet freedom proponents used these developments as justification for government-mandated network neutrality.



AT&T is accused by community media groups of discriminating against local Public, educational, and government access (PEG) cable TV channels:, by "impictions that will severely restrict the audience".
According to Barbara Popovic, Executive Director of the Chicago public-access service CAN-TV, the new AT&T U-verse system forces all Public-access television into a special menu system, denying normal functionality such as channel numbers, access to the standard program guide, and DVR recording. The Ratepayer Advocates division of the California Public Utilities Commission reported: "Instead of putting the stations on individual channels, AT&T has bundled community stations into a generic channel that can only be navigated through a complex and lengthy process."
Sue Buske (president of telecommunications consulting firm the Buske Group and a former head of the National Federation of Local Cable Programmers/Alliance for Community Media) argue that this is "an overall attack [...] on public access across the [United States], the place in the dial around cities and communities where people can make their own media in their own communities".



In June 2010, a hacker group known as Goatse Security discovered a vulnerability within AT&T that could allow anyone to uncover email addresses belonging to customers of AT&T 3G service for the Apple iPad. These email addresses could be accessed without a protective password. Using a script, Goatse Security collected thousands of email addresses from AT&T. Goatse Security informed AT&T about the security flaw through a third party. Goatse Security then disclosed around 114,000 of these emails to Gawker Media, which published an article about the security flaw and disclosure in Valleywag. Praetorian Security Group criticized the web application that Goatse Security exploited as "poorly designed".
In April 2015, AT&T was fined $25m over data security breaches, marking the largest ever fine issued by the U.S. Federal Communications Commission (FCC) for breaking data privacy laws. The investigation revealed the theft of details of approximately 280,000 people from call centres in Mexico, Colombia and the Philippines.



In March 2012, the United States federal government announced a lawsuit against AT&T. The specific accusations state that AT&T "violated the False Claims Act by facilitating and seeking federal payment for IP Relay calls by international callers who were ineligible for the service and sought to use it for fraudulent purposes. The complaint alleges that, out of fears that fraudulent call volume would drop after the registration deadline, AT&T knowingly adopted a non-compliant registration system that did not verify whether the user was located within the United States. The complaint further contends that AT&T continued to employ this system even with the knowledge that it facilitated use of IP Relay by fraudulent foreign callers, which accounted for up to 95 percent of AT&T's call volume. The government's complaint alleges that AT&T improperly billed the TRS Fund for reimbursement of these calls and received millions of dollars in federal payments as a result."



On April 28, 2015, AT&T announced that it had fired Aaron Slator, President of Content and Advertising Sales, for sending racist text messages. Slator was also hit with a $100 million discrimination lawsuit, filed by African-American employee Knoyme King. The day before that, protesters arrived at AT&T's headquarters in Dallas and its satellite offices in Los Angeles as well as at the home of CEO Randall Stephenson to protest alleged systemic racial policies. According to accounts, the protesters are demanding AT&T begin working with 100% black-owned media companies.







AT&T 220 Building   building in Indianapolis, Indiana
AT&T Building   building in Detroit, Michigan
AT&T Building   building in Indianapolis, Indiana
AT&T Building   building in Kingman, Arizona
AT&T Building   (aka "The Batman Building") in Nashville, Tennessee
AT&T Building   building in Omaha, Nebraska
AT&T Building Addition   building in Detroit, Michigan
AT&T Building   building in San Diego
AT&T Center   building in Los Angeles
AT&T Center   building in St. Louis, Missouri
AT&T Center   building in Charlotte, NC
AT&T City Center   building in Birmingham, Alabama
AT&T Corporate Center   building in Chicago, Illinois
AT&T Huron Road Building   building in Cleveland, Ohio
AT&T Lenox Park Campus   AT&T Mobility Headquarters in DeKalb County just outside Atlanta, Georgia
AT&T Midtown Center   building in Atlanta, Georgia
AT&T Switching Center   building in Los Angeles
AT&T Switching Center   building in Oakland, California
AT&T Switching Center   building in San Francisco
AT&T Tower - building in Minneapolis, MN
Whitacre Tower (One AT&T Plaza)   Corporate Headquarters, Dallas, Texas




AT&T Center   San Antonio, Texas (formerly SBC Center)
AT&T Field   Chattanooga, Tennessee (formerly BellSouth Park)
AT&T Park   San Francisco (formerly Pacific Bell Park, SBC Park)
AT&T Plaza   Chicago, Illinois (public space that hosts the Cloud Gate sculpture in Millennium Park)
AT&T Plaza   Dallas, Texas (plaza in front of the American Airlines Center at Victory Park)
AT&T Performing Arts Center   Dallas, Texas
AT&T Stadium   Arlington, Texas (formerly Cowboys Stadium)
Jones AT&T Stadium   Lubbock, Texas (formerly Clifford B. and Audrey Jones Stadium, Jones SBC Stadium)
TPC San Antonio   San Antonio, Texas (AT&T Oaks Course & AT&T Canyons Course)
War Memorial Stadium, AT&T Field - Little Rock, Arkansas



AT&T Champions Classic   Valencia, California
AT&T Championship - San Antonio, Texas
AT&T Classic   Atlanta, Georgia (formerly BellSouth Classic)
AT&T Cotton Bowl Classic (formerly Mobil Cotton Bowl Classic, Southwestern Bell Cotton Bowl Classic, SBC Cotton Bowl Classic)   played in Arlington, Texas, at AT&T Stadium.
AT&T National   Washington, D.C.
AT&T Pebble Beach National Pro-Am
AT&T Red River Rivalry   Dallas, Texas (formerly Red River Shootout, SBC Red River Rivalry)
Major League Soccer and the United States Soccer Federation, including the U.S. men's and U.S. women's national teams and the Major League Soccer All-Star Game from 2009
United States Olympic team
National Collegiate Athletic Association (Corporate Champion)
AT&T American Cup, Artistic gymnastics competition. Sponsored by AT&T since 2011.



AT&T (SEPTA station)   Public Transportation Station in Philadelphia, PA



AT&T offers services in many locations throughout the Asia Pacific; its regional headquarters is located in Hong Kong.
It was announced on November 7, 2014, that Mexican carrier Iusacell is being acquired by AT&T. The acquisition was approved in January 2015.
On April 30, 2015, AT&T acquired wireless operations Nextel Mexico from NII Holdings.


