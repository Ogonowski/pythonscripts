Counting rods (simplified Chinese:  ; traditional Chinese:  ; pinyin: ch u; Japanese:  , sangi) are small bars, typically 3 14 cm long, that were used by mathematicians for calculation in ancient China, Japan, Korea, and Vietnam. They are placed either horizontally or vertically to represent any integer or rational number.
The written forms based on them are called rod numerals. They are a true positional numeral system with digits for 1 9 and a blank for 0, from the Warring states period (circa 475 BCE) to the 16th century.



Counting rods were used by ancient Chinese for more than two thousand years. In 1954, forty-odd counting rods of the Warring States period were found in Zu ji g ngsh n ( ) Ch  Grave No.15 in Changsha, Hunan.
In 1973, archeologists unearthed a number of wood scripts from a Han dynasty tomb in Hubei. On one of the wooden scripts was written:  . This is one of the earliest examples of using counting rod numerals in writing.
In 1976, a bundle of West Han counting rods made of bones was unearthed from Qian Yang county in Shanxi. The use of counting rods must predate it; Laozi (6th or 5th century BCE) said "a good calculator doesn't use counting rods". The Book of Han recorded: "they calculate with bamboo, diameter one fen, length six cun, arranged into a hexagonal bundle of two hundred seventy one pieces". At first calculating rods were round in cross section, but by the time of the Sui dynasty triangular rods were used to represent positive numbers and rectangular rods were used for negative numbers.
After the abacus flourished, counting rods were abandoned except in Japan, where rod numerals developed into a symbolic notation for algebra.




Counting rods represent digits by the number of rods, and the perpendicular rod represents five. To avoid confusion, vertical and horizontal forms are alternately used. Generally, vertical rod numbers are used for the position for the units, hundreds, ten thousands, etc., while horizontal rod numbers are used for the tens, thousands, hundred thousands etc. Sun Tzu wrote that "one is vertical, ten is horizontal".
Red rods represent positive numbers and black rods represent negative numbers. Ancient Chinese clearly understood negative numbers and zero (leaving a blank space for it), though they had no symbol for the latter. The Nine Chapters on the Mathematical Art, which was mainly composed in the first century CE, stated "(when using subtraction) subtract same signed numbers, add different signed numbers, subtract a positive number from zero to make a negative number, and subtract a negative number from zero to make a positive number". Later, a go stone was sometimes used to represent zero.
This alternation of vertical and horizontal rod numeral form is very important to understanding written transcription of rod numerals on manuscripts correctly. For instance, in Licheng suanjin, 81 was transcribed as , and 108 was transcribed as ; it is clear that the latter clearly had a blank zero on the "counting board" (i.e., floor or mat), even though on the written transcription, there was no blank. In the same manuscript, 405 was transcribed as  , with a blank space in between for obvious reasons, and could in no way be interpreted as "45". In other words, transcribed rod numerals may not be positional, but on the counting board, they are positional.   is an exact image of the counting rod number 405 on a table top or floor.



The value of a number depends on its physical position on the counting board. A 9 at the rightmost position on the board stands for 9. Moving the batch of rods representing 9 to the left one position (i.e., to the tens place) gives 9[] or 90. Shifting left again to the third position (to the hundreds place) gives 9[][] or 900. Each time one shifts a number one position to the left, it is multiplied by 10. Each time one shifts a number one position to the right, it is divided by 10. This applies to single-digit numbers or multiple-digit numbers.
Song dynasty mathematician Jia Xian used hand-written Chinese decimal orders   as rod numeral place value, as evident from a facsimile from a page of Yongle Encyclopedia. He arranged   as

 
 

He treated the Chinese order numbers as place value markers, and   became place value decimal number. He then wrote the rod numerals according to their place value:
In Japan, mathematicians put counting rods on a counting board, a sheet of cloth with grids, and used only vertical forms relying on the grids. An 18th-century Japanese mathematics book has a checker counting board diagram, with the order of magnitude symbols " (thousand, hundred, ten, unit, tenth, hundredth, thousandth)

Examples:



Rod numerals are a positional numeral system made from shapes of counting rods. Positive numbers are written as they are and the negative numbers are written with a slant bar at the last digit. The vertical bar in the horizontal forms 6 9 are drawn shorter to have the same character height.
A circle ( ) is used for 0. Many historians think it was imported from Indian numerals by Gautama Siddha in 718, but some think it was created from the Chinese text space filler " ", and others think that the Indians acquired it from China, because it resembles a Confucian philosophical symbol for nothing.
In the 13th century, Southern Song mathematicians changed digits for 4, 5, and 9 to reduce strokes. The new horizontal forms eventually transformed into Suzhou numerals. Japanese continued to use the traditional forms.
Examples:
In Japan, Seki Takakazu developed the rod numerals into symbolic notation for algebra and drastically improved Japanese mathematics. After his period, the positional numeral system using Chinese numeral characters was developed, and the rod numerals were used only for the plus and minus signs.




A fraction was expressed with rod numerals as two rod numerals one on top of another (without any other symbol, like the modern horizontal bar).




The method for using counting rods for mathematical calculation was called rod calculation or rod calculus ( ). Rod calculus can be used for a wide range of calculations, including finding the value of  , finding square roots, cube roots, or higher order roots, and solving a system of linear equations. As a result, the character   is extended to connote the concept of planning in Chinese. For example, the science of using counting rods   does not refer to counting rods; it means operational research.
Before the introduction of written zero, there was no way to distinguish 10007 and 107 in written forms except by inserting a bigger space between 1 and 7, and so rod numerals were used only for doing calculations with counting rods. Once written zero came into play, the rod numerals had become independent, and their use indeed outlives the counting rods, after its replacement by abacus. One variation of horizontal rod numerals, the Suzhou numerals is still in use for book-keeping and in herbal medicine prescription in Chinatowns in some parts of the world.




Unicode 5.0 includes counting rod numerals in their own block in the Supplementary Multilingual Plane (SMP) from U+1D360 to U+1D37F. The code points for the horizontal digits 1 9 are U+1D360 to U+1D368 and those for the vertical digits 1 9 are U+1D369 to U+1D371. The former are called unit digits and the latter are called tens digits, which is opposite of the convention described above. Zero should be represented by U+3007 ( , ideographic number zero) and the negative sign should be represented by U+20E5 (combining reverse solidus overlay). As these were recently added to the character set and since they are included in the SMP, font support may still be limited.



Rod calculus
Abacus
Chinese mathematics
Unicode numerals
Tian yuan shu






For a look of the ancient counting rods, and further explanation, you can visit the sites
http://www.math.sfu.ca/histmath/China/Beginning/Rod.html
http://mathforum.org/library/drmath/view/52557.html
Counting rods in China (Chinese) (Translate: Google, Babelfish)
Counting rods and go stones of a Japanese mathematician around 1872 (Japanese) (Translate: Google, Babelfish)