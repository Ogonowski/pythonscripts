Silicon Graphics International Corp. (SGI; formerly Rackable Systems, Inc.) is an American manufacturer of computer hardware and software, including high-performance computing solutions, x86-based servers for datacenter deployment, and visualization products. It was founded as Rackable Systems in 1999.



On April 1, 2009, Rackable Systems announced an agreement to acquire Silicon Graphics, Inc. for $25 million. The sale, ultimately for $42.5 million, was finalized on May 11, 2009; at the same time, Rackable announced their adoption of "SGI" as their global name and brand.
In February 2010, SGI purchased substantially all the assets and assumed a limited amount of liabilities of COPAN. COPAN was a provider of storage archive products for real-time access to long-term persistent data. COPAN products were offered as part of the SGI storage line.
In March 2011 SGI acquired all outstanding shares of SGI Japan, Ltd. On August 15, 2011, the company announced that it had acquired OpenCFD Ltd., developers of open source computational fluid dynamics (CFD) software. In December 2011, Mark Barrenechea, who had become CEO of Rackable in April 2007, resigned from SGI to join Open Text Corporation. Another restructuring was announced in February 2012 while Ronald Verdoorn was chairman and interim CEO. Jorge Luis Titinger became president and chief executive officer on February 27, 2012.



The "new" SGI had two main product lines: servers and storage continuing from the original Rackable Systems; and servers, storage, visualization and professional services acquired from Silicon Graphics, Inc. At the time of the acquisition's completion, SGI said that they anticipated the survival of the majority of the two companies' product lines, although some consolidation was likely in areas of high overlap between products.






Official web site