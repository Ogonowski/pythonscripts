Whitechapel Computer Works Ltd. (WCW) was a computer workstation company formed in the East End of London, United Kingdom in April 1983, with investment from the Greater London Enterprise Board and the Department of Trade and Industry.
Their first workstation model was the MG-1 (named after the Milliard Gargantubrain from The Hitch-Hiker's Guide to the Galaxy). The MG-1 was based on the National Semiconductor NS32016 microprocessor, with a 1024   800 pixel monochrome display, a 40 MB Rodime ST-506 hard disk and an optional Ethernet interface. Initially, NatSemi's GENIX operating system, a port of 4.1BSD UNIX, was provided; this was later replaced by a port of 4.2BSD called 42nix. The MG-1 also had a partially kernel-based graphical user interface called Oriel.
The MG-1 first shipped to customers in 1984, with a base price of around  10,000, although Oriel was not released until the following year. A colour version, the CG-1, was also announced in 1986, followed by the MG-200, with an NS32332 processor, in 1987.
WCW went into receivership in 1986, but were soon revived, as Whitechapel Workstations Ltd., only to go into liquidation in April 1988. The new company launched the Hitech-10 and Hitech-20 workstations with R2000 and R3000 MIPS architecture processors respectively. These ran the UMIPS variant of UNIX, with either X11 or NeWS-based GUIs, and were aimed at computer animation applications.
Some ex-Whitechapel engineers went on to form Algorithmics Ltd., specialising in MIPS-based embedded systems. Algorithmics was later acquired by MIPS Technologies in 2002. The rights to the Hitech workstations were acquired by Mistral Computer Systems Ltd. in June 1988.



"Methodology of Window Management Systems". Computing at Chilton: 1961-2003. 1985. Retrieved 2008-04-17. 
"Evaluation of Single User Systems". Computing at Chilton: 1961-2003. June 1986. Retrieved 2008-04-17. 
Ian Kemmish. "Whitechapel Workstations". Newsgroup: alt.folklore.computers. Usenet: iank.756504505@tdc. Retrieved 2008-04-17. 
"MIPS acquires Algorithmics". Telecomworldwire. 2002-07-19. Retrieved 2008-04-17.