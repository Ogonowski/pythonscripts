In computer processors the carry flag (usually indicated as the C flag) is a single bit in a system status (flag) register used to indicate when an arithmetic carry or borrow has been generated out of the most significant ALU bit position. The carry flag enables numbers larger than a single ALU width to be added/subtracted by carrying (adding) a binary digit from a partial addition/subtraction to the least significant bit position of a more significant word. It is also used to extend bit shifts and rotates in a similar manner on many processors (sometimes done via a dedicated X flag). For subtractive operations, two (opposite) conventions are employed as most machines set the carry flag on borrow while some machines (such as the 6502 and the PIC) instead reset the carry flag on borrow (and vice versa).



The carry flag is affected by the result of most arithmetic (and typically several bitwise) instructions and is also used as an input to many of them. Several of these instructions have two forms which either read or ignore the carry. In assembly languages these instructions are represented by mnemonics such as ADD/SUB, ADC/SBC (ADD/SUB including carry), SHL/SHR (bit shifts), ROL/ROR (bit rotates), RCR/RCL (rotate through carry), and so on.  The use of the carry flag in this manner enables multi-word add, subtract, shift, and rotate operations.
An example is what happens if one were to add 255 and 255 using 8-bit registers. The result should be 510 which is 1_1111_1110 in binary, requiring 9 bits. The 8 least significant bits always stored in the register would be 1111_1110 binary (254 decimal) but since there is carry out of bit 7 (the eight bit), the carry is set, indicating that the result needs 9 bits. The valid 9-bit result is the concatenation of the carry flag with the result. Note that in an 8-bit two's complement interpretation, this operation is  1 +  1 and yields the correct result of  2, with no overflow, even if the carry is ignored.
Another example may be an 8-bit register with the bit pattern 0101_0101 and the carry flag set; if we execute a rotate left through carry instruction, the result would be 1010_1011 with the carry flag cleared because the most significant bit (bit 7) was rotated into the carry while the carry was rotated into the least significant bit (bit 0).
While the first micro processors Intel 4004 (CLC) and Intel 8008 (FC) had specific operations to set the carry bit to 0, the later Intel 8080 and its compatible processors (such as the Z80) only had operations to set the carry bit to 1. For setting it to zero they use the logic instructions (AND, OR, XOR) which actually do not use the carry bit at all for their functions.
The carry flag is also often used following comparison instructions, which are typically implemented by subtractive operations, to allow a decision to be made about which of the two compared values is lower than (or greater or equal to) the other. Branch instructions which examine the carry flag are often represented by mnemonics such as BCC and BCS to branch if the carry is clear, or branch if the carry is set respectively. When used in this way the carry flag provides a mechanism for comparing the values as unsigned integers. This is in contrast to the overflow flag which provides a mechanism for comparing the values as signed integer values.



While the carry flag is well-defined for addition, there are two possible ways to use the carry flag for subtractive operations.
One uses the bit as a borrow flag, setting it if a<b when computing a b, and a borrow must be performed. A subtract with borrow (SBB) instruction will compute a b C = a (b+C), while a subtract without borrow (SUB) acts as if the borrow bit were clear. The 8080, Z80, x86 and 68k families (among others) use a borrow bit.
The other takes advantage of the identity that  x = not(x)+1 and computes a b as a+not(b)+1. The carry flag is set according to this addition, and subtract with carry computes a+not(b)+C, while subtract without carry acts as if the carry bit were set. The 6502, ARM and PowerPC processors use this convention. The 6502 is a particularly well-known example because it does not have a subtract without carry operation, so software must ensure that the carry flag is set before every subtract operation where a borrow is not required.
The modern convention is to refer to the first alternative as a "borrow bit", while the second is called a "carry bit". However, there are exceptions in both directions; the VAX and NS320xx architectures use the borrow bit convention, but call their a b C operation "subtract with carry" (SBWC). PA-RISC uses a carry bit convention, but calls its a+not(b)+C operation "subtract with borrow" (SUBB).



Binary arithmetic
Half-carry flag
Status register






Carry Flag and Overflow Flag in binary arithmetic
Carry Bit: How does it work?