R is a programming language and software environment for statistical computing and graphics. The R language is widely used among statisticians and data miners for developing statistical software and data analysis. Polls, surveys of data miners, and studies of scholarly literature databases show that R's popularity has increased substantially in recent years.
R is an implementation of the S programming language combined with lexical scoping semantics inspired by Scheme. S was created by John Chambers while at Bell Labs. There are some important differences, but much of the code written for S runs unaltered.
R was created by Ross Ihaka and Robert Gentleman at the University of Auckland, New Zealand, and is currently developed by the R Development Core Team, of which Chambers is a member. R is named partly after the first names of the first two R authors and partly as a play on the name of S.
R is a GNU project. The source code for the R software environment is written primarily in C, Fortran, and R. R is freely available under the GNU General Public License, and pre-compiled binary versions are provided for various operating systems. R uses a command line interface; there are also several graphical front-ends for it.



R and its libraries implement a wide variety of statistical and graphical techniques, including linear and nonlinear modeling, classical statistical tests, time-series analysis, classification, clustering, and others. R is easily extensible through functions and extensions, and the R community is noted for its active contributions in terms of packages. Many of R's standard functions are written in R itself, which makes it easy for users to follow the algorithmic choices made. For computationally intensive tasks, C, C++, and Fortran code can be linked and called at run time. Advanced users can write C, C++, Java, .NET  or Python code to manipulate R objects directly.
R is highly extensible through the use of user-submitted packages for specific functions or specific areas of study. Due to its S heritage, R has stronger object-oriented programming facilities than most statistical computing languages. Extending R is also eased by its lexical scoping rules.
Another strength of R is static graphics, which can produce publication-quality graphs, including mathematical symbols. Dynamic and interactive graphics are available through additional packages.
R has its own LaTeX-like documentation format, which is used to supply comprehensive documentation, both on-line in a number of formats and in hard copy.



R is an interpreted language; users typically access it through a command-line interpreter. If a user types 2+2 at the R command prompt and presses enter, the computer replies with 4, as shown below:

Like other similar languages such as APL and MATLAB, R supports matrix arithmetic. R's data structures include vectors, matrices, arrays, data frames (similar to tables in a relational database) and lists. R's extensible object system includes objects for (among others): regression models, time-series and geo-spatial coordinates. The scalar data type was never a data structure of R. A scalar is represented as a vector with length one in R.
R supports procedural programming with functions and, for some functions, object-oriented programming with generic functions. A generic function acts differently depending on the type of arguments passed to it. In other words, the generic function dispatches the function (method) specific to that type of object. For example, R has a generic print function that can print almost every type of object in R with a simple print(objectname) syntax.
Although used mainly by statisticians and other practitioners requiring an environment for statistical computation and software development, R can also operate as a general matrix calculation toolbox   with performance benchmarks comparable to GNU Octave or MATLAB. Arrays are stored in column-major order.






The following examples illustrate the basic syntax of the language and use of the command-line interface.
In R, the widely preferred assignment operator is an arrow made from two characters <-, although = can be used instead.



Short R code calculating Mandelbrot set through the first 20 iterations of equation z = z2 + c plotted for different complex constants c. This example demonstrates:
use of community-developed external libraries (called packages), in this case caTools package
handling of complex numbers
multidimensional arrays of numbers used as basic data type, see variables C, Z and X.



The ease of function creation by the user is one of the strengths of using R. Objects remain local to the function, which can be returned as any data type. Below is an example of the structure of a function:



The capabilities of R are extended through user-created packages, which allow specialized statistical techniques, graphical devices (ggplot2), import/export capabilities, reporting tools (knitr, Sweave), etc. These packages are developed primarily in R, and sometimes in Java, C, C++ and Fortran. A core set of packages is included with the installation of R, with more than 7,000 additional packages (as of August 2015) available at the Comprehensive R Archive Network (CRAN), Bioconductor, Omegahat, GitHub and other repositories. 
The "Task Views" page (subject list) on the CRAN website lists a wide range of tasks (in fields such as Finance, Genetics, High Performance Computing, Machine Learning, Medical Imaging, Social Sciences and Spatial Statistics) to which R has been applied and for which packages are available. R has also been identified by the FDA as suitable for interpreting data from clinical research.
Other R package resources include Crantastic, a community site for rating and reviewing all CRAN packages, and R-Forge, a central platform for the collaborative development of R packages, R-related software, and projects. R-Forge also hosts many unpublished beta packages, and development versions of CRAN packages.
The Bioconductor project provides R packages for the analysis of genomic data, such as Affymetrix and cDNA microarray object-oriented data-handling and analysis tools, and has started to provide tools for analysis of data from next-generation high-throughput sequencing methods.



The full list of changes is maintained in the "R News" file at CRAN. Some highlights are listed below for several major releases.






Architect   cross-platform open source IDE for data science based on Eclipse and StatET
Deducer   GUI for menu driven data analysis (similar to SPSS/JMP/Minitab).
Java GUI for R   cross-platform stand-alone R terminal and editor based on Java (also known as JGR).
Rattle GUI   cross-platform GUI based on RGtk2 and specifically designed for data mining.
R Commander   cross-platform menu-driven GUI based on tcltk (several plug-ins to Rcmdr are also available).
Revolution R Productivity Environment (RPE)   Revolution Analytics provided Visual Studio based IDE, and has plans for web based point and click interface.
RGUI   comes with the pre-compiled version of R for Microsoft Windows.
RKWard   extensible GUI and IDE for R.
RStudio   cross-platform open source IDE (which can also be run on a remote linux server).
There is a special issue of the Journal of Statistical Software that discusses GUIs for R.



Text editors and Integrated development environments (IDEs) with some support for R include: ConTEXT, Eclipse (StatET), Emacs (Emacs Speaks Statistics), LyX (modules for knitr and Sweave), Vim, jEdit, Kate, Revolution R Enterprise DevelopR (part of Revolution R Enterprise), RStudio, Sublime Text, TextMate, Atom, WinEdt (R Package RWinEdt), Tinn-R, Notepad++ and  Architect.



R functionality has been made accessible from several scripting languages such as Python, Perl, Ruby, F# and Julia. Scripting in R itself is possible via a front-end called littler.



"useR!" is the name given to the official annual gathering of R users. The first such event was useR! 2004 in May 2004, Vienna, Austria. After skipping 2005, the useR conference has been held annually, usually alternating between locations in Europe and North America. Subsequent conferences were:
useR! 2006, Vienna, Austria
useR! 2007, Ames, Iowa, USA
useR! 2008, Dortmund, Germany
useR! 2009, Rennes, France
useR! 2010, Gaithersburg, Maryland, USA
useR! 2011, Coventry, United Kingdom
useR! 2012, Nashville, Tennessee, USA
useR! 2013, Albacete, Spain
useR! 2014, Los Angeles, USA
useR! 2015, Aalborg, Denmark



The general consensus is that R compares well with other popular statistical packages, such as SAS, SPSS and Stata. In January 2009, the New York Times ran an article about R gaining acceptance among data analysts and presenting a potential threat for the market share occupied by commercial statistical packages, such as SAS.



In 2007, Revolution Analytics was founded to provide commercial support for Revolution R, its distribution of R, which also includes components developed by the company. Major additional components include: ParallelR, the R Productivity Environment IDE, RevoScaleR (for big data analysis), RevoDeployR, web services framework, and the ability for reading and writing data in the SAS file format. In 2015, Microsoft Corporation completed the acquisition of Revolution Analytics.
For organizations in highly regulated sectors requiring a validated version of R, Mango Solutions has developed the ValidR product which fully complies with the Food and Drug Administration guidelines for Software verification and validation.
In October 2011, Oracle announced the Big Data Appliance, which integrates R, Apache Hadoop, Oracle Linux, and a NoSQL database with the Exadata hardware. Oracle R Enterprise is now one of two components of the "Oracle Advanced Analytics Option" (the other component is Oracle Data Mining).
IBM offers support for in-Hadoop execution of R, and provides a programming model for massively parallel in-database analytics in R.
Other major commercial software systems supporting connections to or integration with R include: JMP, Mathematica, MATLAB, Spotfire, SPSS, STATISTICA, Platform Symphony, SAS, Tableau,and Dundas.
Tibco offers a runtime version R as a part of Spotfire.


