Lerner Publishing Group, based in Minneapolis in the U.S. state of Minnesota since its founding in 1959, is one of the largest independently owned children's book publishers in the United States. With more than 3,500 titles in print, Lerner Publishing Group offers nonfiction and fiction books for grades K-12. The group is made up of eight imprints, Interface Graphics, and Muscle Bound Bindery.



Lerner was founded in 1959 by Harry Lerner. The company started as a one-room office in the old Lumber Exchange Building in downtown Minneapolis. Lerner's sister asked him to publish her stories about childhood diseases, which became the Medical Books for Children series (1959). The company has expanded to encompass four offices: the main Lerner building, Lerner Distribution Center, and Muscle Bound Bindery, all located in Minneapolis, and a New York office located in the Empire State Building. In 1963, Lerner was the first publisher to print original art featuring multi-racial children, and has continued a tradition of innovation.






Lerner Publications is intended for K-5 students, publishing 130 books annually in photo-driven series for schools and libraries.



Originally a Connecticut-based publisher, Lerner acquired Millbrook Press in 2004. Millbrook publishes under three sub-imprints: Millbrook, Copper Beech, and Twenty-First Century. Millbrook prints books for both the consumer and school markets.[1]



Carolrhoda Books was begun in 1969, named after Carolrhoda Locketz, a close friend of Sharon Lerner who died after serving in the Peace Corps. Carolrhoda publishes about 20 titles annually: picture books, fiction, and single-title nonfiction for trade and library editions.




Graphic Universe was launched in 2006 and prints books in graphic novel format. The imprint has two main series: Twisted Journeys and Graphic Myths and Legends. The imprint is notable for using well-known artists from the comic book genre.



Twenty-First Century Books was acquired along with Millbrook Press in 2004. This imprint focuses on books for grades 6+, usually photo-driven.



Ediciones Lerner prints Spanish-language translations of popular nonfiction titles for elementary school students.



LernerClassroom, launched in 2000, is specifically geared towards educational titles, including resources for teachers. Some titles are from the Group's list, and others are specially developed. The imprint works mostly with nonfiction, but some fiction.




Kar-Ben Publishing was acquired in 2001, for artwork- and photo-driven fiction and nonfiction titles geared for the children's Judaica market.



Created for K-2 grades, they contain bright, eye-catching art and picture book designs to grab the attention young readers.



Designed for grades 3-5, these books are simple, straightforward nonfiction texts that build children's  reading skills.



The Sharon Lerner Scholarship is a four-week scholarship, begun in 1983, to honor Sharon Lerner, art director and founder of Carolrhoda Books. The scholarship is intended to promote understanding and cooperation between international publishers of children's books.[2]



Lerner Publications Company in Pictures and Quotes. Minneapolis, MN: Lerner Publishing Group. 1999. pp. 4, 5. 
Business.com. "Millbrook Press Inc. (the) Information." Business.com. Accessed on July 22, 2008.
Lerner Books. "Lerner Books." Lerner Publishing Group. Accessed on July 22, 2008.