z/OS is a 64-bit operating system for IBM mainframes, produced by IBM. It derives from and is the successor to OS/390, which in turn followed a string of MVS versions. Like OS/390, z/OS combines a number of formerly separate, related products, some of which are still optional. z/OS offers the attributes of modern operating systems but also retains much of the functionality originating in the 1960s and each subsequent decade that is still found in daily use (backward compatibility is one of z/OS's central design philosophies). z/OS was first introduced in October 2000.



z/OS supports stable mainframe systems and standards such as CICS, IMS, DB2, RACF, SNA, WebSphere MQ, record-oriented data access methods, REXX, CLIST, SMP/E, JCL, TSO/E, and ISPF, among others. However, z/OS also supports 64-bit Java, C, C++, and UNIX (Single UNIX Specification) APIs and applications through UNIX System Services   The Open Group certifies z/OS as a compliant UNIX operating system   with UNIX/Linux-style hierarchical HFS and zFS file systems. As a result, z/OS hosts a broad range of commercial and open source software. z/OS can communicate directly via TCP/IP, including IPv6, and includes standard HTTP servers (one from Lotus, the other Apache-derived) along with other common services such as FTP, NFS, and CIFS/SMB. Another central design philosophy is support for extremely high quality of service (QoS), even within a single operating system instance, although z/OS has built-in support for Parallel Sysplex clustering.
z/OS has a Workload Manager (WLM) and dispatcher which automatically manages numerous concurrently hosted units of work running in separate key-protected address spaces according to dynamically adjustable goals. This capability inherently supports multi-tenancy within a single operating system image. However, modern IBM mainframes also offer two additional levels of virtualization: LPARs and (optionally) z/VM. These new functions within the hardware, z/OS, and z/VM   and Linux and OpenSolaris support   have encouraged development of new applications for mainframes. Many of them utilize the WebSphere Application Server for z/OS middleware.
From its inception z/OS has supported tri-modal addressing (24-bit, 31-bit, and 64-bit). Up through Version 1.5, z/OS itself could start in either 31-bit ESA/390 or 64-bit z/Architecture mode, so it could function on older hardware albeit without 64-bit application support on those machines. (Only the newer z/Architecture hardware manufactured starting in the year 2000 can run 64-bit code.) IBM support for z/OS 1.5 ended on March 31, 2007. Now z/OS is only supported on z/Architecture mainframes and only runs in 64-bit mode. z/Architecture hardware always starts running in 31-bit mode, but current z/OS releases quickly switch to 64-bit mode and will not run on hardware that does not support 64-bit mode. Application programmers can still use any addressing mode: all applications, regardless of their addressing mode(s), can coexist without modification, and IBM maintains commitment to tri-modal backward compatibility. However, increasing numbers of middleware products and applications, such as DB2 Version 8 and above, now require and exploit 64-bit addressing.
IBM markets z/OS as its flagship operating system, suited for continuous, high-volume operation with high security and stability.
z/OS is available under standard license pricing as well as via System z New Application License Charges (zNALC) and "System z Solution Edition," two lower priced offerings aimed at supporting newer applications ("new workloads"). U.S. standard commercial z/OS pricing starts at about $125 per month, including support, for the smallest zNALC installation running the base z/OS product plus a typical set of optional z/OS features.
z/OS introduced Variable Workload License Charges (VWLC) and Entry Workload License Charges (EWLC) which are sub-capacity billing options. VWLC and EWLC customers only pay for peak monthly z/OS usage, not for full machine capacity as with the previous OS/390 operating system. VWLC and EWLC are also available for most IBM software products running on z/OS, and their peaks are separately calculated but can never exceed the z/OS peak. To be eligible for sub-capacity licensing, a z/OS customer must be running in 64-bit mode (which requires z/Architecture hardware), must have completely eliminated OS/390 from the system, and must e-mail IBM monthly sub-capacity reports. Sub-capacity billing substantially reduces software charges for most IBM mainframe customers. Advanced Workload License Charges (AWLC) is the successor to VWLC on mainframe models starting with the zEnterprise 196, and EAWLC is an option on zEnterprise 114 models. AWLC and EAWLC offer further sub-capacity discounts.






Within each address space, z/OS typically only permits the placement of data above the 2 GB "bar", not code. z/OS enforces this distinction primarily for performance reasons. There are no architectural impediments to allowing more than 2 GB of application code per address space. IBM has started to allow Java code running on z/OS to execute above the 2 GB bar, again for performance reasons.
Memory is obtained as "Large Memory Objects" in multiples of 1 MB (with the expectation that applications and middleware will manage memory allocation within these large pieces). There are three types of large memory objects:
Unshared   where only the creating address space can access the memory.
Shared   where the creating address space can give access to specific other address spaces.
Common   where all address spaces can access the memory. (This type was introduced in z/OS Release 10.)



IBM supports z/OS release coexistence and fallback on an "N+2" basis. For example, IBM customers running Release 9 can upgrade directly to Release 11 or Release 10, and both releases can operate concurrently within the same Sysplex (cluster) and without conflict using the same datasets, configurations, security profiles, etc. (coexistence), as long as so-called "toleration maintenance" is installed in the older release. If there is a problem with Release 11, the customer can return to Release 9 without experiencing dataset, configuration, or security profile compatibility problems (fallback) until ready to try moving forward again. z/OS customers using Parallel Sysplex (clustering) can operate N+2 releases (e.g. Release 9 and Release 11, or Release 9 and Release 10) in mixed release configurations, in production, as long as required to complete release upgrades. Supported release mixing within a cluster is one of the key strategies of z/OS for avoiding service outages.
IBM's standard support period for each z/OS release is three years. Most z/OS customers take advantage of the N+2 support model, and skip every other release. Thus most z/OS customers are either "odd" or "even."
IBM releases individual small enhancements and corrections (a.k.a. PTFs) for z/OS as needed, when needed. IBM labels critical PTFs as "HIPER" (High Impact PERvasive). IBM also "rolls up" multiple patches into a Recommended Service Update (RSU). RSUs are released periodically (in the range of every one to three months) and undergo additional testing. Although z/OS customers vary in their maintenance practices, IBM encourages every z/OS customer to adopt a reasonable preventive maintenance strategy, to avoid known problems before they might occur.
As of 28 July 2015, Version 2 Release 2 is available.






Fujitsu MSP
HiperDispatch
Hitachi VOS3
Intelligent Resource Director
Linux on z Systems for a mainframe version of a popular operating system
Parallel Sysplex
Resource Measurement Facility
SDSF
SMF
SMP/E
z/TPF
Workload Manager
zAAP, a specialty processor dedicated to particular z/OS workloads
zIIP, another specialty processor dedicated to particular z/OS workloads
z/VSE for another mainframe operating system






IBM: z/OS
IBM: Shop zSeries (ShopZ)
IBM: z/OS Internet Library
IBM Systems Mainframe Magazine