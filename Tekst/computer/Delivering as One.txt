Delivering as One is the name of a report and an initiative. The report was issued by a United Nations panel established by the then UN Secretary-General Kofi Annan, in 2005. The report explored how the United Nations system could work more coherently and effectively across the world in the areas of development, humanitarian assistance and the environment. The panel issued its report in November 2006, and sets out a program of reform of the international humanitarian system. It focuses on four main principles: One Leader, One Budget, One Programme and One Office.
As a result, countries   both Government and UN partners   have undertaken efforts to work together more effectively and efficiently. The United Nations launched the  Delivering as One  pilot initiative in 2007 to respond to the challenges of a changing world and test how the UN family can provide development assistance in a more coordinated way in eight countries. When the Secretary-General launched Delivering as One, the governments of eight countries Albania, Cape Verde, Mozambique, Pakistan, Rwanda, Tanzania, Uruguay, and Viet Nam volunteered to become  Delivering as One  pilots. The pilot countries agreed to work with the UN system to capitalize on the strengths and comparative advantages of the different members of the UN family. Together they are experimenting with ways to increase the UN system s impact through more coherent programmes, reduced transaction costs for governments, and lower overhead costs for the UN system. The Four principles The eight pilots are making reforms based on four principles: One Leader One Budget One Programme One Office These changes respond to varied needs while drawing on all parts of the UN system, whether based in the country or not. The exercise has already helped to align our programmes and funding more closely to national priorities. It has strengthened government leadership and ownership. It is ensuring that governments have access to the experience and expertise of a wider range of United Nations organizations to respond to their national priorities. Several issues we work on have seen increased emphasis, most notably being support to the productive sector, employment, trade, protection of the environment, adaptation to climate change, the global food crisis, and the financial crisis. This improvement has emerged from a process where UN agencies that aren t physically present in the pilot countries have been able to spend more time advising their governments without having to set up costly offices.
This effort is mostly led by the group the United Nations Development Group, a group of 32 United Nations specialised agencies working on International Development issues.




United Nations Development Group
Reform of the United Nations






High Level Panel on UN System-wide Coherence
Delivering as One