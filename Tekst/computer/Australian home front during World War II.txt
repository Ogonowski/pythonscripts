Although most Australian civilians lived far from the front line of World War II, the Australian home front during World War II played a significant role in the Allied victory and led to permanent changes to Australian society.
During the war the Australian Government greatly expanded its powers in order to better direct the war effort, and Australia's industrial and human resources were focused on supporting the Allied armed forces. While there were only a relatively small number of attacks on civilian targets, many Australians feared that the country would be invaded during the early years of the Pacific War.




The Menzies Government (1939 1941), led by Prime Minister Robert Menzies of the United Australia Party governed Australia though the early years of the Second World War. On 3 September 1939, Menzies made a national radio broadcast:

Australia had entered World War II. Earle Page, as leader of the Country Party and John Curtin as leader of the Labor Party both pledged support to the declaration of war. A National Security Act was passed. The recruitment of a volunteer military force for service at home and abroad was announced, the 2nd Australian Imperial Force, and a citizen militia organised for local defence. In 1940 41, Australian forces played prominent roles in the fighting in the Mediterranean theatre.
A special War Cabinet was created after war was declared   initially composed of Prime Minister Menzies and five senior ministers (RG Casey, GA Street, Senator McLeay, HS Gullet and World War I Prime Minister Billy Hughes). In March 1940, troubled negotiations were concluded with the Country Party to re-enter Coalition with the UAP. The replacement of Earle Page as leader by Archie Cameron allowed Menzies to reach accommodation. A new Coalition ministry was formed including the Country Party's Archie Cameron, Harold Thorby and John McEwen, as well as junior ministers Arthur Fadden and Horace Nock.
In Europe, the Phoney War gave way to a succession of Allied defeats, as Germany overran the Low Countries, Norway and France. By June 1940, the British Empire stood alone against Germany. Menzies called for an  all in  war effort and with the support of Curtin, amended the National Security Act to extend government powers to tax, acquire property, control businesses and the labour force and allow for conscription of men for the "defence of Australia". Essington Lewis, the head of Broken Hill Proprietary Ltd was appointed Director-General of Munitions Supply to assist with mobilisation of national resources.
With the 1940 election looming, Menzies lost his Chief of the General Staff and three loyal ministers in a Royal Australian Airforce crash at Canberra airport. The Labor Party meanwhile experienced a split along pro and anti Communist lines over policy towards the Soviet Union for its co-operation with Nazi Germany in the invasion of Poland and Labor narrowly lost the September 1940 Election. The UAP Country Party coalition and the Labor parties won 36 seats each and the Menzies Government was forced to rely upon the support of two Independents, Alex Wilson and Arthur Coles to continue in office.
Menzies proposed an all party unity government to break the impasse, but the Labor Party refused to join. John Curtin agreed instead to take a seat on a newly created Advisory War Council in October 1940. New Country Party leader Arthur Fadden became Treasurer and Menzies unhappily conceded to allow Earle Page back into his ministry.
In January 1941, Menzies flew to Britain to discuss the weakness of Singapore's defences and sat with Winston Churchill's British War Cabinet. He was unable to achieve significant assurances for increased commitment to Singapore's defences, but undertook morale boosting excursions to war affected cities and factories. Returning to Australia via Lisbon and the United States in May, Menzies faced a war-time minority government under ever increasing strain.
In Menzies's absence, John Curtin had co-operated with Deputy Prime Minister Arthur Fadden of the Country Party in preparing Australia for the expected Pacific War. With the threat of Japan imminent and with the Australian army suffering badly in the Greek and Crete campaigns, Menzies re-organised his ministry and announced multiple multi-party committees to advise on war and economic policy. Government critics however called for an all-party government. In August, Cabinet decided that Menzies should travel back to Britain to represent Australia in the War Cabinet   but this time the Labor caucus refused to support the plan. Menzies announced to his Cabinet that he thought he should resign and advise the Governor General to invite John Curtin to form Government. The Cabinet instead insisted he approach Curtin again to form a war cabinet. Unable to secure Curtin's support, and with an unworkable parliamentary majority, Menzies then resigned as prime minister and leader of the UAP on 29 August 1941.
Australian trade unions supported the war, after Germany attacked the Soviet Union in June 1941. However, in spring 1940, the coal miners struck for higher wages for 67 days under communist leadership. On 15 June 1940 the Menzies government suppressed 10 communist and fascist parties and organizations as subversive of the war effort. Police and Army intelligence made hundreds of raids that night, and later broke up public meetings in the capital cities.
In July 1940, the Menzies government imposed regulations using the National Security Act which placed virtually all of Australia's newspapers, radio stations, and film industry under the direct control of the Director-General of Information. Newspaper publishers complained it was a blow struck at the freedom of the press. In January 1941, new regulations were directed against speaking disloyalty in public or even in private. The regulations were aimed at "whisperers" who undermined morale by spreading false rumours.




Billy Hughes replaced Menzies as leader of the UAP and the UAP-Country Party Coalition held office for another month with Arthur Fadden of the Country Party as prime minister. Labor under Curtin formed a minority government in 1941 after independents crossed the floor, bringing down the Coalition minority United Australia Party-Country Party Coalition government which resulted from the 1940 election. Curtin went on to lead federal Labor to its greatest win with two thirds of seats in the lower house and over 58 percent of the two-party preferred vote at the 1943 election. Labor won 49 seats to 12 United Australia Party, 7 Country Party, 3 Country National Party (Queensland), 1 Queensland Country Party, 1 Liberal Country Party (Victoria) and 1 Independent in the Australian House of Representatives. The Labor Party also won all 19 of the seats contested for the Australian Senate. Eight weeks later, Japan attacked Pearl Harbor.
The Curtin Government led Australia when the Australian mainland came under direct military threat during the Japanese advance. He is widely regarded as one of the country's greatest prime ministers. General Douglas MacArthur said that Curtin was "one of the greatest of the wartime statesmen".
With most of Australia s best forces committed to fight against Hitler in the Middle East, Japan attacked Pearl Harbor, the US naval base in Hawaii, on 8 December 1941 (eastern Australia time). The British battleship HMS Prince of Wales and battlecruiser HMS Repulse sent to defend Singapore were sunk soon afterwards. Australia was ill-prepared for an attack, lacking armaments, modern fighter aircraft, heavy bombers, and aircraft carriers. While demanding reinforcements from Churchill, on 27 December 1941 Curtin published an historic announcement:

British Malaya quickly collapsed, shocking the Australian nation. British, Indian and Australian troops made a disorganised last stand at Singapore, before surrendering on 15 February 1942. Curtin predicted that the "battle for Australia" would now follow. On 19 February, Darwin suffered a devastating air raid, the first time the Australian mainland had ever been attacked by enemy forces. Over the following 19 months, Australia was attacked from the air almost 100 times.
U.S. President Franklin Roosevelt ordered his commander in the Philippines, General Douglas MacArthur, to formulate a Pacific defence plan with Australia in March 1942. Curtin agreed to place Australian forces under the command of General MacArthur, who became "Supreme Commander of the South West Pacific". Curtin had thus presided over a fundamental shift in Australia's foreign policy. MacArthur moved his headquarters to Melbourne in March 1942 and American troops began massing in Australia. In late May 1942, Japanese midget submarines sank an accommodation vessel in a daring raid on Sydney Harbour. On 8 June 1942, two Japanese submarines briefly shelled Sydney's eastern suburbs and the city of Newcastle.

In an effort to isolate Australia, the Japanese planned a seaborne invasion of Port Moresby, in the Australian Territory of New Guinea. In May 1942, the U.S. Navy engaged the Japanese in the Battle of the Coral Sea and halted the attack. The Battle of Midway in June effectively defeated the Japanese navy and the Japanese army launched a land assault on Port Moresby from the north.
Concerned to maintain British commitment to the defence of Australia, Prime Minister Curtin announced in November 1943 that Prince Henry, Duke of Gloucester was to be appointed Governor General of Australia. The brother of King George VI arrived in Australia to take up his post in January, 1945. Curtin hoped this might influence the British to despatch men and equipment to the Pacific, and the appointment reaffirmed the important role of the Crown to the Australian nation at that time.
The Battle of Buna-Gona, between November 1942 and January 1943, set the tone for the bitter final stages of the New Guinea campaign, which persisted into 1945. MacArthur to a certain extent excluded Australian forces from the main push north into the Philippines and Japan. It was left to Australia to lead amphibious assaults against Japanese bases in Borneo.

As the end of the war approached, Curtin sought to firm up Australian influence in the South Pacific following the war but also sought to ensure a continuing role for the British Empire, calling Australia "the bastion of British institutions, the British way of life and the system of democratic government in the Southern World". In April 1944, Curtin held talks on postwar planning with President Franklin Roosevelt of the US and with Prime Minister Winston Churchill of Britain and gained agreement for the Australian economy to begin transitioning from military to post-war economy. He returned to Australia and campaigned for an unsuccessful 1944 referendum to extend federal government power over employment, monopolies, Aboriginal people, health and railway gauges.
Prime Minister Curtin suffered from ill health from the strains of office. He suffered a major heart attack in November 1944. Facing the newly formed Liberal Party of Australia opposition led by Robert Menzies, Curtin struggled with exhaustion and a heavy work load   excusing himself from Parliamentary question time and unable to concentrate on the large number of parliamentary bills being drafted dealing with the coming peace. Curtin returned to hospital in April with lung congestion. With Deputy Prime Minister Frank Forde was in the United States and Ben Chifley serving as acting prime minister, it was Chifley who announced the end of the war in Europe on 9 May 1945.
When Curtin died towards the end of the Second World War in July 1945, Frank Forde served as prime minister from 6 13 July, before the party elected Ben Chifley as Curtin's successor. Following his 1945 election as leader of the Labor Party, Chifley, a former railway engine driver, became Australia s 16th prime minister on 13 July 1945. The Second World War ended with the defeat of Japan in the Pacific just four weeks later.



97 air-raids were conducted against Australia by the Japanese Airforce through 1942 and 1943. The Darwin area was hit 64 times. Horn Island was struck 9 times, Broome and Exmouth Gulf 4 times, Townsville and Millingimbi three times, Port Hedland and Wyndham twice and Derby, Drysdale, Katherine, Mossman, Onslow, and Port Patterson once.



Production of selected weapons for the Australian Army
Australian aircraft production during World War II



Australian Women's Land Army
Australian Women's Army Service
Home front during World War II
Military history of Australia during World War II
Military production during World War II






Adam-Smith, Patsy (1984). Australian Women at War. Melbourne: Thomas Nelson Australia. ISBN 0-17-006408-5. 
Barrett, John. "Living in Australia, 1939 1945." Journal of Australian Studies 1#2 (1977): 107 118.
Beaumont, Joan (2001). Australian Defence: Sources and Statistics. The Australian Centenary History of Defence. Volume VI. Melbourne: Oxford University Press. ISBN 0-19-554118-9. 
Butlin, S.J. (1955). War Economy, 1939 1942. Australia in the War of 1939 1945. Series 4   Civil. Canberra: Australian War Memorial. 
Butlin, S.J.; Schedvin, C.B. (1977). War Economy, 1942 1945. Australia in the War of 1939 1945. Series 4   Civil. Canberra: Australian War Memorial. ISBN 0-642-99406-4. 
Darian-Smith, Kate. On the home front: Melbourne in wartime, 1939 1945 (Oxford University Press, 1990)
Davis, Joan. "'Women's Work' and the Women's Services in the Second World War as Presented in Salt," Hecate (192) v 18#1 pp 64+ online Salt was the magazine of the Australian Army Education Service in the Second World War", with a circulation of 185,000
Hasluck, Paul (1952). The Government and the People 1939 1941. Australia in the War of 1939 1945. Series 4   Civil. Canberra: Australian War Memorial. 
Hasluck, Paul (1970). The Government and the People 1942 1945. Australia in the War of 1939 1945. Series 4   Civil. Canberra: Australian War Memorial. ISBN 978-0-642-99367-0. 
McKernan, Michael (1983). All in! Australia During the Second World War. Melbourne: Thomas Nelson Australia. ISBN 0-17-005946-4. 
McKernan, Michael (2006). The Strength of a Nation. Six years of Australians fighting for the nation and defending the homeland during WWII. Sydney: Allen & Unwin. ISBN 978-1-74114-714-8. 
Mellor, D.P. (1958). The Role of Science and Industry. Australia in the War of 1939 1945. Series 4   Civil. Canberra: Australian War Memorial. 
Saunders, Kay. War on the homefront: state intervention in Queensland 1938 1948 (University of Queensland Press, 1993)
Spear, Jonathan A. "Embedded: the Australian Red Cross in the Second World War." (PhD thesis, University of Melbourne, 2007) online.
Spizzica, Mia. "On the Wrong Side of the Law (War): Italian Civilian Internment in Australia during World War Two." International Journal of the Humanities 9#11 (2012): 121 34.
Willis, Ian C, "The women's voluntary services, a study of war and volunteering in Camden, 1939 1945" PhD thesis, School of History and Politics, Universuty of Wollongong, 2004. online



Year Book Australia, 1944 45 (1947) online, highly detailed statistics plus essays
Year Book Australia, 1946 47 (1949) online, highly detailed statistics plus essays



Australian War Memorial Home front: Second World War