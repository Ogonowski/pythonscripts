The exit status or return code of a process in computer programming is a small number passed from a child process (or callee) to a parent process (or caller) when it has finished executing a specific procedure or delegated task. In DOS, this may be referred to as an errorlevel.
When computer programs are executed, the operating system creates an abstract entity called a process in which the book-keeping for that program is maintained. In multitasking operating systems such as Unix or Linux, new processes can be created by active processes. The process that spawns another is called a parent process, while those created are child processes. Child processes run concurrently with the parent process. The technique of spawning child processes is used to delegate some work to a child process when there is no reason to stop the execution of the parent. When the child finishes executing, it exits by calling the exit system call. This system call facilitates passing the exit status code back to the parent, which can retrieve this value using the wait system call.



The parent and the child can have an understanding about the meaning of the exit statuses. For example, it is common programming practice for a child process to return zero to the parent signifying success. Apart from this return value from the child, other information like how the process exited, either normally or by a signal may also be available to the parent process.
The specific set of codes returned is unique to the program that sets it. Typically it indicates success or failure. The value of the code returned by the function or program may indicate a specific cause of failure. On many systems, the higher the value, the more severe the cause of the error. Alternatively, each bit may indicate a different condition, which are then ored together to give the final value; for example, fsck does this.
Sometimes, if the codes are designed with this purpose in mind, they can be used directly as a branch index upon return to the initiating program to avoid additional tests.



In AmigaOS are three levels defined:
WARN 5
ERROR 10
FAILURE 20



The exit status of an executed shell command is the value returned by the waitid system call or equivalent function. The full 32 bit exit code is only available with the waitid call, but not with older wait interfaces. With older wait interfaces, the visible exit code only contains the low 8 bits of the exit code.
For the shell s purposes, a command which exits with a zero exit status has succeeded. A non-zero exit status indicates failure. This seemingly counter-intuitive scheme is used so there is one well-defined way to indicate success and a variety of ways to indicate various failure modes. When a command is terminated by a signal whose number is N, a shell sets the variable $? to a value greater than 128. Most shells use 128+N, while ksh93 uses 256+N.
If a command is not found, the shell should return a status of 127. If a command is found but is not executable, the return status should be 126. Note that this is not the case for all shells.
If a command fails because of an error during expansion or redirection, the exit status is greater than zero.



The C programming language allows programs exiting or returning from the main function to signal success or failure by returning an integer, or returning the macros EXIT_SUCCESS and EXIT_FAILURE. On Unix-like systems these are equal to 0 and 1 respectively. A C program may also use the exit() function specifying the integer status or exit macro as the first parameter.
Apart from the macros EXIT_SUCCESS and EXIT_FAILURE, the C standard does not define the meaning of return codes. Rules for the use of return codes vary on different platforms (see the platform-specific sections).



In DOS terminology, an errorlevel is an integer exit code returned by an executable program or subroutine. Errorlevels typically range from 0 to 255. In DOS there are only 256 error codes available.



In Java, any method can call System.exit(int status), unless a security manager does not permit it. This will terminate the currently running Java Virtual Machine. "The argument serves as a status code; by convention, a nonzero status code indicates abnormal termination."



In OpenVMS, success is indicated by odd values and failure by even values. The value is a 32 bit integer with sub-fields: control bits, facility number, message number and severity. Severity values are divided between success (Success, Informational) and failure (Warning, Error, Fatal).



In Unix and other POSIX-compatible systems, the older wait() and waitpid() system calls set a status value of type int packed as a bitfield with various types of child termination information. If the child terminated by exiting (as determined by the WIFEXITED macro; the usual alternative being that it died from an uncaught signal), SUS specifies that the low-order 8 bits of the exit status can be retrieved from the status value using the WEXITSTATUS macro in wait.h; when using the POSIX waitid() system call (added with POSIX-2001), the range of the status is no longer limited and can be in the full integer range. In the waitid() system call, the child exit status and other information is no longer in a bitfield but in the structure siginfo_t, the W* macros do no longer apply.
POSIX-compatible systems typically use a convention of zero for success and non zero for error. Some conventions have developed as to the relative meanings of various error codes; for example GNU recommend that codes with the high bit set be reserved for serious errors, and FreeBSD have documented an extensive set of preferred interpretations. Meanings for 15 status codes 64 through 78 are defined in sysexits.h. These historically derive from sendmail and other message transfer agents, but they have since found use in many other programs.



Windows uses 32-bit signed integers as exit codes. If a process fails initialization, a Windows system error code may be returned. Windows system error codes are available online.
Exit codes are directly referenced, for example, by the command line interpreter CMD.exe in the errorlevel terminology inherited from DOS. .NET Framework processes and the Windows PowerShell refer to it as the ExitCode property of the Process object.



Return statement
true and false (Unix)


