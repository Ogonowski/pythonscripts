In finance and economics, a financial institution is an institution that provides financial services for its clients or members. One of the most important financial services provided by a financial institution is acting as a financial intermediary. Most financial institutions are regulated by the government.



Financial institutions provide services as intermediaries of financial markets. Broadly speaking, there are three major types of financial institutions:
Depositary institutions   deposit-taking institutions that accept and manage deposits and make loans, including banks, building societies, credit unions, trust companies, and mortgage loan companies;
Contractual institutions   insurance companies and pension funds; and
Investment institutions   investment banks, underwriters, brokerage firms.
Some experts see a trend toward homogenisation of financial institutions, meaning a tendency to invest in similar areas and have similar business strategies. A consequence of this might be fewer banks serving specific target groups, for example small-scale producers could be under served.



Standard Settlement Instructions (SSIs) are the agreements between two financial institutions which fix the receiving agents of each counterparty in ordinary trades of some type. These agreements allow traders to make faster trades since the time used to settle the receiving agents is conserved. Limiting the trader to an SSI also lowers the likelihood of a fraud. SSIs are used by financial institutions to facilitate fast and accurate cross-border payments.



Financial institutions in most countries operate in a heavily regulated environment because they are critical parts of countries' economies, due to economies' dependence on them to grow the money supply via fractional reserve lending. Regulatory structures differ in each country, but typically involve prudential regulation as well as consumer protection and market stability. Some countries have one consolidated agency that regulates all financial institutions while others have separate agencies for different types of institutions such as banks, insurance companies and brokers.
Countries that have separate agencies include the United States, where the key governing bodies are the Federal Financial Institutions Examination Council (FFIEC), Office of the Comptroller of the Currency - National Banks, Federal Deposit Insurance Corporation (FDIC) State "non-member" banks, National Credit Union Administration (NCUA) - Credit Unions, Federal Reserve (Fed) - "member" Banks, Office of Thrift Supervision - National Savings & Loan Association, State governments each often regulate and charter financial institutions.
Countries that have one consolidated financial regulator include: Norway with the Financial Supervisory Authority of Norway, Hong Kong with Hong Kong Monetary Authority, Germany with Federal Financial Supervisory Authority and Russia with Central Bank of Russia. See also List of financial regulatory authorities by country.





