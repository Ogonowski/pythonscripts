A flash file system is a file system designed for storing files on flash memory based storage devices. While the flash file systems are closely related to file systems in general, they are optimized for the nature and characteristics of flash memory (such as to avoid write amplification), and for use in particular operating systems.



While a block device layer can emulate a disk drive so that a general-purpose file system can be used on a flash-based storage device, this is suboptimal for several reasons:
Erasing blocks: flash memory blocks have to be explicitly erased before they can be written to. The time taken to erase blocks can be significant, thus it is beneficial to erase unused blocks while the device is idle.
Random access: general-purpose file systems are optimized to avoid disk seeks whenever possible, due to the high cost of seeking. Flash memory devices impose no seek latency.
Wear leveling: flash memory devices tend to wear out when a single block is repeatedly overwritten; flash file systems are designed to spread out writes evenly.
Log-structured file systems have all the desirable properties for a flash file system. Such file systems include JFFS2 and YAFFS.
Because of the particular characteristics of flash memory, it is best used with either a controller to perform wear leveling and error correction or specifically designed flash file systems, which spread writes over the media and deal with the long erase times of NAND flash blocks. The basic concept behind flash file systems is: when the flash store is to be updated, the file system will write a new copy of the changed data over to a fresh block, remap the file pointers, then erase the old block later when it has time.
In practice, flash file systems are only used for Memory Technology Devices (MTDs), which are embedded flash memories that do not have a controller. Removable flash memory cards and USB flash drives have built-in controllers to manage MTD with dedicated algorithms, like wear leveling, bad block recovery, power loss recovery, garbage collection and error correction, so use of a flash file system has limited benefit.
Flash-based memory devices are becoming more prevalent as the number of mobile devices is increasing, the cost per memory size decreases, and the capacity of flash memory chips increases.



The earliest flash file system, managing an array of flash as a freely writable disk, was TrueFFS by M-Systems of Israel, presented as a software product in PC-Card Expo, Santa Clara CA, July 1992 and patented in 1993.
One of the earliest flash file systems was Microsoft's FFS2, for use with MS-DOS, released in autumn 1992. FFS2 was preceded by an earlier product, called "FFS", which however fell short of being a flash file system, managing a flash array as WORM (Write Once Read Many) space rather than as a freely writable disk.
Around 1994, the PCMCIA, an industry group, approved the Flash Translation Layer (FTL) specification, based on the design of M-Systems' TrueFFS. The specification was authored and jointly proposed by M-Systems and SCM Microsystems, who also provided the first working implementations of FTL. Endorsed by Intel and other industry leaders, FTL became a popular flash file system design in non-PCMCIA media as well.



JFFS, JFFS2 and YAFFS
JFFS was the first flash-specific file system for Linux, but it was quickly superseded by JFFS2, originally developed for NOR flash. Then YAFFS was released in 2002, dealing specifically with NAND flash, and JFFS2 was updated to support NAND flash too.
UBIFS
UBIFS has been merged since Linux 2.6.22  in 2008. UBIFS has been actively developed from its initial merge. UBIFS has documentation hosted at infradead.org along with JFFS2 and MTD drivers. Some initial comparison show UBIFS with compression faster than F2FS.
LogFS
LogFS, another Linux flash-specific file system, is currently being developed to address the scalability issues of JFFS2.
F2FS
F2FS (Flash-Friendly File System) was added to the Linux kernel 3.8. Instead of being targeted at speaking directly to raw flash devices, F2FS is designed to be used on flash-based storage devices that already include a flash translation layer, such as SD cards.



These are union filesystems, that allow multiple filesystems to be combined and presented to the user as a single tree. This allows the system designer to place parts of the operating system that are nominally read-only on different media to the normal read-write areas. OpenWrt is usually installed on raw flash chips without FTL. It uses overlayfs to combine a compressed read-only SquashFS with JFFS2.



Some subsystems are often called flash file systems, while they are more precisely block drivers performing different translations, and they actually do not have a file system interface. Such subsystems include the following:



Despite the name, TrueFFS is not a file system at all; it does not provide a file system interface but a disk interface. TrueFFS is correctly termed a flash translation layer. True flash file system or TrueFFS is designed to run on a raw solid-state drive (most modern consumer SSDs are not raw). TrueFFS implements error correction, bad block re-mapping and wear leveling. Externally, TrueFFS presents a normal hard disk interface.
TrueFFS was created by M-Systems  on well-known "DiskOnChip 2000" product line, who were acquired by Sandisk in 2006. A derivative of TrueFFS, called TFFS or TFFS-lite, is found in the VxWorks operating system, where it functions as a flash translation layer, not as a fully functional file system. A flash translation layer is used to adapt a fully functional file system to the constraints and restrictions imposed by flash memory devices.



ExtremeFFS is a technology being developed by SanDisk allowing for improved random write performance in flash memory compared to traditional systems such as TrueFFS. Sandisk claims that the technology improves random access speed in Solid-state drives by a factor of 100. The company plans on using ExtremeFFS in an upcoming multi-level cell implementation of NAND flash memory.



List of flash file systems
Wear leveling
Write amplification






Presentation on various Flash File Systems - 9/24/2007
Article regarding various Flash File Systems - 2005 USENIX Annual Conference
Survey of various Flash File Systems - 8/10/2005
Anatomy of Linux Flash File Systems - 5/20/2008