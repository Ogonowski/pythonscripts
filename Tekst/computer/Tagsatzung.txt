The Federal Diet of Switzerland or Tagsatzung (German: Eidgen ssische Tagsatzung; French: Di te f d rale; Italian: Dieta federale) was the legislative and executive council of the Swiss confederacy from the beginnings until the formation of the Swiss federal state in 1848.
It was a meeting of delegates of the individual cantons. Its power was very limited, since the cantons were essentially sovereign. It was the most important institution of the Old Swiss Confederacy.
It is the representative organ with the longest longevity in world history.
The composition and functions of the Tagsatzung evolved since the 13th century. It was notably re-organized by the Federal Treaty (Bundesvertrag) of 7 August 1815.



The presiding canton was known as the Vorort, usually the canton which had called the Tagsatzung. The Tagsatzung was held in various locations, Baden being popular because of its hot springs.
The last three presiding cantons were Bern, Lucerne and Z rich. Bern was chosen as the 'federal city' (Bundesstadt, with deliberate avoidance of the term 'capital' or Hauptstadt) in 1848.



List of Presidents of the Swiss Diet
Landammann
Landsgemeinde






Tagsatzung in German, French and Italian in the online Historical Dictionary of Switzerland.
Vorort in German, French and Italian in the online Historical Dictionary of Switzerland.