Hitachi, Ltd. ( , Kabushiki-gaisha Hitachi Seisakusho) (Japanese pronunciation: [ i tat i]) is a Japanese multinational conglomerate company headquartered in Chiyoda, Tokyo, Japan. It is the parent of the Hitachi Group (Hitachi Gur pu) and forms part of the DKB Group of companies. Hitachi is a highly diversified company that operates eleven business segments: Information & Telecommunication Systems, Social Infrastructure, High Functional Materials & Components, Financial Services, Power Systems, Electronic Systems & Equipment, Automotive Systems, Railway & Urban Systems, Digital Media & Consumer Products, Construction Machinery and Other Components & Systems.
Hitachi is listed on the Tokyo Stock Exchange and is a constituent of the Nikkei 225 and TOPIX indices. It is ranked 38th in the 2012 Fortune Global 500 and 129th in the 2012 Forbes Global 2000. On January 21, 2014, numerous tech articles around the globe published findings from the cloud storage provider Backblaze that Hitachi hard disks are the most reliable among prominent hard disk manufactures.




Hitachi was founded in 1910 by electrical engineer Namihei Odaira in Ibaraki Prefecture. The company's first product was Japan's first 5-horsepower electric induction motor, initially developed for use in copper mining. Odaira's company soon became the domestic leader in electric motors and electric power industry infrastructure.
The company began as an in-house venture of Fusanosuke Kuhara's mining company in Hitachi, Ibaraki. Odaira moved headquarters to Tokyo in 1918. Long before that, he coined the company s toponymic name by superimposing two kanji characters: hi meaning  sun  and tachi meaning  rise . The young company's national aspirations were conveyed by its original brand mark, which evoked Japan's imperial rising sun flag.
Hitachi America, Ltd. was established in 1959. Hitachi Europe, Ltd. was established in 1982.
In March 2011, Hitachi agreed to sell its hard disk drive subsidiary, Hitachi Global Storage Technologies, to Western Digital for a combination of cash and shares worth US$4.3 billion. Due to concerns of a duopoly of WD and Seagate by the EU Commission and the Federal Trade Commission, Hitachi's 3.5" HDD division was sold to Toshiba. The transaction was completed in March 2012.
Hitachi entered talks with Mitsubishi Heavy Industries in August 2011 about a potential merger of the two companies, in what would have been the largest merger between two Japanese companies in history. The talks subsequently broke down and were suspended.
In October 2012, Hitachi agreed to acquire the United Kingdom-based nuclear energy company Horizon Nuclear Power, which plans to construct up to six nuclear power plants in the UK, from E.ON and RWE for  700 million.
In November 2012, Hitachi and Mitsubishi Heavy Industries agreed to merge their thermal power generation businesses into a joint venture to be owned 65% by Mitsubishi Heavy Industries and 35% by Hitachi. The joint venture begun operations in February 2014.







ATMs
Disk array subsystems
Mainframe computers
Outsourcing services
Servers
Software
System integration
Telecommunications equipment



Nuclear, thermal and hydroelectric power plants
Wind Power Generation Systems




Elevators
Escalators
Industrial machinery and plants
Railway vehicles and systems



LCDs
Medical electronics equipment
Power tools
Semiconductor manufacturing equipment
Test and measurement equipment




Hydraulic Excavators
Forestry Equipment
Mechanical & Hydraulic Cranes
Mining Dump Trucks
Crawler Dump trucks
Wheel Loaders



Circuit boards and materials
Copper products
High grade casting components and materials
Magnetic materials and components
Semiconductor and Display Related Materials
Specialty steels
Wires and cables




Car Information Systems
Drive Control
Electric Powertrain Systems
Engine Management Systems



Military vehicles
Vetronics
Crisis management
C4I systems
Satellite image processing systems
Social Infrastructure security business (in coordination with Hitachi's Infrastructure Systems Group)
Electric propulsion technology
Electro-mechanical systems (including some robotics research & development)
Advanced Combat Infantry Equipment System [ACIES] (JSDF) - Primary contractor




Batteries
Hard disk drives (Separated division for this product line as Hitachi Global Storage Technologies, then HGST was purchased by Western Digital)
Information Storage Media
LCDs




Air conditioning equipment
Hitachi Magic Wand
LCD projectors
Optical disc drives (Joint venture with optical disc drive division of LG as Hitachi-LG Data Storage)
Plasma and LCD Televisions
Refrigerators
Room air conditioners
Washing machines
Note: A new product from Hitachi called Memory glass is to be introduced in 2015. It is a high density information storage medium utilizing laser etched/readable Fused quartz.



Leasing
Loan Guarantees
Invoice Finance (via the Hitachi Capital arm of the business)



Logistics
Property management



(Pre-war)
Hitachi T.2
Hitachi TR.2



Hitachi Hatsukaze










Hitachi Solutions America is a consulting firm and systems integrator focusing primarily on Microsoft Dynamics. The firm utilizes AX and CRM from the Dynamics family to provide customers with a broad base of solutions. The company is international, with subsidiaries residing in the United Kingdom, Canada, and India.




Hitachi Consulting is an international management and technology consulting firm with headquarters in Dallas, Texas. It was founded in 2000 and currently employs approximately 5,000 people across the United States, Japan, the United Kingdom, India, Spain, Portugal, Germany and China.




Hitachi Data Systems (HDS) is a wholly owned subsidiary of Hitachi which provides hardware, software and services to help companies manage their digital data. Its flagship products are the Virtual Storage Platform (for enterprise storage), Hitachi Unified Storage VM for large sized companies, Hitachi Unified Storage for small and mid-sized companies, Hitachi Content Platform (archiving and cloud architecture), Hitachi Command Suite (for storage management), Hitachi TrueCopy and Hitachi Universal Replicator (for remote replication), and the Hitachi NAS Platform.



Hitachi manufactures many types of electronic products including TVs, Camcorders, Projectors and Recording Media under its own brand name.



(Includes Intellectual Property Group [subgroup])
Technology Strategy Office
Central Research Laboratory
Hitachi Research Laboratory - Includes Mechanical Engineering Research Laboratory (Robotics)
Yokohama Research Laboratory
Design Division
Overseas research centers
In-house/Business division
-Development center
-Development & Design section



Hitachi provides various defense related/derived products & services (see Defense Systems entry in the Products and services section above).



Among other things, Hitachi Metals supplies materials for aircraft engines and fuselage components (e.g. landing gear), along with finished components for same and other aerospace applications. It also provides materials, components and tools for the automotive and electronics industries.



Hitachi manufactures many types of tools including chainsaws, drills, woodworking power tools. Some are branded Koki Tanaka.



Hitachi Plant Technologies, Ltd., along with its subsidiaries, engages in the design, development, manufacture, sale, servicing, and execution of social and industrial infrastructure machinery, mechatronics, air-conditioning systems, industrial plants, and energy plant equipment in Asia and internationally.




Hitachi Rail is involved in designing and manufacturing many models of Shinkansen vehicles, including the N700 Series Shinkansen. and the THSR 700T for Taiwan High Speed Rail.
Hitachi markets a general-purpose train known as the "A-train", which utilises double-skin friction stir welded aluminium body construction. The A-train concept can be customised to form a commuter train like the automated 3000 series train for the Nanakuma Line, a limited express train like the E257 series, or a high-speed train such as the Class 395 operating in the UK. In June 2008, Hitachi also submitted a tender for the UK's Intercity Express Programme.
Hitachi also develops rolling stock for many metro systems, including the Fukuoka Subway, Tokyo Metro, Yokohama Municipal Subway, and MARTA, as well as operating vehicles via Hitachi Monorail.
Hitachi and Mitsubishi Heavy Industries agreed to cooperate in the field of international intra-city railway systems in 2010.



Hitachi Works is the oldest member of the Hitachi Group and consists of three factories: Kaigan Works, Yamate Works, and Rinkai Works. Yamate Works, the oldest of the three factories, was founded in 1910 by Namihei Odaira as an electrical equipment repair and manufacturing facility. This facility was named Hitachi, after the Hitachi Mine near Hitachi, Ibaraki, and is regarded as the ancestral home of Hitachi, Ltd.
Many management trainees intern at Hitachi Works before being permanently assigned to other Hitachi divisions. Senior management personnel are often participants in rotations at Hitachi Works for a few years as their career develops towards eventual head office stature. As a result, many of the senior managers of Hitachi Ltd have passed through Hitachi Works.
Spin-off entities from Hitachi Works include Hitachi Cable (1956) and Hitachi Canadian Industries (1988).







Hitachi Global Storage Technologies (Hitachi GST) manufactures computer hard drives. There are 3 main ranges: Hitachi Travelstar, Hitachi Deskstar, and Hitachi Ultrastar.
On March 7, 2011 Hitachi Global Storage Technologies was purchased by Western Digital Corporation for $3.5 billion in cash and $750 million in Western Digital common stock.



Hitachi Printing Systems was established in 1980 and was acquired by Ricoh in 2004, becoming Ricoh Printing Systems, Ltd.



In August 2011 it was announced that Hitachi would donate an electron microscope to each of five universities in Indonesia (the University of North Sumatra in Medan, the Indonesian Christian University in Jakarta, Padjadjaran University in Bandung, General Soedirman University in Purwokerto and Muhammadiyah University in Malang).


