In computer architecture, a processor register is a small amount of storage available as part of a digital processor, such as a central processing unit (CPU). Such registers are typically addressed by mechanisms other than main memory and can be accessed faster. Almost all computers, load-store architecture or not, load data from a larger memory into registers where it is used for arithmetic, manipulated or tested by machine instructions. Manipulated data is then often stored back into main memory, either by the same instruction or a subsequent one. Modern processors use either static or dynamic RAM as main memory, with the latter usually accessed via one or more cache levels.
Processor registers are normally at the top of the memory hierarchy, and provide the fastest way to access data. The term normally refers only to the group of registers that are directly encoded as part of an instruction, as defined by the instruction set. However, modern high-performance CPUs often have duplicates of these "architectural registers" in order to improve performance via register renaming, allowing parallel and speculative execution. Modern x86 design acquired these techniques around 1995 with the releases of Pentium Pro, Cyrix 6x86, Nx586, and AMD K5.
A common property of computer programs is locality of reference, which refers to accessing the same values repeatedly and holding frequently used values in registers to improve performance; this is what makes fast registers and caches meaningful. Allocating frequently used variables to registers can be critical to a program's performance; this register allocation is performed either by a compiler in the code generation phase, or manually by an assembly language programmer.



Registers are normally measured by the number of bits they can hold, for example, an "8-bit register" or a "32-bit register". A processor often contains several kinds of registers, that can be classified according to their content or instructions that operate on them:
User-accessible registers can be read or written by machine instructions. The most common division of user-accessible registers is into data registers and address registers.
Data registers can hold numeric values such as integer and, in some architectures, floating-point values, as well as characters, small bit arrays and other data. In some older and low end CPUs, a special data register, known as the accumulator, is used implicitly for many operations.
Address registers hold addresses and are used by instructions that indirectly access primary memory.
Some processors contain registers that may only be used to hold an address or only to hold numeric values (in some cases used as an index register whose value is added as an offset from some address); others allow registers to hold either kind of quantity. A wide variety of possible addressing modes, used to specify the effective address of an operand, exist.
The stack pointer is used to manage the run-time stack. Rarely, other data stacks are addressed by dedicated address registers, see stack machine.

General-purpose registers (GPRs) can store both data and addresses, i.e., they are combined data/address registers and rarely the register file is unified to include floating point as well.
Status registers hold truth values often used to determine whether some instruction should or should not be executed.
Floating-point registers (FPRs) store floating point numbers in many architectures.
Constant registers hold read-only values such as zero, one, or pi.
Vector registers hold data for vector processing done by SIMD instructions (Single Instruction, Multiple Data).
Special-purpose registers (SPRs) hold program state; they usually include the program counter, also called the instruction pointer, and the status register; the program counter and status register might be combined in a program status word (PSW) register. The aforementioned stack pointer is sometimes also included in this group. Embedded microprocessors can also have registers corresponding to specialized hardware elements.
In some architectures, model-specific registers (also called machine-specific registers) store data and settings related to the processor itself. Because their meanings are attached to the design of a specific processor, they cannot be expected to remain standard between processor generations.
Memory Type Range Registers (MTRRs)

Internal registers   registers not accessible by instructions, used internally for processor operations.
Instruction register, holding the instruction currently being executed.
Registers related to fetching information from RAM, a collection of storage registers located on separate chips from the CPU:
Memory buffer register (MBR)
Memory data register (MDR)
Memory address register (MAR)

Hardware registers are similar, but occur outside CPUs.
In some architectures, such as SPARC and MIPS, the first or last register in the integer register file is a pseudo-register in a way that it is hardwired to always return zero when read (mostly to simplify indexing modes), and it cannot be overwritten. In Alpha this is also done for the floating-point register file. As a result of this, register files are commonly quoted as having one register more than how many of them are actually usable; for example, 32 registers are quoted when only 31 of them fit within the above definition of a register.



The table shows the number of registers of several mainstream architectures. Note that in x86-compatible processors the stack pointer (ESP) is counted as an integer register, even though there are a limited number of instructions that may be used to operate on its contents. Similar caveats apply to most architectures.
Although all of the above listed architectures are different, almost all are a basic arrangement known as the Von Neumann architecture, first proposed by mathematician John von Neumann.



The number of registers available on a processor and the operations that can be performed using those registers has a significant impact on the efficiency of code generated by optimizing compilers. The Strahler number of an expression tree gives the minimum number of registers required to evaluate that expression tree.


