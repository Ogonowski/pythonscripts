Eric S. Roberts is an American computer scientist noted for his contributions to computer science education through textbook authorship and his leadership in computing curriculum development. He is a co-chair of the ACM Education Council, former co-chair of the ACM Education Board, and a former member of the SIGCSE Board. He led the Java task force in 1994. He is currently a Professor at Stanford University.



Roberts received a A. B. in Applied Mathematics from Harvard University in 1973. He received a S. M. in Applied Mathematics from Harvard University in June 1974 and a Ph.D in Applied Mathematics from Harvard University in 1980.
He then joined the Department of Computer Science at Wellesley College as an assistant professor in 1980. In 1984 1985 he was a visiting lecturer in Computer Science at Harvard University. In 1990 he was an associate professor at Stanford University and promoted to professor (teaching) of Computer Science in 1990. While at Stanford he has also held several other positions such as associate chair and director of indergraduate studies from 1997 to 2002, and senior associate dean for student affairs from 2001 to 2003.
Roberts has written several introductory computer science textbooks, including
Thinking Recursively
The Art and Science of C
Programming Abstractions in C
Thinking Recursively with Java
The Art and Science of Java



Roberts has several notable awards in computer science.
ACM Karl V. Karlstrom Outstanding Educator Award in 2012.
IEEE Computer Society s 2012 Taylor L. Booth Education Award.
ACM Fellow in 2007.






Stanford University: Eric S. Roberts, Department of Computer Science