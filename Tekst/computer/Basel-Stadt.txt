The Canton of Basel-Stadt (German:  Basel-Stadt  "Basel-City"; in French B le-Ville, in Italian Basilea Citt ) is one of the 26 cantons of Switzerland. The city of Basel and the municipalities of Bettingen and Riehen form its territory.



The canton of Basel-Stadt was created when the historic canton of Basel was divided in 1833, following political quarrels and armed conflict in the canton. Some of these were concerned with the rights of the population in the agricultural areas. They ultimately led to the separation of the canton Basel-Landschaft from the city of Basel on 26 August 1833. Since then, there has been a movement for reunification. This movement gained momentum after 1900 when many parts of Basel-Landschaft became industrialized. The two half-cantons agreed in principle to reunite, but in 1969, and again in September 2014, the people of Basel-Landschaft voted against this proposal in favour of retaining their independence.




The canton of Basel-Stadt is located in the north of Switzerland. It borders Germany and France to the north (the three countries meet at the Dreil ndereck), and Basel-Landschaft to the south. Basel is located at the so-called 'knee' of the River Rhine, at the point where from the west the little Birsig River joins the Rhine from the left, and where the Rhine itself switches from flowing in a westerly direction to a northerly flow.
Bettingen, Riehen and a part of Basel city lie on the east bank of the Rhine, bordered on three sides by the German state of Baden-W rttemberg. The rest of the canton lies on the west bank of the Rhine.
The area of the canton is 37 km , making Basel-Stadt the smallest canton in Switzerland.



Basel-Stadt is a half canton. This means that the canton only sends one representative to the Council of States. The capital of the canton Basel-Stadt is the city of Basel. The present constitution of the canton dates from 1889.
The parliament of the canton is the Grand Council, which has 100 members, who are elected for four years at a time. There are eight different political parties represented in the parliament; the largest party is the Social Democratic Party, with 32 seats.
The executive of the canton (Regierungsrat) is made up of seven members. Currently (2006) there are five different political parties represented in the executive.



The population of the canton (as of 30 April 2015) is 196,850. As of 2007, the population included 56,106 foreigners, or about 30.29% of the total population. The population (as of 2000) is nearly evenly split between Roman Catholic (25%) and Protestant (27%). About 10% of the population is classed as "Other Religion" while 36% do not belong to any organized religion.




The chemical industry and the pharmaceutical industry are of greatest significance in the canton. There are a number of multinationals in the city of Basel, attracting workers from both cantons of Basel and the areas across the border in France and Germany. Banking and finance are important as is the service sector in general. Small and middle-sized businesses employ a significant number of people, both in the city as the two municipalities.
Economically the neighbouring lands in Germany and France are not separated from the area of the canton of Basel-Stadt. Good transport links across the border as well as supportive local governments facilitate this link.
The fact that three nation- states come together in one spot near Basel (Dreil ndereck) attracts some tourists. The site is clearly identified and a popular destination for primary school classes. The carnival attracts large number of people from across Switzerland and the neighbouring countries.




There is an international airport at Basel-Mulhouse, actually located 4 km inside French territory but with customs-free access from the city. The canton is well connected by both trains and motorways to the rest of Switzerland and the neighbouring areas in France and Germany.
Basel is a major railway station of Switzerland, connected to Paris and Berlin with direct fast trains.
There is a port at Basel for ships on the river Rhine. This port is of great significance to landlocked Switzerland, as it offers the country's only direct connection to the sea. The port benefits from good connections to both rail and road.




The carnival of the city of Basel (Basler Fasnacht) is a major cultural event in the year. The carnival is one of the biggest in Switzerland and attracts large crowds, despite the fact that many of its central traditions are played out in the early morning starting at 4 AM (Morgestraich) and followed by a continuous run of festivities for 72 hours.
The Autumn Fair in Basel (Basler Herbstmesse) is the biggest in Switzerland.
The canton of Basel is renowned for two of its biscuits. The Basler L ckerli is a hard biscuit made of honey, almonds, candied peel and Kirsch and is enjoyed as a speciality all year round. The Basler Brunsli is made of almonds and generally enjoyed at Christmas all around Switzerland. A famous cultural ambassador is the Basel Boys Choir.
The Basel Messe convention center is host to several international events. The largest are Art Basel, an art show for Modern and contemporary works; and BaselWorld, a major watch and jewellery show.


