The Computer Laboratory is the computer science department of the University of Cambridge. As of 2007, it employs 35 academic staff, 25 support staff, 35 affiliated research staff, and about 155 research students. The current head of department is Professor Andy Hopper.
The Computer Laboratory built and operated the world s first fully operational practical stored program computer (EDSAC, 1949) and offered the world s first postgraduate taught course in computer science in 1953. It currently offers a 3-year undergraduate course and a 1-year masters course (with an optional natural language processing theme). Recent research has focused on virtualization, security, usability, formal verification, formal semantics of programming languages, computer architecture, natural language processing, wireless networking, biometric identification, routing, positioning systems and sustainability ( Computing for the future of the planet ). Members of the Computer Laboratory have been involved in the creation of many successful UK IT companies such as Acorn, ARM, nCipher and XenSource.






As of 2015 the lab employs 20 Professors: 

Other staff include Robert Watson and Michael Kuhn



Former staff in the laboratory include:



The lab has been led by:
1949 Maurice Wilkes
1980 Roger Needham
1996 Robin Milner
1999 Ian Leslie
2004 Andy Hopper






A number of companies have been founded by staff and graduates. Their names were featured in the new laboratory entrance in 2012. Some cited examples of successful companies are ARM, Autonomy, Aveva, CSR and Domino. One common factor they share is that key staff or founder members are "drenched in university training and research". The Cambridge Computer Lab Ring was praised for its "tireless work" by Andy Hopper in 2012, at its tenth anniversary dinner.



It was founded as the Mathematical Laboratory under the leadership of John Lennard-Jones on 14 May 1937, though it did not get properly established until after World War II. The new laboratory was housed in the North Wing of the former Anatomy School, on the New Museums Site. Upon its foundation, it was intended to provide a computing service for general use, and to be a centre for the development of computational techniques in the University. The Cambridge Diploma in Computer Science was the world s first postgraduate taught course in computing, starting in 1953.
In October 1946, work began under Maurice Wilkes on EDSAC (Electronic Delay Storage Automatic Calculator), which subsequently became the world s first fully operational and practical stored program computer when it ran its first program on 6 May 1949. It inspired the world s first business computer, LEO. It was replaced by EDSAC 2, the first microcoded and bitsliced computer, in 1958.
In 1961, David Hartley developed Autocode, one of the first high-level programming languages, for EDSAC 2. Also in that year, proposals for Titan, based on the Ferranti Atlas machine, were developed. Titan became fully operational in 1964 and EDSAC 2 was retired the following year. In 1967, a full ( 24/7 ) multi-user time-shared service for up to 64 users was inaugurated on Titan.
In 1970, the Mathematical Laboratory was renamed the Computer Laboratory, with separate departments for Teaching and Research and the Computing Service, providing computing services to the university and its colleges. The two did not fully separate until 2001, when the Computer Laboratory moved out to the new William Gates building in West Cambridge, off Madingley Road, leaving behind an independent Computing Service.
In 2002, the Computer Laboratory launched the Cambridge Computer Lab Ring, a graduate society named after the Cambridge Ring network.


