In logic, a many-valued logic (also multi- or multiple-valued logic) is a propositional calculus in which there are more than two truth values. Traditionally, in Aristotle's logical calculus, there were only two possible values (i.e., "true" and "false") for any proposition. Classical two-valued logic may be extended to n-valued logic for n greater than 2. Those most popular in the literature are three-valued (e.g.,  ukasiewicz's and Kleene's, which accept the values "true", "false", and "unknown"), the finite-valued (finitely-many valued) with more than three values, and the infinite-valued (infinitely-many valued), such as fuzzy logic and probability logic.



The first known classical logician who didn't fully accept the law of excluded middle was Aristotle (who, ironically, is also generally considered to be the first classical logician and the "father of logic"). Aristotle admitted that his laws did not all apply to future events (De Interpretatione, ch. IX), but he didn't create a system of multi-valued logic to explain this isolated remark. Until the coming of the 20th century, later logicians followed Aristotelian logic, which includes or assumes the law of the excluded middle.
The 20th century brought back the idea of multi-valued logic. The Polish logician and philosopher Jan  ukasiewicz began to create systems of many-valued logic in 1920, using a third value, "possible", to deal with Aristotle's paradox of the sea battle. Meanwhile, the American mathematician, Emil L. Post (1921), also introduced the formulation of additional truth degrees with n   2, where n are the truth values. Later, Jan  ukasiewicz and Alfred Tarski together formulated a logic on n truth values where n   2. In 1932 Hans Reichenbach formulated a logic of many truth values where n infinity. Kurt G del in 1932 showed that intuitionistic logic is not a finitely-many valued logic, and defined a system of G del logics intermediate between classical and intuitionistic logic; such logics are known as intermediate logics.






Kleene's "(strong) logic of indeterminacy" K3 (sometimes ) and Priest's "logic of paradox" add a third "undefined" or "indeterminate" truth value I. The truth functions for negation ( ), conjunction ( ), disjunction ( ), implication ( K), and biconditional ( K) are given by:
The difference between the two logics lies in how tautologies are defined. In K3 only T is a designated truth value, while in P3 both T and I are (a logical formula is considered a tautology if it evaluates to a designated truth value). In Kleene's logic I can be interpreted as being "underdetermined", being neither true nor false, while in Priest's logic I can be interpreted as being "overdetermined", being both true and false. K3 does not have any tautologies, while P3 has the same tautologies as classical two-valued logic.



Another logic is Bochvar's "internal" three-valued logic () also called Kleene's weak three-valued logic. Except for negation and biconditional, its truth tables are all different from the above.
The intermediate truth value in Bochvar's "internal" logic can be described as "contagious" because it propagates in a formula regardless of the value of any other variable.



Belnap's logic B4 combines K3 and P3. The overdetermined truth value is here denoted as B and the underdetermined truth value as N.



In 1932 G del defined a family  of many-valued logics, with finitely many truth values , for example  has the truth values  and  has . In a similar manner he defined a logic with infinitely many truth values, , in which the truth values are all the real numbers in the interval . The designated truth value in these logics is 1.
The conjunction  and the disjunction  are defined respectively as the maximum and minimum of the operands:

Negation  and implication  are defined as follows:

G del logics are completely axiomatisable, that is to say it is possible to define a logical calculus in which all tautologies are provable.



Implication  and negation  were defined by Jan  ukasiewicz through the following functions:

At first  ukasiewicz used these definition in 1920 for his three-valued logic , with truth values . In 1922 he developed a logic with infinitely many values , in which the truth values spanned the real numbers in the interval . In both cases the designated truth walue was 1.
By adopting truth values defined in the same way as for G del logics , it is possible to create a finitely-valued family of logics , the abovementioned  and the logic , in which the truth values are given by the rational numbers in the interval . The set of tautologies in  and  is identical.



In product logic we have truth values in the interval , a conjunction  and an implication , defined as follows

Additionally there is a negative designated value  that denotes the concept of false. Through this value it is possible to define a negation  and an additional conjunction  as follows:



In 1921 Post defined a family of logics  with (as in  and ) the truth values . Negation  and disjunction  are defined as follows:






See Logical matrix






Logics are usually systems intended to codify rules for preserving some semantic property of propositions across transformations. In classical logic, this property is "truth." In a valid argument, the truth of the derived proposition is guaranteed if the premises are jointly true, because the application of valid steps preserves the property. However, that property doesn't have to be that of "truth"; instead, it can be some other concept.
Multi-valued logics are intended to preserve the property of designationhood (or being designated). Since there are more than two truth values, rules of inference may be intended to preserve more than just whichever corresponds (in the relevant sense) to truth. For example, in a three-valued logic, sometimes the two greatest truth-values (when they are represented as e.g. positive integers) are designated and the rules of inference preserve these values. Precisely, a valid argument will be such that the value of the premises taken jointly will always be less than or equal to the conclusion.
For example, the preserved property could be justification, the foundational concept of intuitionistic logic. Thus, a proposition is not true or false; instead, it is justified or flawed. A key difference between justification and truth, in this case, is that the law of excluded middle doesn't hold: a proposition that is not flawed is not necessarily justified; instead, it's only not proven that it's flawed. The key difference is the determinacy of the preserved property: One may prove that P is justified, that P is flawed, or be unable to prove either. A valid argument preserves justification across transformations, so a proposition derived from justified propositions is still justified. However, there are proofs in classical logic that depend upon the law of excluded middle; since that law is not usable under this scheme, there are propositions that cannot be proven that way.






Known applications of many-valued logic can be roughly classified into two groups. The first group uses many-valued logic domain to solve binary problems more efficiently. For example, a well-known approach to represent a multiple-output Boolean function is to treat its output part as a single many-valued variable and convert it to a single-output characteristic function. Other applications of many-valued logic include design of Programmable Logic Arrays (PLAs) with input decoders, optimization of finite state machines, testing, and verification.
The second group targets the design of electronic circuits which employ more than two discrete levels of signals, such as many-valued memories, arithmetic circuits, Field Programmable Gate Arrays (FPGA) etc. Many-valued circuits have a number of theoretical advantages over standard binary circuits. For example, the interconnect on and off chip can be reduced if signals in the circuit assume four or more levels rather than only two. In memory design, storing two instead of one bit of information per memory cell doubles the density of the memory in the same die size. Applications using arithmetic circuits often benefit from using alternatives to binary number systems. For example, residue and redundant number systems can reduce or eliminate the ripple-through carries which are involved in normal binary addition or subtraction, resulting in high-speed arithmetic operations. These number systems have a natural implementation using many-valued circuits. However, the practicality of these potential advantages heavily depends on the availability of circuit realizations, which must be compatible or competitive with present-day standard technologies.



An IEEE International Symposium on Multiple-Valued Logic (ISMVL) has been held annually since 1970. It mostly caters to applications in digital design and verification. There is also a Journal of Multiple-Valued Logic and Soft Computing.


