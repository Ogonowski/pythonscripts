Web storage and DOM storage (Document Object Model storage) are web application software methods and protocols used for storing data in a web browser. Web storage supports persistent data storage, similar to cookies but with a greatly enhanced capacity and no information stored in the HTTP request header. There are two main web storage types: local storage and session storage, behaving similarly to persistent cookies and session cookies respectively.
Web storage is being standardized by the World Wide Web Consortium (W3C). It was originally part of the HTML5 specification, but is now in a separate specification. It is supported by Internet Explorer 8, Mozilla-based browsers (e.g., Firefox 2+, officially from 3.5), Safari 4, Google Chrome 4 (sessionStorage is from 5), and Opera 10.50. As of 14 March 2011 Opera and IE9 support the storage events.



Web storage can be viewed simplistically as an improvement on cookies. However, it differs from cookies in some key ways.



Web storage provides far greater storage capacity (5 MB per origin in Google Chrome, Mozilla Firefox, and Opera; 10 MB per storage area in Internet Explorer; 25MB per origin on BlackBerry 10 devices) compared to 4 kB (around 1000 times less space) available to cookies.



Unlike cookies, which can be accessed by both the server and client side, web storage falls exclusively under the purview of client-side scripting.
Web storage data is not automatically transmitted to the server in every HTTP request, and a web server can't directly write to Web storage. However, either of these effects can be achieved with explicit client-side scripts, allowing for fine-tuning of the desired interaction with the server.



Web storage offers two different storage areas local storage and session storage which differ in scope and lifetime. Data placed in local storage is per origin (the combination of protocol, hostname, and port number as defined in the same-origin policy) (the data is available to all scripts loaded from pages from the same origin that previously stored the data) and persists after the browser is closed. Session storage is per-origin-per-window and is limited to the lifetime of the window. Session storage is intended to allow separate instances of the same web application to run in different windows without interfering with each other, a use case that's not well supported by cookies.



Web storage currently provides a better programmatic interface than cookies because it exposes an associative array data model where the keys and values are both strings. An additional API for accessing structured data is being considered by the W3C Web Applications Working Group. 



Browsers that support web storage have the global variables 'sessionStorage' and 'localStorage' declared at the window level. The following JavaScript code can be used on these browsers to trigger web storage behaviour:









The following code can be used to retrieve all values stored in local storage for the currently browsed domain (the domain for the web page that is being browsed).
This JavaScript code can be executed using development tools available in most modern browsers such as the IE Developer Toolbar, Chrome Developer Tools, the Firebug extension in Firefox, or Opera Dragonfly:



Only strings can be stored via the Storage API. Attempting to store a different data type will result in an automatic conversion into a string in most browsers. Conversion into JSON (JavaScript Object Notation), however, allows for effective storage of JavaScript objects.



The W3C draft is titled "Web Storage", but "DOM storage" is also a commonly used name.
The "DOM" in DOM storage does not literally refer to the Document Object Model. According to the W3C, "The term DOM is used to refer to the API set made available to scripts in Web applications, and does not necessarily imply the existence of an actual Document object..."



Storage of web storage objects is enabled by default in Mozilla Firefox and SeaMonkey, but can be disabled by setting the "about:config" parameter "dom.storage.enabled" to false.
Mozilla Firefox stores all web storage objects in a single file named webappsstore.sqlite. The sqlite3 command can be used to show the elements stored therein.
There are browser extensions/add-ons for Google Chrome and Mozilla Firefox available that let the user deal with web storage, such as "Click&Clean" and "BetterPrivacy" which can be configured to remove the whole web storage automatically on a regular basis.


