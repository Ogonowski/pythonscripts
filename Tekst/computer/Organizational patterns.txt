Organizational patterns are structures of relationship, usually in a professional organization, that help the organization achieve its goals. The patterns are usually inspired by analyzing multiple professional organizations and finding common structures in their social networks and support corporate memory of reorganizations and process changes. They are often used as the foundation of project retrospectives.
Organizational patterns are inspired in large part by the principles of the software pattern community, that in turn takes it cues from Christopher Alexander's work on patterns of the built world.  Organizational patterns also have roots in Kroeber's classic anthropological texts on the patterns that underlie culture and society.  They in turn have provided inspiration for the Agile software development movement, and for the creation of parts of Scrum and of Extreme Programming in particular.



An early explicit citation to patterns of social structure can be found in the anthropological literature.

Patterns are those arrangements or systems of internal relationship which give to any culture its coherence or plan, and keep it from being a mere accumulation of random bits. They are therefore of primary importance.

Kroeber speaks of universal patterns that describe some overall scheme common to all human culture; of systemic patterns are broad but normative forms relating to beliefs, behaviors, signs, and economics; and total culture patterns that are local. Kroeber notes that systemic patterns can pass from culture to culture:

A second kind of pattern consists of a system or complex of cultural material that has proved its utility as a system and therefore tends to cohere and persist as a unit; it is modifiable only with difficulty as to its underlying plan. Any one such systemic pattern is limited primarily to one aspect of culture, such as subsistence, religion, or economics; but it is not limited areally, or to one particular culture; it can be diffused cross-culturally, from one people to another. . . . What distinguishes these systemic patterns of culture or well-patterned systems, as they might also be called is a specific interrelation of their component parts, a nexus that holds them together strongly, and tends to preserve the basic plan... As a result of the persistence of these systemic patterns, their significance becomes most evident on a historical view. 

The pattern aspect of Kroeber's view fits very well the systems-thinking pattern view of Christopher Alexander in the field of architecture. Alexander's books became an inspiration for the software world, and in particular for the object-oriented programming world, in about 1993. Organizational patterns in the sense they are recognized in the software community today first made an appearance at the original Hillside Group workshop that would lead to the pattern community and its PLoP conferences. 
The Hillside Group sent out a call for pattern papers and, in 1994, held the first pattern conference at Allerton Park in central Illinois in the United States. The second conference, also at Allerton, would follow a year later. These first two PLoP conferences witnessed a handful of organizational patterns:
The RaPPEL pattern language (1995) by Bruce Whitenack that described organizational structures suitable to requirements acquisition;
The Caterpillar's Fate pattern language (1995) by Norm Kerth that described organizational structures supporting evolution from analysis to design;
A work by James Coplien (1995) describing several years of organizational research at Bell Laboratories;
Episodes, a pattern language by Ward Cunningham (1996) describing key points of what today we would call Agile software development;
A pattern language by Neil Harrison (1996) on the formation and function of teams.
A flurry of associated publications and follow-up articles followed quickly thereafter, including an extemporization of the organizational patterns approach in the Bell Labs Technical Journal,  an invited piece in ASE,  a CACM article by Alistair Cockburn  and, shortly thereafter, a pattern-laden book by Alistair,  as well as chapters by Benualdi  and Janoff  in the Patterns Handbook. It was also about this time that Michael A. Beedle et al. published patterns that described explicit extensions to existing organizational patterns, for application in projects using a then five-year-old software development framework called Scrum.  A few more articles, such as the one by Brash et al.  also started to appear.
Little more happened on the organizational patterns front until the publication of the book by Berczuk et all on configuration management patterns;  this was a break-off effort from the effort originally centered at Bell Labs.
In the mean time, Jim Coplien and Neil Harrison had been collecting organizational patterns and combining them into a collection of four pattern languages. Most of these patterns were based on the original research from Bell Laboratories, which studied over 120 organizations over the period of a decade. These empirical studies were based on subject role-play in software development organizations, reminiscent of the sociodramas of Moreno's original social network approach.  However, the pattern language also had substantial input from other sources and in particular the works by Cockburn, Berczuk, and Cunningham. This collection was published as Organizational Patterns of Agile Software Development in 2004. 
One of the most recent organizational pattern articles comes from an early pattern contributor and advocate, the object design pioneer Grady Booch. 



Like other patterns, organizational patterns aren't created or invented: they are discovered (or "mined") from empirical observation. The early work on organizational patterns at Bell Laboratories focused on extracting patterns from social network analysis. That research used empirical role-playing techniques to gather information about the structure of relationships in the subject organization. These structures were analyzed for recurring patterns across organization and their contribution to achieving organizational goals. The recurring successful structures were written up in pattern form to describe their tradeoffs and detailed design decisions (forces), the context in which they apply, along with a generic description of the solution.
Patterns provide an incremental path to organizational improvement. The pattern style of building something (in this case, an organization) is:
Find the weakest part of your organization
Find a pattern that is likely to strengthen it
Apply the pattern
Measure the improvement or degradation
If the pattern improved things, go to step 1 and find the next improvement; otherwise, undo the pattern and try an alternative.
As with Alexander-style patterns of software architecture, organizational patterns can be organized into pattern languages: collections of patterns that build on each other. A pattern language can suggest the patterns to be applied for a known set of working patterns that are present.



The history of Agile software development and of organizational patterns have been entwined since the beginning. Kent Beck was the shepherd (interactive pattern reviewer) of the Coplien paper for the 1995 PLoP, and he mentions the influence of this work on extreme programming in a 2003 publication.  The idea of daily Scrum meetings in fact came from a draft of an article for Dr. Dobb's Journal  that described the organizational patterns research on the Borland QPW project.  Beedle's early work with Sutherland brought the pattern perspective more solidly into the history of Scrum. More recently, the Scrum community has taken up newfound interest in organizational patterns  and there is joint research going forward between the two communities. In this vein, the first ScrumPLoP conference took place in Sweden in May, 2010, sanctioned by both the Scrum Alliance and the Hillside Group.



^ Alexander, Christopher. A Pattern Language. Oxford University Press,  1979.
^ Kroeber, Alfred L. Anthropology: Culture, Patterns, and Process. New York: Harcourt, Brace and World, 1948.
^ Kroeber, Alfred L. Anthropology: Culture, Patterns, and Process. New York: Harcourt, Brace and World, 1948, p. 119
^ Kroeber, Alfred L. Anthropology: Culture, Patterns, and Process. New York: Harcourt, Brace and World, 1948, pp. 120 - 121.
^ Coplien, James. The Culture of Patterns. In Branislav Lazarevic, ed., Computer Science and Information Systems Journal 1, 2, Belgrade, Serbia and Montenegro, November 15, 2004, pp. 1-26.
^ Whitenack, Bruce. RAPPeL: a requirements-analysis-process pattern language for object-oriented development. In James Coplien and Doug Schmidt, eds., Pattern Languages of Program Design. Addison-Wesley, 1995, pp. 259 - 291.
^ Kerth, Norm. Caterpillar's Fate: a pattern language for the transformation from analysis to design. In James Coplien and Doug Schmidt, eds., Pattern Languages of Program Design. Addison-Wesley, 1995, pp. 293 - 320.
^ Coplien, James. Organizational Patterns. In James Coplien and Doug Schmidt, eds., Pattern Languages of Program Design. Addison-Wesley, 1995, pp. 183 - 237.
^ Cunningham, Ward. Episodes: a pattern language of competitive development. In Vlissides et al., eds., Pattern Languages of Program Design - 2. Addison-Wesley, 1996, pp. 371 - 388.
^ Harrison, Neil. Organizational Patterns for Teams. In Vlissides et al., eds., Pattern Languages of Program Design - 2. Addison-Wesley, 1996, pp. 345 - 352.
^ Harrison, Neil B. and James O. Coplien. Patterns of productive software organizations. Bell Labs Technical Journal, 1(1):138-145, Summer (September) 1996.
^ Cain, Brendan G., James O. Coplien, and Neil B. Harrison. Social Patterns in Productive Software Organizations. In John T. McGregor, editor, Annals of Software Engineering, 259-286. Baltzer Science Publishers, Amsterdam, December 1996.
^ Cockburn, Alistair. The interaction of social issues and software architecture. CACM 39(10), October 1996.
^ Cockburn, Alistair. Surviving Object-Oriented Projects. Addison-Wesley, 1997.
^ Genualdi, Patricia. Improving software development with process and organizational patterns. In Linda Rising, ed. The Patterns Handbook. Cambridge University Press, 1998, pp. 121 - 129.
^ Janoff, Norm. Organizational patterns at AG communication systems. In Linda Rising, ed. The Patterns Handbook. Cambridge University Press, 1998, pp. 131 - 138.
^ Michael A. Beedle, Martine Devos, Yonat Sharon, Ken Schwaber, and Jeff Sutherland. SCRUM: An extension pattern language for hyperproductive software development. Washington University Technical Report TR #WUCS-98-25, 1998.
^ Brash, Danny, et al. Evaluating organizational patterns for supporting business knowledge management. Proceedings of the 2000 information resources management association international conference on Challenges of information technology management in the 21st century. IGI Publishing, May 2000.
^ Berczuk, Steve, Brad Appleton and Kyle Brown. Software Configuration Management Patterns: Effective Teamwork, Practical Integration. Addison-Wesley, 2003.
^ Moreno, J. L. Who shall survive?: foundations of sociometry, group psychotherapy and sociodrama. Washington, D.C.: Nervous and Mental Disease Publishing Co., 1934.
^ Coplien, James and Neil Harrison. Patterns of Agile Software Development. Addison-Wesley,  2004.
^ Booch, Grady. Architectural Organizational Patterns. IEEE Software 25(3), May 2008, pp. 18 - 19.
^ Fraser, Steven, Kent Beck, Bill Caputo, Tim Mackinnon, James Newkirk and Charlie Pool. "Test Driven Development (TDD)." In M. Marchesi and G. Succi, eds., XP 2003, LNCS 2675, pp. 459   462, 2003.   Springer-Verlag, Berlin and Heidelberg, 2003.
^ Coplien, James O., and Jon Erickson. Examining the Software Development Process. Dr. Dobb's Journal of Software Tools, 19(11):88-95, October 1994.
^ Sutherland, Jeff. Origins of Scrum. Web page [1], accessed 22 September 2008. July 5, 2007.
^ Sutherland, Jeff. Scrum and Organizational Patterns. Web page [2], accessed 14 June, 2013. May 20, 2013.