Ratfor (short for Rational Fortran) is a programming language implemented as a preprocessor for Fortran 66. It provided modern control structures, unavailable in Fortran 66, to replace GOTOs and statement numbers.



Ratfor provides the following kinds of flow-control statements, described by Kernighan and Plauger as "shamelessly stolen from the language C, developed for the UNIX operating system by D.M. Ritchie" ("Software Tools", p. 318):
statement grouping with braces
if-else, while, for, do, repeat-until, break, next
"free-form" statements, i.e., not constrained by Fortran format rules
<, >, >=, ... in place of .LT., .GT., .GE., ...
include
# comments
For example, the following code

might be translated as

The version of Ratfor in Software Tools is itself written in Ratfor, as are the sample programs, and inasmuch as its own translation to Fortran is available, it can be ported to any Fortran system. Ratfor source code file names end in .r or .rat.



Ratfor was designed and implemented by Brian Kernighan at Bell Telephone Laboratories in 1974, and described in Software Practice & Experience in 1975. It was used in the book "Software Tools" (Kernighan and Plauger, 1976).
In 1977, at Purdue University, an improved version of the ratfor preprocessor was written. It was called Mouse4, as it was smaller and faster than ratfor. A published document by Dr. Douglas Comer, professor at Purdue, concluded "contrary to the evidence exhibited by the designer of Ratfor, sequential search is often inadequate for production software. Furthermore, in the case of lexical analysis, well-known techniques do seem to offer efficiency while retaining the simplicity, ease of coding and modularity of ad hoc methods." (CSD-TR236).
In comparison to the ratfor preprocessor on a program of 3000 source lines running on a CDC 6500 system took 185.470 CPU seconds. That was cut by 50% when binary search was used in the ratfor code. Rewriting the ad hoc lexical scanner using a standard method based on finite automata reduced run time to 12.723 seconds.
With the availability of Fortran 77, a successor named ratfiv (ratfor=rat4 => rat5=ratfiv) could, with an option /f77, output a more readable Fortran 77 code:

Initial Ratfor source code was ported to C in 1985  and improved to produce Fortran 77 code too. A git tree has been set in 2010 in order to revive ratfor . Meanwhile, the GNU C compiler which had the ability to directly compile a Ratfor file (.r) without keeping a useless intermediate Fortran code (.f) (gcc foo.r) lost this functionality in version 4 during the move in 2005 from f77 to GNU Fortran.
Source packages, .deb or src.rpm package  are still available for users who needs to compile old Ratfor software on any operating system.



Ratfiv
Fortran






Ratfor
Ratfor90
History of Programming Languages: Ratfor
Purdue summary