Communications in Belgium are extensive and advanced. Belgium possesses the infrastructure for both mobile and land-based telecom, as well as having significant television, radio and internet infrastructure. The country code for Belgium is BE.







Mail regulation is a national competency. Postal service in Belgium is in many cases performed by Belgian Post Group, a semi-private public company which has a monopoly on letters until 50g weight. Competitors include DHL and UPS.
Postal codes in Belgium consist of four digits which indicate regional areas, e.g. "9000" is the postal code for Ghent.




The telephone system itself is highly developed and technologically advanced, with full automation in facilities that handle domestic and international telecom. Domestically speaking, the county has a nationwide cellular telephone system and an extensive network of telephone cables. Telephone regulation is a national competency.
The country code for Belgium is 32 and the international call prefix is 00.
A telephone number in Belgium is a sequence of nine or ten numbers dialled on a telephone to make a call on the telephone network in Belgium. Belgium is under a closed telephone numbering plan, but retains the trunk code, "0", for all national dialling.




There were 4.668 million land telephone lines in use in Belgium in 2007, a slight decrease on the 4.769 million in use in 1997.
The majority state-owned public telephone company of Belgium is Belgacom. Some other or private operators exist, as Scarlet (Belgacom) and BASE (KPN).




Mobile telephone ownership has increased by nearly one thousand percent in the period 1997-2007, from 974,494 to 10.23 million.
There are three licensed mobile network operators (MNO) in Belgium, Proximus (Belgacom), Mobistar (Orange S.A.) and BASE (KPN) and numerous mobile virtual network operators (MVNO).
A fourth license will be auctioned off by the government in January 2010.




There were 61 (2003) internet service providers in Belgium, serving 8.113 million internet users in 2009. The country code for Belgian websites is .be.
In September 2009 in Flanders there were 3,048,260 broadband internet customers (DSL and cable), of which 2,520,481 were residential users and 527,779 business users. Only 65,175 dial-up internet access accounts remained in the residential market and 9,580 in the business market.






Belgium has numerous copper cable internet providers:
Altercom
BASE (KPN)
Belgacom
Destiny
Digiweb
EDPnet
Evonet
Full Telecom
Interxion
LCL
Mobistar (Orange S.A.)
Numericable (France Numericable)
Perceval
Portima
Proximedia Group
Scarlet (Belgacom)
Verizon Business (Verizon Communications)
Only Belgacom and Numericable currently offers fixed telephony and digital television in a triple play formula. All other companies offer also fixed telephony in a duo play formula.



Belgium has three major fiberglas cable internet providers:
Num ricable for the Brussels region (Ypso Holding)
Telenet for the Flanders and Brussels regions (Liberty Global)
VOO for the Walloon and Brussels regions (TECTEO)
These companies all offer fixed telephony and digital television in a triple play formula.
Interoute Managed Services
Interxion
LCL
Nucleus
Verizon Business (Verizon Communications)
These companies all offer specialised services.



Clearwire in Brussels, Ghent, Leuven, Aalst, Halle and Vilvoorde (Sprint Nextel)
Perceval



Verizon Business (Verizon Communications)



The Brussels Regional Informatics Center (BRIC, Centre d'Informatique pour la R gion Bruxelloise in French)offers Internet access to public administrations in the Brussels-Capital Region, relying directly on the national Belnet network and the IRISnet network.



Other ISP are Chat.be, Combell, Connexeon, HostIT, Microsoft Belgium, Netlog, Ulysse, Ven Brussels, Rack66 (EUSIP bvba), WSD Hosting.



The most popular internet websites accessed from a Belgian ISP in 2008 are:
google.be
facebook.com
live.com
youtube.com
google.com
skyrock.com
msn.com
yahoo.com
netlog.com
google.fr
ebay.be
wikipedia.org
dailymotion.com
blogger.com
partypoker.com



The microwave relay network is, however, more limited. For international communications, Belgium has 5 submarine cables and a number of satellite earth stations, two of which are Intelsat, and one Eutelsat.



^ "Belgium". CIA World Factbook. Retrieved 19 November 2012. 
^ http://www.irisnet.eu/en?set_language=en regional



BIPT - Belgian Institute for Postal Services and Telecommunications
ISPA - Internet Service Providers Association of Belgium
DNS - Domain Name System Belgium
MAVISE - Belgian TV market
Agoria - Federation of Belgian IT Employers
Beltug - Federation of Belgian ICT Professionals
UPP - Union of Belgian Periodical Press Publishers
Febelma - Belgian Federation of Magazines
VRM - Flemish Media Regulator (Dutch community)
CSA - High Council for the Audiovisual Media (French community)
MDGB - Germanic Media Council of Belgium (Germanic community)