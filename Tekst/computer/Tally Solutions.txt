Tally Solutions Pvt Ltd is a Bangalore-based software company that currently sells into more than 100 countries beyond its native India, including the United Kingdom, Bangladesh and the Middle East.
Tally's software is mainly used for vouchers, financial statements, and taxation in many industries, and has specialised packages for retail businesses. More advanced capabilities are found in its enterprise resource planning package.

Tally.ERP 9 has advanced integration capabilities in the form of Application programming interfaces to make the tensible. Tally interacts with Software application using XML, ODBC and DLL technologies.



Tally Solutions offers four products:
Tally.ERP 9
Tally.Developer 9
Shoper 9
Tally.Server 9



Sales of Tally's Products, viz. Tally.ERP 9 and Shoper 9, are done through a network of Tally Partners. Tally follows a 2-tier Channel Structure with the top tier called as Master Tally Partners (MTP) and the second tier being called as Tally Partners or Secondary Tally Partners (STP). Tally has around 140 MTP's and around 18,000 Tally Partners. Tally channel partners are structured by service or product as follows:
Tally Partners (TP) handle sales and installation and are often the customer's first point of contact.
Tally Extenders (TE) provide productised modules or solutions to specific requirements.
Tally Integrators (TI) are commissioned for large scale deployment, system integration, product enhancements and large solutions.
Tally Service Partners (TSP) are a certified service system that delivers support services.
Small and medium enterprises (SME) of India are major users of Tally, which has a registered subscriber base of 550,000 users.



S. S. Goenka was the founding chairperson of Tally Solutions Private Limited.
Bharat Goenka is the Co-Founder & Managing Director of Tally Solutions Private Limited. He has been awarded the Lifetime Achievement Award by NASSCOM, and a Lifetime Achievement Award by CellIT, an IT channel magazine.
Thirty software companies of India, who are in the business of product development   including Tally Solutions   have formed an association named Indian Software Product Industry Round Table, or iSpirt, breaking away from NASSCOM. Later, NASSCOM refused to view this as a 'breakaway group', and assured supporting activities of iSpirit.



Tally won the FY2013-14 Channel Champion crown on the back of high scores on price performance, channel profitability, channel marketing, pre-sales support, and channel policy 
Tally awarded the Best ERP Software For SMB - 2014 
In 2004, the company was awarded the Golden Rhino award by 360 Magazine.


