The Pentium M is a family of mobile 32-bit single-core x86 microprocessors (with the modified Intel P6 microarchitecture) introduced in March 2003 and forming a part of the Intel Carmel notebook platform under the then new Centrino brand. The Pentium M processors had a maximum thermal design power (TDP) of 5 27 W depending on the model, and were intended for use in laptops (thus the "M" suffix standing for mobile). They evolved from the core of the last Pentium III branded CPU by adding the front-side bus (FSB) interface of Pentium 4, an improved instruction decoding and issuing front end, improved branch prediction, SSE2 support, and a much larger cache. The first Pentium M branded CPU, code-named Banias, was followed by Dothan. The Pentium M-branded processors were succeeded by the Core-branded dual-core mobile Yonah CPU with a modified microarchitecture.



The Pentium M represented a new and radical departure for Intel, as it was not a low-power version of the desktop-oriented Pentium 4, but instead a heavily modified version of the Pentium III Tualatin design (itself based on the Pentium II core design, which in turn had been a heavily improved evolution of the Pentium Pro). It is optimized for power efficiency, a vital characteristic for extending notebook computer battery life. Running with very low average power consumption and much lower heat output than desktop processors, the Pentium M runs at a lower clock speed than the laptop version of the Pentium 4 (The Pentium 4-Mobile, or P4-M), but with similar performance - a 1.6 GHz Pentium M can typically attain or even surpass the performance of a 2.4 GHz Pentium 4-M. The Pentium M 740 has been tested to perform up to approximately 7,400 MIPS and 3.9 GFLOPS (using SSE2).
The Pentium M coupled the execution core of the Pentium III with a Pentium 4 compatible bus interface, an improved instruction decoding/issuing front end, improved branch prediction, SSE2 support, and a much larger cache. The usually power-hungry secondary cache uses an access method which only switches on the portion being accessed. The main intention behind the large cache was to keep a decent-sized portion of it still available to the processor even when most of the L2 cache was switched off, but its size led to a welcome improvement in performance.
Other power saving methods include dynamically variable clock frequency and core voltage, allowing the Pentium M to throttle clock speed when the system is idle in order to conserve energy, using the SpeedStep 3 technology (which has more sleep stages than previous versions of SpeedStep). With this technology, a 1.6 GHz Pentium M can effectively throttle to clock speeds of 600 MHz, 800 MHz, 1000 MHz, 1200 MHz, 1400 MHz and 1600 MHz; these intermediate clock states allow the CPU to better throttle clock speed to suit conditions. The power requirements of the Pentium M varies from 5 watts when idle to 27 watts at full load. This is useful to notebook manufacturers as it allows them to include the Pentium M into smaller notebooks.
Although Intel has marketed the Pentium M exclusively as a mobile product, motherboard manufacturers such as AOpen, DFI and MSI have been shipping Pentium M compatible boards designed for enthusiast, HTPC, workstation and server applications. An adapter, the CT-479, has also been developed by ASUS to allow the use of Pentium M processors in selected ASUS motherboards designed for Socket 478 Pentium 4 processors. Shuttle Inc. offers packaged Pentium M desktops, marketed for low energy consumption and minimal cooling system noise. Pentium M processors are also of interest to embedded systems' manufacturers because the low power consumption of the Pentium M allows the design of fanless and miniaturized embedded PCs.




As the M line was originally designed in Israel, the first Pentium M was identified by the codename Banias, named after an ancient site in the Golan Heights. The Intel Haifa team had previously been working on the memory controller for Timna, which was based on earlier P6 memory controller designs giving them detailed knowledge of P6 architecture which they used when Intel gave them a crash project to create a backup mobile CPU. Given the product code 80535, it initially had no model number suffix, but was later identified as the Pentium M 705. It was manufactured on a 130 nm process, was released at frequencies from 900 MHz to 1.7 GHz using a 400 MT/s FSB, and had 1 megabyte (MB) of Level 2 cache. The core average TDP (Thermal Design Power) is 24.5 watts.
The Banias family processors internally support PAE but do not show the PAE support flag in their CPUID information; this causes some operating systems (primarily Linux distributions) to refuse to boot on such processors since PAE support is required in their kernels.
The CPUID signature for a Banias is 0x69X.




On September 17, 2003, Intel unveiled plans for releasing its then next-generation of Pentium M processors, codenamed "Dothan" by them. It was named after another ancient town in Israel, and it launched formally on May 10, 2004. Dothan Pentium M processors (product code 80536, CPUID 0x6DX) are among the first Intel processors to be identified using a "processor number" rather than a clockspeed rating, and the mainstream versions are known as Pentium M 710 (1.4 GHz), 715 (1.5 GHz), 725 (1.6 GHz), 735 (1.7 GHz), 740 (1.73 GHz), 745 (1.8 GHz), 750 (1.86 GHz), 755 (2.0 GHz), and 765 (2.1 GHz).
These 700 series Pentium M processors retain the same basic design as the original Pentium M, but are manufactured on a 90 nm process, with twice the secondary cache. Die size, at 87 mm2, remains in the same neighborhood as the original Pentium M, even though the 700 series contains approximately 140 million transistors, most of which make up the 2 MB cache. TDP is also down to 21 watts (from 24.5 watts in Banias), though power use at lower clockspeeds has increased slightly. However, tests conducted by third party hardware review sites show that Banias and Dothan equipped notebooks have roughly equivalent battery life. Additionally third party hardware review sites have benchmarked the Dothan at approx 10-20% better performance than the Banias in most situations.
Revisions of the Dothan core were released in the first quarter of 2005 with the Sonoma chipsets and supported a 533 MT/s FSB and XD (Intel's name for the NX bit) (and PAE support required for it was enabled, unlike earlier Pentium Ms that had it disabled). These processors include the 730 (1.6 GHz), 740 (1.73 GHz), 750 (1.86 GHz), 760 (2.0 GHz) and 770 (2.13 GHz). These models all have a TDP of 27 W and a 2 MB L2 cache.
In July 2005, Intel released the 780 (2.26 GHz) and the low-voltage 778 (1.60 GHz).
The processor line has models running at clock speeds from 1.0 GHz to 2.26 GHz as of July 2005. The models with lower frequencies were either low voltage or ultra-low voltage CPUs designed for improved battery life and reduced heat output. The 718 (1.3 GHz), 738 (1.4 GHz), and 758 (1.5 GHz) models are low-voltage (1.116 V) with a TDP of 10 W, while the 723 (1.0 GHz), 733 (1.1 GHz), and 753 (1.2 GHz) models are ultra-low voltage (0.940 V) with a TDP of 5 W.




An ultra low-power microprocessor based on the Dothan built on a 90 nm process with 512 KB L2 cache and 400 MT/s front side bus (FSB).




The next generation of processors, codenamed Yonah, were based on the Enhanced Pentium M architecture, and released under the Intel Core brand, as Core Duo and Core Solo.




P6 (microarchitecture)
Centrino
List of Intel Pentium M microprocessors






Intel's Pentium M Homepage (via internet archive)
Intel's list of all Pentium M variants
Intel Processor comparison table
Homepage of the software "Notebook Hardware Control"
Homepage of the Software "Linux-PHC" to undervolt a Pentium M with linux
Yonah details from X86-secret (french translated to English through google)
AnandTech: Yonah Performance Preview
SpeedswitchXP - CPU frequency control for notebooks