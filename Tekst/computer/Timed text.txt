Timed text refers to the presentation of text media in synchrony with other media, such as audio and video.



Typical applications of timed text are the real time subtitling of foreign-language movies on the Web, captioning for people lacking audio devices or having hearing impairments, karaoke, scrolling news items or teleprompter applications.
Timed text for MPEG-4 movies and cellphone media is specified in MPEG-4 Part 17 Timed Text, and its MIME type is specified by RFC 3839.



The W3C keeps two standards intended to regulate timed text on the internet: the Timed Text Markup Language (TTML) and WebVTT (currently in draft stage). SMPTE created additional metadata structures for use in TTML and developed a profile of TTML called SMPTE-TT. The DECE incorporated the SMPTE Timed Text in their UltraViolet Common File Format specification.



Interoperability for timed text came up during the development of the SMIL 2.0 specification. Today, incompatible formats for captioning, subtitling and other forms of timed text are used on the Web. This means that when creating a SMIL presentation, the text portion often needs to be targeted to a particular playback environment. Moreover, the accessibility community relies heavily on captioning to make audiovisual content accessible. The lack of an interoperable format adds a significant additional cost to the costs of captioning Web content, which are already high.



The following is an extract from the English closed captioning file, in SubRip format, for the 1916 Krazy Kat Bugolist film.

1
00:00:22,000 --> 00:00:27,000
I'll teach thee Bugology, Ignatzes

2 
00:00:40,000 --> 00:00:43,000 
Something tells me

3 
00:00:58,000 --> 00:01:59,000 
Look, Ignatz, a sleeping bee

The equivalent in W3C WebVTT is the following:

WEBVTT

00:22.000 --> 00:27.000
I'll teach thee Bugology, Ignatzes

00:40.000 --> 00:43.000 
Something tells me

00:58.000 --> 01:59.000 
Look, Ignatz, a sleeping bee

The equivalent in W3C TTML is the following:

 <tt xmlns="http://www.w3.org/ns/ttml">
  <body>
   <div begin="22s" dur="5s">
     <p>I'll teach thee Bugology, Ignatzes</p>
   </div>
   <div begin="40s" dur="3s">
     <p>Something tells me</p>
   </div>
   <div begin="58s" dur="61s">
     <p>Look, Ignatz, a sleeping bee</p>
   </div>
  </body>
 </tt>



Subtitle (captioning)
Text over IP



^ Glenn Adams (Ed.): Timed Text Markup Language (TTML) 1.0 - W3C Recommendation, 18 November 2010
^ WebVTT Draft Report
^ SMPTE (August 2010), ST-2052-1; SMPTE Timed Text, Copyright   2010 SMPTE. August 2010 (PDF), retrieved 2011-03-25 



W3C's Video in the Web Activity Statement
The W3C timed text homepage
Also see DAISY Digital Talking Book standard
Netflix DFXP to SRT Converter