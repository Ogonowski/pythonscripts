The Ionian Islands (Modern Greek:    , Ionia nisia; Ancient Greek, Katharevousa:    , Ionioi N soi; Italian: Isole Ionie) are a group of islands in Greece. They are traditionally called the Heptanese, i.e. "the Seven Islands" (Greek:  , Heptan sa or  , Heptan sos; Italian: Eptaneso), but the group includes many smaller islands as well as the seven principal ones.




The seven islands are; from north to south:
Kerkyra ( ) usually known as Corfu in English and Corf  in Italian
Paxi ( ) also known as Paxos in English
Lefkada ( ) also known as Lefkas in English
Ithaki ( ) usually known as Ithaca in English
Kefalonia ( ) often known as Kefalonia, Cephalonia and Kefallinia in English
Zakynthos ( ) sometimes known as Zante in English and Italian
Kythira ( ) usually known as Cythera in English and sometimes known as Cerigo in English and Italian
The six northern islands are off the west coast of Greece, in the Ionian Sea. The seventh island, Kythira, is off the southern tip of the Peloponnese, the southern part of the Greek mainland. Kythira is not part of the region of the Ionian Islands, as it is included in the region of Attica.



In Ancient Greek the adjective Ionios ( ) was used as an epithet for the sea between Epirus and Italy in which the Ionian Islands are found because Io swam across it. Latin transliteration, as well as Modern Greek pronunciation, may suggest that the Ionian Sea and Islands are somehow related to Ionia, an Anatolian region; in fact the Ionian Sea and Ionian Islands are spelled in Greek with an omicron ( ), whereas Ionia has an omega ( ). In Modern Greek this is purely a spelling distinction, but the different pronunciations in Ancient Greek would have eliminated the risk of confusion between the two areas. Furthermore, Ionian is accented on the antepenult (IPA: [i onia]), and Ionia on the penult (IPA: [io nia]); also the proper adjective for Ionia is Ionic, not Ionian.
The islands themselves are known by a rather confusing variety of names. During the centuries of rule by Venice, they acquired Venetian names, by which some of them are still known in English (and in Italian). Kerkyra was known as Corf , Ithaki as Val di Compare, Kythera as Cerigo, Lefkada as Santa Maura and Zakynthos as Zante.
A variety of spellings are used for the Greek names of the islands, particularly in historical writing. Kefallonia is often spelled as Cephallenia or Cephalonia, Ithaki as Ithaca, Kerkyra as Corcyra, Kythera as Cythera, Lefkada as Leucas or Leucada and Zakynthos as Zacynthus or Zante. Older or variant Greek forms are sometimes also used: Kefallinia for Kefallonia and Paxos or Paxoi for Paxi.




The islands were settled by Greeks at an early date, possibly as early as 1200 BC, and certainly by the 9th century BC. The early Eretrian settlement at Kerkyra was displaced by colonists from Corinth in 734 BC. The islands were mostly a backwater during Ancient Greek times and played little part in Greek politics. The one exception was the conflict between Kerkyra and its mother-City Corinth in 434 BC, which brought intervention from Athens and triggered the Peloponnesian War.
Ithaca was the name of the island home of Odysseus in the epic Ancient Greek poem the Odyssey by Homer. Attempts have been made to identify Ithaki with ancient Ithaca, but the geography of the real island cannot be made to fit Homer's description. Archeological investigations have revealed interesting findings in both Kefalonia and Ithaca.




By the 4th century BC, most of the islands, were absorbed into the empire of Macedon. Some remained under the control of the Macedonian Kingdom and until 146 BC, when the Greek peninsula was gradually annexed by Rome. After 400 years of peaceful Roman rule, the islands passed to the Eastern Roman or Byzantine Empire. Under Byzantine rule, from the mid-8th century, they formed the theme of Cephallenia. The islands were a frequent target of Saracen raids and from the late 11th century, saw a number of Norman and Italian attacks. Most of the islands fell to William II of Sicily in 1185. Although Corfu and Lefkas remained under Byzantine control, Kefallonia and Zakynthos became the County palatine of Cephalonia and Zakynthos until 1357, when this entity was merged with Lefkada and Ithaki to become the Duchy of Leucadia under French and Italian dukes. Corfu, Paxi and Kythera were taken by the Venetians in 1204, after the dissolution of the Byzantine Empire by the Fourth Crusade. These became important overseas colonies of the Republic and were used as way-stations for their maritime trade with the Levant.




From 1204, the Republic of Venice controlled Corfu and slowly all the Ionian islands fell under Venetian rule. In the 15th century, the Ottomans conquered most of Greece, but their attempts to conquer the islands were largely unsuccessful. Zakynthos passed permanently to Venice in 1482, Kefallonia and Ithaki in 1483, Lefkada in 1502. Kythera had been in Venetian hands since 1238.
The islands thus became the only part of the Greek-speaking world to escape Ottoman rule, which gave them both a unity and an importance in Greek history they would otherwise not have had. Corfu was the only Greek island never conquered by the Turks.
Under Venetian rule, many of the upper classes spoke Italian (or Venetian in some cases) and converted to Roman Catholicism, but the majority remained Greek ethnically, linguistically, and religiously.
In the 18th century, a Greek national independence movement began to emerge, and the free status of the Ionian islands made them the natural base for exiled Greek intellectuals, freedom fighters and foreign sympathisers. The islands became more self-consciously Greek as the 19th century, the century of romantic nationalism, neared.




In 1797, however, Napol on Bonaparte conquered Venice, and by the Treaty of Campo Formio the islanders found themselves under French rule, the islands being organised as the d partments Mer- g e, Ithaque and Corcyre. In 1798, the Russian Admiral Ushakov evicted the French, and established the Septinsular Republic under joint Russo-Ottoman protection the first time Greeks had had even limited self government since the fall of Constantinople in 1453. The plenipotentiary of the Czar was Count George Mocenigo, a noble from Zante who had earlier served as Russian diplomat in Italy. However, in 1807, they were ceded again to the French in the Treaty of Tilsit and occupied by the French Empire.




In 1809, the British defeated the French fleet in Zakynthos (October 2, 1809) captured Kefallonia, Kythera and Zakynthos, and took Lefkada in 1810. The French held out in Corfu until 1814. The Treaty of Paris in 1815 turned the islands into the "United States of the Ionian Islands" under British protection (November 5, 1815). In January 1817, the British granted the islands a new constitution. The islanders elected an Assembly of 40 members, who advised the British High Commissioner. The British greatly improved the islands' communications, and introduced modern education and justice systems. The islanders welcomed most of these reforms, and took up afternoon tea, cricket and other English pastimes.
Once Greek independence was established after 1830, however, the islanders began to resent foreign colonial rule by the English, and to press for Enosis, i. e. union with Greece. The British statesman William Ewart Gladstone toured the islands and recommended that having already Malta, giving the islands to Greece wouldn't hurt the interest of the British Empire. The British government resisted, since like the Venetians they found the islands made useful naval bases. They also regarded the Bavarian-born king of Greece, King Otto, as unfriendly to Britain. However, in 1862, Otto was deposed and a pro-British king, George I, was installed.



In 1862, Britain decided to transfer the islands to Greece, as a gesture of support intended to bolster the new king's popularity. On May 2, 1864, the British departed and the islands became three provinces of the Kingdom of Greece though Britain retained the use of the port of Corfu. On 21 May 1864 the Ionian Islands officially reunited with Greece. Prince Philippos of Greece and Denmark was born in Corfu in 1921 and grew up to become Britain's Prince Philip, Duke of Edinburgh.




In 1941, when Axis forces occupied Greece, the Ionian Islands (except Kythera) were handed over to the Italians, who in their three years of rule attempted to Italianize the population of Corfu (as has happened with the Corfiot Italians). In 1943, the Germans replaced the Italians, and deported the centuries-old Jewish community of Corfu to their deaths. By 1944, most of the islands were under the control of the EAM/ELAS resistance movement, and they have remained a stronghold of left-wing sentiment ever since.



The islands were struck by an especially powerful earthquake, of 7.1 magnitude, on August 12, 1953. Building damage was extensive and the southern islands of Kefalonia and Zakynthos were practically levelled. The islands were reconstructed from the ground up over the following years, under a strict building code. The code has proven extremely effective, as many earthquakes since that time have caused no damage to new buildings.




Today, all the islands are part of the Greek region of the Ionian Islands (Ionioi Nisoi), except Kythera, which is part of the region of Attica. Kerkyra has a population of 103,300 (including Paxoi), Zakynthos 40,650, Kefallonia 39,579 (including Ithaca), Lefkada 22,536, Ithaki 3,052, Kythera 3,000 and Paxi 2,438.
In recent decades, the islands have lost much of their population through emigration and the decline of their traditional industries, fishing and marginal agriculture. Today, their major industry is tourism. Specifically Kerkyra, with its harbour, scenery and wealth of ruins and castles, is a favourite stopping place for cruise liners. British tourists in particular are attracted through having read Gerald Durrell's evocative book My Family and Other Animals (1956), which describes his childhood on Kerkyra in the 1930s. The novel and movie Captain Corelli's Mandolin are set in Kefallonia, in which Captain Corelli is part of the Italian occupation force during the Second World War.



The Ionian Islands official population, excluding Cythera, in 2011 was 207,855, decreased by 1,50% compared to the population in 2001. Nevertheless, the region remains the third by population density with 90.1/km  nationwide, well above the national of 81.96/km . The most populous of the major islands is Corfu with a population of 104,371, followed by Zante (40,759), Cephalonia (35,801), Leucas (23.693) and Ithaca (3.231). The foreign-born population was in 2001 19,360 or 9.3%, the majority of which was concentrated in Corfu and Zante. Most of them originate from Albania (13,536). The fertility rate for 2011 according to Eurostat was 1.35 live births per woman during her lifetime.







The regional Gross Domestic Product for 2010 was 4,029 million euros. The GDP per capita for the same year was 18,440 euros per capita which was lower than the national median of 20,481. However, the GDP per capita of Cephalonia and Zante, 23,275 and 24,616 respectively, was much higher than the national figure. Additionally, unemployment for 2012 was 14.7, the lowest among all Greek regions, and much lower compared to the national unemployment of 24.2.




The region is a popular tourist destination. The airports of Corfu, Zante and Cephalonia were in the top ten in Greece by number of international arrivals, with 1,386,289 international arrivals for 2012, with Corfu being the sixth airport by number of arrivals nationwide, with Zante and Cephalonia also being in the top ten. While Cephalonia Airport had the biggest increase nationwide by 13.11% compared to 2011, while Corfu had an increase of 6.31%.'



Argost li ( ), in Kefalonia
K rkyra ( ), in Corfu
Lefk da ( ), in Lefkada
Lixouri ( ), in Kefalonia
Z kynthos ( ), in Zakynthos


