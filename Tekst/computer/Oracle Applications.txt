Oracle Applications comprise the applications software or business software of the Oracle Corporation. The term refers to the non-database and non-middleware parts of Oracle's software portfolio.
Oracle sells many functional modules which use the Oracle RDBMS as a back-end, notably Oracle Financials, Oracle HRMS, Oracle SCM, Oracle Projects, Oracle CRM, Oracle Procurement, etc.
Oracle initially launched its application suite with financials software in the late 1980s. The offering as of 2009 extends to supply-chain management, human-resource management, warehouse-management, customer-relationship management, call-center services, product-lifecycle management, and many other areas. Both in-house expansion and the acquisition of other companies have vastly expanded Oracle's application software business.
Oracle released Oracle E-Business Suite (EBS/ e-BS) Release 12 (R12)   a bundling of several Oracle Applications applications   in February 2007. The release date coincided with new releases of other Oracle-owned products: JD Edwards EnterpriseOne, Siebel Systems and PeopleSoft.



Oracle Corporation's application portfolio consisted as of 2012 of the following software suites and products:
Oracle Fusion Applications
Oracle E-Business Suite
PeopleSoft Enterprise
Siebel
ATG
Endeca
JD Edwards EnterpriseOne
JD Edwards World
Hyperion
Primavera
Agile
AutoVue (for processing CAD and graphics data)
Master Data Management



Oracle's E-Business Suite (also known as Applications/Apps or EB-Suite/EBS) consists of a collection of enterprise resource planning (ERP), customer relationship management (CRM), and supply-chain management (SCM) computer applications either developed or acquired by Oracle. The software utilizes Oracle's core Oracle relational database management system technology. The E-Business Suite contains several product lines often known by short acronyms.
Significant technologies incorporated into the applications include the Oracle database technologies, (engines for RDBMS, PL/SQL, Java, .NET, HTML and XML), the "technology stack" (Oracle Forms Server, Oracle Reports Server, Apache Web Server, Oracle Discoverer, Jinitiator and Sun's Java). Oracle Corporation brands the on-line technical documentation of E-Business Suite as eTRM   "E-Business Suite Technical Reference Manuals".
It makes the following enterprise applications available as part of Oracle eBusiness Suite:
Asset Lifecycle Management
Asset Tracking
Property Management

Customer Relationship Management
Enterprise resource planning
Financial Management
Human Capital Management
Project Portfolio Management

Procurement
Oracle Advanced ProcurementOracle Sourcing

Product Life-cycle Management
Supply Chain ManagementSupply Chain Planning
Logistics & Transportation Management
Order Management
Price Management

Manufacturing
Discrete Manufacturing
Process Manufacturing



The Oracle E-Business Suite provides a set of financial applications used internationally in businesses. Oracle Corporation groups these applications into "suites", which it defines as sets of common, integrated applications designed to execute specific business processes.
Oracle Financials refers to the closely related financial modules such as:
Oracle Assets
Oracle General Ledger
Oracle Payables
Oracle Receivables
Oracle Cash Management
Oracle Tabs
The key business processes enabled by the Financial Applications include:   Procure-to-Pay business process flow involving activities such as procurement, purchasing, making payment to Suppliers and subsequent accounting.   Order-to-Cash business process flow involves activities such as Customer Orders, Order fulfillment, receiving payment from Customers & subsequent accounting. 



Oracle Project Portfolio Analysis
Oracle Project Billing
Oracle Project Analysis
Oracle Daily Business Intelligence
Oracle Project Collaboration
Oracle Project Contracts
Oracle Project Costing
Oracle Project CRM.
Oracle Project Management
Oracle Project Resource



Additional Oracle E-Business Suite products include:



In 2007 Oracle launched a set of applications for mid-size businesses called Oracle Accelerate. Accelerate provides access to Oracle's ERP products through a local partner-network and packages the products to meet vertical industry requirements.



The Oracle User Productivity Kit application provides a content-development, deployment, and maintenance platform.


