Dirt is unclean matter, especially when in contact with a person's clothes, skin or possessions when they are said to become dirty. Common types of dirt include:
dust   a general powder of organic or mineral matter
filth   foul matter such as excrement
grime   a black, ingrained dust such as soot
soil   the mix of clay, sand and humus which lies over the bedrock



A season of artworks and exhibits on the theme of dirt was sponsored by the Wellcome Trust in 2011. The centrepiece was an exhibition at the Wellcome Collection showing pictures and histories of notable dirt such as the great dust heaps at Euston and King's Cross in the 19th century and the Fresh Kills landfill which was once the world's largest.



Computer keyboards are especially dirty as, on average, they contain 70 times more microbes than a lavatory seat.



When things are dirty they are usually cleaned with solutions like hard surface cleaner and other chemicals; much domestic activity is for this purpose   washing, sweeping and so forth.
In a commercial setting, a dirty appearance will give a bad impression of a place such as a restaurant. The dirt in such cases may be classified as temporary, permanent and deliberate. Temporary dirt is streaks and detritus that may be removed by ordinary daily cleaning. Permanent dirt is ingrained stains or physical damage which require major renovation to remove. Deliberate dirt is that which results from design decisions such as decor in dirty yellow or grunge styling.



As cities developed, arrangements were made for the disposal of dirt. In Britain, the Public Health Act 1875 required households to place their refuse into a container which could be moved so that it could be carted away. This was the first legal creation of the dustbin.



Modern society is now thought to be excessively clean. Lack of contact with microorganisms in dirt when growing up is hypothesised to be the cause of the epidemic of allergies such as asthma. The human immune system requires activation and exercise in order to function properly and exposure to dirt may achieve this. For example, the presence of staphylococcus bacteria on the surface of the skin regulates the inflammation which results from injury.
People and animals may eat dirt. This is thought to be caused by mineral deficiency and so the condition is commonly seen in pregnant women.



People may become obsessed by dirt and engage in fantasies and compulsive behaviour about it, such as making and eating mud pies. The source of such thinking may be genetic, as the emotion of disgust is common and a location for it in the brain has been proposed.






Terence McLaughlin (1971), Dirt: a social history as seen through the uses and abuses of dirt, Stein and Day, ISBN 9780812814125 
Suellen Hoy (1996), Chasing Dirt: The American Pursuit of Cleanliness, Oxford University Press, ISBN 9780195111286 
Pamela Janet Wood (2005), Dirt: filth and decay in a new world arcadia, Auckland University Press, ISBN 9781869403485 
Ben Campkin, Rosie Cox (2007), Dirt: new geographies of cleanliness and contamination, I.B. Tauris, ISBN 9781845116729 
Virginia Smith; et al. (2011), Dirt: The Filthy Reality of Everyday Life, Profile Books Limited, ISBN 9781846684791  CS1 maint: Explicit use of et al. (link)



Dirt season at the Wellcome Collection