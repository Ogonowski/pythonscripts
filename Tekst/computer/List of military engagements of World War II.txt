This is a list of military engagements of World War II encompassing land, naval, and air engagements as well as campaigns, operations, defensive lines and sieges. Campaigns generally refer to broader strategic operations conducted over a large bit of territory and over a long period. Battles generally refer to short periods of intense combat localised to a specific area and over a specific period. However, use of the terms in naming such events is not consistent. For example, the Battle of the Atlantic was more or less an entire theatre of war, and the so-called battle lasted for the duration of the entire war. Another misnomer is the Battle of Britain, which by all rights should be considered a campaign, not a mere battle.



























Siege of Warsaw
Siege of Leningrad
Siege of Lw w
Siege of Modlin
Siege of Novorossiysk
Siege of Odessa
Siege of Sevastopol
Siege of Tobruk
Siege of Budapest
Siege of Breslau
Siege of Bastogne
Siege of Budapest (1945)



General
Arctic Convoys
Battle of the Atlantic - the name given to the conflict in the Atlantic Ocean between 1939 and 1945.
see also Timeline of the Battle of the Atlantic (1939-1945)

Battle of the Mediterranean
Battle of the Indian Ocean
Specific1939
The Battle of the River Plate
1940
First Battle of Narvik
Second Battle of Narvik
1941
Battle of Cape Matapan
Battle of Pearl Harbor
1942
Battle of the Coral Sea
Battle of Midway
Battle of Guadalcanal
1943
Battle of the Komandorski Islands
1944
Battle of Leyte Gulf
1945
Operation Ten-Go



General
Strategic bombing during World War II
Strategic bombing survey for the overall impact of the bombing.
Specific
Baedeker raids
Chungking
Coventry
Operation Retribution (1941) - bombing of Belgrade during 1941.
Broome - Japanese raid on the town of Broome, targeting the airfield.
Dresden
Darwin - Japanese target the harbour.
Hamburg
Helsinki - February 1944, was mostly ineffective due to air defence and deception.
Hiroshima - One nuclear weapon, Little Boy dropped from a B-29, devastating a city.
Kassel
London - "The Blitz" and the V-1 and V-2 campaigns
L beck
Nagasaki - One nuclear weapon, Fat Man dropped from a B-29, devastating a city.
Narva - March 1944. Evacuated town was destroyed by Soviet ADD.
Pearl Harbor
Rostock - Heinkel Airplane Construction Plant, Seaport, and City
Rotterdam
Stalingrad - 23 August 1942
Tallinn - February - March 1944. Bombed by Soviet ADD. Large-scale damage.
Tokyo - Several devastating raids.
Warsaw






Small to medium-sized raiding operations were carried out by both Allied and Axis armies during World War II. The modus operandi used included guerrilla attacks by partisans in occupied territory and/or combined operations involving the landing and removal of specialised light infantry, such as commandos, by means of small boats.
Allied
Operation Colossus
10 February 1941
Experimental raid by 38 British Commandos on a fresh water aqueduct near Calitri in southern Italy.

Operation Claymore
4 March 1941
1000 Men from the British Commandos and belonging to the Norwegian Independent Company 1 destroy fish oil factories on the remote islands off the coast of Norway.

Operation Archery
27 December 1941
570 men from the British Commandos and belonging to the Norwegian Independent Company 1 raid and attack German positions on V gs y Island in Norway.

Battle of Timor
19 February 1942   10 February 1943
Continuous raids from Australian commandos against the occupying Japanese.

Operation Chariot
28 March 1942
196 Royal Navy and Army Commando units raid and destroy the heavily defended docks of St. Nazaire in occupied France.

Dieppe Raid
19 August 1942
Over 6,000 infantrymen, mostly Canadian attempted to seize and hold the port of Dieppe.

Makin Island raid, 17 18 August 1942
Operation Jaywick, September 1943
Operation Jedburgh, 1944
Operation Roast, April 1945

Axis
Operation Greif, December 1944

Raiding units
Allied
Multinational
Chindits
Devil's Brigade
Z Special Unit
Popski's Private Army
Gideon Force

Australia
Australian Army Independent Companies

France
Far East French Expeditionary Forces
Intervention Light Corps

Greece
Sacred Band

United Kingdom
Long Range Desert Group
Special Air Service
Royal Marines
Special Operations Executive
British Army Commandos
Layforce

British Paratroopers

United States
Marine Raiders
US Army Rangers
Alamo Scouts
Merrill's Marauders

Axis
Nazi Germany
Brandenburger Regiment
Waffen-SS (commando force led by Otto Skorzeny).

Fascist Italy
Decima Flottiglia MAS

Empire of Japan
Special Naval Landing Forces



Atlantic Wall
Caesar Line
GHQ Line
Gothic Line
Gustav Line
Maginot Line
Mannerheim Line
Metaxas Line
Siegfried Line
Taunton Stop Line



Anglo-Iraqi War
Chinese Civil War
Greek Civil War
Second Italo-Abyssinian War
Sino-Japanese War (1937-1945)
Soviet-Japanese Border War (1939)
Spanish Civil War
Winter War (Russo-Finnish War), Continuation War, Lapland War


