Hantavirus hemorrhagic fever with renal syndrome (HFRS) is a group of clinically similar illnesses caused by species of hantaviruses from the family Bunyaviridae. It is also known as Korean hemorrhagic fever, epidemic hemorrhagic fever, and nephropathis epidemica. The species that cause HFRS include Hantaan River virus, Dobrava-Belgrade, Saaremaa, Seoul, Puumala and other hantaviruses. It is found in Europe, Asia, and Africa. Of these species, Hantaan River virus and Dobrava-Belgrade virus cause the most severe form of the syndrome and have the highest morbidity rates.
Both hemorrhagic fever with renal syndrome (HFRS) and hantavirus pulmonary syndrome (HPS) appear to be immunopathologic, and inflammatory mediators are important in causing the clinical manifestations.



Symptoms of HFRS usually develop within 1 to 2 weeks after exposure to infectious material, but in rare cases, they may take up to 8 weeks to develop. Initial symptoms begin suddenly and include intense headaches, back and abdominal pain, fever, chills, nausea, and blurred vision. Individuals may have flushing of the face, inflammation or redness of the eyes, or a rash. Later symptoms can include low blood pressure, acute shock, vascular leakage, and acute kidney failure, which can cause severe fluid overload.
The severity of the disease varies depending upon the virus causing the infection. Hantaan and Dobrava virus infections usually cause severe symptoms, while Seoul, Saaremaa, and Puumala virus infections are usually more moderate. Complete recovery can take weeks or months.
The course of the illness can be split into five phases:
Febrile phase: Symptoms include redness of cheeks and nose, fever, chills, sweaty palms, diarrhea, malaise, headaches, nausea, abdominal and back pain, respiratory problems such as the ones common in the influenza virus, as well as gastro-intestinal problems. These symptoms normally occur for three to seven days and arise about two to three weeks after exposure.
Hypotensive phase: This occurs when the blood platelet levels drop and symptoms can lead to tachycardia and hypoxemia. This phase can last for 2 days.
Oliguric phase: This phase lasts for three to seven days and is characterised by the onset of renal failure and proteinuria.
Diuretic phase: This is characterized by diuresis of three to six litres per day, which can last for a couple of days up to weeks.
Convalescent phase: This is normally when recovery occurs and symptoms begin to improve.
This syndrome can also be fatal. In some cases, it has been known to cause permanent renal failure.



Transmission by aerosolized rodent excreta still remains the only known way the virus is transmitted to humans. In general, droplet and/or fomite transfer has not been shown in the hantaviruses in either the pulmonary or hemorrhagic forms.



HFRS is difficult to diagnose on clinical grounds alone and serological evidence is often needed. A fourfold rise in IgG antibody titer in a 1-week interval, and the presence of the IgM type of antibodies against hantaviruses are good evidence for an acute hantavirus infection. HFRS should be suspected in patients with acute febrile flu-like illness, kidney failure of unknown origin and sometimes liver dysfunction.



There is no cure or vaccine for HFRS. Treatment involves supportive therapy including renal dialysis. Treatment with ribavirin in China and Korea, administered within 7 days of onset of fever, resulted in a reduced mortality as well as shortened course of illness.



Rodent control in and around the home remains the primary prevention strategy, as well as eliminating contact with rodents in the workplace and campsite. Closed storage sheds and cabins are often ideal sites for rodent infestations. Airing out of such spaces prior to use is recommended. Avoid direct contact with rodent droppings and wear a mask to avoid inhalation of aerosolized rodent secretions.



HFRS is primarily a Eurasian disease, whereas HPS appears to be confined to the Americas. The geography is directly related to the indigenous rodent hosts and the viruses that coevolved with them.



Hantavirus
Hantavirus vaccine



Sloan Science and Film / Short Films / Muerto Canyon by Jen Peel 29 minutes
"Hantaviruses, with emphasis on Four Corners Hantavirus" by Brian Hjelle, M.D., Department of Pathology, School of Medicine, University of New Mexico
CDC's Hantavirus Technical Information Index page
Viralzone: Hantavirus
Virus Pathogen Database and Analysis Resource (ViPR): Bunyaviridae
Occurrences and deaths in North and South America


