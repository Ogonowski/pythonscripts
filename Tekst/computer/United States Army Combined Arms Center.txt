The U.S. Army Combined Arms Center (USACAC) is located at Fort Leavenworth and provides leadership and supervision for leader development and professional military and civilian education; institutional and collective training; functional training; training support; battle command; doctrine; lessons learned and specified areas the Commanding General, United States Army Training and Doctrine Command (TRADOC) designates in order to serve as a catalyst for change and to support developing relevant and ready expeditionary land formations with campaign qualities in support of the joint force commander.



Components (all based in Fort Leavenworth) are:
Battle Command Knowledge System
U.S. Army Information Operations Proponent
Combat Studies Institute
Current Force Integration Directorate
U.S. Army Command and General Staff College (the Army's graduate school and the center's most famous institution)
Combined Arms Center for Training (which includes the Mission Command Training Program and the National Simulations Center)
TRADOC Program Integration Office-Battle Command
Center for Army Lessons Learned
Combined Arms Doctrine Directorate
Center for Army Leadership
Military Review
Mission Command Center of Excellence
Components (based in Fort Rucker) are:
U.S. Army Warrant Officer Career College



Fort Leavenworth, Kansas is the oldest continuously operating Regular Army installation west of the Mississippi River. This historic post, noted for its campus setting, open green spaces and hometown character, is the home of the US Army s Combined Arms Center (CAC). CAC, as a major subordinate headquarters of the US Army Training and Doctrine Command, has often been referred to as the  Intellectual Center of the Army . It is, in many regards,  home base  for the majority of field grade officers across the Army.
Since 1882, CAC and its predecessor organizations have been engaged in the primary mission of preparing the Army and its leaders for war. At present, this mission is divided between preparing the Army for the Global War on Terrorism and transforming it to meet future threats.
In order to accomplish these critical missions, CAC provides Army-wide leadership and supervision for leader development and professional military and civilian education; institutional and collective training; functional training; training support; battle command; doctrine; lessons learned; and other specified areas that the TRADOC Commander designates. All of these are focused toward making CAC a catalyst for change and to support the development of a relevant and ready ground force to support joint, interagency and multinational operations anywhere in the world.



The Combined Arms Center is organized along four basic levels:
The commander exercises overall responsibility over assigned personnel and subordinate organizations to ensure that assigned missions are accomplished in the most efficient and effective manner possible. The Command Sergeant Major, by tradition, is responsible for the conduct and development of enlisted soldiers and non-commissioned officers across the command.
The CAC Chief of Staff manages and oversees the activities of a coordinating staff and a special staff. The coordinating staff is focused on policy and procedure development for the command; the special staff provides command-wide advice in specialized or technical areas.
Major subordinate organizations carry out the majority of the functions assigned to the CAC commander. In general, each is resourced for and focused on a core function and one or more specified functions.
Schools, centers and specialized activities are spread across the country and are responsible for executing a portion of the CAC mission. In general, each of these organizations is responsible for the training of specific branch skills (such as "Infantry") and serving as the Army's functional expert in that area. In this regard, CAC is an integrator of specialized skills, on one hand, and an executor of common skills, on the other.



Since 1922, the center has published the bimonthly journal Military Review.




Since 1976 commandant of the college has been a Lieutenant General (three stars). David Petraeus was a commandant immediately before going to command the Multinational Force - Iraq.


