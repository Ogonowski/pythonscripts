In electronics, rapid single flux quantum (RSFQ) is a digital electronics technology that relies on quantum effects in superconducting devices, namely Josephson junctions, to process digital signals. Josephson junctions are the active elements for RSFQ electronics, just as transistors are the active elements for semiconductor electronics. However, RSFQ is not a quantum computing technology in the traditional sense. Even so, RSFQ is very different from the CMOS transistor technology used in conventional computers:
superconducting devices require cryogenic temperatures;
magnetic flux quanta produced by Josephson junctions are used to encode, process, and transport digital information rather than the voltage levels produced by transistors in semiconductor electronics;
the magnetic flux quanta are carried by picosecond-duration voltage pulses that travel on superconducting transmission lines, instead of static voltage levels in semiconductor electronics.
Consequently the area of the quantized voltage pulses that carry single magnetic flux quanta is constant. Depending on the parameters of the Josephson junctions, the pulses can be as narrow as 1 picosecond with an amplitude of about 2 mV, or broader (typically 5 10 picoseconds) with a lower amplitude;
since pulses usually propagate on superconducting lines, their dispersion is limited and usually negligible if no spectral component of the pulse is above the frequency of the energy gap of the superconductor;
in 2010, the typical values of the maximum pulse amplitude, usually called the IcRn product, is of the order of 0.5 to 1 mV. Rn is the normal resistance of the Josephson junction that generates the voltage pulses, while Ic is its critical current.
In the case of pulses of 5 picoseconds, it is typically possible to clock the circuits at frequencies of the order of 100 GHz (one pulse every 10 picoseconds).



Interoperable with CMOS circuitry, microwave and infrared technology
Extremely fast operating frequency: from a few tens of gigahertz up to hundreds of gigahertz
Low power consumption: about 100,000 times lower than CMOS semiconductors circuits
Existing chip manufacturing technology can be adapted to manufacture RSFQ circuitry
Good tolerance to manufacturing variations
RSFQ circuitry is essentially self clocking, making asynchronous designs much more practical.



Requires cryogenic cooling. Traditionally this has been achieved using cryogenic liquids such as liquid nitrogen and liquid helium. More recently, pulse tube refrigerators have gained considerable popularity as they eliminate cryogenic liquids which are both costly and require periodic refilling. Cryogenic cooling is also an advantage since it reduces the working environment's thermal noise.
The cooling requirements can be relaxed through the use of high-temperature superconductors. However only low to medium complexity RFSQ circuits have been achieved to date using high-Tc superconductors.
Static power dissipation that is typically 10-100 times larger than the dynamic power required to perform logic operations.
As RSFQ is a disruptive technology, dedicated educational degrees and specific commercial software are still to be developed.



Optical and other high-speed network switching devices
Digital signal processing, even up to radiofrequency signals
Ultrafast routers
Software-Defined Radio (SDR)
High speed analog-to-digital converters
Petaflop supercomputers



Superconducting logic includes newer logic families with better energy efficiency than RSFQ.
Quantum flux parametron, a related digital logic technology.






Superconducting Technology Assessment, study of RSFQ for computing applications, by the NSA (2005).



An introduction to the basics and links to further information at the State University of New York at Stony Brook.
K.K. Likharev and V.K. Semenov, RSFQ logic/memory family: a new Josephson-junction technology for sub-terahertz-clock-frequency digital systems. IEEE Trans. Appl. Supercond. 1 (1991), 3. doi:10.1109/77.80745
A. H. Worsham, J. X. Przybysz, J. Kang, and D. L. Miller, "A single flux quantum cross-bar switch and demultiplexer," IEEE Trans. on Appl. Supercond., vol. 5, pp. 2996 2999, June 1995.
Feasibility Study of RSFQ-based Self-Routing Nonblocking Digital Switches (1996)
Design Issues in Ultra-Fast Ultra-Low-Power Superconductor Batcher-Banyan Switching Fabric Based on RSFQ Logic/Memory Family (1997)
A Clock Distribution Scheme for Large RSFQ Circuits (1995)
Josephson Junction Digital Circuits   Challenges and Opportunities (Feldman 1998)
Superconductor ICs: the 100-GHz second generation // IEEE Spectrum, 2000