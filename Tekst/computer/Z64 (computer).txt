The Mr. Backup Z64 is a game backup device designed by Harrison Electronics, Inc., able to store Nintendo 64 games as ROM images on Zip Diskettes. Units such as this can make copies of a game which can be played in a Nintendo 64 emulator.



Unlike the Doctor V64, which plugs into the expansion slot in the bottom, this backup unit plugs into the cartridge slot, so the Nintendo 64 perceives it as a regular game cartridge. The unit features an upgradable BIOS, a basic back lit black text LCD panel, and buttons enabling users to unlock and use features of this backup unit. Some units include the ability to add Game Genie codes and various other cheats and saved games.



The original Z64 has a hardware set limit of 128 megabits. Because it is not capable of addressing any RAM above 16 megabytes, the user can not upgrade the RAM in order play bigger games. Once 256 megabit games became more prevalent, the parent company released hardware version 2.0 which includes a fully addressable 32 megabyte RAM chip, allowing the larger games to play. The 1.0 units can not be upgraded to 2.0. No further hardware revision has been made to allow for the playing of the few 512 megabit games that were released.



Features of the Z64 include the following:
Independently operating without connecting to computers or any peripherals
Simple installation, plug and play with only a few buttons
Capable of automatically detecting a defective game cartridge
Capable of backing up game cartridge data into Zip Disk
Capable of playing games from the cartridge
Capable of playing game files stored on the Zip Disk
Capable of storing the game record on EEPROM and SRAM
Capable of clearing data files stored in the diskette
With built-in 256 megabit memory (32 megabyte DRAM)
With built-in ZIP-100 disk drive
Store average 6 12 files in one ZIP-100 diskette
Average 16 64 seconds to back up each cartridge depending on game size
Flash BIOS for convenient upgrade from Zip Disk



The Z64's system specifications are the following:
Range of Working Temperature: 0 to 55  C*
Range of Storage Temperature: -25 to + 80  C
Net Weight: 1.5 kg
Power: 5W
Input: AC100 - 240V, 0.2A
Output: DC +5V / Maximum: 1.6A
Dimension: 24 cm x 14 cm x 13 cm (L x W x H)
Internal RAM: 256Mbits (32MB, 8x32 72-pin NP EDO SIMM) Standard
Media: 100MB PC Format Zip disk
CPU: 386SX/40 MHz
BIOS: Flashable



CD64 (Nintendo)
Doctor V64
NEO N64 Myth Cart






Mr. Backup Z64 user manual
Information about the Mr. Backup Z64
Mr. Backup Z64 Video Review