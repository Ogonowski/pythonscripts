Walgreens Boots Alliance, Inc. is an American-based multinational holding company headquartered in Deerfield, Illinois that owns Walgreens, Boots and a number of pharmaceutical manufacturing, wholesale and distribution companies. The company was formed on December 31, 2014 after Walgreens purchased the 55% stake in Switzerland-based Alliance Boots that it did not already own. Walgreens had previously purchased 45% of the company in 2012 with an option to purchase the remaining shares within three years.
Under the terms of the merger, the new company was organized into three divisions, of which Walgreens and Boots each became one. The third division is Pharmaceutical Wholesale, incorporating Alliance Healthcare. The new holding company began trading on the NASDAQ on December 31, 2014.
The combined business has operations in over 25 countries. Walgreens had formerly operated solely within the United States and its territories, while Alliance Boots operated a more international business.
In April 2015, Walgreens Boots Alliance published their first post-merger update, reporting an overall rise in sales of 35%, to $26.6bn, and a 33% rise in profits compared with last year.







Walgreens and Duane Reade operate within the Retail Pharmacy USA division of Walgreens Boots Alliance. Both businesses sell prescription and non-prescription drugs, and a range of household items, including personal care and beauty products.
Walgreens provides access to consumer goods and services, plus pharmacy, photo studio, health and wellness services in the United States through its retail drugstores. Walgreens had 8,206 drugstores as of January 31, 2014. Walgreens runs several online stores, such as: Beauty.com, Drugstore.com and VisionDirect.com.
Walgreens has stores in all 50 US states, the District of Columbia, Puerto Rico and the U.S. Virgin Islands.




Boots forms the main part of the Retail Pharmacy International division of the company. The Boots brand has a history stretching back over 160 years and is a familiar sight on Britain's high streets. Stores are located in prominent high street and city center locations as well as in local communities. Most branches include a pharmacy and focus on healthcare, personal care and cosmetic products, with most stores selling over the counter medicines.
Larger stores typically offer a variety of healthcare services in addition to dispensing prescriptions, such as flu vaccination, cholesterol screening, weight loss advice, hair retention treatment, smoking cessation advice and products, and chlamydia testing & treatment (private service). Optician services are also offered in many larger stores, with Boots Opticians providing eye tests along with the sale of spectacles and contact lenses.
Many stores also feature traditional photo processing and/or a Kodak picture kiosk where users of digital cameras and camera phones can create prints. Larger stores usually offer a range of electrical equipment such as hairdryers, curlers and foot massagers, whilst selected stores offer a range of sandwiches, baguettes, wraps, salads and beverages.
Boots operates a loyalty card program branded as the Boots Advantage Card, and claims to have 17.8 million regular users. Users earn 4 points (worth 1p each) for every  1 spent. In Ireland, users earn 4c for every  1 spent. Boots also operate a transactional website for online shopping in the United Kingdom.
Since 1936, there have been Boots stores outside the UK. Stores in countries as widely spread as New Zealand, Canada (see Pharma Plus) and France were all closed in the 1980s. Today, there are Boots branded stores outside the UK and Ireland in countries including United Arab Emirates, Bahrain, Norway, Lithuania, The Netherlands, and Thailand.
The remainder of the division is made up of the pharmacies Benavides in Mexico and Ahumada in Chile.




Alliance Healthcare forms the main part of Walgreens Boots Alliance's Pharmaceutical Wholesale division. It operates twice daily deliveries to more than 16,000 delivery points in the UK alone. Internationally, Alliance Healthcare supplies medicines, other healthcare products and related services to over 180,000 pharmacies, doctors, health centers and hospitals from over 370 distribution centers in 20 countries.
In addition to the wholesale of medicines and other healthcare products, Alliance Healthcare provides services to pharmaceutical manufacturers who are increasingly changing and adapting their approaches to distribution, while at the same time outsourcing non-core activities. These services include pre-wholesale and contract logistics, direct deliveries to pharmacies, and specialized medicine delivery including related home healthcare.
The division's Alphega Pharmacy network provides a range of services for independent pharmacies, including branding, professional training and patient care, retail support services and supply benefits together with pharmacy and IT support.



Boots produces a large number of brands that Alliance Boots and Walgreens sought to launch internationally following the first share purchase in 2012. The majority are produced by its subsidiary BCM Limited, which manufactures both own-brand and third-party medicines, as well as cosmetic ranges including No 7, Kangol, FCUK, Soltan, and Botanics. BCM has facilities in the UK, Germany, France and Poland. BCM Specials manufactures bespoke non-licensed medicines for UK hospital and retail pharmacies.
Launched in 1935, No7 is best known for its anti-aging beauty serums, developed in Nottingham that first appeared in 2007. The range comprises products designed to target the aging concerns of specific age groups. No7 became available in Walgreens and Duane Reade stores in the USA from November 2012, beginning in Los Angeles.
Launched in 1939, Soltan markets its UVA 5-star protection, a standard of protection developed by Boots and now adopted as the benchmark for suncare products in the UK. Although in both 2004 and 2015 Watchdog, a BBC consumer investigative TV program, and cited on BBC News; plus the consumer WHICH? Magazine, each did an investigation finding the 5-star rating was unsubstantiated, and skincare experts declared it to be far less safe than claimed.
First launched in 1995, the Botanics range, developed in partnership with the Royal Botanic Gardens, Kew, uses plant extracts in a variety of products, and includes a range of organic products. The Botanics range is also available through third party retailers.
The Boots own brand range of products includes skincare, medicines, healthcare products and many more. Boots Laboratories skincare range for independent pharmacy customers was launched in France and Portugal in 2008/09and is also sold in Spain, Italy and Germany.
Boots launched Almus, a brand of generic prescription drugs, in the UK in 2003. It is now sold in five countries and is an umbrella brand for a wide range of lower cost generic medicines. Alliance Boots placed considerable emphasis on the design of the packaging in an attempt to reduce the number of errors by the dispensing chemist and by the patient relating to incorrect dosage which can result in either a dangerous accidental overdose or an equally dangerous under dose.
Walgreens has at least one self-branded line of products, "Well at Walgreens".






Official website