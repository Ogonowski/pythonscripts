A constitutional amendment refers to the modification of the Constitution of a nation or state. In many jurisdictions the text of the constitution itself is altered; in others the text is not changed, but the amendments change its effect. The method of modification is typically written into the Constitution itself.
Most constitutions require that amendments cannot be enacted unless they have passed a special procedure that is more stringent than that required of ordinary legislation. Examples of such special procedures include supermajorities in the legislature, or direct approval by the electorate in a referendum, or even a combination of two or more different special procedures. A referendum to amend the constitution may also be triggered in some jurisdictions by popular initiative.
Australia and Ireland provide examples of constitutions requiring that all amendments are first passed by the legislature before being submitted to the people; in the case of Ireland, a simple majority of those voting at the electorate is all that is required, whereas a more complex set of criteria must be met in Australia (a majority of voters in a majority of states is also necessary). Switzerland has procedure similar to that of Australia.
The special procedures for the amendment of some constitutions have proven to be so exacting that of proposed amendments either few (8 Amendments out of 44 proposed), as in Australia, or none, as in Japan, have been passed over a period of several decades. In contrast, the constitution of the U.S. state of Alabama has been amended over 800 times since 1901.



There are a number of formal differences, from one jurisdiction to another, in the manner in which constitutional amendments are both originally drafted and written down once they become law. In some jurisdictions, such as the Republic of Ireland, Estonia, and Australia, constitutional amendments originate as bills and become laws in the form of acts of parliament. This may be the case notwithstanding the fact that a special procedure is required to bring an amendment into force. Thus, for example, in the Republic of Ireland and Australia although amendments are drafted in the form of Acts of Parliament they cannot become law until they have been approved in a referendum. By contrast, in the United States a proposed amendment originates as a special joint resolution of Congress that is not submitted to the President for his or her assent.
The manner in which constitutional amendments are finally recorded takes two main forms. In most jurisdictions, amendments to a constitution take the form of revisions to the previous text. Thus, once an amendment has become law, portions of the original text may be deleted or new articles may be inserted among existing ones. The second, less common method, is for amendments to be appended to the end of the main text in the form of special articles of amendment, leaving the body of the original text intact. Although the wording of the original text is not altered, the doctrine of implied repeal applies. In other words, in the event of conflict, an article of amendment will usually take precedence over the provisions of the original text, or of an earlier amendment. Nonetheless, there may still be ambiguity whether an amendment is intended to supersede or to supplement an existing article in the text.
An article of amendment may, however, explicitly express itself as having the effect of repealing a specific existing article. The use of appended articles of amendment is most famous as a feature of the United States Constitution, but it is also the method of amendment in a number of other jurisdictions, such as Venezuela.
Under the 1919 German Weimar Constitution, the prevailing legal theory was that any law reaching the necessary supermajorities in both chambers of parliament was free to deviate from the terms of the constitution, without itself becoming part of the constitution. This very wide conception of "amendment" eased the rise of Adolf Hitler to power; it was consequently explicitly ruled out in the postwar 1949 constitution, which allows amendments only by explicitly changing the constitution's text.






The Treaties of the European Union are a set of international treaties between member states that describe the constitutional basis of the European Union. Amendments must be ratified unanimously by the member states either by the national parliament or referendum.



The Constitution of Austria is unusually liberal in terms of constitutional amendments. Any piece of parliamentary legislation can be designated as "constitutional law", i.e., as a part of the constitution if the required supermajority and other formalities for an amendment are met. An amendment may take the form of a change of the Bundes-Verfassungsgesetz, the centerpiece of the constitution, a change to another constitutional act, a new constitutional act, or of a section of constitutional law in a non-constitutional act. Furthermore, international treaties can be enacted as constitutional law, as happened in the case of the European Convention of Human Rights. Over the decades, frequent amendments and, in some cases, the intention to immunize pieces of legislation from judicial review, have led to much "constitutional garbage" consisting of hundreds of constitutional provisions spread all over the legal system. This has led to calls for reform.
A majority of two-thirds in the National Council (parliament). Only in the case of a fundamental change (Gesamt nderung) of the constitution a confirmation by referendum is required. Since 1945, this has only happened once when Austria's accession to the European Union was approved by popular vote.
If a constitutional amendment limits the powers of the states, a two-thirds majority in the Federal Council of Austria is required as well. Depending on the matter on hand, two-thirds of the Federal Councilors present (attendance of one-half of all Councilors is required), or two-thirds of all Federal Councilors must approve. If the amendment would change articles 34 or 35, the majority of councilors of at least four of the nine states is an additional requirement.



The Constitution of Belgium can be amended by the federal legislative power, which consists of the King (in practice, the Federal Government) and the Federal Parliament. In order to amend the Constitution, the federal legislative power must declare the reasons to revise the Constitution in accordance with Article 195. This is done by means of two so-called Declarations of Revision of the Constitution, one adopted by the Chamber of Representatives and the Senate, and one signed by the King and the Federal Government.
Following this declaration, the Federal Parliament is automatically dissolved and a new federal election must take place. This makes it impossible to amend the Constitution unless an election has intervened. Following the election, the new Federal Parliament can amend those articles that have been declared revisable. Neither Chamber can consider amendments to the Constitution unless at least two-thirds of its members are present and the Constitution can only be amended if at least two-thirds of the votes cast are in favour of the amendment.



Under the current Constitution of Bulgaria (1991), there are two procedures for amendment, depending on the part of the constitution to be amended:
Normal amendment procedure (Articles 153 156): the Parliament can amend the Constitution for minor issues with a two-thirds majority. This shall be done in three successive readings.
Special amendment procedure (Articles 157 163): this procedure is the only way to revise the international borders of Bulgaria; change the form of government in the country; change the form in which the Constitution and international treaties are applied in Bulgaria (Article 5) or suspend citizens' rights. When such amendment is needed, the Constitution envisages an election for Great National Assembly, which consists of 400 deputies, with 200 elected by proportional vote and 200 elected by the first-past-the-post method. Then the amendments to the Constitution are passed by two-thirds majority in three successive readings.
This procedure is viewed by some critics as too slow and ineffective. There are voices in Bulgaria to remove the institution of Great National Assembly, which they view as an anachronism and to adopt a new procedure of constitutional amendment through popular vote.



Passage of a constitutional act in the Czech Republic can only be accomplished through the agreement of three-fifths of all Deputies and Senators present at the time the proposed act is laid before each house of Parliament. It is the only type of legislation that does not require the signature of the President to become law. Furthermore, it is the only type of legislation the President cannot veto.



The Constitution of Denmark provides an example of multiple special procedures that must be followed. After an amendment has been approved by parliament, a general election must be held; the new parliament must then approve the amendment again before it is finally submitted to a referendum. There is also a requirement that at least 40% of eligible voters must vote at the referendum in order for an amendment to be validly passed.



The Constitution of Estonia can only be modified by three-fifths majority in two successive complements of Parliament, and a referendum for certain chapters.



Amendments to the Constitution of France must first be passed by both houses with identical terms, and then need approval either by a simple majority in a referendum or by a three-fifths majority of the two houses of the French parliament jointly convened in Congress.



The Constitution of Ireland, which is in force in the Republic of Ireland, can only be modified by referendum, following proposal approved by the lower and upper houses of the Oireachtas amongst citizens entitled to vote for the President. The amendment succeeds by simple majority, and no quorum is required.



The Constitution of Italy can be modified by the Parliament: a constitutional bill, approved by the simple majority of both the Houses, has to be newly approved by them at least 3 months later. If two thirds of the deputies and two thirds of senators vote in favor, the bill comes in effect. If the bill is approved only by the majority of the members of the Houses, a referendum can take place, if 500,000 people, or one fifth of the members of a House, or 5 out of 20 regional legislatures require it in the three months after the approval. However, the proposal becomes law after the second approval. If it is rejected in the referendum, it is repealed. Each regional legislature or 50,000 people can propose bills to Parliament.
Constitutional bill is required to amend the national Constitution, to amend the Constitutions of autonomous regions (Sardinia, Sicily, Aosta Valley/Vall e d'Aoste, Trentino-Alto Adige/S dtirol and Friuli-Venezia Giulia), to put into effect some provisions of the Constitution (e.g. the functioning of the Constitutional Court) and to pass a law that will be considered as a part of Constitution.
A similar procedure is required to amend regional Constitutions of non-autonomous regions. In place of national Parliament approvals and referendums, regional legislatures approvals and regional referendums are required. Regional Constitution can not contrast with national Constitution, while Constitutions of autonomous regions are considered as a part of it.
Each constitutional bill approved by Italian Parliament, has met the two third majority, except in 2001 and 2006, when referendums took place. The constitutional bill approved in 2001, that has increased the powers of the Regions, was the only one that has been approved by a referendum in Italy.






Article Five of the United States Constitution describes the process whereby the federal Constitution may be altered. Twenty-seven amendments have been added (appended as codicils) to the Constitution.
Amendment proposals may be adopted and sent to the states for ratification by either:
Two-thirds (supermajority) vote of members present assuming that a quorum exists in both the Senate and the House of Representatives of the United States Congress;
OR
By a two-thirds (supermajority) vote of a national convention called by Congress at the request of the legislatures of at least two-thirds (at present 34) of the states.
All thirty-three amendment proposals that have been sent to the states for ratification since the establishment of the Constitution have come into being via the Congress. State legislatures have however, at various times, used their power to apply for a national convention in order to pressure Congress into proposing a desired amendment. For example, the movement to amend the Constitution to provide for the direct election of senators began to see such proposals regularly pass the House of Representatives only to die in the Senate from the early 1890s onward. As time went by, more and more state legislatures adopted resolutions demanding that a convention be called, thus pressuring the Senate to finally relent and approve what later became the Seventeenth Amendment for fear that such a convention if permitted to assemble might stray to include issues above and beyond just the direct election of senators.
To become an operative part of the Constitution, an amendment, whether proposed by Congress or a national constitutional convention, must be ratified by either:
The legislatures of three-fourths (at present 38) of the states;
OR
State ratifying conventions in three-fourths (at present 38) of the states.
Congress has specified the state legislature ratification method for all but one amendment. The ratifying convention method was used for the Twenty-first Amendment, which became part of the Constitution in 1933.
Since the turn of the 20th century, amendment proposals sent to the states for ratification have generally contained a seven year ratification deadline, either in the body of the amendment or in the resolving clause of the joint resolution proposing it. The Constitution does not expressly provide for a deadline on the state legislatures' or state ratifying conventions' consideration of proposed amendments. In Dillon v. Gloss (1921), the Supreme Court affirmed that Congress if it so desires could provide a deadline for ratification. An amendment with an attached deadline that is not ratified by the required number of states within the set time period is considered inoperative and rendered moot.
An amendment becomes operative as soon as it reaches the three-fourths of the states threshold. Then, once certified by the Archivist of the United States, it officially takes its place as an article of the Constitution.



State constitutions in the U.S. are amended on a regular basis. In 19 states, the state constitutions have been amended at least 100 times.
Amendments are often necessary because of the length of state constitutions, which are, on average, three times longer than the federal constitution, and because state constitutions typically contain extensive detail. In addition, state constitutions are often easier to amend than the federal constitution.
Individual states differ in the difficulty of constitutional amendments. Some states allow for initiating the amendment process through action of the state legislature or by popular initiative.



There are three methods for proposing an amendment to the California State Constitution: by the legislature, by constitutional convention, or by voter initiative. A proposed amendment must be approved by a majority of voters.
With the legislative method, a proposed amendment must be approved by an absolute supermajority of two-thirds of the membership of each house.
With the convention method, the legislature may, by a two-thirds absolute supermajority, submit to the voters at a general election the question whether to call a convention to revise the Constitution. If the majority of the voters vote yes on that question, within six months the Legislature shall provide for the convention. Delegates to a constitutional convention shall be voters elected from districts as nearly equal in population as may be practicable. The constitution does not provide many rules for the operation of the constitutional convention.
With the initiative method, an amendment is proposed by a petition signed by voters equal in number to 8% of the votes for all candidates for governor at the last gubernatorial election. The proposed amendment is then submitted to the voters at a general or special election.



There are two methods of proposing amendments to the New York Constitution. All proposed amendments must be approved by a majority of voters in a referendum.
With the legislative method, an amendment proposal must published for three months, then approved by an absolute majority of the members of each of the two houses, and approved again in a succeeding term of the houses, with an election intervening. Finally, the amendment proposal must be submitted to the people, and for ratification must be approved by a simple majority.
With the convention method, a constitutional convention must be convened by a majority vote of voters in a general election (referendum) on the question.



There are two methods for proposing amendments to the Tennessee State Constitution: through the legislature and by constitutional convention. Proposed amendments must be approved by a majority of voters in a referendum.
With the legislative method, the Tennessee General Assembly passes a resolution calling for an amendment and stating its wording. This must pass in three separate readings on three separate days, with an absolute majority on all readings. It does not require the governor's approval. It must then be published at least six months before the next legislative election in newspapers of wide and general circulation. (This is done by precedent but is not required by law.) After the election, the proposed amendment must go through the same procedure (absolute majority on three separate readings). Then it is put on the ballot as a referendum in the next gubernatorial election. To be ratified it must again achieve an absolute majority of those voting in the gubernatorial election.
With the convention method, the legislature can put on any ballot the question of whether to call a constitutional convention. It must be stated whether the convention is limited or unlimited that is, whether it can only amend the current constitution or totally abolish it and write a new one. If limited, the call must state which provisions of the current constitution are to be subject to amendment, and the subsequent convention, if approved, is limited to considering only amendments to the provisions specified in the call. The proposed amendments must then be submitted to the electorate and approved by a majority of those voting in the election. A constitutional convention cannot be held more frequently than once every six years.



The only method for proposing an amendment to the Texas State Constitution is through the legislature, either in regular or special session. The governor may call a special session, and specify the agenda for the session. To become part of the constitution, proposed amendments must be approved by a majority of voters in a referendum. Texas has had six different constitutions and the current constitution, adopted in 1876, has been amended 474 times.
A proposed amendment must be approved by an absolute supermajority of two-thirds of the elected membership of each house of the legislature. It is submitted to the voters in an election specified by the legislature. The wording of an explanatory statement that will appear on the ballot must be approved by the Texas Attorney General and printed in newspapers. The full text of the amendment must be posted by all county clerks for 30 days before the election.



The only method for proposing an amendment to the Washington State Constitution is through the legislature and can originate in either branch. The proposal must be approved by a two-thirds majority of the legislature. The proposed amendment is placed on the ballot at the next general election, and must be approved by a majority of the electors.



The procedure for amending the Constitution of Australia is detailed in Section 128 of the Constitution.
It firstly requires that the proposal pass by absolute majority in the House of Representatives. This means that out of the 150 members of the House, at least 76 of them must agree to the proposal.
If this succeeds then the proposal is moved to the Senate where it again must achieve an absolute majority, This means that of the 76 members of the Senate, at least 39 of them must agree to the proposal.
Following this, Australians then vote on the proposal. For a referendum to succeed both of the following must be achieved
A majority of states (New South Wales, Victoria, Queensland, Western Australia, South Australia and Tasmania) must agree to the proposal.
A majority of the combined votes of all of Australia must agree to the proposal.
The double majority is a major factor in why since 1906 out of 44 referendums only 8 have been successful.



The Constitution of South Africa can be amended by an Act of Parliament, but special procedures and requirements apply to the passage of constitutional amendments. A bill amending the Constitution must be introduced in the National Assembly, and cannot contain any provisions other than constitutional amendments and directly related matters.
At least 30 days before a constitutional amendment bill is introduced in the National Assembly, the person or committee introducing the amendment must publish it for public comment, submit it to the provincial legislatures, and, if it does not have to be passed by the National Council of Provinces (NCOP), submit it to the NCOP for debate. When the bill is introduced, the comments received must be tabled in the National Assembly, and in the NCOP when appropriate.
All amendments must be passed by an absolute two-thirds supermajority in the National Assembly (the lower house); as the Assembly has 400 members this requires 267 members to vote for the amendment. Most amendments do not have to be considered by the NCOP (the upper house). Amendments of the Bill of Rights, and amendments affecting the role of the NCOP, the "boundaries, powers, functions or institutions" of the provinces or provisions "dealing specifically with provincial matters" must also be passed by the NCOP with a supermajority of at least six of the nine provinces. If an amendment affects a specific province, it must also be approved by the legislature of the province concerned. Section 1, which defines South Africa as "one, sovereign, democratic state" and lists its founding values, is a specially entrenched clause and can only be amended by a three-quarters supermajority in the National Assembly and six of the provinces in the NCOP.
Once an Act is passed by the National Assembly, and by the NCOP if necessary, it must be signed and assented to by the President. As with any other Act of Parliament, by default an amendment comes into effect when it is published in the Government Gazette, but the text of the amendment may specify some other date of commencement, or allow the President to specify one by notice in the Gazette.



Some constitutions use entrenched clauses to restrict the kind of amendment to which they may be subject. This is usually to protect characteristics of the state considered sacrosanct, such as the democratic form of government or the protection of human rights. Amendments are often totally forbidden during a state of emergency or martial law.
Under Article 79 (3) of the German Basic Law, modification of the federal nature of the country or abolition or alteration of Article 1 (human dignity, human rights, immediate applicability of fundamental rights as law) or Article 20 (democracy, republicanism, rule of law, social nature of the state) is forbidden. This is supposed to prevent a recurrence of events like those during the Nazi Gleichschaltung, when Hitler used formally legal constitutional law to de facto abolish the constitution.
The final article of the Constitution of Italy (Article 139, Section 2, Title 6 of Part 2) holds the "form of Republic" above amendment.
Article 4 of Part 1 of the Constitution of Turkey states that the "provision of Article 1 of the Constitution establishing the form of the state as a Republic, the provisions in Article 2 on the characteristics of the Republic, and the provision of Article 3 shall not be amended, nor shall their amendment be proposed".
Article Five of the United States Constitution, ratified in 1788, prohibited any amendments before 1808 which would affect the foreign slave trade, the tax on the slave trade, or the direct taxation provisions of the constitution. The foreign slave trade was outlawed by an act of Congress rather than by a constitutional amendment shortly after that clause expired in 1808. Also, no amendment may affect the equal representation of states in the Senate without their own consent. If the Corwin amendment had passed, any future amendment to the Constitution "interfering with the domestic institutions of the state" (i.e., slavery) would have been banned.
Chapter 6, Article 120, section c of the Constitution of Bahrain prohibits "an amendment to Article 2 [State Religion, Shari'a, Official Language] of this Constitution, and it is not permissible under any circumstances to propose the amendment of the constitutional monarchy and the principle of inherited rule in Bahrain, as well as the bicameral system and the principles of freedom and equality established in this Constitution".
Article 112 of the Constitution of Norway provides that amendments must not "contradict the principles embodied in this Constitution, but solely relate to modifications of particular provisions which do not alter the spirit of the Constitution".
Section 284 of Article 18 of the Alabama State Constitution states that legislative representation is based on population, and any amendments are precluded from changing that.
Part 4, Section, Article 288 of the Constitution of Portugal contains a list of 15 items that amendments "must respect"; Article 288 itself can, however, be amended.
The Supreme Court of India in the Kesavananda Bharati case held that no constitutional amendment can destroy the basic structure of the Constitution of India.
Article 60 of the current 1988 Constitution of Brazil forbids amendments that intend to abolish individual rights or to alter the fundamental framework of the State the Separation of Powers and the Federal Republic.
Article 152 of the Constitution of Romania on the "limits of revision" prohibits amendments regarding the independence and territorial integrity of Romania, the independence of justice, the republican form of government, political pluralism, and the official language. It also forbids amendments which restrict civil rights and liberties.


