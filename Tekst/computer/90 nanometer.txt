The 90 nanometer (90 nm) process refers to the level of CMOS process technology that was reached in the 2004 2005 timeframe, by most leading semiconductor companies, like Intel, AMD, Infineon, Texas Instruments, IBM, and TSMC.
The origin of the 90 nm value is historical, as it reflects a trend of 70% scaling every 2 3 years. The naming is formally determined by the International Technology Roadmap for Semiconductors (ITRS).
The 193 nm wavelength was introduced by many (but not all) companies for lithography of critical layers mainly during the 90 nm node. Yield issues associated with this transition (due to the use of new photoresists) were reflected in the high costs associated with this transition.
Even more significantly, the 300 mm wafer size became mainstream at the 90 nm node. The previous wafer size was 200 mm diameter.



Use of 300 mm wafer size
Use of KrF (248 nm) lithography with optical proximity correction
512 Mbit
1.8 V operation
Derivative of earlier 110 nm and 100 nm processes



IBM PowerPC G5 970FX - 2004
IBM PowerPC G5 970MP - 2005
IBM PowerPC G5 970GX - 2005
IBM "Waternoose" Xbox 360 Processor - 2005
IBM/Sony/ Toshiba Cell Processor - 2005
Intel Pentium 4 Prescott - 2004-02
Intel Celeron D Prescott-256 - 2004-05
Intel Pentium M Dothan - 2004-05
Intel Celeron M Dothan-1024 - 2004-08
Intel Xeon Nocona, Irwindale, Cranford, Potomac, Paxville - 2004-06
Intel Pentium D Smithfield - 2005-05
AMD Athlon 64 Winchester, Venice, San Diego, Orleans - 2004-10
AMD Athlon 64 X2 Manchester, Toledo, Windsor - 2005-05
AMD Sempron Palermo and Manila - 2004-08
AMD Turion 64 Lancaster and Richmond - 2005-03
NVIDIA GeForce 8800 GTS (G80) - 2006
AMD Turion 64 X2 Taylor and Trinidad - 2006-05
AMD Opteron Venus, Troy, and Athens - 2005-08
AMD Dual-core Opteron Denmark, Italy, Egypt, Santa Ana, and Santa Rosa
VIA C7 - 2005-05
Loongson (Godson) 2  STLS2E02 - 2007-04
Loongson (Godson) 2F STLS2F02 - 2008-07
MCST-4R - 2010-12
Elbrus-2S+ - 2011-11


