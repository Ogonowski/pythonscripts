Intel's i486 OverDrive processors are a category of various Intel 80486s that were produced with the designated purpose of being used to upgrade personal computers. The OverDrives typically possessed qualities different from 'standard' i486s with the same speed steppings. Those included built-in voltage regulators, different pin-outs, write-back cache instead of write-through cache, built-in heatsinks, and fanless operation   features that made them more able to work where an ordinary edition of a particular model would not.
Each 486 Overdrive typically came in 2 versions, ODP and ODPR variants. The ODPR chips had 168 pins and functioned as complete swap-out replacements for existing chips, whereas the ODP chips had an extra 169th pin, and were used for inserting into a special 'Overdrive' (Socket 1) socket on some 486 boards, which would disable the existing CPU without needing to remove it(in case that the existing CPU is surface mounted). ODP chips will not work in Pre-Socket 1 486 boards due to the extra pin. The ODP and ODPR labeling can be found in the CPU's model number(i.e.: DX2ODPR66).



Models available included:
20 MHz FSB, 40 MHz core
25 MHz FSB, 50 MHz core
33 MHz FSB, 66 MHz core
25 MHz FSB, 75 MHz core
33 MHz FSB, 100 MHz core
2 P54 core Pentium based CPUs were released for 238pin Socket 2/Socket 3 based systems, for more information, see Pentium OverDrive



Pentium OverDrive
RapidCAD