AngelScript is a game-oriented interpreted compiled scripting language.
AngelScript features static typing, object handles (similar to C++ pointers but garbage collected via reference counting), object-orientation, single inheritance, multiple inheritance with interfaces. Allows operators to be registered and overloaded. AngelScript can be used with any C++ IDE, such as Netbeans, Geany, Eclipse, and also supported by AngelJuice IDE developed specifically for the language.
C and C++ functions can be called within an AngelScript environment. Angelscript's class syntax closely follows C++ classes by design: no proxy functions are required to embed AngelScript in C++ applications easing the two languages integration. There are several differences of AngelScript and C++:
Angelscript does not support multiple inheritance. Multiple-inheritance functionality may be achieved with Interfaces.
It is impossible to declare methods or properties outside of the class body.
All methods (including constructors and destructor) are virtual.
AngelScript is used in video game development, including Amnesia: The Dark Descent, Amy, Dustforce, Gekkeiju Online, King Arthur's Gold, Legend of the Guardians: The Owls of Ga'Hoole, Overgrowth, Penumbra: Overture, Penumbra: Requiem, Puddle, Rigs of Rods, Sine Mora, Star Ruler, SuperTuxKart, Warhammer: Mark of Chaos, Warsow. AngelScript is used at the University of Ulm in interactive 3D-Animation program. AngelScript is also used in robotics, for example, to program behavioral rules of robotic agents.






JIT compiler
http://www.angelcode.com/angelscript/