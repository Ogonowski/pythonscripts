B singen am Hochrhein ("Buesingen on the High Rhine"), commonly known as B singen, is a German town (7.62 km2 or 2.94 sq mi) entirely surrounded by the Swiss canton of Schaffhausen and, south across the High Rhine, by the Swiss cantons of Z rich and Thurgau. It has a population of about 1,450 inhabitants. Since the early 19th century, the town has been separated from the rest of Germany by a narrow strip of land (at its narrowest, about 700 m wide) containing the Swiss village of D rflingen.
Administratively, B singen is part of Germany, forming part of the district of Konstanz, in the Bundesland of Baden-W rttemberg, but economically, it forms part of the Swiss customs area, as do the independent principality of Liechtenstein and the Italian town of Campione d'Italia. There are no border controls between Switzerland and B singen or the rest of Germany since Switzerland joined the Schengen Area in 2008/09.
B singen is highly regarded as a holiday destination in summer by both German and Swiss visitors from around the area for its recreational areas along the Rhine. The town is also the home of the European Nazarene College, a relatively large and internationally oriented Bible college.




According to article 31 ZK, B singen is not part of the customs territory of the European Union. Because B singen is a German town belonging to the Swiss customs territory, there are different regulations. For example, there is no tax on coffee, which would be  2.20 per kg.



B singen is the only German town in which people mostly pay with Swiss francs, although technically the euro is legal tender as throughout Germany. Until the late 1980s, the Deutsche Mark was not accepted in B singen. Even the B singen post office only accepted Swiss francs for paying for German stamps. An amendment forced the B singen people to accept the Deutsche Mark and later the euro. But today, Swiss francs are still more popular, since most of the inhabitants work in Switzerland and are paid in Swiss francs.



On 9 September 1957, a conference between Switzerland and Germany was held in Locarno, with the target to regulate jurisdictions of both countries in B singen. A treaty was signed on 23 November 1964 and came into effect on 4 October 1967.
According to that treaty, the cantonal police of Schaffhausen are allowed to arrest people by themselves in B singen area, and bring them to Switzerland. The number of Swiss policemen is limited to 10 at the same time, the number of German police officers to three per 100 inhabitants. The Swiss police hold jurisdiction in sectors in which Swiss law is used. Otherwise, the German police are responsible.
German police officers traveling to B singen must use designated routes, and refrain from all official acts while they are in Switzerland.



B singen has a kindergarten. A current building opened in 1987 but by 1988 it was overcrowded, so expansions were added to the building. Children up to fourth grade attend an elementary school in B singen. Subsequently their parents may choose either a Swiss school or a German school.
The European Nazarene College and Seminar of the Nazarene Church is a private school which awards degrees up to Bachelor of Arts. The residential programme at B singen was discontinued in 2011. Instead people studying to be ministers attend regional centres in 17 places in Europe.



There is a German post office in B singen; B singen has two postal codes, one Swiss and one German. To send a letter to B singen, one may address it to:
8238 B singen am Hochrhein
Schweiz
or:
78266 B singen am Hochrhein
Deutschland
Letters from B singen may be franked with a Swiss or a German stamp. A standard letter from B singen to Switzerland needs either a Swiss stamp worth 85 Rappen or a German one worth 62 eurocents (approximately 74 Rappen). Outside of the post office, there are Deutsche Telekom and Swisscom phone booths.
Similarly, residents of B singen can be reached by telephone using either a German number (with the prefix +49 7734) or a Swiss one (with the prefix +41 52).



For customs reasons, B singen has its own licence plate (B S), even though it is part of Constance district which has the "KN" sign. These special licence plates were created to simplify the job of the Swiss customs officers. Only vehicles with B S licence plates are treated as Swiss vehicles.
There are very few B S licence plates. The letters B S are mostly followed by an A. B S is the rarest licence plate still in use in Germany.



The local association football team, FC B singen, is the only German team to play in the Swiss Football League.



Long under Austrian control, the town became part of the German kingdom of W rttemberg under the 1805 Peace of Pressburg agreement during the Napoleonic Wars.
In 1918 after the First World War, a referendum was held in B singen in which 96% of voters chose to become part of Switzerland. However, it never happened as Switzerland could not offer anything suitable in exchange, and consequently B singen has remained an exclave of Germany ever since. Later attempts were rejected by Switzerland.
The exclave of B singen was formally defined in 1967 through negotiations between West Germany and Switzerland. At the same time, the West German exclave of Verenahof, consisting of just three houses and fewer than a dozen people, became part of Switzerland.



Residents call the town hall "the glass palace".



B singen has 7.62 square kilometres (2.94 sq mi) of area, four times larger than Monaco. Its boundary with Switzerland is 17.141 kilometres (10.651 mi) long and is marked by 123 stones. One named stone, the Hattinger Stone, marks the B singen-D rflingen boundary. It, along with several other points, is in the Rhine River.




Baarle-Hertog
Baarle-Nassau
Campione d'Italia
Ll via
Jungholz
Kleinwalsertal
Verenahof






B singen Official Website
(German) B singen Official Website
Jan S. Krogh's GeoSite on B singen
B singen am Hochrhein (German Wikipedia)