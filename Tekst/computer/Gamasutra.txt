Gamasutra is a website founded in 1997 that focuses on all aspects of video game development. It is owned and operated by UBM TechWeb (formerly a part of CMP Media), a division of Unite Business Media, and acts as the online sister publication to the print magazine Game Developer.



Gamasutra has five main sections: the News where daily news is posted, the Features where developers post game postmortems and critical essays, Blogs where users can post their thoughts and views on various topics, Jobs/Resume where users can apply for open positions at various development studios, and Contractors where users can apply for contracted work. The articles can be filtered by either topic (All, Console/PC, Social/Online, Smartphone/Tablet, Independent, Serious) or category (Programming, Art, Audio, Design, Production, Biz/Marketing). There are three additional sections: a Store where books on game design may be purchased, RSS where users may subscribe to RSS feeds of each section of the website, and a section that links to the website's Twitter account.



While it does post news found on typical video game websites, Gamasutra is known for providing online resources to aspiring and professional game developers on the disciplines of games, including design, audio, public relations, and art. Gamasutra encourages professionals to publish blogs in order to share their expertise with other developers. Analysis articles are popular reads as writers spark discussions on game design and the various trends of the industry. The editorial staff also takes part in conducting interviews with developers and hardware designers, such as Dragon Quest creator Yuji Horii, Nintendo 3DS designer Hideiki Anno, and Portal writer Erik Wolpaw.



Project postmortems, articles which developers recount the successful and unsuccessful elements of a specific game's development, are the most celebrated features on the website, as they provide direct insight in all aspects of game design and educate other developers of various risks and important tips. Many postmortems have been published, ranging from independent games such as Okabu and The Path to major studio projects such as Okamiden and BioShock. There are currently over 150 collected post-mortems dating back as far as 1997. There have been unusual post-mortem articles published, including  A Story of GameLayers, Inc.  that reveals the tumultuous development and eventual cancellation of a Firefox toolbar-based MMORPG, and  What Went Wrong? Learning from Past Post-Mortems  that details the most common mistakes that developers make as admitted in the articles.



Gamasutra requires users who wish to publish articles on the website to work with the features director Christian Nutt, whether they have fleshed-out drafts, an outline, or a concept. The editorial staff offers help in shaping, polishing, and editing articles before publication. A broad range of topics can be selected given the audience, which consists of businesspeople, educators, and developers, both professional and aspiring. The articles are required to contain at least 1500 words, though the average length tends to be 2500 to 3500 words. Gamasutra also requires a thirty-day exclusivity period from the date of publication, after which the writer is free to take the article elsewhere and retains ownership.
Users are also allowed to comment on articles, but there is a strict set of rules. Comment guidelines are designed to keep user discussions of a given article on topic and prevent comments from reducing to flame wars (hostile interactions on the Internet), as seen on other community-driven websites where comment regulations are looser. Users are encouraged to post only constructive thoughts that add to the conversation.



GameSetWatch was Gamasutra's blog dedicated to alternative video game news. In November 2011, they went on indefinite hiatus because their news increasingly overlapped with their sister site IndieGames.com, and because they felt the mainstream gaming blogs were covering more of the "weirder" and alternative video game news.



IndieGames.com is Gamasutra's sister site dedicated to reporting on indie games. It became the UBM TechWeb's main method to deliver news about independent games after GameSetWatch closed.



Gamasutra and its team of editors won a Webby Award in 2006 and 2007; their five word acceptance speeches were "Heart plus science equals games" and "Art plus science, still games", respectively.


