eSi-RISC is a configurable CPU architecture from Ensilica. It is available in five implementations: the eSi-1600, eSi-1650, eSi-3200, eSi-3250 and eSi-3260. The eSi-1600 and eSi-1650 feature a 16-bit data-path, while the eSi-32x0s feature 32-bit data-paths. Each of these processors is licensed as soft IP cores, suitable for integrating into both ASICs and FPGAs.



The main features of the eSi-RISC architecture are:

RISC-like load/store architecture.
Configurable 16 or 32-bit data-path.
Instructions are encoded in either 16 or 32-bits.
8, 16 or 32 general purpose registers.
0, 8, 16 or 32 vector registers.
0 to 8 accumulators.
Up to 32 external interrupts.
Configurable instruction set including support for integer, floating-point and fixed-point arithmetic.
SIMD operations.
Optional support for user-defined instructions, such as cryptographic acceleration .
Optional caches (Configurable size and associtivity).
Optional MMU supporting both memory protection and dynamic address translation.
AMBA AXI, AHB and APB bus interfaces.
Memory mapped I/O.
5-stage pipeline.
Hardware JTAG debug.
While there are many different 16 or 32-bit Soft microprocessor IP cores available, eSi-RISC is the only architecture licensed as an IP core that has both 16 and 32-bit implementations.
Unlike in other RISC architectures supporting both 16 and 32-bit instructions, such as ARM/Thumb or MIPS/MIPS-16, 16 and 32-bit instructions in the eSi-RISC architecture can be freely intermixed, rather than having different modes where either all 16-bit instructions or all 32-bit instructions are executed. This improves code density without compromising performance. The 16-bit instructions support two register operands in the lower 16 registers, whereas the 32-bit instructions support three register operands and access to all 32 registers.
eSi-RISC includes support for Multiprocessing. Implementations have included up to seven eSi-3250's on a single chip.



The eSi-RISC toolchain is based on combination of a port of the GNU toolchain and the Eclipse IDE. This includes:
GCC   C/C++ compiler.
Binutils   Assembler, linker and binary utilities.
GDB   Debugger.
Eclipse   Integrated Development Environment.
The C library is Newlib and the C++ library is Libstdc++. Ported RTOSes include MicroC/OS-II, FreeRTOS, ERIKA Enterprise and Phoenix-RTOS






EnSilica's eSi-RISC homepage