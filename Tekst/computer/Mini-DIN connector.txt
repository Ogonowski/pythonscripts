The mini-DIN connectors are a family of multi-pin electrical connectors used in a variety of applications. Mini-DIN is similar to the larger, older DIN connector. Both are standards of the Deutsches Institut f r Normung, the German standards body.

Mini-DIN connectors are 9.5 mm in diameter and come in seven patterns, with the number of pins from three to nine. Each pattern is keyed in such a way that a plug with one pattern cannot be mated with any socket of another pattern. An important aspect of why each of these 7 mini-DIN connectors are official standards is because they are each drastically different from the other, with no simultaneously and directly overlapping similarities in (1) pin arrangement, (2) square key size and position, (3) circular shielding metal skirt notches and metallic additions - unlike the nonstandard mini-DIN connectors which may have directly overlapping characteristics to each other or to the standard mini-DIN connectors.
      
(The plug or male connectors shown, as visible when unplugged looking into the connector.)
The pin numbering for the plugs shown above is done left to right, bottom row to top row. Pin 1 will be on the lower left, and the highest pin number will be on the upper right.



Some notable examples of standard mini-DIN connectors include:
Mini-DIN-3 connectors were used in early implementations of Apple LocalTalk.
Mini-DIN-4 connectors are used for S-video, and were used for Apple Desktop Bus.
Mini-DIN-6 connectors were used for IBM PC compatible PS/2 keyboard and mouse ports and for Acorn Archimedes keyboards (prior to the A7000 were proprietary, A7000 and later were standardised PS/2).
Mini-DIN-8 connectors were used for Apple and Sun Microsystems keyboard and mouse ports, as well as for serial printer, modem, and Apple LocalTalk connections.
Mini-DIN-9 connectors were used for Acorn Archimedes Quadrature Mice and Microsoft InPort Bus Mice (not interchangeable).
Mini-DIN-7 and Mini-DIN-9 connectors have been used for a variety of audio and video applications. Also, iRobot Roomba Vacuum cleaning robots use a Mini-DIN-7 to expose an interface for custom sensing and control.
Mini-DIN-6 and Mini-DIN-8 connectors are frequently used in ham radio applications to interface with computers for data packet communications and radio programming.




Several non-standard sockets are designed to mate with standard mini-DIN plugs. These connectors provide extra conductors, and are used to save space by combining functions in one connector that would otherwise require two standard connectors.
Other non-standard connectors mate only with their matching connectors, and are mini-DIN connectors only in the sense of sharing the 9.5 mm plug body. These mini-DIN style plugs are not approved by the Deutsches Institut f r Normung, the German standards body, and many applications could be considered proprietary.
      
(The plug or male connector are shown, as visible when unplugged; female sockets are mirror images.)



JVC Mini-DIN 8
Allen-Bradley Micrologix PLC Mini-DIN 8
Beyerdynamic microphone connector
Some 3-pin and 4-pin DC power connectors look similar enough to Mini-DIN connectors to be erroneously referred to as "Mini-DIN" connectors sometimes.


