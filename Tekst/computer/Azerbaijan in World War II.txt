The Azerbaijan Soviet Socialist Republic entered World War II with the whole Soviet Union, on June 22, 1941. German command gave the great consideration to oil pools of Baku and set a task to take Baku and oil and gas area of Baku under their control in the course of the Battle of the Caucasus. More than 600,000 people from Azerbaijan were conscripted to the Workers  and Peasants  Red Army during World War II, from 1941 to 1945.







Baku and the North Caucasus were the main oil sources for the whole economy of the USSR. 80% of oil was extracted in Azerbaijan throughout the whole USSR. Significance of the Caucasus and Kuban as the source of grain was sharply increased after loss of Ukraine. Reserves of strategic raw materials such as deposit of tungsten-molybdenum ore were in these places. Loss of the Caucasus could cause a significant influence upon general course of the war against the USSR, and that is why Hitler had chosen namely this direction as the main. Group of armies, created for attack to the Caucasus, was given coded identification  A .
Tasks of the group  A  were the following: to surround and destroy the Southern Front in the southern and south-western parts of Rostov-on-Don, which drew off over the Don River, and to capture the North Caucasus; then it was intended to pass round Greater Caucasus with a group from the South, capturing Novorossiysk and Tuapse and with the other group from the East, capturing oil regions of Grozny and Baku. Simultaneously, it was intended to overcome the Dividing Ridge in its central part over its passes and entrance to Georgia, with a bypass manoeuvre.
On February 1, 1942 the Central Inquiry Office under the Soviet of Evacuation carried out a general census of the population came from threatened areas of the USSR, according to the order of the State Defense Committee. According to data of the census, 2745 people were evacuated from Zagatala, Beylagan, Imishli and Bilasuvar Rayons of the Azerbaijan SSR, among which were 114 Russians, 65 Ukrainians, 2545 Jews, 15 Poles, and also Armenians, Tatars, Moldovans and Georgians. 387 Russians, 386 Jews, 168 Ukrainians, 73 Armenians, 5 Georgians, 7 Azerbaijanis, 11 Poles, 8 Tatars and also representatives of other nations of evacuated people came to Gorodskoy and Dzherjinski regions of Baku by February 1, 1942 according to the census.



German army approached the South Caucasus. Even the date of seizure of Baku   September 26, 1942 - was fixed. Defense regions were created around Nalchik, Vladikavkaz, Grozny, Makhachkala and Baku. The first stage of the battle of the Caucasus was from July to December, 1942. German-Romanian army, suffering a great loss, could reach foothills of Greater Caucasus and the Terek River. But, generally,  edelweiss  plan of Germany failed. Only during the 1st stage of the battle the  A  group of armies lost about 100,000 people and Germans couldn t burst the South Caucasus and the Near East.



From August 25 to September 17, 1941 the Great Britain and the Soviet Union carried out a joint campaign under a code name  Consent . Defense of Iranian oil deposits and pools and their capture by German army and their allies, and also defense of transportation corridor (southern corridor), along which allies supplied the Soviet Union according to Lend-Lease, was its main goal. During World War II, Rez  Sh h refused to deploy troops of the Great Britain and the Soviet Union at their request. But, for instance, according to the 5th and 6th points of the agreement between Soviet Russia and Iran of 1921, the USSR had a right to deploy its troops in the territory of Iran in case of appearance of any threat to its southern borders.
Armed forces of the allies invaded Iran, dethroned Reza Pahlavi and established their control over railway roads and oil deposits of the country, in course of the campaign. Meanwhile, troops of the United Kingdom occupied southern Iran, and troops of the Soviet Union   northern part of the country (Iranian Azerbaijan).







Cargo delivery from the USA and the United Kingdom within the Lend-Lease program was implemented through the Trans-Iranian route. Delivery of cargo was implemented by water crafts of the Caspian Flotilla. Cars go along the following routes: Tehran-Ashgabat, Tehran-Astara-Baku and Julfa-Vladikavkaz.
Railwaymen of Azerbaijan carried out a maintenance and general exploitation of Julfa-Tabriz railway road according to rules of the wartime.




681 thousand people from the total population of 3.4 million people were conscripted to the front from the Azerbaijan SSR, and 10,000 women, 300,000 citizens of the USSR conscripted from Azerbaijan perished in the battlefields. 15,000 nurses and sanitary activists, 750 operators and 3,000 drivers were trained for military units. Azerbaijani women also participated in the war. Partisan Aliya Rustambeyova, sniper Ziba Ganiyeva, anti-craft gunner Almaz Ibrahimova, captain of a steamship Shovkat Salimova and others were among them.
Defense of Brest Fortress, Siege of Leningrad, Battle of Moscow, battles of Stalingrad, Caucasus and Kursk, Ukraine, mainly Crimean peninsula, and also Baltic Offensive, Eastern Europe and Battle in Berlin were the main battlefields of soldiers from the Azerbaijan SSR.



The following divisions were created in the territory of the Azerbaijan SSR:
87 battalions
1123 self-defense squads
Were established:
The 77th infantry division
The 223rd infantry division
396th infantry division
402nd infantry division
416th infantry division
Military units and formations in which significant part of Azerbaijani citizens took part:
416th Taganrog, Red Banner, Order of Suvorov Division Infantry Division
76th Mountain Infantry Division named after K.E.Voroshilov (later the 51st Guards Rifle Division of Taganrog)
77th 'Simferopolskaya Red Banner, Order of Suvorov Division Sergo Ordzhonikidze' Mountain Rifle Division
223rd 'Belgrade Red Banner Azerbaijan' Rifle Division
227th Rifle Division
396th Rifle Division
402nd Rifle Division



128 residents of Azerbaijan were conferred on title of Hero of the Soviet Union for military valour and feats shown during World War II. Hazi Aslanov received this title twice. There were 42 ethnic Azerbaijanis who received the title of Hero of the Soviet Union and 14 of them were awarded posthumously. Lieutenant Israfil Mammadov was the first Azerbaijani who became Hero of the Soviet Union.









The Caspian fleet took the first place for cargo-turnover until World War II. Approximately one third of cargo transported throughout all seas of the USSR was delivered along the Caspian Sea. Proximity of oil deposits, petroleum refineries and cotton storehouses to the sea was the reason for it. Transportation of wood and bread to regions of the Caucasus and Middle Asia, which were delivered to the Caspian Sea through the Volga and Ural, also stipulated the high level of cargo-turnover. Oil and oil products, which were mainly transported from Baku to Astrakhan for further transportation to other regions of the USSR through the Volga River, were the main goods determining significance of the Caspian Sea. Territory of Azerbaijan, mainly Baku-Baladjary point, located on the junction of railway and marine roads, was the important link on the railway road of the South Caucasus.
Baku oil was transported to all regions of the USSR and agricultural provision of the Azerbaijan SSR from central regions of the USSR was realized along this railway road. Baku port was distinguished for high intensity of cargo handling and capability of passing. Transportation of oil to the Volga River was the main content of the port.



Baku Air Defense Army was established by resolution of the State Defense Committee since April 5. Its administration was created on the basis of reformed administration of the 3rd corps of anti-craft warfare. Organizationally it was part of the South Caucasus zone of anti-craft defense, but since April, 1944 it became part of the South Caucasus front of anti-craft defense. In May October, 1942, during the period of active actions of German reconnaissance crafts in the borders of the army, its war strength included in:
The 8th destructive aircraft corps of anti-craft defense (6 destructive aircraft regiments),
7 regiments of antiaircraft artillery,
1 regiment of antiaircraft machineguns,
1 searchlight regiment,
A regiment of aerostatic obstacle,
A regiment of troops of air observation, warning and connection,
Other distinct parts.
Commanders: P.M.Beskrovnov, general-major of the artillery (April 1942   February 1945) and N.V.Markov, general-lieutenant of the artillery (February, 1945   till the end of the war).







Until the beginning of World War II the Azerbaijan SSR was a locomotive of the sector: the main provider of oil and oil products, forge of specialists in oil sector and producer of oil petroleum equipment. Despite the military actions, Baku remained the main provider of fuels and lubricants. Azerbaijani oilmen produced 80% of fuel of the whole country during the wartime. In the first year of the war, they sent 23,5 million tones of oil. Only 75 million tones of oil were transported for military needs during World War II. Georgy Zhukov, marshal of the Soviet Union wrote:
Vasiliy Istratov, former ambassador of Russia to Azerbaijan wrote:
Toiling under  Everything for front!Everything for victory!  slogan Azerbaijani oilmen were awarded a Red challenge banner of the State Defense Committee, All-Union Central Council of Professional Unions and National Commissariat of Oil Industry of the USSR, which was the indicator of high evaluation of the work.
Nikolai Baibakov, a citizen of Baku, was in charge of a special headquarters coordinating the work on supply of military units with fuel. In 1942, he became an authorized person of the State Defense Committee in destruction of oil wells and oil-producing enterprises in the Caucasian region. He organized the work in the following way: in the time of enemy s approach all valuable equipment was demonstrated and sent to the east of the country, stripped wells were immediately wrecked, but significantly productive ones continued being used and were wrecked only in very utmost conditions. As a result, Germans couldn t use resource of Krasnodar oil fields. Later, N.K.Baibakov became a representative of the State Defense Committee in relocation of oilmen and equipment of Caucasian regions to the East. In 1941, The State Defense Committee decided to relocate parts of oil enterprises of Baku, evacuate residents and reorganize transportation flows. According to N.K.Baibakov,  Great Resettlement  of Baku oilmen is happening . More than 10 thousands of people were sent to Krasnovodsk with their families and oil equipments on steamships and tankers, and then to unpopulated regions by the trains. N.K.Baibakov wrote in his  Azerbaijan-my motherland  book that:
Equipment, specialists and their families were sent to  The second Baku  district (Bashkortostan, Samara and Perm Oblasts) for creation of new oil enterprises and factories there. So,  Krasniy Proletariy  factory of oil engineering industry of Baku was transferred to Sterlitamak, factory named after Myasnikov to Perm, factory named after Dzerjinski to Sarapul, The State Engineering Plant of the Union named after Stalin to Ishimbay. In autumn  Azneftrazvedka  trust  was transferred to Volga Region and A.F.Rustambeyov, prominent oilman of Azerbaijan, was engaged in organization of its activity in the new place.

In 1942, by the order of the State Defense Committee Oil Refinery of Baku was stopped to produce various kinds of products, for solving the main task   provision of the aviation with ultimate gasoline. 13 million tones of 17 million oil products were produced in Azerbaijan. Approximately 85% of aviation gasoline was produced in Azerbaijan. 1 million tones of - B-78 - high-octane gasoline was delivered to the front. Terminal stations, which were located on the shore of the Caspian Sea and The Volga River and other great population aggregates and which adjoined railway roads, were created. Transportation of oil and oil products from Baku to petroleum storage depots and to oil refineries of various regions of the country was implemented through the sea and then by railway roads. In July of the same year navigation in the Volga was stopped because of entrance of German troops to Volgograd. The main railway roads along which oil and oil products were sent from Baku to the front were blocked by German troops. German-fascist troops were at the gates of the Caucasus, they tended to Baku petroleum and Baku itself was under threat of war. As the direct road for oil transportation was blocked, a new way for transportation of oil products to Stalingrad had to be found. It was decided to send oil products through the only way through Krasnovodsk, and then through the railway road of Middle Asia and Kazakhstan to Stalingrad. But there was not enough amount of cisterns in the railway road of Middle Asia for transportation of oil. And then Baku administration of the sector had a risky solution for that: to send cisterns of oil products afloat by tugs to Krasnovodsk along the sea and then to transport then to railway road.
In September, 1942 a military situation was announced in the South Caucasus and situation in Baku became critical. Only 1,6 million tones of oil was sent instead of 6 million tones till the end of navigation. Special oil wells, into which were pumped hundred thousand tones of topped oil, were allocated. Scarcity of tanks led to collapse of work.  Neftchala    was the only trust which produced petroleum in autumn. As N.K.Baibakov wrote:
Konstantin Rokossovsky, marshal of the Soviet Union in his letter to the Central Committee of the Communist Party of the Azerbaijan SSR wrote :
On April 28, 1945 Fyodor Tolbukhin, marshal of the Soviet Union in his article under a subtitle of  Glory to Azerbaijani nation  wrote:
Semyon Budyonny, marshal of the Soviet Union, who came to Baku after the war, said journalists the following words:
Gleb Krasnevsky, Secretary General of the Embassy of Belarus in Azerbaijan said:

World War II on stamps of Azerbaijan


