Dorothy Elizabeth Denning (born August 12, 1945) is the daughter of C. Lowell and Helen Watson Robling. A prominent American information security researcher who is a graduate of the University of Michigan, she has published four books and 140 articles. At Georgetown University, she was the Patricia and Patrick Callahan Family Professor of computer science and director of the Georgetown Institute of Information Assurance. She is now a professor in the Department of Defense Analysis at the Naval Postgraduate School.
Denning has received several awards. Among them are the Augusta Ada Lovelace Award, National Computer Systems Security Award, and the 2004 Harold F. Tipton Award "in recognition of her outstanding information security career". In 1995 she was inducted as a Fellow of the Association for Computing Machinery.
Denning privately reviewed, at federal request, the Skipjack block cipher, as part of the controversial Clipper chip initiative, put forth by the NSA for encryption of private communications. In Congressional testimony, she stated that general publication of the algorithm would enable someone to build a hardware or software product that used SKIPJACK without escrowing keys. In public forums, such as the Usenet forum comp.risks, she defended this program. Denning also served as a witness in the 1990 trial of United States v. Riggs. Her testimony was instrumental in leading the government to drop charges against defendant Craig Neidorf.



In 1974 she married Peter J. Denning. He is currently the Chairman of the Computer Science Department at the Naval Postgraduate School.








