Gruy re (/ ru j r/ or / r j r/; French pronunciation:  [ yj ], German: Greyerzer) is a hard yellow cheese, named after the town of Gruy res in Switzerland, and originated in the cantons of Fribourg, Vaud, Neuch tel, Jura, and Berne. Before 2001, when Gruy re gained the appellation d'origine contr l e (AOC, now AOP) status as a Swiss cheese, some controversy existed whether French cheeses of a similar nature could also be labelled Gruy re (French Gruy re style cheeses include Comt  and Beaufort).
Gruy re is sweet but slightly salty, with a flavor that varies widely with age. It is often described as creamy and nutty when young, becoming with age more assertive, earthy and complex. When fully aged (five months to a year) it tends to have small cracks which impart a slightly grainy texture.



Gruy re cheese is generally known as one of the finest cheeses for baking, having a distinctive but not overpowering taste. In quiche, Gruy re adds savoriness without overshadowing the other ingredients. It is a good melting cheese, particularly suited for fondues, along with Vacherin and Emmental. It is also traditionally used in French onion soup, as well as in croque-monsieur, a classic French toasted ham and cheese sandwich. Gruy re is also used in chicken and veal cordon bleu. It is a fine table cheese, and when grated, it is often used with salads and pastas. It is used, grated, atop le tourin, a type of garlic soup from France which is served on dried bread. White wines, such as Riesling, pair well with Gruy re. Sparkling apple cider and Bock beer are also beverage affinities.




To make Gruy re, raw milk is heated to 34  C (93  F) in a copper vat, and then curdled by the addition of liquid rennet. The curd is cut up into pea sized pieces and stirred, releasing whey. The curd is cooked at 43  C (109  F), and raised quickly to 54  C (129  F).
The whey is strained, and the curds placed into molds to be pressed. After salting in brine and smearing with bacteria, the cheese is ripened for two months at room temperature, generally on wooden boards, turning every couple of days to ensure even moisture distribution. Gruy re can be cured for 3 to 10 months, with long curing producing a cheese of intense flavor.



In 2001, Gruy re gained the Appellation d'origine contr l e status. Since then the production and the maturation is defined in the Swiss law, and all Swiss Gruy re producers must follow these rules. To be accepted throughout Europe as an AOC, the "Interprofession du Gruy re" in Switzerland plans to make a transnational AOC with the French producers of Gruy re.



Gruy re-style cheeses are very popular in Greece, where the local varieties are known as   (gravi ra). Some Greek gruy res come from San Mich l  (   , "St. Michael's") from the island of Syros in the Cyclades, the Naxian varieties, that tend to be milder and more sweet and various gravi ras from Crete.
Gruy re-style cheeses are also produced in the United States, Wisconsin having the largest output.




An important and the longest part of the production of the Le Gruyere Switzerland AOC is the "affinage" (French for maturation).
According to the AOC, the cellars to mature a Swiss Gruy re must have a climate close to that of a natural cave. This means that the humidity should be between 94% to 98%. If the humidity is lower, the cheese dries out. If the humidity is too high, the cheese does not mature and becomes smeary and gluey. The temperature of the caves should be between 13  C (55  F) and 14  C (57  F). This relatively high temperature is required for excellent quality cheese. Lower quality cheeses result from temperatures between 10  C (50  F) and 12  C (54  F). The lower the temperature is, the less the cheese matures, resulting in a texture that is harder and more crumbly.




Le Gruy re Switzerland AOC has many different varieties, with different aged profiles, and an organic version of the cheese is also sold. There is a special variety that is produced only in summer on the Swiss Alps: the Le Gruy re Switzerland AOC Alpage.
Generally, one can distinguish the following age profiles.
mild/doux: min. 5 months old
r serve: min. 10 months old
In Switzerland, many other age profiles can be found, including surchoix, vieux, sal , and H hlengereift (cave aged), but these age profiles are not part of the AOC.
The French Le Brou re cheese, made in nearby Vosges, is considered a variant of Gruy re.



Le Gruy re Premier Cru is a special variety, produced and matured exclusively in the canton of Fribourg and matured for 14 months in humid caves with a humidity of 95% and a temperature of 13.5  C (56.3  F).
It is the only cheese that has won the title of best cheese of the world at the World Cheese Awards in London four times: in 1992, 2002, 2005 and 2006.


