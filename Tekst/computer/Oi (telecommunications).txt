Oi (IPA: [ oj], Portuguese for "Hi"), formerly known as Telemar, is the largest telecommunications company in Brazil and South America, both in terms of subscribers and revenues. It is headquartered in Rio de Janeiro. Oi's major subsidiaries include Telemar and Brasil Telecom.
At the end of 2013 Oi had 74.5 million subscribers, including 16.9 million for landline, 50.3 million for wireless , 5.3 million for ADSL, and 1 million for other services.
In 2013, Oi announced its merger with Portugal Telecom, the largest telecommunication company in Portugal, in order to strengthen the Brazilian firm and simplify its ownership structure. In June 2015 Portugal Telecom was acquired by Altice Group.



Oi's chief executive officer (as of October 2014) is Bayard Gontijo, who also serves as the company's chief financial officer and director of investor relations.



Oi (then known as Telemar) was formed as Tele Norte Leste to merge sixteen state-owned incumbent local exchange carriers, during the privatization of Brazilian telecommunications system. Each carrier served a particular Brazilian state in the northern, northeastern and southeastern part of the country. In the break-up of Telebras in 1998 it was sold to a consortium led by the Brazilian construction firm Andrade Gutierrez (21.2%) and Inepar Holdings (20%) as well as other Brazilian corporate and individual investors. The consortium paid 3.434 billion reais.
The states that formed the base of Telemar at its inception were Rio de Janeiro, Minas Gerais, Esp rito Santo, Bahia, Sergipe, Alagoas, Pernambuco, Para ba, Rio Grande do Norte, Piau , Cear , Maranh o, Par , Amazonas, Amap  and Roraima, corresponding to 65% of the Brazilian territory and 20 million households.
Initially, Telemar was allowed to offer only local voice and data services and interstate long distance voice services. Today, Telemar and its subsidiaries offer local, long distance and international voice and data services, besides a growing mobile phone network.
In April 2006, it was announced that Telemar would restructure itself, merging its three holding companies into a single company, that would have been named either Telemar Participa es S.A. or Oi Participa es S.A. However, those plans failed, since there was no consensus between Telemar shareholders. But on March 1, 2007, Telemar rebranded itself to "Oi", unifying all of its companies and services under the Oi umbrella. The company is still legally known as "Telemar Norte Leste S.A.", "Tele Norte Leste Participa es S.A." and "Telemar Participa es S.A.". Oi owns the brands:
Oi Fixo (landline service, formerly Telefone Telemar)
Oi M vel (mobile service, cornerstone of the Oi brand)
Oi Velox (ADSL, 3G formerly Velox)
Oi Internet (ISP)
31 (long-distance and international calling)
Oi Wi-Fi (Wi-Fi access, at home or via hotspots)
Oi TV (DTH pay TV)
Oi Voip (Voice over IP)
In 2010, Portugal Telecom acquired 22.4% of Oi shares.
In February 2014, Oi announced it would raise $5.9 billion in a share offering as part of the firm s merger process with Portugal Telecom.






Oi launched its mobile network in 2002 in its license states. It was the first network using GSM in Brazil. Oi has the practice of not calling its phones "cell phones", but rather "Ois". In 2007, Oi started selling only unlocked handsets, focusing on SIM card and plan sales. In October 2007, Oi acquired a license to operate with GSM in S o Paulo, where the network went live on October 24, 2008. In December 2007, Oi purchased licenses to operate a 3G network in its area, including S o Paulo, but with the exception of the Franca area. That network is expected to go live in 2009. Also in December 2007, Oi announced its purchase of Amaz nia Celular, which was a condition of the sale of its sister company, Telemig Celular, to Vivo.



Oi Internet is an ISP that was launched in 2004. Oi Internet started services with a promotion that offered 31% off the dial-up connection costs on the subscriber's bill. However, Anatel, the Brazilian telecom regulator, did not allow this practice. Later, the ISP relaunched the promotion, offering 31% of the dial-up connection costs deposited in the subscriber's bank account or twice of that on a prepaid Oi phone. The Oi Internet dial-up dialer can send SMS messages to Oi phones.
In early 2005, Oi Internet launched its broadband services, initially available only for Oi's Oi Velox DSL subscribers, but now also available for Brasil Telecom Turbo and Telef nica Speedy subscribers.



In 2008, Oi announced it would purchase Brasil Telecom, creating a major Brazilian telecommunications company, already nicknamed "Supertele" or "SuperOi". That takeover required changes in legislation, which at the time prohibited a fixed telephone company from purchasing another fixed telephone company in a different license area. That legislation has changed since, and Oi completed its purchase of Brasil Telecom on January 9, 2009. Rollout of the Oi brand in the Brasil Telecom area starts with prepaid mobile service on May 17, 2009.



Brazilian Olympic Committee
Sandro Dias
S o Paulo Fashion Week (Spring/Summer 2009 edition)
Fashion Rio - Rio Fashion Week
2007 Pan American Games, held in Rio de Janeiro
Oi Casa Grande - teatro no Rio de Janeiro
Oi FM - online radio station
Oi Fashion Rocks Brasil 2009, 2010
X Games Brasil
2014 FIFA World Cup Brazil



2002-2013: Simples assim. ("It's that simple.")
2013 present: A Oi completa voc . ("Oi completes you.")




Telecommunications in Brazil
List of internet service providers in Brazil






The company's investor relations page in English
Mundo Oi website