Odoo (formerly known as OpenERP and before that, TinyERP) is a suite of open-source enterprise management applications. Targeting companies of all sizes, the application suite includes billing, accounting, manufacturing, purchasing, warehouse management, and project management.
The source code for the OpenObject framework and core ERP (enterprise resource planning) modules is curated by the Belgium based Odoo S.A. Additionally, customized programming, support, and other services are provided by an active global community and a network of 500 official partners. The main Odoo components are the OpenObject framework, about 30 core modules (also called official modules) and more than 3000 community modules.
Odoo has been used as a component of university courses. A study on experiential learning suggested that OpenERP provides a suitable alternative to proprietary systems to supplement teaching.
Several books have been written about Odoo, some covering specific areas such as accounting or development.
Odoo has received awards including Trends Gazelle and BOSSIE Awards three years in a row.




The 3 last LTS version are supported in parallel. This means that when a new LTS version is released, an older version reaches its end-of-life, and is not supported anymore. As an example, 7.0 LTS will be supported along with 8.0 LTS and 9.0 LTS, but will reach end-of-life when 10.0 LTS is released.




Comparison of accounting software
List of free and open source ERP packages
List of free and open source software packages concerning finance
Tryton, a fork of TinyERP version 4.2.






Official website
Odoo Community Association (OCA)