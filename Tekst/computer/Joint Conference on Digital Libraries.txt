The Joint Conference on Digital Libraries (JCDL) is an annual international forum focusing on digital libraries and associated technical, practical, and social issues. It is jointly sponsored by the Association for Computing Machinery and the IEEE Computer Society. It was formed in 2000 by combining the ACM Digital Libraries Conference (DL) and the IEEE CS Advances in Digital Libraries (ADL) Conference. The Conferences awards the Vannevar Bush prize for best paper.



Joint Conference on Digital Libraries