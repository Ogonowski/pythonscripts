The following are articles about the topic of France during World War II:

Maginot Line and Alpine Line of fortifications and defences along the borders with Germany and Italy

Phoney War, or dr le de guerre ("strange war"), the period of little military activity between the defeat of Poland in October 1939 and April 1940.
Anglo-French Supreme War Council set up to organize a joint Entente Cordiale strategy against Germany

The Battle of France, in which the German victory led to the fall of the Third Republic in May and June 1940.
Free France (La France Libre) the government-in-exile in London and provisional government over unoccupied and liberated territories, and the forces under its control (Forces fran aises libres or FFL), fighting on the Allies' side after the Appeal of 18 June of its leader, General de Gaulle.
French Liberation Army (Arm e fran aise de la Lib ration) formed on 1 August 1943 by the merger of the FFL and all other Free French units, principally the Army of Africa
French Forces of the Interior (Forces fran aises de l'int rieur) elements of the Resistance loyal to London and under its operational military command
Free French Air Force
Free French Naval Forces

Vichy France, the rump state established in June 1940 under marshal P tain in the non-occupied Zone libre, officially neutral and independent until invaded by the Axis and the Allies in November 1942
Vichy French Air Force
Scuttling of the French fleet in Toulon

Axis occupation of France:
German occupation of France during World War II - 1940-1944 in the northern zones, and 1942-1944 in the southern zone
French Resistance and the National Council of the Resistance which coordinated the various groups that made up the resistance
Service du travail obligatoire - the provision of French citizens as forced labour in Germany
The Holocaust in France
Italian occupation of France during World War II - limited to border areas 1940-1942, almost all Rh ne left-bank territory 1942-1943
Japanese and Thai occupation of French Indochina - beginning with the Japanese invasion in September 1940 and with the Franco-Thai War which started in October 1940

Liberation of France
Operation Overlord - the invasion of northern France by the western Allies in June 1944
Operation Dragoon - the invasion of southern France by the western Allies in August 1944
Liberation of Paris - the freeing of the French capital in August 1944

Allied advance from Paris to the Rhine - advance (as the right flank of the western front) into Alsace-Lorraine in 1944
Western Allied invasion of Germany - invasion (as the right flank of the western front) of Baden-W rtemberg in 1945