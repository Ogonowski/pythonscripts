Digital evidence or electronic evidence is any probative information stored or transmitted in digital form that a party to a court case may use at trial. Before accepting digital evidence a court will determine if the evidence is relevant, whether it is authentic, if it is hearsay and whether a copy is acceptable or the original is required.
The use of digital evidence has increased in the past few decades as courts have allowed the use of e-mails, digital photographs, ATM transaction logs, word processing documents, instant message histories, files saved from accounting programs, spreadsheets, internet browser histories, databases, the contents of computer memory, computer backups, computer printouts, Global Positioning System tracks, logs from a hotel s electronic door locks, and digital video or audio files.
Many courts in the United States have applied the Federal Rules of Evidence to digital evidence in a similar way to traditional documents, although important differences such as the lack of established standards and procedures have been noted. In addition, digital evidence tends to be more voluminous, more difficult to destroy, easily modified, easily duplicated, potentially more expressive, and more readily available. As such, some courts have sometimes treated digital evidence differently for purposes of authentication, hearsay, the best evidence rule, and privilege. In December 2006, strict new rules were enacted within the Federal Rules of Civil Procedure requiring the preservation and disclosure of electronically stored evidence. Digital evidence is often attacked for its authenticity due to the ease with which it can be modified, although courts are beginning to reject this argument without proof of tampering.



Digital evidence is often ruled inadmissible by courts because it was obtained without authorization. In most jurisdictions a warrant is required to seize and investigate digital devices. In a digital investigation this can present problems where, for example, evidence of other crimes are identified while investigating another. During a 1999 investigation into online harassment by Keith Schroeder investigators found pornographic images of children on his computer. A second warrant had to be obtained before the evidence could be used to charge Schroeder.



As with any evidence, the proponent of digital evidence must lay the proper foundation. Courts largely concerned themselves with the reliability of such digital evidence. As such, early court decisions required that authentication called "for a more comprehensive foundation." US v. Scholle, 553 F.2d 1109 (8th Cir. 1976). As courts became more familiar with digital documents, they backed away from the higher standard and have since held that "computer data compilations  should be treated as any other record." US v. Vela, 673 F.2d 86, 90 (5th Cir. 1982).
A common attack on digital evidence is that digital media can be easily altered. However, in 2002 a US court ruled that "the fact that it is possible to alter data contained in a computer is plainly insufficient to establish untrustworthiness" (US v. Bonallo, 858 F. 2d 1427 - 1988 - Court of Appeals, 9th).
Nevertheless, the "more comprehensive" foundation required by Scholle remains good practice. The American Law Reports lists a number of ways to establish the comprehensive foundation. It suggests that the proponent demonstrate "the reliability of the computer equipment", "the manner in which the basic data was initially entered", "the measures taken to ensure the accuracy of the data as entered", "the method of storing the data and the precautions taken to prevent its loss", "the reliability of the computer programs used to process the data", and "the measures taken to verify the accuracy of the program". 7 American Law Reports 4th, 8, 2b.
In its turn it gave rise to a breed of commercial software technology solutions designed to preserve digital evidence in its original form and to authenticate it for admissibility in disputes and in court.



In the United Kingdom, examiners usually follow guidelines issued by the Association of Chief Police Officers (ACPO) for the authentication and integrity of evidence. They were updated to Version 5 in October 2011 when computer based evidence was replaced with digital evidence reflecting the development of investigating cyber security incidents in a wider context. The guidelines consist of four principles:
Principle 1: No action taken by law enforcement agencies, persons employed within those agencies or their agents should change data which may subsequently be relied upon in court.
Principle 2: In circumstances where a person finds it necessary to access original data, that person must be competent to do so and be able to give evidence explaining the relevance and the implications of their actions.
Principle 3: An audit trail or other record of all processes applied to digital evidence should be created and preserved. An independent third party should be able to examine those processes and achieve the same result.
Principle 4: The person in charge of the investigation has overall responsibility for ensuring that the law and these principles are adhered to.
These guidelines are widely accepted in courts of England and Scotland, but they do not constitute a legal requirement and their use is voluntary. It is arguable that whilst voluntary, non adherence is almost certain to lead to the exclusion of evidence that does not comply subject to the provisions of s.76 Police and Criminal Evidence Act 1984 (Power to exclude evidence obtained unfairly)



Building on the ACPO Guidelines with a more generic application outside of law enforcement, then Murdoch University student Richard Brian Adams proposed, in his dissertation, the following overriding principles to be followed by digital forensic practitioners:
The activities of the digital forensic practitioner should not alter the original data. If the requirements of the work mean that this is not possible then the effect of the practitioner s actions on the original data should be clearly identified and the process that caused any changes justified.
A complete record of all activities associated with the acquisition and handling of the original data and any copies of the original data must be maintained. This includes compliance with the appropriate rules of evidence, such as maintaining a chain of custody record, and verification processes such as hashing.
The digital forensic practitioner must not undertake any activities which are beyond their ability or knowledge.
The digital forensic practitioner must take into consideration all aspects of personal and equipment safety whilst undertaking their work.
At all times the legal rights of anyone affected by your actions should be considered.
The practitioner must be aware of all organisational policies and procedures relating to their activities
Communication must be maintained as appropriate with the client, legal practitioners, supervisors and other team members



Digital evidence is almost never in a format readable by humans, requiring additional steps to include digital documents as evidence (i.e. printing out the material). It has been argued that this change of format may mean digital evidence does not qualify under the "best evidence rule". However, the "Federal Rules of Evidence" rule 1001(3) states "if data are stored in a computer , any printout or other output readable by sight, shown to reflect the data accurately, is an  original. "
Commonly courts do not bar printouts under the best evidence rule. In Aguimatang v. California State Lottery, the court gave near per se treatment to the admissibility of digital evidence stating "the computer printout does not violate the best evidence rule, because a computer printout is considered an  original. " 234 Cal. App. 3d 769, 798.



Electronic discovery






General:
Stephen Mason, general editor, Electronic Evidence (LexisNexis Butterworths, 2012) covering Australia, Canada, England & Wales, European Union, Hong Kong, India, Ireland, New Zealand, Scotland, Singapore, South Africa, United States of America
Stephen Mason, general editor, International Electronic Evidence, (British Institute of International and Comparative Law, 2008).
Australia:
Allison Stanfield Computer forensics, electronic discovery and electronic evidence
Canada:
Daniel M. Scanlan, "Digital Evidence in Criminal Law" (Thomson Reuters Canada Limited, 2011)
United States of America on discovery and evidence:
Michael R Arkfeld, Arkfeld on Electronic Discovery and Evidence (3rd edn, Lexis, 2011) Looseleaf
Adam I. Cohen and David J. Lender, Electronic Discovery: Law and Practice (2nd end, Aspen Publishers, 2011) Looseleaf
Jay E. Grenig, William C. Gleisner, Troy Larson and John L. Carroll, eDiscovery & Digital Evidence (2nd edn, Westlaw, 2011) Looseleaf
Michele C.S. Lange and Kristen M. Nimsger, Electronic Evidence and Discovery: What Every Lawyer Should Know (2nd edn, American Bar Association, 2009)
George L. Paul, Foundations of Digital Evidence (American Bar Association, 2008)
Paul R. Rice, Electronic Evidence - Law and Practice (American Bar Association, 2005)
United States of America on discovery:
Brent E. Kidwell, Matthew M. Neumeier and Brian D. Hansen, Electronic Discovery (Law Journal Press) Looseleaf
Joan E. Feldman, Essentials of Electronic Discovery: Finding and Using Cyber Evidence (Glasser Legalworks, 2003)
Sharon Nelson, Bruce A. Olson and John W. Simek, The Electronic Evidence and Discovery Handbook (American Bar Association, 2006)
Ralph C. Losey, e-Discovery: New Ideas, Case Law, Trends and Practices (Westlaw, 2010)
United States of America on visual evidence:
Gregory P. Joseph, Modern Visual Evidence (Law Journal Press) Looseleaf



Jonathan D. Frieden and Leigh M. Murray, The Admissibility of Electronic Evidence Under the Federal Rules of Evidence, XVII Rich. J.L. & Tech. 5 (2011).



Digital Evidence: Standards and Principles
The Digital Evidence in the Information Era