ADP, LLC., is an American provider of business outsourcing solutions. It was also a provider of computing services to automobile and heavy equipment dealers, but spun off those businesses in 2014. ADP was formerly one of four American companies to get a AAA credit rating from Standard & Poor's (S&P) and Moody's, but S&P and Moody's downgraded ADP to AA in April 2014, after the dealer services unit spin off.



In 1949 Henry Taub founded Automatic Payrolls, Inc. as a manual payroll processing business with his brother Joe Taub. Frank Lautenberg joined the brothers in the company's infancy. In 1952, Lautenberg became Chairman and CEO of the company. In 1957, the company changed its name to Automatic Data Processing, Inc. (ADP), and began using punched card machines, check printing machines, and mainframe computers. ADP went public in 1961 with 300 clients, 125 employees and revenues of approximately US$400,000. The company established a subsidiary in the United Kingdom in 1965 and acquired the pioneering online computer services company Time Sharing Limited (TSL) in 1974. Lautenberg continued in his roles as Chairman and CEO until elected to the United States Senate from New Jersey in 1982.
From 1985 onward, ADP s annual revenues exceeded the $1 billion mark, with paychecks processed for about 20% of the U.S. workforce. In the 1990s, ADP began acting as a professional employer organization (PEO). Around this time, the company acquired Autonom, a German company and the payroll and human resource services company, GSI, headquartered in Paris. Kerridge Computer Co. Ltd., a dealer management systems (DMS) provider to auto dealers in the UK was acquired in 2006.
In September 1998, ADP acquired UK-based Chessington Computer Centre that supplied administration services to the UK Government.
In 2007, the ADP Brokerage Service Group was spun off to form Broadridge Financial Solutions, Inc. (NYSE: BR), removing about US$2 billion from ADP's total yearly revenue. ADP distributed one share of Broadridge common stock for every four shares of ADP common stock held by shareholders of record as of the close of business on March 23, 2007.
ADP is sponsor of the ADP National Employment Report as well as the ADP Small Business Report.




Formerly called ADP Dealer Services, CDK Global was formed in 1972 and provides integrated technology services and solutions to over 26,500 automotive dealerships internationally, as well as vehicle manufacturers. In 2010, ADP acquired the automotive marketing company Cobalt.
BZ Results (Automotive Dealer Services), winner of the 2006  Innovative Company of the Year , was purchased by ADP in 2006. At the time, BZ Results was valued at $125 million.
On April 7, 2014, ADP announced plans to spin off the Dealer Services division as a standalone company. On August 19, 2014, ADP Dealer Services announced that the name of the new company, post-spinoff, would be CDK Global.



In 2012, ADP was recently listed as #47 on Computerworld's 19th annual list of the 100 Best Places to Work in IT.
In 2011, ADP achieved a 100 percent rating in the Corporate Equality Index (CEI) report.
In 2010, ADP was awarded the #2 ranking in the Training Top 125 List by Training magazine. ADP ranked 12th in 2009 and 20th in 2008.
In 2009, the Aberdeen Group named ADP as one of the top 25 Most Influential Technology Vendors for 2009.
In 2010, ADP ranked first on Fortune's list of America s Most Admired Companies within the Financial Data Services category.
In 2006, ADP ranked #2 on InformationWeek 500's top 250 Innovators.
In 2003, 2004, and 2005, ADP was ranked as one of the top 10 companies in North America, having been AAA/Aaa rated by both S&P and Moody's.


