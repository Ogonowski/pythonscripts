TK Solver (originally TK!Solver) is a mathematical modeling and problem solving software system based on a declarative, rule-based language, commercialized by Universal Technical Systems, Inc.



Invented by Milos Konopasek in the late 1970s and initially developed in 1982 by Software Arts, the company behind VisiCalc, TK Solver was acquired by Universal Technical Systems in 1984 after Software Arts fell into financial difficulty and was sold to Lotus Software. Konopasek's goal in inventing the TK Solver concept was to create a problem solving environment in which a given mathematical model built to solve a specific problem could be used to solve related problems (with a redistribution of input and output variables) with minimal or no additional programming required: once a user enters an equation, TK Solver can evaluate that equation as is without isolating unknown variables on one side of the equals sign.



TK Solver's core technologies are a declarative programming language, algebraic equation solver, an iterative equation solver, and a structured, object-based interface. The interface comprises nine classes of objects that can be shared between and merged into other TK files:
Rules: equations, formulas, function calls which may include logical conditions
Variables: a listing of the variables that are used in the rules, along with values (numeric or non-numeric) that have been entered by the user or calculated by the software
Units: all units conversion factors, in a single location, to allow automatic update of values when units are changed
Lists: ranges of numeric and non-numeric values which can be associated with a variable or processed directly by procedure functions
Tables: collections of lists displayed together
Plots: line charts, scatterplots, bar charts, and pie charts
Functions: rule-based, table look-up, and procedural programming components
Formats: settings for displaying numeric and string values
Comments: for explanation and documentation
Each class of object is listed and stored on its own worksheet the Rule Sheet, Variable Sheet, Unit Sheet, etc. Within each worksheet, each object has properties summarized on subsheets or viewed in a property window. The interface uses toolbars and a hierarchal navigation bar that resembles the directory tree seen on the left side of the Windows Explorer.
The declarative programming structure is embodied in the rules, functions and variables that form the core of a mathematical model.



All rules are entered in the Rule Sheet or in user-defined functions. Unlike a spreadsheet or imperative programming environment, the rules can be in any order or sequence and are not expressed as assignment statements. "A + B = C / D" is a valid rule in TK Solver and can be solved for any of its four variables. Rules can be added and removed as needed in the Rule Sheet without regard for their order and incorporated into other models. A TK Solver model can include up to 32,000 rules, and the library that ships with the current version includes utilities for higher mathematics, statistics, engineering and science, finances, and programming.
Variables in a rule are automatically posted to the Variable Sheet when the rule is entered and the rule is displayed in mathematical format in the MathLook View window at the bottom of the screen. Any variable can operate as an input or an output, and the model will be solved for the output variables depending on the choice of inputs.
A database of unit conversion factors also ships with TK Solver, and users can add, delete, or import unit conversions in a way similar to that for rules. Each variable is associated with a "calculation" unit, but variables can also be assigned "display" units and TK automatically converts the values. For example, rules may be based upon meters and kilograms, but units of inches and pounds can be used for input and output.



TK Solver has three ways of solving systems of equations. The "direct solver" solves a system algebraically by the principle of consecutive substitution. When multiple rules contain multiple unknowns, the program can trigger an iterative solver which uses the Newton-Raphson algorithm to successively approximate based on initial guesses for one or more of the output variables. Procedure functions can also be used to solve systems of equations. Libraries of such procedures are included with the program and can be merged into files as needed. A list solver feature allows variables to be associated with ranges of data or probability distributions, solving for multiple values, which is useful for generating tables and plots and for running Monte Carlo simulations. The premium version now also includes a "Solution Optimizer" for direct setting of bounds and constraints in solving models for minimum, maximum, or specific conditions.
TK Solver includes roughly 150 built-in functions: mathematical, trigonometric, Boolean, numerical calculus, database access, and programming functions, including string handling and calls to externally compiled routines. Users may also define three types of functions: declarative rule functions; list functions, for table lookups and other operations involving pairs of lists; and procedure functions, for loops and other procedural operations which may also process or result in arrays (lists of lists). The complete NIST database of thermodynamic and transport properties is included, with built-in functions for accessing it. TK Solver is also the platform for engineering applications marketed by UTS, including Advanced Spring Design, Integrated Gear Software, Interactive Roark s Formulas, Heat Transfer on TK, and Dynamics and Vibration Analysis.



Tables, plots, comments, and the MathLook notation display tool can be used to enrich TK Solver models. Models can be linked to other components with Microsoft Visual Basic and .NET tools, or they can be web-enabled using the RuleMaster product or linked with Excel spreadsheets using the Excel Toolkit product. There is also a DesignLink option linking TK Solver models with CAD drawings and solid models. In the premium version, standalone models can be shared with others who do not have a TK license, opening them in Excel or the free TK Player.



Optimization (mathematics)
Multidisciplinary design optimization



TK Solver product listing on the UTS website
Short introduction to using TK Solver
Tips for new users of TK Solver
CadDigest reviews of various versions of TK Solver
Computers in Higher Education Economics Review discusses version 4
Design News review of version 3.0
IEEE Spectrum review of version 3.0
Review of TK Solver 5.0