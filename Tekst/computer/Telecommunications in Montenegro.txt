Telecommunications in Montenegro includes radio, television, fixed and mobile telephones, and the Internet.




Radio stations:
14 local public radio stations and more than 40 private radio stations, the state-funded national radio-TV broadcaster operates 2 radio networks (2007);
31 stations (2004).

Television stations:
4 public and some 20 private TV stations and 1 satellite TV channel, the state-funded national radio-TV broadcaster operates 2 terrestrial TV networks (2007);
13 stations (2004).

Radio Television of Montenegro (RTCG) is the state-owned public broadcaster with nationwide coverage. Other privately owned television broadcast stations mostly cover the major cities in Montenegro.
Government opponents claim that, despite some improvement, RTCG is still controlled by the ruling political structures and that the public broadcaster clearly favors the government in its programming and reporting.




Calling code: +382
Main lines: 163,000 lines in use, 131st in the world (2012).Fixed line services are provided by T-Com Montenegro (owned by Crnogorski Telekom), MTEL (owned by Telekom Srbija).

Mobile cellular: 1.1 million lines, 154th in the world; 178 for every 100 people, 9th in the world (2012).Mobile cellular services are provided by three GSM operators, Telenor Montenegro (owned by Telenor), T-Mobile Montenegro (owned by Crnogorski Telekom) and m:tel (owned by Telekom Srbija). All providers have national coverage, and provide advanced services. 3G services were offered by all three operators starting in the summer of 2007.

Telephone system: modern telecommunications system with access to European satellites; GSM mobile-cellular service, available through multiple providers with national coverage, is growing; 2 international switches connect with the national system (2011).
At 178% Montenegro had the second highest mobile cellular phone penetration rate in Europe, behind only Russia, and ranked 9th world-wide.



Top-level domain: .me, the top level domain for Montenegro, began its "national sunrise" starting phase in May 2008; next were the "general sunrise" and "land rush" periods; and starting in July 2008, applications were processed on a "first come, first served" basis.
Internet users:
373,655 users, 134th in the world; 56.8% of the population, 70th in the world (2012).
280,000 users, 133rd in the world (2009).

Fixed broadband: 54,439 subscriptions, 112th in the world; 8.3% of the population, 90th in the world (2012).
Wireless broadband: 177,437 subscriptions, 112th in the world; 27.0% of the population, 59th in the world (2012).
IPv4: 171,520 addresses allocated, less than 0.05% of the world total, 260.9 addresses per 1000 people (2012).
Internet hosts: 10,088 hosts 135th in the world (2012).
Internet services are provided by Crnogorski Telekom and MTEL. Crnogorski Telekom provides dial-up and ADSL access, while MTEL provides WiMAX access.
In October 2010, there were 2,347 dial-up connections and 63,155 broadband connections.
ADSL became available in Montenegro in 2005. So far, the sole provider of ADSL services in Montenegro is Crnogorski Telekom. There were 55,443 ADSL connections in Montenegro in October 2010, which makes ADSL the most popular Internet access technology in the country. Speeds up to 7 Mbit/s downstream are available. Recently, the company started to connect end users with fiber optics, with speeds up to 40 Mbit/s downstream. However, currently their "fiber to the home" offer is only available in half of the Podgorica (the capital city), and in a few small areas on the coast.
Another broadband Internet provider is M-Kabl, who uses DOCSIS technology. Speeds up to 16 Mbit/s downstream are available with an 18-month contract. However they only operate in major cities.
WiMAX access is provided by MTEL, and also by WiMax Montenegro. There were 7,381 WiMAX connections in Montenegro in October 2010. Speeds of up to 4 Mbit/s down and 1 Mbit/s up are available.



There are no government restrictions on access to the Internet. Until ordered to cease doing so in March 2011, one of the country s principal Internet service providers gave police direct access to all forms of communications carried on its servers. It is unknown whether authorities made use of this access to monitor e-mail or Internet Web sites or chat rooms. There is no evidence that the government collects or discloses personally identifiable information about individuals based on the individual's peaceful expression of political, religious, or ideological opinion or belief.
The constitution and law provide for freedom of speech and press, but there are some restrictions. The law criminalizes inciting hatred and intolerance on national, racial, and religious grounds, and there have been prosecutions on these grounds. Individuals can criticize the government publicly or privately without reprisal. Following the repeal of the criminal libel law in 2011, parliament enacted a law on amnesty to pardon persons convicted of defamation and insult.
In March 2012, representatives of 19 print and electronic media outlets formed a media council for self-regulation. However, some of the most influential media declined to join what they described as an excessively progovernment group. They indicated that they would form a separate self-regulatory mechanism. A group of small local media outlets from the northern region of the country established their own self-regulation council.
The constitution and law prohibit arbitrary interference with privacy, family, home, or correspondence without court approval or legal necessity, and prohibit police from searching a residence or conducting undercover or monitoring operations without a warrant. The government generally respects the prohibitions relating to physical and property searches, but has been less compliant regarding digital privacy.
The law requires the Agency for National Security (ANB) to obtain court authorization for wiretaps, but authorities reportedly use wiretapping and surveillance inappropriately against opposition parties, the international community, NGOs, and other groups without appropriate legal authority. The NGO Alternativa stated that during 2011, the ANB performed secret surveillance and data collection against 113 persons. NGOs claimed that police and the state prosecutor s office illegally monitor citizens' electronic communications and fail to account for how many people or Internet addresses they monitor.



 This article incorporates public domain material from the CIA World Factbook document "2014 edition".
 This article incorporates public domain material from websites or documents of the United States Department of State.



domain.me, .me domain registry.
Montenegro Agency for Electronic Communications and Postal Services