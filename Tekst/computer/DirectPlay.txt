DirectPlay is a deprecated API that was part of Microsoft's DirectX API. DirectPlay is a network communication library intended for computer game development, although its general nature certainly allows it to be used for other purposes.
DirectPlay is a high-level software interface between applications and communication services that makes it easy to connect games over the Internet, a modem link, or a network. DirectPlay features a set of tools that allow players to find game sessions and sites to manage the flow of information between hosts and players. It provides a way for applications to communicate with each other, regardless of the underlying online service or protocol. DirectPlay also resolves many connectivity issues, such as NAT.
DirectPlay, like the rest of DirectX, runs in COM and it is accessed through COM (Component Object Model) interfaces. By default, DirectPlay uses multi-threaded programming techniques and requires careful thought to avoid the usual threading issues. Since DirectX version 9, this issue can be alleviated at the expense of efficiency.



Under the hood, DirectPlay is built on the User Datagram Protocol (UDP) to allow it speedy communication with other DirectPlay applications.
DirectPlay sits on layer 4 and 5 of the OSI model.
On layer 4, DirectPlay can handle the following tasks if requested by the application:
Message ordering, which ensures that data arrives in the same order it was sent.
Message reliability, which ensures that data is guaranteed to arrive.
Message flow control, which ensures that data is only sent at the rate the receiver can receive it.

On layer 5, DirectPlay always handles the following tasks:
Connection initiation and termination.



The primary interfaces (methods of access) for DirectPlay are:
IDirectPlay8Server, which allows access to server functionality
IDirectPlay8Client, which allows access to client functionality
IDirectPlay8Peer, which allows access to peer-to-peer functionality

Secondary interfaces are:
Various lobby interfaces that allows players to find and prepare a game before it actually commences.
Thread management interfaces that allows the programmer to fine-tune resource usage. This also allows for a general disabling of worker threads so that tricky multi-threaded issues are avoided at the cost of decreased responsiveness.
Various network address translation (NAT) interfaces that make it easier for players behind routers to host games. Most players on broadband internet connections face this problem.
Various voice communication interfaces known as DirectPlay Voice that make it easier to support audio communication and voice input in games.
Various queue monitoring interfaces that allow the application to get feedback on how much data has been sent and received as requested. This allows an application to decrease/increase the amount of data sent in accordance with the capabilities of the receiver.



DirectPlay Voice was introduced in Windows Me as part of DirectX 7.1 for multiplayer games. It is a voice communications, recording and playback API that allows gamers to use voice chat in games written to take advantage of the API, through a DirectPlay network transport session itself.



DirectPlay was traditionally one of the components of DirectX that received less attention, but for DirectX version 8 it received a major overhaul and became a relatively lightweight networking library. However, as part of Microsoft's unveiling of XNA in 2004, Microsoft revealed that DirectPlay would be deprecated in favor of Games for Windows - Live technology already available on Xbox and being ported for use on Windows PCs. DirectPlay will be supported in DirectX DLLs for the lifetime of Microsoft Windows XP, but from the autumn of 2007  the headers and libraries   vital components if developers wanted to develop new programs that utilize the technology   were absent from the DirectX SDK.
In Windows Vista, DirectPlay has been deprecated and DirectPlay Voice and DirectPlay's NAT Helper have been removed.



DirectX






Microsoft DirectX 9 The last SDK to ship with DirectPlay headers and libraries.