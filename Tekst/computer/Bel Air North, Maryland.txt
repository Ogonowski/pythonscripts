Bel Air North is a census-designated place (CDP) in Harford County, Maryland, in the United States. It is immediately north of (and excludes) the incorporated town of Bel Air, Maryland. As of the 2000 census, the CDP population was 25,798.



Bel Air North is located at 39 33 2 N 76 21 42 W (39.550439, -76.361781).
According to the United States Census Bureau, the CDP has a total area of 16.4 square miles (42 km2), all of it land.



As of the census of 2000, there were 25,798 people, 8,716 households, and 7,190 families residing in the CDP. The population density was 1,577.0 people per square mile (608.8/km ). There were 8,978 housing units at an average density of 548.8/sq mi (211.9/km ). The racial makeup of the CDP was 94.96% White, 2.22% African American, 0.12% Native American, 1.61% Asian, 0.02% Pacific Islander, 0.27% from other races, and 0.79% from two or more races. Hispanic or Latino of any race were 1.10% of the population.
There were 8,716 households out of which 48.3% had children under the age of 18 living with them, 72.0% were married couples living together, 8.2% had a female householder with no husband present, and 17.5% were non-families. 14.6% of all households were made up of individuals and 5.3% had someone living alone who was 65 years of age or older. The average household size was 2.94 and the average family size was 3.28.
In the CDP the population was spread out with 31.4% under the age of 18, 5.9% from 18 to 24, 32.3% from 25 to 44, 22.3% from 45 to 64, and 8.1% who were 65 years of age or older. The median age was 35 years. For every 100 females there were 94.4 males. For every 100 females age 18 and over, there were 90.5 males.
The median income for a household in the CDP was $86,881, and the median income for a family was $94,119. Males had a median income of $54,381 versus $32,521 for females. The per capita income for the CDP was $26,726. About 1.2% of families and 1.7% of the population were below the poverty line, including 1.6% of those under age 18 and 3.4% of those age 65 or over.



^ "US Gazetteer files: 2010, 2000, and 1990". United States Census Bureau. 2011-02-12. Retrieved 2011-04-23. 
^ "CENSUS OF POPULATION AND HOUSING (1790-2000)". U.S. Census Bureau. Archived from the original on 8 July 2010. Retrieved 2010-07-17. 
^ "American FactFinder". United States Census Bureau. Retrieved 2008-01-31. 
^ http://factfinder.census.gov/servlet/ACSSAFFFacts?_event=Search&geo_id=&_geoContext=&_street=&_county=bel+air+north&_cityTown=bel+air+north&_state=04000US24&_zip=&_lang=en&_sse=on&pctxt=fph&pgsl=010