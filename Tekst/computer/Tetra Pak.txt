Tetra Pak is a multinational food packaging and processing company of Swedish origin with head offices in Lund, Sweden, and Lausanne, Switzerland. The company offers packaging solutions, filling machines and processing solutions for dairy, beverages, cheese, ice-cream and prepared food, including distribution tools like accumulators, cap applicators, conveyors, crate packers, film wrappers, line controllers and straw applicators.
Tetra Pak was founded by Ruben Rausing and built on Erik Wallenberg's innovation, a tetrahedron-shaped plastic-coated paper carton, from which the company name was derived. In the 1960s and 1970s, the development of the Tetra Brik package and the aseptic packaging technology made possible a cold chain supply, substantially facilitating distribution and storage. From the beginning of the 1950s to the mid-1990s, the company was headed by the two sons of Ruben Rausing, Hans and Gad, who took the company from a family business of six employees, in 1954, to a multinational corporation. Tetra Pak is currently the largest food packaging company in the world by sales, operating in more than 170 countries and with over 23,000 employees (2012). The company is privately owned by the family of Gad Rausing through the Swiss-based holding company Tetra Laval, which also includes the dairy farming equipment producer DeLaval and the PET bottle manufacturer Sidel. In November 2011, the Tetra Brik carton package was represented at the exhibition Hidden Heroes   The Genius of Everyday Things at the London Science Museum/Vitra Design Museum, celebrating "the miniature marvels we couldn t live without". The aseptic packaging technology has been called the most important food packaging innovation of the 20th Century by the Institute of Food Technologists and the Royal Swedish Academy of Engineering Sciences called the Tetra Pak packaging system one of Sweden s most successful inventions of all time.




Tetra Pak was created in 1951 as a subsidiary to  kerlund & Rausing, a food carton company established in Malm  in 1929 by Ruben Rausing and Erik  kerlund. Rausing, who had studied in New York at the beginning of the 1920s, had seen self-service grocery stores in the United States, which was unheard of in Europe at the time, and realised that pre-packaging was part of the future in food retailing as a more hygienic and practical way of distributing staple groceries. At the time, these were sold over the counter in cumbersome glass bottles or impractical paper wraps in most European countries. At the end of the 1920s, Rausing bought a run-down packaging factory in Malm  together with the industrialist Erik  kerlund.  kerlund & Rausing was the first packaging company in Scandinavia and eventually became a leading manufacturer of dry food cartons. Initially, however,  kerlund & Rausing had difficulties making profits and in 1933  kerlund sold his share to Rausing, who became the sole owner. Fonterra Sri Lanka started tetra pac for their newly launched milk base product



 kerlund & Rausing produced all sorts of paper packaging for dry staple groceries, but Rausing was determined to find a way to pre-package liquids like milk and cream, and spent large sums on developing a viable package. The goal was to provide optimal food safety, hygiene and distribution efficiency using a minimum amount of material, according to the subsequently famous credo that a package should "save more than it costs". The new package had to be sufficiently cheap to be able to compete with loose milk, meaning that a minimum of material waste and a maximum of production efficiency needed to be obtained.
In 1943, the  kerlund & Rausing lab started to work on developing the milk carton, and, in 1944, came up with the idea of constructing a tetrahedron-shaped package out of a tube of paper. The idea was simple but efficient, making optimal use of the material involved. After some initial hesitation, Rausing understood the potential of the package and filed for a patent on 27 March 1944. The rest of the 1940s were spent developing viable packaging materials and solving the technical problems of filling, sealing, and distribution.

It was reportedly Rausing s wife Elisabeth who came up with the idea of continuously sealing the packages through the milk while filling the tube in the manner of stuffing sausages, to prevent oxygen from entering the package. In 1946, a model for a packaging machine was presented by engineer Harry J rund, and in collaboration with Swedish paper mills and foreign chemical companies a viable paper for packaging was finally produced when coating carton paper with polyethylene, which made the paper waterproof and allowing for heat-sealing during filling.




AB Tetra Pak was established in Lund, Sweden, in 1951. In May of that year, the new packaging system was presented to the press, and in 1952, the first filling machine producing 100 ml cream tetrahedrons was delivered to Lundaortens Mejerif rening, a local dairy. In the subsequent years, the tetrahedron packages became more and more frequent in Swedish grocery stores, and in 1954, the first machine producing 500 ml milk packages was sold to a Stockholm dairy. That same year the first machine was exported to Hamburg, Germany, soon to be followed by France (1954), Italy (1956), Switzerland (1957) and later the Soviet Union (1959) and Japan (1962). Rausing relentlessly strove to improve the Tetra Classic system, wrought with many technical problems during the 1950s, and spent enormous amounts on development. The different projects   the tetrahedron, the aseptic packaging technology, Tetra Brik   all demanded extremely large resources and the company had financial troubles well into the 1960s. Indeed, Tetra Pak's commercial breakthrough did not arrive until the mid-1960s with the new Tetra Brik package, introduced in 1963, and the development of the Aseptic technology. To liberate capital,  kerlund & Rausing was sold in 1965 while AB Tetra Pak was retained.
International expansion had begun by the beginning of the 1960s, when the first production plant outside of Sweden was established in Mexico in 1960, soon to be followed by another in the United States in 1962. In 1962, the first Tetra Classic Aseptic machine outside of Europe was installed in Lebanon. The late 1960s and 1970s saw a global expansion of the company, much due to the new Tetra Brik Aseptic package, launched in 1969, which opened up new markets in the developing world and sparked off a virtual explosion in sales.



In 1981, Tetra Pak relocated its corporate headquarters to Lausanne, Switzerland, for tax reasons, however retaining all R&D functions in Lund, Sweden. For the equivalent of $2.5 billion, Tetra Pak acquired Alfa-Laval AB in 1991, a venerable Swedish company producing industrial and agricultural equipment and milk separators, world-leading in its industry, in what was at the time Sweden's largest takeover. Since the deal allowed Tetra Pak to integrate the Alfa Laval competence within processing, the merger eventually made it possible for Tetra Pak to offer entire packaging and processing solutions to its clients. The deal drew anti-competitive scrutiny from the European Commission, but it went through after various concessions from both companies. After the merger with Alfa Laval, Tetra Pak announced plans to return the headquarters to Sweden, and in 1993 Group Tetra Laval was created with joint headquarters in Lund and Lausanne. Alfa Laval's liquid processing unit was absorbed into Tetra Pak and the unit specialising on machinery for dairy production was organised separately in Alfa Laval Agri. Alfa Laval Agri was later renamed DeLaval, after Alfa Laval's founder Gustaf de Laval, and is still a part of the Tetra Laval group. The part of Alfa Laval that was not directly linked to Tetra Pak's activities   heat exchangers and separation equipment among others   was sold in 2000 to Swedish finance group Industri Kapital. In 2001, Tetra Laval acquired the French plastic packaging group Sidel. The merger was prohibited by the European Commission on the grounds that both Tetra Pak and Sidel were market leading in their field and operated in related business areas. The European Court of Justice eventually ruled in favour for Tetra Laval in a high-profile case. The Tetra Laval Group is controlled by the holding company Tetra Laval International, whose board of directors include the three children of Gad Rausing.







Tetra Pak operates globally through 40 market companies, which are subsidiaries to Tetra Pak International SA, doing business in over 170 countries. Because of the low relative cost of its end products, the developing world has been an important market for Tetra Pak from the start. In 2010, Tetra Pak reported a 5.2% increase in sales, with an annual turnover of almost  10 billion. In its 2010/2011 annual report, Tetra Pak announced particularly strong growth in China, Southeast Asia, Eastern Europe and Central and South America. Rising income levels in these markets enabled higher consumption of protein-rich foodstuffs such as dairy products, and Tetra Pak has announced that it will increase investment in the emerging markets with 10% to over  200 million (2009). After investing close to  200 million in new packaging plants in Russia (2007) and China (2008), in 2011 Tetra Pak announced the construction of new packaging plants in India and Pakistan to meet increasing demand. The new plants on the Indian sub-continent are thought to supply the growing demand in the Middle East. The Financial Times reported that the rise in milk consumption in emerging markets particularly regarded UHT milk, facilitating transportation and food safety, something which is favourable for Tetra Pak whose aseptic packages represent two-thirds of its sales. Tetra Pak's most popular product is the Tetra Brik Aseptic, a best-seller since the 1970s. In May 2011 Tetra Pak launched the first aseptic carton bottle for milk, the Tetra Evero Aseptic.



In an interview in Swedish business monthly Aff rsv rlden in 2006, Tetra Pak CEO Dennis J nsson defined Tetra Pak's current main competitor to be Swiss manufacturer SIG Combibloc, however adding that Tetra Pak's main competition generally no longer comes from companies producing similar packaging but from industries and companies producing other types of packaging with a lower cost of production, like the PET bottle. Indeed, J nsson perceived the PET bottle as Tetra Pak's biggest threat in the European market at the time. The Norwegian company Elopak/Pure-Pak produces similar style carton packages and has historically been Tetra Pak's principal competitor. The Chinese packaging company Greatview has begun challenging Tetra Pak, both in the Chinese market as well as in Europe.







The aseptic packaging technology is Tetra Pak's key innovation and paved the way for Tetra Pak's success. In aseptic processing the product and the package are sterilized separately and then combined and sealed in a sterile atmosphere, in contrast to canning, where product and package are first combined and then sterilized. When filled with ultra-heat treated (UHT) foodstuffs (liquids like milk and juice or processed food like vegetables and preserved fruits), the aseptic packages can be preserved without being chilled for up to one year, with the result that distribution and storage costs, as well as environmental impact, is greatly reduced and product shelf life expanded.



Tetra Classic is the name of the first, tetrahedral package, launched by Tetra Pak in 1952, with an aseptic version released in 1961 and still in use, mainly for portion-sized cream packages and children's juices.
The Tetra Brik, a package in the shape of a rectangular cuboid, was launched in 1963 after a long and costly development process. An aseptic version, Tetra Brik Aseptic was launched in 1969. In terms of entities sold, it is the most popular of the Tetra Pak packages.
The pillow-shaped Tetra Fino Aseptic was introduced in 1997, aiming to provide low cost and simplicity.
Tetra Gemina Aseptic was introduced in 2007 as the "world's first roll-fed gable top package with full aseptic performance".
The Tetra Prisma Aseptic was launched in 1996. It has an octagonal shape with the aim of providing a more ergonomic experience.
The Tetra Rex is a cuboid shaped package with a gable-top. It was launched in Sweden in 1966.
Tetra Recart is a newly launched package shaped as a rectangular cuboid that is meant to provide an alternative to previously canned foodstuffs such as vegetables, fruit and pet food.
Tetra Top was launched in 1986 as a re-closable, rounded cuboid package with a plastic upper part, including opening and closure elements. The lid, moulded in polyethylene in a single mold, makes it easy to open and reclose.
Tetra Wedge Aseptic was developed to keep packaging material to a minimum while retaining a square surface underneath. It was introduced in 1997.
The Tetra Evero Aseptic is the latest of the Tetra Pak packages, launched in 2011 and marketed as the world's first aseptic carton bottle for ambient milk.



While the original idea was to provide hygienic pre-packaging for liquid food stuffs, Tetra Pak is now providing a range of different packaging and processing solutions due to its acquisition of Alfa Laval in 1991, consequently supplying complete systems of processing, packaging and distribution within fields as various as ice cream, cheese, fruit and vegetables and pet food. In addition to its various packaging solutions, Tetra Pak thus provides integrated processing and distribution lines for different kinds of food manufacturing, including packaging machines and carton paper, equally providing distribution equipment like conveyors, tray packers, film wrappers, crates, straws and roll containers. The company additionally offers automated production solutions and technical service.






Tetra Pak has a very pronounced sustainability profile and claims to continuously strive to improve best practice for itself and for its customers. Ruben Rausing's in many ways preclusive mantra from the 1940s that a package should save more money than it costs has over the years resulted in a highly sustainable product that makes efficient use of all resources involved. The use of materials and waste management are indeed, says Financial Times, "big issues" for the company. In 2011, Tetra Pak published a new set of sustainability targets, which included maintaining the CO2 emission levels on the same level until 2020 and increasing recycling by 100% in the same period. Previous Tetra Pak sustainability targets (2005 2010) were met and exceeded with exceptional results reported the WWF, with whom Tetra Pak has a long time collaboration, affirming that Tetra Pak's energy use was lower in 2006 than in 2002, despite a production growth of 23%. Maintaining current CO2 emission levels until 2020 would result in a total of 40% relative cut in emissions with an average growth rate of 5% per year, according to Food Production Daily. Tetra Pak said it will increase its use of Forest Stewardship Council (FSC) certified paper to 100% in 2020, with an interim target of 50% by 2012. The new targets will encompass the whole value chain, from suppliers to customers, putting pressure on partners to perform coherently.

The company reported that it secures raw material for the paper carton in cooperation with the World Wide Fund for Nature (WWF), the Global Forest & Trade Network (GFTN) and Forest Stewardship Council (FSC) and that it strives to source polyethylene made from sugarcane from sustainable suppliers in Brazil. In 2010, 40% of Tetra Pak's carton supply was FSC-certified. Slowly, sectors where glass bottles have been paramount, like the wine and spirits industry, have begun to look at carton bottles as a possible packaging solution as the carbon footprint of a carton container is said to be about one tenth of that of an equivalent glass bottle.



Since the aseptic packages contain different layers of plastic and aluminium in addition to raw paper, they cannot be recycled as "normal" paper waste, but need to go to special recycling units for separation of the different materials. Tetra Pak has operated limited recycling since the mid-1980s, introducing a recycling program for its cartons in Canada as early as 1990. In 2000, Tetra Pak invested  20 million ( 500,000) in the first recycling plant for aseptic packages in Thailand. Recycling of the aseptic packages has indeed been one of Tetra Pak's big challenges. Once separated, the aseptic carton results in aluminium and pure paraffin, which can be used in industry. Even without separating the carton materials, however, the aseptic carton can be reused, a Tetra Pak spokesman said, for example in engineering equipment. In 2010, 30 billion used Tetra Pak carton packages were recycled, a doubling since 2002. The company stated that it aims to help double the recycling rate within the next ten years, something that will require an engagement within the whole recycling chain. As of 2011, 20% of Tetra Pak cartons are recycled globally, with countries like Belgium, Germany, Spain and Norway showing local recycling rates of over 50%. To increase the level of recycling and meet the targets, Tetra Pak has engaged in driving recycling activities such as facilitating the development of collection schemes, launch new recycling technologies and raise the awareness about recycling and sustainability. Used Tetra Pak packages have been used as construction material in different design projects, with varying results.



Tetra Pak's sustainability work has gained recognition from industry and retailers alike, and in 2010 it received the Swedish Forest Industries Climate Award with the motivation that the company takes global responsibility for the forests which provide its raw material. The recently introduced Tetra Recart has also been hailed by large retail groups like Sainsbury's as "the 21st Century alternative to canned foods" as the cartons' rectangular shape makes transportation, storage and distribution more efficient, taking up 21% less space and weighing two-thirds of a tin can of equivalent volume.
Tetra Pak cartons have been criticized for being more difficult to recycle than tin cans and glass bottles. The difficulty lies in the fact that the process demands specific recycling plants that are not easily accessible and that if not recycled, they end up in landfills. Tetra Pak has stated that it is currently working on joint ventures with local governments around the world to increase the number of recycling facilities.






Tetra Pak was early in engaging in community projects and the company has supported School Milk and School Feeding programmes for 45 years. In the late 1970s, Ruben Rausing engaged himself personally in Operation Flood, a joint venture between the World Food Programme, the World Bank and Tetra Pak to supply western milk surplus to Indian households.

The Food for Development programme (FfD) was initiated to improve nutrition and health and alleviate poverty globally. The FfD programmes is mainly focusing on school nutrition and school milk for children, but also on projects improving agricultural practices and dairy handling, providing training for farmers to enhance efficiency, productivity and food safety. This is also beneficial for Tetra Pak as it builds relations and secures supply on less developed markets. Tetra Pak works with local governments and NGOs to secure and develop the programmes.



The school milk programmes are part of the Food for Development projects and aim at providing milk to school children to help improve nutrition. Tetra Pak supplies the cartons for the school milk at cost and does not make any profit on sales. UNDP and World Bank case studies of Tetra Pak school milk programmes in Nigeria showed that vitamin deficiency, energy, growth and cognitive skills were improved and that children were more interested in their school work after taking part in the programme.



Tetra Pak has supported disaster relief e.g., after the Haiti earthquake, Pakistan floods, and Russian wildfires in 2010, and Japan's T hoku earthquake and tsunami and Thailand floods in 2011, and during the 2010 Pakistan floods and Thailand in 2011. In China, Tetra Pak helped improve food safety, sustainability and best practices in the dairy industry after the 2008 contamination scandal that, although Tetra Pak had nothing to do with the scandal, seriously damaged the market for packaged milk in China. As the Financial Times stated, it was not solely a philanthropic act but a way of securing the future for the market, helping the industry become safer, more sustainable and more efficient. The training programme was reported to be very successful with substantial elevation of standards in dairy handling and farming.






Tetra Pak has occasionally been subject to controversy, most notably regarding its near-monopoly position on certain markets for many years. Especially attempts at mergers have been subject to scrutiny. Its merger with French PET-production company Sidel in 2001 drew anti-competition allegations from the European Commission. The court case was drawn out for many years and twice appealed to the European Court of First Instance before the European Court of Justice ruled in favour of Tetra Laval. In 2004, Tetra Pak was accused of using its near-monopoly in China, where it held 95% on the market for aseptic carton packaging. The allegations were contested by Tetra Pak.



In January 2004, Italian dairy giant Parmalat was caught in a multibillion-euro accounting scandal, culminating in the arrest of the owner Calisto Tanzi. Parmalat CFO Fausto Tonna told the Italian business daily Il Sole 24 Ore that Tetra Pak had made substantial payments to Tanzi and his family and to a company in the Cayman Islands belonging to Parmalat. Tetra Pak acknowledged having made payments to Parmalat but stated that the payments had been made as discounts to subsidize marketing operations and pricing, as is usual practice with large customers. Tetra Pak was asked by Italian authorities to provide documentation on the transactions, and found that payments had been made since 1995 as part of regular operations but that no payments had been made specifically to the Tanzi family. Calisto Tanzi was eventually sentenced to 8 years imprisonment for fraud by the Italian high court in Milan after several appeals in lower instances.



Upon visiting the Tetra Pak factory in Lund in the 1950s, Danish physics professor and Nobel Prize laureate Niels Bohr allegedly claimed to "never have seen such an adequate practical application of a mathematical problem" as the tetrahedron package and the innovation of the milk tetrahedron, the basis for Tetra Pak, has indeed given rise to differences in view as to who is to be credited for the invention. Erik Wallenberg did not get any formal recognition until 1991 when he was awarded the Royal Swedish Academy of Engineering Sciences Great Gold Medal for outstanding achievement for the invention.






Tetra Pak Homepage
Tetra Pak sustainability
Tetra Laval Homepage
DeLaval Homepage
Sidel Homepage
Alfa Laval Homepage
Tetra Pak historic films on YouTube
How Tetra Pak containers are made (video)