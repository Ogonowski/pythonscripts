The E86 cluster bomb was an American biological cluster bomb first developed in 1951. Though the U.S. military intended to procure 6,000 E86s, the program was halted in the first half of the 1950s.



The E86 cluster bomb was developed as a biological weapon by the United States Army Chemical Corps and the United States Air Force beginning in October 1951. The Ralph M. Parsons Company was contracted to produce the E86 in October 1952. In 1953 procurement began for 6,000 E86 cluster bombs, with their production expected no earlier than 1958. When U.S. military munition requirements were reviewed in the first half of the 1950s, production and further development of the E86 was halted. The E86 cluster bomb supplanted technologies such as the E77 balloon bomb.



The E86 was similar to the M115 biological bomb, except it was larger. While the M115 weighed 500 pounds (227 kg), the E86 was a 750-pound (340 kg) weapon. Regardless, operationally, the E86 was similar to the M115. It was designed as an anti-crop weapon; the U.S. biological weapons program produced three anti crop agents, wheat and rye stem rust and rice blast. The weapon was in a steel case and intended to be dropped from the exterior of an aircraft such as the B-47 or B-52. Sub-munitions included the E14 munition; the sub-munition was originally intended as anti-crop weapons as well, but was later altered and used in testing as the U.S. pursued an entomological warfare program.



M33 cluster bomb


