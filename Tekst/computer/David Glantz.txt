David M. Glantz (born January 11, 1942 in Port Chester, New York) is an American military historian and the chief editor of the Journal of Slavic Military Studies.
Glantz received degrees in history from the Virginia Military Institute and the University of North Carolina at Chapel Hill, and is a graduate of the U.S. Army Command and General Staff College, Defense Language Institute, Institute for Russian and Eastern European Studies, and U.S. Army War College. He entered active service with the United States Army in 1963.



He began his military career in 1963 as a field artillery officer from 1965 to 1969. He served in various assignments in the United States, and in Vietnam during the Vietnam War with the II Field Force Fire Support Coordination Element (FSCE) at the "Plantation" in Long Binh.



After teaching history at the United States Military Academy from 1969 through 1973, he completed the army s Soviet foreign area specialist program and became chief of Estimates in US Army Europe s Office of the Deputy Chief of Staff for Intelligence (USAREUR ODCSI) from 1977 to 1979. Upon his return to the United States in 1979, he became chief of research at the Army s newly formed Combat Studies Institute (CSI) at Fort Leavenworth, Kansas, from 1979 to 1983 and then Director of Soviet Army Operations at the Center for Land Warfare, U.S. Army War College in Carlisle, Pennsylvania, from 1983 to 1986. While at the College, Col. Glantz was instrumental in conducting the annual "Art of War" symposia which produced the best analysis of the conduct of operations on the Eastern Front during the Second World War in English to date. The symposia included attendance of a number of former German participants in the operations, and resulted in publication of the seminal transcripts of proceedings. Returning to Fort Leavenworth in 1986, he helped found and later directed the U.S. Army s Soviet (later Foreign) Military Studies Office (FMSO), where he remained until his retirement in 1993 with the rank of Colonel.
Col. David M. Glantz, was a Mark W. Clark visiting professor of History at The Citadel, The Military College of South Carolina.



In 1993, while at FMSO, he established The Journal of Slavic Military Studies, a scholarly journal for which he still serves as chief editor, that covers military affairs in the states of Central and Eastern Europe as well as the former Soviet Union.
A member of the Russian Academy of Natural Sciences, he has written or co-authored more than twenty commercially published books, over sixty self-published studies and atlases, and over one hundred articles dealing with the history of the Red (Soviet) Army, Soviet military strategy, operational art, and tactics, Soviet airborne operations, intelligence, and deception, and other topics related to World War II. In recognition of his work, he has received several awards, including the Society for Military History's prestigious Samuel Eliot Morison Prize for his contributions to the study of military history.
Glantz is known as a military historian of the Soviet role in World War II. He is perhaps most associated with the thesis that World War II Soviet military history has been prejudiced in the West by its over-reliance on German oral and printed sources, without being balanced by a similar examination of Soviet source material; a more complete version of this thesis can be found in his paper "The Failures of Historiography: Forgotten Battles of the German-Soviet War (1941-1945)." Glantz has also, however, met with some criticism for his stylistic choices, such as inventing specific thoughts and feelings of historical figures without reference to documented sources.
Glantz is also known as an opponent of Viktor Suvorov's thesis, which he endeavored to rebut with the book Stumbling Colossus.
He lives with his wife Mary Ann Glantz in Carlisle, Pennsylvania. The Glantzes' daughter Mary E. Glantz, also a historian, has written FDR And The Soviet Union: The President's Battles Over Foreign Policy (ISBN 070061365X).



Air University Review, March-April 1983, Soviet Offensive Ground Doctrine Since 1945
1984 Art of War symposium, From the Don to the Dnepr: Soviet Offensive Operations - December 1942 - August 1943, A transcript of Proceedings, Center for Land Warfare, US Army War College, 26 30 March 1984
1985 Art of War symposium, From the Dnepr to the Vistula: Soviet Offensive Operations - November 1943 - August 1944, A transcript of Proceedings, Center for Land Warfare, US Army War College, 29 3 May 1985
1986 Art of War symposium, From the Vistula to the Oder: Soviet Offensive Operations - October 1944 - March 1945, A transcript of Proceedings, Center for Land Warfare, US Army War College, 19 23 May 1986
August Storm: The Soviet strategic offensive in Manchuria by David M.Glantz (pdf)
August Storm: Soviet Tactical and Operational Combat in Manchuria, 1945 by LTC David M. Glantz (pdf)
The Soviet Airborne Experience by LTC David M. Glantz
Soviet Defensive Tactics at Kursk, July 1943 by COL David M. Glantz



Soviet Military Deception in the Second World War. London: Frank Cass. 1989. ISBN 0-7146-3347-X. 
The role of intelligence in Soviet military strategy in World War II. Novato, CA: Presidio. 1990. ISBN 0-89141-380-4. 
Soviet military operational art: in pursuit of deep battle. London, England, Portland, Oregon: F. Cass. 1991. ISBN 0-7146-4077-8. 
From the Don to the Dnepr: Soviet Offensive Operations, December 1942-August 1943. London, England, Portland, Oregon: F. Cass. 1991. ISBN 0-7146-3350-X. 
The Military Strategy of the Soviet Union: A History. London, England, Portland, Oregon: F. Cass. 1992. ISBN 0-7146-3435-2. 
The History of Soviet Airborne Forces (1994) ISBN 0-7146-3483-2
Soviet Documents on the Use of War Experience: The Winter Campaign, 1941-1942 (Cass Series on the Soviet Study of War, 2), David M. Glantz (Editor), Harold S. Orenstein (Editor)
When Titans Clashed: How the Red Army Stopped Hitler (1995) ISBN 0-7006-0717-X
The Initial Period of War on the Eastern Front, 22 June - August 1941: Proceedings of the Fourth Art of War Symposium, Garmisch, October, 1987 (Cass Series on Soviet Military Experience, 2), edited by Colonel David M. Glantz, Routledge, (1997) ISBN 978-0-7146-4298-7
Stumbling Colossus: The Red Army on the Eve of World War (1998) ISBN 0-7006-0879-6
Kharkov 1942: Anatomy of a Military Disaster (1998) ISBN 1-885119-54-2
Reviewed by John Erickson in The Journal of Military History, Vol. 63, No. 2 (April, 1999), pp. 482 483

Zhukov's Greatest Defeat: The Red Army's Epic Disaster in Operation Mars, 1942" (1999) ISBN 0-7006-0944-X
Foreword to Forging Stalin's Army: Marshal Tukhachevsky And The Politics Of Military Innovation by Sally Stoecker
The Battle of Kursk (1999) ISBN 0-7006-0978-4
Barbarossa: Hitler's Invasion of Russia 1941 (2001) ISBN 0-7524-1979-X
Captured Soviet Generals: The Fate of Soviet Generals Captured by the Germans, 1941-1945, Aleksander A. Maslov, edited and translated by David M. Glantz and Harold S. Orenstein, Routledge; 1 edition (2001), ISBN 978-0-7146-5124-8
The Siege of Leningrad, 1941-1944: 900 Days of Terror (2001) ISBN 0-7603-0941-8
Belorussia 1944: The Soviet General Staff Study, Soviet Union Raboche-Krestianskaia Krasnaia Armiia Generalnyi Shtab, Glantz, David M. (Editor), Orenstein, Harold S. (Editor), Frank Cass & Co, 2001 ISBN 978-0-7146-5102-6
The Battle for Leningrad, 1941-1944 (2002) ISBN 0-7006-1208-4
Before Stalingrad: Barbarossa, Hitler's Invasion of Russia 1941 (Battles & Campaigns), Tempus, 2003 ISBN 978-0-7524-2692-1
Battle for the Ukraine: The Korsun'-Shevchenkovskii Operation (Soviet (Russian) Study of War), Frank Cass Publishers, 2003 ISBN 0-7146-5278-4
The Soviet Strategic Offensive in Manchuria, 1945: August Storm (2003) ISBN 0-7146-5279-2
Atlas and operational summary : the border battles, 22 June-1 July 1941; daily situation maps prepared by Michael Avanzini, Publisher: David Glantz, 2003
Hitler and His Generals: Military Conferences 1942-1945: The First Complete Stenographic Record of the Military Situation Conferences, from Stalingrad to Berlin, Helmut Heiber and David M. Glantz editors (English edition), Enigma Books; (2005) ISBN 978-1-929631-28-5
Colossus Reborn: The Red Army at War, 1941-1943 (2005) ISBN 0-7006-1353-6
Companion to Colossus Reborn: Key Documents and Statistics (2005) ISBN 0-7006-1359-5
Red Storm Over the Balkans: The Failed Soviet Invasion of Romania, Spring 1944 (2006) ISBN 0-7006-1465-6
Stalingrad: How the Red Army Survived the German Onslaught, Casemate Publishers and Book Distributors, Jones, Michael K. (Author), Glantz, David M. (Foreword) 2007 ISBN 978-1-932033-72-4
To the Gates of Stalingrad: Soviet-German Combat Operations, April August 1942 (The Stalingrad Trilogy, Volume 1) (Modern War Studies) with Jonathan M. House, University Press of Kansas, 2009
Armageddon in Stalingrad: September November 1942 (The Stalingrad Trilogy, Volume 2) (Modern War Studies) with Jonathan M. House, University Press of Kansas, 2009
After Stalingrad: The Red Army's Winter Offensive 1942-1943 ISBN 978-1-906033-26-2
Barbarossa Derailed: The Battle for Smolensk, 10 July-10 September 1941 Volume 1, Helion & Company, 2010; ISBN 1906033722
Barbarossa Derailed: The Battle for Smolensk, 10 July-10 September 1941 Volume 2, Helion & Company, 2012; ISBN 1906033900
Barbarossa Derailed: The Battle for Smolensk, 10 July-10 September 1941 Volume 3, Helion & Company, 2014; ISBN 1909982113


