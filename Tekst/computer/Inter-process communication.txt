In computer science, inter-process communication (IPC) is the activity of sharing data across multiple and commonly specialized processes using communication protocols. Typically, applications using IPC are categorized as clients and servers, where the client requests data and the server responds to client requests. Many applications are both clients and servers, as commonly seen in distributed computing. Methods for achieving IPC are divided into categories which vary based on software requirements, such as performance and modularity requirements, and system circumstances, such as network bandwidth and latency.
There are several reasons for implementing inter-process communication systems:
Sharing information; for example, web servers use IPC to share web documents and media with users through a web browser.
Distributing labor across systems; for example, Wikipedia uses multiple servers that communicate with one another using IPC to process user requests.
Privilege separation; for example, HMI software systems are separated into layers based on privileges to minimize the risk of attacks. These layers communicate with one another using encrypted IPC.






There are several mechanisms which may be used for IPC, including the following:

The following are platform or programming language specific APIs:


