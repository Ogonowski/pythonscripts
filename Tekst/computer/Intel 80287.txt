x87 is a floating point-related subset of the x86 architecture instruction set. It originated as an extension of the 8086 instruction set in the form of optional floating point coprocessors that worked in tandem with corresponding x86 CPUs. These microchips had names ending in "87". This was also known as the NPX (Numeric Processor eXtension). Like other extensions to the basic instruction set, x87-instructions are not strictly needed to construct working programs, but provide hardware and microcode implementations of common numerical tasks, allowing these tasks to be performed much faster than corresponding machine code routines can. The x87 instruction set includes instructions for basic floating point operations such as addition, subtraction and comparison, but also for more complex numerical operations, such as the computation of the tangent function and its inverse, for example.
Most x86 processors since the Intel 80486 have had these x87 instructions implemented in the main CPU but the term is sometimes still used to refer to that part of the instruction set. Before x87 instructions were standard in PCs, compilers or programmers had to use rather slow library calls to perform floating-point operations, a method that is still common in (low-cost) embedded systems.



The x87 registers form an 8-level deep non-strict stack structure ranging from ST(0) to ST(7) with registers that can be directly accessed by either operand, using an offset relative to the top, as well as pushed and popped. (This scheme may be compared to how a stack frame may be both pushed, popped and indexed.)
There are instructions to push, calculate, and pop values on top of this stack; monadic operations (FSQRT, FPTAN etc.) then implicitly address the topmost ST(0) while dyadic operations (FADD, FMUL, FCOM, etc.) implicitly address ST(0) and ST(1). The non-strict stack-model also allows dyadic operations to use ST(0) together with a direct memory operand or with an explicitly specified stack-register, ST(x), in a role similar to a traditional accumulator (a combined destination and left operand). This can also be reversed on an instruction-by-instruction basis with ST(0) as the unmodified operand and ST(x) as the destination. Furthermore, the contents in ST(0) can be exchanged with another stack register using an instruction called FXCH ST(x).
These properties makes the x87 stack usable as seven freely addressable registers plus a dedicated accumulator (or as seven independent accumulators). This is especially applicable on superscalar x86 processors (such as the Pentium of 1993 and later) where these exchange instructions (codes D9C8..D9CFh) are optimized down to a zero clock penalty by using one of the integer paths for FXCH ST(x) in parallel with the FPU instruction. Despite being natural and convenient for human assembly language programmers, some compiler writers have found it complicated to construct automatic code generators that schedule x87 code effectively. Such a stack-based interface potentially can minimize the need to save scratch variables in function calls compared with a register-based interface  (although, historically, design issues in the original implementation limited that potential ).
The x87 provides single precision, double precision and 80-bit double-extended precision binary floating-point arithmetic as per the IEEE 754-1985 standard. By default, the x87 processors all use 80-bit double-extended precision internally (to allow for sustained precision over many calculations- see IEEE 754 design rationale). A given sequence of arithmetic operations may thus behave slightly differently compared to a strict single-precision or double-precision IEEE 754 FPU. As this may sometimes be problematic for some semi-numerical calculations written to assume double precision for correct operation, to avoid such problems, the x87 can be configured via a special configuration/status register to automatically round to single or double precision after each operation. Since the introduction of SSE2, the x87 instructions are not as essential as they once were, but remain important as a high precision scalar unit for numerical calculations sensitive to round-off error and requiring the 64-bit mantissa precision and extended range available in the 80-bit format.



Clock cycle counts for examples of typical x87 FPU instructions (only register-register versions shown here).
The A~B notation (minimum to maximum) covers timing variations dependent on transient pipeline status as well as the arithmetic precision chosen (32, 64 or 80 bits); it also includes variations due to numerical cases (such as the number of set bits, zero, etc.). The L H notation depicts values corresponding to the lowest (L) and the highest (H) maximum clock frequencies that were available.
* An effective zero clock delay is often possible, via superscalar execution.
  The 5 MHz 8087 was the original x87 processor. Compared to typical software-implemented floating point routines on an 8086 (without an 8087), the factors would be even larger, perhaps by another factor of 10 (i.e., a correct floating point addition in assembly language may well consume over 1000 cycles).



Companies that have designed or manufactured floating point units compatible with the Intel 8087 or later models include AMD (287, 387, 486DX, 5x86, K5, K6, K7, K8), Chips and Technologies (the Super MATH coprocessors), Cyrix (the FasMath, Cx87SLC, Cx87DLC, etc., 6x86, Cyrix MII), Fujitsu (early Pentium Mobile etc.), Harris Semiconductor (manufactured 80387 and 486DX processors), IBM (various 387 and 486 designs), IDT (the WinChip, C3, C7, Nano, etc.), IIT (the 2C87, 3C87, etc.), LC Technology (the Green MATH coprocessors), National Semiconductor (the Geode GX1, Geode GXm, etc.), NexGen (the Nx587), Rise Technology (the mP6), ST Microelectronics (manufactured 486DX, 5x86, etc.), Texas Instruments (manufactured 486DX processors etc.), Transmeta (the TM5600 and TM5800), ULSI (the Math Co coprocessors), VIA (the C3, C7, and Nano, etc.), and Xtend (the 83S87SX-25 and other coprocessors).







The 8087 was the first math coprocessor for 16-bit processors designed by Intel. It was built to be paired with the Intel 8088 or 8086 microprocessors. However, the Intel 8231 floating-point processor was an earlier design. It was a licensed version of AMD's Am9511 of 1977. The Am9511 was primarily intended for the Intel 8080, but, using some amount of glue logic, it was possible to use it with almost any microprocessor (-system) that had a spare interrupt input or interrupt vector available. The family included the 32-bit Am9511 and Am9511A (or Intel 8231/8231A) and the later 64-bit Am9512 (or Intel 8232).



The 80187 (80C187) was the math coprocessor for Intel 80186 CPU. It is incapable of operating with the 80188, as the 80188 has an 8 bit data bus; the 80188 can only use the 8087. The 80187 did not appear at the same time as the 80186 and 80188, but was in fact launched after the 80287 and the 80387. Although the interface to the main processor was the same as that of the 8087, its core was that of the 80387, and was thus fully IEEE 754 compliant as well as capable of executing all the 80387's extra instructions.




The 80287 (i287) was the math coprocessor for the Intel 80286 series of microprocessors. Intel's models included variants with specified upper frequency limits ranging from 6 up to 12 MHz. Later followed the i80287XL with 387 microarchitecture and the i80287XLT, a special version intended for laptops, as well as other variants.
The 80287XL was actually an 80387SX with a 287 pinout. It contained an internal 3/2 multiplier so that motherboards which ran the coprocessor at 2/3 CPU speed could instead run the FPU at the same speed of the CPU. Other 287 models with 387-like performance were the Intel 80C287, built using CHMOS III, and the AMD 80EC287 manufactured in AMD's CMOS process, using only fully static gates.
The 80287 and 80287XL worked with the 80386 microprocessor, and were initially the only coprocessors available for the 80386 until the introduction of the 80387 in 1987. Finally, they were able to work with the Cyrix Cx486SLC. However, for both of these chips the 80387 was strongly preferred for its higher performance and the greater capability of its instruction set.



The 80387 (387 or i387) was the first Intel coprocessor to be fully compliant with the IEEE 754-1985 standard. Released in 1987, a full two years after the 386 chip, the i387 included much improved speed over Intel's previous 8087/80287 coprocessors, and improved the characteristics of trigonometric functions. The 8087 and 80287's FPTAN and FPATAN instructions were limited to an argument in the range  /4 ( 45 ) and had no direct instructions for the sin and cos functions.

Without a coprocessor, the 386 normally performed floating-point arithmetic through (slow) software routines, implemented at runtime through a software exception-handler. When a math coprocessor is paired with the 386, the coprocessor performs the floating point arithmetic in hardware, returning results much faster than an (emulating) software library call.
The i387 was compatible only with the standard i386 chip, which had a 32-bit processor bus. The later cost-reduced i386SX, which had a narrower 16-bit data bus, could not interface with the i387's 32-bit bus. The i386SX required its own coprocessor, the 80387SX, which was compatible with the SX's narrower 16-bit data bus.



The i487SX was marketed as a floating point unit coprocessor for Intel i486SX machines. It actually contained a full-blown i486DX implementation. When installed into an i486SX system, the i487 disabled the main CPU and took over all CPU operations.



The Nx587 was the last FPU for x86 to be manufactured separately from the CPU, in this case NexGen's Nx586.



MMX
SSE, SSE2, SSE3, SSSE3, SSE4, SSE5
3DNow!
SIMD




Intel 64 and IA-32 Architectures Software Developer s Manual Volume 1: Basic Architecture (PDF). Intel. 






Everything you always wanted to know about math coprocessors