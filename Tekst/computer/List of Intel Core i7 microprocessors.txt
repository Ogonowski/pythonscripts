The following is a list of Intel Core i7 brand microprocessors.









All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x, Hyper-threading, Turbo Boost, Smart Cache.
FSB has been replaced with QPI.
Transistors: 731 million
Die size: 263 mm 
Steppings: C0, D0



All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), TXT, Intel VT-x, Intel VT-d, Hyper-threading, Turbo Boost, Smart Cache.
Core i7-875K features an unlocked multiplier and does not support Intel TXT and Intel VT-d..
FSB has been replaced with DMI.
Transistors: 774 million
Die size: 296 mm 
Stepping: B1






All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x, Hyper-threading, Turbo Boost, AES-NI, Smart Cache.
Core i7-980X and 990X feature an unlocked multiplier.
FSB has been replaced with QPI.
Transistors: 1170 million
Die size: 239 mm 
Steppings: B1






Most models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), TXT, Intel VT-x, Intel VT-d, Hyper-threading, Turbo Boost, AES-NI, Smart Cache, Intel Insider, vPro
Support for up to 4 DIMMS of DDR3-1333 memory.
S processors feature lower-than-normal TDP (65 W on 4-core models).
K processors have unlocked turbo multiplier but does not support Intel TXT, Intel VT-d  and vPro.
Non-K processors will have limited turbo overclocking.
Transistors: 1.16 billion
Die size: 216 mm 



All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x, Intel VT-d, Hyper-threading, Turbo Boost, AES-NI, Smart Cache.
Support for up to 8 DIMMS of DDR3-1600 memory.
Transistors: 1.27 (M1 stepping) or 2.27 (C1, C2 steppings) billion
Die size: 294 (M1 stepping) or 435 (C1, C2 steppings) mm 






All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, F16C, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x, Hyper-threading, Turbo Boost 2.0, AES-NI, Smart Cache, Intel Insider,
Support for up to 4 DIMMS of DDR3-1600 memory.
All models except the K processors additionally support Intel TXT, Intel VT-d and vPro.
S processors feature lower-than-normal TDP (65 W on 4-core models).
T processors are performance optimized
K processors have unlocked turbo multiplier but does not support Intel TXT, Intel VT-d  and vPro. Non-K processors will have limited turbo overclocking.
Transistors: 1.4 billion
Die size: 160 mm 



All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, F16C, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x, Intel VT-d, Hyper-threading, Turbo Boost 2.0, AES-NI, Smart Cache.
Support for up to 8 DIMMS of DDR3-1866 memory.
Transistors: 1.86 billion
Die size: 256.5 mm 






All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, AVX2, FMA3, F16C, (BMI1)(Bit Manipulation Instructions1)+BMI2, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x, Hyper-threading, Turbo Boost 2.0, AES-NI, Smart Cache, Intel Insider
All models except the i7-4770K additionally support Intel TSX-NI and Intel VT-d
All models except the i7-4770K and i7-4790K additionally support vPro and TXT
Transistors: 1.4 billion
Die size: 177 mm 



All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, AVX2, FMA3, F16C, (BMI1)(Bit Manipulation Instructions1)+BMI2, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x, Intel VT-d, Hyper-threading, Turbo Boost 2.0, AES-NI, Smart Cache, Intel Insider.
i7-4770R does not support TSX, TXT and Vpro.
Core i7-4770R also contains "Crystalwell": 128 MiB eDRAM built at (22 nm) acting as L4 cache
Transistors: 1.4 billion
Die size: 264mm  + 84mm 



All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, AVX2, FMA3, F16C, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x, Intel VT-d, Hyper-threading, Turbo Boost 2.0, AES-NI, Smart Cache.
Support for up to 8 DIMMS of DDR4-2133 memory.
Transistors: 2.60 billion
Die size: 356 mm 
i7-5820K has 28 PCI Express lanes; i7-5930K and i7-5960X have 40






All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, AVX2, FMA3, F16C, (BMI1)(Bit Manipulation Instructions1)+BMI2, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x, Intel VT-d, Hyper-threading, Turbo Boost 2.0, AES-NI, Smart Cache, Intel Insider.
All models also contain "Crystal Well": 128 MiB eDRAM acting as L4 cache
Transistors:
Die size:






All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, AVX2, FMA3, F16C, (BMI1)(Bit Manipulation Instructions1)+BMI2, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x, Intel VT-d, Hyper-threading, Turbo Boost 2.0, AES-NI, Smart Cache, Intel Insider.
Embedded models also support: Intel vPro, Intel TXT.
Transistors:
Die size:









All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), TXT, Intel VT-x, Intel VT-d, Hyper-threading, Turbo Boost, Smart Cache.
FSB has been replaced with DMI.
Transistors: 774 million
Die size: 296 mm 
Steppings: B1






All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), TXT, Intel VT-x, Intel VT-d, Hyper-threading, Turbo Boost, AES-NI, Smart Cache.
FSB has been replaced with DMI.
Contains 45 nm "Ironlake" GPU.
CPU Transistors: 382 million
CPU die size: 81 mm 
Graphics and Integrated Memory Controller transistors: 177 million
Graphics and Integrated Memory Controller die size: 114 mm 
Steppings: C2, K0
Core i7-610E, i7-620UE, i7-620LE and i7-660UE have support for ECC memory and PCI express port bifurcation.






All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), TXT, Intel VT-x, Intel VT-d, Hyper-threading, Turbo Boost, AES-NI, Smart Cache.
Core i7-2620M, Core i7-2640M, Core i7-2637M, and Core i7-2677M support Intel Insider
Core i7-2610UE, Core i7-2655LE does not support XD bit(Execute Disable Bit).
Core i7-2610UE, Core i7-2655LE has support for ECC memory
Transistors: 624 million
Die size: 149 mm 



All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), TXT, Intel VT-x, Intel VT-d, Hyper-threading, Turbo Boost, AES-NI, Smart Cache, Intel Insider.
Core i7-2630QM, Core i7-2635QM, Core i7-2670QM, Core i7-2675QM do not support TXT and Intel VT-d.
Core i7-2715QE has support for ECC memory.
Core i7-2710QE, Core i7-2715QE do not support Intel Insider and XD bit(Execute Disable Bit).
Transistors: 1.16 billion
Die size: 216 mm 






All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, F16C, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), TXT, Intel VT-x, Intel VT-d, Hyper-threading, Turbo Boost2.0, AES-NI, Smart Cache, Intel Insider.
Core i7-3517U, i7-3537U do not support Intel TXT.
Core i7-3555LE and Core i7-3517UE do not support Intel Insider.



All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, F16C, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), TXT, Intel VT-x, Intel VT-d, Hyper-threading, Turbo Boost, AES-NI, Smart Cache, Intel Insider.
Core i7-3610QM, Core i7-3612QM and Core i7-3630QM (Socket G2) do not support Intel VT-d.
Core i7-3610QE, Core i7-3615QE and Core i7-3612QE do not support Intel Insider.
Transistors: 1.4 billion
Die size: 160 mm 






All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, AVX2, FMA3, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel TXT, Intel VT-x, Intel VT-d, Hyper-threading, Turbo Boost, AES-NI, Intel vPro, Intel TSX-NI, Smart Cache
Transistors: 1.3 billion
Die size: 181 mm 



All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, AVX2, FMA3, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x, Hyper-threading, Turbo Boost, AES-NI, Smart Cache
Core i7-4550U and higher also support Intel VT-d
Core i7-4600U and i7-4650U also support Intel vPro and Intel TXT
Transistors: 1.3 billion
Die size: 181 mm 



All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, AVX2, FMA3, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x, Hyper-threading, Turbo Boost, AES-NI, Smart Cache, Intel VT-d, Intel vPro, Intel TXT, and Intel TSX-NI
Transistors: 1.3 billion
Die size: 181 mm 



All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, F16C, AVX2, FMA3, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x, Hyper-threading, Turbo Boost, AES-NI, Smart Cache, Intel Insider
Core i7-48xxMQ, i7-49xxMQ, and all MX models also support Intel TXT, Intel VT-d, and vPro.
Transistors: 1.4 billion
Die size: 177 mm 



All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, AVX2, FMA3, F16C, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x, Intel VT-d, Intel TXT, Hyper-threading, Turbo Boost, AES-NI, Smart Cache, Intel Insider.
Core i7-48xxHQ, i7-49xxHQ, and all EQ models also support Intel vPro and Intel TSX-NI
Models with Iris Pro Graphics 5200 also contain "Crystalwell": 128 MiB eDRAM built at (22 nm) acting as L4 cache
EQ models support ECC memory
Transistors:
Die size: 264mm  + 84mm 






All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, AVX2, FMA3, F16C, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x, Intel VT-d, Intel TXT, Hyper-threading, Turbo Boost, AES-NI, Smart Cache, Intel Insider, and configurable TDP (cTDP) down (47W 37W).
Models with Iris Pro Graphics 6200 also contain "Crystalwell": 128 MiB eDRAM acting as L4 cache
EQ models also support Intel vPro, Intel TSX-NI, and ECC memory.
Transistors:
Die size:



All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, AVX2, FMA3, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x, Intel VT-d, Hyper-threading, Turbo Boost, AES-NI, Smart Cache, configurable TDP (cTDP) down
Core i7-5600U and higher also support Intel vPro, Intel TXT, and Intel TSX-NI
Transistors: 1.3-1.9 billion 
Die size: 82-133 mm  






All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, AVX2, FMA3, F16C, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x, Intel VT-d, Hyper-threading, Turbo Boost, AES-NI, Smart Cache, Intel Insider, Intel TSX-NI, and configurable TDP (cTDP) down (45W 35W).
Core i7-6820HQ, Core i7-6920HQ, and embedded models also support Intel vPro, Intel TXT.
Core i7-6820HK features an unlocked multiplier.
Transistors:
Die size:



All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, AVX2, FMA3, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x, Intel VT-d, Hyper-threading, Turbo Boost, AES-NI, Smart Cache, Intel TSX-NI, and configurable TDP (cTDP) down
Core i7-6600U and higher also support Intel vPro, Intel TXT.
Transistors:
Die size:


