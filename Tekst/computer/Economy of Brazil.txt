Brazil has the eighth largest economy by nominal GDP in the world as of 2015, and seventh largest by purchasing power parity. The Brazilian economy is characterized by moderately free markets and an inward-oriented economy.
Brazil s economy is the largest of Latin America and the second largest in the western hemisphere. From 2000 up to 2012, Brazil was one of the fastest-growing major economies in the world, with an average annual GDP growth rate of over 5%, with its economy in 2012 surpassing that of the United Kingdom, making Brazil the world's sixth largest economy. However, Brazil's economy growth has decelerated in 2013 and had almost no liquid growth throughout 2014, and the country's economy is expected to shrink by 2.5% in 2015.
According to the World Economic Forum, Brazil was the top country in upward evolution of competitiveness in 2009, gaining eight positions among other countries, overcoming Russia for the first time, and partially closing the competitiveness gap with India and China among the BRIC economies. Important steps taken since the 1990s toward fiscal sustainability, as well as measures taken to liberalize and open the economy, have significantly boosted the country s competitiveness fundamentals, providing a better environment for private-sector development.
In 2012 Forbes ranked Brazil as having the 5th largest number of billionaires in the world, a number much larger than what is found in other Latin American countries, and even ahead of United Kingdom and Japan. Brazil is a member of diverse economic organizations, such as Mercosur, Unasul, G8+5, G20, WTO, and the Cairns Group.




When the Portuguese explorers arrived in the 15th century, the native tribes of current-day Brazil, totaling about 2.5 million people, had lived virtually unchanged since the Stone Age. From Portugal's colonization of Brazil (1500-1822) until the late 1930s, the market elements of the Brazilian economy relied on the production of primary products for exports. Within the Portuguese Empire, Brazil was a colony subjected to an imperial mercantile policy, which had three main large-scale economic production cycles - sugar, gold and, from the early 19th century on, coffee. The economy of Brazil was heavily dependent on African enslaved labour until the late 19th century (about 3 million imported African enslaved individuals in total). In that period Brazil was also the colony with the largest amount of European settlers, most of them being ethnic Portuguese (including Azoreans and Madeirans), but also some Dutch (see Dutch Brazil), Spaniards, English, French, Germans, Flemish, Danish, Scottish and sephardic Jews. Since then, Brazil experienced a period of strong economic and demographic growth accompanied by mass immigration from Europe, mainly from Portugal (including the Azores and Madeira), Italy, Spain, Germany, Poland, Ukraine, Switzerland, Austria and Russia. Smaller amounts of immigrants also came from the Netherlands, France, Finland, Iceland and the Scandinavian countries, Lithuania, Belgium, Bulgaria, Hungary, Greece, Latvia, England, Ireland, Scotland, Croatia, Czech Republic, Malta, Macedonia and Luxembourg), the Middle East (mainly from Lebanon, Syria and Armenia), Japan, the United States and South Africa, until the 1930s. In the New World, the United States, Argentina, Brazil, Canada, Australia, Uruguay, New Zealand, Chile, Mexico, Cuba, Venezuela, Paraguay, Puerto Rico and Peru (in descending order) were the countries that received most immigrants. In Brazil's case, statistics show that 4.5 million people emigrated to the country between 1882 and 1934.
Currently, with a population of over 190 million and abundant natural resources, Brazil is one of the ten largest markets in the world, producing tens of millions of tons of steel, 26 million tons of cement, 3.5 million television sets, and 3 million refrigerators. In addition, about 70 million cubic meters of petroleum were being processed annually into fuels, lubricants, propane gas, and a wide range of hundred petrochemicals. Furthermore, Brazil has at least 161,500 kilometers of paved roads and more than 93 Gigawatts of installed electric power capacity.
Its real per capita GDP has surpassed US$ 10,500 in 2008, due to the strong and continued appreciation of the real for the first time this decade. Its industrial sector accounts for three fifths of the Latin American economy's industrial production. The country s scientific and technological development is argued to be attractive to foreign direct investment, which has averaged US$30 billion per year the last years, compared to only US$2 billion per year last decade, thus showing a remarkable growth. The agricultural sector, locally called the agroneg cio (agrobusiness), has also been remarkably dynamic: for two decades this sector has kept Brazil amongst the most highly productive countries in areas related to the rural sector. The agricultural sector and the mining sector also supported trade surpluses which allowed for massive currency gains (rebound) and external debt paydown. Due to downturn in Western economies Brazil found itself in 2010 trying to halt the appreciation of the real.
Data from the Asian Development Bank and the Tax Justice Network show the untaxed "shadow" economy of GDP for Brazil is 39%.



The service sector is the largest component of GDP at 67.0 percent, followed by the industrial sector at 27.5 percent. Agriculture represents 5.5 percent of GDP (2011). Brazilian labor force is estimated at 100.77 million of which 10 percent is occupied in agriculture, 19 percent in the industry sector and 71 percent in the service sector.




Agribusiness contributes to Brazil s trade balance, in spite of trade barriers and subsidizing policies adopted by the developed countries.
In the space of fifty five years (1950 to 2005), the population of Brazil grew from 51 million to approximately 187 million inhabitants, an increase of over 2 percent per year. Brazil created and expanded a complex agribusiness sector. However, some of this is at the expense of the environment, including the Amazon.
The importance given to the rural producer takes place in the shape of the agricultural and cattle-raising plan and through another specific subsidy program geared towards family agriculture (Pronaf), which guarantee financing for equipment and cultivation and encourage the use of new technology. With regards to family agriculture, over 800 thousand rural inhabitants are assisted by credit, research and extension programs. A special line of credit is available for women and young farmers.
With The Land Reform Program, on the other hand, the country's objective is to provide suitable living and working conditions for over one million families who live in areas allotted by the State, an initiative capable of generating two million jobs. Through partnerships, public policies and international partnerships, the government is working towards the guarantee of an infrastructure for the settlements, following the examples of schools and health outlets. The idea is that access to land represents just the first step towards the implementation of a quality land reform program.
Over 600,000 km  of land are divided into approximately five thousand areas of rural property; an agricultural area currently with three borders: the Central-western region (savanna), the Northern region (area of transition) and parts of the Northeastern region (semi-arid). At the forefront of grain crops, which produce over 110 million tonnes/year, is the soybean, yielding 50 million tonnes.
In the bovine cattle-raising sector, the "green ox," which is raised in pastures, on a diet of hay and mineral salts, conquered markets in Asia, Europe and the Americas, particularly after the "mad cow disease" scare period. Brazil has the largest cattle herd in the world, with 198 million heads, responsible for exports surpassing the mark of US$1 billion/year.
A pioneer and leader in the manufacture of short-fiber timber cellulose, Brazil has also achieved positive results within the packaging sector, in which it is the fifth largest world producer. In the foreign markets, it answers for 25 percent of global exports of raw cane and refined sugar; it is the world leader in soybean exports and is responsible for 80 percent of the planet's orange juice, and since 2003, has had the highest sales figures for beef and chicken, among the countries that deal in this sector.




Brazil has the third largest manufacturing sector in the Americas. Accounting for 28.5 percent of GDP, Brazil's diverse industries range from automobiles, steel and petrochemicals to computers, aircraft, and consumer durables. With increased economic stability provided by the Plano Real, Brazilian and multinational businesses have invested heavily in new equipment and technology, a large proportion of which has been purchased from U.S. firms.
Brazil has a diverse and sophisticated services industry as well. During the early 1990s, the banking sector accounted for as much as 16 percent of the GDP. Although undergoing a major overhaul, Brazil's financial services industry provides local businesses with a wide range of products and is attracting numerous new entrants, including U.S. financial firms. On 8 May 2008, the S o Paulo Stock Exchange (Bovespa) and the S o Paulo-based Brazilian Mercantile and Futures Exchange (BM&F) merged, creating BM&FBOVESPA, one of the largest stock exchanges in the world. Also, the previously monopolistic reinsurance sector is being opened up to third party companies.
As of 31 December 2007, there were an estimated 21,304,000 broadband lines in Brazil. Over 75 percent of the broadband lines were via DSL and 10 percent via cable modems.
Proven mineral resources are extensive. Large iron and manganese reserves are important sources of industrial raw materials and export earnings. Deposits of nickel, tin, chromite, uranium, bauxite, beryllium, copper, lead, tungsten, zinc, gold, and other minerals are exploited. High-quality coking-grade coal required in the steel industry is in short supply.




In 2011, 36 Brazilian companies were listed in the Forbes Global 2000 list - an annual ranking of the top 2000 public companies in the world by Forbes magazine. The 13 leading companies were:




The Brazilian government has undertaken an ambitious program to reduce dependence on imported petroleum. Imports previously accounted for more than 70% of the country's oil needs but Brazil became self-sufficient in oil in 2006-2007. Brazil is one of the world's leading producers of hydroelectric power, with a current capacity of about 260,000 megawatts. Existing hydroelectric power provides 90 percent of the nation's electricity. Two large hydroelectric projects, the 19,900 megawatt Itaipu Dam on the Paran  River (the world's largest dam) and the Tucurui Dam in Par  in northern Brazil, are in operation. Brazil's first commercial nuclear reactor, Angra I, located near Rio de Janeiro, has been in operation for more than 10 years. Angra II was completed in 2002 and is in operation too. An Angra III has its planned inauguration scheduled for 2014. The three reactors would have combined capacity of 9,000 megawatts when completed. The government also plans to build 19 more nuclear plants by the year 2020.







After the arrival of the Portuguese explorers in 1500, it was only in 1808 that Brazil obtained a permit from the Portuguese colonial government to set up its first factories and manufacturers. In the 21st century, Brazil reached the status of 8th largest economy in the world. Originally, the export list was basic raw and primary goods, such as sugar, rubber and gold. Today, 84 percent of exports consists of manufactured and semi-manufactured products.
The period of great economic transformation and growth occurred between 1875 and 1975.
In the last decade, domestic production increased by 32.3 percent and agribusiness (agriculture and cattle-raising), which grew by 47 percent or 3.6 percent per year, was the most dynamic sector   even after having weathered international crises that demanded constant adjustments to the Brazilian economy. The Brazilian government also launched a program for economic development acceleration called Programa de Acelera o do Crescimento, aiming to spur growth.
Brazil's transparency ranking status in the international world is 75th according to Transparency International.



Among measures recently adopted in order to balance the economy, Brazil carried out reforms to its Social security (state and retirement pensions) and Tax systems. These changes brought with them a noteworthy addition: a Law of Fiscal Responsibility which controls public expenditure by the Executive Branches at federal, state and municipal levels. At the same time, investments were made towards administration efficiency and policies were created to encourage exports, industry and trade, thus creating "windows of opportunity" for local and international investors and producers.
With these alterations in place, Brazil has reduced its vulnerability: it doesn't import the oil it consumes; it has halved its domestic debt through exchange rate-linked certificates and has seen exports grow, on average, by 20% a year. The exchange rate does not put pressure on the industrial sector or inflation (at 4% a year), and does away with the possibility of a liquidity crisis. As a result, the country, after 12 years, has achieved a positive balance in the accounts which measure exports/imports, plus interest payments, services and overseas payment. Thus, respected economists say that the country won't be deeply affected by the current world economic crisis.



Support for the productive sector has been simplified at all levels; active and independent, Congress and the Judiciary Branch carry out the evaluation of rules and regulations. Among the main measures taken to stimulate the economy are the reduction of up to 30 percent on Manufactured Products Tax (IPI), and the investment of $8 billion on road cargo transportation fleets, thus improving distribution logistics. Further resources guarantee the propagation of business and information telecenters.
The Policy for Industry, Technology and Foreign Trade, at the forefront of this sector, for its part, invests $19.5 billion in specific sectors, following the example of the software and semiconductor, pharmaceutical and medicine product, and capital goods sectors.




Between 1993 and 2010, 7.012 mergers & acquisitions with a total known value of $707 billion USD with the involvement of Brazilian firms have been announced. The year 2010 was a new record in terms of value with $115 billion of transactions. The largest transaction with involvement of Brazilian companies has been: Cia Vale do Rio Doce acquired Inco in a tender offer valued at $18.9 billions.



According to a search of Global Entrepreneurship Monitor in 2011 Brazil had 27 million adults aged between 18 and 64 either starting or owning a business, meaning that more than one in four Brazilian adults were entrepreneurs. In comparison to the other 54 countries studied, Brazil was the third-highest in absolute number of entrepreneurs. Ipea, a government agency, found that 37 million jobs in Brazil were associated with businesses with up to 10 employees.
The most recent research of Global Entrepreneurship Monitor revealed in 2013 that 50.4% of Brazilian new entrepreneurs are men, 33.8% are in the 35-44 age group, 36.9% completed high school and 47.9% earn 3-6 times the Brazilian minimum wage. In contrast, 49.6% of entrepreneurs are female, only 7% are in the 55 64 age group, 1% have postgraduate education and 1.7% earn more than 9 times the minimum wage.



Brazil's credit rating was downgraded by Standard & Poor's to BBB in March 2014, just one notch above junk.




The minimum wage set for the year of 2014 is R$724 reais per month plus an additional 13th salary in second half of December. The GDP per capita in 2011 was US$12,906.



List of Brazilian federative units by gross domestic product






S o Paulo Stock Exchange Official website
Central Bank of Brazil Official website
"The Informality Trap: Tax Evasion, Finance, and Productivity in Brazil" World Bank Public Policy Journal
Macroeconomic Policy, Growth and Income Distribution in the Brazilian Economy in the 2000s Center for Economic and Policy Research
Comprehensive current and historial economic data
World Bank Trade Summary Statistics Brazil 2012
Brazil Economic Outlook
Information about Brazilian banks in English
Tariffs applied by Brazil as provided by ITC's Market Access Map, an online database of customs tariffs and market requirements.
Brazilian Soda Market
International Trade Statistics - Brazil (2014)



Furtado, Celso. Forma o econ mica do Brasil. (http://www.afoiceeomartelo.com.br/posfsa/Autores/Furtado,%20Celso/Celso%20Furtado%20-%20Forma%C3%A7%C3%A3o%20Econ%C3%B4mica%20do%20Brasil.pdf)
Prado Junior, Caio. Hist ria econ mica do Brasil. (http://www.afoiceeomartelo.com.br/posfsa/Autores/Prado%20Jr,%20Caio/Historia%20Economica%20do%20Brasil.pdf)
Baer, Werner. The Brazilian Economy: Growth and Development. 5th. Westport, CT: Praeger Publishers, 2001
Center for Economic and Policy Research, The Brazilian Economy in Transition: Macroeconomic Policy, Labor and Inequality. (http://www.cepr.net/index.php/publications/reports/brazil-2014-09 ) September 2014