Serial Storage Architecture (SSA) was a serial transport protocol used to attach disk drives to server computers.



SSA was invented by Ian Judd of IBM in 1990. IBM produced a number of successful products based upon this standard before it was overtaken by the more widely adopted Fibre Channel protocol.
SSA was promoted as an open standard by the SSA Industry Association, unlike its predecessor the first generation Serial Disk Subsystem.  A number of vendors including IBM, Pathlight Technology and Vicom Systems produced products based on SSA. It was also adopted as an American National Standards Institute (ANSI) X3T10.1 standard. SSA devices are logically SCSI devices and conform to all of the SCSI command protocols.
SSA provides data protection for critical applications by helping to ensure that a single cable failure will not prevent access to data. All the components in a typical SSA subsystem are connected by bi-directional cabling. Data sent from the adaptor can travel in either direction around the loop to its destination. SSA detects interruptions in the loop and automatically reconfigures the system to help maintain connection while a link is restored.
Up to 192 hot swappable hard disk drives can be supported per system. Drives can be designated for use by an array in the event of hardware failure. Up to 32 separate RAID arrays can be supported per adaptor, and arrays can be mirrored across servers to provide cost-effective protection for critical applications. Furthermore, arrays can be sited up to 25 metres apart - connected by thin, low-cost copper cables - allowing subsystems to be located in secure, convenient locations, far from the server itself.
SSA was deployed in server RAID environments, where it was capable of providing for up to 80 Mbyte/s of data throughput, with sustained data rates as high as 60 Mbytes/s in non-RAID mode and 35 Mbytes/s in RAID mode.



The copper cables used in SSA configurations are round bundles of two or four twisted pairs, up to 25 metres long and terminated with 9-pin micro-D connectors. Impedances are 75 ohm single-ended, and 150 ohm differential. For longer-distance connections, it is possible to use fiber-optic cables up to 10 km (6 mi) in length. Signals are differential TTL. The transmission capacity is 20 megabytes per second in each direction per channel, with up to two channels per cable. The transport layer protocol is non-return-to-zero, with 8B/10B encoding (10 bits per character). Higher protocol layers were based on the SCSI-3 standard.



IBM 7133 Disk expansion enclosures
IBM 2105 Versatile Storage Server (VSS)
IBM 2105 Enterprise Storage Server (ESS)
IBM 7190 SBUS SSA Adapter
Pathlight Technology Streamline PCI Host Bus Adapter, SSA Data Pump, storage area network gateway



List of device bandwidths


