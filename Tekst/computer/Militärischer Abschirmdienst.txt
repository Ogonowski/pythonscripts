The Milit rischer Abschirmdienst (Military Counterintelligence Service, MAD) or more officially Amt f r den Milit rischen Abschirmdienst (in the past Amt f r die Sicherheit der Bundeswehr), is one of the three federal intelligence agencies in Germany and is responsible for military counterintelligence. The other two are the Bundesnachrichtendienst (Federal Intelligence Service, BND), which is the foreign intelligence agency, and the Bundesamt f r Verfassungsschutz (Federal Office for the Protection of the Constitution, BfV) which is the domestic civilian intelligence agency.
The headquarters of the MAD are in Cologne, with twelve groups located in cities throughout Germany. These MAD groups are collectively known to be the Milit rischer Abschirmdienst. The agency has about 1,300 military and civilian employees and in 1995 had an annual budget of 74 million euros.



The MAD is part of the Bundeswehr, the German armed forces. As a domestic intelligence service, it has similar functions within the military as the Bundesamt f r Verfassungsschutz and works closely together with the BfV. The main duties of the MAD are counterintelligence and detection of "anticonstitutional activities" within the Bundeswehr. Other duties include the protection of Bundeswehr properties from sabotage and foreign espionage. Members of the MAD are also involved in planning and construction of buildings with high security requirements. The MAD has no prosecution power. The lead agency for the German military intelligence operations as well as strategic defense-related intelligence is the Bundesverteidigungsministerium (Ministry of Defense) in Berlin.
The legal basis for the MAD is the MAD Law of December 20, 1990, as amended by Article 8 of the law of April 22, 2005.



As well as a department for administrative affairs, there are the following specialist departments:
Department I: Central services
Department II: Counter-extremism and counter-terrorism
Department III: Counterespionage and operative security
Department IV: Protection of secrets (personnel and material)
Department V: Technology
The 12 regional offices are in:
Amberg
Hannover
Hilden
Kiel
Koblenz
Leipzig
Mainz
Munich
Rostock
Schwielowsee
Stuttgart
Wilhelmshaven



The MAD developed out of a liaison office between the Allies and the German government and was founded in its present form in 1956, after the Bundeswehr was created. Until 1984, its headquarters was called "Amt f r Sicherheit der Bundeswehr" (ASBw, Federal armed forces office of security). As of September 1984, on the basis of the H cherl report, the service was restructured and more civilian positions were created.
The MAD had 7 groups and 28 regional offices after the former East German army, the NVA (the National People's Army), was incorporated into the Bundeswehr in October 1990. This was reduced to 14 offices in 1994 when there was a reduction of the armed forces.



The MAD has been involved in a number of scandals, one of them was the secret surveillance of the home of the secretary of then minister of defence Georg Leber. This was done without Mr. Leber's knowledge. His secretary was suspected of espionage for the East German Ministerium f r Staatssicherheit (Ministry of State Security or Stasi). The suspicions turned out to be false. Leber was informed of the illegal surveillance at the beginning of 1978 but did not inform the Bundestag until the magazine Quick published an article on 26 January 1978. Georg Leber retired his position on 16 February 1978 taking sole responsibility for the surveillance scandal. He resigned against the wishes of then chancellor Helmut Schmidt.
Another scandal was the Kie ling Affair in 1983, when the MAD investigated G nter Kie ling, a Bundeswehr four-star general working with NATO (Commander of NATO land forces and Deputy Supreme Allied Commander Europe.). The general was deemed to be a security risk based on allegations of homosexuality originating from questionable sources and was given early retirement by the then defence minister Manfred W rner. The general was later rehabilitated. The affair had significant consequences for the service: the commander was removed, and a commission was set up under the former minister of the interior Hermann H cherl (CSU). The H cherl Commission investigated the way in which the MAD operated and made recommendations for improvement. These recommendations were implemented speedily.






Milit rischer Abschirmdienst (German)
Geheimdienste.org (German)