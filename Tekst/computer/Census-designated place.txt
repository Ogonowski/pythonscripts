A census designated place (CDP) is a concentration of population identified by the United States Census Bureau for statistical purposes. CDPs are delineated for each decennial census as the statistical counterparts of incorporated places, such as cities, towns, and villages. CDPs are populated areas that lack separate municipal government, but which otherwise physically resemble incorporated places.
CDPs are delineated solely to provide data for settled concentrations of population that are identifiable by name but are not legally incorporated under the laws of the state in which they are located. They include small rural communities, colonias located along the U.S. border with Mexico, and unincorporated resort and retirement communities. The boundaries of a CDP have no legal status. Thus, they may not always correspond with the local understanding of the area or community with the same name. However, criteria established for the 2010 Census require that a CDP name "be one that is recognized and used in daily communication by the residents of the community" (not "a name developed solely for planning or other purposes") and recommend that a CDP's boundaries be mapped based on the geographic extent associated with residents' use of the place name.
The U.S. Census Bureau states that census designated places are not considered incorporated places and that it includes only census designated places in its city population list for Hawaii because that state has no incorporated cities. In addition, census city lists from 2007 include Arlington County, Virginia's CDP in the list with the incorporated places.



The Census Bureau reported data for some unincorporated places as early as the 1850 Census, though usage continued to develop through the 1890 Census, in which, for the first time, the Census mixed unincorporated places with incorporated places in its products with "town" or "village" as its label. This made it confusing to determine which of the "towns" were or were not incorporated.
The 1900 through 1930 Censuses did not report data for unincorporated places.
For the 1940 Census, the Census Bureau compiled a separate report of unofficial, unincorporated communities of 500 or more people. The Census Bureau officially defined this category as "unincorporated places" in the 1950 Census and used that term through the 1970 Census. For the 1950 Census, these types of places were identified only outside "urbanized areas". In 1960, the Census Bureau also identified unincorporated places inside urbanized areas (except in New England), but with a population of at least 10,000. For the 1970 Census, the population threshold for "unincorporated places" in urbanized areas was reduced to 5,000. For the 1980 Census, the designation was changed to "census designated places" and the designation was made available for places inside urbanized areas in New England. For the 1990 Census, the population threshold for CDPs in urbanized areas was reduced to 2,500. From 1950 through 1990, the Census Bureau specified other population requirements for unincorporated places or CDPs in Alaska, Puerto Rico, island areas, and Native American reservations. Minimum population criteria for CDPs were dropped with the 2000 Census.
The Census Bureau's Participant Statistical Areas Program (PSAP) allows designated participants to review and suggest modifications to the boundaries for CDPs. The PSAP was to be offered to county and municipal planning agencies during 2008.



The boundaries of such places may be defined in cooperation with local or tribal officials, but are not fixed, and do not affect the status of local government or incorporation; the territories thus defined are strictly statistical entities. CDP boundaries may change from one census to the next to reflect changes in settlement patterns. Further, as statistical entities, the boundaries of the CDP may not correspond with local understanding of the area with the same name. Recognized communities may be divided into two or more CDPs while on the other hand, two or more communities may be combined into one CDP. A CDP may also cover the unincorporated part of a named community where the rest lies within an incorporated place.
By defining an area as a CDP, that locality then appears in the same category of census data as incorporated places. This distinguishes CDPs from other census classifications, such as minor civil divisions (MCDs), which are in a separate category.
The population and demographics of the CDP are included in the data of county subdivisions containing the CDP. In no case is a CDP defined within the boundaries of what the Census Bureau regards to be an incorporated city, village or borough. However, the Census Bureau considers towns in New England states and New York as well as townships in some other states as MCDs, even though they are incorporated municipalities in those states. Thus, CDPs may be defined within New England towns or spanning the boundaries of multiple towns.



There are a number of reasons for the CDP designation:
The area may be more urban than its surroundings, having a concentration of population with a definite residential nucleus, such as Whitmore Lake, Michigan, or Hershey, Pennsylvania.
A formerly incorporated place may disincorporate or be partly annexed by a neighboring town, but the former town or a part of it may still be reported by the census as a CDP by meeting criteria for a CDP. Examples are the former village of Covedale (village in Ohio), compared with Covedale (CDP), Ohio or the recently disincorporated town of Cedar Grove, Florida.
The area may contain an easily recognizable institution, usually occupying a large land area, with an identity distinct from the surrounding community. This could apply to some college campuses & large military bases (or parts of a military base) that are not within the limits of any existing community, such as Notre Dame, Indiana, Stanford, California (which houses the Stanford University campus), Fort Campbell North, Kentucky and Fort Leonard Wood, Missouri.
In other cases, the boundary of an incorporated place may bisect a recognized community. An example of this is Bostonia, California, which straddles the city limits of El Cajon. The USGS places the nucleus of Bostonia within El Cajon. The Bostonia CDP covers the greater El Cajon area in unincorporated San Diego County that is generally north of that part of Bostonia within El Cajon.
The Census Bureau treats all townships as unincorporated places, even in those states where townships are incorporated under state law. This is so even in those states (i.e., Indiana, Michigan, Minnesota, New Jersey and South Dakota) where the Census Bureau acknowledges that "All townships are actively functioning governmental units."
In some states, a CDP may be defined within an incorporated municipality that (for the purposes of the census) is regarded as a minor civil division. For example, towns in Massachusetts and Connecticut are incorporated municipalities, but may also include both rural and urban areas. CDPs may be defined to describe urbanized areas within such municipalities, as in the case of North Amherst, Massachusetts.
Hawaii is the only state that has no incorporated places recognized by the U.S. Census Bureau below the county level. All data for places in Hawaii reported by the census are CDPs.
Few CDPs represent an aggregation of several nearby communities, for example Shorewood-Tower Hills-Harbert, Michigan or Egypt Lake-Leto, Florida. However, the Census Bureau has discontinued this method for most CDPs during the 2010 Census.
In rare cases, a CDP was also defined for the urbanized area surrounding an incorporated municipality, but which is outside the municipal boundaries, for example, Greater Galesburg, Michigan, or Greater Upper Marlboro, Maryland. This practice was discontinued in 2010.
In some states, the Census Bureau would designate an entire minor civil division (MCD) as a CDP (for example West Bloomfield Township, Michigan or Reading, Massachusetts). Such designations were used in states where the MCDs function with strong governmental authority and provide services equivalent to an incorporated municipality (New England, the Middle Atlantic States, Michigan, and Wisconsin). MCDs appear in a separate category in census data from places (i.e., incorporated places and CDPs); however, such MCDs strongly resemble incorporated places, and so CDPs coterminous with the MCDs were defined so that such places appear in both categories of census data. This practice was also discontinued in 2010.




Census county division
Populated Place, used by the United States Board on Geographic Names
Designated place, a counterpart in the Canadian census
Incorporated place
ZIP Code Tabulation Area






U.S. Census Bureau, Geography Division, "Cartographic Boundary Shapefiles   Places (Incorporated Places and Census Designated Places)". Cartographic Operations Branch, December 11, 2014.
U.S. Census Bureau, "Census 2000 Statistical Areas Boundary Criteria" at the Wayback Machine (archived November 15, 2012), Census Designated Places (CDPs)   Census 2000 Criteria.
U.S. Census Bureau, Geographic Areas Reference Manual, United States Department of Commerce.