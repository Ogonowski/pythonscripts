A self-propelled gun (SPG) is a form of self-propelled artillery, and in modern use is usually used to refer to artillery pieces such as howitzers.
Self-propelled guns are mounted on a motorized wheeled or tracked chassis. As such the gun can be maneuvered under its own power as opposed to a towed gun that relies upon a vehicle or other means to be moved on the battlefield. Self-propelled guns are combat support weapons; they are employed by combat support units fighting in support of, or attached to, the main combat units: infantry and armour. Self-propelled guns are best at providing indirect fire but can give direct fire when needed. It may be armoured, in which case it is considered an armoured fighting vehicle (AFV). Typically, self-propelled guns are more lightly armoured and may not have turrets and their purpose is distinct from that of tanks.
The greatest tactical advantage in the case of artillery guns is clearly the greater degree of mobility they have compared to towed artillery. Not only is it important in offering military forces greater flexibility, but it is critical in avoiding attack from the enemy (counter-battery fire) by allowing the guns to change position immediately after firing one or more salvos and before their position can be located ("shoot-and-scoot" tactics). A secondary advantage in the case of armoured   even lightly   guns is the increased protection offered to the gun crews.




The first attempts to give artillery a greater degree of maneuverability was in World War I. Although mechanical tractors had been used to tow some artillery, most were still towed by horses. The Gun Carrier Mark I was an artillery piece that was transported by and could be fired from a tracked chassis.
Between the wars, in the development of their armoured warfare tactics, the British put the Birch Gun into limited use. It carried an 18 pounder gun on a chassis derived from their then medium tank and as such was able to keep up and cross the same ground as the tanks it was intended to support. As well as use as a field gun, the gun could be elevated sufficiently for use against aircraft.




Self-propelled guns and howitzers are used in the same way as their towed variety, generally for long-range bombardment. Self-propelled artillery can however also include other types of weapons not considered a self-propelled gun, one example of which would be rocket artillery.




Assault guns are large-caliber artillery pieces, meant to support infantry by direct fire with high explosive ammunition.



See List of Self-propelled howitzers


