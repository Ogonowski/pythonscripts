MSCDEX or Microsoft MS-DOS CD-ROM Extensions is a software program produced by Microsoft and included with MS-DOS 6.x and certain versions of Microsoft Windows. Earlier versions of MSCDEX were installable add-ons beginning with MSDOS 3.1.
It is a driver executable which allows DOS programs to recognize, read, and control CD-ROMs using the ISO 9660 file system. This requires the previous loading of an appropriate CD-ROM device driver (example: OAKCDROM.SYS), usually from CONFIG.SYS. The program was used up until Windows 95 when it was replaced by the 32-bit version CDFS.
The final version of the MSCDEX program was 2.96, included with Windows 95 and used when creating bootable floppy disks with CD-ROM support.
A cloaked variant of MSCDEX was provided as part of Helix Software's Multimedia Cloaking product. It uses Cloaking to relocate and run in protected mode on 386 processors.
The driver uses the Microsoft Networks interface in MS-DOS. This is the reason that at least version 3.1 of MS-DOS is required. The driver essentially looks like a network drive from the system perspective. It's implemented as a TSR program and an extension to the redirector interface (CDEX).



Novell DOS 7, Caldera OpenDOS 7.01 and DR-DOS 7.02 and higher provide a functional equivalent to MSCDEX named NWCDEX, which also runs under MS-DOS and PC DOS. It has more flexible load-high capabilities, also allowing to relocate and run in protected mode through DPMS on 286 and higher processors, thereby leaving only a 7 KB stub in conventional or upper memory (in comparison to MSCDEX, which occupies some 16 KB). Using EMS with a page frame, NWCDEX can reduce its footprint even down to a few bytes in conventional memory. In contrast to MSCDEX, the driver does not depend on undocumented DOS APIs and therefore, with a third-party helper tool named INSTCDEX, can be loaded via INSTALL statements and be fully functional in CONFIG.SYS to increase chances to load the driver high and, under these operating systems, allow to load other drivers not only from hard disk but also from CD-ROM while the operating system is still processing CONFIG.SYS.
Based on NWCDEX, IMS REAL/32, a successor to Novell's Multiuser DOS and Digital Research's Concurrent DOS, provides a similar driver named IMSCDEX.
There's a free alternative called SHSUCDX that is used with the IDE/ATA driver UIDE.SYS first released in 2005 that is often used with FreeDOS and works with MS-DOS as well.
In 1998 Caldera provided a DRFAT32 driver for DR-DOS to dynamically mount and unmount FAT32 volumes on DOS versions otherwise not natively supporting FAT32. DRFAT32 uses a variation and extension of the CDEX API in order to achieve this and work with older DOS versions.
Corel also offered CORELCDX.COM as their alternative.






Command-Line Switches for MSCDEX.EXE
MS-DOS CD-ROM Extensions 2.2 Information Pack (self-extracting archive, includes Microsoft MS-DOS CD-ROM Extensions Hardware-Dependent Device Driver Specification)
Examples of use in CONFIG.SYS