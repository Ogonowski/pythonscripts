In a motor vehicle, the term powertrain or powerplant describes the main components that generate power and deliver it to the road surface, water, or air. This includes the engine, transmission, drive shafts, differentials, and the final drive (drive wheels, continuous track as in military tanks or caterpillar tractors, propeller, etc.). Sometimes "powertrain" is used to refer to simply the engine and transmission, including the other components only if they are integral to the transmission.
A motor vehicle's driveline or drivetrain consists of the parts of the powertrain excluding the engine and transmission. It is the portion of a vehicle, after the transmission, that changes depending on whether a vehicle is front-wheel, rear-wheel, or four-wheel drive, or less-common six-wheel or eight-wheel drive.
In a wider sense, the power-train includes all of its components used to transform stored (chemical, solar, nuclear, kinetic, potential, etc.) energy into kinetic energy for propulsion purposes. This includes the utilization of multiple power sources and non wheel-based vehicles.



Powertrain development for diesel engines involves the following: exhaust gas recirculation (EGR), and advanced combustion. Spark ignition engine development include: fuel injection, including the gasoline direct injection variant, as well as improving volumetric efficiency by using multi-valves per cylinder, variable valve timing, variable length intake manifolds, and turbocharging. Changes also include new fuel qualities (no sulphur and aromates) to allow new combustion concepts. So-called "combined combustion systems" (CCV) or "diesotto" cycles are based on synthetic fuels (synthetic diesel, biomass to liquid (BTL) or gas to liquid (GTL)).
BEVs, FCEVs and PHEV powertrains are expected to reach parity with ICE powertrains in 2025.



The manufacturing of powertrain components and systems is important to industry, including the automotive and other vehicle sectors. Competitiveness drives companies to engineer and produce powertrain systems that over time are more economical to manufacture, higher in product quality and reliability, higher in performance, more fuel efficient, less polluting, and longer in life expectancy. In turn these requirements have led to designs involving higher internal pressures, greater instantaneous forces, and increased complexity of design and mechanical operation. The resulting designs in turn impose significantly more severe requirements on parts shape and dimension; and material surface flatness, waviness, roughness, and porosity. Quality control over these parameters is achieved through metrology technology applied to all of the steps in powertrain manufacturing processes.



In automotive manufacturing, the frame plus the "running gear" (powertrain) makes the chassis.
Later, a body (sometimes referred to as "coachwork"), which is usually not necessary for integrity of the structure, is built on the chassis to complete the vehicle. Commercial vehicle manufacturers may have "chassis only" and "cowl and chassis" versions that can be outfitted with specialized bodies. These include buses, motor homes, fire engines, ambulances, etc.
The frame plus the body makes a glider (a vehicle without a drivetrain).



The final drive is the last in the set of components which delivers torque to the drive wheels. In a road vehicle, it incorporates the differential. In a railway vehicle, it sometimes incorporates the reversing gear. Examples include the Self-Changing Gears RF 28 (used in many first-generation diesel multiple units of British Railways) and RF 11 used in the British Rail Class 03 and British Rail Class 04 diesel shunting locomotives.


