IEEE Spectrum is a magazine edited by the Institute of Electrical and Electronics Engineers. The IEEE's description of it is:
IEEE Spectrum is the flagship magazine and website of the IEEE, the world s largest professional organization devoted to engineering and the applied sciences. Our charter is to keep over 400,000 members informed about major trends and developments in technology, engineering, and science. Our blogs, podcasts, news and features stories, videos and interactive infographics engage our visitors with clear explanations about emerging concepts and developments with details they can t get elsewhere.
IEEE Spectrum began publishing in January 1964 as a successor to Electrical Engineering. It contains peer-reviewed articles pertaining to technology and science trends affecting business and society, with a scope that covers information pertaining to electrical and electronics engineering, mechanical and civil engineering, computer science, biology, physics and mathematics. Additional content is gleaned from several hundred annual international conferences.
As a general magazine, the articles attempt to be accessible to non-specialists, though an engineering background is assumed. Twelve issues are published annually, and IEEE Spectrum has a circulation of over 380,000 engineers worldwide, making it one of the leading science and engineering magazines.
Article submission to IEEE Spectrum is open access. Individuals and corporations have the right to post their IEEE-copyrighted materials on their own servers without express permission.
In 2010, IEEE Spectrum was the recipient of Utne Reader magazine's Utne Independent Press Award for Science/Technology Coverage. In 2012 IEEE Spectrum was selected as the winner of the National Magazine Awards  General Excellence Among Thought Leader Magazines  category.






Official website