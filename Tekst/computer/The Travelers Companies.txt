The Travelers Companies is an American insurance company. It is the second largest writer of U.S. commercial property casualty insurance and the third largest writer of U.S. personal insurance through independent agents. Travelers is incorporated in Minnesota, with headquarters in New York City and its largest office in Hartford, Connecticut. Travelers also maintains a large office in St. Paul, Minnesota. It has been a component of the Dow Jones Industrial Average since June 8, 2009.
The company has field offices in every U.S. state, plus operations in the United Kingdom, Ireland, Singapore, China, Canada, and Brazil. In 2014, the company reported revenues of US $27 billion and total assets of US $103 billion.
Travelers, through its subsidiaries and approximately 14,000 independent agents and brokers, provides commercial and personal property and casualty insurance products and services to businesses, government units, associations, and individuals. The company offers insurance through three segments:
Personal Insurance, which includes home, auto and other insurance products for individuals
Business Insurance, which includes a broad array of property and casualty insurance and insurance-related services in the United States
Bond and Specialty Insurance, which includes surety, crime, and financial liability businesses which primarily use credit-based underwriting processes, as well as property and casualty products that are predominantly marketed on an international basis.



The Travelers Companies' Management Team is composed of:




The main predecessor companies of The Travelers Companies, Inc. are The St. Paul Companies, Inc. and Travelers Property Casualty Corporation.
Saint Paul Fire and Marine Insurance Co. was founded March 5, 1853, in St. Paul, Minnesota, serving local customers who were having a difficult time getting claim payments in a timely manner from insurance companies on the east coast of the United States. It barely survived the Panic of 1857 by dramatically paring down its operations and later reorganizing itself into a stock company (as opposed to a mutual company). It soon spread its operations across the country. In 1998 it acquired USF&G, known formerly as United States Fidelity and Guaranty Company, an insurance company based in Baltimore, Maryland, but was forced to downsize by almost half due to a competitive marketplace.
Travelers was founded in 1864 in Hartford. It was originally founded to provide travel insurance to railroad travelers at a time when travel was far more risky and dangerous than today, hence the name. Along the way it had many industry firsts, including the first automobile policy, the first commercial airline policy, and the first policy for space travel. By the early 1990s, Travelers was predominantly a general property and casualty insurer that also happened to do some travel insurance on the side, and it quietly exited its original business in 1993. What was left of Travelers' travel insurance business was acquired by a private entrepreneur and is now known as Travel Insured International.

In the 1990s, it went through a series of mergers and acquisitions. It was bought by Primerica in 1993, but the resulting company retained the Travelers name. In 1995 it became The Travelers Group. It bought Aetna's property and casualty business in 1996.
In 1998, the Travelers Group merged with Citicorp to form Citigroup. However, the synergies between the banking and insurance arms of the company did not work as well as planned, so Citigroup spun off Travelers Property and Casualty into a subsidiary company in 2002, although it kept the red umbrella logo. Three years later, Citigroup sold Travelers Life & Annuity to MetLife. In 2003, Travelers bought renewal rights for Royal & SunAlliance Personal Insurance and Commercial businesses.
In 2004, the St. Paul and Travelers Companies merged and renamed itself St. Paul Travelers, with the headquarters set in St. Paul, Minnesota. In August of that year, it was charged of misleading statements associated with the merger. Despite many assurances from CEO Jay Fishman that the newly formed company would retain the St. Paul name, the corporate name only lasted until 2007, when the company repurchased the rights to the famous red umbrella logo from Citigroup and readopted it as its main corporate symbol, while also changing the corporate name to The Travelers Companies.
Notably, many of Travelers' ancestor companies, such as St. Paul and USF&G, are still around today and still write policies and accept claims, but only on paper. As is typical of most insurers in the United States, Travelers never dissolved the various companies it acquired, but simply made them wholly owned subsidiaries and trained its employees to act on behalf of those subsidiaries. This is a common risk management strategy used by U.S. insurance groups. If any one company in the group gets hit with too many claims, the situation can be easily contained to that one company (which is placed in runoff and allowed to run its policies to completion), while the remainder of the group continues to operate normally.
Travelers is currently 116 on the Fortune 500 list of largest U.S. companies. On June 8, 2009, Travelers replaced its former parent Citigroup on the Dow Jones Industrial Average.
On August 4, 2015 the company announced that Alan Schnitzer would succeed Jay Fishman as Chief Executive Officer effective December 1, 2015.



In January 2007, Travelers agreed to pay US$77 million to six states to settle a class action suit and end investigations into its insurance practices. The charges involved paying the insurance broker Marsh & McLennan Companies contingent commissions to win business without the knowledge of clients, thus creating a conflict of interest. Additionally, the investigation examined whether Travelers had created the illusion of competition by submitting fake bids, thus misleading clients into believing they were receiving competitive commercial premiums.



In August 2012, Travelers sued the National Football League for forcing the company and its subsidiaries to pay to defend the league for failing to protect players from brain injury, in a case filed in the New York State Supreme Court called Discover Property & Casualty Co. et al. vs. National Football League et al., New York State Supreme Court, New York County, No. 652933/2012. The league had sued over three dozen insurance companies the week before in an attempt to cover up the claims that players made against the league.




The original logo of the red umbrella started in 1870 when it appeared in a newspaper ad for the young insurance company. It was revived in the early 1960s, when it was given its signature red color by Harry W. Knettell, then the account executive for The Travelers and Vice President at the Charles Brunelle advertising agency. During the late 1960s Charles Brunelle was the largest advertising agency in Hartford, a city known as "the insurance capital of the world" due to the many insurance companies in that town. The Travelers was one of their many insurance company clients.
In 2006, a Travelers commercial titled Snowball was nominated for an Emmy. Snowball featured a man, walking down a steep San Francisco sidewalk, who trips and knocks over a table of items at a garage sale. The man and the items roll down the street, forming a ball which gathers garbage cans, pedestrians, construction materials, motorcycles, light poles, and other items, in a manner very reminiscent of the familiar cartoon "snowball" effect or the cult video game Katamari Damacy. The creators of the ad say it is simply based on the snowball effect, they have never heard of the game, and that the resulting similarity was a surprise to them.
In 2007, the company secured naming rights for an annual event to be called the Travelers Championship golf tournament, formerly the Greater Hartford Open, on the PGA Tour.
In April 2008, The Travelers purchased back the rights to the famous red umbrella, which is featured in several commercials starring Paul Freeman as well as other advertisements. In July 2008, the spot "Delivery," also starring Freeman, was nominated for an Emmy.



Edward Budd, former Travelers Chairman & CEO
Robert I. Lipp, former Travelers CEO
Joe Plumeri, former Travelers Vice Chairman, former Chairman & CEO of Willis Group Holdings, and owner of the Trenton Thunder
Sandy Weill, former Travelers Chairman & CEO






Company home page