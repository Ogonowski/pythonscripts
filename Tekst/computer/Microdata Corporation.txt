Microdata Corporation was an Irvine, California-based computer company, developing hardware and operating systems to run its Reality environment. It later was taken over by its international distributor CMC Leasings, which in turn was taken over in 1983 by McDonnell Douglas Corporation, and is now part of Northgate Information Solutions.



The Reality name was given to Microdata's computer system and the operating system   the first version of what was later called the Pick operating system. At the time, Microdata owned rights to the Pick operating system in parallel with Dick Pick who developed it with Microdata in the early 1970s. As at 2009, Reality was still owned by the same lineage of companies   from Microdata, McDonnell Douglas Information Systems, MDIS to the current Northgate Information Solutions.
The original machine was a Microdata 800 microcomputer first made in 1969. This computer was licensed to the French company Intertechnique who sold it in Europe under the name Multi-8. It was particularly in use in nuclear power stations, research applications (such as crystallography and biology) and process control. Later this evolved into the Microdata 1600, an updated version of the 800 processor (commercialized under the name Multi-4 by Intertechnique). The original development of Reality was done on Microdata 800. The first Reality system was based on the 1600 and was sold commercially in 1973. The unique feature of the early Microdata processors was that the microcode was accessible to the user and allowed the creation of custom assembler level instructions. The Reality system design made extensive use of this capability.
The Microdata 3200 was developed in 1974 and was a 16 bit microprogrammed system designed to implement a high level language similar to IBM's PL/I language. It was designed to a more specific purpose, but still retained a great deal of flexibility in the firmware to allow for very complex microprogrammed architectures to be supported.



Dick Elleray (16 July 1986). "Project Management Bulletin 1986/09 - "The Reality Operating System Revealed"" (paper original). 1986/09. Project Management Group, McDonnell Douglas Informations Systems Group.  
John Luttrell (Microdata 1979 - Novadyne 1994). "Microdata Alumni website - Microdata-Alumni.org". Microdata Alumni website - history of Microdata, McDonnell Douglas Information Systems Group, and Novadyne Computers Systems, Inc. Includes photos, video, articles, literature, etc. 
Reiter, Reinhold ; Sladkovic, Rudolf ; Carnuth, Walter Atmospheric Aerosols between 700 and 3000 m above Sea Level. Part V. A Study of the Effects of Atmospheric Fine Structure Characteristics on the Vertical Distribution of Aerosols a technical report of the Fraunhofer-Gesellschaft on atmospheric physics where a Multi-8 was used for performing computations (July 1971)



Microdata-Alumni



Some manuals of the Microdata 800
Some manuals of the Microdata 1600
Some manuals of the Microdata 3200



Some pictures of the Intertechnique Multi-8 clone of the Microdata 800
Some pictures of the Intertechnique Multi-4 clone of the Microdata 1600