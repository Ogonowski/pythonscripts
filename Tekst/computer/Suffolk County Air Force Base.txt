Francis S. Gabreski Air National Guard Base (Suffolk County Army Air Field in World War II) is a former air defense military installation collocated to use runways with the Westhampton, New York, municipal airport. Some of the facilities and real estate of Suffolk County AFB, which closed in 1969, are now part of the renamed Francis S. Gabreski Airport, while other former Air Force facilities, as well as new military construction, are used by the New York Air National Guard's 106th Rescue Wing (106 RQW) stationed at Francis S. Gabreski Air National Guard Base.



The Suffolk County Army Air Field was built in 1943 as a United States Army Air Forces sub-base of nearby Mitchel Field. Later assigned to First Air Force, the 437th Army Air Force Base Unit defended the New York City area and flew antisubmarine patrols. Post-war, the airfield was conveyed to Suffolk County for use as a civilian airport, but to include a reversal clause if future military use was ever needed.
Renamed when the United States Air Force reclaimed the airport in 1951, Suffolk County Air Force Base was part of the Eastern Air Defense Force's defense of the NYC metro area. The Air National Guard's 103rd Fighter-Interceptor Wing (103 FIW), along with the 118th Fighter-Interceptor Squadron and its F-47N Thunderbolts, was federalized on March 2, 1951 and moved from Brainard Field, Connecticut, to Suffolk County AFB on June 1. The 103 FIW was returned to state control on February 6, 1952 and remained briefly at Suffolk County until replaced by the 45th and 75th Fighter-Interceptor Squadrons in November 1952, flying the F-86 Sabre as part of the 23rd Fighter-Interceptor Group (23 FIG).
In 1955, the 23 FIG was reassigned to Presque Isle AFB, Maine and replaced by the newly activated 52nd Fighter-Interceptor Wing (52 FIW), which flew under various designations from Suffolk County AFB until 1969, with the 2d and 5th Fighter-Interceptor Squadrons flying F-94 Starfire, F-101 Voodoo and F-102 Delta Dagger interceptors. In 1963, the 52 FIW was renamed the 52nd Fighter Wing (Air Defense) and became part of the New York Air Defense Sector (NY ADS), the NY ADS being one of four USAF air defense sectors employing the Semi-Automatic Ground Environment (SAGE) system.
Suffolk County AFB was also the main support base for the Suffolk County Missile Annex, a nearby USAF CIM-10 Bomarc surface-to-air missile launch complex for the defense of the New York City metropolitan area under the control of a missile launch control center at McGuire Air Force Base, New Jersey.
As a result of funding shortfalls for the Vietnam War that resulted in the closure of numerous stateside air force bases and naval air stations, Suffolk County AFB deactivated in 1969 and the military installation was again transferred to the Suffolk County government for use as a civilian airport.



Suffolk County Airport (Francis S. Gabreski Airport after 1991) operated without a military unit until June 1970 when the 102nd Air Refueling Squadron of the New York Air National Guard relocated from Naval Air Station Floyd Bennett Field with its KC-97 Stratofreighters. In 1972, the 102d switched to F-102 Delta Daggers and became the 102d Fighter-Interceptor Squadron. In 1975, the unit had another mission change and became an Aerospace Rescue and Recovery squadron, later renamed the 102nd Rescue Squadron in 1995. Today, the 106th Rescue Wing uses HC-130P Hercules and HH-60G Pave Hawk helicopters for both peacetime and combat search and rescue.
The book The Perfect Storm and the film by the same name detail the crash of one of the 106th's HH-60G Pave Hawk rescue helicopters while conducting civilian search and rescue operations from the airport during the 1991 Perfect Storm.





