Blaise Pascal was the inventor of the mechanical calculator in the early 17th century. Pascal designed the machine in 1642. He was spurred to it when participating in the burden of arithmetical labor involved in his father's official work as supervisor of taxes at Rouen. First called the Arithmetic Machine, Pascal's Calculator and later Pascaline, his invention was primarily intended as an adding machine which could add and subtract two numbers directly although its description could be extended to a "mechanical calculator with which it was possible to multiply and divide by repetition."
Pascal went through 50 prototypes before presenting his first machine to the public in 1645. He dedicated it to Pierre S guier, the chancellor of France at the time. He built around twenty more machines during the next decade, often improving on his original design. Nine machines have survived the centuries, most of them being on display in European museums. In 1649 a royal privilege, signed by Louis XIV of France, gave him the exclusivity of the design and manufacturing of calculating machines ...
Pascal designed the first mechanical calculator to still survive from the 17th century. His device was particularly successful in the smooth working of the so-called "carry mechanism" the mechanism which allows an addition of 1 to 9 on one dial to replace the 9 with a 0, and carry 1 to the next dial. His innovation made each digit independent of the state of the others allowing for multiple carries to rapidly cascade from one digit to another regardless of the capacity of the machine. Pascal was also the first person to shrink and adapt for his purpose a lantern gear, used in turret clocks and water wheels, which was capable of resisting the strength of any operator input with very little added friction.
Pascal was also influential since he sought to commercialise his machine (albeit with only limited success). Whilst other forms of calculation (using the abacus, calculi, counting boards and other means of ready reckoning) were in widespread use, Pascal's machine symbolised a step forward compatible with the increasing interest in using machines to displace tedious work. In this sense his invention was an important step in the development of mechanical calculators first in Europe and then all over the world. Development which culminated, three centuries later, in the invention of the microprocessor developed for a Busicom calculator in 1971 ; the microprocessor is now at the heart of all computers and embedded systems.
Other later developments often reflected aspects of the Pascaline, either because they were actually inspired by it or at least shaped by the same historical influences which led Pascal to develop his invention. Gottfried Leibniz invented his Leibniz wheels after 1671 after trying to add an automatic multiplication feature to the Pascaline. In 1820, Thomas de Colmar designed his arithmometer which, whilst it is not at all clear he ever saw Leibniz's device, either re-invented or utilised Leibniz's invention of the step drum. The arithmometer was the first mechanical calculator strong enough and reliable enough to be used daily in an office environment.









The abacus, probably invented in Sumer between 2700 and 2300 BC, is an early computing device invented by humans. Whilst it has no automatically moving parts or gears the movement of its beads on its structured rods provides a powerful mechanism for carrying out all four arithmetic operations. As late as the middle of the last century the speed of these on the abacus could rival or exceed the speed of the same computations performed on the latest electric calculating machines. Over centuries, support for arithmetic was provided either by the abacus, or other counting technologies (such as the counting board and calculi and "the pen and jeton"). In 1617 John Napier built on a known method of "lattice multiplication" to publish his "rods" or Napier's Bones which, appearing in a range of forms over subsequent centuries, were found to considerably facilitate the performance of multiplication and division, especially by those not well equipped with memorised multiplication tables.




A short list of other precursors to the mechanical calculator must include the Greek Antikythera mechanism from around 100 BC, an out of place, unique, geared mechanism followed more than a millennium later by early mechanical clocks and geared astrolabes ; These machines were all made of toothed gears linked by some sort of carry mechanisms. They belong to a group of mechanical analog computers which, once set, are only modified by the continuous and repeated action of their actuators (crank handle, weight, wheel...)
Some measuring instruments and automatons were also precursors to the calculating machine.

An odometer, instrument for measuring distances, was first described around 25 BC by the Roman engineer Vitruvius in the tenth volume of his De architectura. It was made of a set of toothed gears linked by a carry mechanism ; the first one was driven by one of the chariot wheels and the last one dropped a small pebble in a bag for each Roman mile traveled.
A Chinese text of the third century AD described a chariot equipped with a geared mechanism that operated two wooden figures. One would strike a drum for every Chinese Li traveled, the other one would strike a gong for every ten Li traveled.
Around the end of the tenth century, the French monk Gerbert d'Aurillac, whose abacus taught the Hindu-Arabic numeral system to the Europeans, brought back from Spain the drawings of a brazen head, invented by the Moors, which answered Yes or No to the questions it was asked (binary arithmetic).
Again in the thirteenth century, the monks Albertus Magnus and Roger Bacon built mechanical speaking heads made of earthware without any further development (Albertus Magnus complained that he had wasted forty years of his life when Thomas Aquinas, terrified by this speaking machine, destroyed it).
The Italian polymath Leonardo da Vinci drew an odometer before 1519.
In 1525, the French craftsman Jean Fernel built the first pedometer. It was made in the shape of a watch and had 4 dials (units, tens, hundreds, thousands) linked by a single tooth carry mechanism.
In 1623 and 1624, Wilhelm Schickard, in two letters that he sent to Kepler, reported his design and construction of what he referred to as an  arithmeticum organum  ( arithmetical instrument ) that he has invented, but also described as a Rechen Uhr (calculating clock) in his note to an artisan constructing it, and which was able to assist all four operations of arithmetic. The notes on Schickard's invention did not provide full details of mechanism, and based on what was shown may well have had limitations in the effectiveness of its operation (discussed below). Other designs for calculating machines that followed in the 17th century included the very different and in some ways superior (but in other ways less ambitious) conception by Pascal (discussed below) and the designs of Tito Burattini, Samuel Morland and Ren  Grillet). A later more sophisticated calculating clock was built by the Italian Giovanni Poleni in the 18th century (1709). It was a two-motion calculating clock were the numbers are inscribed first and then are processed (see Pascal versus Schickard).




Pascal began to work on his calculator in 1642, when he was only 19 years old. He had been assisting his father, who worked as a tax commissioner, and sought to produce a device which could reduce some of his workload. Pascal received a Royal Privilege in 1649 that granted him exclusive rights to make and sell calculating machines in France. By 1654 Pascal had sold about twenty machines, but the cost and complexity of the Pascaline was a barrier to further sales, and production ceased in that year. By that time Pascal had moved on to the study of religion and philosophy which gave us both the Lettres provinciales and the Pens es.
Besides being the first calculating machine made public during its time, the pascaline is also:
the only operational mechanical calculator in the 17th century.
the first calculator to have a controlled carry mechanism which allowed for an effective propagation of multiple carries
the first calculator to be used in an office (his father's to compute taxes)
the first calculator commercialized (with around twenty machines built)
the first calculator to be patented (royal privilege of 1649)
the first calculator to be described in an encyclopaedia (Diderot & d'Alembert, 1751):

"...The first arithmetic machine presented to the public was from Blaise Pascal, born in Clermont, Auvergne on June 19, 1623 ; he invented it at the age of 19. Other machines have been designed since which, in the judgement of Mr of the Academy of Sciences, seem to have more practical advantages ; but Pascal's machine is the oldest one ; it could have served as model to all the others ; this is why we preferred it."

the first calculator sold by a distributor:

"Mr de Roberval ... located in the college Maitres Gervais ... every morning until 8..."







Gottfried Leibniz started to work on his own calculator after Pascal's death. He first tried to build a machine that could multiply automatically while sitting on top of the Pascaline, assuming (wrongly) that all the dials on Pascal's calculator could be operated at the same time. Even though it couldn't be done, this was the first time that a pinwheel was described and used in the drawings of a calculator.
He then devised a competing design, the Stepped Reckoner which was meant to perform additions, subtractions and multiplications automatically and division under operator control; Leibniz struggled for forty years to perfect this design and produced two machines, one in 1694 and one in 1706. Only the machine built in 1694 is known to exist; it was rediscovered at the end of the 19th century, having spent 250 years forgotten in an attic in the University of G ttingen.
The German calculating-machine inventor Arthur Burkhardt was asked to put this machine in operating condition if possible. His report was favorable except for the sequence in the carry. and "therefore, especially in the case of multiple carry transfers, the operator had to check the result and manually correct the possible errors". Leibniz had not succeeded in creating a calculator that worked properly, but he had invented his Leibniz wheel, the principle of a two motion mechanical calculator. He was also the first to have cursors to inscribe the first operand and a movable carriage for results.
There were five additional attempts at designing direct entry calculating machines in the 17th century (including the designs of Tito Burattini, Samuel Morland and Ren  Grillet)).
Claude Perrault designed an "Abaque Rhabdologique" around 1660 which is often mistaken for a mechanical calculator because it has a carry mechanism in between the numbers. But it is an abacus, because it requires the operator to handle the machine differently when a carry transfer takes place.
Pascal's calculator was the most successful mechanical calculator for addition and subtraction of large numbers, developed in the 17th century.
Calculating machines did not become commercially viable until 1851, when Thomas de Colmar released, after thirty years of development, his simplified Arithmometer, the first machine strong enough to be used daily in an office environment. The Arithmometer was designed around Leibniz wheels and initially used Pascal's 9's complement method for subtractions.



Pascal tercentenary celebration of his invention of the mechanical calculator happened during WWII when France was occupied by Germany and therefore the main celebration was held in London, England. Some of the speeches given best describe his achievement:

...when he invented the calculating machine essentially a practical instrument Pascal was already well known in the realm of pure mathematics. His perseverance with this invention helped one to appreciate more fully the character of Pascal. He had to fight not only ill-health but also the ignorance of his time, for his conception far out-stripped the mechanical experience and ability of those to whom the work was entrusted. It was not until Pascal had made more than fifty models that he achieved his final design.
The invention of the calculating machine illustrated Pascal's extraordinary creative imagination, allied with mathematical genius and precision, and tempered with critical penetration. These qualities were characteristic of the man throughout his life.

Pascal's invention of the calculating machine, just three hundred years ago, was made while he was a youth of nineteen. He was spurred to it by seeing the burden of arithmetical labor involved in his father's official work as supervisor of taxes at Rouen. He conceived the idea of doing the work mechanically, and developed a design appropriate for this purpose ; showing herein the same combination of pure science and mechanical genius that characterized his whole life. But it was one thing to conceive and design the machine, and another to get it made and put into use. Here were needed those practical gifts that he displayed later in his inventions....
In a sense, Pascal's invention was premature, in that the mechanical arts in his time were not sufficiently advanced to enable his machine to be made at an economic price, with the accuracy and strength needed for reasonably long use. This difficulty was not overcome until well on into the nineteenth century, by which time also a renewed stimulus to invention was given by the need for many kinds of calculation more intricate than those considered by Pascal.



Pascalines came in both decimal and non-decimal varieties, both of which exist in museums today. They were designed to be used by scientists, accountants and surveyors. The simplest Pascaline had five dials ; later production variants had up to ten dials.
The contemporary French currency system used livres, sols and deniers with 20 sols to a livre and 12 deniers to a sol. Length was measured in toises, pieds, pouces and lignes with 6 pieds to a toise, 12 pouces to a pied and 12 lignes to a pouce. Therefore, the pascaline needed wheels in base 6, 10, 12 and 20. Non decimal wheels were always located before the decimal part.
In an accounting machine (..10,10,20,12), the decimal part counted the number of livres (20 sols), sols (12 deniers) and deniers. In a surveyor's machine (..10,10,6,12,12), the decimal part counted the number of toises (6 pieds), pieds (12 pouces), pouces (12 lignes) and lignes. Scientific machines just had decimal wheels.

The metric system was adopted in France on December 10, 1799 by which time Pascal's basic design had inspired other craftsmen, although with a similar lack of commercial success.



Most of the machines that have survived the centuries are of the accounting type. Seven of them are in European museums, one belongs to the IBM corporation and one is in private hands.






The calculator had spoked metal wheel dials, with the digit 0 through 9 displayed around the circumference of each wheel. To input a digit, the user placed a stylus in the corresponding space between the spokes, and turned the dial until a metal stop at the bottom was reached, similar to the way a rotary telephone dial is used. This would display the number in the display windows at the top of the calculator. Then, one would simply redial the second number to be added, causing the sum of both numbers to appear in the accumulator.
Each dial is associated with a one-digit display window located directly above it. It displays the value of the accumulator for this position. The complement of this digit, in the base of the wheel (6, 10, 12, 20), is displayed just above this digit. A horizontal bar hides either all the complement numbers when it is slid to the top or all the direct numbers when it is slid toward the center of the machine and thereby displays either the content of the accumulator or the complement of its value.
Since the gears of the calculator rotated in only one direction, negative numbers could not be directly summed. To subtract one number from another, the method of nine's complement was used. The only two differences between an addition and a subtraction are the position of the display bar (direct versus complement) and the way the first number is entered (direct versus complement).



For a 10-digit wheel (N), the fixed outside wheel is numbered from 0 to 9 (N-1). The numbers are inscribed in a decreasing manner clockwise going from the bottom left to the bottom right of the stop lever. To add a 5, one must insert a stylus between the spokes that surround the number 5 and rotate the wheel clockwise all the way to the stop lever. The number displayed on the corresponding display register will be increased by 5 and, if a carry transfer takes place, the display register to the left of it will be increased by 1. To add 50, use the tens input wheel (second dial from the right on a decimal machine), to add 500, use the hundreds input wheel, etc...



On all the wheels of all the known machines, except for the machine tardive, two adjacent spokes are marked ; these marks differ from machine to machine, on the wheel pictured on the right, they are drilled dots, on the surveying machine they are carved, some are just scratches or marks made with a bit of varnish, some were even marked with little pieces of paper.
These marks are used to set the corresponding cylinder to its maximum number, ready to be re-zeroed. To do so, the operator must insert the stylus in between these two spokes and turn the wheel all the way to the stopping lever. This works because each wheel is directly linked to its corresponding display cylinder (it automatically turns by one during a carry operation) ; to mark the spokes during manufacturing, one can move the cylinder so that its highest number is displayed and then mark the spoke under the stopping lever and the one to the right of it.



Four of the known machines have inner wheels of complements; they were used to enter the first operand in a subtraction. They are mounted at the center of each spoked metal wheel and turn with it. The wheel displayed in the picture above has an inner wheel of complements but the numbers written on it are barely visible. On a decimal machine, the digits 0 through 9 are carved clockwise, with each digit positioned between two spokes so that the operator can directly inscribe its value in the window of complements by positioning his stylus in between them and turning the wheel clockwise all the way to the stop lever. The marks on two adjacent spokes flank the digit 0 inscribed on this wheel.



On four of the known machines, above each wheel, a small quotient wheel is mounted on the display bar. These quotient wheels, which are set by the operator, have numbers from 1 to 10 inscribed clockwise on their peripheries (even above a non decimal wheel). Quotient wheels seem to have been used during a division to memorize the number of times the divisor was subtracted at each given index.






Pascal went through 50 prototypes before settling on his final design; we know that he started with some sort of calculating clock mechanism that used springs which apparently "works by springs and which has a very simple design", was used "many times" and remained in "operating order". Nevertheless, "while always improving on it" he found reason to try to make the whole system more reliable and robust Eventually he adopted a component of very large clocks, shrinking and adapting for his purpose the robust gears that can be found in a turret clock mechanism called a lantern gear, itself derived from a water wheel mechanism. This could easily handle the strength of an operator input
In the colored drawing above, the blue gear (input) meshes with the yellow gears (processing) which themselves drive the red gear (output). The intersection of two perpendicular cylinders is one point and therefore, in theory, the blue gear and the yellow gear mesh in one single point. Pascal designed a gear that could easily take the strength of the strongest operator and yet added almost zero friction to the entire mechanism.



Pascal adapted a pawl and ratchet mechanism to his own turret wheel design; the pawl prevents the wheel from turning counterclockwise during an operator input, but it is also used to precisely position the display wheel and the carry mechanism for the next digit when it is pushed up and lands into its next position. Because of this mechanism, each number displayed is perfectly centered in the display window and each digit is precisely positioned for the next operation. This mechanism would be moved six times if the operator dialed a six on its associated input wheel.







The sautoir is the centerpiece of the pascaline's carry mechanism. In his "Avis n cessaire...", Pascal wrote:

... as for the ease of use of this ... movement ... moving one thousand or even ten thousand wheels if they were there, each one performing their motion perfectly, is as easy as moving only one (I don't know if, after the principle that I perfected to design this device, there is another one left to be found in nature)...

A machine with 10,000 wheels would work as well as a machine with two wheels because each wheel is independent of the other. When it is time to propagate a carry, the sautoir, under the sole influence of gravity, is thrown toward the next wheel without any contact between the wheels. During its free fall the sautoir behaves like an acrobat jumping from one trapeze to the next without the trapezes touching each other (sautoir comes from the French verb sauter which means to jump). All the wheels (including gears and sautoir) have therefore the same size and weight independently of the capacity of the machine.
Pascal used gravity to arm the sautoirs. One must turn the wheel five steps from 4 to 9 in order to fully arm a sautoir, but the carry transfer will move the next wheel only one step. Thus much extra energy is accumulated during the arming of a sautoir.
All the sautoirs are armed by either an operator input or a carry forward. To re-zero a 10,000-wheel machine, if one existed, the operator would have to set every wheel to its maximum and then add a 1 to the "unit" wheel. The carry would turn every input wheel one by one in a very rapid Domino effect fashion and all the display registers would be reset.




The animation on the right shows the three phases of a carry transmission.
The first phase happens when the display register goes from 4 to 9. The two carry pins (one after the other) lift the sautoir pushing on its protruding part marked (3,4,5). At the same time the kicking pawl (1) is pulled up, using a pin on the receiving wheel as guidance, but without effect on this wheel because of the top pawl/ratchet (C).
The second phase starts when the display register goes from 9 to 0. The kicking pawl passes its guiding pin and its spring (z,u) positions it above this pin ready to push back on it. The sautoir keeps on moving up and suddenly the second carry pin drops it. The sautoir is falling of its own weight.
The kicking pawl (1) pushes the pin on the receiving wheel and starts turning it. The upper pawl/ratchet (C) is moved to the next space. The operation stops when the protruding part (T) hits the buffer stop (R). The upper pawl/ratchet (C) position the entire receiving mechanism in its proper place.
During the first phase, the active wheel touches the one that will receive the carry through the sautoir, but it never moves it or modifies it and therefore the status of the receiving wheel has no impact whatsoever on the active wheel.
During the second phase, the sautoir and the two wheels are completely disconnected.
During the third phase the sautoir, which no longer touches the active wheel, adds one to the receiving wheel.






The Pascaline is a direct adding machine (it has no crank), so the value of a number is added to the accumulator as it is being dialed in. By moving a display bar, the operator can see either the number stored in the calculator or the complement of its value. Subtractions are performed like additions using some properties of 9's complement arithmetic.



The 9's complement of any one digit decimal number d is 9 - d. So the 9's complement of 4 is 5 and the 9's complement of 9 is 0. Similarly the 11's complement of 3 is 8.
In a decimal machine with n dials the 9's complement of a number A is:
CP(A)= 10n - 1 - A
and therefore the 9's complement of (A - B) is:
CP(A - B)= 10n -1 - (A - B) = 10n -1 - A + B = CP(A) + B
CP(A - B)= CP(A) + B
In other words, the 9's complement of the difference of two numbers is equal to the sum of the 9's complement of the minuend added to the subtrahend. The same principle is valid and can be used with numbers composed of digits of various bases (base 6, 12, 20) like in the surveying or the accounting machines.
This can also be extended to:
CP(A - B - C - D)= CP(A) + B + C + D
This principle applied to the pascaline:



The machine has to be re-zeroed before each new operation.
To reset his machine, the operator has to set all the wheels to their maximum, using the marks on two adjacent spokes, and then add 1 to the rightmost wheel.
The method of re-zeroing that Pascal chose, which propagates a carry right through the machine, is the most demanding task for a mechanical calculator and proves, before each operation, that the machine is fully functional.
This is a testament to the quality of the Pascaline because none of the 18th century criticisms of the machine mentioned a problem with the carry mechanism and yet this feature was fully tested on all the machines, by their resets, all the time.



Additions are performed with the display bar moved closest to the edge of the machine, showing the direct value of the accumulator.
After re-zeroing the machine, numbers are dialed in one after the other.
The following table shows all the steps required to compute: 12,345 + 56,789 = 69,134



Subtractions are performed with the display bar moved closest to the center of the machine showing the complement value of the accumulator.
The accumulator contains CP( A ) during the first step and CP( A - B) after adding B. In displaying that data in the complement window, the operator sees CP( CP( A)) which is A and then CP(CP( A - B )) which is (A - B). It feels like an addition since the only two differences in between an addition and a subtraction are the position of the display bar (direct versus complement) and the way the first number is entered (direct versus complement).
The following table shows all the steps required to compute: 54,321 - 12,345 = 41,976




For a more extensive treatment of this question see Schickard versus Pascal: An Empty Debate?



From the introduction of the Pascaline and for more than three centuries Pascal was known as the inventor of the mechanical calculator, but then, in 1957, Franz Hammer, a biographer of Johannes Kepler, challenged this fact by announcing that the drawings of a previously unknown working calculating clock, predating Pascal's work by twenty years had been rediscovered, after three centuries of absence, in two letters that Wilhelm Schickard had written to his friend Johannes Kepler in 1623 and 1624. The 1624 letter stated that the first machine to be built by a professional had been destroyed in a fire during its construction and that he was abandoning his project.
Franz Hammer asserted that because these letters had been lost for three hundred years, Blaise Pascal had been called and celebrated as the inventor of the mechanical calculator in error during all this time.



After careful examination it was found, in contradistinction to Franz Hammer's understanding, that Schikard's drawings had been published at least once per century starting from 1718. The other part of Franz Hammer's claim, which may also be erroneous, was that the two letters contained the drawings of a calculating clock that actually worked. This of course may have been a deficiency in the completeness of the fragmentary notes that have survived, rather than the actual machine built under Schickard's design, which he certainly reported to Kepler as working.



Bruno von Freytag Loringhoff, a mathematics professor at the University of T bingen built the first replica of Schickard's machine but not without adding wheels and springs to finish the design:

This simple-looking device actually presents a host of problems to anyone attempting to construct an adding machine based on this principle. The major problem is caused by the fact that the single tooth must enter into the teeth of the intermediate wheel, rotate it 36 degrees (one tenth of a revolution), and exit from the teeth, all while only rotating 36 degrees itself. The most elementary solution to this problem consists of the intermediate wheel being, in effect, two different gears, one with long and one with short teeth together with a spring-loaded detente (much like the pointer used on the big wheel of the gambling game generally known as Crown and Anchor) which would allow the gears to stop only in specific locations. It is not known if Schickard used this mechanism, but it certainly works well on the reproductions constructed by von Freytag Loringhoff.

This detail is not described in the two surviving Schickard's letters and drawings but as these were merely notes this cannot be taken to mean that he was misleading Kepler when he stated that he had built such a machine and it worked. As Schickard noted "...Arithmeticum organum alias delineabo accuratius, nunc et festinate hoc have" or, in English: "..I will describe the computer more precisely some other time, now I don't have enough time." Amongst the detail omitted could well have been the d tente. The role of a d tente catchment was widely understood by clockmakers, and as Schickard had turned to clockmakers to construct his machine, it is highly likely that this type of approach would have been included before the finalisation of his machine.



A problem in the operation of the Schickard machine, based on the surviving notes, was found after the replicas were built:

... it is almost certain that Pascal would not have known of Schickard's machine ...
Pascal seems to have realized right from the start that the single-tooth gear, like that used by Schickard, would not do for a general carry mechanism. The single-tooth gear works fine if the carry is only going to be propagated a few places but, if the carry has to be propagated several places along the accumulator, the force needed to operate the machine would be of such magnitude that it would do damage to the delicate gear works.

Schickard's machine used clock wheels which were made stronger and were therefore heavier, to prevent them from being damaged by the force of an operator input. Each digit used a display wheel, an input wheel and an intermediate wheel. During a carry transfer all these wheels meshed with the wheels of the digit receiving the carry. The cumulative friction and inertia of all these wheels could "...potentially damage the machine if a carry needed to be propagated through the digits, for example like adding 1 to a number like 9,999".
The great innovation in Pascal's calculator was that it was designed so that each input wheel is totally independent from all the others and carries are propagated in sequence. Pascal chose, for his machine, a method of re-zeroing that propagates a carry right through the machine. It is the most demanding operation to execute for a mechanical calculator and proved, before each operation, that the carry mechanism of the Pascaline was fully functional. This could be taken as a testament to the quality of the Pascaline because none of the 18th century criticisms of the machine mentioned a problem with the carry mechanism and yet this feature was fully tested on all the machines, by their resets, all the time. Alternatively, it could be taken as a prudent test of the functioning of the machine prior to use since, as experimenting with replicas suggests, it is not too hard for such designs, even built with modern tools, to go out of adjustment.



Even though Schickard designed his machine twenty years earlier a controversy continues as to whether Schickard or Pascal should be described as the inventor of the mechanical calculator. On the one hand, we have from Schickard's drawings the first description of a mechanical calculator. But the carry mechanism of Schickard's calculator is not fully described (which does not necessarily mean there was not one well worked out) and from what is described in the surviving notes it appears that if a carry was required across several places simultaneously the mechanism would jam. On the other hand, examples of Pascal's calculator survive and modern replicas have been made which work perfectly. But Pascal designed and built his machines later than Schickard. In the end, the answer to the question of who should be recognised as having primacy in this invention depends on precisely how that question is phrased.
The two machines were essentially different in that Pascal's machine was designed primarily for addition (and with the use of complementary numbers) for subtraction. The adding machine in Schickard's design may have jammed in the unusual case of a carry being required across too many dials, but it could smoothly subtract by reversing the motion of the input dials, in a way that was not possible in the Pascaline. (Experiments with replicas show that in the event of a jam when a carry is attempted across more than (say) three dials, it is obvious to the operator who may intervene to assist the machine to perform the additional carries. This is not as efficient as with the Pascaline, but it is not a fatal deficiency.) The Schickard adding machine also has provision for an audible warning when an output was too large for the available dials. This was not provided for in the Pascaline.
In any case, in contradistinction to the aims of Pascal which appear to have been to create a smoothly functioning adding machine for use by his father initially, and later for commercialisation the adding machine in Schickard's design appears to have been introduced to assist the grander objective of multiplication (through the calculation of partial products using Napier's rods, a process that can also be used to assist division). Experiments with building modern replicas of either machine suggest that there were things that in practice could interfere with the smooth performance of either the Pascaline or the adding machine in Schickard's design. Modern replicas of the Pascaline (when constructed with similar materials and technique to the originals) demonstrate that with some additional tweaks it can work perfectly for addition with careful adjustment and operation. But it does not take much to throw it into less than perfect performance. The Schickard could resist carry over to too many output wheels at once, and when this occurred would need to be assisted by the operator to complete the carry. So it depends on what is considered as important as to whether either machine could be seen as a success or failure. Neither, was a success in the sense that it was taken up and used widely in practice. From what is known neither can be shown to be a failure in the sense that it could not be used by a careful operator to calculate in one way or another. For this reason, and given the ongoing debate on how most felicitously to characterise the matter in the scholarly literature, attempting to produce an artificial closure on this debate is probably unproductive.


