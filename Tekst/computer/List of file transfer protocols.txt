A list of notable file transfer protocols:






9P
Apple Filing Protocol (AFP)
BitTorrent
Infinit
FTAM
FTP
FTP over SSL (FTPS)

HFTP
HULFT
HTTP
HTTPS
WebDAV

rcp
rsync
Simple Asynchronous File Transfer (SAFT), bound to TCP port 487
Secure copy (SCP)
SSH file transfer protocol (SFTP)
Simple File Transfer Protocol
T.127



File Service Protocol
Multicast File Transfer Protocol
Multipurpose Transaction Protocol
Reliable Blast UDP
Trivial File Transfer Protocol (TFTP) -- designed for simplicity rather than speed
Tsunami UDP Protocol
UDP-based Data Transfer Protocol (UDT)
UFTP   UDP Based FTP with Multicast



ASCII dump
BiModem
BLAST
CModem
CompuServe B (aka B protocol or CIS-B)
JMODEM
HS/Link
Kermit and variants:
Kermit
SuperKermit

LeechModem
Lynx (protocol)
NMODEM
Punter family
SEAlink
SMODEM
SuperK
TELINK
Tmodem
UUCP and variants:
UUCP
UUCP-g

XMODEM and variants:
MODEM7 (Batch XMODEM)
XMODEM, XMODEM-1K, XMODEM-G
WXMODEM

YMODEM and variants:
YMODEM, YMODEM-1K, YMODEM-G

ZMax
ZMODEM



File transfer
Protocol (computing)
Communications protocol
Bulletin board system
List of network protocols
Connect:Direct
Cross File Transfer






Evolution and Selection of File Transfer Protocols by Chuck Forsberg