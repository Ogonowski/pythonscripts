Pascal Lamy (born 8 April 1947) is a French political consultant and businessman. He was the Director-General of the World Trade Organization (WTO) until 1 September 2013. His appointment took effect on 1 September 2005 for a four-year term. In April 2009, WTO members reappointed Lamy for a second four-year term, beginning on 1 September 2009. He was then succeeded by Roberto Azev do. Pascal Lamy was also European Commissioner for Trade and is currently the Honorary President of the Paris-based think tank, Notre Europe.



Born in Levallois-Perret, Hauts-de-Seine, a suburb of Paris, Lamy studied at Sciences Po Paris, from HEC and  NA, graduating second in his year of those specialising in economics. Lamy is also an honorary graduate of the University of Warwick.
He then joined the civil service, and in this role he ended up serving as an adviser to Jacques Delors as Economics and Finance Minister and Pierre Mauroy as Prime Minister.
Lamy has been a member of the French Socialist Party since 1969.



When Delors became President of the European Commission in 1984, he took Lamy with him to serve as chef de cabinet, which he did until the end of Delors' term in 1994. During his time there, Lamy became known as the Beast of the Berlaymont, the Gendarme and Exocet due to his habit of ordering civil servants, even Directors-General (head of departments) "precisely what to do   or else." He was seen as ruling Delors' office with a "rod of iron", with no-one able to bypass or manipulate him and those who tried being "banished to one of the less pleasant European postings".
Lamy briefly moved into business at Cr dit Lyonnais. Promoted to second in command, he was involved in the restructuring and privatisation of the bank. Returning to the European Commission in 1999, he was appointed European Commissioner for Trade by Commission President Romano Prodi. Lamy served to the expiry of the commission's term in 2004. His ability to manage the powerful civil servants in his department was noted.



On 13 May 2005, Pascal Lamy was chosen as the next director-general of the World Trade Organization, and took office on 1 September 2005 for a four-year term. On 30 April 2009, Lamy was re-elected unanimously by the WTO general council for a second term of four years, beginning 1 September 2009. He also served as the chairman of the organization's Trade Negotiations Committee. He is the WTO's fifth director-general.



Lamy is married and has three sons. His hobbies include running and cycling.



Lamy, Pascal. The Geneva Consensus: Making Trade Work for All. Cambridge: Cambridge University Press, 2013.
Lamy, Pascal. The Economic Summit and the European Community. Bissell Paper No. 5. Toronto: University of Toronto, Centre for International Studies, 1988



The Relationship between WTO Law and General International Law in the Lecture Series of the United Nations Audiovisual Library of International Law






About Pascal Lamy
Pascal Lamy's Commissioner's profile
Notre Europe
"Pascal Lamy   Managing Global Expectations", The Globalist, 23 February 2006
Pascal Lamy: Free Trade and Interdependence Help Promote Freedom   video report by Democracy Now!