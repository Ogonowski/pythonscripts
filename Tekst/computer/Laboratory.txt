A laboratory (/l b r t ri/ or / l b r t ri/; informally, lab) is a facility that provides controlled conditions in which scientific or technological research, experiments, and measurement may be performed.
Laboratories used for scientific research take many forms because of the differing requirements of specialists in the various fields of science and engineering. A physics laboratory might contain a particle accelerator or vacuum chamber, while a metallurgy laboratory could have apparatus for casting or refining metals or for testing their strength. A chemist or biologist might use a wet laboratory, while a psychologist's laboratory might be a room with one-way mirrors and hidden cameras in which to observe behavior. In some laboratories, such as those commonly used by computer scientists, computers (sometimes supercomputers) are used for either simulations or the analysis of data collected elsewhere. Scientists in other fields will use still other types of laboratories. Engineers use laboratories as well to design, build, and test technological devices.
Scientific laboratories can be found in schools and universities, in industry, in government or military facilities, and even aboard ships and spacecraft.



Early instances of "laboratories" recorded in English involved alchemy and the preparation of medicines.



Laboratory techniques are the sum of procedures used on natural sciences such as chemistry, biology, physics in order to conduct an experiment, all of them follow scientific method; while some of them involves the use of complex laboratory equipment from laboratory glassware to electrical devices others require such specific or expensive supplies.




Laboratory equipment refers to the various tools and equipment used by scientists working in a laboratory. These include tools such as Bunsen burners, and microscopes as well as specialty equipment such as operant conditioning chambers, spectrophotometers and calorimeters. Another important type of laboratory equipment is laboratory glassware such as the beaker or reagent bottle, or even a thermometer.
Laboratory equipment is generally used to either perform an experiment or to take measurements and gather data. Larger or more sophisticated equipment is generally called a scientific instrument. Both laboratory equipment and scientific instruments are increasingly being designed and shared using open hardware principles. 
open source labs use open source scientific hardware.



The title of laboratory is also used for certain other facilities where the processes or equipment used are similar to those in scientific laboratories. These notably include:
film laboratory or darkroom
clandestine lab for the production of illegal drugs
computer lab
crime lab used to process crime scene evidence
language laboratory
medical laboratory (involves handling of chemical compounds)
public health laboratory




In some laboratories, the conditions are no more dangerous than in any other room. In many labs, though, hazards are present. Laboratory hazards are as varied as the subjects of study in laboratories, and might include poisons; infectious agents; flammable, explosive, or radioactive materials; moving machinery; extreme temperatures; lasers, strong magnetic fields or high voltage. In laboratories where dangerous conditions might exist, safety precautions are important. Rules exist to minimize the individual's risk, and safety equipment is used to protect the lab user from injury or to assist in responding to an emergency.
The Occupational Safety and Health Administration (OSHA) in the United States, recognizing the unique characteristics of the laboratory workplace, has tailored a standard for occupational exposure to hazardous chemicals in laboratories. This standard is often referred to as the "Laboratory Standard". Under this standard, a laboratory is required to produce a Chemical Hygiene Plan (CHP) which addresses the specific hazards found in its location, and its approach to them.
In determining the proper Chemical Hygiene Plan for a particular business or laboratory, it is necessary to understand the requirements of the standard, evaluation of the current safety, health and environmental practices and assessment of the hazards. The CHP must be reviewed annually. Many schools and businesses employ safety, health, and environmental specialists, such as a Chemical Hygiene Officer (CHO) to develop, manage, and evaluate their CHP. Additionally, third party review is also used to provide an objective "outside view" which provides a fresh look at areas and problems that may be taken for granted or overlooked due to habit.
Inspections and audits like also be conducted on a regular basis to assess hazards due to chemical handling and storage, electrical equipment, biohazards, hazardous waste management, chemical waste, housekeeping and emergency preparedness, radiation safety, ventilation as well as respiratory testing and indoor air quality. An important element of such audits is the review of regulatory compliance and the training of individuals who have access to and/or work in the laboratory. Training is critical to the ongoing safe operation of the laboratory facility. Educators, staff and management must be engaged in working to reduce the likelihood of accidents, injuries and potential litigation. Efforts are made to ensure laboratory safety videos are both relevant and engaging.


