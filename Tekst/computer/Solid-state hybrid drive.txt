In computing, a hybrid drive (also known by the initialism SSHD) is a logical or physical storage device that combines NAND flash solid-state drive (SSD) with hard disk drive (HDD) technology, with the intent of adding some of the speed of SSDs to the cost-effective storage capacity of traditional HDDs. The purpose of the SSD in a hybrid drive is to act as a cache for the data stored on the HDD, improving the overall performance by keeping copies of the most frequently used data on the SSD.
There are two main technologies used for implementing hybrid drives: dual-drive hybrid systems and solid-state hybrid drives. In dual-drive hybrid systems, separate SSD and HDD devices are installed in the same computer, having the data placement optimization performed either manually by the end user, or automatically by the operating system through creation of a "hybrid" logical device. In solid-state hybrid drives, SSD and HDD functionalities are built into the same physical storage device, by adding a certain amount of NAND flash storage to a hard disk drive; the data placement decisions are performed either entirely by the device (self-optimized mode), or through placement "hints" supplied by the operating system (host-hinted mode).




There are two main "hybrid" storage technologies that combine NAND flash memory, or SSDs, with the HDD technology: dual-drive hybrid systems and solid-state hybrid drives.



Dual-drive hybrid systems combine the usage of separate SSD and HDD devices installed in the same computer. Overall performance optimizations are managed either by the computer user (by manually placing more frequently accessed data on an SSD), or by the computer's operating system software (by combining SSDs and HDDs into hybrid volumes, transparently to the end-users). Examples of hybrid volumes implementations in operating systems are bcache and dm-cache on Linux, and Apple s Fusion Drive.
A common implementation of the dual-drive system seen in the laptop computer category is through the use of flash cache modules (FCMs). FCMs combine the use of separate SSD (usually an mSATA SSD module) and HDD components, while managing performance optimizations via host software, device drivers, or a combination of both. Intel Smart Response Technology (SRT), which is implemented through a combination of certain Intel chipsets and Intel storage drivers, is the most common implementation of FCM hybrid systems today.
There are also dual-drive systems for laptops which combine separate SSD and HDD components into the same 2.5-inch HDD-size unit, while at the same time (unlike SSHDs) keeping these two components visible and accessible to the operating system as two separate hardware devices.



Solid-state hybrid drive (SSHD) refers to products that incorporate a significant amount of NAND flash memory into a hard disk drive (HDD), resulting in a single, integrated device. SSHD is a more precise term than the more general term hybrid drive, which has previously been used to describe SSHD devices and non-integrated combinations of solid-state drives (SSDs) and hard disk drives.
The fundamental design principle behind SSHDs is to identify data elements that are most directly associated with performance (frequently accessed data, boot data, etc.) and store these data elements in the NAND flash memory. This has been shown to be effective in delivering significantly improved performance over the standard HDD.



In the two forms of hybrid storage technologies (dual-drive hybrid systems and SSHDs), the goal is to combine HDD and NAND flash memory storage technologies to provide a balance of improved performance and high-capacity storage availability. In general, this is achieved by placing "hot data", or data that is most directly associated with improved performance, in the NAND flash memory or SSD part of the storage architecture.
Making decisions about which data elements are prioritized for NAND flash memory is at the core of SSHD technology. Products offered by various vendors may achieve this through device firmware, through device drivers or through software modules and device drivers.
SSHD products operate in two primary modes:
Self-optimized mode
In this mode of operation, the SSHD works independently from the host operating system or host device drives to make all decisions related to identifying data that will be stored in NAND flash memory. This mode results in a storage product that appears and operates to a host system exactly as a traditional hard drive would.
Host-optimized mode (or host-hinted mode)
In this mode of operation, the SSHD enables an extended set of SATA commands defined in the so-called Hybrid Information feature, introduced in version 3.2 of the Serial ATA International Organization (SATA-IO) standards for the SATA interface. Using these SATA commands, decisions about which data elements are placed in the NAND flash memory come from the host operating system, device drivers, file systems, or a combination of these host-level components.



Some of the specific features of SSHD drives, such as the host-hinted mode, require software support from the operating system. Microsoft added support for the host-hinted operation into Windows 8.1, while patches for the Linux kernel are available since October 2014, pending their inclusion into the Linux kernel mainline.



In 2007, Seagate and Samsung introduced the first hybrid drives with the Seagate Momentus PSD and Samsung SpinPoint MH80 products. Both models were 2.5-inch drives, featuring 128 MB or 256 MB NAND flash memory options. Seagate s Momentus PSD emphasized power efficiency for a better mobile experience and relied on Microsoft Vista s ReadyDrive. The products were not widely adopted.
In May 2010, Seagate introduced a new hybrid product called the Momentus XT and used the term solid-state hybrid drive. This product focused on delivering the combined benefits of hard drive capacity points with SSD-like performance. It shipped as a 500 GB HDD with 4 GB of integrated NAND flash memory.
In November 2011, Seagate introduced what they referred to as their second-generation SSHD, which increased the capacity to 750 GB and pushed the integrated NAND flash memory to 8 GB.
In March 2012, Seagate introduced their third-generation laptop SSHDs with two models   a 500 GB and 1 TB, both with 8 GB of integrated NAND flash memory.
In September 2012, Toshiba announced its first SSHD, delivering SSD-like performance and responsiveness by combining 8 GB of Toshiba s own NAND flash memory and innovative, self-learning algorithms with up to 1 TB of storage capacity.
In September 2012, Western Digital (WD) announced a hybrid technology platform pairing cost-effective MLC NAND flash memory with magnetic disks to deliver high-performance, large-capacity integrated storage systems.
In April 2013, WD introduced 2.5-inch WD Black SSHD products, including a 5 mm high SSHD with 500 GB of storage capacity and NAND flash memory size options of 8 GB, 16 GB and 24 GB.



Late 2011 and early 2012 benchmarks using an SSHD consisting of a 750 GB HDD and 8 GB of NAND cache found that SSHDs did not offer SSD performance on random read/write and sequential read/write, but were faster than HDDs for application startup and shutdown.
The 2011 benchmark included loading an image of a system that had been used heavily, running many applications, to bypass the performance advantage of a freshly-installed system; it found in real-world tests that performance was much closer to an SSD than to a mechanical HDD. Different benchmark tests found the SSHD to be between an HDD and SSD, but usually significantly slower than an SSD. In the case of uncached random access performance (multiple 4kB random reads and writes) the SSHD was no faster than a comparable HDD; there is advantage only with data that is cached. The author concluded that the SSHD drive was the best non-SSD type of drive by a significant margin, and that the larger the solid-state cache, the better the performance.




bcache, dm-cache, and Flashcache on Linux
Hybrid array
ExpressCache
ReadyBoost





