Scopolamine (USAN), hyoscine (BAN) also known as levo-duboisine or burundanga, sold as Scopoderm, is a tropane alkaloid drug with muscarinic antagonist effects. It is among the secondary metabolites of plants from Solanaceae (nightshade) family of plants, such as henbane, jimson weed (Datura), angel's trumpets (Brugmansia), and corkwood (Duboisia). Scopolamine exerts its effects by acting as a competitive antagonist at muscarinic acetylcholine receptors; it is thus classified as an anticholinergic, antimuscarinic drug. Although it is usually referred to as a nonspecific antagonist, there is indirect evidence for m1-receptor subtype specificity. (See the article on the parasympathetic nervous system for details of this physiology.)
Its use in medicine is relatively limited, with its chief uses being in the treatment of motion sickness and postoperative nausea and vomiting.
Scopolamine is named after the plant genus Scopolia. The name "hyoscine" is from the scientific name for henbane, Hyoscyamus niger.



Scopolamine has a number of uses in medicine, where it is used to treat:
Postoperative nausea and vomiting and sea sickness, leading to its use by scuba divers
Motion sickness (where it is often applied as a transdermal patch behind the ear)
Gastrointestinal spasms
Renal or biliary spasms
Aid in gastrointestinal radiology and endoscopy
Irritable bowel syndrome
Clozapine-induced hypersalivation (drooling)
Bowel colic
Eye inflammation
It is sometimes used as a premedication (especially to reduce respiratory tract secretions) to surgery, mostly commonly by injection.



Adverse effect incidence:
Uncommon (0.1%-1% incidence) adverse effects include
Dry mouth
Dyshidrosis (reduced ability to sweat in order to cool off)
Tachycardia (usually occurs at higher doses and is succeeded by bradycardia)
Bradycardia
Urticaria
Pruritus (itching)
Rare (<0.1% incidence) adverse effects include
Constipation
Urinary retention
Hallucinations
Agitation
Confusion
Restlessness
Seizures
Unknown frequency adverse effects include
Anaphylactic shock
Anaphylactic reactions
Dyspnea (shortness of breath)
Rash
Erythema
Other hypersensitivity reactions
Blurred vision
Mydriasis (dilated pupils)
Drowsiness
Dizziness
Somnolence



Physostigmine is an acetylcholinesterase inhibitor that readily crosses the blood-brain barrier, and has been used as an antidote to treat the CNS depression symptoms of scopolamine overdose. Other than this supportive treatment, gastric lavage and induced emesis (vomiting) are usually recommended as treatments for overdoses. The symptoms of overdose include:
Tachycardia
Arrhythmia
Blurred vision
Photophobia
Urinary retention
Drowsiness or paradoxical excitement which can present with hallucinations
Cheyne-Stokes respiration
Dry mouth
Skin reddening
Inhibition of gastrointestinal motility



Due to interactions with metabolism of other drugs, scopolamine can cause significant unwanted side effects when taken with other medications. Specific attention should be paid to other medications in the same pharmacologic class as scopolamine, also known as anticholinergics. The following medications could potentially interact with the metabolism of scopolamine: analgesics/pain medications, ethanol, zolpidem, thiazide diuretics, buprenorphine, anticholinergic drugs such as tiotropium, etc.



The steps of the biosynthesis of scopolamine are:
Ornithine decarboxylase (EC 4.1.1.17) decarboxylates L-ornithine to putrescine.
Putrescine N-methyltransferase (EC 2.1.1.53) methylates putrescine to N-methylputrescine.
Putrescine oxidase (EC 1.4.3.10) deaminates N-methylputrescine to 4-methylaminobutanal.
4-Methylaminobutanal spontaneously ring-closes to N-methyl-pyrrolium cation.
Something (no enzyme has been found) condenses pyrrolium cation with acetoacetic acid yielding hygrine.
Hygrine rearranges to tropinone.
Tropinone reductase I (EC 1.1.1.206) converts tropinone to tropine.
Tropine condenses with phenyllactate (made from phenylalanine) to form littorine.
A cytochrome P450 classified as Cyp80F1 oxidizes and rearranges littorine to hyoscyamine aldehyde.
6beta-hydroxyhyoscyamine epoxidase (EC 1.14.11.14) epoxidizes hyoscyamine to scopolamine.



One of the earlier alkaloids isolated from plant sources, scopolamine has been in use in its purified forms (such as various salts, including hydrochloride, hydrobromide, hydroiodide and sulfate), since its isolation by the German scientist Albert Ladenburg in 1880, and as various preparations from its plant-based form since antiquity and perhaps prehistoric times. Following the description of the structure and activity of scopolamine by Ladenburg, the search for synthetic analogues of and methods for total synthesis of scopolamine and/or atropine in the 1930s and 1940s resulted in the discovery of diphenhydramine, an early antihistamine and the prototype of its chemical subclass of these drugs, and pethidine, the first fully synthetic opioid analgesic, known as Dolatin and Demerol amongst many other trade names.
Scopolamine was used in conjunction with morphine, oxycodone, or other opioids from before 1900 into the 1960s to put mothers in labor into a kind of "twilight sleep". The analgesia from scopolamine plus a strong opioid is deep enough to allow higher doses to be used as a form of anaesthesia.
Scopolamine mixed with oxycodone (Eukodal) and ephedrine was marketed by Merck as SEE (from the German initials of the ingredients) and Scophedal starting in 1928, and the mixture is sometimes mixed on site on rare occasions in the area of its greatest historical usage, namely Germany and Central Europe.
Scopolamine was also one of the active ingredients in Asthmador, an over-the-counter (OTC) smoking preparation marketed in the 1950s and 1960s claiming to combat asthma and bronchitis. In November 1990, the US Food and Drug Administration forced OTC products with scopolamine and several hundred other ingredients that had allegedly not been proved effective off the market. Scopolamine shared a small segment of the OTC sleeping pill market with diphenhydramine, phenyltoloxamine, pyrilamine, doxylamine, and other first-generation antihistamines, many of which are still used for this purpose in drugs such as Sominex, Tylenol PM, NyQuil, etc.



Scopolamine can be administered orally, subcutaneously, ophthalmically and intravenously, as well as via a transdermal patch. The transdermal patch (e.g., Transderm Sc p) for prevention of nausea and motion sickness employs scopolamine base, and is effective for up to three days. The oral, ophthalmic, and intravenous forms have shorter half-lives and are usually found in the form scopolamine hydrobromide (for example in Scopace, soluble 0.4-mg tablets or Donnatal).
NASA is currently developing a nasal administration method. With a precise dosage, the NASA spray formulation has been shown to work faster and more reliably than the oral form.



Scopolamine crosses the placenta and is a pregnancy category C medication, meaning a risk to the fetus cannot be ruled out. Either studies in animals have revealed adverse effects on the fetus (teratogenic or embryocidal effects or other) and no controlled studies in women have been made, or studies in women and animals are not available. Drugs should be given only if the potential benefits justify the potential risk to the fetus. It may cause respiratory depression and/or neonatal hemorrhage when used during pregnancy, and some animal studies did report adverse events. Transdermal scopolamine has been used as an adjunct to epidural anesthesia for Caesarean delivery without adverse CNS effects on the newborn. Except when used prior to Caesarean section, use it during pregnancy only if the benefit to the mother outweighs the potential risk to the fetus.



Scopolamine enters breast milk by secretion. Although no human studies exist to document the safety of scopolamine while nursing, the manufacturer recommends caution be used if scopolamine be administered to a nursing woman.



While it is occasionally used recreationally for its hallucinogenic properties, the experiences are often mentally and physically extremely unpleasant, and frequently physically dangerous, so repeated use is rare.



Scopolamine use in the elderly can increase the likelihood of experiencing adverse effects from the drug. This phenomenon is especially true of the elder population who are also concurrently on several other medications. Avoid scopolamine use in this age group due to potent anticholinergic adverse effects and uncertain effectiveness.



About one in five emergency room admissions for poisoning in Bogot , Colombia, have been attributed to scopolamine. In June 2008, more than 20 people were hospitalized with psychosis in Norway after ingesting counterfeit rohypnol tablets containing scopolamine.



The effects of scopolamine were studied by criminologists in the early 20th century. In 2009, it was proven that Czechoslovak communist state security secret police used scopolamine at least three times to obtain confessions from alleged antistate conspirators. Because of a number of undesirable side effects, scopolamine was shortly disqualified as a truth serum.
In 1910, scopolamine was detected in the remains believed to be those of Cora Crippen, wife of Dr. Hawley Harvey Crippen, and was accepted at the time as the cause of her death, since her husband was known to have bought some at the start of the year.



In 2008, Vice News aired an episode called Colombian Devil's Breath recounting the use of scopolamine by Colombian criminals as a suggestion drug. The two-part investigation contains multiple first-hand accounts of its use.
A 2012 example claims small amounts are blown into victims' faces on the street to turn the victims into "mindless zombies". However, that these claims are largely considered hoaxes with no substantial evidence to support them.
Scopolamine has been used under the name burundanga in Venezuelan and Thailand resorts in order to drug and then rob tourists. While there are unfounded rumors that delivery mechanisms include using pamphlets and flyers laced with the drug, not enough is readily absorbed through the skin to have an effect. However, spiked alcoholic drinks are occasionally used.
In recent years the criminal use of scopolamine has become an epidemic. Approximately half of emergency room admissions for poisoning in Bogot  have been attributed to scopolamine.
Victims of this crime are often admitted to a hospital in police custody, under the assumption that the patient is experiencing a psychotic episode. A telltale sign is a fever accompanied by a lack of sweat.
Scopolamine is used criminally as a date rape drug and as an aid to robbery, the most common act being the clandestine drugging of a victim's drink. It is preferred because it induces anterograde amnesia, or an inability to recall events a certain amount of time after its administration or during the time of intoxication.
Per the United States State Department (March 4, 2012):

One common and particularly dangerous method that criminals use in order to rob a victim is through the use of drugs. The most common has been scopolamine. Unofficial estimates put the number of annual scopolamine incidents in Colombia at approximately 50,000. Scopolamine can render a victim unconscious for 24 hours or more. In large doses, it can cause respiratory failure and death. It is most often administered in liquid or powder form in foods and beverages. The majority of these incidents occur in night clubs and bars, and usually men, perceived to be wealthy, are targeted by young, attractive women. To avoid becoming a victim of scopolamine, one should never accept food or beverages offered by strangers or new acquaintances or leave food or beverages unattended. Victims of scopolamine or other drugs should seek immediate medical attention.



Scopolamine has been shown in multiple studies to be a potent antidepressant and anxiolytic medication. Two methods of administration have been studied. The first is in-patient sessions where the patients receive an intravenous infusion of a relatively large quantity of scopolamine during a session that lasts 1 2 hours. The patients are monitored during the infusion and released soon after as the effects wear off quickly. In research assessing intravenous infusions, scopolamine has been found to produce rapid and robust antidepressant effects in patients with major depressive disorder.
The second route of administration is oral scopolamine in a pill; it has been studied for depression.


