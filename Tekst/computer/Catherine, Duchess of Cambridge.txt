Catherine, Duchess of Cambridge (Catherine Elizabeth "Kate"; n e Middleton; born 9 January 1982), is the wife of Prince William, Duke of Cambridge. Following his father Charles, Prince of Wales, William is second in line to succeed his grandmother, Queen Elizabeth II, as monarch of the United Kingdom and 15 other Commonwealth realms.
Middleton grew up in Chapel Row, a village near Newbury, Berkshire, England. She studied art history in Scotland at the University of St Andrews, where she met William in 2001. Their engagement was announced on 16 November 2010, and she attended many high-profile royal events before they married on 29 April 2011 at Westminster Abbey. The Duke and Duchess have two children: Prince George and Princess Charlotte of Cambridge, who are respectively third and fourth in line to the British throne.
She has had a major impact upon British and American fashion, which has been termed the "Kate Middleton effect", and in 2012 and 2013 she was selected as one of the "100 Most Influential People in the World" by Time magazine.



Catherine Elizabeth Middleton was born at Royal Berkshire Hospital in Reading on 9 January 1982, and christened at St Andrew's Bradfield, Berkshire, on 20 June 1982. She is the eldest of three children born to Michael and Carole Middleton, a former flight dispatcher and flight attendant, who in 1987 founded Party Pieces, a mail order private company that sells party supplies and decorations with an estimated worth of  30 million. The Middletons have another daughter, Philippa "Pippa", and a son, James.
Michael and Carole Middleton worked for British Airways, in Amman, Jordan, from May 1984 to September 1986. In Jordan, Catherine Middleton went to an English-language nursery school before returning to their home in Berkshire. Following her return from Amman, Middleton was enrolled at St Andrew's School near the village of Pangbourne in Berkshire, then briefly at Downe House. She was a boarder at Marlborough College, a co-educational independent boarding school in Wiltshire, and graduated in 2005, from the University of St Andrews in Fife, Scotland, with an undergraduate MA (2:1 Hons) in the history of art. In November 2006, Middleton accepted a position as an accessory buyer with the clothing chain Jigsaw, where she worked part-time until November 2007. She also worked until January 2011 at Party Pieces; her role within the family business included catalogue design and production, marketing and photography.






In 2001, Middleton met Prince William while they were both students in residence at St Salvator's Hall at the University of St. Andrews. The couple began dating in 2003, although their relationship remained unconfirmed. On 17 October 2005, Middleton complained through her lawyer about harassment from the media, stating that she had done nothing significant to warrant publicity.
Media attention increased around the time of her 25th birthday in January 2007, prompting warnings from both the Prince of Wales and Prince William and from Middleton's lawyers, who threatened legal action. Two newspaper groups, News International, which publishes The Times and The Sun; and the Guardian Media Group, publishers of The Guardian, decided to refrain from publishing paparazzi photographs of her. Middleton attended at least one event as an official royal guest: Prince William's Passing Out Parade at the Royal Military Academy Sandhurst on 15 December 2006.
On 17 May 2008, Middleton attended the wedding of Prince William's cousin Peter Phillips to Autumn Kelly, which the prince did not attend. On 19 July 2008, she was a guest at the wedding of Lady Rose Windsor and George Gilman. Prince William was away on military operations in the Caribbean, serving aboard HMS Iron Duke. In 2010, Middleton pursued an invasion of privacy claim against two agencies and photographer Niraj Tanna, who took pictures of her over Christmas 2009. She obtained a public apology,  5,000 in damages, and legal costs.



In April 2007 Prince William and Middleton split up. The couple decided to break up during a holiday in the Swiss resort of Zermatt. Clarence House declined to comment about the relationship's end, according to The Times, stating, "We don't comment on Prince William's private life". Newspapers speculated about the reasons for the split, although these reports relied on anonymous sources. Middleton and her family attended the Concert for Diana at Wembley Stadium, where she and Prince William sat two rows apart. The couple were subsequently seen together in public on a number of occasions and news sources stated that they had "rekindled their relationship".




Prince William and Catherine Middleton became engaged in October 2010, in Kenya, during a 10-day trip to the Lewa Wildlife Conservancy to celebrate Prince William's passing his RAF helicopter search and rescue course. Clarence House announced the engagement on 16 November 2010. Prince William gave Middleton the engagement ring that had belonged to his mother, Diana, Princess of Wales. The couple married in Westminster Abbey on 29 April 2011 (St. Catherine's Day), with the day declared a bank holiday in the United Kingdom. Estimates of the global audience for the wedding ranged around 300 million or more, whilst 26 million watched the event live in Britain alone.
In October, several months after the wedding, Commonwealth leaders pledged that they would implement changes in British royal succession law to adopt absolute primogeniture, meaning that the first child of the Duke and Duchess would be eligible to take the throne regardless of whether it is male or female.




In January 2013, the Queen issued new letters patent enabling all children of the eldest son, as opposed to only the eldest son, of the Prince of Wales to enjoy the princely title and style of Royal Highness.
On 3 December 2012, St James's Palace announced that the Duchess was pregnant with her first child. The announcement was made earlier in the pregnancy than is traditional as she had been admitted to King Edward VII's Hospital Sister Agnes suffering from hyperemesis gravidarum, a severe form of morning sickness. She stayed in hospital for three days. On 14 January 2013, St James's Palace announced that the child was due to be born in July 2013, and that the condition of the Duchess was improving. The Duchess was admitted to St Mary's Hospital in London in the early stages of labour on the morning of 22 July 2013, and gave birth to a boy, weighing 8 pounds 6 ounces (3.80 kg), at 16:24 BST that day. On 24 July 2013, Kensington Palace announced that the baby would be named George Alexander Louis.
The Duchess's second pregnancy was announced on 8 September 2014. As with her first pregnancy, the Duchess suffered from hyperemesis gravidarum and was required to cancel official engagements. On 2 May 2015, at 08:34 BST, the Duchess gave birth to a girl weighing 8 pounds 3 ounces (3.71 kg). On 4 May 2015, Kensington Palace announced that the baby would be named Charlotte Elizabeth Diana.




Middleton became prominent for her fashion style and has been placed on numerous "best dressed" lists. She was selected by The Daily Telegraph as the "Most Promising Newcomer" in its 2006 list of style winners and losers. Tatler placed her at number 8 on its yearly listing of the top ten style icons in 2007. She was featured in People magazine's 2007 and 2010 best-dressed lists. Middleton was named as one of Richard Blackwell's ten "Fabulous Fashion Independents" of 2007. In June 2008, Style.com selected Middleton as their monthly beauty icon. In July 2008, Middleton was included in Vanity Fair ' s international best-dressed list. In February 2011, she was named the Top Fashion Buzzword of the 2011 season by the Global Language Monitor. In January 2012, she was voted 'Headwear Person of the Year.' Middleton was number one on Vanity Fair's annual Best Dressed lists in 2010, 2011, 2012 and 2013; she also appeared as the cover star in 2012.







Middleton was formally introduced to public life on 24 February 2011, two months before the wedding, when she and Prince William attended a lifeboat-naming ceremony in Trearddur, Anglesey, in North Wales. A day later they appeared in St Andrews to launch the university's 600th anniversary celebrations. On 16 February 2011, Clarence House announced that the Duke and Duchess's first royal tour of Canada would take place in July 2011. In May 2011, shortly after the wedding, Clarence House announced that the Duke and Duchess would extend their tour to visit California. This was to be the Duchess of Cambridge's first visit to the United States.

The Duchess's first official engagement after the wedding came in May, when she and her husband met Barack Obama, the President of the United States, and First Lady Michelle Obama. In June 2011, the Duke and Duchess presented medals to members of the Irish Guards.
On 26 October 2011, she undertook her first solo event for In Kind Direct, stepping in for the Prince of Wales, who was in Saudi Arabia. On 2 November, the Duke and Duchess of Cambridge visited the UNICEF Supply Division Centre for supplying food to malnourished African children in Copenhagen, Denmark.
On St Patrick's Day, 17 March 2012, the Duchess carried out the traditional awarding of shamrocks to the Irish Guards at their base in Aldershot; this was her first solo military engagement. On 19 March, she gave her first speaking engagement for the opening of the Treehouse, a new children's hospice opened by East Anglia's Children's Hospices (EACH), a charity of which she is a patron.

The Duchess has involved herself with the charities supported by her husband and his brother, Prince Harry. On 29 September 2011, the Duchess officially became a patron of The Foundation of Prince William and Prince Harry. Since November 2011 she and the Duke have attended the biannual Princes' Charities Forum, which unites the various charitable interests of the two princes. The Duchess did not attend the forum in November 2014 due to her pregnancy. In June 2012, The Foundation of Prince William and Prince Harry was renamed The Royal Foundation of The Duke and Duchess of Cambridge and Prince Harry, to reflect Catherine's contribution to the charity.
The Duke and Duchess were announced as Ambassadors for the 2012 Summer Olympics in London, alongside Prince Harry. The Duchess attended both the Opening and Closing Ceremonies of the Olympics. On 29 August 2012, the Duchess attended the Paralympic Opening Ceremony accompanied by her husband, the Duke of Cambridge. As part of her role, the Duchess attended numerous sporting events throughout the games.

In September 2012, the Duke and Duchess embarked on a tour of Singapore, Malaysia, Tuvalu, and the Solomon Islands as part of the Royal Jubilee celebrations. During this overseas visit, the Duchess made her first official speech abroad, while visiting a hospice in Malaysia, drawing on her experience as patron of East Anglia's Children's Hospices.
Due to her pregnancy, the Duchess carried out fewer engagements in 2013 than in previous years. After the birth of Prince George, she carried out her first engagement in late August when she accompanied the Duke to meet runners preparing for an ultra-marathon on the isle of Anglesey, where they have a residence.
At the beginning of March 2014 details were announced of the half-month-long tour to New Zealand and Australia that the Duchess and her husband and son would be taking from 16 to 25 April. The tour was Catherine's first visit to the area and Prince George's first major public appearance since his christening in October 2013. The tour began in New Zealand where they visited Wellington, Blenheim, Auckland, Dunedin, Queenstown and Christchurch. It ended in Australia where they visited Sydney, the Blue Mountains, Brisbane, Uluru, Adelaide, and Canberra.
On 21 July 2014, it was announced that the Duchess would be making her first solo trip, visiting the island of Malta on 20 21 September 2014, when the island was celebrating its 50th independence anniversary. Her trip was cancelled, with the Duke taking her place, after the announcement of her second pregnancy in early September.




In March 2011, the Duke and Duchess set up a gift fund held by The Foundation of Prince William and Prince Harry to allow well-wishers who want to give them a wedding gift to donate money to charities they care about instead. The gift fund supported 26 charities of the couple's choice, incorporating the armed forces, children, the elderly, art, sport and conservation. These causes are close to their hearts and reflect the experiences, passions and values of their lives so far.
The Duchess supports charities The Art Room, National Portrait Gallery, London, East Anglia's Children's Hospice, Action on Addiction, Place2Be, Natural History Museum, Sportsaid, and The 1851 Trust. The Natural History Museum is a patronage formerly held by Diana, Princess of Wales.
She is also a local volunteer leader with the Scout Association in north Wales. In October 2012, the Duchess gave her royal backing to the M-PACT programme (Moving Parents and Children Together), one of the only UK programmes to focus specifically on the impact of drug addiction on families as a whole.
Her first official portrait was unveiled at the National Portrait Gallery in January 2013, meeting mixed reviews from both critics and audiences.



In 1997, William's mother, Diana, Princess of Wales, died in a road accident in Paris while being chased by paparazzi. This incident has influenced the Duke's attitude towards intrusive media attention. Both the Duchess and her husband have been clear that, when off-duty, their privacy should be respected, yet the media, at times, has violated the couple's wishes.
In 2009, before her engagement to William, Middleton was awarded  10,000 damages and an apology from the photographic press agency Rex Features Ltd after she was photographed playing tennis on Christmas Eve when on holiday in Cornwall.
On 13 September 2012, it was reported that the French edition of "la presse people" magazine Closer and the Italian gossip magazine Chi, had both published photographs of the Duchess sun-bathing topless while on holiday at the Ch teau d'Autet (a private ch teau on a 260-ha estate some 71 km north of Aix-en-Provence). Analysts from The Times believed that the photograph was taken from the D22 (Vaucluse) road half a kilometre from the pool   a distance that would require an 800-mm or a 1000-mm lens. On 17 September 2012, the Duke and Duchess laid a criminal complaint to the French Prosecution Department and launched a claim for civil damages at the Tribunal de Grande Instance de Nanterre; the following day the courts granted an injunction against Closer prohibiting further publication of the pictures and also announced that a criminal investigation would be initiated. Under French law, punitive damages cannot be awarded but such intrusions of privacy are a criminal offence carrying a maximum jail sentence of one year and a fine of up to  45,000 for individuals and  225,000 for companies.
In December 2012, two Australian radio hosts, Michael Christian and Mel Greig, called King Edward VII's Hospital Sister Agnes where the Duchess was an in-patient for hyperemesis gravidarum. Pretending to be the Queen and the Prince of Wales, Greig and Christian telephoned the hospital and spoke to a nurse on the Duchess's ward, enquiring about her condition. Following a hospital inquiry and a public backlash against the hoax, the nurse who put the call through to the ward, Jacintha Saldanha, committed suicide. The radio hosts subsequently apologised for their actions.
In its second breach of privacy, in February 2013, Chi published the first photos of Catherine's exposed baby bump, taken during her vacation on the private island of Mustique. The British press have refused to publish the paparazzi shots out of respect for the couple. Whilst the Duchess was visiting the Blue Mountains in Sydney a picture was taken of her bare bottom as her dress blew up. Many newspapers refused to follow the ban imposed by British media and published the picture.



Following international attention regarding the wedding, Lifetime aired a TV film entitled William & Kate on 18 April 2011, in the US. The film premiered in the UK on 24 April 2011. Middleton was played by Camilla Luddington and Prince William by Nico Evers-Swindell. TV programmes were also shown in the UK prior to the wedding which provided deeper insights into the couple's relationship and backgrounds, including When Kate Met William and Channel 4's Meet the Middletons.
A second TV film was produced that covers similar ground to William & Kate. That film, titled William & Catherine: A Royal Romance and filmed in Bucharest, starred Alice St. Clair as Kate Middleton and Dan Amboyer as Prince William. Of note in this second television film was the appearance of Jane Alexander as the Queen and Victor Garber as the Prince of Wales. The film aired on 27 August 2011, in the United States on the Hallmark Channel.







9 January 1982   29 April 2011: Miss Catherine Elizabeth Middleton
29 April 2011   present: Her Royal Highness The Duchess of Cambridge
in Scotland: 29 April 2011   present: Her Royal Highness The Countess of Strathearn

Upon marriage, Catherine became known as "Her Royal Highness The Duchess of Cambridge". A fuller version of her title and style is Her Royal Highness The Duchess of Cambridge, Countess of Strathearn and Lady Carrickfergus. In Scotland, she is also styled as "Her Royal Highness The Countess of Strathearn". In Northern Ireland, she is occasionally known as "Lady Carrickfergus".
Unlike the majority of royal brides, Catherine's immediate family is neither aristocratic nor royal. On the morning of their wedding day on 29 April 2011, at 8:00 am, officials at Buckingham Palace announced that in accordance with royal tradition and on recognition of the day by the Queen, Prince William was created Duke of Cambridge, Earl of Strathearn and Baron Carrickfergus.




Medals
 6 February 2012: Queen Elizabeth II Diamond Jubilee Medal



 Canada
5 July 2011   present: Canadian Ranger



Awards
 Ontario: Their Royal Highnesses The Duke and Duchess of Cambridge Award, University of Waterloo, Waterloo



In September 2013, the Queen granted a conjugal coat of arms to the Duke and Duchess of Cambridge, consisting of their individual arms displayed side by side, beneath a helm and coronet denoting the Duke's status as grandson of the Sovereign. Below is shown the earlier grant of the Duchess's personal arms, impaled with those of her husband.




Middleton's father, Michael, and her paternal ancestors were from Leeds, West Yorkshire. Her paternal great-grandmother, Olive, was a member of the Lupton family, who are described in the City of Leeds Archives as "landed gentry, a political and business dynasty"; previously unpublished pictures revealed in March 2015 that Olive Middleton had grown up on her family's Potternewton Hall Estate alongside her cousin, Baroness von Schunck, n e Kate Lupton. Middleton's paternal ancestors also include her great-great-grandfather, politician Francis Martineau Lupton (1848 1921), whose first cousin, Sir Thomas Martineau, was reported in June 2014 as being the uncle of World War II Prime Minister Neville Chamberlain.
Middleton's maternal ancestors, the Harrisons, were working-class labourers and miners from Sunderland and County Durham. Ancestors through her maternal line include Sir Thomas Conyers, 9th Baronet (1731 1810), who was a descendant of King Edward IV through his illegitimate daughter Elizabeth Plantaganet. Other ancestors are Sir Thomas Fairfax (1475 1520), whose wife Anne Gascoigne was a descendant of King Edward III.



^ As a titled royal, Catherine need not use a surname, but when one is used, it is Mountbatten-Windsor. Many media outlets, however, refer to her by her maiden name, Catherine (or Kate) Middleton.


