The Army Research Laboratory (ARL) is the U.S. Army's corporate research laboratory. ARL is headquartered at the Adelphi Laboratory Center (ALC) in Adelphi, Maryland. Its largest single site is at Aberdeen Proving Ground, Maryland. Other major ARL locations include Research Triangle Park, North Carolina, White Sands Missile Range, New Mexico, Orlando, Florida, and NASA's Glenn Research Center, Ohio and Langley Research Center, Virginia.
In addition to the Army Research Office, ARL has six technical directorates :
Computational and Information Sciences
Human Research and Engineering
Sensors and Electron Devices
Survivability/Lethality Analysis
Vehicle Technology
Weapons and Materials Research



Before the forming of the ARL, the United States Army had research facilities dating back to 1820 when the laboratory at Watertown Arsenal, Massachusetts, studied pyrotechnics and waterproof paper cartridges. This facility would evolve into the Materials Technology Laboratory. Most pre-WWII military research occurred within the military by military personnel, but in 1945, the Army published a policy affirming the need for civilian scientific contributions in military planning and weapons production. Non-military involvement before this time was frequent; however, methods for contribution to warfare technology was on limited and incidental basis. On June 11, 1946, a new research and development division of the War Department General Staff was created; however, due to internal forces within the military which supported the traditional technical service structure the division was closed. A variety of reorganizations took place over the next four decades, which put many organizations in command of Army research and development. Often commanders of these organizations were advocates of the reorganization, while some middle level management was opposed to the change.



The ARL represents the realization of a memorandum dated January 6, 1989 from the LABCOM Commander recommending integrating the corporate laboratories into a single entity. As part of the Base Realignment and Closure of 1989/1991, the consolidated research facilities would be located primarily at Adelphi Laboratory Center and Aberdeen Proving Ground. This would also relocate the majority of operations at MTL to APG. The Federal Advisory Commission reviewed and accepted the creation of ARL in 1992.






The Army Research Office (ARO) funds extramural basic research, that is, research in outside academic and industrial organizations, providing grants to both single investigators and university-affiliated research centers, as well as outreach programs.



The Computational and Information Sciences Directorate (CISD) is the foremost U.S. Army organization for research and development of modern electronic systems. This research supports capabilities in the analysis, distribution, and assimilation of real or simulated digitized battlefield information. In addition to the digitized war initiatives, high performance computing research is performed at a variety of centers under the ARL. The use of supercomputers for mathematical simulations instead of mass fabrication can result in the conservation of human and physical resources. The directorate is a key player in the International Technology Alliance in conjunction with SEDD and HRED directorates.



The U.S. Army Research Laboratory s Human Research and Engineering Directorate (HRED) is the principal Army organization for research and development (R&D) in the human dimension. HRED conducts a broad-based program of scientific research and technology directed toward optimizing Soldier performance and Soldier-machine interactions to maximize battlefield effectiveness. HRED also executes an analysis mission that provides the Army with human factors leadership to ensure that Soldier performance requirements are adequately considered in technology development and system design. HRED coordinates technologies within the Army, other services and their laboratories, industry, and academia to leverage basic and applied research opportunities for the benefit of the Army.



Sensors and Electron Devices Directorate (SEDD) is a group dedicated to produce equipment from tiny chips to fully integrated systems. SEDD also helps develop sensors and electronic devices which become an important part of modern warfare. Other components of this directorate include: multifunction radio frequency equipment, autonomous sensing, power generation and management, and signal processing algorithms. One of the best ways to protect the soldier is to develop autonomous sensing systems for all activities, from intelligence gathering to waging warfare.



The Survivability/Lethality Analysis Directorate (SLAD) is the primary center for expertise in survivability, lethality, and vulnerability of all army systems, across the full range of battlefield threats: ballistic, electronic warfare, information operations, and nuclear, biological, and chemical warfare. SLAD's mission is to assist technology and system developers in optimizing system designs and to provide analytical data to evaluators and decision makers in the Army and the DOD.



The U.S. Army Research Laboratory's Vehicle Technology Directorate (VTD) manages research and development of vehicle propulsion and structure. In addition, VTD conducts analytical and practical experiments in loads analysis, structural dynamics, aero-elasticity, structural acoustics, and vibration reduction. VTD makes ground combat vehicles lighter, more reliable, safer, and more fuel efficient. Air combat vehicles such as the helicopter are studied to decrease the vibrations caused by the rotors. Metallic materials are engineered to increase strength and decrease corrosion through the use of composites. The divisions of VTD are Loads & Dynamics, Structural Mechanics, Engine & Transmission Systems, Engine Components, and Unmanned Vehicles.



The U.S. Army Research Laboratory Weapons and Materials Research Directorate (WMRD) is the principal U.S. Army organization for research and development in weapons and materials technologies. The directorate is charged with making bullets pierce farther and ballistic vests stronger. Although these interests may seem opposed, the goal is to increase the survivability of the soldier. In the WMRD, traditional armaments are studied, as are advanced ballistic defense systems. This directorate is especially linked with VTD in order to produce safer vehicles, transports, and aircraft. One aspect of this directorate's analysis duty is to evaluate military technologies from an economical standpoint in order to reduce overall system costs.



Beginning in 1996, the Army Research Laboratory entered into innovative cooperative agreements with industrial and academic partners to form "federated laboratories," or "FedLabs", to perform research in broad areas important to the U.S. Army where the ARL could extract significant leverage from work being performed in the commercial and academic arenas. The first three FedLabs were in Advanced Displays, Advanced Sensors, and Telecommunications. Each FedLab was a large consortium of companies, universities, and non-profit organizations, with both an overall industrial leader and an ARL leader. The cooperative agreements forming the FedLabs were somewhat unusual in that the ARL was not a mere funder of research, but an active consortium participant. The first three FedLabs won a National Performance Award in 1999 from then-Vice President Al Gore.






Army Research Laboratory website
Historic American Engineering Record (HAER) No. MD-114, "Aurora Pulsed Radiation Simulator, U.S. Army Research Laboratory, Building No. 500, Adelphi, Prince George's County, MD", 19 photos, 70 data pages, 3 photo caption pages