Kythnos (Greek:  ) is a Greek island and municipality in the Western Cyclades between Kea and Serifos. It is 56 nautical miles (104 km) from the harbor of Piraeus. Kythnos is 100 km2 (39 sq mi) in area and has a coastline of about 100 km (62 mi). It has more than 70 beaches, many of which are still inaccessible by road. Of particular note is the crescent-shaped isthmus of fine sand at Kolona.




The island has two significant settlements, the village of Messaria or Kythnos (pop. 561 in 2011 census), known locally as Chora, and the village of Dryopis or Dryopida Dryopida (pop. 325), also known as Chorio. Both villages are notable for their winding and often stepped streets, too narrow for vehicular traffic. The villages are very picturesque but in different architectural styles. Chora has the more-typical flat roofs of the Cyclades, while Dryopida's rooftops are slanted and tiled. Chora is also notable for its large Greek Orthodox Church.
There is also a growing coastal settlement called Kanala on the east side of the island, and many of the larger beaches are settled by a handful of residents. Aghios Dimitrios, at the southern tip of the island, is a mostly modern settlement, with small vacation houses dotting the hillside above a wide beach that is dotted with sea daffodils. On the northeast end of the island lies Loutra (pop. 81), a village famous for its thermal springs, which are said to have curative properties. Although the large tourist hotel there has been closed for several years, the bathhouse is still functioning and visitors may soak in its marble tubs for a modest fee.
The port town is called Merichas (pop. 369), its population significantly fluctuating during the year. Before the 1970s, there were no year-round residents in recent history; a Greek fisherman named Manolas Psaras and his wife Foto were the first to live in the port year-round. Today, there is a growing year-round population, and, especially during the peak of the summer tourist season, the town becomes quite busy. Many residents of the port speak at least some English, the most popular second language. Merihas is connected to Piraeus and to Lavrion by ferry boat, and the ride takes one to four hours, depending on the speed of the ship and the weather. Construction of a new mole began in 2005 to accommodate larger ferryboats and was completed in 2008. Kythnos was until recently considered to be one of the last Cycladic islands unaffected by the impact of tourism, but this is inexorably changing. Still, the island has not yet been overdeveloped, and in the more remote areas of the villages, traditional ways live on relatively unchanged.



Kythnos can lay claim to one of the oldest known habitations in the Cycladic islands, a Mesolithic settlement (10000 BCE   8000 BCE) at Maroulas on the northeast coast. The site, close to the village of Loutra, is situated on the shore, and large portions have eroded into the sea. Excavations in 1996 found intact human skeletons, along with stone artifacts and part of a floor pavement, which indicates a long-term settlement, probably of hunter-gatherers.




Third millennium BCE (First Cycladic Period) findings at Skouries near the highest peak of the island, Mt. Profitis Elias, suggest that Kythnos was a supplier of raw materials for metallurgy to other islands during the Bronze Age. Remains of copper smelting sites and open-air copper mines were investigated in 1984-1985. (A recent paper by Myrto Georgakopoulo points to the seminal work here by Gale and others.)
The earliest inhabitants of the island known to historians were the Kares (Carians), a pre-Hellenic tribe probably allied to or under the dominion of the Minoans, who eventually were forced by pressure of invading tribes to move on and settle in Asia Minor. Herodotus (Bk. viii, 73) records that in the 13th century BCE, another pre-Hellenic tribe, the Dryopes, originally from the Greek mainland near Mount Parnassus, migrated to the islands, first to Euboea and later spreading to Kea, Kythnos, and beyond. This tribe most probably gave rise to the name of one of the two main villages, Dryopis or Dryopida. Some sources say the island took its name from King Kythnos of the Dryopes; others suggest this is a mythical rather than a historical figure. (Speculations on the origin of the name are contained in Vallinda, 1896.) The Dryopes eventually moved on as well under pressure from Ionians, who migrated out of mainland Greece as Dorian tribes moved in from the north.

A Hellenistic site at Vryokastro, above the bay of Episkopi on the northwest part of the island, was partially excavated in the mid-20th century, yielding floor plans of houses and a sanctuary, as well as a few artifacts. In Greek newspaper articles of December 19, 2002, archaeologist Alexandros Mazarakis-Ainian announced a spectacular discovery on this site: an inner sanctum (adyton) of the temple was found intact and unplundered. Over 1,400 objects, including precious jewels and gold, silver and bronze artifacts, terracotta figurines, and painted vases, were excavated from what the archaeologists have determined is a 2,700-year-old temple dedicated to either Hera, queen of the gods, or Aphrodite, goddess of love. The artifacts date mostly from the 7th to the 5th centuries BCE. The site at Vryokastro was inhabited until Roman times. In this era, the islands of the Cyclades suffered frequent predation by pirates, and perhaps for this reason, the main settlements moved inland and to more defensible locations.
Remains of another old settlement, with extensive stone walls, can be seen in the extreme northern headland. This site, called Kastro (Greek for Castle), was likely the capital of the island from about the 1st century through the Byzantine era and into Frankish times. This site seems nearly impregnable: on three sides is a sheer 500-foot (150 m) drop to the sea. The fourth side is approached via a narrow track, which was barricaded with a thick, high wall, parts of which are still extant (along with walls delineating hundreds of houses). Nevertheless, there is evidence that the town was destroyed and rebuilt several times. The population fluctuated dramatically during this period and at times the island was decimated due to marauders and plague (Smith, 1854 and Bent, 1885, reprinted 2002).




Only rarely is the island mentioned by ancient authors. In the Battle of Salamis (480 BCE), Herodotus records that Kythnos contributed a trireme and a penteconter, and this contribution is commemorated on the base of a golden tripod at Delphi (Herodotus, Bk viii, 46).
Innumerable sources repeat, without providing a citation, that Aristotle praised the government of Kythnos in his "Constitution of Kythnos." Exactly what he wrote is difficult to ascertain, since all of his essays on the constitutions of 158 city-states are lost except for the one on Athens. (Possibly, the origin of the quote is from the 2nd century lexicographer Harpocration. See Smith, 1854. The entry on Kythnos is under the spelling "Cythnus").



In 1207, Kythnos came under the rule of the Frankish overlord Marco Sanudo, and it remained in Venetian hands for 400 years. A chronology of the rulers can be found on the history ring website Regnal Chronologies. During this period, it was known as Thermia, a name derived from the hot mineral springs occurring on the northeastern coast at the village of Loutra (which means "baths" in Greek). These medicinal baths were renowned at least since Roman times and were a fashionable spa and resort area. The capital remained at Kastro, which was rebuilt as a Frankish fortress and was known as the Tower of Thermia (Smith, 1854 and Bent, 1885, reprinted 2002).



Following a siege, the Turks were victorious over the last Venetian overlord, Angelo Gozzadini, in 1617. (According to a story recounted by J.L. Bent in his 1885 travelogue on the Cyclades, Kastro fell only because of a treacherous ploy by the Turks: a young woman, heavy with child and apparently in pain, approached the entrance and begged to be admitted; the watchman's daughter opened the gate for her and for the Turks hiding nearby. Bent writes that this version is preserved in a popular island ballad (Bent, 1885, reprinted 2002, chap. 17).
After its fall to the Turks, Kastro was abandoned and the new capital was located inland, as the name Messaria suggests. Later, this town became known as Hora (the generic Greek name for a capital town, also spelled Chora). Under the Ottomans, Kythnos enjoyed religious freedom, but was a poor, under-populated place, still beset by pirates and suffering frequent epidemics. Most of its population succumbed to the plague in 1823. Nevertheless, it was one of the first islands to revolt against the Turks and join Mainland Greece in its fight for independence. During the reign of the first Greek King, Othon, Kythnos was a place of exile for political prisoners and was the scene of an unsuccessful revolt in 1862 by rebels from Syros who attempted to free the prisoners.




In the 19th century, Kythnians mainly earned their living as they had for centuries before: as shepherds or by fishing. The island had few natural resources and, lacking a deep-water mooring for boats, was relatively inaccessible. Then, as the new century dawned, iron ore was discovered on the island and Kythnians were able to supplement their meager incomes by working in the mines. These mines, however, were mostly played out by World War II, and once again, the population of the island went into decline, as young people left to find employment and a better life in Athens or even further afield.
The Greek tourist boom beginning in the mid-20th century largely bypassed Kythnos because its harbor lacked a deep-water dock for ferryboats. The construction of a new mole in 1974 precipitated great changes. Today, the island is a modern, prosperous place, with a burgeoning tourist trade. It is in the forefront of alternative energy experiments, with the establishment in 1983 of Greece s first wind park. With the addition of a photovoltaic system and storage batteries, the amount of diesel fuel required to supply the island s electricity has been reduced by 11%. Numerous individual houses on remote coves are equipped with photovoltaic systems, and nearly all houses employ solar water heaters.
Due to its proximity to Athens, Kythnos has become a fashionable setting for vacation homes, in addition to being an accessible destination for foreign visitors. Besides its numerous beaches and picturesque villages, it also is the site of one of the largest caves in Greece, Katafiki Cave in Dryopida. This cave, first visited in the 1830s and described by the geologist Fiedler, has unique "schratten" or rock curtains, as well as speleotherms. It was the site of an iron mine until 1939 and has now been developed as a tourist attraction.



Timanthes (4th century BC), painter
Giorgos Zambetas 1925-1992,composer



Bent, James Theodore, "The Cyclades: Life Among the Insular Greeks," 1885, reprinted in 2002 by Archaeopress, Oxford. Chapter 17 recounts his impressions of Kythnos
Herodotus, "The History," trans. by David Greene, Chicago: University of Chicago Press, paperback edition, 1988
Smith, William, ed., "Dictionary of Greek and Roman Geography," 1854. Can be accessed at the Tufts University Perseus Project.
Vallinda, Antoniou, "History of the Island of Kythnos," monograph (in Greek), 1896, reprinted by the Syndesmos Kythnion, Athens, 1990.






Official website (English) (Greek)