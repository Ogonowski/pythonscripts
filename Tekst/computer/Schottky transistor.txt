A Schottky transistor is a combination of a transistor and a Schottky diode that prevents the transistor from saturating by diverting the excessive input current. It is also called a Schottky-clamped transistor.




Standard transistor-transistor logic (TTL) uses transistors as saturated switches. A saturated transistor is turned on hard, which means it has a lot more base drive than it needs for the collector current it is drawing. The extra base drive creates a stored charge in the base of the transistor. The stored charge causes problems when the transistor needs to be switched from on to off: while the charge is present, the transistor is on; all the charge must be removed before the transistor will turn off. Removing the charge takes time (called storage time), so the result of saturation is a delay between the applied turn-off input at the base and the voltage swing at the collector. Storage time accounts for a significant portion of the propagation delay in the original TTL logic family.
Storage time can be eliminated and propagation delay can be reduced by keeping the switching transistors from saturating. Schottky transistors prevent saturation and the stored base charge. A Schottky transistor places a Schottky diode between the base and collector of the transistor. As the transistor comes close to saturating, the Schottky diode conducts and shunts any excess base drive to the collector. (This saturation avoidance technique is used in the 1956 Baker clamp.) The resulting transistors, which do not saturate, are Schottky transistors. The Schottky TTL logic families (such as S and LS) use Schottky transistors in critical places.



When forward biased, a Schottky diode's voltage drop is much less than a standard silicon diode's, 0.25 V versus 0.6 V. In a standard saturated transistor, the base-to-collector voltage is 0.4 V. In a Schottky transistor, the Schottky diode shunts current from the base into the collector before the transistor goes into saturation.
The input current which drives the transistor in active state sees two paths, one from base to Schottky diode to collector to emitter and the other from base to emitter. When transistor conducts it will develop 0.6 V across its base and emitter. The same voltage will appear across shunt path along the Schottky diode and C to E. Schottky will give 0.25 V drop so remaining 0.35 will occur at C to E branch. So transistor will not go in saturation because it has 0.2 volt across C to E branch in saturation.



In 1956, Richard Baker described some discrete diode clamp circuits to keep transistors from saturating. The circuits are now known as Baker clamps. One of those clamp circuits used a single germanium diode to clamp a silicon transistor in a circuit configuration that is the same as the Schottky transistor. The circuit relied on the germanium diode having a lower forward voltage drop than a silicon diode would have.
In 1964, James R. Biard filed a patent for the Schottky transistor. In his patent the Schottky diode prevented the transistor from saturating by minimizing the forward bias on the collector-base transistor junction, thus reducing the minority carrier injection to a negligible amount. The diode could also be integrated on the same die, it had a compact layout, it had no minority carrier charge storage, and it was faster than a conventional junction diode. His patent also showed how the Schottky transistor could be used in DTL circuits and improve the switching speed of saturated logic designs, such as the Schottky-TTL, at a low cost.


