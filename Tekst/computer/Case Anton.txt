Operation Anton or Fall Anton in German, was the codename for the military occupation of Vichy France carried out by Germany and Italy in November 1942. It marked the end of the Vichy regime as a nominally independent state and the disbandment of its army (the severely limited "Armistice Army"), though it continued its existence as a puppet government in occupied France. One of the last actions of its armed forces before their dissolution was the scuttling of the French navy in Toulon to prevent it from falling into Axis hands.




A German plan to occupy Vichy France had originally been drawn up in December 1940 under the codename of Operation Attila, and soon came to be considered as a single operation with Operation Camellia, the plan to occupy Corsica. Operation Anton updated the original Operation Attila, including different German units and adding Italian involvement.
Following the Allied landings in French North Africa on 8 November 1942 (Operation Torch), Adolf Hitler could not risk an exposed flank on the French Mediterranean. Following a final conversation with French Premier Pierre Laval, Hitler gave orders for Corsica to be occupied on 11 November, and Vichy France the following day.




By the evening of 10 November 1942, Axis forces had completed their preparations for Anton. The German First Army advanced from the Atlantic coast, parallel to the Spanish border, while the German Seventh Army advanced from central France towards Vichy and Toulon both armies under the command of General Johannes Blaskowitz. The Italian 4th Army occupied the French Riviera and an Italian division landed on Corsica. By the evening of 11 November, German tanks had reached the Mediterranean coast.
The Germans formulated Operation Lila with the aim of capturing intact the demobilised French fleet at Toulon. French naval commanders, however, managed to delay the Germans by negotiation and subterfuge long enough to scuttle their ships on 27 November, before the Germans could seize them, preventing three battleships, seven cruisers, 28 destroyers and 20 submarines from falling into the hands of the Axis powers. While the German Naval War Staff were disappointed, Adolf Hitler considered that the elimination of the French fleet sealed the success of Operation Anton. The destruction of the fleet also denied it to Charles de Gaulle and the Free French Navy.
Other than that, Vichy France limited its resistance to radio broadcasts objecting to the violation of the armistice of 1940. The 50,000-strong Vichy French Army initially took defensive positions around Toulon, but when confronted by German demands to disband, lacked the military capability to resist the Axis forces. It was disbanded shortly thereafter.


