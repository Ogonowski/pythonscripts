Communications in the Turks and Caicos Islands



Telephones - main lines in use: 3,000 (1994)
Telephones - mobile cellular: 0 (1994)
Telephone system: fair cable and radiotelephone services
domestic: NA
international: 2 submarine cables; satellite earth station - 1 Intelsat (Atlantic Ocean)



Radio broadcast stations: AM 3 (one inactive), FM 6, shortwave 0 (1998) A partial list of AM/FM/SW stations in the Turks and Caicos Islands is provided below:
VSI-AM 1460
ZVIC-FM 96.7
VSI-FM 101.9
ZIBF-FM 105.5
ZIBS-FM 107.1
VSI-8 4780 kHz
VSI-35 8000 kHz
Radios: 8,000 (1997)



Television broadcast stations: 2
WIV Cable has been operating on the islands for over 10 years (Channel 4)
New to Turks & Caicos, TCeyeTV started broadcasting on 3 July 2007
broadcasts from The Bahamas are also received; cable television is established) (1997)



Internet Service Providers (ISPs): 3 (2013)
LIME (ADSL)
WIV (DSL)
Islandcom (4G)
Country code (Top level domain): TC