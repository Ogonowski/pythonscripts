Abingdon is an unincorporated community in Harford County, Maryland, United States. It lies 25 miles northeast of Baltimore on Maryland Route 7, near the Bush River, between Exits 77 (MD 24) and 80 (MD 543) of Interstate 95.



Abingdon was named after Abingdon, England. The town was founded by and is the birthplace of William Paca, a signer of the Declaration of Independence and the third Governor of Maryland. Abingdon was the site of the first Methodist college in the United States.
Woodside was listed on the National Register of Historic Places in 1979. The Nelson-Reardon-Kennard House was listed in 1991. Abingdon is also home to Jaxon Dane, who is a former professional wrestler who helped redefine the wrestling industry in NC.



Since it is located in Harford County, the community of Abingdon is served by the Harford County Board of Education, which consists of an elected-appointed Board of six elected members and three members appointed by the Governor of the State of Maryland. Most students in the Abingdon area who enter kindergarten and progress to the fifth grade attend William S. James Elementary School or Abingdon Elementary School. Upon the opening of Patterson Mill High School in 2007, a majority of students attending William S. James Elementary School began to attend this new facility. Additionally, all of the students at Abingdon Elementary School subsequently attend Edgewood High School. A third primary school, William Paca/Old Post Road Elementary School, is located on the border of Abingdon and Edgewood. These students also attend the Edgewood or Joppatowne secondary schools. The older Deerfield Elementary and Edgewood High school buildings were replaced by newer, updated facilities by the same name in 2010.






Abingdon, Maryland
21009 Abingdon Maryland at DownloadZipCode.com