HP Software is the Enterprise software segment of information technology company Hewlett-Packard (HP).
According to Software Magazine, HP is the 3rd largest software company in the world in total software revenue, behind IBM and Microsoft as the first and second largest, respectively. HP claims that the HP Software segment is effectively the globe's sixth-largest software vendor with 5,000 partners and 50,000 customers, and works with 94 Fortune 100 companies.



HP Software sells several categories of software, including: business service management software, application lifecycle management software, mobile apps, big data and analytics, service and portfolio management software, automation and orchestration software, and enterprise security software. HP Software also provides Software as a service (SaaS), cloud computing solutions, and software services, including consulting, education, professional services, and support.
Enterprise Security Products Include:
ArcSight
Fortify
TippingPoint
Atalla



The HP Business Service Management (BSM) software portfolio provides end-to-end visibility into the health of business services, powered by intelligence and analytics.
HP Business Service Management software manages business service health in dynamic, heterogeneous environments; provides service Intelligence to address known and unknown issues; analyzes, anticipates, and remediates service health issues; and addresses performance and availability across applications, systems, networks, and storage, over cloud, virtual, and traditional IT environments. HP BSM software provide information about the health of IT services by automating the correlation and analysis of consolidated data, including machine data, logs, events, topology, and performance information.



HP Application Lifecycle Management (ALM) software provides a single repository for application teams to plan, build, collaborate, test, and accelerate the delivery of secure, reliable applications.
HP Application Lifecycle Management software helps organize and manage all phases of the application lifecycle management process, including defining releases, specifying requirements, test management including planning and executing tests and tracking defects.
HP Application Lifecycle Management helps to manage the application lifecycle from requirements through readiness for delivery using a consistent user experience and customizable dashboards. The software includes dashboards, cross-project reporting, and the ability to share assets.



HP Mobile Apps software provides end-to-end management of enterprise applications, including application lifecycle management and end-to-end monitoring and management of mobile applications. HP Mobile Apps software includes mobile application development, mobile application monitoring, and mobile testing.
In May 2013, HP launched a mobile-focused version of HP Anywhere, the company's platform for building, distributing, and managing mobile apps for the enterprise. A mobile enterprise application platform, HP Anywhere allows developers to deliver apps through mobile containers and connect them to back-end systems. These containers, available on the Apple Store and Google Play, are designed to take processes that are managed by legacy systems and deliver them with a strong mobile interface. HP Anywhere works on HTML5, JavaScript, and Apache Cordova and support Sencha Touch, Enyo, and jQuery Mobile. The platform also integrates with mobile device management software players such as SAP Afaria.
HP also announced the HP Anywhere Developer Zone, designed to provide developers access to HP software developer kits (SDKs), demo applications, and application cookbooks.



HP provides big data and analytics software and hardware that locate, understand, analyze, and act on data, regardless of the format, language, or location.
In June 2013, HP announced the HAVEn platform, a big data platform that leverages HP s analytics software, hardware, and services to create the big data-ready analytics applications. HAVEn stands for Hadoop, Autonomy Corporation, Vertica, HP Enterprise Security Products and "n" number of applications being brought together in a framework. Autonomy is HP's software for unstructured data search and analysis. Vertica is HP's columnar, massively parallel processing analytical database, which is also available on a ready-to-run, expand-as-needed HP appliance. The Enterprise Security products include security monitoring and risk management.



HP Service and Portfolio Management software is designed to help IT manage a portfolio of services and includes project and portfolio management, IT service management (ITSM), asset management, and configuration management systems.
HP Project and Portfolio Management (PPM) is web-based software that provides visibility into strategic and operational demand, resource productivity and utilization, and associated financial information across a portfolio.
HP IT service management software integrates and automates service management across hybrid environments and provides quality control for IT services. HP IT service management aligns with the IT Service Lifecycle described by Information Technology Infrastructure Library version 3 (ITIL v3), supporting ITIL change management, using a lifecycle approach.
HP asset management software manages the lifecycle of IT assets. Related capabilities include a configuration management system (CMDB) for configuration data and relationship mapping and a  universal discovery  tool for automated hardware and software discovery.



HP automation, orchestration, and cloud management software is an integrated portfolio that helps automate the lifecycle of IT services from routine, repetitive tasks and operations to automated application delivery in the data center or as a cloud service.
In May 2013, HP released version 10 of HP Operations Orchestration, version 10 of HP Server Automation, and version 10 of HP Database and Middleware Automation 10 and packaged these software programs, along with HP Cloud Service Automation 3.2, into a single integrated package. The new package was designed to speed deployment of application-based services across a hybrid cloud environment. HP also released the Server Automation software as an appliance called HP Server Automation Standard. It includes much of the core configuration, deployment, and compliance checking functionality found in the full software edition.



Based on products from ArcSight, Atalla, Fortify Software, and TippingPoint, HP provides enterprise IT security solutions, including cyber security products. HP s approach to security is to disrupt the lifecycle of an attack with prevention and real-time threat detection, from the application layer to the hardware and software interface.
In September 2013, HP announced HP security offerings that provide real-time threat disruption and self-healing technology combined with crowd-sourced security intelligence. These offerings included HP Threat Central, a community-sourced security intelligence platform to facilitate automated, real-time collaboration among organizations in addressing active cyberthreats; HP TippingPoint Next-Generation Firewall (NGFW); HP ArcSight Application View, HP ArcSight Management Center, HP ArcSight Risk Insight and HP ArcSight Enterprise Security Manager (ESM) v6.5c; HP SureStart self-healing technology that automatically restores a system s PC Basic Input/Output System (BIOS) to its previously safe state if attacked or corrupted; HP Supplier Security Compliance Solution; HP Continuous Monitoring for the U.S. Public Sector; HP Distributed Denial of Services (DDoS) Protection Services; and HP Security Risk and Controls Advisory Service for Mobility.



HP delivers several software capabilities through the cloud, using the Software as a Service (SaaS) model, including application lifecycle management, business service management, and IT service management products.



A user group, the HP Software Solutions Community, officially launched publicly in April 2010 and includes all former software-related communities and is designed for IT practitioners.
While it is not an actual "user group", the ITRC is an online forum about HP Software products. In June 2011, the ITRC was folded into the HP Software Solutions Community.
In June 2011, HP Software announced a new Discover Performance community and online resource center designed to serve IT executives and CIOs.
HP also hosts an Enterprise Software IT Experts Community forum.



In 2011, HP Enterprise Business, along with participating independent user groups, combined its annual HP Software Universe, HP Technology Forum and HP Technology@Work into a single event, HP Discover. There are two HP Discover events annually, one for the Americas and one for Europe, Middle East and Africa (EMEA). HP Discover 2013 took place on June 10 12 in Las Vegas, Nevada and is scheduled for December 10 12 in Barcelona, Spain.



Apr. 2014: Shunra, provider of network virtualization products for software testing
Oct. 2011: Autonomy Corporation, provider of enterprise search and knowledge management applications solutions
Mar. 2011: Vertica Systems, analytic database management software
Oct. 2010: ArcSight, security management software
Aug. 2010: Stratavia, database and application automation software
Aug. 2010: Fortify Software, software security assurance solutions
May 2008: Tower Software, document and records management software
Jan. 2008: Exstream Software, variable data publishing software
July 2007: Opsware, data center automation software
June 2007: SPI Dynamics, Web applications security software
Feb. 2007: Bristol Technology, Inc., business transaction monitoring technologies
Feb. 2007: PolyServe, Inc., storage software for application and file serving utilities
Feb. 2006: OuterBay, archiving software for enterprise applications and databases
Nov. 2006: Mercury Interactive Corporation, application management, application delivery and IT governance software
Dec. 2006: Bitfone, mobile device software
Dec. 2006: Knightsbridge Solutions LLC, a datawarehousing consulting and integration services company
Nov. 2005: Trustgenix, Inc., federated identity management solutions
Sept. 2005: Peregrine Systems Inc., asset and service management software
Sept. 2005: AppIQ, open storage area network management and storage resource management technologies






HP Software official site
HP Software Solutions Community
ArcSight
Fortify
TippingPoint
Atalla
Protect 724 Community
HP LoadRunner fundamentals