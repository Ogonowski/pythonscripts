The Syrmian Front (Serbo-Croatian: Srijemski front,    ) was an Axis line of defense during World War II. It was established as part of the Eastern Front in late October 1944 in Syrmia and east Slavonia, northwest of Belgrade.
After the Yugoslav Partisans and the Red Army expelled the Germans from Belgrade in the Belgrade Offensive, the retreating Wehrmacht and the Croatian Armed Forces used fortifications to protect the withdrawal of German Army Group E from the Balkans. With help from their Soviet allies, the Partisans (by then recognized as the Yugoslav army), joined by Bulgarian and Italian forces, fought a difficult winter campaign and finally broke through the front on 12 April 1945.
After the Syrmian front was broken, occupied Yugoslavia was liberated.




After the September advance through Romania and Bulgaria in October 1944, The Red Army, together with Yugoslav forces, took Belgrade (central communication node of the Balkans) in the Belgrade Offensive. Due to Yugoslav partisan activity, the Yugoslav-Allied Operation Ratweek, and pressure from the Bulgarian Army, the Germans failed to prevent this while they awaited the redeployment of Army Group E troops from Greece. The Red Army decided to exploit this delay and continued to advance with the 3rd Ukrainian Front from Belgrade to south-west Hungary. The aim of the advance was to separate and protect their main attack in Hungary from attacks on the flank by Army Group E from the south.
From September 1944 to January 1945, Army Group E pushed its way through Macedonia, Kosovo, Sanjak, and Bosnia, and soon their sole available escape route was in a line between Sarajevo and Slavonski Brod. For this reason, it was of vital significance for the Germans to defend the zone around Slavonski Brod, which was threatened by the Soviet-Yugoslav advance through Syrmia. To prevent Army Group E from being cut off, the German South-East command prepared seven successive fortified defense lines between the Danube and Sava river from Ruma to Vinkovci. The Syrmian Front campaign consisted of Yugoslavian attempts to break through these lines of defense.




The Syrmian Front saw some of the most difficult fighting in Yugoslavia in World War II. It lasted for almost six months. As the bulk of the Red Army involved in the Belgrade operation continued their offensive in Hungary, the Yugoslav Army, accustomed to guerrilla warfare in the mountainous terrain of the Dinaric Alps, remained to fight the entrenched front line heavily contested by the Axis on the flat ground of the Pannonian plain. Young men from Vojvodina and Central Serbia, many from freshly liberated regions, were drafted en masse and sent to the front, and the amount of training they received and their casualty levels remain in dispute.
Although mostly stationary, the front moved several times, generally westward, as the Axis forces were pushed back. The fighting started east of Ruma and stabilized in January 1945 west of  id after the town changed hands due to Axis counterattacks. In late March and early April 1945, Yugoslav Army units mounted a general offensive on all fronts. The Yugoslav First Army, commanded by Peko Dap evi , broke through German XXXIV Corps defenses in Syrmia on 12 April, quickly capturing the cities of Vukovar, Vinkovci, and  upanja, and enabling further advances through Slavonia toward Slavonski Brod and Zagreb in the last month of the war.
The campaign can be divided into four distinct phases:
The first phase lasted from October 24 to the end of December 1944, and was characterized by slow but steady advancement of Yugoslav and Soviet forces through the seven German fortified lines of defense through fierce battles and heavy losses on both sides.
In the second phase, from 3 January to 26 January 1945, the Germans performed a successful counterattack with the newly arrived forces of XXXIV Army Corps of Army Group E, and succeeded in winning back to the Nibelung Line, the main line of defense in Syrmia, while inflicting heavy losses to the Yugoslav Army.
The third phase was a stalemate period from 26 January to 12 April 1945. In this period both sides only performed limited reconnaissance activities.
The fourth phase began when Yugoslav forces broke through the German defense lines on 12 April, with heavy German losses and fierce battles and Army Group E retreating.




According to controversial Serbian historian Sr an Cvetkovi , the partisans used the front as a way to undertake reprisals against individuals perceived to be collaborators or identified as "enemies of the people" (those holding values opposed to Communism), which resulted in the deaths of thousands of young bourgeois men from Belgrade and other recently captured Serbian cities. The men were sent to the front rather than explicitly liquidated.






Cvetkovi , Sr an (2005). Izme u srpa i  eki a (in Serbian). Belgrade: Institut za savremenu istoriju. Retrieved 19 April 2011.