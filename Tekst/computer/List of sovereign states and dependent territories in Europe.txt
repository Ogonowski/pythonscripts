The list below includes all entities falling even partially under any of the various common definitions of Europe, geographical or political. Fifty six sovereign states, six of which have limited recognition, are listed with territory in Europe and/or membership in international European organisations. There are eight areas that are not integral parts of a European state or have special political status.




Under the commonly used geographic definition, the border between Asia and Europe stretches along the Ural Mountains, Ural River, and Caspian Sea in the east, the Greater Caucasus range and the Black Sea, with its outlets, the Bosporus and Dardanelles, in the south. Based on that division, transcontinental states Azerbaijan, Georgia, Kazakhstan, Russia, and Turkey have territory both in Europe and Asia.
The island of Cyprus in West Asia is approximate to Anatolia (or Asia Minor) and is on the Anatolian Plate  but is often considered part of Europe as a current member of the European Union (EU). Armenia is entirely in West Asia also but is a member of certain European organisations. Sometimes Israel is considered as part of geopolitical Europe as well.
Although the Mediterranean Sea provides a clearer divide between Africa and Europe, some traditionally European islands such as Malta , Sicily, Pantelleria and the Pelagian Islands are located on the African continental plate. The island of Iceland is part of the Mid-Atlantic Ridge, straddling the Eurasian Plate and the North American Plate.
Some territories geographically outside Europe have strong connections with European states. Greenland has socio-political connections with Europe and is part of the Kingdom of Denmark, but is grouped with the continent of North America.
Other territories are part of European countries but are geographically located in other continents, such as the French overseas departments, the Spanish cities of Ceuta and Melilla on the coast of Africa, and the Dutch Caribbean territories of Bonaire, Saba and Sint Eustatius.



A sovereign state is a political association with effective sovereignty over a population for whom it makes decisions in the national interest. According to the Montevideo convention, a state must have a permanent population, a defined territory, a government, and the capacity to enter into relations with other states.



There are 50 internationally recognized sovereign states with territory located within the common definition of Europe and/or membership in international European organisations, of which 44 have their capital city within Europe. All except the Vatican City are members of the United Nations (UN), and all except Belarus, Kazakhstan and Vatican City are members of the Council of Europe. Since 2013, 28 of these countries are also member states of the EU, which means they are highly integrated with each other and share their sovereignty with EU institutions.
Each entry in the list below has a map of its location in Europe. Territory in Europe is shown in dark-green; territory not geographically in Europe is shown in a lighter shade of green. The lightest shade of green represents states in the EU and is shown on the maps of all territories within the EU.



The following six entities in Europe have partial diplomatic recognition by one or more UN member states (and therefore are defined as states by the constitutive theory of statehood) or have no diplomatic recognition by any UN member state but are defined as states by the declarative theory of statehood and are recognised by one or more Non-UN member states. None are members of the UN, Council of Europe or EU.



The following six European entities are dependent territories.



The following places are considered integral parts of their controlling state, but have a political arrangement which was decided through an international agreement.



Lists of European countries:
by date of formation
by GDP
by GDP per capita
by GDP PPP

by geographic area

International organisations in Europe
List of former sovereign states in Europe
Predecessors of sovereign states in Europe
Timeline of European nations
Geopolitical divisions of Europe





