Japan had an official slave system from the Yamato period (3rd Century) until the end of the Sengoku period.



The export of a slave from Japan is recorded in 3rd century Chinese historical record, but it is unclear what system was involved, and whether this was a common practice at that time. These slaves were called Seik  ( ) (lit. "living mouth"). The export of slaves from Japan ceased, in part because of Japanese separation from the Sinocentrism.
In the 8th century, slaves were called Nuhi ( ) and laws were issued under Ritsuryousei ( ). These slaves tended farms and worked around houses. Information on the slave population is sketchy, but the proportion of slaves are estimated to be around 5% of the population.
Slavery persisted into the Sengoku period (1467-1615) even though the attitude that slavery was anachronistic seems to have become widespread among elites. In 1590, slavery was officially banned under Toyotomi Hideyoshi; but forms of contract and indentured labor persisted alongside the period penal codes' forced labor. Somewhat later, the Edo period penal laws prescribed "non-free labor" for the immediate family of executed criminals in Article 17 of the Got ke reij  (Tokugawa House Laws), but the practice never became common. The 1711 Got ke reij  was compiled from over 600 statutes promulgated between 1597 and 1696.



After the Portuguese first made contact with Japan in 1543, a large scale slave trade developed in which Portuguese purchased Japanese as slaves in Japan and sold them to various locations overseas, including Portugal itself, throughout the sixteenth and seventeenth centuries. Many documents mention the large slave trade along with protests against the enslavement of Japanese. Japanese slaves are believed to be the first of their nation to end up in Europe, and the Portuguese purchased large amounts of Japanese slave girls to bring to Portugal for sexual purposes, as noted by the Church in 1555. Sebastian of Portugal feared that it was having a negative effect on Catholic proselytization since the slave trade in Japanese was growing to massive proportions, so he commanded that it be banned in 1571
Japanese slave women were even sold as concubines to black African crewmembers, along with their European counterrparts serving on Portuguese ships trading in Japan, mentioned by Luis Cerqueira, a Portuguese Jesuit, in a 1598 document. Japanese slaves were brought by the Portuguese to Macau, where some of them not only ended up being enslaved to Portuguese, but as slaves to other slaves, with the Portuguese owning Malay and African slaves, who in turn owned Japanese slaves of their own.
Hideyoshi was so disgusted that his own Japanese people were being sold en masse into slavery on Kyushu, that he wrote a letter to Jesuit Vice-Provincial Gaspar Coelho on 24 July 1587 to demand the Portuguese, Siamese (Thai), and Cambodians stop purchasing and enslaving Japanese and return Japanese slaves who ended up as far as India. Hideyoshi blamed the Portuguese and Jesuits for this slave trade and banned Christian proselytizing as a result.
Some Korean slaves were bought by the Portuguese and brought back to Portugal from Japan, where they had been among the tens of thousands of Korean prisoners of war transported to Japan during the Japanese invasions of Korea (1592 98). Historians pointed out that at the same time Hideyoshi expressed his indignation and outrage at the Portuguese trade in Japanese slaves, he himself was engaging in a mass slave trade of Korean prisoners of war in Japan.
Fillippo Sassetti saw some Chinese and Japanese slaves in Lisbon among the large slave community in 1578, although most of the slaves were blacks.
The Portuguese "highly regarded" Asian slaves like Chinese and Japanese, much more "than slaves from sub-Saharan Africa". The Portuguese attributed qualities like intelligence and industriousness to Chinese and Japanese slaves which is why they favored them more.
In 1595 a law was passed by Portugal banning the selling and buying of Chinese and Japanese slaves.




In the first half of the Sh wa era, as the Empire of Japan annexed Asian countries, from the late 19th century onwards, archaic institutions including slavery were abolished in those countries. However, during the Second Sino-Japanese War and the Pacific War, the Japanese military used millions of civilians and prisoners of war as forced labor, on projects such as the Burma Railway.
According to a joint study by historians including Zhifen Ju, Mitsuyoshi Himeta, Toru Kubo and Mark Peattie, more than 10 million Chinese civilians were mobilized by the K a-in (East Asia Development Board) for forced labour. According to the Japanese military's own record, nearly 25% of 140,000 Allied POWs died while interned in Japanese prison camps where they were forced to work (U.S. POWs died at a rate of 27%). More than 100,000 civilians and POWs died in the construction of the Burma Railway. The U.S. Library of Congress estimates that in Java, between 4 and 10 million romusha (Japanese: "manual laborer"), were forced to work by the Japanese military. About 270,000 of these Javanese laborers were sent to other Japanese-held areas in South East Asia. Only 52,000 were repatriated to Java, meaning that there was a death rate as high as 80%. (For further details, see Japanese war crimes.)
According to the Korean historians, Approximately 670,000 Koreans, were conscripted into labor from 1944 to 1945 by the National Mobilization Law. About 670,000 of them were taken to Japan, where about 60,000 died between 1939 and 1945 due mostly to exhaustion or poor working conditions. Many of those taken to Karafuto Prefecture (modern-day Sakhalin) were trapped there at the end of the war, stripped of their nationality and denied repatriation by Japan; they became known as the Sakhalin Koreans. The total deaths of Korean forced laborers in Korea and Manchuria for those years is estimated to be between 270,000 and 810,000.

According to the United States House of Representatives House Resolution 121, As many as 200,000 "comfort women"  mostly from Korea and China, and some other countries such as the Philippines, Taiwan, Burma, the Dutch East Indies, Netherlands, and Australia were forced into sexual slavery during World War II to satisfy Japanese Imperial Army and Navy members. While apologies have been handed out by the Japanese government and government politicians including the Asian Women's fund which grants donated financial compensations to former comfort women, the Japanese government has also worked to downplay its use of comfort women in recent times claiming that all compensations for its war conduct were resolved with post war treaties such as the Treaty of San Francisco, with an example including asking the mayor of Palisades Park, New Jersey to take down a memorial in memory of the women.




History of slavery in Asia




http://www.iiclo.or.jp/100books/1946/htm/050main.htm (Japanese)
Dias, Maria Suzette Fernandes (2007), Legacies of slavery: comparative perspectives, Cambridge Scholars Publishing, p. 238, ISBN 1-84718-111-2 



Nelson, Thomas. "Slavery in Medieval Japan," Monumenta Nipponica 2004 59(4): 463-492