Windows 9x is a generic term referring to a series of Microsoft Windows computer operating systems produced from 1995 to 2000, which were based on the Windows 95 kernel and its underlying foundation of MS-DOS, both of which were updated in subsequent versions. This includes all versions of Windows 95, Windows 98, and Windows ME.
Windows 9x is predominantly known for its use in desktops. In 1998, Windows made up 82% of operating system market share.
Internal release versions for versions of Windows 9x are 4.x. Previous MS-DOS based versions of Windows used version numbers of 3.2 or lower. Windows NT, which was aimed at professional users such as networks and businesses, used a similar but separate version number between 3.1 and 4.0.
The final version of Windows 9x was Windows ME, released in September 2000 with an internal version number of 4.9. All future versions of Windows, starting with Windows 2000 and Windows XP would be based on the Windows NT codebase.






The first independent version of Microsoft Windows, version 1.0, released on 20 November 1985, achieved little popularity. It was originally going to be called "Interface Manager" but Rowland Hanson, the head of marketing at Microsoft, convinced the company that the name Windows would be more appealing to consumers. Windows 1.0 was not a complete operating system, but rather an "operating environment" that extended MS-DOS, and shared the latter's inherent flaws and problems.
The second release of Microsoft Windows, version 2.0, came out on 9 December 1987, and used the real-mode memory model, which confined it to a maximum of 1 megabyte of memory. In such a configuration, it could run under another multitasking system like DESQview, which used the 286 Protected Mode.
Microsoft Windows scored a significant success with Windows 3.0, released in 1990. In addition to improved capabilities given to native applications, Windows also allowed users to better multitask older MS-DOS based software compared to Windows/386, thanks to the introduction of virtual memory.
Microsoft developed Windows 3.1, which included several minor improvements to Windows 3.0, but primarily consisted of bugfixes and multimedia support. It also excluded support for Real mode, and only ran on an 80286 or better processor. Later Microsoft also released Windows 3.11, a touch-up to Windows 3.1 which included all of the patches and updates that followed the release of Windows 3.1 in 1992.
Meanwhile, Microsoft continued to develop Windows NT. The main architect of the system was Dave Cutler, one of the chief architects of VMS at Digital Equipment Corporation (later purchased by Compaq, now part of Hewlett-Packard). Microsoft hired him in August 1988 to create a successor to OS/2, but Cutler created a completely new system instead.
Microsoft announced at the conference its intentions to develop a successor to both Windows NT and Windows 3.1's replacement (Windows 95, code-named Chicago), which would unify the two into one operating system. This successor was codenamed Cairo. In hindsight, Cairo was a much more difficult project than Microsoft had anticipated and, as a result, NT and Chicago would not be unified until Windows XP.




After Windows 3.11, Microsoft began to develop a new consumer oriented version of the operating system code-named Chicago. Chicago was designed to have support for 32-bit preemptive multitasking like OS/2 and Windows NT, although a 16-bit kernel would remain for the sake of backward compatibility. The Win32 API first introduced with Windows NT was adopted as the standard 32-bit programming interface, with Win16 compatibility being preserved through a technique known as "thunking". A new GUI was not originally planned as part of the release, although elements of the Cairo user interface were borrowed and added as other aspects of the release (notably Plug and Play) slipped.
Microsoft did not change all of the Windows code to 32-bit; parts of it remained 16-bit (albeit not directly using real mode) for reasons of compatibility, performance and development time. Additionally it was necessary to carry over design decisions from earlier versions of Windows for reasons of backwards compatibility, even if these design decisions no longer matched a more modern computing environment. These factors immediately began to impact the operating system's efficiency and stability.
Microsoft marketing adopted Windows 95 as the product name for Chicago when it was released on August 24, 1995.
Microsoft went on to release five different versions of Windows 95:
Windows 95 - original release
Windows 95 A - included Windows 95 OSR1 slipstreamed into the installation.
Windows 95 B - (OSR2) included several major enhancements, Internet Explorer (IE) 3.0 and full FAT32 file system support.
Windows 95 B USB - (OSR2.1) included basic USB support.
Windows 95 C - (OSR2.5) included all the above features, plus IE 4.0. This was the last 95 version produced.
OSR2, OSR2.1 and OSR2.5 were not released to the general public; rather, they were available only to OEMs that would preload the OS onto computers. Some companies sold new hard drives with OSR2 preinstalled (officially justifying this as needed due to the hard drive's capacity).
The first Microsoft Plus! add-on pack was sold for Windows 95.




On 25 June 1998, Microsoft released Windows 98. It included new hardware drivers and better support for the FAT32 file system which allows support for disk partitions larger than the 2 GB maximum accepted by Windows 95. The USB support in Windows 98 is more robust than the basic support provided by the OEM editions of Windows 95. It also controversially integrated the Internet Explorer browser into the Windows GUI and Windows Explorer file manager.
In 1999, Microsoft released Windows 98 Second Edition, an interim release whose notable features were the addition of Internet Connection Sharing and improved WDM audio and modem support. Internet Connection Sharing is a form of network address translation, allowing several machines on a LAN (Local Area Network) to share a single Internet connection. Windows 98 Second Edition has certain improvements over the original release. Hardware support through device drivers was increased. Many minor problems present in the original Windows 98 were found and fixed which make it, according to many, the most stable release of Windows 9x family to the extent that commentators used to say that Windows 98's beta version was more stable than Windows 95's final (gamma) version.




In September 2000, Microsoft introduced Windows ME (Millennium Edition), which upgraded Windows 98 with enhanced multimedia and Internet features. It also introduced the first version of System Restore, which allowed users to revert their system state to a previous "known-good" point in the case of system failure. The first version of Windows Movie Maker was introduced as well.
Windows ME was conceived as a quick one-year project that served as a stopgap release between Windows 98 and Windows XP. Many of the new features were available from the Windows Update site as updates for older Windows versions. As a result, Windows ME was not acknowledged as a distinct operating system along the lines of 95 or 98, and is often included in the Windows 9x series.
Windows ME was criticized by users for its instability and unreliability, due to frequent freezes and crashes. A PC World article dubbed Windows ME the "Mistake Edition" and placed it 4th in their "Worst Tech Products of All Time" feature.
The inability of users to easily boot into Real-Mode 16bit MS-DOS mode, as in Windows 95 and 98, led users to quickly learn how to hack their Windows ME installations to provide the needed service.



The release of Windows 2000 marked a shift in the user experience between the Windows 9x series and the Windows NT series. Where NT4 suffered from a lack of USB support, Plug and Play support, and no support for DirectX preventing users to play contemporary games on Windows NT 4, Windows 2000 had a modern interface, and better USB and plug and play support than any version of Windows 9x.
The release of Windows XP signalled a change of direction for Microsoft, bringing the consumer and business operating systems together.
One by one, support for the Windows 9x series ended, and Microsoft stopped selling the software to end users, then later to OEMs. By March 2004, it was impossible to purchase any copy of any Windows in the 9x series.



Microsoft continued to support the use of the Windows 9x series until July 11, 2006, when extended support ended for Windows 98, Windows 98 Second Edition (SE), and Windows Millennium Edition (Me).
Microsoft DirectX, a set of standard gaming APIs, stopped being updated on Windows 95 at Version 8.0a. The last version of DirectX supported for Windows 98, ME, 2000 and XP is 9.0c. DirectX 10 shipped with Windows Vista, although it can be upgraded to DirectX 11 via Service Pack 2 and the Platform Update. DirectX 11 shipped with Windows 7, and DirectX 11.1 shipped with Windows 8. Previous versions of Windows are not able to officially run DirectX 10 and DirectX 11-exclusive applications.
Support for Microsoft Internet Explorer also ended with Windows 9x. The last version of Internet Explorer for Windows 95 is Internet Explorer 5.5. Internet Explorer 6 with Service Pack 1 was the last version to which the browser could be upgraded in Windows 98 and ME. Microsoft Internet Explorer 7, the first major update to Internet Explorer 6 in half a decade, was only available for Windows XP SP2 and Windows Vista.
The growing number of important updates caused by the end of service life of these pieces of software have slowly made Windows 9x less and less practical for everyday use. Today, even open source projects such as Mozilla will not run on Windows 9x without rework.







Windows 9x is a series of hybrid 16/32-bit operating systems.
Like most operating systems, Windows 9x consists of kernel space and user space memory.
Although Windows 9x features memory protection, it does not protect the first megabyte of memory from userland applications. This area of memory contains code critical to the functioning of the operating system, and by writing into this area of memory an application can crash or freeze the operating system. This was a source of instability as faulty applications could accidentally write into this region and with that halt the operating system.



The user-mode parts of Windows 9x consist of three subsystems: the Win16 subsystem, the Win32 subsystem and MS-DOS.
Windows 9x/ME set aside two blocks of 64 KB memory regions for GDI and heap resources. By running multiple applications, applications with numerous GDI elements or by running applications over a long span of time, it could exhaust these memory areas. If free system resources dropped below 10%, Windows would become unstable and likely crash.



The kernel mode parts consist of the Virtual Machine Manager (VMM), the Installable File System Manager (IFSHLP), the Configuration Manager, and in Windows 98 and later, the WDM Driver Manager (NTKERN). As a 32-bit operating system, virtual memory space is 4 GiB, divided into a lower 2 GiB for applications and an upper 2 GiB for kernel per process.



Like Windows NT, Windows 9x stores user-specific and configuration-specific settings in a large information database called the Windows registry. Hardware-specific settings are also stored in the registry, and many device drivers use the registry to load configuration data. Previous versions of Windows used files such as AUTOEXEC.BAT, CONFIG.SYS, WIN.INI, SYSTEM.INI and other files with an .INI extension to maintain configuration settings. As Windows became more complex and incorporated more features, .INI files became too unwieldy for the limitations of the then-current FAT filesystem. Backwards-compatibility with .INI files was maintained until Windows XP succeeded the 9x and NT lines.
Although Microsoft discourages using .INI files in favor of Registry entries, a large number of applications (particularly 16-bit Windows-based applications) still use .INI files. Windows 9x supports .INI files solely for compatibility with those applications and related tools (such as setup programs). The AUTOEXEC.BAT and CONFIG.SYS files also still exist for compatibility with real-mode system components and to allow users to change certain default system settings such as the PATH environment variable.
The registry consists of two files: User.dat and System.dat. In Windows ME, Classes.dat was added.



The Virtual Machine Manager (VMM) is the 32-bit protected mode kernel at the core of Windows 9x. Its primary responsibility is to create, run, monitor and terminate virtual machines. The VMM provides services that manage memory, processes, interrupts and protection faults. The VMM works with virtual devices (loadable kernel modules, which consist mostly of 32-bit ring 0 or kernel mode code, but may include other types of code, such as a 16-bit real mode initialisation segment) to allow those virtual devices to intercept interrupts and faults to control the access that an application has to hardware devices and installed software. Both the VMM and virtual device drivers run in a single, 32-bit, flat model address space at privilege level 0 (also called ring 0). The VMM provides multi-threaded, preemptive multitasking. It runs multiple applications simultaneously by sharing CPU (central processing unit) time between the threads in which the applications and virtual machines run.
The VMM is also responsible for creating MS-DOS environments for system processes and Windows applications that still need to run in MS-DOS mode. It is the replacement for Win386 in Windows 3.x, and the file vmm32.vxd is a compressed archive containing most of the core VxD, including VMM.vxd itself and ifsmgr.vxd (which facilitates file system access without the need to call the real mode file system code of the DOS kernel).






Windows 9x does not natively support NTFS or HPFS, but there are third-party solutions which allow Windows 9x to have read-only access to NTFS volumes.
Early versions of Windows 95 did not support FAT32.
Like Windows for Workgroups 3.11, Windows 9x provides support for 32-bit file access based on IFSHLP.SYS, and unlike Windows 3.x, Windows 9x has support for the VFAT file system, allowing file names with a maximum of 255 characters instead of having 8.3 filenames.



Also, there is no support for event logging and tracing or error reporting which the Windows NT family of operating systems has, although software like Norton CrashGuard can be used to achieve similar capabilities on Windows 9x.



Windows 9x is designed as a single-user system. Thus, the security model is much less effective than the one in Windows NT. One reason for this is the FAT file systems (including FAT12/FAT16/FAT32), which are the only ones that Windows 9x supports officially, though Windows NT also supports FAT12 and FAT16 (but not FAT32) and Windows 9x can be extended to read and write NTFS volumes using third-party Installable File System drivers. FAT systems have very limited security; every user that has access to a FAT drive also has access to all files on that drive. The FAT file systems provide no access control lists and file-system level encryption like NTFS.
Some operating systems that were available at the same time as Windows 9x are either multi-user or have multiple user accounts with different access privileges, which allows important system files (such as the kernel image) to be immutable under most user accounts. In contrast, while Windows 95 and later operating systems offer the option of having profiles for multiple users, they have no concept of access privileges, making them roughly equivalent to a single-user, single-account operating system; this means that all processes can modify all files on the system that aren't open, in addition to being able to modify the boot sector and perform other low-level hard drive modifications. This enables viruses and other clandestinely installed software to integrate themselves with the operating system in a way that is difficult for ordinary users to detect or undo. The profile support in the Windows 9x family is meant for convenience only; unless some registry keys are modified, the system can be accessed by pressing "Cancel" at login, even if all profiles have a password. Windows 95's default login dialog box also allows new user profiles to be created without having to log in first.
Users and software can render the operating system unable to function by deleting or overwriting important system files from the hard disk. Users and software are also free to change configuration files in such a way that the operating system is unable to boot or properly function.
Installation software often replaced and deleted system files without properly checking if the file was still in use or of a newer version. This created a phenomenon often referred to as DLL hell.
Windows ME introduced System File Protection and System Restore to handle common problems caused by this issue.



Windows 9x offers share-level access control security for file and printer sharing as well as user-level access control if a Windows NT based operating system is available on the network. In contrast, Windows NT-based operating systems offer only user-level access control but integrated with the operating system's own user account security mechanism.






Device drivers in Windows 9x can be virtual device drivers or (starting with Windows 98) WDM drivers. VxDs usually have the filename extension .vxd or .386, whereas WDM compatible drivers usually use the extension .sys. The 32-bit VxD message server (msgsrv32) is a program that is able to load virtual device drivers (VxDs) at startup and then handle communication with the drivers. Additionally, the message server performs several background functions, including loading the Windows shell (such as Explorer.exe or Progman.exe).
Another type of device drivers are .DRV drivers. These drivers are loaded in user-mode, and are commonly used to control devices such as multimedia devices. To provide access to these devices, a dynamic link library is required (such as MMSYSTEM.DLL).
Drivers written for Windows 9x/Windows ME are loaded into the same address space as the kernel. This means that drivers can by accident or design overwrite critical sections of the operating system. Doing this can lead to system crashes, freezes and disk corruption. Faulty operating system drivers were a source of instability for the operating system. Other monolithic and hybrid kernels, like Linux and Windows NT, are also susceptible to malfunctioning drivers impeding the kernel's operation.
Often the software developers of drivers and applications had insufficient experience with creating programs for the 'new' system, thus causing many errors which have been generally described as "system errors" by users, even if the error is not caused by parts of Windows or DOS. Microsoft has repeatedly redesigned the Windows Driver architecture since the release of Windows 95 as a result.



Windows 9x has no native support for hyper-threading, Data Execution Prevention, symmetric multiprocessing or multi-core processors.
Windows 9x has no native support for SATA host bus adapters (and neither did Windows 2000 nor Windows XP), or USB drives (except Windows ME). There are, however, many SATA-I controllers for which Windows-98/ME drivers exist, and USB mass storage support has been added to Windows 95 OSR2 and Windows 98 through third party drivers. Hardware driver support for Windows 98/ME began to decline in 2005, most notably for motherboard chipsets and video cards.
Early versions of Windows 95 had no support for USB or AGP acceleration.



Windows 95 was able to reduce the role of MS-DOS in Windows much further than had been done in Windows 3.1x and earlier. According to Microsoft developer Raymond Chen, MS-DOS served two purposes in Windows 95: as the boot loader, and as the 16-bit legacy device driver layer.
When Windows 95 started up, MS-DOS loaded, processed CONFIG.SYS, launched COMMAND.COM, ran AUTOEXEC.BAT and finally ran WIN.COM. The WIN.COM program used MS-DOS to load the virtual machine manager, read SYSTEM.INI, load the virtual device drivers, and then turn off any running copies of EMM386 and switch into protected mode. Once in protected mode, the virtual device drivers (VxDs) transferred all state information from MS-DOS to the 32-bit file system manager, and then shut off MS-DOS. These VxDs allow Windows 9x to interact with hardware resources directly, as providing low-level functionalities such as 32-bit disk access and memory management. All future file system operations would get routed to the 32-bit file system manager. In Windows ME, win.com was no longer executed during the startup process; instead it went directly to execute VMM32.VXD from IO.SYS.
The second role of MS-DOS (as the 16-bit legacy device driver layer) was as a backward compatibility tool for running DOS programs in Windows. Many MS-DOS programs and device drivers interacted with DOS in a low-level way; for example, by patching low-level BIOS interrupts such as int 13h, the low-level disk I/O interrupt. When a program issued an int 21h call to access MS-DOS, the call would go first to the 32-bit file system manager, which would attempt to detect this sort of patching. If it detects that the program has tried to hook into DOS, it will jump back into the 16-bit code to let the hook run. A 16-bit driver called IFSMGR.SYS would previously have been loaded by CONFIG.SYS, the job of which was to hook MS-DOS first before the other drivers and programs got a chance, then jump from 16-bit code back into 32-bit code, when the DOS program had finished, to let the 32-bit file system manager continue its work. According to Windows developer Raymond Chen, "MS-DOS was just an extremely elaborate decoy. Any 16-bit drivers and programs would patch or hook what they thought was the real MS-DOS, but which was in reality just a decoy. If the 32-bit file system manager detected that somebody bought the decoy, it told the decoy to quack.



Windows 9x can run MS-DOS applications within itself using a method called "Virtualization", where an application is run on a "Virtual DOS Machine".



Windows 95 and Windows 98 also offer regressive support for DOS applications in the form of being able to boot into a native "DOS Mode" (MS-DOS can be booted without booting Windows, not putting the CPU in protected mode). Through Windows 9x's memory managers and other post-DOS improvements, the overall system performance and functionality is improved. This differs from the emulation used in Windows NT-based operating systems. Some old applications or games may not run properly in a DOS box within Windows and require real DOS Mode.
Having a command line mode outside of the GUI also offers the ability to fix certain system errors without entering the GUI. For example, if a virus is active in GUI mode it can often be safely removed in DOS mode, by deleting its files, which are usually locked while infected in Windows.
Similarly, corrupted registry files, system files or boot files can be restored from the command line. Windows 95 and Windows 98 can be started from DOS Mode by typing 'WIN' <enter> at the command prompt. However, the Recovery Console for Windows 2000, which as a version of Windows NT played a similar role in removing viruses.
Because DOS was not designed for multitasking purposes, Windows versions such as 9x that are DOS-based lack File System security, such as file permissions. Further, if the user uses 16-bit DOS drivers, Windows can become unstable. Hard disk errors often plague the Windows 9x series.




Users can control a Windows 9x-based system through a command-line interface (or CLI), or a graphical user interface (or GUI). For desktop systems, the default mode is usually graphical user interface, where the CLI is available through MS-DOS windows.
The GDI, which is a part of the Win32 and Win16 subsystems, is also a module that is loaded in user mode, unlike Windows NT where the GDI is loaded in kernel mode.
Alpha compositing and therefore transparency effects, such as fade effects in menus, are not supported by the GDI in Windows 9x.
On desktop machines, Windows Explorer is the default user interface, though a variety of additional Windows shell replacement exist.
Other GUIs include LiteStep, bbLean and Program Manager. The GUI provides a means to control the placement and appearance of individual application windows, and interacts with the Window System.



Comparison of operating systems
Architecture of Windows 9x






 Learning materials related to Windows 9x at Wikiversity
 Basic Computing Using Windows/Appendices/Dual Booting at Wikibooks
 Operating System Design/Case Studies/Windows 9x at Wikibooks