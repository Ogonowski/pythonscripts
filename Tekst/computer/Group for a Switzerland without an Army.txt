The Group for a Switzerland without an Army, (GSwA; German: Gruppe f r eine Schweiz ohne Armee, GSoA; French: Groupe pour une Suisse sans arm e, GSsA; Italian: Gruppo per una Svizzera senza Esercito, GSsE) is a group working to reduce the military activities of Switzerland. The Group was created in Solothurn on 12 September 1982 by 120 people. Its roster has varied considerably; as of 2009 its website states that it consists of about 20,000 members or supporters, consisting largely of pacifists and anti-militarists.



The group is politically active and spreads awareness of its cause in several ways:
using the direct democratic tools in Switzerland to propose constitutional amendments and stimulate the public debate;
practical and legal information, such as alternatives to Switzerland's mandatory military service, e.g. Swiss Civilian Service, often opted for by conscientious objectors;
media work, covering subjects such as arms equipment sales.
Currently, the group focuses on the following topics:
opposition to arms trade: In 2007, the group filed enough signatures for a national initiative against arms trade. The Swiss will vote in a referendum on an amendment to the constitution that would ban the export of any weapon from Switzerland. This national referendum was held on November 29, 2009.
opposition to arms procurements, for example against the fighter jets which the Swiss army is about to purchase;
constant criticism of Switzerland's militaristic traditions.



In early 1986 the group launched its first initiative, named "F r eine Schweiz ohne Armee und f r eine umfassende Friedenspolitik" (For a Switzerland without an army and an overall peaceful political stance), which gathered 111,300 signatures (100,000 were required to initiate a vote). On 26 November 1989, 68.6% of eligible voters cast their ballot, resulting in 1,052,218 (35.6%) votes for the initiative. The high percentage of yes voters stunned everybody, and was interpreted as a manifestation of a remarkable loss of significance of the military in Swiss society. Although the initiative was rejected, the surprising result of the referendum influenced later reforms of Switzerland's armed forces.
In Spring 1992, both chambers of the Swiss parliament decided to procure 34 F/A-18 Hornet fighter jets. Within 32 days, the GSoA gathered 503,719 signatures. Never before and never since has an initiative been submitted within such a short time or with a comparable number of signatures. Nevertheless, the Swiss people rejected the initiative on the ballot by 57% to 43%.
In 1999, the GSoA assisted in collecting the 100,000 valid signatures necessary for a referendum on whether Switzerland would join the United Nations. The Swiss electorate approved this proposal in 2002. In 2001, the GSoA gathered more than 60,000 signatures for a referendum against the revised legislation regulating the structure and deployment of the armed forces. The GSoA especially opposed the option to send armed troops abroad   a concern that was shared by some right-wing traditionalists. On 10 June 2001, the revised legislation was approved by 51% of the voters.
On 2 December 2001, Swiss voters considered two additional GSoA initiatives, called "In favor of a credible security policy and a Switzerland without an army" and "Solidarity instead of soldiers: In favor of a voluntary civilian peace service". In contrast to the first initiative to abolish the Swiss army, there was only sparse public debate on the issue - another topic dominated the media and the minds of the people: The September 11 attacks which occurred a few weeks before the vote took place. Both initiatives were rejected by 78% and 77% of the voters.
On 21 September 2007, the GSoA submitted an initiative that required a ban on all arms exports from Switzerland. This initiative was rejected on November 29, 2009 with 68.2% of the voters casting a no.
Another initiative required stricter legislation on private possession of firearms and a stop to the Swiss tradition to store the assault rifles of each soldier at home. The initiative was supported by some 40 organisations, of which the Social Democratic Party of Switzerland, the Green Party of Switzerland and the GSoA made the largest contributions. The initiative was rejected on February 13, 2011 with 56.3% of the voters saying no.
GSoA's initiative to abolish conscription was rejected by 73.2% of the Swiss electorate on September 22, 2013.
In 2008 the Swiss Federal Council announced its plans to purchase a new generation of fighter aircraft. Immediately after this, the GSoA started an initiative that proposes a 10-year-moratorium for aircraft procurements. The collection of signatures was completed in May 2009. After several turns of the deal, 53% of Swiss voters decided not to buy new Gripen fighter jets on May 18, 2014. This was the first victory on the ballot for GSoA in a nationwide referendum.






(English) Official website