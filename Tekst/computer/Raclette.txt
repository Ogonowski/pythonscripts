Raclette /r kl t/ is a semi-firm cow's milk cheese that is usually fashioned into a wheel of about 6 kg (13 lb). It is most commonly used for melting. It is also a Swiss dish based on heating the cheese and scraping off (racler) the melted part.



Raclette was mentioned in medieval writings, in texts from Swiss-German convents dating from as far as 1291, as a particularly nutritious meal consumed by peasants in mountainous Switzerland and France (Savoy region). It was then known in the German-speaking part of Switzerland as Bratch s, or "roasted cheese." Traditionally, the Swiss cow herders used to take the cheese with them when they were moving cows to or from the pastures up in the mountains. In the evenings around the campfire, they would place the cheese next to the fire and, when it had reached the perfect softness, scrape it on top of bread.
In the Swiss canton of Valais, raclette is typically served with tea or other warm beverages. Another popular option is to serve raclette with white wine, such as the traditional Savoy wine or Fendant, but Riesling and Pinot gris are also common. Local tradition cautions that other drinks   water for example   will cause the cheese to harden in the stomach, leading to indigestion.




Raclette is also a dish indigenous to parts of Switzerland. The Raclette cheese round is heated, either in front of a fire or by a special machine, then scraped onto diners' plates; the term raclette derives from the French word racler, meaning "to scrape," a reference to the fact that the melted cheese must be scraped from the unmelted part of the cheese onto the plate.
Traditionally the melting happens in front of an open fire with the big piece of cheese facing the heat. One then regularly scrapes off the melting side. It is accompanied by small firm potatoes (Bintje, Charlotte or Raclette varieties), gherkins, pickled onions, and dried meat, such as jambon cru/cuit and viande des Grisons.
A modern way of serving raclette involves an electric table-top grill with small pans, known as coupelles, in which to melt slices of raclette cheese. Generally the grill is surmounted by a hot plate or griddle. The cheese is brought to the table sliced, accompanied by platters of boiled or steamed potatoes, other vegetables and charcuterie. These are then mixed with potatoes and topped with cheese in the small, wedge-shaped coupelles that are placed under the grill to melt and brown the cheese. Alternatively, slices of cheese may be melted and simply poured over food on the plate. The emphasis in raclette dining is on relaxed and sociable eating and drinking, the meal often running to several hours. French and other European supermarkets generally stock both the grill apparatus and ready-sliced cheese and charcuterie selections, especially around Christmas. Restaurants also provide raclette evenings for parties of diners.



Fondue, a different Swiss dish based on cheese melted in a pot
Culinary Heritage of Switzerland
Appellation d'origine prot g e






 Media related to Raclette at Wikimedia Commons