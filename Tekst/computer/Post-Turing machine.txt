The article Turing machine gives a general introduction to Turing machines, while this article covers a specific class of Turing machines.
A Post Turing machine is a "program formulation" of an especially simple type of Turing machine, comprising a variant of Emil Post's Turing-equivalent model of computation described below. (Post's model and Turing's model, though very similar to one another, were developed independently. Turing's paper was received for publication in May 1936, followed by Post's in October.) A Post Turing machine uses a binary alphabet, an infinite sequence of binary storage locations, and a primitive programming language with instructions for bi-directional movement among the storage locations and alteration of their contents one at a time. The names "Post Turing program" and "Post Turing machine" were used by Martin Davis in 1973 1974 (Davis 1973, p. 69ff). Later in 1980, Davis used the name "Turing Post program" (Davis, in Steen p. 241).



In his 1936 paper "Finite combinatory processes formulation 1" (which can be found on page 289 of The Undecidable), Emil Post described a model of extreme simplicity which he conjectured is "logically equivalent to recursiveness", and which was later proved to be so. The quotes in the following are from this paper.
Post's model of a computation differs from the Turing-machine model in a further "atomization" of the acts a human "computer" would perform during a computation.
Post's model employs a "symbol space" consisting of a "two-way infinite sequence of spaces or boxes", each box capable of being in either of two possible conditions, namely "marked" (as by a single vertical stroke) and "unmarked" (empty). Initially, finitely-many of the boxes are marked, the rest being unmarked. A "worker" is then to move among the boxes, being in and operating in only one box at a time, according to a fixed finite "set of directions" (instructions), which are numbered in order (1,2,3,...,n). Beginning at a box "singled out as the starting point", the worker is to follow the set of instructions one at a time, beginning with instruction 1.
The instructions may require the worker to perform the following "basic acts" or "operations":
(a) Marking the box he is in (assumed empty),
(b) Erasing the mark in the box he is in (assumed marked),
(c) Moving to the box on his right,
(d) Moving to the box on his left,
(e) Determining whether the box he is in, is or is not marked.
Specifically, the i th "direction" (instruction) given to the worker is to be one of the following forms:
(A) Perform operation Oi [Oi = (a), (b), (c) or (d)] and then follow direction ji,
(B) Perform operation (e) and according as the answer is yes or no correspondingly follow direction ji' or ji' ' ,
(C) Stop.
(The above indented text and italics are as in the original.) Post remarks that this formulation is "in its initial stages" of development, and mentions several possibilities for "greater flexibility" in its final "definitive form", including
(1) replacing the infinity of boxes by a finite extensible symbol space, "extending the primitive operations to allow for the necessary extension of the given finite symbol space as the process proceeds",
(2) using an alphabet of more than two symbols, "having more than one way to mark a box",
(3) introducing finitely-many "physical objects to serve as pointers, which the worker can identify and move from box to box".



As briefly mentioned in the article Turing machine, Post, in his paper of 1947 (Recursive Unsolvability of a Problem of Thue) atomized the Turing 5-tuples to 4-tuples:
"Our quadruplets are quintuplets in the Turing development. That is, where our standard instruction orders either a printing (overprinting) or motion, left or right, Turing's standard instruction always order a printing and a motion, right, left, or none"(footnote 12, Undecidable p. 300)
Like Turing he defined erasure as printing a symbol "S0". And so his model admitted quadruplets of only three types (cf p. 294 Undecidable):
qi Sj L ql,
qi Sj R ql,
qi Sj Sk ql
At this time he was still retaining the Turing state-machine convention   he had not formalized the notion of an assumed sequential execution of steps until a specific test of a symbol "branched" the execution elsewhere.



For an even further reduction   to only four instructions   of the Wang model presented here see Wang B-machine.
Wang (1957, but presented to the ACM in 1954) is often cited (cf Minsky (1967) p. 200) as the source of the "program formulation" of binary-tape Turing machines using numbered instructions from the set
write 0
write 1
move left
move right
if scanning 0 then goto instruction i
if scanning 1 then goto instruction j
where sequential execution is assumed, and Post's single "if ... then ... else" has been "atomised" into two "if ... then ..." statements. (Here '1' and '0' are used where Wang used "marked" and "unmarked", respectively, and the initial tape is assumed to contain only '0's except for finitely-many '1's.)
Wang noted the following:
"Since there is no separate instruction for halt (stop), it is understood that the machine will stop when it has arrived at a stage that the program contains no instruction telling the machine what to do next." (p. 65)
"In contrast with Turing who uses a one-way infinite tape that has a beginning, we are following Post in the use of a 2-way infinite tape." (p. 65)
Unconditional gotos are easily derived from the above instructions, so "we can freely use them too". (p. 84)
Any binary-tape Turing machine is readily converted to an equivalent "Wang program" using the above instructions.



Martin Davis was an undergraduate student of Emil Post's. Along with Stephen Kleene he completed his PhD under Alonzo Church (Davis (2000) 1st and 2nd footnotes p. 188).
The following model he presented in a series of lectures to the Courant Institute at NYU in 1973 1974. This is the model to which Davis formally applied the name "Post Turing machine" with its "Post Turing language". The instructions are assumed to be executed sequentially (Davis 1974, p. 71):
"Write 1
"Write B
"To A if read 1
"To A if read B
"RIGHT
"LEFT
Note that there is no "halt" or "stop".



The following model appears as an essay What is a computation? in Steen pages 241 267. For some reason Davis has renamed his model a "Turing Post machine" (with one back-sliding on page 256.)
In the following model Davis assigns the numbers "1" to Post's "mark/slash" and "0" to the blank square. To quote Davis: "We are now ready to introduce the Turing Post Programming Language. In this language there are seven kinds of instructions:

"PRINT 1
"PRINT 0
"GO RIGHT
"GO LEFT
"GO TO STEP i IF 1 IS SCANNED
"GO TO STEP i IF 0 IS SCANNED
"STOP

"A Turing Post program is then a list of instructions, each of which is of one of these seven kinds. Of course in an actual program the letter i in a step of either the fifth or sixth kind must replaced with a definite (positive whole) number." (Davis in Steen, p. 247).
Confusion arises if one does not realize that a "blank" tape is actually printed with all zeroes   there is no "blank".
Splits Post's "GO TO" ("branch" or "jump") instruction into two, thus creating a larger (but easier-to-use) instruction set of seven rather than Post's six instructions.
Does not mention that instructions PRINT 1, PRINT 0, GO RIGHT and GO LEFT imply that, after execution, the "computer" must go to the next step in numerical sequence.



"Although the formulation of Turing we have presented is closer in spirit to that originally given by Emil Post, it was Turing's analysis of the computation that has made this formulation seem so appropriate. This language has played a fundamental role in theoretical computer science." (Davis et al. (1994) p. 129)
This model allows for the printing of multiple symbols. The model allows for B (blank) instead of S0. The tape is infinite in both directions. Either the head or the tape moves, but their definitions of RIGHT and LEFT always specify the same outcome in either case (Turing used the same convention).

PRINT    ;Replace scanned symbol with  
IF   GOTO L  ;IF scanned symbol is   THEN goto "the first" instruction labelled L
RIGHT  ;Scan square immediately right of the square currently scanned
LEFT  ;Scan square immediately left of the square currently scanned

Note that only one type of "jump"   a conditional GOTO   is specified; for an unconditional jump a string of GOTO's must test each symbol.
This model reduces to the binary { 0, 1 } versions presented above, as shown here:

PRINT 0 = ERASE  ;Replace scanned symbol with 0 = B = BLANK
PRINT 1  ;Replace scanned symbol with 1
IF 0 GOTO L  ;IF scanned symbol is 0 THEN goto "the first" instruction labelled L
IF 1 GOTO L  ;IF scanned symbol is 1 THEN goto "the first" instruction labelled L
RIGHT  ;Scan square immediately right of the square currently scanned
LEFT  ;Scan square immediately left of the square currently scanned






The following "reduction" (decomposition, atomizing) method   from 2-symbol Turing 5-tuples to a sequence of 2-symbol Post Turing instructions   can be found in Minsky (1961). He states that this reduction to "a program ... a sequence of Instructions" is in the spirit of Hao Wang's B-machine (italics in original, cf Minsky (1961) p. 439).
(Minsky's reduction to what he calls "a sub-routine" results in 5 rather than 7 Post Turing instructions. He did not atomize Wi0: "Write symbol Si0; go to new state Mi0", and Wi1: "Write symbol Si1; go to new state Mi1". The following method further atomizes Wi0 and Wi1; in all other respects the methods are identical.)
This reduction of Turing 5-tuples to Post Turing instructions may not result in an "efficient" Post Turing program, but it will be faithful to the original Turing-program.
In the following example, each Turing 5-tuple of the 2-state busy beaver converts into
(i) an initial conditional "jump" (goto, branch), followed by
(ii) 2 tape-action instructions for the "0" case   Print or Erase or None, followed by Left or Right or None, followed by
(iii) an unconditional "jump" for the "0" case to its next instruction
(iv) 2 tape-action instructions for the "1" case   Print or Erase or None, followed by Left or Right or None, followed by
(v) an unconditional "jump" for the "1" case to its next instruction
for a total of 1 + 2 + 1 + 2 + 1 = 7 instructions per Turing-state.
For example, the 2-state busy beaver's "A" Turing-state, written as two lines of 5-tuples, is:
The table represents just a single Turing "instruction", but we see that it consists of two lines of 5-tuples, one for the case "tape symbol under head = 1", the other for the case "tape symbol under head = 0". Turing observed (Undecidable, p. 119) that the left-two columns   "m-configuration" and "symbol"   represent the machine's current "configuration"   its state including both Tape and Table at that instant   and the last three columns are its subsequent "behavior". As the machine cannot be in two "states" at once, the machine must "branch" to either one configuration or the other:
After the "configuration branch" (J1 xxx) or (J0 xxx) the machine follows one of the two subsequent "behaviors". We list these two behaviors on one line, and number (or label) them sequentially (uniquely). Beneath each jump (branch, go to) we place its jump-to "number" (address, location):
Per the Post Turing machine conventions each of the Print, Erase, Left, and Right instructions consist of two actions:
(i) Tape action: { P, E, L, R}, then
(ii) Table action: go to next instruction in sequence
And per the Post Turing machine conventions the conditional "jumps" J0xxx, J1xxx consist of two actions:
(i) Tape action: look at symbol on tape under the head
(ii) Table action: If symbol is 0 (1) and J0 (J1) then go to xxx else go to next instruction in sequence
And per the Post Turing machine conventions the unconditional "jump" Jxxx consists of a single action, or if we want to regularize the 2-action sequence:
(i) Tape action: look at symbol on tape under the head
(ii) Table action: If symbol is 0 then go to xxx else if symbol is 1 then go to xxx.
Which, and how many, jumps are necessary? The unconditional jump Jxxx is simply J0 followed immediately by J1 (or vice versa). Wang (1957) also demonstrates that only one conditional jump is required, i.e. either J0xxx or J1xxx. However, with this restriction the machine becomes difficult to write instructions for. Often only two are used, i.e.
(i) { J0xxx, J1xxx }
(ii) { J1xxx, Jxxx }
(iii) { J0xxx, Jxxx },
but the use of all three { J0xxx, J1xxx, Jxxx } does eliminate extra instructions. In the 2-state Busy Beaver example that we use only { J1xxx, Jxxx }.



The mission of the busy beaver is to print as many ones as possible before halting. The "Print" instruction writes a 1, the "Erase" instruction (not used in this example) writes a 0 (i.e. it is the same as P0). The tape moves "Left" or "Right" (i.e. the "head" is stationary).
State table for a 2-state Turing-machine busy beaver:
Instructions for the Post Turing version of a 2-state busy beaver: observe that all the instructions are on the same line and in sequence. This is a significant departure from the "Turing" version and is in the same format as what is called a "computer program":
Alternately, we might write the table as a string. The use of "parameter separators" ":" and instruction-separators "," are entirely our choice and do not appear in the model. There are no conventions (but see Booth (1967) p. 374, and Boolos and Jeffrey (1974, 1999) p. 23), for some useful ideas of how to combine state diagram conventions with the instructions   i.e. to use arrows to indicate the destination of the jumps). In the example immediately below, the instructions are sequential starting from "1", and the parameters/"operands" are considered part of their instructions/"opcodes":
J1:5, P, R, J:8, P, L, J:8, J1:12, P, L, J1:1, P, N, J:15, H
The state diagram of a two-state busy beaver (little drawing, right-hand corner) converts to the equivalent Post Turing machine with the substitution of 7 Post Turing instructions per "Turing" state. The HALT instruction adds the 15th state: 
A "run" of the 2-state busy beaver with all the intermediate steps of the Post Turing machine shown:



The following is a two-state Turing busy beaver with additional instructions 15 20 to demonstrate the use of "Erase", J0, etc. These will erase the 1's written by the busy beaver:
Additional Post Turing instructions 15 through 20 erase the symbols created by the busy beaver. These "atomic" instructions are more "efficient" than their Turing-state equivalents (of 7 Post Turing instructions). To accomplish the same task a Post Turing machine will (usually) require fewer Post Turing states than a Turing-machine, because (i) a jump (go-to) can occur to any Post Turing instruction (e.g. P, E, L, R) within the Turing-state, (ii) a grouping of move-instructions such as L, L, L, P are possible, etc.:




This example is a reference to show how a "multiply" computation would proceed on a single-tape, 2-symbol { blank, 1 } Post Turing machine model.
This particular "multiply" algorithm is recursive through two loops. The head moves. It starts to the far left (the top) of the string of unary marks representing a' :

Move head far right. Establish (i.e. "clear") register c by placing a single blank and then a mark to the right of b
a_loop: Move head right once, test for the bottom of a' (a blank). If blank then done else erase mark;
Move head right to b' . Move head right once past the top mark of b' ;
b_loop: If head is at the bottom of b' (a blank) then move head to far left of a' , else:

Erase a mark to locate counter (a blank) in b' .
Increment c' : Move head right to top of c' and increment c' .
Move head left to the counter inside b' ,
Repair counter: print a mark in the blank counter.
Decrement b'  count: Move head right once.
Return to b_loop.

Multiply a   b = c, for example: 3   4 = 12. The scanned square is indicated by brackets around the mark i.e. [ | ]. An extra mark serves to indicate the symbol "0":
At the start of the computation a' is 4 unary marks, then a separator blank, b' is 5 unary marks, then a separator mark. An unbounded number of empty spaces must be available for c to the right:
....a'.b'.... = : ....[ | ] | | | . | | | | | ....

During the computation the head shuttles back and forth from a' to b' to c' back to b' then to c' , then back to b' , then to c' ad nauseam while the machine counts through b' and increments c' . Multiplicand a' is slowly counted down (its marks erased   shown for reference with x's below). A "counter" inside b' moves to the right through b (an erased mark shown being read by the head as [ . ] ) but is reconstructed after each pass when the head returns from incrementing c' :
....a.b.... = : ....xxx | . | | [ . ] | | . | | | | | | | ...

At end of computation: c' is 13 marks = "successor of 12" appearing to the right of b' . a' has vanished in process of the computation
....b.c = ......... | | | | | . | | | | | | | | | | | | | ...



^ a: Difference between Turing- and Post Turing machine models
In his chapter XIII Computable Functions, Kleene adopts the Post model; Kleene's model uses a blank and one symbol "tally mark  " (Kleene p. 358), a "treatment closer in some respects to Post 1936. Post 1936 considered computation with a 2-way infinite tape and only 1 symbol" (Kleene p. 361). Kleene observes that Post's treatment provided a further reduction to "atomic acts" (Kleene p. 357) of "the Turing act" (Kleene p. 379). As described by Kleene "The Turing act" is the combined 3 (time-sequential) actions specified on a line in a Turing table: (i) print-symbol/erase/do-nothing followed by (ii) move-tape-left/move-tape-right/do-nothing followed by (iii) test-tape-go-to-next-instruction: e.g. "s1Rq1" means "Print symbol " ", then move tape right, then if tape symbol is " " then go to state q1". (See Kleene's example P. 358).
Kleene observes that Post atomized these 3-actions further into two types of 2-actions. The first type is a "print/erase" action, the second is a "move tape left/right action": (1.i) print-symbol/erase/do-nothing followed by (1.ii) test-tape-go-to-next-instruction, OR (2.ii) move-tape-left/move-tape-right/do-nothing followed by (2.ii) test-tape-go-to-next-instruction.
But Kleene observes that while
"Indeed it could be argued that the Turing machine act is already compound, and consists psychologically in a printing and change in state of mind, followed by a motion and another state of mind [, and] Post 1947 does thus separate the Turing act into two; we have not here, primarily because it saves space in the machine tables not to do so."(Kleene p. 379)
In fact Post's treatment (1936) is ambiguous; both (1.1) and (2.1) could be followed by "(.ii) go to next instruction in numerical sequence". This represents a further atomization into three types of instructions: (1) print-symbol/erase/do-nothing then go-to-next-instruction-in-numerical-sequence, (2) move-tape-left/move-tape-right/do-nothing then go-to-next-instruction-in-numerical-sequence (3)test-tape then go-to-instruction-xxx-else-go-to-next-instruction-in-numerical-sequence.



Stephen C. Kleene, Introduction to Meta-Mathematics, North-Holland Publishing Company, New York, 10th edition 1991, first published 1952. Chapter XIII is an excellent description of Turing machines; Kleene uses a Post-like model in his description and admits the Turing model could be further atomized, see Footnote 1.
Martin Davis, editor: The Undecidable, Basic Papers on Undecidable Propositions, Unsolvable Problems And Computable Functions, Raven Press, New York, 1965. Papers include those by G del, Church, Rosser, Kleene, and Post.
Martin Davis, "What is a computation", in Mathematics Today, Lynn Arthur Steen, Vintage Books (Random House), 1980. A wonderful little paper, perhaps the best ever written about Turing Machines. Davis reduces the Turing Machine to a far-simpler model based on Post's model of a computation. Includes a little biography of Emil Post.
Martin Davis, Computability: with Notes by Barry Jacobs, Courant Institute of Mathematical Sciences, New York University, 1974.
Martin Davis, Ron Sigal, Elaine J. Weyuker, (1994) Computability, Complexity, and Languages: Fundamentals of Theoretical Computer Science   2nd edition, Academic Press: Harcourt, Brace & Company, San Diego, 1994 ISBN 0-12-206382-1 (First edition, 1983).
Fred Hennie, Introduction to Computability, Addison Wesley, 1977.
Marvin Minsky, (1961), Recursive Unsolvability of Post's problem of 'Tag' and other Topics in Theory of Turing Machines, Annals of Mathematics, Vol. 74, No. 3, November, 1961.
Roger Penrose, The Emperor's New Mind: Concerning computers, Minds and the Laws of Physics, Oxford University Press, Oxford England, 1990 (with corrections). Cf: Chapter 2, "Algorithms and Turing Machines". An overly-complicated presentation (see Davis's paper for a better model), but a thorough presentation of Turing machines and the halting problem, and Church's lambda calculus.
Hao Wang (1957): "A variant to Turing's theory of computing machines", Journal of the Association for Computing Machinery (JACM) 4, 63 92.