The Canton of Thurgau (German:  Thurgau , anglicized as Thurgovia) is a northeast canton of Switzerland.
It is named for the Thur River, and the name Thurgovia was historically used for a larger area, including part of this river's basin upstream of the modern canton. The area of what is now Thurgau was acquired as subject territories by the cantons of the Old Swiss Confederacy from the mid 15th century. Thurgau was first declared a canton in its own right at the formation of the Helvetic Republic in 1798.
The population, as of December 2014, is 263,733. In 2007, there were a total of 47,390 (or 19.9% of the population) who were resident foreigners. The capital is Frauenfeld.



In prehistoric times the lands of the canton were inhabited by people of the Pfyn culture along Lake Constance. During Roman times the canton was part of the province Raetia until in 450 the lands were settled by the Alamanni.
In the 6th century Thurgovia became a Gau of the Frankish Empire as part of Alemannia, passing to the Duchy of Swabia in the early 10th century. At this time, Thurgovia included not just what is now the canton of Thurgau, but also much of the territory of the modern canton of St. Gallen, the Appenzell and the eastern parts of the canton of Zurich. The most important cities of Thurgovia in the early medieval period were Constance as the seat of the bishop, and St. Gallen for its abbey.
The dukes of Z hringen and the counts of Kyburg took over much of the land in the High Middle Ages. The town of Z rich was part of the Thurgau until it became reichsunmittelbar in 1218. When the Kyburg dynasty became extinct in 1264 the Habsburgs took over that land.
The Old Swiss Confederacy allied with ten freed bailiwicks of the former Toggenburg seized the lands of the Thurgau from the Habsburgs in 1460, and it became a subject territory of seven Swiss cantons (Zurich, Lucerne, Uri, Schwyz, Unterwalden, Zug and Glarus).
During the Protestant Reformation in Switzerland, both the Catholic and emerging Reformed parties sought to swing the subject territories, such as the Thurgau, to their side. In 1524, in an incident that resonated across Switzerland, local peasants occupied the cloister of Ittingen in the Thurgau, driving out the monks, destroying documents, and devastating the wine-cellar. Between 1526 and 1531, most of the Thurgau's population adopted the new Reformed faith spreading from Zurich; Zurich's defeat in the War of Kappel (1531) ended Reformed predominance. Instead, the First Peace of Kappel protected both Catholic and Reformed worship, though the provisions of the treaty generally favored the Catholics, who also made up a majority among the seven ruling cantons. Religious tensions over the Thurgau were an important background to the First War of Villmergen (1656), during which Zurich briefly occupied the Thurgau.
In 1798 the land became a canton for the first time as part of the Helvetic Republic. In 1803, as part of the Act of Mediation, the canton of Thurgau became a member of the Swiss confederation. The cantonal coat of arms was designed in 1803, based on the coat of arms of the House of Kyburg which ruled the Thurgau in the 13th century, changing the background to green-and-white, at the time considered "revolutionary" colours (c.f. tricolour); as the placement of a yellow (or) charge on white (argent) is a violation of heraldic principles, there have been suggestions to modify the design, including a 1938 suggestion to use a solid green field divided by a diagonal white line, but they were not successful.
The current cantonal constitution of Thurgau dates from 1987.




To the north the canton is bound by the Lake Constance across which lies Germany and Austria. The river Rhine creates the border in the northwest. To the south lies the canton of St. Gallen; to the west lie the cantons of Z rich and Schaffhausen.
The area of the canton is 991 km2 (383 sq mi) and commonly divided into three hill masses. One of these stretches along Lake Constance in the north. Another is further inland between the river Thur and the river Murg. The third one forms the southern border of the canton and merges with the H rnli mountain in the pre-Alps.



The population of the canton (as of 31 December 2014) is 263,733. The canton is mostly German speaking. The population (as of 2000) is split between Protestants (45%) and Roman Catholics (36%).







Since January 2011, Thurgau is divided into five districts which are named after their capitals. Before this date, there were eight districts (Steckborn District, Bischofszell District and Diessenhofen District formed own districts with their surrounding municipalities).
Frauenfeld District with capital Frauenfeld
Kreuzlingen District with capital Kreuzlingen
Weinfelden District with capital Weinfelden
M nchwilen District with capital M nchwilen
Arbon District with capital Arbon




As of 2009, there are 80 municipalities in the canton.



The canton of Thurgau is known for its fine agricultural produce. Particularly, apples, pears, fruits and vegetables are well-known. The many orchards in the canton are mainly used for the production of cider. Wine is produced in the Thur valley.
There is also industry in the canton of Thurgau. The main industries are printing, textiles and handicrafts. Small and middle-sized businesses are important for the cantonal economy. Many of these are concentrated around the capital.






Official website (German)
Official statistics