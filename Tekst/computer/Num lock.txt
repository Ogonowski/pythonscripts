Num Lock or Number Lock ( ) is a key on the numeric keypad of most computer keyboards. It is a lock key, like Caps Lock and Scroll Lock. Its state (on or off) affects the function of some of the keys, and is commonly displayed by an LED built into the keyboard.

The Num Lock key exists because earlier 84-key IBM PC keyboards did not have arrows separate from the numeric keypad. Num Lock would be used to choose between the two functions. On some laptop computers, the Num Lock key is used to convert part of the main keyboard to act as a (slightly skewed) numeric keypad rather than letters. On some laptop computers, the Num Lock key is absent and replaced by the use of a key combination.
Since most modern desktop computers have a full-size keyboard with both a numeric pad and separate arrow keys, Num Lock is rarely used for its original purpose, and ends up confusing the user if it has for some reason been activated without the user being aware of this. This can be more of an issue on most laptop computers, since activating the Num Lock function typically requires use of the Fn key and if a user accidentally switches it on they may have no idea how to switch it off. If a full-size keyboard is plugged into a laptop, then the Num Lock function is normally off, and the user would have to activate the Num Lock function to use the numeric keypad for numeric entry. Since Apple keyboards never had a combination of arrow keys and numeric keypad (but some lacked arrow keys, function keys and a numeric keypad altogether), Apple has keyboards with a separate numeric keypad but no functional Num Lock key. Instead, these keyboards include a Clear key.






NUMLOCK (CONFIG.SYS directive)