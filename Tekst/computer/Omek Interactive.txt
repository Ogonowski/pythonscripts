Omek Interactive was a venture-backed technology company developing advanced motion sensing software for human-computer interaction. Omek was co-founded in 2007 by Janine Kutliroff and Gershom Kutliroff.



Omek Interactive is an Israeli company that develops gesture recognition and motion tracking software for use in combination with 3D depth sensor cameras. Omek s middleware is sensor-independent, supporting multiple cameras including those based on a Structured light and Time-of-flight camera technology. Omek's software works with the following cameras: PrimeSense-based Microsoft Kinect, Pmdtechnologies CamCube, SoftKinetic DepthSense, and Panasonic D-Imager.
In July 2011 Intel Capital led their Round C financing with $7 million.
Intel confirmed, that it acquired Omek July 16, 2013.



Omek s flagship product is the Beckon Development Suite, which converts raw depth data from 3D cameras and turns it into intelligence about humans in the scene, through background subtraction, joints tracking, skeleton identification, and gesture recognition. The Beckon software solution includes the Gesture Authoring Tool, a machine learning tool that enables developers to create gestures without writing any code. Beckon is no longer available as a free, non-commercial download from the Omek website.
In March 2012, at the Embedded Vision Alliance Summit, Omek announced the upcoming availability of their Grasp Development Suite. Grasp focuses on close-range, hand and finger tracking and gesture recognition at distances of 1 meter and less. At the same event Omek also announced support for Texas Instruments  BeagleBoard-xM evaluation board, a low-cost, low-power, embedded computing platform.



Natural user interface
Human-computer interaction
Gesture recognition
Computer vision






Official Website
See Chapter 7: Application Development with the [Omek] Beckon Framework "Meet the Kinect: An Introduction to Programming Natural User Interfaces", by Sean Kean, Jonathan Hall, Phoenix Perry