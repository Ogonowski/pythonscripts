Asset management, broadly defined, refers to any system that monitors and maintains things of value to an entity or group. It may apply to both tangible assets such as buildings and to intangible assets such as human capital, intellectual property, and goodwill and financial assets. Asset management is a systematic process of deploying, operating, maintaining, upgrading, and disposing of assets cost-effectively.
The term is most commonly used in the financial world to describe people and companies that manage investments on behalf of others. These include, for example, investment managers that manage the assets of a pension fund.
Alternative views of asset management in the engineering environment are: the practice of managing assets to achieve the greatest return (particularly useful for productive assets such as plant and equipment), and the process of monitoring and maintaining facilities systems, with the objective of providing the best possible service to users (appropriate for public infrastructure assets).



Civilization has always relied on its technological assets to support key functions like transport, public health, business, and commerce. There is a clear link between the provision and sophistication of technological assets and our modern lifestyle. Romans built a strong empire through their construction of roads, aqueducts and other assets. Similar stories are found when examining Asia and Africa.






The most common usage of the term "asset manager" refers to investment management, the sector of the financial services industry that manages investment funds and segregated client accounts. An asset management is a part of a financial company which comprises experts who manage money and handle the investments of clients. From studying the client's assets to planning and looking after the investments, all things are looked after by the asset managers.



Infrastructure asset management is the combination of management, financial, economic, engineering, and other practices applied to physical assets with the objective of providing the required level of service in the most cost-effective manner. It includes the management of the entire lifecycle including design, construction, commissioning, operating, maintaining, repairing, modifying, replacing and decommissioning/disposal of physical and infrastructure assets. Operating and sustainment of assets in a constrained budget environment require a prioritization scheme. As a way of illustration, the recent development of renewable energy has seen the rise of effective asset managers involved in the management of solar systems (solar park, rooftops and windmills. These teams are actually more and more teaming-up with financial asset managers in order to offer turn key solutions to investors. Infrastructure asset management became very important in most of the developed countries in the 21st century, since their infrastructure network became almost complete in the 20th century and they have to manage to operate and maintain them cost effectively.



Enterprise asset management is the business processes and enabling information systems that support management of an organization's assets, both physical assets, called "tangible", and non-physical, "intangible" assets.
Physical asset management: the practice of managing the entire lifecycle (design, construction, commissioning, operating, maintaining, repairing, modifying, replacing and decommissioning/disposal) of physical and infrastructure assets such as structures, production and service plant, power, water and waste treatment facilities, distribution networks, transport systems, buildings and other physical assets. It is related to asset health management.
Infrastructure asset management expands on this theme in relation primarily to public sector, utilities, property and transport systems. Additionally, Asset Management can refer to shaping the future interfaces amongst the human, built, and natural environments through collaborative and evidence-based decision processes
Fixed assets management: an accounting process that seeks to track fixed assets for the purposes of financial accounting
IT asset management: the set of business practices that join financial, contractual and inventory functions to support life cycle management and strategic decision making for the IT environment. This is also one of the processes defined within IT service management
Digital asset management: a form of electronic media content management that includes digital assets



Public asset management, also called corporate asset management, expands the definition of enterprise asset management (EAM) by incorporating the management of all things of value to a municipal jurisdiction and its citizens' expectations.
An EAM requires an asset registry (inventory of assets and their attributes) combined with a computerized maintenance management system (CMMS). All public assets are interconnected and share proximity, and this connectivity is possible through the use of geographic information system (GIS).
GIS-centric public asset management standardizes data and allows interoperability, providing users the capability to reuse, coordinate, and share information in an efficient and effective manner by making the GIS geo-database the asset registry. A GIS-centric public asset management that standardizes data and allows interoperability, providing users the capability to reuse, coordinate, and share information in an efficient and effective manner.
In the United States, the de facto GIS standard is the Esri GIS for utilities and municipalities. An Esri GIS platform combined with the overall public asset management umbrella of both physical "hard" assets and "soft" assets helps remove the traditional silos of structured municipal functions. While the hard assets are the typical physical assets or infrastructure assets, the soft assets of a municipality includes permits, license, code enforcement, right-of-ways and other land-focused work activities.
GIS is not a panacea; effective asset managers of physical assets such as buildings make informed-decisions about what to do and when to their assets in-order to maximize resource-return on their organizational goals. Sometimes, information displayed geospatially can help those decisions. Moreoften, however, deep understanding of markets, engineering systems, and human interaction enabled by analysis and synthesis of information lead to these effective decisions. The geospatial context may not be the most important one to make understand these facets.
Land-use development and planning is interconnected to other local government assets and work activities. Public asset management is the term that encompasses this subset of land-focused asset management, considering the importance that public assets affect other public assets and work activities and are important sources of revenue and are various points of citizen interaction.


