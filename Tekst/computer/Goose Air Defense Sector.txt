The Goose Air Defense Sector (GADS) is an inactive United States Air Force organization. Its last assignment was with the 26th Air Division, being stationed at Goose Air Force Base, Labrador, Canada. It was inactivated on 1 April 1966 and replaced by the 37th Air Division.



Command and control echelon of command, controlling both radar and fighter squadrons in Canada. Provided air defense of northeast North America. Assigned units of the 4732d Air Defense Group at Goose AFB in April 1960 and the 4731st Air Defense Group at Ernest Harmon AFB in June. Both groups were discontinued. These units included two Fighter-Interceptor Squadrons (FIS), although the 323rd FIS was discontinued almost immediately after transfer. Both squadrons flew F-102 "Delta Darts," although the 59th FIS had not completed its conversion from F-89 "Scorpions" when it was first assigned Also operated Semi Automatic Ground Environment (SAGE) Direction Center (DC-31) (Manual) at CFB North Bay, Ontario 46 20 15 N 079 24 42 W Inactivated in 1966 and personnel and equipment transferred to 37th Air Division.



Constituted as Goose Air Defense Sector on 7 March 1960
Activated on 1 April 1960
Inactivated on 1 April 1966.



64th Air Division, 1 April 1950 - 1 July 1963
26th Air Division, 1 July 1963 - 1 April 1966






Air Forces Iceland
Keflavik Airport, 4 September 1963 - 1 April 1966



4683d Air Defense Wing
Thule AB, Greenland, 1 July 1963 - 1 July 1965



4683d Air Base Group 1 July 1965 - 1 April 1966
Thule AB, Greenland, 1 October 1960
4684th Air Base Group
Sondrestrom AB, Greenland, 4 September 1963 - 1 April 1966



59th Fighter-Interceptor Squadron 6 Jun 1960 -
Goose AFB, Labrador, 1 April 1956 - 1 April 1966
323d Fighter-Interceptor Squadron
Ernest Harmon AFB, Newfoundland, 6 June 1960 - 1 July 1960



Goose AFB, Labrador, Canada, 1 April 1960 - 1 April 1966



F-89J 1960
F-102A 1960-1966




 This article incorporates public domain material from websites or documents of the Air Force Historical Research Agency.

Cornett, Lloyd H; Johnson, Mildred W (1980). A Handbook of Aerospace Defense Organization, 1946 - 1980 (PDF). Peterson AFB, CO: Office of History, Aerospace Defense Center. 
Maurer, Maurer, ed. (1982) [1969]. Combat Squadrons of the Air Force, World War II (PDF) (reprint ed.). Washington, DC: Office of Air Force History. ISBN 0-405-12194-6. 
Winkler, David F.; Webster, Julie L (1997). Searching the skies : the legacy of the United States Cold War Defense Radar Program (PDF). Champaign, IL: US Army Construction Engineering Research Laboratories. LCCN 97020912. 
Radomes.org Goose Air Defense Sector