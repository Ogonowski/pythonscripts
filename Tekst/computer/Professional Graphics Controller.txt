Professional Graphics Controller (PGC, often called Professional Graphics Adapter and sometimes Professional Graphics Array) was a graphics card manufactured by IBM for PCs. It consisted of three interconnected PCBs, and contained its own processor and memory. The PGC was, at the time of its release, the most advanced graphics card for the IBM XT and aimed for tasks such as CAD.
Introduced in 1984, the Professional Graphics Controller offered a maximum resolution of 640 480 with 256 colors at a refresh rate of 60 Hertz a higher resolution and color depth than EGA and VGA supported. This mode is not BIOS-supported. It was intended for the computer-aided design market and included 320 kB of display RAM and an on-board Intel 8088 microprocessor. The 8088 was placed directly on the card to permit rapid updates of video memory. Other cards forced the PC's CPU to write to video memory through a slower ISA bus. While never widespread in consumer-class personal computers, its US $4,290 list price compared favorably to US$50,000 dedicated CAD workstations of the time. It was discontinued in 1987 with the arrival of VGA and 8514.



The board was targeted at the CAD market, therefore a limited software support is to be expected. Yet, the only software known to support the PGC are IBM's Graphical Kernel System, P-CAD 4.5, Canyon State Systems CompuShow and AutoCAD 2.5.



PGC supports:
640x480 with 256 colors from a palette of 4096 (4 bits per color component)
Color Graphics Adapter text and graphics modes. Text modes use EGA 14-pixel font and have 400 rows.



The display adapter was composed of three physical circuit boards (one with the on-board microprocessor, firmware ROMs and video output connector, one providing CGA emulation, and the third mostly carrying RAM) and occupied two adjacent expansion slots on the XT or AT motherboard; the third card was located in between the two slots. The PGC could not be used in the original IBM PC without modification due to the different spacing of its slots.
In addition to its native 640 x 480 mode, the PGC optionally supported the documented text and graphics modes of the Color Graphics Adapter, which could be enabled using an onboard jumper. However, it was only partly register-compatible with CGA.



The PGC's matching display was the IBM 5175, an analog RGB monitor that is unique to it and not compatible with any other video card without modification. With hardware modification, the 5175 can be used with VGA, Macintosh, and various other analog RGB video sources. Some surplus 5175s in VGA-converted form were still sold by catalog retailers such as COMB as late as the early 1990s.



Matrox PG-640, PG-1280 and QG-640 (for the DEC MicroVAX)
Dell NEC MVA-1024 card
Everex EPGA
Orchid Technology TurboPGA
Vermont Microsystems IM-640, IM-1024



PC/GX
List of defunct graphics chips and card companies






Professional Graphics Controller: Notes - Pictures and programming information