An interceptor aircraft, or simply interceptor, is a type of fighter aircraft designed specifically to prevent successful missions by enemy aircraft, particularly bombers and reconnaissance aircraft. There are two general classes of interceptor: relatively lightweight aircraft built for high performance, and heavier aircraft designed to fly at night or in adverse weather and operate over longer ranges.
For daytime operations, conventional fighters normally fill the interceptor role, as well as many other missions. Daytime interceptors have been used in a defensive role since the World War I era, but are perhaps best known from several major actions during World War II, notably the Battle of Britain where the Supermarine Spitfire and Hawker Hurricane developed a good reputation. Few aircraft can be considered dedicated daytime interceptors, with the obvious exception for example of the Messerschmitt Me 163, and to a lesser degree designs like the Mikoyan-Gurevich MiG-15 which had heavy armament specifically intended for anti-bomber missions.
Night fighters and bomber destroyers are, by definition, interceptors of the heavy type, although initially they were rarely referred to as such. In the early Cold War era the combination of jet-powered bombers and nuclear weapons created air forces' demand for highly capable interceptors; it is during this period that the term is perhaps most recognized and used. Examples of classic interceptors of this era include the F-106 Delta Dart, Sukhoi Su-15 and English Electric Lightning. Through the 1960s rapid improvements in design led to most fighters having the performance to take on the interceptor role, and the strategic threat moved from bombers to intercontinental ballistic missiles (ICBMs). Dedicated interceptor designs became rare, with the only widely used examples designed after the 1960s being the Tornado F3 and Mikoyan MiG-31.



The first dedicated interceptor squadrons were formed up during World War I in order to defend London against attacks by Zeppelins, and later against early fixed-wing long-range bombers. Early units generally used aircraft withdrawn from front-line service, notably the Sopwith Pup. They were told about their target's location prior to take-off from a centralized command center in the Horse Guards building. The Pup proved to have too low performance to easily intercept the Gotha G.IV bombers, and the much higher performance Sopwith Camels supplanted them.
Before Second World War started, offensive bomber speeds had improved so much that it appeared that the interceptor mission would be effectively impossible. The visual and acoustic detection had a short range on the order of a few miles, and would not give interceptor enough time to climb to altitude before the bombers were already on their targets. This could be addressed through the use of a standing cover of aircraft, but only at enormous cost. The conclusion at the time was that "the bomber will always get through." The introduction of radar upset this equation.

In the 1950s, during the Cold War, a strong interceptor force was crucial for the great powers, as the best means to defend against threat of the unexpected nuclear strike by strategic bombers. Hence for a brief period of time they faced rapid development. At the end of the 1960s, the nuclear threat became unstoppable with the addition of various ballistic missiles which could not be intercepted approaching from outside the atmosphere at speeds as high as 5 7 km/s. Thus, the doctrine of mutually assured destruction replaced the trend of defense strengthening, and left interceptors with much less strategic justification. Their utility waned as the role became blurred into the role of the heavy air superiority fighters dominant in military thinking at the time.




The interceptor mission is, by its nature, a difficult one. Consider the desire to protect a single target from attack by long-range bombers. The bombers have the advantage of being able to select the parameters of the mission   attack vector, speed and altitude. As the bombers will ideally be detected at long range from the target, there is an enormous area from which an attack can start. The interceptor must be able to start, launch, climb to altitude, maneuver for attack and then attack the bomber before the bomber can cover the distance between detection and deploying its weapons.
Intercept aircraft sacrifice capabilities of the air superiority fighter (i.e., fighting enemy fighter aircraft in Air combat manoeuvring) by tuning their performance for either fast climbs and/or high speeds. The result is that interceptors often look very impressive on paper, typically outrunning, outclimbing and outgunning less specialized fighter designs. Yet they tend to fare poorly in fighter-to-fighter combat against the same "less capable" designs due to limited maneuverability.




In the spectrum of various interceptors, one design approach especially shows sacrifices necessary to achieve decisive benefit in a chosen aspect of performance. A "Point defense interceptor" is of a lightweight design, intended to spend most of its time on the ground located at the defended target, and able to launch on demand, climb to altitude, manoeuvre and then attack the bomber in a very short time, before the bomber can deploy its weapons.
At the end of Second World War, the Luftwaffe's most critical requirement was for interceptors as the Commonwealth and American air forces pounded German targets night and day. As the bombing effort grew, notably in early 1944, the Luftwaffe introduced a rocket-powered design, the Messerschmitt Me 163 Komet, in the very-short-range interceptor role. The engine allowed about 7 minutes of powered flight, but offered such tremendous performance that they could fly right by the defending fighters. The Me 163 required an airbase, however, which were soon under constant attack. Following the Emergency Fighter Program, the Germans developed even odder designs, such as the Bachem Ba 349 Natter, which launched vertically and thus eliminated the need for an airbase. In general all these initial German designs proved difficult to operate, often becoming death traps for their pilots, and had little effect on the bombing raids.
In the initial stage of Cold War, bombers were expected to attack flying higher and faster, even at transonic speeds. Initial transonic and supersonic fighters had modest internal fuel tanks in their slim fuselages, but a very high fuel consumption. This led fighter prototypes emphasizing acceleration and operational ceiling, with a sacrifice on the loiter time, essentially limiting them to point defense role. Such were the mixed jet/rocket power Convair XF-92 or Saunders Roe SR.53. The Soviet and Western trials with zero length launch were also related. None of these found practical use. Designs that depended solely on jet engines achieved more success with the F-104 Starfighter (initial A version) and the English Electric Lightning.
The role of manned point defense designs was reassigned to unmanned interceptors surface-to-air missiles (SAMs) which first reached an adequate level in 1954 1957. SAM advancements ended the concept of massed high-altitude bomber operations, in favor pairs of aircraft using a combination of techniques colloquially known as "flying below the radar". By flying terrain masking low-altitude nap-of-the-earth flight profiles the effective range, and therefore reaction time, of ground based radar was limited to at best the radar horizon. In the case of ground radar systems this can be countered by placing radar systems on mountain tops to extend the radar horizon, or through placing high performance radars in interceptors or in AWACs aircraft used to direct point defense interceptors.



As capabilities continued to improve, especially through the widespread introduction of the jet engine, and the adoption of high speed low level flight profiles the time allowed between detection and interception dropped. Even the most advanced point defence interceptors combined with long-range radars were struggling to keep the reaction time down enough to be effective. Fixed times, like the time needed for the pilot to climb into the cockpit, became an increasing portion of the overall mission time, there were few ways to reduce this. During the Cold War in times of heightened tensions, quick reaction alert (QRA) aircraft were kept piloted, fully fuelled and armed, with the engines running at idle, the aircraft being kept topped up with fuel via hoses to underground fuel tanks. If a possible intruder was identified, the aircraft would be ready to take off as soon as the external fuel lines were detached. However, keeping QRA aircraft at this state of readiness was physically and mentally draining to the pilots, rapidly depleted the engine and airframe life, and was expensive in terms of fuel.
This ushered in a move to longer-range designs with extended loiter times became the main design concept. These area defense interceptors or area defense fighters were in general larger designs intended to stay on lengthy patrol and protect a much larger area from attack, depending on great detection capabilities, both in the aircraft themselves and operating in concert with AWACS, rather than high speed to reach targets. The exemplar of this concept being the Tupolev Tu-28, the later Panavia Tornado ADV was able to achieve long range in a much smaller airframe through the use of very efficient engines. Rather than focusing on acceleration and climb rate, the design emphasis is on range and missile carrying capacity, which together translate into combat endurance; and look-down/shoot-down radars good enough to detect and track fast moving interdictors against ground clutter, and the capability to provide guidance to air-to-air missiles (AAM) against these targets. High speed and acceleration was put into long-range and medium-range AAMs, and agility into short range dog fighting AAMs, rather than into the aircraft themselves. They were first to introduce all-weather avionics, assuring successful operations during night, rain, snow, or fog.
Countries that were strategically dependent on surface fleet, most notably US and UK, maintained also fleet defense fighters such as the F-14 Tomcat that acted very similarly to their non-naval counterparts.







During the Cold War, an entire military service, not just an arm of the pre-existing air force, was designated for their use. The planes of the Soviet Anti-Air Defense (PVO-S) differed from those of the Soviet Air Force (VVS) in that they were by no means small or crudely simple, but huge and refined with large, sophisticated radars; they could not take off from grass, only concrete runways; they could not be disassembled and shipped back to a maintenance center in a boxcar. Similarly, their pilots were given less training in combat maneuvers, and more in radio-directed pursuit. The main interceptor was first the Su-9, then the Su-15, and then the MiG-25. The auxiliary Tu-28, an area range interceptor, was notably the heaviest fighter aircraft ever to see service in the world. The latest and most advanced interceptor aircraft is the MiG-31. Although it is the first one to carry an internal cannon, to this day it remains too cumbersome for dogfights with the contemporary air superiority fighters.
Russia, despite merging the PVO into the VVS, still plans to maintain a number of dedicated interceptors.




From 1946 to 1980 the United States maintained a dedicated Aerospace Defense Command, consisting primarily of dedicated interceptors. Many post-war designs were of limited performance, including designs like the F-86D and F-89 Scorpion. In the late 1940s ADC started a project to build a much more advanced interceptor under the 1954 interceptor effort, which eventually delivered the F-106 Delta Dart after a lengthy development process. Further replacements were studied, notably the NR-349 proposal during the 1960s, but came to nothing as the USSR strengthened their strategic force with ICBMs. Hence, the F-106 ended up serving as the primary USAF interceptor into the 1980s. As it was retired, intercept missions were assigned to the contemporary F-15 and F-16 fighters, among their other roles. Presently, the F-22 is the USA's latest combat aircraft that serves in part as an interceptor.
In the 1950s, the United States Navy led an unsuccessful F6D Missileer project. Later it launched the development of a large F-111B fleet air defense fighter, but this project was cancelled too. Finally, the role was assigned to the F-14 Tomcat, carrying AIM-54 Phoenix missiles. This aircraft was well-capable of fighter-to-fighter combat, as well as air interdiction missions, so it does not exactly fit the "pure" interceptor niche. Both the fighter and the missile were retired in 2006.




The British Royal Air Force operated a supersonic day fighter, the English Electric Lightning, alongside the Gloster Javelin in the subsonic night/all-weather role. Efforts to replace the Javelin with a supersonic design under Operational Requirement F.155 came to naught. The air defence variant (ADV) of the Panavia Tornado was introduced in the 1980s, and continued to serve until replaced with a multirole design, the Eurofighter Typhoon.




The Shenyang J-8 (Chinese:  -8; NATO reporting name: Finback) is a high-speed, high-altitude Chinese-built single-seat interceptor. Initially designed in the early 1960s to counter US-built B-58 Hustler bombers, F-105 Thunderchief fighter-bombers and Lockheed U-2 spy planes, it still retains the ability to 'sprint' at Mach 2-plus speeds and later versions can carry medium-range PL-12/SD-10 MRAAM missiles for interception purposes. The PLAAF/PLANAF currently still operates approximately 300 or so J-8s of various configurations.




Several other countries also introduced interceptor designs, although in the 1950s-1960s several planned interceptors never came to fruition, with the expectation that missiles would replace bombers.
The Argentine FMA I.Ae. 37 was a prototype jet fighter developed during the 1950s. It never flew and was cancelled in 1960.
The Canadian subsonic Avro CF-100 served in numbers through 1950s. Its supersonic replacement, the Avro Arrow, was controversially cancelled in 1959.
The Swedish Saab 35 Draken was specifically designed for intercepting aircraft passing Swedish airspace at high altitudes in the event of a war between the Soviet Union and NATO. With the advent of low flying cruise-missiles and high-altitude AA-missiles the flight profile was changed, but regained the interceptor profile with the final version J 35J.



Air superiority fighter
Interdictor
Air interdiction
Anti-aircraft warfare
Escort fighter
Heavy fighter
Fighter aircraft


