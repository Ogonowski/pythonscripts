Paper data storage refers to the use of paper as a data storage device. This includes writing, illustrating, and the use of data that can be interpreted by a machine or is the result of the functioning of a machine. A defining feature of paper data storage is the ability of humans to produce it with only simple tools and interpret it visually.
Though this is now mostly obsolete, paper was once also an important form of computer data storage.



Before paper was used for storing data, it had been used in several applications for storing instructions to specify a machine s operation. The earliest use of paper to store instructions for a machine was the work of Basile Bouchon who, in 1725, used punched paper rolls to control textile looms. This technology was later developed into the wildly successful Jacquard loom. The 19th century saw several other uses of paper for controlling machines. In 1846, telegrams could be prerecorded on punched tape and rapidly transmitted using Alexander Bain's automatic telegraph. Several inventors took the concept of a mechanical organ and used paper to represent the music.
In the late 1880s Herman Hollerith invented the recording of data on a medium that could then be read by a machine. Prior uses of machine readable media, above, had been for control (Automatons, Piano rolls, looms, ...), not data. "After some initial trials with paper tape, he settled on punched cards..." Hollerith's method was used in the 1890 census and the completed results were "... finished months ahead of schedule and far under budget". Hollerith's company eventually became the core of IBM.
Other technologies were also developed that allowed machines to work with marks on paper instead of punched holes. This technology was widely used for tabulating votes and grading standardized tests. Barcodes made it possible for any object that was to be sold or transported to have some computer readable information securely attached to it. Banks used magnetic ink on checks, supporting MICR scanning.
In an early electronic computing device, the Atanasoff-Berry Computer, electric sparks were used to singe small holes in paper cards to represent binary data. The altered dielectric constant of the paper at the location of the holes could then be used to read the binary data back into the machine by means of electric sparks of lower voltage than the sparks used to create the holes. This form of paper data storage was never made reliable and was not used in any subsequent machine.
As of 2014, Universal Product Code barcodes, first used in 1974, are ubiquitous.
Some people recommend a width of at least 3 pixels for each minimum-width gap and each minimum-width bar for 1D barcodes; and a width of at least 4 pixels -- e.g., a 4x4 pixel = 16 pixel module for 2D barcodes. For a typical black-and-white barcode scanned by a typical 300 dpi image scanner, and assuming roughly half the space is occupied by finder patterns, fiducial alignment patterns, and error detection and correction codes, that recommendation gives a maximum data density of roughly 50 bits per linear inch (about 2 bit/mm) for 1D barcodes, and roughly 2 800 bits per square inch (about 4.4 bit/mm^2).



The limits of data storage depend on the technology to write and read such data. For example, an 8"x10" (roughly A4 without margins) 300dpi 8-bit greyscale image map contains 7.2 megabytes of data assuming a scanner can accurately reproduce the printed image to that resolution and color depth, and a program can accurately interpret such an image. A similarly sized image in 2400dpi 24-bit true color theoretically contains 1.38 gigabytes of information.



Banknote read by Vending machine
Book music
Edge-notched card
Index card
Kimball tag
Machine-readable medium
Magnetic ink character recognition
Mark sense
Music roll
Optical mark recognition
Paper disc
Perfin
Perforation
Punched tape
Spindle (stationery)
Stenotype
Ticker tape
DataGlyphs
PaperBack data storage



^ Columbia University Computing History - Herman Hollerith
^ U.S. Census Bureau: Tabulation and Processing
^ Accusoft. "Using Barcodes in Documents   Best Practices". 2007. Retrieved 2014-04-25.