Intel Hub Architecture (IHA) was Intel's architecture for the 8xx family of chipsets, starting circa 1999 with the Intel 810. It uses a memory controller hub (MCH) that is connected to an I/O Controller Hub (ICH) via a 266 MB/s bus. The MCH chip supports memory and AGP, while the ICH chip provides connectivity for PCI, USB, sound, IDE hard disks and LAN.
Intel claims that, because of the high-speed channel between the sections, the IHA is much faster than the earlier northbridge/southbridge design, which hooked all low-speed ports to the PCI bus. The IHA also optimizes data transfer based on data type.



Intel Hub Interface 2.0 was employed in Intel's line of E7xxx server chipsets. This new revision allowed for dedicated data paths for transferring greater than 1.0 GB/s of data to and from the MCH, which support I/O segments with greater reliability and faster access to high-speed networks.



IHA is now considered obsolete and no longer used, being superseded by the Platform Controller Hub introduced with the Intel 5 Series chipset in 2008.


