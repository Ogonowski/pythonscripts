A data storage device is a device for recording (storing) information (data). Recording can be done using virtually any form of energy, spanning from manual muscle power in handwriting, to acoustic vibrations in phonographic recording, to electromagnetic energy modulating magnetic tape and optical discs.
A storage device may hold information, process information, or both. A device that only holds information is a recording medium. Devices that process information (data storage equipment) may either access a separate portable (removable) recording medium or a permanent component to store and retrieve data.
Electronic data storage requires electrical power to store and retrieve that data. Most storage devices that do not require vision and a brain to read data fall into this category. Electromagnetic data may be stored in either an analog data or digital data format on a variety of media. This type of data is considered to be electronically encoded data, whether it is electronically stored in a semiconductor device, for it is certain that a semiconductor device was used to record it on its medium. Most electronically processed data storage media (including some forms of computer data storage) are considered permanent (non-volatile) storage, that is, the data will remain stored when power is removed from the device. In contrast, most electronically stored information within most types of semiconductor (computer chips) microcircuits are volatile memory, for it vanishes if power is removed.
Except for barcodes and OCR data, electronic data storage is easier to revise and may be more cost effective than alternative methods due to smaller physical space requirements and the ease of replacing (rewriting) data on the same medium. However, the durability of methods such as printed data is still superior to that of most electronic storage media. The durability limitations may be overcome with the ease of duplicating (backing-up) electronic data.



Devices that are not used exclusively for recording such as hands, mouths, musical instruments, and devices that are intermediate the storing/retrieving process, like eyes, ears, cameras, scanners, microphones, speakers, monitors, or video projectors, are generally not considered storage devices. Devices that are exclusively for recording such as printers, exclusively for reading, like barcode readers, or devices that process only one form of information, like phonographs may or may not be considered storage devices. In computing these are known as input/output devices.
All information is data. However, not all data is information.
Many data storage devices are also media players. Any device that can store and playback multimedia may also be considered a media player such as in the case with the HD media player. Designated hard drives are used to play saved or streaming media on home cinemas or home theater PCs.



In a recent study in Science it was estimated that the world's technological capacity to store information in analog and digital devices grew from less than 3 (optimally compressed) exabytes in 1986, to 295 (optimally compressed) exabytes in 2007, and doubles roughly every 3 years.
It is estimated that the year 2002 marked the beginning of the digital age for information storage, the year that marked the date when human kind started to store more information on digital than on analog storage devices. In 1986 only 1% of the world's capacity to store information was in digital format, which grew to 3% by 1993, 25% in the year 2000, and exploded to 97% of the world's storage capacity by 2007.



Any input/output equipment may be considered data storage equipment if it writes to and reads from a data storage medium.



Data storage equipment uses either:
portable methods (easily replaced),
semi-portable methods, requiring mechanical disassembly tools and/or opening a chassis, or
Volatile methods, meaning loss of memory if disconnected from the unit.
The following are examples of those methods:



Hand crafting
Flat surface
Printmaking
Photographic

Fabrication
Automated assembly
Textile
Molding
Solid freeform fabrication

Cylindrical accessing
Memory card reader/drive
Tape drive
Mono reel or reel-to-reel
Compact Cassette player/recorder
Data cartridge (tape)

Disk accessing
Disk drive
Disk pack
Disk enclosure

ROM cartridge
Peripheral networking
Flash memory devices



Hard disk drive
non-volatile RAM



Volatile RAM
Neurons



A recording medium is a physical material that holds data expressed in any of the existing recording formats. With electronic media, the data and the recording medium is sometimes\s part of the surface of the medium.
Some recording media may be temporary either by design or by nature. Volatile organic compounds may be used to preserve the environment or to purposely make data expire over time. Data such as smoke signals or skywriting are temporary by nature.




Optical
Any object visible to the eye, used to mark a location such as a stone, flag, or skull.
Any crafting material used to form shapes such as clay, wood, metal, glass, wax, or quipu.
Any hard surface that could hold carvings.
Any branding surface that would scar under intense heat (chiefly for livestock or humans).
Any marking substance such as paint, ink, or chalk.
Any surface that would hold a marking substance such as, papyrus, paper, or skin.

Chemical
RNA
DNA   for uses of DNA by people for storing information, see DNA digital data storage
Pheromone




Chemical
Dipstick

Thermodynamic
Thermometer

Photochemical
Photographic film

Mechanical
Pins and holes
Paper
Punched card
Paper tape
Music roll

Music box cylinder or disk

Grooves (See also Audio Data)
Phonograph cylinder
Gramophone record
Dictabelt (groove on plastic belt)
Capacitance Electronic Disc

Magnetic storage
Wire recording (stainless steel wire)
Magnetic tape
Drum memory (magnetic drum)
Floppy disk

Optical storage
Optical jukebox
Photographic paper
X-ray
Microform
Hologram
Projected foil
Optical disc
Magneto-optical drive
Holographic data storage
3D optical data storage

Electrical
Semiconductor used in volatile random-access memory
Floating-gate transistor used in non-volatile memory cards



A typical way to classify data storage media is to consider its shape and type of movement (or non-movement) relative to the read/write device(s) of the storage apparatus as listed:
Paper card storage
Punched card (mechanical)

Cams and tracers (pipe organ combination-action memory memorizing stop selections)
Tape storage (long, thin, flexible, linearly moving bands)
Paper tape (mechanical)
Magnetic tape (a tape passing one or more read/write/erase heads)

Disk storage (flat, round, rotating object)
Gramophone record (used for distributing some 1980s home computer programs) (mechanical)
Floppy disk, ZIP disk (removable) (magnetic)
Holographic
Optical disc such as CD, DVD, Blu-ray Disc
Minidisc
Hard disk drive (magnetic)

Magnetic bubble memory
Flash memory/memory card (solid state semiconductor memory)
xD-Picture Card
MultiMediaCard
USB flash drive (also known as a "thumb drive" or "keydrive")
SmartMedia
CompactFlash I and II
Secure Digital
Sony Memory Stick (Std/Duo/PRO/MagicGate versions)
Solid-state drive

Bekenstein (2003) foresees that miniaturization might lead to the invention of devices that store bits on a single atom.



Especially for carrying around data, the weight and volume per MB are relevant. They are quite large for written and printed paper compared with modern electronic media. On the other hand, written and printer paper do not require (the weight and volume of) reading equipment, and handwritten edits only require simple writing equipment, such as a pen.
With mobile data connections the data need not be carried around to be available.


