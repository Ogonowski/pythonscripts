Lassa fever or Lassa hemorrhagic fever (LHF) is an acute viral hemorrhagic fever caused by the Lassa virus and first described in 1969 in the town of Lassa, in Borno State, Nigeria. Lassa fever is a member of the Arenaviridae virus family. Similar to ebola, clinical cases of the disease had been known for over a decade, but had not been connected with a viral pathogen.
Lassa frequently infects people in West Africa. It results in 300,000 to 500,000 cases annually and causes about 5,000 deaths each year. Outbreaks of the disease have been observed in Nigeria, Liberia, Sierra Leone, Guinea, and the Central African Republic. The primary animal host of the Lassa virus is the Natal multimammate mouse (Mastomys natalensis), an animal found in most of sub-Saharan Africa. The virus is probably transmitted by contact with the feces or urine of animals accessing grain stores in residences. Given its high rate of incidence, Lassa fever is a major problem in affected countries.



In 80% of cases, the disease is asymptomatic, but in the remaining 20%, it takes a complicated course. The virus is estimated to be responsible for about 5,000 deaths annually. The fever accounts for up to one-third of deaths in hospitals within the affected regions and 10 to 16% of total cases.
After an incubation period of six to 21 days, an acute illness with multiorgan involvement develops. Nonspecific symptoms include fever, facial swelling, and muscle fatigue, as well as conjunctivitis and mucosal bleeding. The other symptoms arising from the affected organs are:
Gastrointestinal tract
Nausea
Vomiting (bloody)
Diarrhea (bloody)
Stomach ache
Constipation
Dysphagia (difficulty swallowing)
Hepatitis

Cardiovascular system
Pericarditis
Hypertension
Hypotension
Tachycardia (abnormally high heart rate)

Respiratory tract
Cough
Chest pain
Dyspnoea
Pharyngitis
Pleuritis

Nervous system
Encephalitis
Meningitis
Unilateral or bilateral hearing deficit
Seizures

Clinically, Lassa fever infections are difficult to distinguish from other viral hemorrhagic fevers such as Ebola and Marburg, and from more common febrile illnesses such as malaria.
The virus is excreted in urine for 3-9 weeks and in semen for three months.




Lassa virus is zoonotic (transmitted from animals), in that it spreads to humans from rodents, specifically multimammate mice (Mastomys natalensis). This is probably the most common mouse in equatorial Africa, ubiquitous in human households and eaten as a delicacy in some areas. In these rodents, infection is in a persistent asymptomatic state. The virus is shed in their excreta (urine and feces), which can be aerosolized. In fatal cases, Lassa fever is characterized by impaired or delayed cellular immunity leading to fulminant viremia.
Infection in humans typically occurs by exposure to animal excrement through the respiratory or gastrointestinal tracts. Inhalation of tiny particles of infectious material (aerosol) is believed to be the most significant means of exposure. It is possible to acquire the infection through broken skin or mucous membranes that are directly exposed to infectious material. Transmission from person to person has also been established, presenting a disease risk for healthcare workers. Frequency of transmission by sexual contact has not been established.



A range of laboratory investigations are performed to diagnose the disease and assess its course and complications. An ELISA test for antigen and IgM antibodies give 88% sensitivity and 90% specificity for the presence of the infection. Other laboratory findings in Lassa fever include lymphopenia (low white blood cell count), thrombocytopenia (low platelets), and elevated aspartate aminotransferase levels in the blood. Lassa fever can also be found in cerebrospinal fluid. In West Africa, where Lassa is most prevalent, it is difficult for doctors to diagnose due to the absence of proper equipment to perform tests. In cases with abdominal pain, diagnoses in countries where Lassa is endemic are often made for other illnesses, such as appendicitis and intussusception, delaying treatment with ribavirin.




Control of the Mastomys rodent population is impractical, so measures are limited to keeping rodents out of homes and food supplies, as well as maintaining effective personal hygiene. Gloves, masks, laboratory coats, and goggles are advised while in contact with an infected person. These issues in many countries are monitored by a department of public health. In less developed countries, these types of organizations may not have the necessary means to effectively control outbreaks.
Researchers at the USAMRIID facility, where military biologists study infectious diseases, have a promising vaccine candidate. They have developed a replication-competent vaccine against Lassa virus based on recombinant vesicular stomatitis virus vectors expressing the Lassa virus glycoprotein. After a single intramuscular injection, test primates have survived lethal challenge, while showing no clinical symptoms.



All persons suspected of Lassa fever infection should be admitted to isolation facilities and their body fluids and excreta properly disposed of.
Early and aggressive treatment using ribavirin was pioneered by Joe McCormick in 1979. After extensive testing, early administration was determined to be critical to success. Additionally, ribavirin is almost twice as effective when given intravenously as when taken by mouth. Ribavirin is a prodrug which appears to interfere with viral replication by inhibiting RNA-dependent nucleic acid synthesis, although the precise mechanism of action is disputed. The drug is relatively inexpensive, but the cost of the drug is still very high for many of those in West African states. Fluid replacement, blood transfusion, and fighting hypotension are usually required. Intravenous interferon therapy has also been used.
When Lassa fever infects pregnant women late in their third trimester, induction of delivery is necessary for the mother to have a good chance of survival. This is because the virus has an affinity for the placenta and other highly vascular tissues. The fetus has only a one in ten chance of survival no matter what course of action is taken; hence, the focus is always on saving the life of the mother. Following delivery, women should receive the same treatment as other Lassa fever patients.
Work on a vaccine is continuing, with multiple approaches showing positive results in animal trials.



About 15-20% of hospitalized Lassa fever patients will die from the illness. The overall mortality rate is estimated to be 1%, but during epidemics, mortality can climb as high as 50%. The mortality rate is greater than 80% when it occurs in pregnant women during their third trimester; fetal death also occurs in nearly all those cases. Abortion decreases the risk of death to the mother. Some survivors experience lasting effects of the disease.
Because of treatment with ribavirin, fatality rates are continuing to decline.



The dissemination of the infection can be assessed by prevalence of antibodies to the virus in populations of:
Sierra Leone - 8 52%
Guinea - 4 55%
Nigeria - about 21%
Lassa fever is a viral hemorrhagic fever in West Africa. Studies show up to half a million cases of Lassa fever per year in West Africa, with about 5,000 resulting in death. Lassa virus was detected in 25 of 60 (42%) patients in northern and central Edo State. The Lassa Virus affects adults and children alike.
While most humans are infected either from contact with an infected rat or inhalation of air contaminated with rat excretions, like other hemorrhagic fevers, Lassa fever can be transmitted directly from one human to another. It can be contracted through direct contact with infected human blood excretions and secretions, including through sexual contact. No evidence of airborne transmission person-to-person is seen. Transmission through breast milk has also been observed. 


