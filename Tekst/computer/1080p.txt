1080p (also known as Full HD or FHD and BT.709) is a set of HDTV high-definition video modes characterized by 1080 horizontal lines of vertical resolution and progressive scan, as opposed to interlaced, as is the case with the 1080i display standard. The term usually assumes a widescreen aspect ratio of 16:9, implying a resolution of 1920x1080 (2.1 megapixel) often marketed as Full HD.




Any display device that advertises 1080p typically refers to the ability to accept 1080p signals in native resolution format, which means there are a true 1920 pixels in width and 1080 pixels in height, and the display is not over-scanning, under-scanning, or reinterpreting the signal to a lower resolution. The HD ready 1080p logo program, by DIGITALEUROPE, requires that certified TV sets support 1080p 24 fps, 1080p 50 fps, and 1080p 60 fps formats, among other requirements, with fps meaning frames per second.
For live broadcast applications, a high-definition progressive scan format operating at 1080p at 50 or 60 frames per second is currently being evaluated as a future standard for moving picture acquisition. EBU has been endorsing 1080p50 as a future-proof production format because it improves resolution and requires no deinterlacing, allows broadcasting of standard 1080i25 and 720p50 signal alongside 1080p50 even in the current infrastructure and is compatible with DCI distribution formats.
1080p50/p60 production format will require a whole new range of studio equipment including cameras, storage and editing systems, and contribution links (such as Dual-link HD-SDI and 3G-SDI) as it has doubled the data rate of current 50 or 60 fields interlaced 1920x1080 from 1.485 Gbit/s to nominally 3 Gbit/s using uncompressed RGB encoding. Most current revisions of SMPTE 372M, SMPTE 424M and EBU Tech 3299 require YCbCr color space and 4:2:2 chroma subsampling for transmitting 1080p50 (nominally 2.08 Gbit/s) and 1080p60 signal.
Recent studies show that for digital broadcasts compressed with H.264/AVC, transmission bandwidth savings of interlaced video over fully progressive video are minimal even when using twice the frame rate; i.e., 1080p50 signal (50 progressive frames per second) actually produces the same bit rate as 1080i50 signal (25 interlaced frames or 50 sub-fields per second).



In the United States, the original ATSC standards for HDTV supported 1080p video, but only at the frame rates of 23.976, 24, 25, 29.97 and 30 frames per second (colloquially known as 1080p24, 1080p25 and 1080p30).
In July 2008, the ATSC standards were amended to include H.264/MPEG-4 AVC compression and 1080p at 50, 59.94 and 60 frames per second (1080p50 and 1080p60). Such frame rates require H.264/AVC High Profile Level 4.2, while standard HDTV frame rates only require Level 4.0.
This update is not expected to result in widespread availability of 1080p60 programming, since most of the existing digital receivers in use would only be able to decode the older, less-efficient MPEG-2 codec, and because there is a limited amount of bandwidth for subchannels.



In Europe, 1080p25 signals have been supported by the DVB suite of broadcasting standards. The 1080p50 is considered to be a future-proof production format and, eventually, a future broadcasting format. 1080p50 broadcasting should require the same bandwidth as 1080i50 signal and only 15 20% more than that of 720p50 signal due to increased compression efficiency, though 1080p50 production requires more bandwidth and/or more efficient codecs such as JPEG 2000, high-bitrate MPEG-2, or H.264/AVC and HEVC.
From September 2009, ETSI and EBU, the maintainers of the DVB suite, added support for 1080p50 signal coded with MPEG-4 AVC High Profile Level 4.2 with Scalable Video Coding extensions or VC-1 Advanced Profile compression; DVB also supports 1080p encoded at ATSC frame rates of 23.976, 24, 29.97, 30, 59.94 and 60.
EBU requires that legacy MPEG-4 AVC decoders should avoid crashing in the presence of SVC and/or 1080p50 (and higher resolution) packets. SVC enables forward compatibility with 1080p50 and 1080p60 broadcasting for older MPEG-4 AVC receivers, so they will only recognize baseline SVC stream coded at a lower resolution or frame rate (such as 720p60 or 1080i60) and will gracefully ignore additional packets, while newer hardware will be able to decode full-resolution signal (such as 1080p60).






In the United States, 1080p over-the-air broadcasts still do not exist as of September 2015; all major networks use either 720p60 or 1080i60 encoded with MPEG-2. However, satellite services (e.g., DirecTV, XstreamHD and Dish Network) utilize the 1080p/24-30 format with MPEG-4 AVC/H.264 encoding for pay-per-view movies that are downloaded in advance via satellite or on-demand via broadband. At this time, no pay service channel such as USA, HDNET, etc. nor premium movie channel such as HBO, etc., stream their services live to their distributors (MVPD) in this format because many MVPDs, especially DBS and cable, do not have sufficient bandwidth to provide the format streaming live to their subscribers without negatively impacting their current services. In addition is the high "cost" of using more bandwidth for one 1080p/24 channel than that necessary for a 1080i or even a 720p channel and for only those relatively few subscribers who have HDTV devices that can display 1080p/24 being an efficient use of their limited bandwidth.
For material that originates from a progressive scanned 24 frame/s source (such as film), MPEG-2 lets the video be coded as 1080p24, irrespective of the final output format. These progressively-coded frames are tagged with metadata (literally, fields of the PICTURE header) instructing a decoder how to perform a 3:2 pulldown to interlace them. While the formal output of the MPEG-2 decoding process from such stations is 1080i60, the actual content is coded as 1080p24 and can be viewed as such (using a process known as inverse telecine) since no information is lost even when the broadcaster performs the 3:2 pulldown.



Blu-ray Discs are able to hold 1080p HD content, and most movies released on Blu-ray Disc produce a full 1080p HD picture when the player is connected to a 1080p HDTV via an HDMI cable. The Blu-ray Disc video specification allows encoding of 1080p23.976, 1080p24, 1080i50, and 1080i59.94. Generally this type of video runs at 30 to 40 megabits per second, compared to the 3.5 megabits per second for conventional standard definition broadcasts.



Smartphones with 1080p FullHD display have been available on the market since 2012. As of the end of 2014, it is the standard for mid to high end smartphones and many of the flagship devices of 2014 used even higher resolutions.



Several websites, including YouTube, allow videos to be uploaded in the 1080p format. YouTube streams 1080p content at approximately 4 megabits per second compared to Blu-ray's 30 to 40 megabits per second. Digital distribution services also deliver 1080p content, such as movies available on Blu-ray Disc and/or from broadcast sources. This can include distribution services like peer-to-peer websites and public or private tracking networks.
Netflix is offering SuperHD content in the US and other countries through select internet providers since 2013.



As of 2012, most consumer televisions being sold provide 1080p inputs, mainly via HDMI, and support full high-definition resolutions. 1080p resolution is available in all types of television, including plasma, LCD, DLP front and rear projection and LCD projection.
For displaying film-based 1080i60 signals, a scheme called 3:2 pulldown reversal (reverse telecine) is beginning to appear in some newer 1080p displays, which can produce a true 1080p quality image from film-based 1080i60 programs. Similarly, 25fps content broadcast at 1080i50 may be deinterlaced to 1080p content with no loss of quality or resolution.
AV equipment manufacturers have adopted the term Full HD to mean a set can display all available HD resolutions up to 1080p. The term is misleading, however, because it does not guarantee the set is capable of rendering digital video at all frame rates encoded in source files with 1080 pixel vertical resolution. Most notably, a "Full HD" set is not guaranteed to support the 1080p24 format, leading to consumer confusion.
DigitalEurope (formerly EICTA) maintains the HD ready 1080p logo program that requires the certified TV sets to support 1080p24, 1080p50, and 1080p60, without overscan/underscan and picture distortion.



Most widescreen cathode ray tube (CRT) and liquid crystal display (LCD) monitors can natively display 1080p content. For example, widescreen WUXGA monitors support 1920x1200 resolution, which can display a pixel for pixel reproduction of the 1080p (1920x1080) format. Additionally, many 23, 24, and 27-inch (690 mm) widescreen LCD monitors use 1920x1200 as their native resolution; 30 inch displays can display beyond 1080p at up to 2560x1600 (1600p). Many 27" monitors have native resolutions of 2560x1440 and hence operate at 1440p.



Video game consoles such as Sony's PlayStation 3, Microsoft's Xbox 360 and Nintendo's Wii U, as well as microconsoles like OUYA, GameStick and Nvidia Shield can display upscaled games and video content in 1080p, although the vast majority of games are rendered at lower resolutions. For all of the consoles, this is done through HDMI connections (in the case of the Xbox 360, HDMI is only available on consoles manufactured after June 2007). Additionally, the upscaled 1080p video is available on the PlayStation 3 and Xbox 360 via an analog component/D-Terminal (YPBPR) connection,  as well as the VGA connection on the Xbox 360. On the PlayStation 3, developers must provide specific resolution support at the software level as there is no hardware upscaling support, whereas on the Xbox 360 games can be upscaled using a built in hardware scaler chip. However, most games on both consoles do not run at a native 1080p resolution.
The Wii U, PlayStation 3, and Xbox 360 provide 1080p video services. Sony provides both the PlayStation Store VOD service and Blu-ray Disc playback. Microsoft provides the Zune Video Marketplace for "instant on" 1080p  video content but does not have Blu-ray disc playback capability. It does however support the now-defunct HD DVD disc standard via the Xbox 360 HD DVD Player add-on. Both consoles also offer support for streaming 1080p  content in various formats over home network from other computers, and also via USB connection to external storage devices.
^  Due to potential copyright issues, when an analog component connection is used, only system menus and gameplay are available in 1080p; video content is displayed at a lower resolution or in 1080i.^  While 1080p analog component output is supported by the consoles, some display hardware will only accept component connections up to 1080i.



Many consumer camcorders, professional video and DSLR cameras, and smartphones can capture 1080p24, 1080p25, or 1080p30 video, often encoding it in progressive segmented frame format.




List of common resolutions
4320p, 2160p, 1440p, 1080i, 720p, 576p, 576i, 480p, 480i, 360p, 240p
Progressive segmented frame
High-definition television
HD Ready 1080p
Display resolution






1080p and the Acuity of Human Vision Audioholics Home Theater Magazine. 2 April 2007.
High Definition 1080p TV: Why You Should Be Concerned. Secrets of Home Theater and High Fidelity. 28 February 2007.
The Facts and Fiction of 1080p. 17 April 2006.
High Definition (HD) Image Formats for Television Production (EBU technical publication). December 2004.