Race and ethnicity in the United States Census, defined by the federal Office of Management and Budget (OMB) and the United States Census Bureau, are self-identification data items in which residents choose the race or races with which they most closely identify, and indicate whether or not they are of Hispanic or Latino origin (the only categories for ethnicity).
The racial categories represent a social-political construct for the race or races that respondents consider themselves to be and "generally reflect a social definition of race recognized in this country." OMB defines the concept of race as outlined for the U.S. Census as not "scientific or anthropological" and takes into account "social and cultural characteristics as well as ancestry", using "appropriate scientific methodologies" that are not "primarily biological or genetic in reference." The race categories include both racial and national-origin groups.
Race and ethnicity are considered separate and distinct identities, with Hispanic or Latino origin asked as a separate question. Thus, in addition to their race or races, all respondents are categorized by membership in one of two ethnic categories, which are "Hispanic or Latino" and "Not Hispanic or Latino". However, the practice of separating "race" and "ethnicity" as different categories has been criticized both by the American Anthropological Association and members of U.S. Commission on Civil Rights.
In 1997, OMB issued a Federal Register notice regarding revisions to the standards for the classification of federal data on race and ethnicity. OMB developed race and ethnic standards in order to provide "consistent data on race and ethnicity throughout the Federal Government. The development of the data standards stem in large measure from new responsibilities to enforce civil rights laws." Among the changes, OMB issued the instruction to "mark one or more races" after noting evidence of increasing numbers of interracial children and wanting to capture the diversity in a measurable way and having received requests by people who wanted to be able to acknowledge their or their children's full ancestry rather than identifying with only one group. Prior to this decision, the Census and other government data collections asked people to report only one race.



The OMB states, "many federal programs are put into effect based on the race data obtained from the decennial census (i.e., promoting equal employment opportunities; assessing racial disparities in health and environmental risks). Race data are also critical for the basic research behind many policy decisions. States require these data to meet legislative redistricting requirements. The data are needed to monitor compliance with the Voting Rights Act by local jurisdictions".
"Data on ethnic groups are important for putting into effect a number of federal statutes (i.e., enforcing bilingual election rules under the Voting Rights Act; monitoring and enforcing equal employment opportunities under the Civil Rights Act). Data on Ethnic Groups are also needed by local governments to run programs and meet legislative requirements (i.e., identifying segments of the population who may not be receiving medical services under the Public Health Act; evaluating whether financial institutions are meeting the credit needs of minority populations under the Community Reinvestment Act)."









In 1790, first official year of the U.S. Census, these questions were asked, four of which had racial implications:
Number of free white males aged under 16 years
Number of free white males aged 16 years and upward
Number of free white females
Number of other free people
Number of slaves
In 1800 and 1810, the age question regarding free white males was more detailed.



The 1820 census built on the questions asked in 1810 by asking age questions about the slaves who were formerly owned. Also the term "colored" entered the census nomenclature. In addition, a question stating "Number of foreigners not naturalized" was included.



For the 1830 census, a new question which stated "The number of White persons who were foreigners not naturalized" was included. This reflected the growth of Nativist movements in American society at this time - as well as combining the number and age question of both slaves and free colored individuals.



The 1850 census saw a dramatic shift in the way information about residents was collected. For the first time, free persons were listed individually instead of by head of household. There were two questionnaires: one for free inhabitants and one for slaves. The question on the free inhabitants schedule about color was a column that was to be left blank if a person was white, marked "B" if a person was black, and marked "M" if a person was mulatto. Slaves were listed by owner, and classified by gender and age, not individually, and the question about color was a column that was to be marked with a "B" if the slave was black and an "M" if mulatto.



For the 1870 census, the color/racial question was expanded to include "C" for Chinese, which was a category that included all east Asians, as well as "I" for American Indians.



For 1890, the Census Office changed the design of the population questionnaire. Residents were still listed individually, but a new questionnaire sheet was used for each family. Additionally, this was the first year that the census distinguished between different East Asian races, such as Japanese and Chinese, due to increased immigration. This census also marked the beginning of the term "race" in the questionnaires. Enumerators were instructed to write "White," "Black," "Mulatto," "Quadroon," "Octoroon," "Chinese," "Japanese," or "Indian."



For 1900, the "Color or Race" question was slightly modified, removing the term "Mulatto". Also, there was an inclusion of an "Indian Population Schedule" in which "enumerators were instructed to use a special expanded questionnaire for American Indians living on reservations or in family groups off of reservations." This expanded version included the question "Fraction of person's lineage that is white."






The 1910 census was similar to that of 1900, but it included a reinsertion of "Mulatto" and a question about the respondent's "mother tongue." "Ot" was also added to signify "other races", with space for a race to be written in. This decade's version of the Indian Population Schedule featured questions asking the individual's proportion of white, black, or American Indian lineage.



The 1920 census questionnaire was similar to 1910, but excluded a separate schedule for American Indians. "Hin", "Kor", and "Fil" were also added to the "Color or Race" question, signifying Hindu (South Asia Indian), Korean, and Filipino, respectively.



The biggest change in this year's census was in racial classification. Enumerators were instructed to no longer use the "Mulatto" classification. Instead, they were given special instructions for reporting the race of interracial persons. A person with both white and black ancestry (termed "blood") was to be recorded as "Negro," no matter the fraction of that lineage (the "one-drop rule"). A person of mixed black and American Indian ancestry was also to be recorded as "Neg" (for "Negro") unless he was considered to be "predominantly" American Indian and accepted as such within the community. A person with both White and American Indian ancestry was to be recorded as an Indian, unless his American Indian ancestry was small, and he was accepted as white within the community. In all situations in which a person had White and some other racial ancestry, he was to be reported as that other race. Persons who had minority interracial ancestry were to be reported as the race of their father.
For the first and only time, "Mexican" was listed as a race. Enumerators were instructed that all persons born in Mexico, or whose parents were born in Mexico, should be listed as Mexicans, and not under any other racial category. But, in prior censuses and in 1940, enumerators were instructed to list Mexican Americans as white, perhaps because many of them were of white background (mainly Spanish and French), many others mixed white and Native American and some of them Native American.
The Supplemental American Indian questionnaire was back, but in abbreviated form. It featured a question asking if the person was of full or mixed American Indian ancestry.



President Franklin D. Roosevelt promoted a "good neighbor" policy that sought better relations with Mexico. In 1935 a federal judge ruled that three Mexican immigrants were ineligible for citizenship because they were not white, as required by federal law. Mexico protested, and Roosevelt decided to circumvent the decision and make sure the federal government treated Hispanics as white. The State Department, the Census Bureau, the Labor Department, and other government agencies therefore made sure to uniformly classify people of Mexican descent as white. This policy encouraged the League of United Latin American Citizens in its quest to minimize discrimination by asserting their whiteness.
The 1940 census was the first to include separate population and housing questionnaires. The race category of "Mexican" was eliminated in 1940, and the population of Mexican descent was counted with the White population.
1940 census data was used for Japanese American internment. The Census Bureau's role was denied for decades, but was finally proven in 2007.



The 1950 Census questionnaire removed the word "color" from the racial question, and also removed Hindu and Korean from the race choices.



The 1960 Census re-added the word "color" to the racial question, and changed "Indian" to "American Indian", as well as added Hawaiian, Part-Hawaiian, Aleut, and Eskimo. The Other (print out race) option was removed.



This year's census included "Negro or Black", re-added Korean and the Other race option. East Indians (the term used at that time for people whose ancestry is from the Indian subcontinent) were counted as White. There was a questionnaire that was asked of only a sample of respondents. These questions were as follows:

a. Where was this person born?
b. Is this person's origin or descent...
Mexican
Puerto Rican
Cuban
Central or South American
Other Spanish
None of These

What country was the person's father born in?
What country was the person's mother born in?
a. For persons born in a foreign country- Is the person naturalized?
b. When did the person come to the United States to stay?
What language, other than English, was spoken in the person's home as a child?
Spanish
French
Italian
German
Other
None, only English



This year added several options to the race question, including Vietnamese, Indian (East), Guamanian, Samoan, and re-added Aleut. Again, the term "color" was removed from the racial question, and the following questions were asked of a sample of respondents:

In what state or foreign country was the person born?
If this person was born in a foreign country...
a. Is this person a naturalized citizen of the United States?
b. When did this person come the United States to stay?
a. Does this person speak a language other than English at home?
b. If yes, what is this language?
c. If yes, how well does this person speak English?
What is this person's ancestry?



The racial categories in this year are as they appear in the 2000 and 2010 Census. The following questions were asked of a sample of respondents for the 1990 Census:

In what U.S. State or foreign country was this person born?
Is this person a citizen of the United States?
If this person was not born in the United States, when did this person come to the United States to stay?

The 1990 Census was not designed to capture multiple racial responses, and when individuals marked the Other race option and provided a multiple write in, the response was assigned according to the race written first. "For example, a write in of "Black-White" was assigned a code of Black, a write in of "White-Black" was assigned a code of White."

In the United States, census data indicate that the number of children in interracial families grew from less than one half million in 1970 to about two million in 1990. In 1990, for interracial families with one white American partner, the other parent...was Asian American for 45 percent...



Race was asked differently in the Census 2000 in several other ways than previously. Most significantly, respondents were given the option of selecting one or more race categories to indicate racial identities. Data show that nearly seven million Americans identified as members of two or more races. Because of these changes, the Census 2000 data on race are not directly comparable with data from the 1990 census or earlier censuses. Use of caution is therefore recommended when interpreting changes in the racial composition of the US population over time.
The following definitions apply to the 2000 census only.
"White. A person having origins in any of the original peoples of Europe, the Middle East, or North Africa. It includes people who indicate their race as "White" or report entries such as Irish, German, English, Scottish, Italian, Lebanese, Near Easterner, Arab, or Polish.
"Black or African American. A person having origins in any of the Black racial groups of Africa. It includes people who indicate their race as 'Black, African Am.' or provide written entries such as Kenyan, Nigerian, or Haitian."
"American Indian and Alaska Native. A person having origins in any of the original peoples of North and South America (including Central America) and who maintain tribal affiliation or community attachment."

"Asian. A person having origins in any of the original peoples of the Far East, Southeast Asia, or the Indian subcontinent including, for example, Cambodia, China, India, Indonesia, Japan, Korea, Malaysia, Pakistan, the Philippine Islands, Thailand, and Vietnam. It includes 'Asian Indian,' 'Chinese', 'Filipino', 'Korean', 'Japanese', 'Vietnamese', and 'Other Asian'."
"Native Hawaiian and Other Pacific Islander. A person having origins in any of the original peoples of Hawaii, Guam, Samoa, or other Pacific Islands. It includes people who indicate their race as 'Native Hawaiian', 'Guamanian or Chamorro', 'Samoan', and 'Other Pacific Islander'."
"Some other race. Includes all other responses not included in the 'White', 'Black or African American', 'American Indian and Alaska Native', 'Asian' and 'Native Hawaiian and Other Pacific Islander' race categories described above. Respondents providing write-in entries such as multiracial, mixed, interracial, We-Sort, or a Hispanic/Latino group (for example, Mexican, Puerto Rican, or Cuban) in the "Some other race" category are included here."
"Two or more races. People may have chosen to provide two or more races either by checking two or more race response check boxes, by providing multiple write-in responses, or by some combination of check boxes and write-in responses."
The federal government of the United States has mandated that "in data collection and presentation, federal agencies are required to use a minimum of two ethnicities: 'Hispanic or Latino' and 'Not Hispanic or Latino'." The Census Bureau defines "Hispanic or Latino" as "a person of Cuban, Mexican, Puerto Rican, South or Central American or other Spanish culture or origin regardless of race." For discussion of the meaning and scope of the Hispanic or Latino ethnicity, see the Hispanic and Latino Americans and Racial and ethnic demographics of the United States articles.
Use of the word 'ethnicity' for Hispanics only is considerably more restricted than its conventional meaning, which covers other distinctions, some of which are covered by the "race" and "ancestry" questions. The distinct questions accommodate the possibility of Hispanic and Latino Americans' also declaring various racial identities (see also White Hispanic and Latino Americans, Asian Latinos, and Black Hispanic and Latino Americans).
In the 2000 Census, 12.5% of the US population reported "Hispanic or Latino" ethnicity and 87.5% reported "Not-Hispanic or Latino" ethnicity.






The 2010 US Census included changes designed to more clearly distinguish Hispanic ethnicity as not being a race. That included adding the sentence: "For this census, Hispanic origins are not races." Additionally, the Hispanic terms were modified from "Hispanic or Latino" to "Hispanic, Latino or Spanish origin".
Although used in the Census and the American Community Survey, "Some other race" is not an official race, and the Bureau considered eliminating it prior to the 2000 Census. As the 2010 census form did not contain the question titled "Ancestry" found in prior censuses, there were campaigns to get non-Hispanic West Indian Americans, Turkish Americans, Armenian Americans, Arab Americans and Iranian Americans to indicate their ethnic or national background through the race question, specifically the "Some other race" category.
The Interagency Committee has suggested that the concept of marking multiple boxes be extended to the Hispanic origin question, thereby freeing individuals from having to choose between their parents' ethnic heritages. In other words, a respondent could choose both "Hispanic or Latino" and "Not Hispanic or Latino".



The Census Bureau warns that data on race in 2000 Census are not directly comparable to those collected in previous censuses. Many residents of the United States consider race and ethnicity to be the same.
In the 2000 Census, respondents were tallied in each of the race groups they reported. Consequently, the total of each racial category exceeds the total population because some people reported more than one race.
According to James P. Allen and Eugene Turner from California State University, Northridge, by some calculations in the 2000 Census the largest part white biracial population is white/Native American and Alaskan Native, at 7,015,017, followed by white/black at 737,492, then white/Asian at 727,197, and finally white/Native Hawaiian and other Pacific Islander at 125,628.
The Census Bureau implemented a Census Quality Survey, gathering data from about 50,000 households to assess the reporting of race and Hispanic origin in the 2000 Census with the purpose creating a way to make comparisons between the 2000 Census with previous Census racial data.
In September 1997, during the process of revision of racial categories previously declared by OMB directive no. 15, the American Anthropological Association (AAA) recommended that OMB combine the "race" and "ethnicity" categories into one question to appear as "race/ethnicity" for the 2000 US Census. The Interagency Committee agreed, stating that "race" and "ethnicity" were not sufficiently defined and "that many respondents conceptualize 'race' and 'ethnicity' as one in the same  [sic] underscor[ing] the need to consolidate these terms into one category, using a term that is more meaningful to the American people."
The AAA also stated,

The American Anthropological Association recommends the elimination of the term "race" from OMB Directive 15 during the planning for the 2010 Census. During the past 50 years, "race" has been scientifically proven to not be a real, natural phenomenon. More specific, social categories such as "ethnicity" or "ethnic group" are more salient for scientific purposes and have fewer of the negative, racist connotations for which the concept of race was developed.
Yet the concept of race has become thoroughly and perniciously woven into the cultural and political fabric of the United States. It has become an essential element of both individual identity and government policy. Because so much harm has been based on "racial" distinctions over the years, correctives for such harm must also acknowledge the impact of "racial" consciousness among the U.S. populace, regardless of the fact that "race" has no scientific justification in human biology. Eventually, however, these classifications must be transcended and replaced by more non-racist and accurate ways of representing the diversity of the U.S. population.

The recommendations of the AAA were not adopted by the Census Bureau for the 2000 or the 2010 Censuses.



In 2001, the National Institutes of Health adopted the new language to comply with the revisions to Directive 15, as did the Equal Employment Opportunity Commission of the United States Department of Labor in 2007. See Race and ethnicity (EEO).



Historical racial and ethnic demographics of the United States
Language (United States Census)
Race (classification of humans)
Race and ethnicity in censuses
Race and ethnicity in the United States
Classification of ethnicity in the United Kingdom
Visible minority






Prewitt, Kenneth. What Is Your Race? The Census and Our Flawed Efforts to Classify Americans (Princeton University Press; 2013) 271 pages; argues for dropping the race question from the census.