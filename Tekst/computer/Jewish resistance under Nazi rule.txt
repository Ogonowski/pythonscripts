Jewish resistance under Nazi rule refers to various forms of resistance conducted against German occupation regimes in Europe by Jews during World War II. The term is particularly connected with the Holocaust and includes a multitude of different social responses by those oppressed, as well as both passive and armed resistance conducted by Jews themselves.
Due to the careful organization and military strength of Nazi Germany and its supporters, as well as the hostility of other sections of the civilian population, few Jews were able to effectively resist the killings militarily. Nevertheless, there are many cases of attempts at resistance in one form or another including over a hundred armed Jewish uprisings. Historiographically, the study of Jewish resistance to German rule is considered an important aspect of the study of the Holocaust.



In his book The Holocaust: The Jewish Tragedy, Martin Gilbert describes the types of resistance:

"In every ghetto, in every deportation train, in every labor camp, even in the death camps, the will to resist was strong, and took many forms. Fighting with the few weapons that would be found, individual acts of defiance and protest, the courage of obtaining food and water under the threat of death, the superiority of refusing to allow the Germans their final wish to gloat over panic and despair.
Even passivity was a form of resistance. To die with dignity was a form of resistance. To resist the demoralizing, brutalizing force of evil, to refuse to be reduced to the level of animals, to live through the torment, to outlive the tormentors, these too were acts of resistance. Merely to give a witness of these events in testimony was, in the end, a contribution to victory. Simply to survive was a victory of the human spirit."

This view is supported by Yehuda Bauer, who wrote that resistance to the Nazis comprised not only physical opposition, but any activity that gave the Jewish people dignity and humanity despite the humiliating and inhumane conditions. Bauer disputes the popular view that most Jews went to their deaths passively. He argues that, given the conditions in which the Jews of Eastern Europe had to live under and endure, what is surprising is not how little resistance there was, but rather how much resistance was present.




In 1940, the Warsaw ghetto was cut off from its access to Polish underground newspapers, and the only newspaper allowed to be imported into the confines of the ghetto was the General Government propaganda organ Gazeta  ydowska. As a result, from roughly May 1940 to October 1941, the Jews of the ghetto published their own underground newspapers, offering hopeful news about the war and prospects for the future. The most prominent of these were published by the Jewish Socialist party and the Zionist Labor Movement. These papers lamented the carnage of war, but for the most part did not encourage armed resistance.
Between April and May 1943, Jewish men and women of the Warsaw Ghetto took up arms and rebelled against the Nazis after it became clear that the Germans were deporting remaining Ghetto inhabitants to the Treblinka extermination camp. Warsaw Jews of the Jewish Combat Organization and the Jewish Military Union fought the Germans with a handful of small arms and Molotov cocktails, as Polish resistance attacked from the outside in support. After fierce fighting, vastly superior German forces pacified the Warsaw Ghetto and either murdered or deported all of the remaining inhabitants to the Nazi killing centers. The Germans claimed that they lost 18 dead and 85 wounded, though this figure has been disputed, with resistance leader Marek Edelman estimating 300 German casualties. Some 13,000 Jews were killed, and 56,885 were deported to concentration camps.
There were many other major and minor ghetto uprisings, however most were not successful. Some of the ghetto uprisings include the Bia ystok Ghetto Uprising and the Cz stochowa Ghetto Uprising.




There were also major resistance efforts in three of the extermination camps.
In August 1943, an uprising took place at the Treblinka extermination camp. The participants obtained guns and grenades after two young men used forged keys and snuck into the weapons store. The weapons were then distributed around the camp in garbage bins. However, during the distribution of arms, a Nazi guard stopped a prisoner and found contraband money on him. Fearing that the prisoner would be tortured and give away the plan, the organizers decided to launch the revolt ahead of schedule without completing the distribution of weapons, and set off a single grenade the agreed-upon signal for the uprising. The prisoners then attacked the Nazi guards with guns and grenades. Several German and Ukrainian guards were killed, a fuel tank was set on fire, barracks and warehouses were burned, military vehicles were disabled, and grenades were thrown at the SS headquarters. The guards replied with machine-gun fire, and 1,500 inmates were killed yet 70 inmates escaped to freedom. The guards chased those who had escaped on horseback and in cars, but some of those who escaped were armed, and returned the guards' fire. Gassing operations at the camp were interrupted for a month.
In October 1943, an uprising took place at Sobib r extermination camp, led by Polish-Jewish prisoner Leon Feldhendler and Soviet-Jewish POW Alexander Pechersky. The inmates covertly killed 11 German SS officers, including the deputy commander, and a number of Ukrainian guards. Although the plan was to kill all of SS members and walk out of the main gate of the camp, the guards discovered the killings and opened fire. The inmates then had to then run for freedom under fire, with roughly 300 of the 600 inmates in the camp escaping alive. All but 50 70 of the inmates were killed in the surrounding minefields or recaptured and executed by the Germans. However, the escape forced the Nazis to close the camp, saving countless lives.
On October 7, 1944, the Jewish Sonderkommandos (inmates kept separate from the main camp and put to work in the gas chambers and crematoria) at Auschwitz staged an uprising. Female inmates had smuggled in explosives from a weapons factory, and Crematorium IV was partly destroyed by an explosion. At this stage they were joined by the Birkenau One Kommando, which also overpowered their guards and broke out of the compound. The inmates then attempted a mass escape, but were stopped by heavy fire. Three SS guards were killed in the uprising, including one who was pushed alive into an oven. Almost all of the 250 escapees were killed. There were also international plans for a general uprising in Auschwitz, coordinated with an Allied air raid and a Polish resistance attack from the outside.




There were a number of Jewish partisan groups operating in many countries, especially Poland. The most notable of the groups is the Bielski partisans, whom the movie Defiance portrays, and the Parczew partisans in the forests near Lublin, Poland. Hundreds of Jews escaped the ghettoes and joined the Partisan resistance groups.



On 4 February 1936, the leader of the NSDAP (Nazi) party in Switzerland Wilhelm Gustloff was assassinated by David Frankfurter, a Croatian Jew.
On 9 November 1938, Nazi diplomat Ernst vom Rath was assassinated in Paris by a Jewish youth, Herschel Grynszpan.







Belgian resistance to the treatment of Jews crystallised between August September 1942, following the passing of legislation regarding wearing yellow badges and the start of the deportations. When deportations began, Jewish partisans destroyed records of Jews compiled by the AJB. The first organization specifically devoted to hiding Jews, the Comit  de D fense des Juifs (CDJ-JVD), was formed in the summer of 1942. The CDJ, a left-wing organization, may have saved up to 4,000 children and 10,000 adults by finding them safe hiding places. It produced two Yiddish language underground newspapers, Unzer Wort ("Our Word", with a Labour-Zionist stance) and Unzer Kamf ("Our Fight", with a Communist one). The CDJ was only one of dozens of organised resistance groups that provided support to hidden Jews. Other groups and individual resistance members were responsible for finding hiding places and providing food and forged papers. Many Jews in hiding went on to join organised resistance groups. Groups from left wing backgrounds, like the Front de l'Ind pendance (FI-OF), were particularly popular with Belgian Jews. The Communist-inspired Partisans Arm s (PA) had a particularly large Jewish section in Brussels.
The resistance was responsible for the assassination of Robert Holzinger, the head of the deportation program, in 1942. Holzinger, an active collaborator, was an Austrian Jew selected by the Germans for the role. The assassination led to a change in leadership of the AJB. Five Jewish leaders, including the head of the AJB, were arrested and interned in Breendonk, but were released after public outcry. A sixth was deported directly to Auschwitz.
The Belgian resistance was unusually well informed on the fate of the deported Jews. In August 1942 (two months after the start of the Belgian deportations), the underground newspaper De Vrijschutter reported that "They [the deported Jews] are being killed in groups by gas, and others are killed by salvos of machinegun fire."
In early 1943, the Front de l'Ind pendance sent Victor Martin, an academic economist at the Catholic University of Louvain, to gather information on the fate of deported Belgian Jews using the cover of his research post at the University of Cologne. Martin visited Auschwitz and witnessed the crematoria. Arrested by the Germans, he escaped, and was able to report his findings to the CDJ in May 1943.



Despite amounting to only 1% of the French population, Jews comprised about 15-20% of the French Resistance. Some of the Jewish resistance members were Hungarian-Jewish refugees.
French Jews set up their own armed resistance movement: the Arm e Juive (Jewish Army), a Zionist, which at its height, numbered some 2,000 fighters. Operating throughout France, it smuggled hundreds of Jews to Spain and Switzerland, launched attacks against occupying German forces, and targeted Nazi informants and Gestapo agents. Armee Juive participated in the general French uprising of August 1944, fighting in Paris, Lyon, and Toulouse.



Jewish resistance within Germany itself during the Nazi era took a variety of forms, from sabotage and disruptions to providing intelligence to Allied forces, distributing anti-Nazi propaganda, as well as participating in attempts to assist Jewish emigration out of Nazi-controlled territories. It has been argued that, for Jews during the Holocaust, given the intent of the Nazi regime to exterminate Jews, survival itself constituted an act considered a form of resistance. Jewish participation in the German resistance was largely confined to the underground activities of left-wing Zionist groups such as Werkleute, Hashomer Hatzair and Habonim, and the German Social Democrats, Communists, and independent left-wing groups such as New Beginning. Much of the non-left wing and non-Jewish opposition to Hitler in Germany (i.e., conservative and religious forces), although often opposed to the Nazi plans for extermination of German and European Jewry, in many instances itself harbored anti-Jewish sentiments.
A celebrated case involved the arrest and execution of Helmut Hirsch, a Jewish architectural student originally from Stuttgart, in connection with a plot to bomb Nazi Party headquarters in Nuremberg. Hirsch became involved in the Black Front, a breakaway faction from the Nazi Party led by Otto Strasser. After being captured by the Gestapo in December 1936, Hirsch confessed to planning to murder Julius Streicher, a leading Nazi official and editor of the virulently anti-Semitic Der St rmer newspaper, on behalf of Strasser and the Black Front. Hirsch was sentenced to death on March 8, 1937, and on June 4 was beheaded with an axe.
Perhaps the most significant Jewish resistance group within Germany for which records survive was the Berlin-based Baum Group (Baum-Gruppe), which was active from 1937 to 1942. Largely young Jewish women and men, the group disseminated anti-Nazi leaflets, and organized semi-public demonstrations. Its most notable action was the bombing of an anti-Soviet exhibit organized by Joseph Goebbels in Berlin's Lustgarten. The action resulted in mass arrests, executions, and reprisals against German Jews. Because of the reprisals it provoked, the bombing led to debate within opposition circles similar to those that took place elsewhere where the Jewish resistance was active taking action and risking murderous reprisals vs. being non-confrontational with the hopes of maximizing survival.



In the Netherlands, the only pre-war group that immediately started resistance against the German occupation was the communist party. During the first two war years, it was by far the biggest resistance organization, much bigger than all other organizations put together. A major act of resistance was the organisation of the February strike in 1941, in protest against anti-Jewish measures. In this resistance, many Jews participated. About 1,000 Dutch Jews took part in resisting the Germans, and of those, 500 perished in doing so. In 1988, a monument to their memory was unveiled by the then mayor of Amsterdam, Ed van Thijn.
Among the first Jewish resisters was the German fugitive Ernst Cahn, owner of an ice cream parlor. Together with his partner, Kohn, he had an ammonia gas cylinder installed in the parlor to stave off attacks from the militant arm of the fascist NSB, the so-called "Weerafdeling"("WA"). One day in February 1941 the German police forced their entrance into the parlor, and were gassed. Later, Cahn was caught and on March 3, 1941 he became the first civilian to be executed by a Nazi firing squad in the Netherlands.
Benny Bluhm, a boxer, organized Jewish fighting parties consisting of members of his boxing school to resist attacks. One of these brawls led to the death of a WA-member, H. Koot, and subsequently the Germans ordered the first Dutch razzia (police raid) of Jews as a reprisal. That in turn led to the Februaristaking, the February Strike. Bluhm's group was the only Jewish group resisting the Germans in the Netherlands and the first active group of resistance fighters in the Netherlands. Bluhm survived the war, and strove for a monument for the Jewish resisters that came about two years after his death in 1986.
Numerous Jews participated in resisting the Germans. The Jewish director of the assembly center in the "Hollandsche Schouwburg", a former theatre, Walter Susskind, was instrumental in smuggling children out of his centre. He was aided by his assistant Jacques van de Kar and the director of the nearby cr che, Mrs Pimentel.
Within the underground communist party, a militant group was formed: de Nederlandse Volksmilitie (NVM, Dutch Peoples Militia). The leader was the Jewish Sally (Samuel) Dormits, who had military experience from guerrilla warfare in Brazil and participation in the Spanish Civil War. This organisation was formed in The Hague but became mainly located in Rotterdam. It counted about 200 (mainly Jewish) participants. They made several bomb attacks on German troop trains and arson attacks on cinemas, which were forbidden for Jews. Dormits was caught after stealing a handbag off a woman in order to obtain an identification card for his Jewish girlfriend, who also participated in the resistance. Dormits committed suicide in the police station by shooting himself through the head. From a cash ticket of a shop the police found the hiding place of Dormits and discovered bombs, arson material, illegal papers, reports about resistance actions and a list of participants. The Gestapo was warned immediately and that day two hundred people were arrested, followed by many more connected people in Rotterdam, The Hague and Amsterdam. The Dutch police participated in torturing the Jewish communists. After a trial more than 20 were shot to death; most of the others died in concentration camps or were gassed in Auschwitz. Only a few survived. The war grave of Dormits has recently been destroyed by municipal authorities in Rotterdam.




The British Army trained 37 Jewish volunteers from Mandate Palestine to parachute into Europe in an attempt to organize resistance. The most famous member of this group was Hannah Szenes.) She was parachuted into Yugoslavia to assist in the rescue of Hungarian Jews about to be deported to the German death camp at Auschwitz. Szenes was arrested at the Hungarian border, then imprisoned and tortured, but refused to reveal details of her mission. She was eventually tried and executed by firing squad. She is regarded as a national heroine in Israel.
The British government formed in July 1944 the Jewish Brigade, which comprised more than 5,000 Jewish volunteers from Palestine, organized into three infantry regiments, an artillery regiment, and supporting units. They were attached to the British Eight Army in Italy from November 1944, taking part to the spring 1945 "final offensive" on that front. After the end of the war in Europe the Brigade was moved to Belgium and the Netherlands in July 1945. As well as participating in combat operations against German forces, the brigade assisted and protected Holocaust survivors.
The Special Interrogation Group was a British Army commando unit comprising German-speaking Jewish volunteers from Palestine. It carried out commando and sabotage raids behind Axis lines during the Western Desert Campaign, and gathered military intelligence by stopping and questioning German transports while dressed as German military police. They also assisted other British forces. Following the disastrous failure of Operation Agreement, a series of ground and amphibious operations carried out by British, Rhodesian and New Zealand forces on German and Italian-held Tobruk in September 1942, the survivors were transferred to the Royal Pioneer Corps.




Mordechaj Anielewicz, leader of the Jewish Combat Organization during the Warsaw Ghetto Uprising, killed in action in 1943
Dawid Apfelbaum, a commander of the Jewish Military Union during the Warsaw Ghetto Uprising, killed in action during heavy fighting at the beginning of the uprising
Pawe  Frenkiel, a Polish Jewish youth leader in Warsaw and a senior commander of the Jewish Military Union, killed in action defending the JMU headquarters
Yitzhak Arad, a former Soviet partisan
Herbert Baum, a Jewish member of the German resistance against National Socialism, tortured to death by the Gestapo
Bielski partisans, an organization of Jewish partisans who rescued Jews from extermination in western Belarus
Frank Blaichman, a Holocaust survivor who was a Polish-Jewish leader of Jewish resistance
Thomas Blatt, a survivor from the uprising and escape from the Sobib r extermination camp in October 1943
Masha Bruskina, a 17-year-old Jewish member of the Minsk Resistance, executed by the Nazis
Eugenio Cal , an Italian partisan, executed by the Nazis
Franco Cesana, an Italian Jew who joined a partisan group, killed by the Nazis at age 13
Icchak Cukierman, one of the leaders of the Warsaw Ghetto Uprising 1943 and fighter in the 1944 Warsaw Uprising
Szymon Datner, he helped smuggle several people out of Bia ystok Ghetto in 1943
Marek Edelman, a leader of the Warsaw Ghetto Uprising
Selma Engel-Wijnberg, the only known Dutch prisoner of Sobib r extermination camp who escaped and survived
Leon Feldhendler, a Polish-Jewish resistance fighter who organized the 1943 prisoner uprising at the Sobib r extermination camp
Dov Freiberg, a participant in the Sobib r prisoners' revolt who joined Joseph Serchuk's partisan unit
Munyo Gruber, a member of the Parczew partisans who fought the Germans while attempting to save as many Jewish lives as possible
Abba Kovner, a member of the United Partisan Organization, one of the first armed underground organizations in the Jewish ghettos under Nazi occupation
Zivia Lubetkin, one of the leaders of the Jewish underground in Nazi-occupied Warsaw and the only woman on the High Command of the resistance group  ydowska Organizacja Bojowa
Dov Lopatyn, leader of one of the first ghetto uprisings of the war and member of a partisan unit, killed in action
Vladka Meed, a member of Jewish resistance in Poland who smuggled dynamite into the Warsaw Ghetto and also helped children escape out of the Ghetto
Parczew partisans, fighters in irregular military groups participating in the Jewish resistance movement
Alexander Pechersky, one of the organizers, and the leader of the most successful uprising and mass-escape of Jews from a Nazi extermination camp during World War II; which occurred at the Sobibor extermination camp in 1943
Mo e Pijade, one of the leaders of the uprising in Montenegro against the Italian occupation forces in Axis-occupied Yugoslavia
Haviva Reik, one of 32 or 33 Palestinian Jewish parachutists sent by the Jewish Agency and Britain's Special Operations Executive (SOE) on military missions in Nazi-occupied Europe; she was captured and executed
Joseph Serchuk, commander of the Jewish partisan unit in the Lublin area in Poland
Hannah Szenes, one of 37 Jews from Mandatory Palestine parachuted by the British Army into Yugoslavia, she was captured, tortured, and executed by the Nazis
Lelio Vittorio Valobra, leader of DELASEM, which helped Jewish refugees to escape the Holocaust
Dawid Wdowi ski, founder of the  ZW group in the Warsaw Ghetto who served as its political leader
Yitzhak Wittenberg, a Jewish resistance fighter in Vilnius; after he was captured by the Gestapo, he committed suicide in his jail cell
Shalom Yoran, a Jewish resistance fighter who fought back the Germans and their collaborators
Simcha Zorin, a Jewish Soviet partisan commander in Minsk of a group that consisted of 800 Jewish fighters







In the aftermath of the war, Holocaust survivors led by former members of Jewish resistance groups banded together. Calling themselves Nokmim (Hebrew for "avengers"), they tracked down and executed former Nazis who took part in the Holocaust. They killed an unknown number of Nazis, and their efforts are believed to have progressed into the 1950s. The Nazis were often kidnapped and killed by hanging or strangulation, others were killed by hit-and-run attacks, and a former high-ranking Gestapo officer died when kerosene was injected into his bloodstream while he was in hospital awaiting an operation. It is possible that some of the most successful Nokmim were veterans of the Jewish Brigade, who had access to military intelligence, transport, and the right to freely travel across Europe.
Nokmim also travelled to places such as Latin America, Canada, and Spain to track down and kill Nazis who had settled there. In one instance, they are believed to have confronted Aleksander Laak, responsible for killing 8,500 Jews at J gala concentration camp, at his suburban Winnipeg home, and after telling him that they intended to kill him, allowed him to commit suicide.
In 1946, the Nokmim carried out a mass poisoning attack against former SS members imprisoned at Stalag 13, lacing their bread rations with arsenic at the bakery which supplied it. Approximately 1,200 prisoners fell ill, but no deaths were reported. The U.S. Army mustered its medical resources to treat the poisoned prisoners. Nokmin response ranged from viewing this mass assignation attempt as a failure to claiming the Allies covered up the fact that there had been deaths.




Anti-fascism
History of the Jews during World War II
Ilse Stanley
Inglourious Basterds
Defiance (2008 film)
Uprising (2001 film)
Resistance during World War II






Ginsberg, Benjamin (2013). How the Jews Defeated Hitler: Exploding the Myth of Jewish Passivity in the Face of Nazism. Rowman & Littlefield Publishers. ISBN 1-4422-2238-7. 



Jewish Armed Resistance and Rebellions on the Yad Vashem website
 Jewish Resistance: A Working Bibliography.  The Miles Lerman Center for the Study of Jewish Resistance. Center for Advanced Holocaust Studies United States Holocaust Memorial Museum. Washington, DC. PDF version available here [1]
Jewish Resistance During the Holocaust from Holocaust Survivors and Remembrance Project
About the Holocaust
Jewish Partisan Educational Foundation
Interviews from the Underground: Eyewitness accounts of Russia's Jewish resistance during World War II documentary film and website (www.jewishpartisans.net)
United States Holocaust Memorial Museum - Armed Jewish Resistance: Partisans