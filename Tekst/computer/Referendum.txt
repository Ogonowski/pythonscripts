A referendum (in some countries synonymous with plebiscite   or a vote on a ballot question) is a direct vote in which an entire electorate is asked to vote on a particular proposal. This may result in the adoption of a new law.
Some definitions of plebiscite suggest that it is a type of vote to change the constitution or government of a country. Others define it as the opposite. Australia defines 'referendum' as a vote to change the constitution and 'plebiscite' as a vote that does not affect the constitution. In contrast, Ireland has only ever held one 'plebiscite' which was the vote to adopt its constitution and each other similar vote has been styled a 'referendum'.



Referendum is the gerund of the Latin verb refero, and has the meaning "bringing back" (i.e. bringing the question back to the people). The term plebiscite has a generally similar meaning in modern usage, and comes from the Latin plebiscita, which originally meant a decree of the Concilium Plebis (Plebeian Council), the popular assembly of the Roman Republic. Today, a "referendum" can also often be referred to as a "plebiscite", but in some countries the two terms are used differently to refer to votes with differing types of legal consequences.
Referendums and referenda are both commonly used as plurals of referendum. However, the use of referenda is deprecated by the Oxford English Dictionary, which advises that:

Referendums is logically preferable as a plural form meaning ballots on one issue (as a Latin gerund, referendum has no plural). The Latin plural gerundive referenda, meaning things to be referred, necessarily connotes a plurality of issues.

The name and use of the "referendum" is thought to have originated in the Swiss canton of Gra bunden as early as the 16th century.



Mechanisms of direct democracy are defined as a set of procedures allowing citizens to make political decisions directly through a vote, without the involvement of a parliament or a government. These mechanisms can be grouped conveniently according to who started the call for a vote. The vote can be prescribed by a constitution or a law and, thus, be automatic; it can be triggered without the collection of signatures by the authorities in power (be it a parliament, a government, or often a president); or it can depend on a collection of signatures in order to, on the one hand, block decisions post factum or, on the other hand, introduce legal provisions independent of previous legislative action.



From a political philosophical perspective, referendums are an expression of direct democracy. However, in the modern world, most referendums need to be understood within the context of representative democracy. As such, they tend to be used quite selectively, covering issues such as changes in voting systems, where currently elected officials may not have the legitimacy or inclination to implement such changes.




Since the end of the 18th century, more than 500 national referendums have been organised in the world; more than 300 of these were held in Switzerland. Australia ranked second with dozens of referendums.



A referendum usually offers the electorate a choice of accepting or rejecting a proposal, but this is not necessarily the case. In Switzerland, for example, multiple choice referendums are common. Two multiple choice referendums held in Sweden, in 1957 and 1980, offered voters three options; in 1977 a referendum held in Australia to determine a new national anthem was held in which voters had four choices; and in 1992, New Zealand held a five-option referendum on their electoral system.
A multiple choice referendum poses the question of how the result is to be determined if no single option receives the support of an absolute majority (more than half) of voters - a proviso for some; others regard a non-majoritarian methodology like the Borda count as more inclusive and more accurate. This question can be resolved by applying voting systems designed for single winner elections to a multiple-choice referendum.
Swiss referendums get around this problem by offering a separate vote on each of the multiple options as well as an additional decision about which of the multiple options should be preferred. In the Swedish case, in both referendums the 'winning' option was chosen by the Single Member Plurality ("first past the post") system. In other words, the winning option was deemed to be that supported by a plurality, rather than an absolute majority, of voters. In the 1977 Australian referendum the winner was chosen by the system of preferential instant-runoff voting. The 1992 New Zealand poll was counted under the two-round system, as were polls in Newfoundland (1949) and Guam (1982), for example.
Although California does not have deliberate multiple-choice referendums in the Swiss or Swedish sense (in which only one of several counter-propositions can be victorious, and the losing proposals are wholly null and void), it does have so many yes-or-no referendums at each Election Day that the State's Constitution provides a method for resolving inadvertent conflicts when two or more inconsistent propositions are passed on the same day. This is a de facto form of Approval Voting - i.e., the proposition with the most "yes" votes prevails over the others to the extent of any conflict.
Other voting systems which could be used in multi-option referendums are the Borda and Condorcet rules.



Although some advocates of direct democracy would have the referendum become the dominant institution of government, in practice and in principle, in almost all cases, the referendum exists merely as a complement to the system of representative democracy, in which most major decisions are made by an elected legislature. An often-cited exception is the Swiss canton of Glarus, in which meetings are held on the village lawn to decide on matters of public concern. In most jurisdictions that practice them, referendums are relatively rare occurrences and are restricted to important issues. The most frequent type of direct popular participation is the referendum on constitutional matters.
Advocates of the referendum argue that certain decisions are best taken out of the hands of representatives and determined directly by the people. Some adopt a strict definition of democracy, saying elected parliaments are a necessary expedient to make governance possible in the large, modern nation-state, though direct democracy is nonetheless preferable and the referendum takes precedence over Parliamentary decisions.
Other advocates insist that the principle of popular sovereignty demands that certain fundamental questions, such as the adoption or amendment of a constitution, the secession of a state or the altering of national boundaries, be determined with the directly expressed consent of the people.
Advocates of representative democracy say referendums are used by politicians to avoid making difficult or controversial decisions.




Critics of the referendum argue that voters in a referendum are more likely driven by transient whims than by careful deliberation, or that they are not sufficiently informed to make decisions on complicated or technical issues. Also, voters might be swayed by strong personalities, propaganda and expensive advertising campaigns. James Madison argued that direct democracy is the "tyranny of the majority".
Some opposition to the referendum has arisen from its use by dictators such as Adolf Hitler and Benito Mussolini who, it is argued, used the plebiscite to disguise oppressive policies as populism. Hitler's use of plebiscites is argued as the reason why, since World War II, there has been no provision in Germany for the holding of referendums at the federal level.



British politician Chris Patten summarized many of the arguments used by those who oppose the referendum in an interview in 2003 when discussing the possibility of a referendum in the United Kingdom on the European Union Constitution:



Some critics of the referendum attack the use of closed questions. A difficulty which can plague a referendum of two issues or more is called the separability problem. If one issue is in fact, or in perception, related to another on the ballot, the imposed simultaneous voting of first preference on each issue can result in an outcome that is displeasing to most.



Several commentators have noted that the use of citizens' initiatives to amend constitutions has so tied the government to a mishmash of popular demands as to render the government unworkable. The Economist has made this point about the US State of California, which has passed so many referendums restricting the ability of the state government to tax the people and pass the budget that the state has become effectively ungovernable. Calls for an entirely new Californian constitution have been made.



Emerson, P J. Defining Democracy puts both two-option and multi-option referendums into their historical context and suggests which are the more accurate measures of "the will of the people". The de Borda Institute is at http://www.deborda.org
Emerson Peter, Designing an All-Inclusive Democracy (Springer-Verlag, 2007), describes the Modified Borda Count (MBC), as well as the Quota Borda System (QBS) and the matrix vote.
The Federal Authorities of the Swiss Confederation, statistics (German). http://www.bfs.admin.ch/bfs/portal/de/index/themen/17/03/blank/key/stimmbeteiligung.html



Referendums by country
History of direct democracy in the United States
List of politics-related topics
Political science
Direct democracy
Referendums related to the European Union
Initiative
United Nations in Kashmir
Independence referendum
Popular referendum
Right to petition
War referendum



Arizona Proposition 204, 2006
Australian referendum, 1967 (Aboriginals)
Good Friday Agreement (1998)
Bolivian gas referendum, 2004
Carinthian Plebiscite (1920)
Cypriot Annan Plan referendum, 2004
Edinburgh congestion charge (2005)
Kenyan constitutional referendum, 2005
Montenegrin independence referendum, 1992
Montenegrin independence referendum, 2006
Norwegian prohibition referendum, 1919
Norwegian continued prohibition referendum, 1926
Norwegian European Communities membership referendum, 1972
Norwegian European Union membership referendum, 1994
Panama Canal expansion referendum, 2006
People's Republic of China referendums
Hongkong civil referendums,6.20-29, 2014

Puerto Rico status referendums (1967, 1993, 1998)
Republic of China referendums
Serbian constitutional referendum, 2006
South African referendum, 1992
Tokelauan self-determination referendum, 2006
Venezuelan recall referendum, 2004
Referendums in Canada
Alberta liquor plebiscite, 1957
British Columbia aboriginal treaty referendum, 2002
British Columbia electoral reform referendum, 2005
British Columbia electoral reform referendum, 2009
Charlottetown Accord
List of Northwest Territories plebiscites
Newfoundland referendums, 1948
Northwest Territories division plebiscite, 1982
Nunavut capital plebiscite, 1995
Ontario electoral reform referendum, 2007
Ontario prohibition plebiscite, 1894
Ontario prohibition referendum, 1902
Ontario prohibition referendum, 1919
Ontario prohibition referendum, 1921
Ontario prohibition referendum, 1924
Prince Edward Island electoral reform referendum, 2005
Quebec referendum, 1980
Quebec referendum, 1995
Saint John, New Brunswick ward plebiscite, 2007

Referendums in the United Kingdom
United Kingdom European Communities membership referendum, 1975
United Kingdom European Constitution referendum (proposed)
United Kingdom Alternative Vote referendum, 2011
Northern England devolution referendums, 2004
Northern Ireland Belfast Agreement referendum, 1998
Northern Ireland sovereignty referendum, 1973
Scottish devolution referendum, 1979
Scottish devolution referendum, 1997
Scottish independence referendum, 2014
Welsh devolution referendum, 1979
Welsh devolution referendum, 1997
Welsh devolution referendum, 2011
Edinburgh congestion charge
Greater London Authority referendum, 1998

Referendums related to European Union accession:
Croatian European Union membership referendum, 2012
Danish European Communities membership referendum, 1972
Norwegian European Communities membership referendum, 1972
Norwegian European Union membership referendum, 1994
Polish European Union membership referendum, 2003






Irish Examiner article of 2015