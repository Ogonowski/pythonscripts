The Federal Chancellor (German: Bundeskanzler(in); French: Chancelier(-i re) f d ral(e); Italian: Cancelliere(-a) della Confederazione; Romansh: Chancelier(a) federal(a)) is the head of the Federal Chancellery of Switzerland, which acts as the general staff of the seven-member Federal Council. The Swiss Chancellor is not a member of the government, and his or her position is not comparable to that of the Chancellor of Germany or Austria.
The Federal Chancellor is elected for a four-year term by the Federal Assembly, assembled together as the United Federal Assembly, at the same time as they elect the Federal Council. The current Chancellor, Corina Casanova, a member of the Christian Democratic People's Party from Graub nden, was elected on 12 December 2007 to begin her term on 1 January 2008.



Although the chancellor only has a technocratic role, the position is a political appointment made by a vote of both houses of the Federal Assembly, sitting together as the United Federal Assembly, for a term of four years.
One or two Vice-Chancellors (before 1852 this position was called the State Secretary of the Confederation) are also appointed; in contrast to the chancellor, their appointment is made directly by the Federal Council.



The chancellor attends meetings of the Federal Council (although he or she does not have a vote), and prepares the Federal Council's reports to the Federal Assembly on its policy and activities. Nonetheless, his or her influential position is often referred to as that of an 'eighth Federal Councillor'. The chancellery is also responsible for the publication of all federal laws.





