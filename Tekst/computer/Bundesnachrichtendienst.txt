The Federal Intelligence Service (German: Bundesnachrichtendienst (German pronunciation: [ b nd s na tn di nst], BND; CIA code name CASCOPE)) is the foreign intelligence agency of Germany, directly subordinated to the Chancellor's Office. Its headquarters are in Pullach near Munich, and Berlin (planned to be centralised in Berlin by 2016, with about 4,000 people). The BND has 300 locations in Germany and foreign countries. In 2005, the BND employed around 6,050 people, 10% of them Bundeswehr soldiers; those are officially employed by the Amt f r Milit rkunde (Office for Military Sciences). The annual budget of the BND for 2015 is  615,577,000.
The BND acts as an early warning system to alert the German government to threats to German interests from abroad. It depends heavily on wiretapping and electronic surveillance of international communications. It collects and evaluates information on a variety of areas such as international non-state terrorism, weapons of mass destruction proliferation and illegal transfer of technology, organized crime, weapons and drug trafficking, money laundering, illegal migration and information warfare. As Germany s only overseas intelligence service, the BND gathers both military and civil intelligence. While the Strategic Reconnaissance Command (KSA) of the Bundeswehr also fulfills this mission, it is not an intelligence service. There is close cooperation between the BND and the KSA.
The domestic secret service counterparts of the BND are the Federal Office for the Protection of the Constitution (Bundesamt f r Verfassungsschutz, or BfV) and 16 counterparts at the state level Landes mter f r Verfassungsschutz (State Offices for the Protection of the Constitution); there is also a separate military intelligence organisation, the Milit rischer Abschirmdienst (MAD, Military Counterintelligence Service).
The BND is a successor to the Gehlen Organization. The most central figure in its history was Reinhard Gehlen, its first president.




The predecessor of the BND was the German eastern military intelligence agency during World War II, the Abteilung Fremde Heere Ost or FHO Section in the General Staff, led by Wehrmacht Major General Reinhard Gehlen. Its main purpose was to collect information on the Red Army. After the war Gehlen worked with the U.S. occupation forces in West Germany. In 1946 he set up an intelligence agency informally known as the Gehlen Organization or simply "The Org" and recruited some of his former co-workers. Many had been operatives of Admiral Wilhelm Canaris' wartime Abwehr (counter-intelligence) organization, but Gehlen also recruited people from the former Sicherheitsdienst, SS and Gestapo, after their release by the Allies. The latter recruits were later controversial because the SS and its associated groups were notoriously the perpetrators of many Nazi atrocities during the war. The organization worked at first almost exclusively for the CIA, which contributed funding, equipment, cars, gasoline and other materials. On 1 April 1956 the Bundesnachrichtendienst was created from the Gehlen Organization, and transferred to the West German government, with the recruits of the former Sicherheitsdienst, SS and Gestapo. Reinhard Gehlen became President of the BND and remained its head until 1968.






In the first years of oversight by the State Secretary in the federal chancellery of Konrad Adenauer of the operation in Pullach, the BND continued the ways of its forebear, the Gehlen Organization. For the average West German citizen, concerned at the time mainly with improving his existence, espionage was a questionable business, better left for shady characters, but not their descendants.
The BND racked up its initial East-West cold war successes by concentrating on East Germany. The BND's reach encompassed the highest political and military levels of the GDR regime. They knew the carrying capacity of every bridge, the bed count of every hospital, the length of every airfield, the width and level of maintenance of the roads that Soviet armor and infantry divisions would have to traverse in a potential attack on the West. Almost every sphere of eastern life was known to the BND.
Unsung analysts at Pullach, with their contacts in the East, figuratively functioned as flies on the wall in ministries and military conferences. When the Soviet KGB suspected an East German army intelligence officer, a Lieutenant Colonel and BND agent, of spying, the Soviets investigated and shadowed him. The BND was positioned and able to inject forged reports implying that the loose spy was actually the KGB investigator, who was then arrested by the Soviets and shipped off to Moscow. Not knowing how long the caper would stay under wraps, the real spy was told to be ready for recall; he made his move to the West at the appropriate time.
The East German regime, however, fought back. With still unhindered flight to the west a possibility, infiltration started on a grand scale and a reversal of sorts took hold. During the early 1960s as many as 90% of the BND's lower-level informants in East Germany worked as double agents for the East German security service, later known as Stasi. Several informants in East Berlin reported in June and July 1961 of street closures, clearing of fields, accumulation of building materials and police and army deployments in specific parts of the eastern sector, as well as other measures that BND determined could lead to a division of the city. However, the agency was reluctant to report communist initiatives and had no knowledge of the scope and timing because of conflicting inputs. The erection of the Berlin Wall on 13 August 1961 thus came as a surprise, and the BND's performance in the political field was thereafter often wrong and remained spotty and unimpressive.
"This negative view of BND was certainly not justified during   [1967 and] 1968." The BND's military work "had been outstanding", and in certain sectors of the intelligence field the BND still showed brilliance: in Latin America and in the Middle East it was regarded as the best-informed secret service.
The BND offered a fair and reliable amount of intelligence on Soviet and Soviet-bloc forces in Eastern Europe, regarding the elaboration of a NATO warning system against any Soviet operations against NATO territory, in close cooperation with the Bundeswehr (German Armed Forces). It also detected the deployment of Soviet missiles to Cuba in 1962, and subsequently warned the United States, resulting in the Cuban Missile Crisis.
One high point of BND intelligence work culminated in its early June 1967 forecast   almost to the hour   of the outbreak of the Six-Day War in the Middle East on 5 June 1967.
According to declassified transcripts of a United States National Security Council meeting on 2 June 1967, CIA Director Richard Helms interrupted Secretary of State Dean Rusk with "reliable information"   contrary to Rusk's presentation   that the Israelis would attack on a certain day and time. Rusk shot back: "That is quite out of the question. Our ambassador in Tel Aviv assured me only yesterday that everything was normal." Helms replied: "I am sorry, but I adhere to my opinion. The Israelis will strike and their object will be to end the war in their favor with extreme rapidity." President Lyndon Johnson then asked Helms for the source of his information. Helms said: "Mr. President, I have it from an allied secret service. The report is absolutely reliable." Helms' information came from the BND.
A further laudable success involved the BND's activity during the Czech crisis in 1968. With Pullach cryptography fully functioning, the BND predicted an invasion of Soviet and other Warsaw Pact troops into Czechoslovakia. CIA analysts on the other hand did not support the notion of "fraternal assistance" by the satellite states of Moscow; and US ambassador to the Soviet Union, Llewellyn Thompson, quite irritated, called the secret BND report he was given "a German fabrication". At 23:11 on 20 August 1968, BND radar operators first observed abnormal activity over Czech airspace. An agent on the ground in Prague called a BND out-station in Bavaria: "The Russians are coming." Warsaw Pact forces had moved as forecast.
However, the slowly sinking efficiency of BND in the last years of Reinhard Gehlen became evident. His refusal to correct reports with questionable content strained the organization's credibility, and dazzling achievements became an infrequent commodity. A veteran agent remarked at the time that the BND pond then contained some sardines, though a few years earlier the pond had been alive with sharks.
The fact that the BND could score certain successes despite East German communist Stasi interference, internal malpractice, inefficiencies and infighting, was primarily due to select members of the staff who took it upon themselves to step up and overcome then existing maladies. Abdication of responsibility by Reinhard Gehlen was the malignancy; cronyism remained pervasive, even nepotism (at one time Gehlen had 16 members of his extended family on the BND payroll). Only slowly did the younger generation then advance to substitute new ideas for some of the bad habits caused mainly by Gehlen's semi-retired attitude and frequent holiday absences.



After Gehlen s departure, his successor, Bundeswehr Brigadier General Gerhard Wessel, immediately called for a program of modernization and streamlining. With political changes in the West German government and a reflection that BND was at a low level of efficiency, the service began to rebuild.



The kidnapping and murder of Israeli athletes at the 1972 Olympics in Munich was a watershed event for the BND, following early warnings from other countries, because it led the agency to build counter-terrorism capabilities.






In 1986, the BND deciphered the report of the Libyan Embassy in East Berlin regarding the "successful" implementation of the 1986 Berlin discotheque bombing.






In 2005, a public scandal erupted (dubbed the Journalistenskandal, journalists scandal) over revelations that the BND had placed a number of German journalists under surveillance since the-mid 1990s, in an attempt to discover the source of information leaks from the BND regarding the activities of the service in connection with the war in Iraq and the "war against terror". The Bundestag constituted an investigative committee ("Parlamentarischer Untersuchungsausschuss") to investigate the allegations. The committee tasked the former Federal Appellate Court (Bundesgerichtshof) judge Dr. Gerhard Sch fer as special investigator, who published a report confirming illegal BND operations involving and targeting journalists between 1993 and 2005. As a consequence, the Chancellery issued an executive order banning BND operational measures against journalists with the aim to protect the service.
The committee published a final report in 2009, which mostly confirmed the allegations, identifying the intent to protect the BND from disclosure of classified information and finding a lack of oversight within the senior leadership of the service but did not identify any responsible members from within the government.






On 5 February 2003, Colin Powell made the case for a military attack on Iraq in front of the UN Security Council. Powell supported his case with information received from the BND, instead of Mr. Hans Blix and the IAEA. The BND had collected intelligence from an informant known as Rafid al-Janabi alias CURVEBALL, who claimed Iraq would be in possession of Weapons of Mass Destruction, apart from torturing and killing over 1000 dissidents (human persons) each year, for over 20 years. Rafid was employed before and after the 2003 incident which ultimately lead to the invasion of Iraq. The payments of 3000 Euros monthly were made by a cover firm called Thiele und Friedrichs (Munich).
As a result of the premature cancellation, al-Janabi filed a lawsuit at the Munich industrial court and won the case.



Following the 2006 Lebanon War. the BND mediated secret negotiations between Israel and Hezbollah, eventually leading up to the 2008 Israel-Hezbollah prisoner swap.



In the beginning of 2008, it was revealed that the BND had managed to recruit excellent sources within Liechtenstein banks and had been conducting espionage operations in the principality since the beginning of the 2000s. The BND mediated the German Finance Ministry's $7.3 million acquisition of a CD from a former employee of the LGT Group   a Liechtenstein bank owned by the country's ruling family. While the Finance Ministry defends the deal, saying it would result in several hundred millions of dollars in back tax payments, the sale remains controversial, as a government agency has paid for possibly stolen data. See 2008 Liechtenstein tax affair.



In November 2008, three German BND agents were arrested in Kosovo for allegedly throwing a bomb at the European Union International Civilian Office, which oversees Kosovo's governance. Later the "Army of the Republic of Kosovo" had accepted responsibility for the bomb attack. Laboratory tests had shown no evidence of the BND agents' involvement. However, the Germans were released only 10 days after they were arrested. It was suspected that the arrest was a revenge by Kosovo authorities for the BND report about organized crime in Kosovo which accuses Kosovo Prime Minister Hashim Tha i, as well as the former Prime Minister Ramush Haradinaj of far-reaching involvement in organized crime.




In 2014 an employee of BND was arrested for handing over secret documents to the United States. He was suspected of handing over documents about the committee investigating the NSA spying in Germany. The German government responded to this espionage by expelling the top CIA official in Berlin.



The BND has been reported to store 220 million sets of metadata every day. That is, they record with whom, when, where and for how long someone communicates. Apparently this data is collected across the world but the exact locations remains unclear. The Bundestag committee investigating the NSA spying scandal has uncovered that the German intelligence agency intercepts communications traveling via both satellites and Internet cables. It seems certain that the metadata only come from "foreign dialed traffic," that is, from telephone conversations and text messages that are held and sent via mobile phones and satellites. Of these 220 million data amassed every day, one percent is archived for 10 years "for long-term analysis." Apparently this long-term storage doesn t hold any Internet communications, data from social networks or emails though.



Since 2009 the Bundesnachrichtendienst is divided into the following directorates:
Gesamtlage / F hrungs- und Informationszentrum (GL) (Situation Centre)
Unterst tzende Fachdienste (UF) (Specialized Supporting Services)
Einsatzgebiete / Auslandsbeziehungen (EA) (Areas of Operation / Foreign Liaison)
Technische Aufkl rung (TA) (Signal Intelligence)
Regionale Auswertung und Beschaffung A (LA) und Regionale Auswertung und Beschaffung B (LB) (Regional Analysis and Procurement, A/B countries)
Internationaler Terrorismus und Internationale Organisierte Kriminalit t (TE) (Terrorism and International Organised Crime)
Proliferation, ABC-Waffen, Wehrtechnik (TW) (Proliferation, NBC Weapons)
Eigensicherung (SI) (Security)
Technische Unterst tzung (TU) (Technical Support)
Technische Entwicklung (TK) (Technical Development)
Zentralabteilung (ZY) (Central Services)
Gesamtumzug (UM) (Relocation [to Berlin])



The head of the Bundesnachrichtendienst is its President. The following persons have held this office since 1956:
The President of the BND is a federal Beamter paid according to BBesO order B, B9, which is in payment the equivalent of a Lieutenant General.



The President of the BND has three deputies: one Vice President, one Vice President for Military Affairs (Since December 2003), and one Vice President for Central Functions and Modernization (Possibly Since 2013). Prior to December 2003, there was only one Vice President. The following persons have held this office since 1957:



Agency 114
Abwehr
Federal Constitutional Court of Germany
List of intelligence agencies of Germany






Official website (English)