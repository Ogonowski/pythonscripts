This article is about communications systems in American Samoa.
In 2009, American Samoa was connected to the Internet using the ASH undersea cable that increased bandwidth from 20 Mbit/s to 1 Gbit/s. The project used a defunct PacRim East cable built in 1993 that previously connected Hawaii with New Zealand. The cable system now connects Samoa to American Samoa and then to Hawaii where it will connect to global submarine networks.



Main lines in use: 10,400 (2004)country comparison to the world: 202
Mobile cellular: 2,200 (2004)country comparison to the world: 210
Telephone system:domestic: good telex, telegraph, facsimile and cellular telephone services; domestic satellite system with 1 Comsat earth stationinternational: satellite earth station - 1 Intelsat (Pacific Ocean)international access code: +1.684 (in the North American Numbering Plan, Area code 684)



Radio broadcast stations: AM 2, FM 5, shortwave 0 (2005)
Radios: 57,000 (1997)



Television broadcast stations: 4 (2006) Televisions: 14,000 (1997)



Internet Service Providers (ISPs): at least 3 
Internet country code: .as
Internet Hosts: 1,923 (2008)country comparison to the world: 141
Internet users: NA



Despite the millions of dollars that governor Togiola Tulafono spent into bringing fiber optic to American Samoa, the Internet is still as slow before. The two Internet service providers that anyone on the island is actually familiar with offer the same speed at the same prices. Before fiber optic, one can see speeds up to 256k for the public while paying $75. After fiber optic, the latency may have changed but not the pricing/speed scheme which stayed at around 256k/$75.
American Samoa has the most expensive internet in America according to Engadget.



American Samoa


