Language input keys are keys designed to translate letters entered by users, usually found on Japanese and Korean keyboards, for use with an input method editor. On non-Japanese or Korean keyboard layouts using an IME, these functions can usually be reproduced via hotkeys, though not always directly corresponding to the behavior of these keys.




The OADG 109A and older 109 keyboard layouts which are the standard for Microsoft Windows have four dedicated language input keys, halfwidth/fullwidth ( / ), kanji ( ), non-conversion ( ), conversion ( ), hiragana/katakana/r maji ( / / ), and one combined with non-language specific key, alphanumeric.
Apple keyboards designed for Mac OS X have two language input keys, alphanumeric ( ) and kana ( ).
The keyboards for NEC PC-9800 series which was dominant in Japan during the 1980s and early 1990s have three language input keys, kana, NFER (no transfer, same as nonconversion), XFER (transfer, same as conversion).



Used to switch between entering Japanese and English text. It is not found as a separate key in the modern Japanese 106/109-key keyboard layout, but it is found on the IBM PS/55 5576-001 keyboard. On the IBM PS/55 5576-002 keyboard, it is mapped to the left Alt key. Many other 106/109-key keyboards do not label it as the Kanji key. On the Common Building Block (CBB) Keyboard for Notebooks, the Kanji key is located on the Half-width/Full-width key.



Conversion ( , henkan) is used to convert kana to kanji. In the Microsoft IME, Conversion selects conversion candidates on highlighted input, and Shift + Conversion is used to display the previous candidate, or zenk ho ( ). The alt version of this key is also pronounced zenk ho ( ), which means "all candidates", shows all input candidates.



Non-conversion ( , muhenkan) specifies that the kana characters entered are not to be converted into kanji candidates.



Half-width/Full-width ( / , hankaku/zenkaku) toggles between entering half-width or full-width characters (if 2 versions of same character exists) in older systems (prior to Windows 98), but now it toggles between IME on (for Japanese) and off (for English) in many systems. In Japanese 106/109-key, it is located at the upper-left corner of typing area. It can also be found as shown below.



Used to switch between hiragana or katakana characters. It can also be found for switching between hiragana, katakana and r maji as shown below. Alt+Hiragana/Katakana or Ctrl+Shift+Hiragana/Katakana (this feature is printed as R maji ( ) on the same key) toggles between r maji input and direct kana input in some IMEs (e.g. Microsoft IME).



Alphanumeric ( , eis ) toggles alphanumeric characters. In the Japanese 106/109-key layout, it is located on the Caps Lock key. Pressing Alphanumeric/Caps Lock key alone actually means alphanumeric function, a user has to press Shift+Alphanumeric/Caps Lock key to get caps lock function.




The standard keyboard layout for IBM PC compatibles of South Korea is physically almost identical to the layout of United States with some exceptions:
Hangul characters are printed on the keytops.
On the top of the \ key, the backslash is replaced with the   (Won sign) or both of them are printed. The backslash has the shape of the Won sign including system fonts such Gulim ( ) and Malgun Gothic (   ). Note that vertical bar | (  Shift+\) is also replaced as the broken bar   on some South Korean keyboards, but the broken bar in Unicode (U+00A6) is not inputted by most of Korean IMEs.
The keyboards with 1x \ and   Backspace keys, and the J-shaped   Enter key are overwhelmingly used in South Korea.
There are two additional keys:  /  Han/Yeong (or   HanYeong) and   Hanja (or   Hanja) keys. They do not exist as independent keys on some keyboards.



It toggles between entering Korean (Hangul) and English (ISO basic Latin alphabet).
Many computer systems support the alternative keys or key sequences for keyboards without the Han/Yeong key. Usually, it is eliminated on most of embedded keyboard on laptop computers in South Korea and the right Alt key is alternatively used. On the right Alt key of those laptops, only " / " (Han/Yeong) or both " / " (Han/Yeong) and "Alt" are printed.



It convert Hangul to Chinese characters (hanja) or some special characters.
Many computer systems support the alternative keys or key sequences for keyboards without the Hanja key. Usually, it is eliminated on most of embedded keyboard on laptop computers in South Korea and the right Ctrl key is alternatively used. On the right Ctrl key of those laptops, only " " (Hanja) or both " " (Hanja) and "Ctrl" are printed.






Japanese keyboards
Korean keyboards
5576 Keyboard Series