This article is about communications systems in French Polynesia.
The Honotua fiber optic cable will connect Tahiti to Hawaii in 2010. Honotua will increase the speed of transferring digital information from the current maximum Internet speed of 500 megabits per second to more than 20 gigabits per second. The cable will also connect to Moorea and the Leeward Islands of Huahine, Raiatea and Bora Bora.



Main lines in use: 32,000 (1995)
Mobile cellular: 4,000 (1995)
Telephone system:domestic: NAinternational: satellite earth station - 1 Intelsat (Pacific Ocean)



Radio broadcast stations: AM 2, FM 14, shortwave 2 (1998)
Radios: 128,000 (1997)



Television broadcast stations: 7 (plus 17 low-power repeaters) (1997)
Televisions: 40,000 (1997)



Internet Service Providers (ISPs): OPT (national operator),
Country code (Top level domain): PF
ITU Prefix: F
Amateur radio prefix (Designated by France): FO


