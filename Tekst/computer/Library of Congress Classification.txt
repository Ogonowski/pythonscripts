The Library of Congress Classification (LCC) is a system of library classification developed by the Library of Congress. It is used by most research and academic libraries in the U.S. and several other countries; in these countries, most public libraries and small academic libraries continue to use the older Dewey Decimal Classification (DDC).
LCC should not be confused with LCCN, the system of Library of Congress Control Numbers assigned to all books (and authors), which also defines URLs of their online catalog entries, such as "82006074" and "http://lccn.loc.gov/82006074". The Classification is also distinct from Library of Congress Subject Headings, the system of labels such as "Boarding schools" and "Boarding schools Fiction" that describe contents systematically. Finally, the classifications may be distinguished from the call numbers assigned to particular copies of books in the collection, such as "PZ7.J684 Wj 1982 FT MEADE Copy 1" where the classification is "PZ7.J684 Wj 1982".
The classification was invented by Herbert Putnam in 1897, just before he assumed the librarianship of Congress. With advice from Charles Ammi Cutter, it was influenced by his Cutter Expansive Classification, the Dewey Decimal System, and the Putnam Classification System (developed while Putnam was head librarian at the Minneapolis Public Library). It was designed specifically for the purposes and collection of the Library of Congress to replace the fixed location system developed by Thomas Jefferson. By the time Putnam departed from his post in 1939, all the classes except K (Law) and parts of B (Philosophy and Religion) were well developed.
LCC has been criticized for lacking a sound theoretical basis; many of the classification decisions were driven by the practical needs of that library rather than epistemological considerations. Although it divides subjects into broad categories, it is essentially enumerative in nature. That is, it provides a guide to the books actually in one library's collections, not a classification of the world.
The National Library of Medicine classification system (NLM) uses the initial letters W and QS QZ, which are not used by LCC. Some libraries use NLM in conjunction with LCC, eschewing LCC's R for Medicine. Others use LCC's QP QR schedules and include Medicine R.







Subclass AC   Collections. Series. Collected works
Subclass AE   Encyclopedias
Subclass AG   Dictionaries and other general reference works
Subclass AI   Indexes
Subclass AM   Museums. Collectors and collecting
Subclass AN   Newspapers
Subclass AP   Periodicals
Subclass AS   Academies and learned societies
Subclass AY   Yearbooks. Almanacs. Directories
Subclass AZ   History of scholarship and learning. The humanities




Subclass B   Philosophy (General)
Subclass BC   Logic
Subclass BD   Speculative philosophy
Subclass BF   Psychology
Subclass BH   Aesthetics
Subclass BJ   Ethics
Subclass BL   Religions. Mythology. Rationalism
Subclass BM   Judaism
Subclass BP   Islam. Bahaism. Theosophy, etc.
Subclass BQ   Buddhism
Subclass BR   Christianity
Subclass BS   The Bible
Subclass BT   Doctrinal Theology
Subclass BV   Practical Theology
Subclass BX   Christian Denominations




Subclass CB   History of Civilization
Subclass CC   Archaeology
Subclass CD   Diplomatics. Archives. Seals
Subclass CE   Technical Chronology. Calendar
Subclass CJ   Numismatics
Subclass CN   Inscriptions. Epigraphy
Subclass CR   Heraldry
Subclass CS   Genealogy
Subclass CT   Biography




Subclass D   History (General)
Subclass DA   Great Britain
Subclass DAW   Central Europe
Subclass DB   Austria   Liechtenstein   Hungary   Czechoslovakia
Subclass DC   France   Andorra   Monaco
Subclass DD   Germany
Subclass DE   Greco-Roman World
Subclass DF   Greece
Subclass DG   Italy   Malta
Subclass DH   Low Countries   Benelux Countries
Subclass DJ   Netherlands (Holland)
Subclass DJK   Eastern Europe (General)
Subclass DK   Russia. Soviet Union. Former Soviet Republics   Poland
Subclass DL   Northern Europe. Scandinavia
Subclass DP   Spain   Portugal
Subclass DQ   Switzerland
Subclass DR   Balkan Peninsula
Subclass DS   Asia
Subclass DT   Africa
Subclass DU   Oceania (South Seas)
Subclass DX   Gypsies










Subclass G   Geography (General). Atlases. Maps
Subclass GA   Mathematical geography. Cartography
Subclass GB   Physical geography
Subclass GC   Oceanography
Subclass GE   Environmental Sciences
Subclass GF   Human ecology. Anthropogeography
Subclass GN   Anthropology
Subclass GR   Folklore
Subclass GT   Manners and customs (General)
Subclass GV   Recreation. Leisure




Subclass H   Social sciences (General)
Subclass HA   Statistics
Subclass HB   Economic theory. Demography
Subclass HC   Economic history and conditions
Subclass HD   Industries. Land use. Labor
Subclass HE   Transportation and communications
Subclass HF   Commerce
Subclass HG   Finance
Subclass HJ   Public finance
Subclass HM   Sociology (General)
Subclass HN   Social history and conditions. Social problems. Social reform
Subclass HQ   The family. Marriage, Women and Sexuality
Subclass HS   Societies: secret, benevolent, etc.
Subclass HT   Communities. Classes. Races
Subclass HV   Social pathology. Social and public welfare. Criminology
Subclass HX   Socialism. Communism. Anarchism




Subclass J   General legislative and executive papers
Subclass JA   Political science (General)
Subclass JC   Political theory
Subclass JF   Political institutions and public administration
Subclass JJ   Political institutions and public administration (North America)
Subclass JK   Political institutions and public administration (United States)
Subclass JL   Political institutions and public administration (Canada, Latin America, etc.)
Subclass JN   Political institutions and public administration (Europe)
Subclass JQ   Political institutions and public administration (Asia, Africa, Australia, Pacific Area, etc.)
Subclass JS   Local government. Municipal government
Subclass JV   Colonies and colonization. Emigration and immigration. International migration
Subclass JX   International law, see JZ and KZ (obsolete)
Subclass JZ   International relations




Subclass K   Law in general. Comparative and uniform law. Jurisprudence
Subclass KB   Religious law in general. Comparative religious law. Jurisprudence
Subclass KBM   Jewish law
Subclass KBP   Islamic law
Subclass KBR   History of canon law
Subclass KBS   Canon law of Eastern churches
Subclass KBT   Canon law of Eastern Rite Churches in Communion with the Holy See of Rome
Subclass KBU   Law of the Roman Catholic Church. The Holy See
Subclasses   KD/KDK - United Kingdom and Ireland
Subclass KDZ   America. North America
Subclass KE   Canada
Subclass KF   United States
Subclass KG   Latin America   Mexico and Central America   West Indies. Caribbean area
Subclass KH   South America
Subclasses KJ-KKZ   Europe
Subclasses KL-KWX   Asia and Eurasia, Africa, Pacific Area, and Antarctica
Subclass KU/KUQ - Law of Australia and New Zealand
Subclass KZ   Law of nations



Subclass L   Education (General)
Subclass LA   History of education
Subclass LB   Theory and practice of education
Subclass LC   Special aspects of education
Subclass LD   Individual institutions   United States
Subclass LE   Individual institutions   America (except United States)
Subclass LF   Individual institutions   Europe
Subclass LG   Individual institutions   Asia, Africa, Indian Ocean islands, Australia, New Zealand, Pacific islands
Subclass LH   College and school magazines and papers
Subclass LJ   Student fraternities and societies, United States
Subclass LT   Textbooks




Subclass M   Music
Subclass ML   Literature on music
Subclass MT   Instruction and study



Subclass N   Visual arts
Subclass NA   Architecture
Subclass NB   Sculpture
Subclass NC   Drawing. Design. Illustration
Subclass ND   Painting
Subclass NE   Print media
Subclass NK   Decorative arts
Subclass NX   Arts in general




Subclass P   Philology. Linguistics
Subclass PA   Greek language and literature. Latin language and literature
Subclass PB   Modern languages. Celtic languages
Subclass PC   Romanic languages
Subclass PD   Germanic languages. Scandinavian languages
Subclass PE   English language
Subclass PF   West Germanic languages
Subclass PG   Slavic languages and literatures. Baltic languages. Albanian language
Subclass PH   Uralic languages. Basque language
Subclass PJ   Oriental languages and literatures
Subclass PK   Indo-Iranian languages and literatures
Subclass PL   Languages and literatures of Eastern Asia, Africa, Oceania
Subclass PM   Hyperborean, Native American, and artificial languages
Subclass PN   Literature (General)
Subclass PQ   French literature   Italian literature   Spanish literature   Portuguese literature
Subclass PR   English literature
Subclass PS   American literature
Subclass PT   German literature   Dutch literature   Flemish literature since 1830   Afrikaans literature -Scandinavian literature   Old Norse literature: Old Icelandic and Old Norwegian   Modern Icelandic literature   Faroese literature   Danish literature   Norwegian literature   Swedish literature
Subclass PZ   Fiction and juvenile belles lettres




Subclass Q   Science (General)
Subclass QA   Mathematics
Subclass QB   Astronomy
Subclass QC   Physics
Subclass QD   Chemistry
Subclass QE   Geology
Subclass QH   Natural history   Biology
Subclass QK   Botany
Subclass QL   Zoology
Subclass QM   Human anatomy
Subclass QP   Physiology
Subclass QR   Microbiology



Subclass R   Medicine (General)
Subclass RA   Public aspects of medicine
Subclass RB   Pathology
Subclass RC   Internal medicine
Subclass RD   Surgery
Subclass RE   Ophthalmology
Subclass RF   Otorhinolaryngology
Subclass RG   Gynecology and Obstetrics
Subclass RJ   Pediatrics
Subclass RK   Dentistry
Subclass RL   Dermatology
Subclass RM   Therapeutics. Pharmacology
Subclass RS   Pharmacy and materia medica
Subclass RT   Nursing
Subclass RV   Botanic, Thomsonian, and Eclectic medicine
Subclass RX   Homeopathy
Subclass RZ   Other systems of medicine



Subclass S   Agriculture (General)
Subclass SB   Horticulture. Plant propagation. Plant breeding
Subclass SD   Forestry. Arboriculture. Silviculture
Subclass SF   Animal husbandry. Animal science
Subclass SH   Aquaculture. Fisheries. Angling
Subclass SK   Hunting



Subclass T   Technology (General)
Subclass TA   Engineering Civil engineering (General).
Subclass TC   Hydraulic engineering. Ocean engineering
Subclass TD   Environmental technology. Sanitary engineering
Subclass TE   Highway engineering. Roads and pavements
Subclass TF   Railroad engineering and operation
Subclass TG   Bridges
Subclass TH   Building construction
Subclass TJ   Mechanical engineering and machinery
Subclass TK   Electrical engineering. Electronics. Nuclear engineering
Subclass TL   Motor vehicles. Aeronautics. Astronautics
Subclass TN   Mining engineering. Metallurgy
Subclass TP   Chemical technology
Subclass TR   Photography
Subclass TS   Manufacturing engineering. Mass production
Subclass TT   Handicrafts. Arts and crafts
Subclass TX   Home economics



Subclass U   Military science (General)
Subclass UA   Armies: Organization, distribution, military situation
Subclass UB   Military administration
Subclass UC   Military maintenance and transportation
Subclass UD   Infantry
Subclass UE   Cavalry. Armor
Subclass UF   Artillery
Subclass UG   Military engineering. Air forces
Subclass UH   Other military services




Subclass V   Naval science (General)
Subclass VA   Navies: Organization, distribution, naval situation
Subclass VB   Naval administration
Subclass VC   Naval maintenance
Subclass VD   Naval seamen
Subclass VE   Marines
Subclass VF   Naval ordnance
Subclass VG   Minor services of navies
Subclass VK   Navigation. Merchant marine
Subclass VM   Naval architecture. Shipbuilding. Marine engineering



Subclass Z   Books (General). Writing. Paleography. Book industries and trade. Libraries. Bibliography
Subclass ZA   Information resources/materials (General)



ACM Computing Classification System
Brinkler classification
Chinese Library Classification
Comparison of Dewey and Library of Congress subject classification
Harvard Yenching Classification
ISBN
Minnie Earl Sears, formulated Sears Subject Headings, simplified for use by small libraries
Database of Recorded American Music









Library of Congress classification outline, loc.gov
Library of Congress   classification, loc.gov
Cataloging Distribution Services   source of Library of Congress Classification schedules. loc.gov
Classification outline, loc.gov
How to read LCC call numbers, geography.about.com
How to use LCC to organize a home library, zackgrossbart.com
Easy Navigation and Search for LCC code, globaljournals.org