The following is a list of Intel's manufacturing and assembly/test sites. Processors are manufactured in semiconductor fabrication plants ("fabs") which are then sent to assembly and testing sites before delivery to customers. Approximately 75% of Intel's semiconductor fabrication is performed in the USA.
Intel's competitive strength is in manufacturing and process technology, enabling it to produce and sell advanced products versus its primary competitors, such as Advanced Micro Devices, a fabless chip manufacturer. They are generally on the cutting edge of the International Technology Roadmap for Semiconductors (ITRS) developed by SEMATECH.






The Intel Fab 3 building is a wafer manufacturing plant located in Livermore, California on North Mines Road. The plant opened in 1972 and began making wafers in April 1973. Fab 3 closed its doors in 1991. It was the first plant outside of the Santa Clara area, and where the famous Bunny Suits were first introduced.



Heredia, Costa Rica
Chandler, Arizona, USA
Chengdu, Sichuan, China
Kulim, Malaysia
Penang, Malaysia
Ho Chi Minh City, Vietnam
Jerusalem, Israel



^ "http://newsroom.intel.com/servlet/JiveServlet/download/1882-25-3841/US_Manufacturing.pdf" (PDF). newsroom.intel.com. Retrieved 2015-05-11. 
^ http://www.azcentral.com/community/chandler/articles/20140113intel-says-factory-stay-shut-now.html
^ http://www.tomshardware.co.uk/intel-fab42-14nm-cpu-factory,news-37599.html
^ http://download.intel.com/newsroom/kits/22nm/pdfs/Global-Intel-Manufacturing_FactSheet.pdf
^ http://www.techpowerup.com/179071/Mass-Production-at-Intel-s-14-nanometer-Node-Begins-This-Year.html
^ http://www.elivermore.com/photos/intel_fab3.htm