This article concerns the systems of communication in Lebanon. Lebanon possesses a number of systems of telecommunication, some of which are currently being reconstructed following damage during the civil war that ended in 1991. The country code and top-level domain for Lebanon is ".lb".



There are 1,150,000 landlines giving a relatively high fixed line penetration rate (20.5%) along with 2,916,000 mobile telephones in use in Lebanon. The telephone system was severely damaged during the civil war but was completely rebuilt and revamped. The systems that provide the infrastructure for the telephone network are, domestically, microwave radio relay stations and cables, and internationally, two Intelsat satellite-earth stations, a microwave radio relay station to Syria and three international undersea fiber optic cables: I-ME-WE, CADMOS (to Cyprus), and BERYTAR (to Syria).



Lebanon possesses one AM radio broadcast station, and 32 FM radio broadcast stations. In 2005, there were 28 privately owned FM radio stations. One FM station, which shifts between French, English, and Armenian, and the sole AM radio station, which broadcasts solely in Arabic, are owned by the state-owned Radio Lebanon, which is under the jurisdiction of the Ministry of Information. Radio Lebanon also relays Radio France International at 13:00 (UTC) daily. Among private broadcasters are Mix FM, PAX Radio, the Lebanese Broadcasting Corporation(LBCI), National Broadcasting Network, Radio One, and the Voice of Tomorrow. There are 2.85 million radios is Lebanon. In 1998 Lebanon's radio penetration rate was 906 radios per 1000 people. Furthermore, Lebanon has two digital cable television companies, Cable Vision and echonet.
There are 28 television broadcast stations in Lebanon, although the application of the audiovisual law has caused the closure of a number of TV stations. The PAL television standard is used in Lebanon. Other than the state-owned T l  Liban, most broadcasters are privately owned and earn revenues from advertising. Some of the most important television networks are the LBC, Murr TV, Al Jadeed, Future TV, Orange TV (OTV), Al-Manar, NBN, T l  Lumi re, and TL (controlled by the government). There are 1.18 million television sets in Lebanon.



There are five cable TV companies in Lebanon: Cable Vision [1], Eco Net [2], City TV [3] , Digiteck and UCL.



The development and growth of internet infrastructure has been particularly slow in Lebanon due to corruption and allegations of a sector monopoly.
Internet services are administered in Lebanon by the Ministry of Telecommunication. Lebanon provides three types of services: dialup services, wireless Internet service and ADSL. Lebanon ranks 80th on the netindex.com (as of 12 January 2014).



Dialup services cost around $7 a month but users have to pay for the cost of phone communication.



ADSL was offered for the first time in April 2007 and there were, as of July 2011, 1,284,361 subscribers. The ADSL network has been undergoing large upgrades throughout the country. The addition of the new IMEWE underwater cable during the summer of 2011 has dramatically increased Lebanon's international bandwidth capacity, allowing for increased speeds and larger data caps. The prices for ADSL varies slightly depending on the DSP but typically cost from $16/month (2 Mbit/s) to $65/month (>10 Mbit/s) on unlimited data plans.
To fix the problem, the Ministry of Telecommunications signed an 18-month contract with Consolidated Engineering & Trading and French/American Company Alcatel-Lucent to install a Fiber Optics grid. It was expected that by the end of 2011 all the areas of Lebanon will have fast internet ranging from 10-15 Mbit/s download, and 20 Mbit/s and more will be available the year after, allowing Lebanon to finally catch up with the rest of the world, Which eventually never happened. A new fiber-to-the-home initiative was launched in 2015 by the Ministry of Telecommunications under Boutrous Harb. It is expected to be fully implemented by 2020, presenting users with VDSL2+ plans capable of offering speeds reaching 150Mbit/s.



Wireless Internet services were offered for the first time in 2005 to palliate for the absence of ADSL infrastructure at the time. It's fees across ISPs revolves around $45/month. Wireless internet is portable: users can connect nearly anywhere through a receiver (connected to the client via USB or Ethernet) and it provides download rates between 2 Mbit/s and 9 Mbit/s depending on the chosen plan. Coverage weakens in densely built areas or remote locations.
Fiber-to-the-Home(FTTH)/ Fibre-to-the-Business (FTTB):
Fibre to the Home/Fibre to the Business is being deployed in a phased approach in Lebanon under the management of the Ministry of Post & Telecoms www.mpt.gov.lb and Ogero (the fixed network owner & maintainer)www.ogero.gov.lb with the aim for the majority of Lebanese population to have access to a full range of services (FTTH, IPTV, video conferencing) by 2020.
The number of Internet users in Lebanon grew rapidly in the past two years after the Telecoms Ministry and state-run Ogero introduced fast DSL and 3G and 4G service.
A 4,700 km fiber optic network is being deployed across Lebanon linking 300 fixed central offices with thousands of Active Cabinets being installed with the last mile using copper connections, allowing subscribers to reach a connection speed of 4 Mbit/s and more at home. The backbone FO network consist of 13 rings and is almost complete.
Experts say that Lebanon should have much faster Internet thanks to the international capacity cables. They added that only a small fraction of the cable capacity was used in Lebanon. However, the country did not perform as well in terms of fiber-to-the-home services.
FTTB is not yet widely deployed for businesses in Lebanon.
FTTH/FTTB Networks Deployed in Lebanon
Solidere the Lebanese company for the development and reconstruction of the Beirut Central District, has deployed a Broadband Network in partnership with Orange Business Services in March 2007. Orange operates this IP network using a fiber-optic backbone with dual connection to each building in the city center. Under its unified communication network, Solidere provides IPTV services to all its residents operated and monitored from the network main operation center. 
Beirut Digital District (BDD) was launched in September 2012 as a government facilitated project with broadband internet and telephone infrastructure facilities. Beirut Digital District (BDD) will represent a community focused urbanized hub for the creative companies and talents. The project aims to become an all-inclusive zone dedicated to improving the digital industry in Lebanon through providing state-of-the-art infrastructure and superior support services for businesses and the healthiest living environment for the young and dynamic workforce; all at competitive and affordable rates.
3.9G & 4G LTE data services
Alfa and MTC Touch have commercial 3.9G & 4G LTE data services in many regions in Lebanon (mostly dense urban regions) starting May 2013.
ISPs:
There are 17 licensed ISPs (Internet Services Providers) and 9 licensed DSPs (Data Service Providers) operating in Lebanon: Broadband Plus, ComNet, Cyberia, Farah Net, Fiberlink Networks), IDM, Keblon, Lebanon OnLine, Masco Group, Mobi, Moscanet (Wise), Onet Plus, Pro Services, Sodetel, Solidere, Terranet, Transmog (Cyberia), Tri Network Consultants, Virtual ISP (VISP).
DSPs:
Cable One, Cedarcom, GlobalCom Data Services, Pesco, Sodetel, Solidere, LCNC S.A.L., TRISAT S.A.R.L., Waves S.A.L.
In 2009 Lebanon had 2,000,000 internet users (48% of the population).
In mid-2011 Lebanon had 2,500,820, 59% penetration rate 
As of 31 December 2011, Lebanon had 3,367,220 Internet users (87% penetration rate).
As of 30 June 2012, Lebanon had 3,722,950 Internet users (92% penetration rate).



The cabinet of ministers passed a decree on 23 August 2011, to increase the current speeds of Internet connection setting the minimum speed to 2 Mbit/s in addition to lowering the prices.


