Picture Transfer Protocol (PTP) is a protocol developed by the International Imaging Industry Association to allow the transfer of images from digital cameras to computers and other peripheral devices without the need of additional device drivers. The protocol has been standardised as ISO 15740.
It is further standardized for USB by the USB Implementers Forum as the still image capture device class. USB is the default network transport media for PTP devices. USB PTP is a common alternative to USB MSC, as a digital camera connection protocol. Some cameras support both modes.



PTP specifies a way of creating, transferring and manipulating objects which are typically photographic images such as a JPEG file. While it is common to think of the objects that PTP handle as files, they are abstract entities identified solely by a 32-bit object ID. These objects can however have parents and siblings so that a file-system like view of device contents can be created.



Until the standardization of PTP, digital camera vendors used different proprietary protocols for controlling digital cameras and transferring images to computers and other host devices. The term "Picture Transfer Protocol" and the acronym "PTP" were both coined by Steve Mann, summarizing work on the creation of a Linux-friendly way of transferring pictures to and from home-made wearable computers, at a time when most cameras required the use of Microsoft Windows or Mac OS device drivers to transfer their pictures to a computer.
PTP was originally standardized as PIMA 15740 in 2000, while it was developed by the IT10 committee. Key contributors to the standard included Tim Looney and Tim Whitcher (Eastman Kodak Company) and Eran Steinberg (Fotonation).
The Media Transfer Protocol (MTP) is an extension to the Picture Transfer Protocol.



PTP does not specify a way for objects to be stored   it is a communication protocol. Nor does it specify a transport layer. However, it is designed to support existing standards, such as Exif, TIFF/EP, DCF, and DPOF, and is commonly implemented over the USB and FireWire transport layers.
Images on digital cameras are generally stored as files on a mass storage device, such as a memory card, which is formatted with a file system, most commonly FAT12, FAT16 or FAT32, which may be laid out as per the Design rule for Camera File system (DCF) specification. But none of these are required as PTP abstracts from the underlying representation.
By contrast, if a camera is mounted via USB MSC, the physical file system and layout are exposed to the user.



Many modern digital cameras from Canon and Nikon can be controlled via PTP from a USB host enabled computing device (Smartphone, PC or Arduino for example). As is the norm for PTP, the communication takes place over a USB connection. When interacting with the camera in this manner, it is expected that the USB endpoints are in (synchronous) Bulk Transfer Mode, for getting/setting virtually all the camera's features/properties (such as ISO, Aperture, Shutter speed and focus). Events raised by the camera, in response to specific actions performed by the host device, are sent back to the host via the USB asynchronous Interrupt endpoint.
In addition to changing the camera's settings and operating mode, it is possible to receive a through-the-lens view using "Live View". As described above, the storage objects of the camera's memory cards can be manipulated too.
By controlling a camera in this way it is possible to augment its capabilities. For example, if the controlling software was running on a Smartphone with GPS functionality, it would be possible to add the GPS coordinates to an image's Exif data, at the time of image capture - even if the camera itself had no GPS functionality.



A number of protocols have been developed that extend PTP. PTP/IP, developed by FotoNation and first implemented in a round of Wi-Fi digital cameras by Nikon, Canon, and Eastman Kodak, allows data transfer over any IP-based network.
Media Transfer Protocol, developed by Microsoft, allows for transfer over wireless or wired networks based in part on FotoNation's PTP/IP, but also allows users to transfer other media aside from pictures, as well as for tagging objects with extended metadata (such as title, artist and similar).



Both Microsoft and Apple include PTP support in their operating systems, from Windows ME onwards (excluding Windows CE), and Mac OS X v10.1 onwards, respectively. Microsoft implements PTP on Windows through Windows Image Acquisition.
PTP on Linux and other free software/open source operating systems is supported by a number of libraries, such as libgphoto and libptp, libraries used by applications such as digiKam and F-Spot.
The Poseidon USB stack on Amiga includes ptp.class.



PTP v1.1 (ISO15740:2008) is an update to PTP that has recently been published by ISO.
As of the middle of 2008, most devices and operating systems do not yet support PTP v1.1. This updated version of PTP is fully backward-compatible with PTP v1.0, and offers optional performance, compatibility, and feature enhancements including:
A mechanism for handling streaming content
A mechanism to support multiple vendor extension sets
Support for objects larger than the 4GiB size limit set by PTP v1.0, by requiring 64 bits (8 bytes) for object size
Support for retrieval of ObjectHandles in enumerated chunks. This may reduce long response times for some devices that possess large numbers of objects
Support for arbitrary resizing prior to image transmission (responder scaling). In PTP v1.0, image sizes might be requested in full-resolution or thumbnail size only
Support for arrays of datasets. This can be used to reduce the number of required transactions necessary for device characterization from being a function of the number of objects on the device down to one
A fast file characterization operation that exploits dataset arrays to request, in a single transaction, only the minimum data required to characterize a typical filesystem
A new standard ObjectFormatCode to support the Digital Negative (DNG) file format



Renaming file objects directly is not possible without copying or rewriting them
Modification of file contents is not supported (the file needs to be re-transferred completely)



Design rule for Camera File system
PictBridge






PTP transport over USB specifications, USB.org.
Microsoft and FotoNation Team to Support the Media Transfer Protocol For Wireless Digital Still Cameras, Microsoft News Center.
Free software with PTP support