The Wireless Experimental Centre was one of two overseas outposts of Station X, Bletchley Park, the British signals analysis centre during World War II. The other outpost was the Far East Combined Bureau.
Situated at Anand Parbat outside Delhi, it was staffed by members of the Intelligence Corps, the British and Indian armies and the Air Force. Under Colonel Patrick Marr-Johnson, it was used partly as a wireless intercept station for Japanese codes. There were three outstations, the Wireless Experimental Depot in Abbottabad, the Western Wireless sub centre at Bangalore and the Eastern Wireless sub centre at Barrackpore. There were also about 88 listening wireless sets around India, and several mobile Y-stations.



^ March 1944 : The Invasion Postponed, from Bletchley Park archives, accessed 15 February 2008
^ Intelligence and the War Against Japan: Britain, America and the Politics of Secret Service, Richard James Aldrich, Cambridge University Press 2000