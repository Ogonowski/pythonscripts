The Zilog eZ80 is an 8-bit microprocessor from Zilog which is essentially an updated version of the company's earlier Z80 8-bit microprocessor.



The eZ80 (like the Z380) is binary compatible with the Z80 and Z180, but almost four times as fast as the original Z80 chip at the same clock frequency. Available at up to 50 MHz (2004), the performance is comparable to a Z80 clocked at 200 MHz if fast memory is used (i.e. no wait states for opcode fetches, for data, or for I/O) or even higher in some applications (a 16-bit addition is 11 times as fast as in the original). The eZ80 also supports direct continuous addressing of 16 MB of memory without a memory management unit, by extending most registers (HL, BC, DE, IX, IY, SP, and PC) from 16 to 24 bits. In order to do so, the CPU works in a Z80-compatible mode or a full 24-bit address mode.
The processor has a 24-bit ALU (Arithmetic Logic Unit) and overlapped processing of several instructions (a so-called pipeline) which are the two primary reasons for its speed. Unlike the older Z280 and Z380 it does not have (or need) a cache memory. Instead, it is intended to work with fast SRAM directly as main memory (as this has become much cheaper). Nor does it have the multiplexed bus of the Z280, making it as easy to work with (interface to) as the original Z80 and Z180, and equally predictable when it comes to exact execution times.
The chip has a memory interface that is similar to the original Z80, including the bus request/acknowledge pins, and adds four integrated chip selects. Versions are available with on-chip flash memory and on-chip zero wait-state SRAM (up to 256 KB flash memory and 16 KB SRAM) but there are also external buses on all models. The eZ80 supports a TCP/IP stack and operating system based on the Xinu operating system, as well as a real-time kernel.



eZ80Acclaim! is a family of eZ80 single chip computers labeled as "application-specific standard products" (ASSP) that feature up to 128 KB of flash memory, up to 8 KB of SRAM, and can operate at speeds of up to 20 MHz. Like other eZ80 variants, it has an external address and data bus and can thus be used as a general purpose microprocessor as well.
eZ80AcclaimPlus! is a family of connectivity ASSPs that feature up to 256 KB of flash memory, 16 KB of SRAM, can operate at speeds of up to 50 MHz. It adds an integrated 10/100BaseT Ethernet MAC, TCP/IP stack over the eZ80Acclaim! line. Like other eZ80 variants, it has an external address and data bus and can thus be used as a general purpose microprocessor as well.






Cantrell, Tom (February 2002). "eZ Embedded Web". Circuit Cellar (139). Retrieved 2009-07-15. 
Harston, J.G. (1998-04-15). "Full eZ80 Opcode List". Retrieved 2009-07-15.