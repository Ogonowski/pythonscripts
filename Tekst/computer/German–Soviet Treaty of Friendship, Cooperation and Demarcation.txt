The German-Soviet Frontier Treaty was a second supplementary protocol, of the 1939 Hitler-Stalin Pact (known as the German-Soviet Treaty of Nonaggression, or by its original name of the German Soviet Treaty of Friendship, Cooperation and Demarcation). It was a secret clause as amended on September 28, 1939 by Nazi Germany and the Soviet Union after their joint invasion and occupation of sovereign Poland. It was signed by Joachim von Ribbentrop and Vyacheslav Molotov, the foreign ministers of Germany and the Soviet Union respectively, in the presence of Joseph Stalin. The treaty was a follow-up to the first secret protocol of the Molotov Ribbentrop Pact signed on August 23, 1939 between the two countries prior to their invasion of Poland and the start of World War II in Europe. Only a small portion of the protocol which superseded the first treaty was publicly announced while the spheres of influence of Nazi Germany and the Soviet Union remained classified. The third secret protocol of the Pact was signed on January 10, 1941 by Friedrich Werner von Schulenberg, and Molotov, whereas Germany renounced its claims to portions of Lithuania, only a few months before their anti-Soviet Operation Barbarossa.




Several secret articles were attached to the treaty. These articles allowed for the exchange of Soviet and German nationals between the two occupied zones of Poland, redrew parts of the central European spheres of interest dictated by the Molotov Ribbentrop Pact, and also stated that neither party to the treaty would allow on its territory any "Polish agitation" directed at the other party.

During the western invasion of Poland, the German Wehrmacht had taken control of the Lublin Voivodeship and eastern Warsaw Voivodeship - territories which according to the Molotov Ribbentrop Pact were in the Soviet sphere of influence. To compensate the Soviet Union for this "loss", the treaty's secret attachment transferred Lithuania to the Soviet sphere of influence, with the exception of a small territory in the Suwa ki Region, sometimes known as the Suwa ki Triangle. After this transfer, the Soviet Union issued an ultimatum to Lithuania, occupied it on June 15, 1940 and established the Lithuanian SSR.




Eidintas, Alfonsas; Vytautas  alys; Alfred Erich Senn (September 1999). Ed. Edvardas Tuskenis, ed. Lithuania in European Politics: The Years of the First Republic, 1918 1940 (Paperback ed.). New York: St. Martin's Press. p. 170. ISBN 0-312-22458-3. 



Text of the treaty (English translation) from ElectronicMuseum online, Polish-Canadian website commemorating Katyn massacre
 Media related to German Soviet Boundary and Friendship Treaty at Wikimedia Commons  Works related to German-Soviet Boundary and Friendship Treaty 28 September 1939 at Wikisource