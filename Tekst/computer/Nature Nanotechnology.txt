Nature Nanotechnology is a monthly peer-reviewed scientific journal published by Nature Publishing Group. It was established in October 2006. The editor-in-chief is Fabio Pulizzi. It covers all aspects of nanoscience and nanotechnology.



The journal is abstracted and indexed in:
Chemical Abstracts Service
Science Citation Index
Current Contents/Physical, Chemical & Earth Sciences
Current Contents/Engineering, Computing & Technology
Index Medicus/MEDLINE/PubMed
According to the Journal Citation Reports, the journal has a 2014 impact factor of 34.048.






Official website