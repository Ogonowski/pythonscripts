A subnotebook (also called an ultraportable or mini notebook) is a class of laptop computers that are smaller and lighter than a typical notebook.
These computers are sometimes confused with the ultra-mobile PC category, which is the name of a platform of small form-factor tablet PCs. UMPCs are smaller than subnotebooks, however both generally are found to run full desktop operating systems such as Windows or Linux, rather than specialized software such as Windows CE, Palm OS, or Internet Tablet OS. Subnotebooks are also sometimes confused with netbooks which are a different category of devices that branched off from mini notebooks in general. Netbooks are most often much less expensive than subnotebooks, as they are optimized for use as portable Internet capable devices and generally lack the processing power of a workstation. Netbooks typically have 9W TDP CPUs, sacrificing performance for power efficiency, whereas subnotebooks often use 18W TDP processors.
Subnotebooks are smaller than full sized laptops but larger than handheld computers. They often have smaller-sized screens, less than 14 inches, and weigh less than typical laptops, usually being less than 2 kg (4.4 lbs). The savings in size and weight are usually achieved partly by omitting ports or having removable media or optical disc drives. Many can be paired with docking stations to compensate.






The Compaq LTE, launched in 1989, was the first to be widely known as a "notebook computer" because its relatively small dimensions 4.8 22 28 cm (1.9   8.5   11 inches) were similar to an A4 paper notebook. The Compaq was followed in October 1992 by the very popular IBM ThinkPad, which was the first to include a 26.416 cm (10.4 inches) screen in a notebook measuring 5.6   21.1   28.4 cm (2.2   8.3   11.7 inches). Portables with smaller form factors thus became known as subnotebooks. The term was also applied to the NEC UltraLite, unveiled in 1988 its dimensions (1.4   8.3   11.75 inches) were very similar to the Compaq LTE.
Smaller DOS-based PCs such as the Poqet PC and the Atari Portfolio both released in 1989 were called either "pocket PCs" or handhelds.



One early subnotebook was the PowerBook 100 released in 1991 by Apple Inc., measuring 8.5  deep by 11  wide by 1.8  high and weighing 5.1 lbs. (2.3 kg)  Then the Gateway Handbook, originally released in 1992 and updated to use a 486 processor in late 1993, was only 9.7 inches (246 mm) wide, 5.9 inches (150 mm) deep, and 1.6 inches (41 mm) high, and weighed less than three pounds (1.4 kg). Apple followed-up with PowerBook Duo series in October 1992, which further reduced their subnotebook line to 8.5  deep by 10.9  wide by 1.4  high and is an example of a portable supporting few on-board features, but which could be inserted into a docking station to achieve the full functionality of a desktop   a feature soon emulated by other manufacturers.
Another early subnotebook was the Hewlett-Packard OmniBook 300, which was launched as a "superportable" in 1993. It measured 1.4   6.4   11.1 inches, and was available with an optional Flash memory disk instead of a hard drive, to reduce the weight.
Toshiba, which had concentrated on portables in the 1980s, also entered the market that year with the Portege T3400, claiming that "It's the first subnotebook computer with all the functionality of a much larger computer". The version with an 8.4  monochrome screen measured just 1.7   7.9   9.8 inches and weighed 1.8 kg. Toshiba also introduced the T3400CT at this time which was the first subnotebook with a color screen.
Then Toshiba really put the subnotebook PC format on the map in 1995 with the Libretto 20. This featured a 6.1  screen and 270 MB hard disk. CNet reported about the Libretto 50CT that "[it] is the first full-fledged Windows 95 notebook in the United States weighing less than two pounds".
Compaq introduced its own short lived subnotebook line in 1994 called Contura Aero, which had two models: the greyscale display 4/25 and the color 4/33, notable for using a battery which was intended to be standard rather than only useful for Compaq products.




In 1997, Apple launched the relatively light-weight (4.4 lbs) but short-lived PowerBook 2400c. This was co-designed by IBM and made for Apple by IBM Japan to replace the aging PowerBook Duo line. However, it measured 1.9   8.5   10.5 inches, so it was actually bigger than a Compaq LTE notebook. IBM had sold "thin and light" models in its ThinkPad range, such as the ThinkPad 560 ultraportable (1996) and best-selling ThinkPad 600 (1998). It finally entered the subnotebook market in 1999 with the 1.3 kg ThinkPad 240, targeted at business travellers. The 240 and 240X had 10.4  screens. Later, however, IBM replaced these with the X range, with 12.1  screens. At 8.3    10.6 , the ThinkPad X40 is not much smaller than A4 (8.3  by 11.7 ) and better described as an ultraportable, rather than a subnotebook.
Sony launched an ultraportable (ultrathin) less than an inch thick in Japan in 1997, the PCG-505, which reached the US in 1997 as the VAIO 505GX. This was followed by the even thinner Sony VAIO X505, which measured just 0.8   8.2   10.2 inches, and reached 0.3  at its thinnest. However, it was very expensive and had poor battery life, and was soon withdrawn.
Sony also launched the C1 range of subnotebooks, starting in Japan in January 2000.



One of the most notable Sony models was the Transmeta-based Vaio PCG-C1VE or PictureBook (2001), which measured only 1 6 9.8 inches. It had a digital camera built into the lid, which could be used for video conferencing or swivelled round to photograph a scene.
This was followed in 2005 by the Transmeta-based Flybook convertible with a touch-sensitive 8.9 inch. widescreen from Taiwan's Dialogue Technology. (Later models used Intel ULV processors.) The Flybook features a built-in phone connection for GPRS or 3G networking, and is available in a range of bright colors. This attracted the attention of non-computer magazines including GQ, FHM, Elle and Rolling Stone. Dimensions of 9.3    6.1  put the original Flybook A33i on a par with the Libretto, but styled to appeal more to fashion models than computer geeks.
In 2006, Microsoft stimulated a new round of subnotebook development with the UMPC or ultra-mobile PC format code-named Origami. These are basically small versions of Tablet PC computers, which originally shipped with the Microsoft Windows XP Tablet PC Edition 2005. An example is the Samsung Q1.




In 2007, the ASUS Eee PC was the first of a new class of low-cost subnotebook commonly called the netbook. These devices are based around using the Internet. Netbooks are typically based upon the Atom processors, while subnotebooks use more powerful processors such as ULV types.
At Computex 2011, Intel announced a new class of subnotebooks called Ultrabooks. The term is used to describe a highly portable laptop that has strict limits on size, weight and battery life and has tablet-like features such as instant-on functionality.
In 2015, LG introduced what is probably the lightest 14" laptop, weighing 0.98 kilograms (2.2 lb), less than a 11.6" MacBook Air.


