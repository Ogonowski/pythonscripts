The Phoenix Air Defense Sector (PhADS) is an inactive United States Air Force organization. Its last assignment was with the Air Defense Command 28th Air Division, being stationed at Luke Air Force Base, Arizona. It was inactivated on 1 April 1966



PhADS was established in June 1959 assuming control of former ADC Western Air Defense Force units in Arizona; southwestern California; southern Nevada and southwestern Utah. The organization provided command and control over several aircraft and radar squadrons.
On 15 June the new Semi Automatic Ground Environment (SAGE) Direction Center (DC-21) became operational. 33 32 34 N 112 21 27 W DC-21 was equipped with dual AN/FSQ-7 Computers. The day-to-day operations of the command was to train and maintain tactical flying units flying jet interceptor aircraft (F-94 Starfire; F-102 Delta Dagger; F-106 Delta Dart) in a state of readiness with training missions and series of exercises with SAC and other units simulating interceptions of incoming enemy aircraft.
The Sector was inactivated on 1 April 1966 as part of an ADC consolidation and reorganization; then redesignated as 27th Air Division.



Established as Phoenix Air Defense Sector on 15 June 1959
Inactivated on 1 April 1966



Western Air Defense Force, 15 June 1959
28th Air Division, 1 July 1960   1 April 1966



Luke AFB, Arizona, 15 February 1959   1 April 1966






15th Fighter-Interceptor Squadron
Davis-Monthan AFB, Arizona, 1 May 1961-4 December 1964







List of USAF Aerospace Defense Command General Surveillance Radar Stations
Aerospace Defense Command Fighter Squadrons



 This article incorporates public domain material from websites or documents of the Air Force Historical Research Agency.

A Handbook of Aerospace Defense Organization 1946 - 1980, by Lloyd H. Cornett and Mildred W. Johnson, Office of History, Aerospace Defense Center, Peterson Air Force Base, Colorado
Winkler, David F. (1997), Searching the skies: the legacy of the United States Cold War defense radar program. Prepared for United States Air Force Headquarters Air Combat Command.
Maurer, Maurer (1983). Air Force Combat Units Of World War II. Maxwell AFB, Alabama: Office of Air Force History. ISBN 0-89201-092-4.
Ravenstein, Charles A. (1984). Air Force Combat Wings Lineage and Honors Histories 1947-1977. Maxwell AFB, Alabama: Office of Air Force History. ISBN 0-912799-12-9.
Radomes.org Phoenix Air Defense Sector