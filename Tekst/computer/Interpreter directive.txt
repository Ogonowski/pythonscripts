An interpreter directive is a computer language construct that is used to control which interpreter parses and interprets the instructions in a computer program. It is also commonly referred to as an interpreter designator.
In Unix, Linux and other Unix-like operating systems (OS) the the Shebang (#!) command is the first line in a script, as it is used to tell the OS shell that this file is to be executed and what scripting language to use when executing the script commands.
An example would be #!/bin/bash, meaning run this script with the bash shell found in the /bin directory. The file system permissions must also be set so the script may be executed.   
Other systems or files may use some other magic number as the interpreter directives. (Shebang's #! magic number is a code value 23 21 followed by the path to an interpreter.)


