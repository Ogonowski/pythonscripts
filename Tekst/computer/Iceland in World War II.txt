At the beginning of World War II, Iceland was a sovereign kingdom in personal union with Denmark, with King Christian X as head of state. Iceland officially remained neutral throughout World War II. However, the British invaded Iceland on 10 May 1940. On 7 July 1941, the defence of Iceland was transferred from Britain to the United States, which was still a neutral country until five months later. On 17 June 1944, Iceland dissolved its union with Denmark and the Danish monarchy and declared itself a republic.



German interest in Iceland in the 1930s grew from nothing at all to proportions found by the British government to be alarming. The Third Reich's overtures began with friendly competition between German and Icelandic football teams. When war began, Denmark and Iceland declared neutrality and limited visits to the island by military vessels and aircraft of the belligerents.



During the German occupation of Denmark, contact between the countries was disrupted. Initially, the Kingdom of Iceland declared itself to be neutral, and limited visits of belligerent warships and imposed a ban on belligerent aircraft within Icelandic territory.
Following the invasion of Denmark on 9 April 1940 Iceland opened a legation in New York City. Iceland, however, unlike Norway, did not closely enforce limitations within its territorial waters and even slashed funding for the Icelandic Coast Guard. Many Axis merchant vessels seeking shelter within the neutral waters around Iceland were sunk by Allied warships. The Chief of the Capital Police Forces, Agnar Kofoed-Hansen, started to train the National Defence forces in early 1940.




The British imposed strict export controls on Icelandic goods, preventing profitable shipments to Germany, as part of its naval blockade. London offered assistance to Iceland, seeking cooperation "as a belligerent and an ally", but Reykjavik declined and reaffirmed its neutrality. The German diplomatic presence in Iceland, along with the island's strategic importance, alarmed the British. After a few failed attempts at persuading the Icelandic government by diplomatic means to join the Allies and becoming a co-belligerent in the war against the Axis forces, the British invaded Iceland on 10 May 1940. The initial force of 746 British Royal Marines commanded by Colonel Robert Sturges was replaced on 17 May by two regular army brigades. In June the first elements of "Z" Force arrived from Canada to relieve the British which immediately returned to the defence of the UK. Three Canadian brigades   the Royal Regiment of Canada, the Cameron Highlanders and the Fusiliers Mont-Royal   garrisoned the island until drawn down for the defence of the UK in the spring of 1941, and replaced by British garrison forces.
On 7 July 1941, the defence of Iceland was transferred from Britain to the (still officially neutral) United States, by agreement with Iceland, and US marines replaced the British. Iceland's strategic position along the North Atlantic sea-lanes, perfect for air and naval bases, could bring new importance to the island. The 1st Marine Brigade consisting of approximately 4,100 troops garrisoned Iceland until early 1942 when they were replaced by U.S. Army troops, so they could join their fellow Marines fighting in the Pacific.
Iceland cooperated with the British and then the Americans, but officially remained neutral throughout World War II.




During the war, drifting mines became a serious problem for Icelanders as well as the Allied forces. The first Icelandic Explosive Ordnance Disposal (EOD) personnel were trained in 1942 by the British Royal Navy to help deal with the problem. The British forces also supplied the Icelandic Coast Guard with weapons and ammunition, such as depth-charges against Axis U-boats. During the war, German U-boats damaged and sank a number of Icelandic vessels. Iceland's reliance on the sea, to provide nourishment and for trade, resulted in significant loss of life. In 1944 British Naval Intelligence built a group of five Marconi wireless direction finding stations on the coast west of Reykjav k. The stations were part of a ring of similar groups located around the north Atlantic to locate wireless transmissions from U-boats.
On 10 February 1944, German Focke-Wulf Fw 200 Condor from the I./KG 40, stationed in Norway, sank the British tanker SS El Grillo at Sey isfj r ur.
On 17 June 1944, Iceland dissolved its union with Denmark and the Danish monarchy and declared itself a republic.



The presence of British and American troops in Iceland had a lasting impact on the country. Engineering projects, initiated by the occupying forces   especially the building of Reykjav k Airport   brought employment to many Icelanders. This was the so-called Bretavinna or  Brit labour . Also, the Icelanders had a source of revenue by exporting fish to the United Kingdom.
There was large-scale interaction between young Icelandic women and soldiers, which came to be known as  standi  ("the condition" or "situation") in Icelandic. Many Icelandic women married Allied soldiers and subsequently gave birth to children, many of whom bore the patronymic Hansson (hans translates as "his" in Icelandic), which was used because the father was unknown or had left the country. Some children born as a result of the  standi  have English surnames.


