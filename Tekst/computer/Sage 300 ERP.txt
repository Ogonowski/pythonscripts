Sage 300 ERP is the name for the mid market Sage ERP line of Application (formerly Sage ERP Accpac), primarily serving small and medium-sized businesses. Sage 300 ERP is now produced by Sage following its acquisition in 2004.
As part of an extensive product re-branding campaign in 2012, Sage renamed Accpac to Sage 300.



Sage 300 ERP is a Windows based range of ERP software, available with a variety of database backends. This can run under a Windows environment and has an option of being hosted by Sage. Sage 300 ERP has the following modules/features:
Multi-Company and Global Operations Management
Customer Relationship Management
Intelligence Reporting
Accounts Payable
Accounts Receivable
Alerts and Alerts Server
Fixed Asset Management
General Ledger
Project and Job Costing
Return Material Authorization
Transaction Analysis and Optional Field Creator
US & Canadian Payroll
Inventory Control
Purchase Orders
(Sales) Order Entry
Intercompany Transactions 
It is multi-user, multi-currency and multi-language. It is available in five languages: English, Spanish, French and Chinese (Simplified and Traditional).



The original product, EasyBusiness Systems, was developed for the CP/M operating system in 1976 by the Basic Software Group and distributed by Information Unlimited Software. This was ported to MS-DOS and the IBM-PC in 1983.
Computer Associates acquired Information Unlimited Software in 1983 and ran it as an independent business unit. Easy Business Systems added payroll processing in 1984 and supported multiuser networking at this time. In 1987, it implemented a multi-window interface to allow moving between different modules. Easy Business Systems was renamed Accpac Plus in 1987 with the release of version 5. Accpac became popular in Canada with support of Canadian public accounting firms that would sell and support the software. The name Accpac is an acronym for 'A Complete and Comprehensive Program for Accounting Control'.
The first Windows version, CA-Accpac/2000, was developed in the early 1990s and released in October 1994. The Windows version marked the move to client/server and was developed with all new code developed in COBOL with Computer Associates development tools. These components were eventually redeveloped in C and Visual Basic. In 1996 it was known as Accpac for Windows, then ACCPAC Advantage Series in 2001.
Sage Software acquired Accpac from Computer Associcates in 2004. Sage named it Sage Accpac ERP in 2006, then Sage ERP Accpac in 2009. Sage dropped the Accpac name in 2012 when it was renamed to Sage 300 ERP.


