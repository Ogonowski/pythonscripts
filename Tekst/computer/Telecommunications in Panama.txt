Telecommunications in Panama includes radio, television, fixed and mobile telephones, and the Internet.




Radio stations: More than 100 commercial radio stations (2007).
Radios: 815,000 (1997).
Television stations: Multiple privately owned TV networks and a government-owned educational TV station; multi-channel cable and satellite TV subscription services are available (2007).
Televisions: 720,000 (2006).
The media of Panama has been highly influenced by that of the United States, since the construction of the Panama Canal. Radio broadcasting in Panama began in 1922, and television broadcasting in 1956.
Panama's official broadcaster is the National Television of Panama, which was founded in 1961. NTP started Panama's first color television service in 1972. A famous military broadcast network, the Southern Command Network (SCN), broadcast in Panama until the United States withdrew from the canal in 1999.
The broadcast media were under strict control during the regime of the dictators that ruled Panama from 1968 to 1989, including Manuel Noriega.




Calling code: +507
International call prefix: 00
Main lines: 640,000 lines in use, 90th in the world (2012).
Mobile cellular: 6.8 million lines, 96th in the world (2012).
Domestic: Facilities well developed; mobile-cellular telephone subscribership has increased rapidly; point-to-point and point-to-multi-point microwave, fiber-optic and coaxial cable link rural areas; Internet service is available.
International: Facilities well developed (2011).Communications cables: landing point for the Americas Region Caribbean Ring System (ARCOS-1), the MAYA-1, and PAN-AM submarine cable systems that together provide links to the US and parts of the Caribbean, Central America, and South America (2011).
Satellite earth stations: 2 Intelsat (Atlantic Ocean) (2011).
Connected to the Central American Microwave System (2011).



Country code (Top level domain): .pa
Internet users: 1.6 million users (2012), 102nd in the world; 45.2% of the population, 96th in the world.
Fixed broadband: 288,280 subscriptions, 80th in the world; 8.2% of the population, 91st in the world (2012).
Wireless broadband: 526,893 subscriptions, 91st in the world; 15.0% of the population, 81st in the world (2012).
Internet hosts: 11,022 hosts, 132nd in the world (2012).
IPv4: 1.6 million addresses allocated, less than 0.05% of the world total, 448 addresses per 1000 people (2012).
Internet Service Providers: 11 ISPs (2005).



There are no government restrictions on access to the Internet, but there have been anecdotal reports that the government monitors private e-mails. In a few cases, law enforcement monitoring of suspects  computers led to arrests for sex crimes.
The constitution provides for freedoms of speech and press, but there have been attempts by the government to impede the media s freedom of expression and silence criticism of public officials. The Inter-American Commission on Human Rights, Inter-American Press Association, the NGO Reporters Without Borders, and other groups criticized government efforts to censor the press. In January 2012 the president supported a bill introduced by his party in the National Assembly to penalize speech that criticized the president and his administration. The bill was withdrawn after debate.
A 2012 poll conducted by the Forum of Journalists concluded that 82 percent of local journalists considered freedom of expression restricted by threats and pressure from President Martinelli s administration. Legal actions brought by officials of the former government remained pending against many journalists. There were nine new judicial actions taken against journalists during 2012, seeking a total of 19.5 million balboas ($19.5 million). At the end of 2012 six cases were pending a decision from the tribunals, and one case was in the investigative stage.
In February 2012 indigenous mining protests blocked the Pan American Highway near Vigui in the province of Veraguas and in San F lix District in the province of Chiriqu . The Panamanian National Police (PNP), as well as demonstrators, threatened local and international journalists covering this major demonstration. The government cut off cell phone and Internet services in Veraguas and Chiriqui Provinces from 3 7 February during protests by the Ng be Bugl  indigenous group that were repressed by the PNP and the Panamanian National Border Service (SENAFRONT) patrol units. According to the Public Utilities Authority, it received orders from the National Security Council of the Ministry of the Presidency to restrict communications. Users filed multiple lawsuits charging that the action threatened freedom of communication and information.
The law prohibits arbitrary interference with privacy, family, home, or correspondence, and the government generally respects these prohibitions. Nevertheless, there have been complaints that in some cases law-enforcement authorities failed to follow legal requirements and conducted unauthorized searches. The Public Ministry maintains representatives in each PNP division to approve searches, and numerous searches were approved during 2012. The law also sets forth requirements for conducting wiretap surveillance. It denies prosecutors authority to order wiretaps on their own and requires judicial oversight. During the year several citizens claimed to have been wiretapping targets after making statements critical of the government.



Panama



 This article incorporates public domain material from the CIA World Factbook document "2013 edition".
 This article incorporates public domain material from websites or documents of the United States Department of State.



Autoridad Nacional de los Servicios P blicos (ASEP), National public services authority (Spanish).
Operators
Cable & Wireless Panama (Spanish)
Cable Onda (Spanish)
ClaroCOM (Spanish)
Columbus Networks
Digicel Panama (Spanish)
PaNETma