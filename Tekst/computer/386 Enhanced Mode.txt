In computing, protected mode, also called protected virtual address mode, is an operational mode of x86-compatible central processing units (CPUs). It allows system software to use features such as virtual memory, paging and safe multi-tasking designed to increase an operating system's control over application software.
When a processor that supports x86 protected mode is powered on, it begins executing instructions in real mode, in order to maintain backward compatibility with earlier x86 processors. Protected mode may only be entered after the system software sets up several descriptor tables and enables the Protection Enable (PE) bit in the control register 0 (CR0).
Protected mode was first added to the x86 architecture in 1982, with the release of Intel's 80286 (286) processor, and later extended with the release of the 80386 (386) in 1985. Due to the enhancements added by protected mode, it has become widely adopted and has become the foundation for all subsequent enhancements to the x86 architecture, although many of those enhancements, such as added instructions and new registers, also brought benefits to the real mode.



The Intel 8086, the predecessor to the 286, was originally designed with a 20-bit address bus for its memory. This allowed the processor to access 220 bytes of memory, equivalent to 1 megabyte. At the time, 1 megabyte was considered a relatively large amount of memory, so the designers of the IBM Personal Computer reserved the first 640 kilobytes for use by applications and the operating system and the remaining 384 kilobytes for the BIOS (Basic Input/Output System) and memory for add-on devices.
As the cost of memory decreased and memory use increased, the 1 MB limitation became a significant problem. Intel intended to solve this limitation along with others with the release of the 286.




The initial protected mode, released with the 286, was not widely used; for example, it was used by Microsoft Xenix (around 1984), Coherent and Minix. Several shortcomings such as the inability to access the BIOS or DOS calls due to inability to switch back to real mode without resetting the processor prevented widespread usage. Acceptance was additionally hampered by the fact that the 286 only allowed memory access in 16 bit segments via each of four segment registers, meaning only 4*216 bytes, equivalent to 256 kilobytes, could be accessed at a time. Because changing a segment register in protected mode caused a 6-byte segment descriptor to be loaded into the CPU from memory, the segment register load instruction took many tens of processor cycles, making it much slower than on the 8086; therefore, the strategy of computing segment addresses on-the-fly in order to access data structures larger than 128 kilobytes (the combined size of the two data segments) became impractical, even for those few programmers who had mastered it on the 8086/8088.
The 286 maintained backwards compatibility with its precursor the 8086 by initially entering real mode on power up. Real mode functioned virtually identically to the 8086, allowing the vast majority of existing 8086 software to run unmodified on the newer 286. Real mode also served as a more basic mode in which protected mode could be set up, solving a sort of chicken-and-egg problem. To access the extended functionality of the 286, the operating system would set up some tables in memory that controlled memory access in protected mode, set the addresses of those tables into some special registers of the processor, and then set the processor into protected mode. This enabled 24 bit addressing which allowed the processor to access 224 bytes of memory, equivalent to 16 megabytes.




With the release of the 386 in 1985, many of the issues preventing widespread adoption of the previous protected mode were addressed. The 386 was released with an address bus size of 32 bits, which allows for 232 bytes of memory accessing, equivalent to 4 gigabytes. The segment sizes were also increased to 32 bits, meaning that the full address space of 4 gigabytes could be accessed without the need to switch between multiple segments. In addition to the increased size of the address bus and segment registers, many other new features were added with the intention of increasing operational security and stability. Protected mode is now used in virtually all modern operating systems which run on the x86 architecture, such as Microsoft Windows, Linux, and many others.
Furthermore, learning from the failures of the 286 protected mode to satisfy the needs for multiuser DOS, Intel added a separate virtual 8086 mode, which allowed multiple virtualized 8086 processors to be emulated on the 386. Hardware support required for virtualizing the protected mode itself, however, had to wait for another 20 years.



With the release of the 386, the following additional features were added to protected mode:
Paging
32-bit physical and virtual address space (The 32-bit physical address space is not present on the 80386SX, and other 386 processor variants which use the older 286 bus.)
32-bit segment offsets
Ability to switch back to real mode without resetting
Virtual 8086 mode



Until the release of the 386, protected mode did not offer a direct method to switch back into real mode once protected mode was entered. IBM devised a workaround (implemented in the IBM AT) which involved resetting the CPU via the keyboard controller and saving the system registers, stack pointer and often the interrupt mask in the real-time clock chip's RAM. This allowed the BIOS to restore the CPU to a similar state and begin executing code before the reset. Later, a triple fault was used to reset the 286 CPU, which was a lot faster and cleaner than the keyboard controller method (and does not depend on IBM AT-compatible hardware, but will work on any 80286 CPU in any system).
To enter protected mode, the Global Descriptor Table (GDT) must first be created with a minimum of three entries: a null descriptor, a code segment descriptor and data segment descriptor. In an IBM-compatible machine, the A20 line (21st address line) also must be enabled to allow the use of all the address lines so that the CPU can access beyond 1 megabyte of memory (Only the first 20 are allowed to be used after power-up, to guarantee compatibility with older software written for the Intel 8088-based IBM PC and PC/XT models). After performing those two steps, the PE bit must be set in the CR0 register and a far jump must be made to clear the prefetch input queue.

With the release of the 386, protected mode could be exited by loading the segment registers with real mode values, disabling the A20 line and clearing the PE bit in the CR0 register, without the need to perform the initial setup steps required with the 286.



Protected mode has a number of features designed to enhance an operating system's control over application software, in order to increase security and system stability. These additions allow the operating system to function in a way that would be significantly more difficult or even impossible without proper hardware support.




In protected mode, there are four privilege levels or rings, numbered from 0 to 3, with ring 0 being the most privileged and 3 being the least. The use of rings allows for system software to restrict tasks from accessing data, call gates or executing privileged instructions. In most environments, the operating system and some device drivers run in ring 0 and applications run in ring 3.



According to the Intel 80286 Programmer's Reference Manual,
For the most part, the binary compatibility with real-mode code, the ability to access up to 16 MB of physical memory, and 1 GB of virtual memory, were the most apparent changes to application programmers. This was not without its limitations, if an application utilized or relied on any of the techniques below it wouldn't run:
Segment arithmetic
Privileged instructions
Direct hardware access
Writing to a code segment
Executing data
Overlapping segments
Use of BIOS functions, due to the BIOS interrupts being reserved by Intel
In reality, almost all DOS application programs violated these rules. Due to these limitations, virtual 8086 mode was introduced with the 386. Despite such potential setbacks, Windows 3.0 and its successors can take advantage of the binary compatibility with real mode to run many Windows 2.x (Windows 2.0 and Windows 2.1x) applications, which run in real mode in Windows 2.x, in protected mode.




With the release of the 386, protected mode offers what the Intel manuals call virtual 8086 mode. Virtual 8086 mode is designed to allow code previously written for the 8086 to run unmodified and concurrently with other tasks, without compromising security or system stability.
Virtual 8086 mode, however, is not completely backwards compatible with all programs. Programs that require segment manipulation, privileged instructions, direct hardware access, or use self-modifying code will generate an exception that must be served by the operating system. In addition, applications running in virtual 8086 mode generate a trap with the use of instructions that involve input/output (I/O), which can negatively impact performance.
Due to these limitations, some programs originally designed to run on the 8086 cannot be run in virtual 8086 mode. As a result, system software is forced to either compromise system security or backwards compatibility when dealing with legacy software. An example of such a compromise can be seen with the release of Windows NT, which dropped backwards compatibility for "ill-behaved" DOS applications.




In real mode each logical address points directly into physical memory location, every logical address consists of two 16 bit parts: The segment part of the logical address contains the base address of a segment with a granularity of 16 bytes, i.e. a segments may start at physical address 0, 16, 32, ..., 220-16. The offset part of the logical address contains an offset inside the segment, i.e. the physical address can be calculated as physical_address : = segment_part   16 + offset (if the address line A20 is enabled), respectively (segment_part   16 + offset) mod 220 (if A20 is off) Every segment has a size of 216 bytes.



In protected mode the segment_part is replaced by a 16 bit selector, the 13 upper bits (bit 3 to bit 15) of the selector contains the index of an entry inside a descriptor table. The next bit (bit 2) specifies if the operation is used with the GDT or the LDT. The lowest two bits (bit 1 and bit 0) of the selector are combined to define the privilege of the request; where a value of 0 has the highest priority and value of 3 is the lowest.
The descriptor table entry defines:
the real linear address of the segment
a limit value for the segment size
some attribute bits (flags)



The segment address inside the descriptor table entry has a length of 24 bits so every byte of the physical memory can be defined as bound of the segment. The limit value inside the descriptor table entry has a length of 16 bits so segment length can be between 1 byte and 216 byte. The calculated linear address equals the physical memory address.



The segment address inside the descriptor table entry is expanded to 32 bits so every byte of the physical memory can be defined as bound of the segment. The limit value inside the descriptor table entry is expanded to 20 bits and completed with a granularity flag (G-bit, for short):
If G-bit is zero limit has a granularity of 1 byte, i.e. segment size may be 1, 2, ..., 220 bytes.
If G-bit is one limit has a granularity of 212 bytes, i.e. segment size may be 1   212, 2   212, ..., 220   212 bytes. If paging is off, the calculated linear address equals the physical memory address. If paging is on, the calculated linear address is used as input of paging.
The 386 processor also uses 32 bit values for the address offset.
For maintaining compatibility with 286 protected mode a new default flag (D-bit, for short) was added. If the D-bit of a code segment is off (0) all commands inside this segment will be interpreted as 16-bit commands by default; if it is on (1), they will be interpreted as 32-bit commands.




 Byte offset inside entry.
 First range is the bit offset inside entry; second range is the bit offset inside byte.




In addition to adding virtual 8086 mode, the 386 also added paging to protected mode. Through paging, system software can restrict and control a task's access to pages, which are sections of memory. In many operating systems, paging is used to create an independent virtual address space for each task, preventing one task from manipulating the memory of another. Paging also allows for pages to be moved out of primary storage and onto a slower and larger secondary storage, such as a hard disk drive. This allows for more memory to be used than physically available in primary storage.
The x86 architecture allows control of pages through two arrays: page directories and page tables. Originally, a page directory was the size of one page, four kilobytes, and contained 1,024 page directory entries (PDE), although subsequent enhancements to the x86 architecture have added the ability to use larger page sizes. Each PDE contained a pointer to a page table. A page table was also originally four kilobytes in size and contained 1,024 page table entries (PTE). Each PTE contained a pointer to the actual page's physical address and are only used when the four-kilobyte pages are used. At any given time, only one page directory may be in active use.




Through the use of the rings, privileged call gates, and the Task State Segment (TSS), introduced with the 286, preemptive multitasking was made possible on the x86 architecture. The TSS allows general-purpose registers, segment selector fields, and stacks to all be modified without affecting those of another task. The TSS also allows a task's privilege level, and I/O port permissions to be independent of another task's.
In many operating systems, the full features of the TSS are not used. This is commonly due to portability concerns or due to the performance issues created with hardware task switches. As a result, many operating systems use both hardware and software to create a multitasking system.



Operating systems like OS/2 1.x try to switch the processor between protected and real modes. This is both slow and unsafe, because a real mode program can easily crash a computer. OS/2 1.x defines restrictive programming rules allowing a Family API or bound program to run in either real or protected mode. Some early Unix operating systems, OS/2 1.x, and Windows used this mode.
Windows 3.0 was able to run real mode programs in 16-bit protected mode; when switching to protected mode, it decided to preserve the single privilege level model that was used in real mode, which is why Windows applications and DLLs can hook interrupts and do direct hardware access. That lasted through the Windows 9x series. If a Windows 1.x or 2.x program is written properly and avoids segment arithmetic, it will run the same way in both real and protected modes. Windows programs generally avoid segment arithmetic because Windows implements a software virtual memory scheme, moving program code and data in memory when programs are not running, so manipulating absolute addresses is dangerous; programs should only keep handles to memory blocks when not running. Starting an old program while Windows 3.0 is running in protected mode triggers a warning dialog, suggesting to either run Windows in real mode or to obtain an updated version of the application. Updating well-behaved programs using the MARK utility with the MEMORY parameter avoids this dialog. It is not possible to have some GUI programs running in 16-bit protected mode and other GUI programs running in real mode. In Windows 3.1, real mode was no longer supported and could not be accessed.
In modern operating systems, 16-bit protected mode is still used for running applications, e.g. DPMI compatible DOS extender programs (through virtual DOS machines) or Windows 3.x applications (through the Windows on Windows subsystem) and certain classes of device drivers (e.g. for changing the screen-resolution using BIOS functionality) in OS/2 2.0 and later, all under control of a 32-bit kernel.


