A scientific calculator is a type of electronic calculator, usually but not always handheld, designed to calculate problems in science, engineering, and mathematics. They have almost completely replaced slide rules in almost all traditional applications, and are widely used in both education and professional settings.
In certain contexts such as higher education, scientific calculators have been superseded by graphing calculators, which offer a superset of scientific calculator functionality along with the ability to graph input data and write and store programs for the device. There is also some overlap with the financial calculator market.



Modern scientific calculators generally have many more features than a standard four or five-function calculator, and the feature set differs between manufacturers and models; however, the defining features of a scientific calculator include:
scientific notation
floating point arithmetic
logarithmic functions, using both base 10 and base e
trigonometric functions (some including hyperbolic trigonometry)
exponential functions and roots beyond the square root
quick access to constants such as pi and e
In addition, high-end scientific calculators generally include:
cursor controls to edit equations and view previous calculations
hexadecimal, binary, and octal calculations, including basic Boolean math
complex numbers
fractions calculations
statistics and probability calculations
programmability   see Programmable calculator
equation solving
matrix calculations
calculus
letters that can be used for spelling words or including variables into an equation
conversion of units
physical constants
While most scientific models have traditionally used a single-line display similar to traditional pocket calculators, many of them have at the very least more digits (10 to 12), sometimes with extra digits for the floating point exponent. A few have multi-line displays, with some recent models from Hewlett-Packard, Texas Instruments, Casio, Sharp, and Canon using dot matrix displays similar to those found on graphing calculators.



Scientific calculators are used widely in any situation where quick access to certain mathematical functions is needed, especially those such as trigonometric functions that were once traditionally looked up in tables; they are also used in situations requiring back-of-the-envelope calculations of very large numbers, as in some aspects of astronomy, physics, and chemistry.
They are very often required for math classes from the junior high school level through college, and are generally either permitted or required on many standardized tests covering math and science subjects; as a result, many are sold into educational markets to cover this demand, and some high-end models include features making it easier to translate the problem on a textbook page into calculator input, from allowing explicit operator precedence using parentheses to providing a method for the user to enter an entire problem in as it is written on the page using simple formatting tools.



The first scientific calculator that included all of the basic features above was the programmable Hewlett-Packard HP-9100A, released in 1968, though the Wang LOCI-2 and the Mathatronics Mathatron had some features later identified with scientific calculator designs. The HP-9100 series was built entirely from discrete transistor logic with no integrated circuits, and was one of the first uses of the CORDIC algorithm for trigonometric computation in a personal computing device, as well as the first calculator based on Reverse Polish Notation (RPN) entry. HP became closely identified with RPN calculators from then on, and even today some of their high-end calculators (particularly the long-lived HP-12C financial calculator and the HP-48 series of graphing calculators) still offer RPN as their default input mode due to having garnered a very large following.
The HP-35, introduced on February 1, 1972, was Hewlett-Packard's first pocket calculator and the world's first handheld scientific calculator. Like some of HP's desktop calculators it used RPN. Introduced at US$395, the HP-35 was available from 1972 to 1975.
Texas Instruments (TI), after the introduction of several units with scientific notation, came out with a handheld scientific calculator on January 15, 1974, in the form of the SR-50. TI continues to be a major player in the calculator market, with their long-running TI-30 series being one of the most widely used scientific calculators in classrooms.
Casio and Sharp have also been major players, with Casio's FX series (beginning with the Casio FX-1 in 1972) being a very common brand, used particularly in schools. Casio is also a major player in the graphing calculator market, and was the first company to produce one (Casio fx-7000G).


