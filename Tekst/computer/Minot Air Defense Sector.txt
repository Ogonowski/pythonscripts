The Minot Air Defense Sector (MADS) is an inactive United States Air Force organization. Its last assignment was with the Air Defense Command 29th Air Division, being stationed at Minot Air Force Base, North Dakota. It was inactivated on 1 December 1963



Established in April 1959 assuming control of former ADC Western Air Defense Force 27th Air Division units in western North Dakota and eastern Montana. The organization provided command and control over several aircraft and radar squadrons.
The sector operated a SAGE Air-Defense Control Center (ADCC) (DC-19). A SAGE Direction Center built, and an AN/FSQ-7 direction computers operated the live air picture. The Combat Control Center or CC and its accompanying An/FSQ - 8 computer were never installed or activated. The SAGE DC was consolidated with the Grand Forks Air Defense Sector on 1 March 1963, and inactivated on 1 December 1963.



Established as Minot Air Defense Sector on 1 April 1959
Inactivated on 1 December 1963



29th Air Division, 1 April 1959   1 December 1963



Minot AFB, North Dakota, 1 April 1959   1 December 1963






32d Fighter Wing (Air Defense)
Minot AFB, North Dakota, 1 February 1961-1 July 1962



32d Fighter Group (Air Defense)
Minot AFB, North Dakota, 1 August 1960-1 February 1961



5th Fighter-Interceptor Squadron
Minot AFB, North Dakota, 1 July 1962-25 June 1963
13th Fighter-Interceptor Squadron
Minot AFB, North Dakota, 1 January 1961-25 June 1963





