The involvement of the Belgian Congo (the modern-day Democratic Republic of Congo) in World War II began with the German invasion of Belgium in May 1940. Despite Belgium's surrender, the Congo remained in the conflict on the Allied side, administered by the Belgian government in exile, and provided much-needed raw materials, most notably gold and uranium, to Britain and the United States.
Congolese troops of the Force Publique fought alongside British forces in the East African Campaign, and a Congolese medical unit served in Madagascar and in the Burma Campaign. Congolese formations also acted as garrisons in Egypt, Nigeria and Palestine.
The increasing demands placed on the Congolese population by the colonial authorities during the war, however, provoked strikes, riots and other forms of resistance, particularly from the indigenous Congolese. These were repressed, often violently, by the Belgian colonial authorities. The Congo's comparative prosperity during the conflict led to a wave of post-war immigration from Belgium, bringing the white population to 100,000 by 1950, as well as a period of industrialisation that continued throughout the 1950s. The role played by Congolese uranium during the hostilities caused the country to be of interest to the Soviet Union during the Cold War.




Following World War I, Belgium possessed two colonies in Africa the Belgian Congo, which it had controlled since its annexation of the Congo Free State in 1908, and Ruanda-Urundi, a former German colony that had been mandated to Belgium in 1924 by the League of Nations. The Belgian colonial military numbered 18,000 soldiers, making it one of the largest standing colonial armies in Africa at the time.
The Belgian government followed a policy of neutrality during the interwar years. Nazi Germany invaded on 10 May 1940 and, after 18 days of fighting, Belgium surrendered on 28 May and was occupied by German forces. King Leopold III, who had surrendered to the Germans, was kept a prisoner for the rest of the war. Just before the fall of Belgium, its government, including the Minister of the Colonies Albert de Vleeschauwer, fled first to Bordeaux in France, then to London, where it formed an official Belgian government in exile in October 1940.
The Governor-General of the Congo, Pierre Ryckmans, decided on the day of Belgium's surrender that the colony would remain loyal to the Allies, in stark contrast to the French colonies that later pledged allegiance to the pro-German Vichy government. The Congo was therefore administered from London by the Belgian government in exile during the war.
Despite this assurance, disruption broke out in the city of Stanleyville (now Kisangani in the eastern Congo) among the white population panicking about the future of the colony and the threat of an Italian invasion.




Soon after the arrival of the Belgian government in exile in London, negotiations began between the Belgians and the British about the role which the Congo would play in the Allied war effort. The British were determined that the Congo should not fall into Axis hands, and planned to invade and occupy the colony if the Belgians did not come to an arrangement. This was particularly because, after the fall of Dutch and British colonies in the Far East to Japan, the Allies were desperate for raw materials like rubber which the Congo could produce in abundance. Eventually, the two parties came to an arrangement in which virtually all the British demands were accepted, including a 30 percent devaluation of the Congolese franc.
With the official agreement and the Congolese declaration of support for the Allies, the economy of the Congo and in particular its production of important raw materials, was placed at the disposal of Belgium's Allies, particularly Britain and the United States.

The Congo had become increasingly centralised economically during the Great Depression of the 1930s, as the Belgian government encouraged the production there of cotton, which had value on the international market. The greatest economic demands on the Congo were related to raw materials. Between 1938 and 1944, the number of workers employed in the mines of the Union Mini re du Haut Katanga (UMHK) rose from 25,000 to 49,000 to cope with the increased demand. In order to increase production for the war effort, the colonial authorities increased the hours and the speed at which workers, both European and African, were expected to work. This led to increasing labour unrest across the colony. Discontent among the white population was also increased by the raising of a 40 percent "war tax". High taxes and price controls were enforced from 1941, limiting the amount of profit that could be made and curbing profiteering.
The vast majority of the Congolese-produced raw resources were exported to other Allied countries. By 1942, the entire colony's output of copper, palm oil and industrial diamonds were being exported to the United Kingdom, while almost all the colony's lumber was sent to South Africa. Exports to the United States also rose from $600,000 in early 1940 to $2,700,000 by 1942.
Tax revenue from the Belgian Congo enabled the Belgian government in exile and Free Belgian Forces to fund themselves, unlike most other states in exile, which operated through subsidies and donations from sympathetic governments. It also meant that the Belgian gold reserves, which had been moved to London in 1940, were not needed to fund the war effort, and therefore were still available at the end of the war.



The Congo possessed major uranium deposits and was one of the few sources of the material available to the Allies. Uranium extracted from the disused Shinkolobwe uranium mine, owned by the UMHK in Katanga in the southern Congo, was instrumental in the development of an atomic bomb during the American Manhattan Project. The director of UMHK, Edgar Sengier, secretly despatched half of its uranium stock to New York in 1940; in September 1942, he sold it to the United States Army.
Sengier himself moved to New York, from where he directed the UMHK's operations for the rest of the war. The U.S. government sent soldiers from the Army Corps of Engineers to Shinkolobwe in 1942 to restore the mine and improve its transport links by renovating the local aerodromes and port facilities. In 1944, the Americans acquired a further 1,720 long tons (1,750 t) of Uranium ore from the newly reopened mine.




The Force Publique (or "Public Force") was the combined police and military force of both the Congo and Ruanda-Urundi. During World War II, it constituted the bulk of the Free Belgian Forces, numbering some 40,000 men. Like other colonial armies of the time, the Force Publique was racially segregated; it was led by 280 white officers and NCOs, but otherwise comprised indigenous black Africans. The Force Publique had never received the more modern equipment supplied to the Belgian Armed Forces before the war, and so had to use outdated weapons and equipment like the Stokes mortar and the Saint Chamond 75 mm gun.




Three brigades of the Force Publique were sent to Abyssinia alongside British forces to fight the Italians in June 1940. This was done, in spite of the government in exile's reservations, to demonstrate its allegiance to the Allied cause and in retaliation for the deployment of Italian bombers in bases on the channel coast within occupied Belgium.
The Belgian 1st Colonial Brigade operated in the Galla-Sidamo area in the South-West sector. In May 1941, around 8,000 men of the Force Publique, under Major-General Auguste- douard Gilliaert, successfully cut off the retreat of General Pietro Gazzera's Italians at Saio, in the Ethiopian Highlands after marching over 1,000 kilometres (620 mi) from their bases in western Congo. The troops suffered from malaria and other tropical diseases, but successfully defeated the Italians in a number of engagements. Gilliaert subsequently accepted the surrender of Gazzera and 7,000 Italian troops in a number of small engagements. Over the course of the campaign in Abyssinia, the Force Publique received the surrender of nine Italian generals, 370 high-ranking officers and 15,000 Italian colonial troops before the end of 1941. The Congolese forces in Abyssinia suffered about 500 fatalities.
After the Allied victory in Abyssinia the Force Publique moved to the British colony of Nigeria, which was being used as a staging ground for a planned invasion of Vichy-controlled Dahomey which did not occur, was also garrisoned by 13,000 Congolese troops. Then a part of the Force Publique went to Egypt and British Mandatory Palestine and was redesignated the 1st Belgian Congo Brigade Group during 1943 and 1944.



A medical unit from the Congo, the 10th (Belgian Congo) Casualty Clearing Station, was formed in 1943, and served alongside British forces during the invasion of Madagascar and in the Far East during the Burma Campaign. The unit (which had a small body of Force Publique troops for local defense of the station) included 350 black and 20 white personnel, and continued to serve with the British until 1945.



At the start of the war, the population of the Congo numbered approximately 12 million black people and 30,000 whites. The colonial government segregated the population along racial lines and there was very little mixing between the colours. The white population was highly urbanized and, in L opoldville, the capital, lived in a quarter of the city separated from the black majority. All blacks in the city had to adhere to a curfew.
Education was overwhelmingly controlled by Protestant and Catholic missions, which were also responsible for providing limited medical and welfare support to the rural Congolese. Food remained unrationed during the war, with only the sales of tyres and automobiles restricted by the government. One of the consequences of the Congo's economic mobilisation during the war, particularly for the black population, was significant urbanisation. Just 9% of the indigenous population lived in cities in 1938; by 1950, the figure stood at close to 20%. The colonial authorities arrested enemy aliens in the Congo and confiscated their property in 1940.






The demands made by the colonial government on Congolese workers during the war provoked strikes and riots from the workforce. Whites in the colony were allowed to form trade unions for the first time during the war, and their demands for better pay and working conditions were often emulated by black workers. In October 1941, white workers in the colony unsuccessfully attempted a general strike across the colony.

In December 1941, black mine workers at various sites in Katanga Province, including Jadotville and  lisabethville, went on strike, demanding that their pay be increased from 1.50 francs to 2 francs to compensate for rising living costs. The strike started on 3 December, and by the next day 1,400 workers had downed tools. All UMHK sites were affected by 9 December. The strike was also fuelled by other grievances against the colonial order and segregation.
From the start, the colonial authorities attempted to persuade the strikers to disperse and go back to work. When they refused, they were fired on. In Jadotville, 15 strikers were shot dead by the military. In  lisabethville, the strikers, including their leader L onard Mpoyi, were invited to negotiations at the town's stadium, where they were offered various concessions, including a 30% pay rise.

When the workers refused, the Governor of Katanga, Amour Maron, shot Mpoyi, killing him. The Governor then ordered his soldiers to fire on the other strikers in the stadium. Between 60 and 70 strikers were killed during the protest, although the official estimate was around 30. The miners returned to work on 10 December.
Numerous smaller strikes occurred in the Congo later in the war, though not on the same scale as in 1941. In 1944 strikes broke out in Katanga and Kasa , provoked by the conscription of workers for the mines and deteriorating working conditions. In 1945, riots and strikes occurred among the black dockworkers in the port city of Matadi.



The colonial government in the Congo depended on its military to maintain civil order and, above all, it depended on the loyalty of the native troops who made up the bulk of the Force Publique. Black non-commissioned Officers led by First Sergeant-Major Ngoie Mukalabushi, a veteran of the East Africa Campaign, munitied at Luluabourg in the central Congolese province of Kasa  in February 1944; the trigger for this was a plan to vaccinate troops who had served at the front, though the soldiers were also unhappy about the demands placed on them and their treatment by their white officers.
The mutineers broke into the base's armoury on the morning of 20 February and pillaged the white quarter of the town. The town's inhabitants fled, and a Belgian officer and two white civilians were killed. The mutineers attacked visible signs of the colonial authorities and proclaimed their desire for independence. The mutineers then dispersed to their home villages, pillaging on the way; they failed to spread the insurrection to neighbouring garrisons. Two mutineers, including Mukalabushi, were executed for their part in the insurrection.




As a result of the Congo's comparative prosperity during the conflict, the post-war period saw a wave of immigration to the country from Belgium. By 1950, 100,000 whites were living in the Congo. Nevertheless, the war highlighted the precarious nature of the colonial administration, leading Governor Ryckmans to remark that "the days of colonialism are over" in 1946. In the years after the war, the colonial government underwent extensive reform. Black people were granted significantly more rights and freedoms, leading to the growth of a so-called  volu  ("evolved") class.
Following the industrial unrest, trade unions for black workers were instituted in 1946, though they lacked power and influence. Workers at the UMHK continued to demand higher wages, and strikes were common in the colony for the next decade. Nevertheless, both wages and living conditions improved significantly in the years after the war. The war began a second wave of industrialisation that lasted right up to Congolese independence in 1960.
The 1941  lisabethville massacre is a recurrent theme in Congolese art and folklore, and was later incorporated into the popular Congolese anti-colonial narrative. The importance of Congolese uranium during the war caused the Soviet Union to become interested in the territory; it was subsequently an area of Soviet interest during the Cold War.




Belgium in World War II
Free Belgian Forces
Ruzagayura famine (1943 44)






Primary sources

Thematic studies