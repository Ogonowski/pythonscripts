Papua New Guinea is richly endowed with natural resources, but exploitation has been hampered by the rugged terrain and the high cost of developing infrastructure. Agriculture provides a subsistence livelihood for the bulk of the population. Mineral deposits, including oil, copper, and gold, account for 72% of export earnings. Budgetary support from Australia and development aid under World Bank auspices have helped sustain the economy. In 1995, Port Moresby reached an agreement with the International Monetary Fund (IMF) and World Bank on a structural adjustment program, of which the first phase was successfully completed in 1996. In 1997, droughts caused by the El Ni o weather pattern wreaked havoc on Papua New Guinea's coffee, cocoa, and coconut production, the mainstays of the agricultural-based economy and major sources of export earnings. The coffee crop was slashed by up to 50% in 1997. Despite problems with drought, the year 1998 saw a small recovery in GDP. Growth increased to 3.6% in 1999 and may be even higher in 2000, say 4.3%.



The economy generally can be separated into subsistence and market sectors, although the distinction is blurred by smallholder cash cropping of coffee, cocoa, and copra. About 75% of the country's population relies primarily on the subsistence economy. The minerals, timber, and fish sectors are dominated by foreign investors. Manufacturing is limited, and the formal labour sector consequently also is limited.




In 1999, mineral production accounted for 26.3% of gross domestic product. Government revenues and foreign exchange earning minerals. Copper and gold mines are currently in production at Porgera, Ok Tedi, Misima, Lihir, Simberi and Hidden Valley. As of 2014, talks of resuming mining operations in the Panguna mine have also resurfaced, with the Autonomous Bougainville Government and National Government of Papua New Guinea expressing interest in restarting mining operations in the area. New nickel, copper and gold projects have been identified and are awaiting a rise in commodity prices to begin development. At early 2011, there are confirmation that Mount Suckling project has found at least two new large highly prospective porphyry bodies at Araboro Creek and Ioleu Creek. A consortium led by Chevron is producing and exporting oil from the Southern Highlands Province of Papua New Guinea. In 2001, it expects to begin the commercialization of the country's estimated 640 km  (23 trillion cubic feet) of natural gas reserves through the construction of a gas pipeline from Papua New Guinea to Queensland, Australia.That project was shelved.




Papua New Guinea also produces and exports valuable agricultural, timber, and fish products. Agriculture currently accounts for 25% of GDP and supports more than 80% of the population. Cash crops ranked by value are coffee, oil, cocoa, copra, tea, rubber, and sugar. The timber industry was not active in 1998, due to low world prices, but rebounded in 1999. About 40% of the country is covered with timber rich trees, and a domestic woodworking industry has been slow to develop. Fish exports are confined primarily to shrimp. Fishing boats of other nations catch tuna in Papua New Guinea waters under license.
Papua New Guinea is the largest yam market in Asia.



In general, the Papua New Guinea economy is highly dependent on imports for manufactured goods. Its industrial sector exclusive of mining accounts for only 9% of GDP and contributes little to exports. Small-scale industries produce beer, soap, concrete products, clothing, paper products, matches, ice cream, canned meat, fruit juices, furniture, plywood, and paint. The small domestic market, relatively high wages, and high transport costs are constraints to industrial development.



Australia, Singapore, and Japan are the principal exporters to Papua New Guinea. Petroleum, mining machinery and aircraft have been the primary U.S. exports to Papua New Guinea. In 1999, as mineral exploration and new minerals investments declined, so did United States exports. Australia is Papua New Guinea's most important export market, followed by Japan and the European Union. Crude oil is the largest U.S. import from Papua New Guinea, followed by gold, cocoa, coffee, and copper ore.
U.S. companies are active in developing Papua New Guinea's mining and petroleum sectors. Chevron operates the Kutubu and Gobe oil projects and is developing its natural gas reserves. A 5,000-6,000 m  (30,000-40,000 barrel) per day oil refinery project in which there is an American interest also is under development in Port Moresby.
Papua New Guinea became a participating economy in the Asia-Pacific Economic Cooperation (APEC) Forum in 1993. It joined the World Trade Organization (WTO) in 1996.



Papua New Guinea is highly dependent on foreign aid. Australia is the largest bilateral aid donor to Papua New Guinea, offering about US$200 million a year in assistance. Budgetary support, which has been provided in decreasing amounts since independence, was phased out in 2000, with aid concentrated on project development. Other major sources of aid to Papua New Guinea are Japan, the European Union, the People's Republic of China, the Republic of China, the United Nations, the Asian Development Bank, the International Monetary Fund, and the World Bank. Volunteers from a number of countries, including the United States, and mission church workers also provide education, health, and development assistance throughout the country.



(as of 2003)
By mid-1999, Papua New Guinea's economy was in crisis. Although its agricultural sector had recovered from the 1997 drought and timber prices were rising as most Asian economies recovered from their 1998 slump, Papua New Guinea's foreign currency earnings suffered from low world mineral and petroleum prices. Estimates of minerals in exploration expenditure in 1999 were one-third of what was spent in 1997. The resulting lower foreign exchange earnings, capital flight, and general government mismanagement resulted in a precipitous drop in the value of Papua New Guinea's currency, the kina, leading to a dangerous decrease in foreign currency reserves. The kina has floated since 1994. Economic activity decreased in most sectors; imports of all kinds shrunk; and inflation, which had been over 21% in 1998, slowed to an estimated annual rate of 8% in 1999.
Citing the previous government's failure to successfully negotiate acceptable commercial loans or bond sales to cover its budget deficit, the government formed by Sir Mekere Morauta in July 1999 successfully requested emergency assistance from the International Monetary Fund and the World Bank. With assistance from the Fund and the Bank, the government has made considerable progress toward macroeconomic stabilization and economic reform.



Household income or consumption by percentage share:lowest 10%: 4.3%highest 10%: 36% (2008)
Labour force: 2.078 million
Electricity - production: 2,200 GWh (2008)
Electricity - production by source:fossil fuel: 67.78%hydro: 32.22%nuclear: 0%other: 0% (2008)
Electricity - consumption: 2,000 GWh (2008)
Electricity exports: 10 kWh (2008)
Electricity - imports: 0 kWh (2008)
Agriculture - products: coffee, cocoa, coconuts, palm kernels, tea, rubber, sweet potatoes, fruit, vegetables; poultry, pork, vanilla
Currency: 1 kina (K) = 100 toea
Exchange rates: kina (K) per US$1   2.7624 (November 1999), 2.520 (1999), 2.058 (1998), 1.434 (1997), 1.318 (1996), 1.276 (1995)


