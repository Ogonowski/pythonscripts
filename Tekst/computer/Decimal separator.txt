A decimal mark is a symbol used to separate the integer part from the fractional part of a number written in decimal form.
Different countries officially designate different symbols for the decimal mark. The choice of symbol for the decimal mark also affects the choice of symbol for the thousands separator used in digit grouping, so the latter is also treated in this article.
In mathematics the decimal mark is a type of radix point, a term that also applies to number systems with bases other than ten. Conversationally, the decimal point is commonly referred to as a "decimal dot."



In the Middle Ages, before printing, a bar (   ) over the units digit was used to separate the integral part of a number from its fractional part, e.g. 9995 (meaning, 99.95 in decimal point format). This practice derived from the decimal system used in Indian mathematics and was popularized by the Persian mathematician Al-Khwarizmi, when Latin translation of his work on the Indian numerals introduced the decimal positional number system to the Western world. His Compendious Book on Calculation by Completion and Balancing presented the first systematic solution of linear and quadratic equations in Arabic. A similar notation remains in common use as an underbar to superscript digits, especially for monetary values without a decimal mark, e.g. 9995. Later, a separator (a short, roughly vertical, ink stroke) between the units and tenths position became the norm among Arab mathematicians, e.g. 99 95. When this character was typeset, it was convenient to use the existing comma (99,95) or full stop (99.95) instead.
Gerbert of Aurillac marked triples of columns with an arc (called a "Pythagorean arc") when using his Hindu Arabic numeral-based abacus in the 10th century. Fibonacci followed this convention when writing numbers such as in his influential work Liber Abaci in the 13th century.
In France, the full stop was already in use in printing to make Roman numerals more readable, so the comma was chosen. Many other countries, such as Italy, also chose to use the comma to mark the decimal units position. It has been made standard by the ISO for international blueprints. However, English-speaking countries took the comma to separate sequences of three digits. In some countries, a raised dot or dash (upper comma) may be used for grouping or decimal mark (see examples below); this is particularly common in handwriting.
In the United States, the full stop or period (.) was used as the standard decimal mark.

In the nations of the British Empire, although the full stop could be used in typewritten material and its use was not banned, the interpunct (also known as the decimal point, point, or mid-dot) was preferred for the decimal mark in printing technologies that could accommodate it, e.g. 99 95. However, as the mid-dot was already in common use in the mathematics world to indicate multiplication, the SI rejected its use as the decimal mark. During the beginning of British metrication in the late 1960s, and with impending currency decimalisation, there was widespread debate in the United Kingdom as to whether the decimal comma or decimal point should be preferred; the British Standards Institution and some sectors of industry advocated the comma, and the Decimal Currency Board advocated for the point. In the event, the point was decided on by the Ministry of Technology in 1968.
When South Africa adopted the metric system, it adopted the comma as its decimal mark, although a number of house styles, including some English-language newspapers, such as The Sunday Times continue to use the full stop.
The three most spoken international auxiliary languages, Ido, Esperanto, and Interlingua all use the comma as the decimal mark. Interlingua has used the comma as its decimal mark since the publication of the Interlingua Grammar in 1951. Esperanto also uses the comma as its official decimal mark, while thousands are separated by non-breaking spaces: 12 345 678,9. Ido's Kompleta Gramatiko Detaloza di la Linguo Internaciona Ido (Complete Detailed Grammar of the International Language Ido) officially states that commas are used for the decimal mark while full stops are used to separate thousands, millions, etc. So the number 12,345,678.90123 (in American notation) for instance, would be written 12.345.678,90123 in Ido. The 1931 grammar of Volap k by Arie de Jong uses the comma as its decimal mark, and (somewhat unusually) uses the middle dot as the thousands separator (12 345 678,90123).
In 1958, disputes between European and American delegates over the correct representation of the decimal mark nearly stalled the development of the ALGOL computer programming language. ALGOL ended up allowing different decimal marks, but most computer languages and standard data formats (e.g. C, Java, Fortran, Cascading Style Sheets (CSS)) specify a dot.
The 22nd General Conference on Weights and Measures declared in 2003 that "the symbol for the decimal marker shall be either the point on the line or the comma on the line". It further reaffirmed that "numbers may be divided in groups of three in order to facilitate reading; neither dots nor commas are ever inserted in the spaces between groups" e.g. 1 000 000 000. This usage has therefore been recommended by technical organizations, such as the United States' National Institute of Standards and Technology.



For ease of reading, numbers with many digits may be divided into groups using a delimiter. In some countries, these "digit group separators" are only employed to the left of the decimal mark; in others, they are also used to separate numbers with a long fractional part. An important reason for grouping is that it allows rapid judgement of the number of digits, via subitizing (telling at a glance) rather than counting   contrast 1,000,000,000 with 1000000000 for a short billion.
The groups created by the delimiters tend to follow the use of the local language, which varies. In European languages, large numbers are read in groups of thousands and the delimiter (which occurs every three digits when it is used) may be called a "thousands separator". In East Asian cultures, particularly China, Japan, and Korea, large numbers are read in groups of myriads (10,000s) but the delimiter commonly separates every three digits. The Indian numbering system is somewhat more complex: it groups the rightmost three digits in a similar manner to European languages but then groups every two digits thereafter: 1.5 million would accordingly be written 15,00,000 and read as "15 lakh".
The convention for digit group separators varies but usually seeks to distinguish the delimiter from the decimal mark. Typically, English-speaking countries employ commas as the delimiter 10,000 and other European countries employ periods or spaces: 10.000 or 10 000. Because of the confusion that can result in international documents, the superseded SI/ISO 31-0 standard advocates the use of spaces and the International Bureau of Weights and Measures and International Union of Pure and Applied Chemistry advocate the use of a "thin space" in "groups of three". Within the United States, the American Medical Association's widely followed AMA Manual of Style also calls for a thin space. In some online encoding environments (for example, ASCII-only) a thin space is not practical or available, in which case a regular word space or no delimiter are the alternatives. The Metrication Board proposed this system for the United Kingdom and, while it is not universally adopted, it is standard within the country's construction industry.



Digit group separators can occur either as part of the data or as a mask through which the data is displayed. This is an example of the separation of presentation and content. In many computing contexts, it is preferred to omit digit group separators from the data and instead overlay them as a mask (an input mask or an output mask). Common examples include spreadsheets and databases in which currency values are entered without such marks but are displayed with them inserted. Similarly, phone numbers can have hyphens or spaces as a mask rather than as data.
In some programming languages, it is possible to group the digits in the program's source code to make it easier to read; see Integer literal: Digit separators. Ada, D, Java, Perl and Ruby use the underscore (_) character for this purpose. All these languages allow writing seven hundred million as 700_000_000. C++14 allows the use of the apostrophe (') for digit grouping and therefore seven hundred million can be represented as 700'000'000.



The International Bureau of Weights and Measures states that "when there are only four digits before or after the decimal marker, it is customary not to use a space to isolate a single digit". Likewise, some manuals of style state that thousands separators should not be used in normal text for numbers from 1000 to 9999 inclusive where no decimal fractional part is shown (in other words, for four-digit whole numbers), whereas others use thousands separators, and others use both. For example, APA style stipulates a thousands separator for "most figures of 1,000 or more" except for page numbers, binary digits, temperatures, etc.
There are always common-sense exceptions to digit grouping, such as years, postal codes and ID numbers of predefined nongrouped format, which style guides usually point out.



In binary (base-2), a full space can be used between groups of four digits, corresponding to a nibble, or equivalently to a hexadecimal digit. Alternatively, binary digits may be grouped by threes, corresponding to an octal digit. Similarly, in hexadecimal (base-16), full spaces are usually used to group digits into twos, making each group correspond to a byte.



In countries with a decimal comma, the decimal point is also common as the "international" notation because of the influence of devices, such as electronic calculators, which use the decimal point. Most computer operating systems allow selection of the decimal mark and programs that have been carefully internationalized will follow this, but some programs ignore it and a few are even broken by it.






Countries where a dot "." is used as decimal mark include:



Countries where a comma "," is used as decimal mark include:



Unicode defines a decimal separator key symbol (  in hex U+2396, decimal 9110) which looks similar to the apostrophe. This symbol is from ISO/IEC 9995 and is intended for use on a keyboard to indicate a key that performs decimal separation.
In the Arab world, where Eastern Arabic numerals are used for writing numbers, a different character is used to separate the integer and fractional parts of numbers. It is referred to as an Arabic decimal separator ( ) in Unicode. An Arabic thousands separator ( ) also exists.
In Persian, the decimal mark is called momayyez, which is written like a forward slash there is a small difference between the "comma" character used in sentences and the momayyez ( ) used to separate sequences of three digits. To separate sequences of three digits, a comma or blank space may be used; however this is not a standard.
In English Braille, the decimal point,  , is distinct from both the comma,  , and the period / full stop  .



The following examples show the decimal mark and the thousands separator in various countries that use the Hindu Arabic numeral system.
In Albania, Belgium, Bosnia, Estonia, France, Hungary, Poland, Slovakia and much of Latin Europe as well as French Canada: 1234567,89 (In Spain, in handwriting it is also common to use an upper comma: 1.234.567'89)
In Brazil, Denmark, Germany, Greece, Indonesia, Italy, Netherlands, Portugal, Romania, Russia, Slovenia, Sweden and much of Europe: 1234567,89 or 1.234.567,89. In handwriting, 1 234 567,89 is also seen, but never in Brazil, Denmark, Germany, the Netherlands, Portugal, Romania, Russia, Slovenia or Sweden. In Italy a straight apostrophe is also used in handwriting: 1'234'567,89. In the Netherlands, the points thousands separator is preferred in currency amounts, and the space for other large numbers.
In Germany and Austria, thousands separators were occasionally denoted by alternating uses of comma and point, e.g. 1.234,567.890,12 for "one billion 234 million ..."
In Switzerland: There are two cases. 1'234'567.89 is used for currency values. An apostrophe as thousands separator along with a "." as decimal mark. For other values the SI style 1234567,89 is used with a "," as decimal mark. The apostrophe is also the most common variety for non-currency values: 1'234'567,89, though this usage is officially discouraged.
In Ireland, Israel, Japan, Korea (both), Malaysia, New Zealand, the Philippines, Singapore, Taiwan, Thailand, the United Kingdom, and the United States: 1,234,567.89 or 1,234,567 89; the latter is generally found only in older, and especially handwritten, documents. Australia used this style up until the 1970s; now it uses the SI style.
In English Canada: There are two cases. The preferred method for currency values is $4,000.00 while for numeric values it is 1234567.89; however, commas are also sometimes used although no longer taught in school or used in official publications.
SI style: 1 234 567.89 or 1 234 567,89 (in their own publications the dot is used in the English version and the comma in the French version).
In China, comma and space are used to mark digit groups because dot is used as decimal mark. There is no universal convention on digit grouping, so both thousands grouping and no digit grouping can be found. Although names for large numbers in Chinese are based on powers of 10,000 (Trad / Simp:  / ) (e.g. the next new word (T/S:  / ) is for 108), grouping is never done for every 4 digits. Japan is similar, although when grouping by myriads numeral kanji are frequently used as separators: 1 2345 6789 / 1 2345 6789.
In India, due to a numeral system using lakhs (lacs) (1,23,456 equal to 123 456) and crores (1,23,45,678 equal to 12 345 678), comma is used at levels of thousand, lakh and crore, for example, 10 million (1 crore) would be written as 1,00,00,000. In Pakistan, there is a greater tendency to use the standard western system, while using the Indian numbering system when conducting business in Urdu.



Used with Western Arabic numerals (0123456789):
U+0020   space (HTML &#32;)
U+0027 ' apostrophe (HTML &#39;    &apos;)
U+002C , comma (HTML &#44;)
U+002E . full stop (HTML &#46;)
U+00B7   middle dot (HTML &#183;    &middot;)
U+2009   thin space (HTML &#8201;    &thinsp;)
U+202F   narrow no-break space (HTML &#8239;)
U+02D9   dot above (HTML &#729;)
Used with Eastern Arabic numerals ( ):
U+066B   arabic decimal separator (HTML &#1643;)
U+066C   arabic thousands separator (HTML &#1644;)
Used with keyboards:
U+2396   decimal separator key symbol (HTML &#9110;) (resembles an apostrophe)


