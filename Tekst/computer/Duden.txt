The Duden (German pronunciation: [ du d n]) is a dictionary of the German language, first published by Konrad Duden in 1880. The Duden is updated regularly, with new editions appearing every four or five years. As of August 2013 it is in its 26th edition and in 12 volumes, each covering different aspects such as loanwords, etymology, pronunciation, synonyms, etc.
The first of these volumes, Die deutsche Rechtschreibung (English: German Orthography), has long been the prescriptive source for the spelling of German. The Duden has become the bible of the German language, being the definitive set of rules regarding grammar, spelling and use of German language.






In 1872, Konrad Duden, headmaster of a Gymnasium (secondary school) in Schleiz, Thuringia, published a German dictionary called the Schleizer Duden, the first Duden. In 1880 he published the Vollst ndiges Orthographisches W rterbuch der deutschen Sprache ("Complete Orthographical Dictionary of the German Language"); this seminal treatise was declared the official source for correct spelling in the administration of Prussia the same year. The first edition of this Duden contained 28,000 entries.



In 1902 the Bundesrat confirmed the Duden as the official standard for German spelling; Austria-Hungary and Switzerland soon followed suit.
In the ensuing decades, the Duden continued to be the de facto standard for German orthography. After World War II this tradition continued separately in East and West Germany in Leipzig and Mannheim.
In West Germany, some publishing houses began to attack the Duden "monopoly" in the 1950s, publishing dictionaries that contained alternative spellings. In reaction, in November 1955 the ministers of culture of the states of Germany confirmed that the spellings given by the Duden would continue to be the official standard.




In 1954, the first published Duden appeared in Mannheim, the western counterpart to the traditional Duden printing city of Leipzig. The first East German Duden appeared in Leipzig in 1951 but was largely ignored as illegitimate by West Germany. The printing continued in both Mannheim and Leipzig until the fall of the Berlin Wall in 1989. The differences between the two versions of Duden printed during this period appear in the number of entries (Stichw rter). When the printing of the two Dudens began, in 1954 and 1951, the number of Stichw rter included was roughly the same. As the split between the printers continued, the East German Duden slowly began diminishing the number of Stichw rter in its volume while the West German Duden printed in Mannheim increased the number of Stichw rter. The major differences between the two Dudens are seen in the lexical entries. The East German Duden included various loan words from Russian, particularly in the area of politics, Politb ro and Sozialdemokratismus. Also new to the East German Duden were words stemming from Soviet agricultural and industrial organization and practices.
Of note are the few semantic changes that are recorded in the East German Duden that originated from contact with Russian. The East German Duden records the nominalization of German words by adding the suffix -ist, borrowed from the Russian suffix. Also recorded is the increasing number of adverbs and adjectives negated with the prefix un-, unernst and unkonkret. The few lexical and semantic items recorded in the East German Duden come from der gro e Duden because the printing press in Leipzig did not publish the multiple volume Duden that has become the current staple.




On the cover of the Duden, 25th Edition, Volume 1, it says in red letters: Das umfassende Standardwerk auf der Grundlage der aktuellen amtlichen Regeln. This is: "The comprehensive standard reference based on the current official rules."
The "current official rules" are the outcome of the German spelling reform of 1996.



Die deutsche Rechtschreibung   The Spelling Dictionary
Das Stilw rterbuch   The Dictionary of Style
Das Bildw rterbuch   The Pictorial Dictionary
Die Grammatik   The Grammar
Das Fremdw rterbuch   The Dictionary of Foreign Words
Das Aussprachew rterbuch   The Pronouncing Dictionary
Das Herkunftsw rterbuch   The Etymological Dictionary
Das Synonymw rterbuch   The Thesaurus
Richtiges und gutes Deutsch   Correct and Good German (Guide to usage)
Das Bedeutungsw rterbuch   The Dictionary (Definitions)
Redewendungen   Figures of Speech
Zitate und Ausspr che   Quotations and Sayings



Betz, Werner. Ver ndert Sprache die Welt: Semantik, Politik und Manipulation. Edition Interfrom AG: Z rich, 1977.
Hellmann, Manfred W.,ed. Zum  ffentlichen Sprachgebrauch in Bundesrepublik Deutschland un in der DDR. D sseldorf: P dagogischer Verlag Schwann, 1973.
Reich, Hans H. Sprache und Politik: Untersuchungen zu Wortschatz und Wortwahl des offiziellen Sprachgebrauchs in der DDR. M nchen: Max Hueber Verlag, 1968.
Schlosser, Horst Dieter, ed. Kommunkationsbedingungen und Alltagssprache in der ehemaligen DDR. Hamburg: Helmut Buske Verlag, 1991.
Siegl, Elke Annalene. Duden-Ost Duden-West: Zur Sprache in Deutschland seit 1945: ein Vergleich der Leipziger und der Mannheimer Dudenauflagen seit 1947. D sseldorf: Schwann, 1989.






Official Duden site (German)