NEC Corporation ( , Nippon Denki Kabushiki Gaisha) is a Japanese multinational provider of information technology (IT) services and products, with its headquarters in Minato, Tokyo, Japan. NEC provides IT and network solutions to business enterprises, communications services providers and to government agencies. The company was known as the Nippon Electric Company, Limited, before rebranding in 1983 as just NEC. Its NEC Semiconductors business unit was one of the worldwide top 20 semiconductor sales leaders before merging with Renesas Electronics. NEC is a member of the Sumitomo Group.






Kunihiko Iwadare and Takeshiro Maeda established Nippon Electric Limited Partnership on August 31, 1898 by using facilities that they had bought from Miyoshi Electrical Manufacturing Company. Iwadare acted as the representative partner; Maeda handled company sales. Western Electric, which had an interest in the Japanese phone market, was represented by Walter Tenney Carleton. Carleton was also responsible for the renovation of the Miyoshi facilities. It was agreed that the partnership would be reorganized as a joint-stock company when treaty would allow it. On July 17, 1899 the revised treaty between Japan and the United States went into effect. Nippon Electric Company, Limited was organized the same day with Western Electric Company to become the first Japanese joint-venture with foreign capital. Iwadare was named managing director. Ernest Clement and Carleton were named as directors. Maeda and Mototeru Fujii were assigned to be auditors. Iwadare, Maeda and Carleton handled the overall management.
The company started with the production, sales and maintenance of telephones and switches. NEC modernized the production facilities with the construction of the Mita Plant in 1901 at Mita Shikokumachi. It was completed in December 1902.
The Japanese Ministry of Communications adopted a new technology in 1903: the common battery switchboard supplied by NEC. The common battery switchboards powered the subscriber phone, eliminating the need for a permanent magnet generator in each subscriber's phone. The switchboards were initially imported, but were manufactured locally by 1909.
NEC started exporting telephone sets to China in 1904.
In 1905, Iwadare visited Western Electric in the U.S. to see their management and production control. On his return to Japan he discontinued the "oyakata" system of sub-contracting and replaced it with a new system where managers and employees were all direct employees of the company. Inefficiency was also removed from the production process. The company paid higher salaries with incentives for efficiency. New accounting and cost controls were put in place, and time clocks installed.
Between 1899 and 1907 the number of telephone subscribers in Japan rose from 35,000 to 95,000. NEC entered the China market in 1908 with the implementation of the telegraph treaty between Japan and China. They also entered the Korean market, setting up an office in Seoul in January 1908. During the period of 1907 to 1912 sales rose from 1.6 million yen to 2 million yen. The expansion of the Japanese phone service had been a key part of NEC's success during this period. This expansion was about to take a pause.
The Ministry of Communications delayed a third expansion plan of the phone service in March, 1913, despite having 120,000 potential telephone-subscribers waiting for phone installations. NEC sales fell sixty percent between 1912 and 1915. During the interim, Iwadare started importing appliances, including electric fans, kitchen appliances, washing machines and vacuum cleaners. Electric fans had never been seen in Japan before. The imports were intended to prop up company sales. In 1916, the government resumed the delayed telephone-expansion plan, adding 75,000 subscribers and 326,000 kilometers of new toll lines. Thanks to this third expansion plan, NEC expanded at a time when much of the rest of Japanese industry contracted.



In 1919, NEC started its first association with Sumitomo, engaging Sumitomo Densen Seizosho to manufacture cables. As part of the venture, NEC provided cable manufacturing equipment to Sumitomo Densen. Rights to Western Electrics duplex cable patents were also transferred to Sumitomo Densen.
The Great Kant  earthquake struck Japan in 1923. 140,000 people were killed and 3.4 million were left homeless. Four of NEC's factories were destroyed, killing 105 of NEC's engineers and workers. Thirteen of Tokyo's telephone offices were destroyed by fire. Telephone and telegraph service was interrupted by damage to telephone cables. In response, the Ministry of Communications accelerated major programs to install automatic telephone switching systems and enter radio broadcasting. The first automatic switching systems were the Strowger-type model made by Automatic Telephone Manufacturing Co. (ATM) in the United Kingdom. NEC participated in the installation of the automatic switching systems, ultimately becoming the general sales agent for ATM. NEC developed its own Strowger-type automatic switching system in 1924, a first in Japan. One of the plants almost leveled during the Kanto earthquake, the Mita Plant, was chosen to support expanding production. A new three-story steel-reinforced concrete building was built, starting in 1925. It was modeled after the Western Electric Hawthorne Works.
NEC started its radio communications business in 1924. Japan's first radio broadcaster, Radio Tokyo was founded in 1924 and started broadcasting in 1925. NEC imported the broadcasting equipment from Western Electric. The expansion of radio broadcasting into Osaka and Nagoya marked the emergence of radio as an Industry. NEC established a radio research unit 1924. NEC started developing electron tubes in 1925. By 1930, they were manufacturing their first 500 W radio transmitter. They provided the Chinese Xinjing station with a 100 kW radio broadcasting system in 1934.
Photo-telegraphic equipment developed by NEC transmitted photos of the accession ceremony of Emperor Hirohito. The ceremony was held in Kyoto in 1928. The Newspapers Asahi Shimbun and Mainichi Shimbun were competing to cover the ceremony. The Asahi Shimbun was using a Siemens device. The Mainichi was planning to use French photo-telegraphic equipment. In the end, both papers acquired and used the NEC product, due to its faster transmission rate and higher picture quality.
In 1929 Nippon Electric provided Japan's Ministry of Communications with the A-type switching system, the first of these systems to be developed in Japan. Nippon supplied Japan's Ministry of Communications with nonloaded line carrier equipment for long distance telephone channels in 1937.



World War II was described by the company as being the blackest days of its history. In 1938 the Mita and Tamagawa plants were placed under military control, with direct supervision by military officers. In 1939, Nippon Electric established a research laboratory in the Tamagawa plant. It became the first Japanese company to successfully test microwave multiplex communications. On December 22, 1941, the enemy property control law was passed. NEC shares owned by International Standard Electric Corporation (ISE), an ITT subsidiary and Western Electric affiliate were seized. Capital and technical relations were abruptly severed. The "Munitions Company Law" was passed in October 1943, placing overall control of NEC plants under military jurisdiction. The Ueno plant was leveled by military attack in March 1945. Fire bombings in April and May heavily damaged the Tamagawa Plant, reducing its capacity by forty percent. The Okayama Plant was totally destroyed by a bombing attack in June of the same year. At the end of the war, NEC s production had been substantially reduced by damage to its facilities, and by material and personnel shortages.



After the war, production was slowly returned to civilian use. NEC re-opened its major plants by the end of January 1946. NEC began transistor research and development in 1950. It started exporting radio-broadcast equipment to Korea under the first major postwar contract in 1951. NEC received the Deming prize for excellence in quality control in 1952. Computer research and development began in 1954. NEC produced the first crossbar switching system in Japan. It was installed at Nippon Telegraph and Telephone Public Corporation (currently Nippon Telegraph and Telephone Corporation; NTT) in 1956. NEC began joint research and development with NTT of electronic switching systems the same year. NEC established Taiwan Telecommunication Company as their first postwar overseas joint venture in 1958. They completed the NEAC-1101 and NEAC-1102 computers the same year. In 1959 NEC demonstrated their first transistorized computer, the NEAC-2201. They demonstrated it at the UNESCO AUTOMATH show in Paris. The company began integrated circuit research and development in 1960. In 1963 NEC started trading as American Depositary Receipts, ten million shares being sold in the United States. Nippon Electric New York (now NEC America Inc.) was incorporated in the same year.

NEC supplied KDD with submarine cable systems for laying in the Pacific Ocean in 1964. They supplied short-haul 24 channel PCM carrier transmission equipment to NTT in 1965. NEC de Mexico, S. A. de C. V., NEC do Brasil, S. A., NEC Australia Pty. Ltd. were established between 1968 and 1969. NEC supplied Comsat Corporation with the SPADE satellite communications system in 1971. In 1972, Switzerland ordered a NEC satellite communications earth station. The same year, a small transportable satellite communications earth station was set up in China. Shares of NEC common stock were listed on the Amsterdam Stock Exchange in 1973. NEC also designed an automated broadcasting system for the Japan Broadcasting Corporation the same year. NEC Electronics (Europe) GmbH was also established. In 1974, the ACOS series computer was introduced. The New Central Research Laboratories were completed in 1975. In 1977, Japan's National Space Development Agency launched the NEC geostationary meteorological satellite, named Himawari.
During this period NEC introduced the concept of "C&C", the integration of computers and communications. NEC America Inc. opened a plant in Dallas, Texas to manufacture PABX and telephone systems in 1978. They also acquired Electronic Arrays, Inc. of California the same year to start semiconductor chip production in the United States.




In 1980, NEC created the first digital signal processor, the NEC  PD7710. NEC Semiconductors (UK) Ltd. was established in 1981, producing VLSIs and LSIs. NEC introduced the 8-bit PC-8800 series personal computer in 1981, followed by the 16-bit PC-9800 series in 1982. In 1983 NEC stock was listed on the Basel, Geneva and Zurich, Switzerland exchanges. NEC changed its English company name to NEC Corporation the same year. NEC Information Systems, Inc. started manufacturing computers and related products in the United States in 1984. NEC also released the V-series processor the same year. In 1986, NEC delivered its SX-2 super computer to the Houston Advanced Research Center, The Woodlands, Texas. In the same year, the NEAX61 digital switching system went in to service. In 1987, NEC Technologies (UK) Ltd. was established in the United Kingdom to manufacture VCRs, printers and computer monitors and mobile telephones for Europe. Also that year, NEC licensed technology from Hudson Soft, a video game manufacturer, to create a video game console called the PC-Engine (later released in 1989 as the TurboGrafx-16 in the North American market). Its successor, the PC-FX, was released in Japan in 1994. NEC USA, Inc. was established in 1989 as a holding company for North American operations.
In 1990, the new head office building, known as the "Super Tower", was completed in Shiba, Tokyo. Additionally, joint-venture agreements were established to manufacture and market digital electronic switching systems and LSIs in China. In 1993 NEC's asynchronous transfer mode (ATM) switching system, the NEAX61 (Nippon Electronic Automatic Exchange) ATM Service Node, went into service in the United States. NEC Europe, Ltd. was established as a holding company for European operations the same year. The NEC C&C Research Laboratories, NEC Europe, Ltd. were opened in Germany in 1994. NEC (China) Co, Ltd. was established as a holding company for Chinese operations in 1996. In 1997 NEC developed 4Gbit DRAM, and their semiconductor group was honored with one of the first Japan Quality Awards. In 1998, NEC opened the world's most advanced semiconductor R&D facility.




NEC celebrated their 100th anniversary in 1999. NEC Electronics Corporation was separated from NEC in 2002 as a new semiconductor company. NEC Laboratories America, Inc. (NEC Labs) started in November, 2002 as a merger of NEC Research Institute (NECI) and NEC USA s Computer and Communications Research Laboratory (CCRL). NEC built the Earth Simulator Computer (ESC), the fastest supercomputer in the world from 2002 to 2004, and since produced the NEC N343i in 2006.
In 2007, NEC and Nissan Co. Corp. started evaluating a joint venture to produce lithium ion batteries for hybrid and electric cars.
On April 23, 2009, Renesas Technology Corp and NEC Electronics Corp struck a basic agreement to merge by around April 2010. On April 1, 2010 NEC Electronics and Renesas Technology merged forming Renesas Electronics which is set to be fourth largest semiconductor company according to iSuppli published data.
On January 27, 2011, NEC formed a PC joint venture with Chinese PC maker Lenovo, the fourth largest PC maker in the world. As part of the deal, the companies said in a statement they will establish a new company called Lenovo NEC Holdings B.V., which will be registered in the Netherlands. NEC will receive US$175 million from Lenovo through the issuance of Lenovo's shares. Lenovo, through a unit, will own a 51% stake in the joint venture, while NEC will hold a 49% stake. In February 2011, Bloomberg News said the joint venture would allow Lenovo to expand in the field of servers, and NEC's Masato Yamamoto said NEC would be able to grow in China.
On January 26, 2012 NEC Corporation announced that it would cut 10,000 jobs globally due to big loss on NEC's consolidated financial statement in line with economic crisis in Europe and lagged in the development of smartphones in the domestic market compare to Apple and Samsung. Previously, in January 2009 NEC has cut about 20,000 jobs, mainly in sluggish semiconductor and liquid crystal display related businesses.
In August 2014, NEC Corporation was commissioned to build a super-fast undersea data transmission cable linking the United States and Japan for a consortium of five international companies including Google, KDDI and SingTel. The pipeline is expected to be completed by the end of 2016.



NEC has structured its organization around three principal segments: IT solutions, network solutions and electronic devices.
The IT solutions business delivers computing solutions to business enterprises, government and individual customers   in the form of software, hardware and related services.
The network solutions business designs and provides broadband network systems, mobile and wireless communications network systems, mobile handsets, broadcast and other systems.
NEC's electronic devices business includes semiconductors, displays and other electronic components. NEC produces Versa notebooks for the international market and the LaVie series for Japanese markets.
Principal subsidiaries of NEC include:
NEC Corporation of America
NEC Display Solutions of America Inc.
NetCracker Technology Corp.




NEC MobilePro - a handheld computer running Windows CE
NEC Aspire hybrid small business phone system
Electric vehicle batteries (Automotive Energy Supply Corporation, a joint-venture between Nissan, NEC Corporation and NEC TOKIN)
NEC mobile phone
NEC America MultiSync Monitors and Fax devices
NEC Information Systems, Inc. LaVie / NEC VERSA notebook
NEC Information Systems, Inc. POWERMATE desktop PC
NEC Information Systems, Inc. Valuestar / NEC POWERMATE hybrid computer
NEC (Division unknown) Car Stereos and Keyless Entry Systems
NEC accelerator mass spectrometry systems
PC Engine (TurboGrafx-16 in US) and all related hardware and successors; co-produced by Hudson Soft.
PC-FX
Defense products include:
J/TPS-102 Self-propelled ground-based early warning 3D radar (JGSDF)
Broadband multipurpose radio system (JGSDF)
Advanced Combat Infantry Equipment System [ACIES] (JSDF) - Major subcontractor
Howa rifle system (JSDF) - Major subcontractor as part of ACIES




1983 Announced the SX-1 and SX-2 supercomputers
1989 Introduction of SX-3
1994 First announcement of SX-4
1999 Delivery of SX-5
2002 Introduced SX-6
2002 Installation of the Earth Simulator, the world's fastest supercomputer from 2002 to 2004 reaching a speed of 35,600 gigaflops
2005 NEC SX-8 in production
2006 Announced the SX-8R
2007 Announced the SX-9
2011 First announcement of the NEC SX-9's successor 
2013 Announced the SX-ACE



Achievements of NEC include:
the discovery of single-walled carbon nanotubes by Sumio Iijima
the invention of the widely used MUX-scan design for test methodology (contrast with the IBM-developed level-sensitive scan design methodology)
the world's first demonstration of the one-qubit rotation gate in solid state devices.
As for mobile phones, NEC pioneered key technologies like color displays, 3G support, dual screens and camera modules.
As of 2009 NEC ranked consistently in the top four companies over the previous five years for the number of U.S. patents issued, averaging 1764 each year.



NEC was the main (title) sponsor of the Davis Cup competition until 2002, when BNP Paribas took over the sponsorship.
NEC sponsored the English football club Everton from 1985 to 1995. The 1995 FA Cup Final triumph was Everton's final game of the decade-long NEC sponsorship, and Danka took over as sponsors.
NEC signed a deal to sponsor the Sauber F1 Team from the 2011 season until the 2014 season.
NEC signed a new deal to sponsor the Sahara Force India F1 Team for the 2015 season.
In April 2013, NEC became the umbrella sponsor for the PGA Tour Latinoam rica, a third-tier men's professional golf tour.



These started as works teams, but over the years came to include professional players:
NEC Blue Rockets (men's volleyball)
NEC Red Rockets (women's volleyball)
NEC Green Rockets (men's rugby union)
NEC also used to own Montedio Yamagata of the football (soccer) J. League, but as of 2009 just sponsors them along with other local companies.


