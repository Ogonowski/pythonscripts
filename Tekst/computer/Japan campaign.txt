The Japan Campaign was a series of battles and engagements in and around the Japanese Home Islands, between Allied forces and the forces of Imperial Japan during the last stages of the Pacific Campaign of World War II. The Japan Campaign lasted from around June 1944 to August 1945.



Periodic air raids on Japan were the first attacks undertaken by Allied forces. In late 1944, these raids were followed by a major strategic bombing campaign against Tokyo and other military and civilian targets throughout Japan.



In early 1945, there were two major island battles:
The Battle of Iwo Jima (16 February to 26 March): Of approximately 21,000 Japanese defenders, only 216 survived.
The Battle of Okinawa (1 April to 21 June): Of approximately 100,000 Japanese defenders, only 24,455 survived.
There were also two naval battles:
Operation Ten-Go (7 April): Most of the Japanese vessels committed were lost.
The Battle of Tokyo Bay (22 and 23 July 1945): Most of the Japanese vessels committed were heavily damaged or lost.
Most importantly, their was a massive series of Air raids on Japan, most notably:
Bombing of Tokyo
Atomic bombing of Hiroshima
Atomic bombing of Nagasaki
Allied warships also bombarded several Japanese cities during July and August 1945. The air raids resulted in heavy damage to Japanese infrastructure and the deaths of hundreds of thousands of Japanese (mostly civilians), as well as the loss thousands of aircraft and flak guns. The Americans, in turn, only lost a few hundred aircraft (mostly bombers) to Japanese anti-air defenses and fighters.
The battles of Iwo Jima and Okinawa foretold what was to be expected when the Japanese Home Islands were attacked. Iwo Jima and Okinawa were lost only after extremely fierce resistance was overcome. In both cases, the Japanese refused to surrender and there were few survivors. While Japanese losses were extremely high, Allied forces paid dearly to take both islands.
Naval operations included a suicidal Japanese counteroffensive on 7 April 1945 (Operation Ten-Go), to relieve Okinawa and an Allied campaign to place air and submarine-delivered mines in Japanese shipping lanes. This was illustrated by the naval surface interdiction of Tokyo Bay in July 1945.




World War II ended with the surrender of Japan after the atomic bombings of Hiroshima and Nagasaki. Before those two attacks, Japan was unwilling to surrender. The firebombing of Japanese cities resulted in thousands of deaths but did not move the government towards surrender. The Japanese government was clearly prepared to fight an Allied invasion of the home islands as fiercely as they had defended Iwo Jima and fought on the Japanese home island of Okinawa.
The Japan Campaign was intended to provide staging areas and preparation for a possible Allied invasion of Japan and to support Allied air and naval campaigns against the Japanese mainland. Japan still had a homeland army of about two million soldiers and sufficient resources to cripple an Allied invasion. Consequently, had that invasion been necessary, it most likely would have resulted in a much higher death toll for both sides.
The bombings of Hiroshima and Nagasaki may have saved many lives on both sides by causing the surrender of Japan before an invasion of the Japanese mainland was carried out. Estimates made at the time ran as high as 7,000,000 Japanese civilian and military casualties and as many as 500,000 American military casualties. To this day, there is an ongoing debate over the necessity of the atomic bombings and their results.


