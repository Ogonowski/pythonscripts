Feeder cattle are steers (castrated males) or heifers (females) mature enough to be placed in a feedlot where they will be fattened prior to slaughter. Feeder calves are less than 1 year old; feeder yearlings are between 1 and 2 years old. Both types are often produced in a cow-calf operation.



 This article incorporates public domain material from the Congressional Research Service document "Report for Congress: Agriculture: A Glossary of Terms, Programs, and Laws, 2005 Edition" by Jasper Womach.