Windows Fundamentals for Legacy PCs ("WinFLP") is a thin client operating system from Microsoft, based on Windows XP Embedded, but optimized for older, less powerful hardware. It was released on July 8, 2006. Windows Fundamentals for Legacy PCs is not marketed as a full-fledged general purpose operating system, although it is functionally able to perform most of the tasks generally associated with one. It includes only certain functionality for local workloads such as security, management, document viewing related tasks and the .NET Framework. It is designed to work as a client server solution with RDP clients or other third party clients such as Citrix ICA.



WinFLP was originally announced with the code name "Eiger" on 12 May 2005. ("M nch" was announced as a potential follow-up project at about the same time.) The name "Windows Fundamentals for Legacy PCs" appeared in a press release in September 2005, when it was introduced as "formerly code-named  Eiger " and described as "an exclusive benefit to SA [Microsoft Software Assurance] customers".
A Gartner evaluation from April 2006 stated that  The main purpose of Windows Fundamentals for Legacy PCs (WinFLP) is to allow users running old PCs to be able to replace unsupported Windows NT Workstation v.4, Windows 95 and Windows 98 with a supported release of Windows XP (or, eventually, a version based on Windows Vista). [...] Because WinFLP will have the ability to run some applications locally   including Internet Explorer, media players, Instant-Messaging clients, Java Virtual Machines, terminal emulators and ICA or Remote Desktop Protocol clients, but not any random application (including MS Office)   we believe WinFLP is better described as a "lean client" than a "thin client." 
The RTM version of Windows Fundamentals for Legacy PCs was released on July 8, 2006. The release was announced to the press on July 12, 2006.
In May 2011 Microsoft announced Windows Thin PC as the successor product.



Microsoft positions WinFLP as an operating system that provides basic computing services on older hardware, while still providing core management features of more recent Windows releases, such as Windows Firewall, Group Policy, Automatic Updates, and other management services. However, it is not considered to be a general-purpose OS by Microsoft.
Windows Fundamentals for Legacy PCs is a Windows XP Embedded derivative and, as such, is optimized for legacy PCs. It requires significantly fewer system resources than the fully featured Windows XP. It also features basic networking, extended peripheral support, DirectX, and the ability to launch the remote desktop clients from compact discs. In addition to local applications, it offers support for those hosted on a remote server using Remote Desktop. It can be installed on a local hard drive, or configured to run on a diskless workstation.
The installer for Windows Fundamentals for Legacy PCs uses a modern GUI based install process, similar to the one in Windows Vista.



In addition to giving better performance on older machines, the reduced number of files increases boot speed, and the reduced number of services also improves security and responsiveness. People with old hardware unable to run an updated version of Windows XP usefully have the opportunity to use a more secure version of Windows than the older, unpatched OSes.



Despite being optimized for older PCs, the hardware requirements are similar to Windows XP, although it is faster running on slower clock speeds than Windows XP.



Windows Fundamentals for Legacy PCs has a smaller feature set than Windows XP. For example, WinFLP does not include Paint, Outlook Express and Windows games such as Solitaire. Another limitation is the absence of the Compatibility tab in the Properties... dialog box for executable files.
Internet Explorer 8 (and 7) can be installed, but a hotfix is required for auto-complete to work in these newer versions of the browser.



Windows Fundamentals for Legacy PCs is exclusively available to Microsoft Software Assurance customers, as it is designed to be an inexpensive upgrade option for corporations that have a number of Windows 9x computers, but lack the hardware necessary to support the latest Windows. It is not available through retail or OEM channels.
On October 7, 2008, Service Pack 3 for Windows Embedded for Point of Service and Windows Fundamentals for Legacy PCs was made available.
On April 18, 2013, Service Pack 3 for Windows Fundamentals for Legacy PCs (version 3) was made available.
The Microsoft marketing pages for Windows Fundamentals now redirect to those of Windows Thin PC, suggesting that Windows Fundamentals is no longer available for any customers.
WinFLP has the same lifecycle policy as Windows XP; as such, its support lifespan ended on 8 April 2014.






Windows Fundamentals for Legacy PCs home page on Microsoft's official site (Archived)
A review
Choosing the right Virtual OS: Windows XP vs. Windows FLP
Fixing null.sys on WinFLP