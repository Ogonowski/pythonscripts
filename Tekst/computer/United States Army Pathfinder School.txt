The mission of the United States Army Pathfinder School is to train personnel in the U.S. Army and its sister services in a three week course, during which the candidate pathfinder learns how to navigate dismounted, establish and operate a day / night helicopter landing zone, establish and operate day / night parachute drop zones (DZs), including computed air release system (CARP) DZs, ground marked release system (GMRS) DZs and Army verbally initiated release system (VIRS) DZs, conduct sling load operations, provide air traffic control (ATC) and navigational assistance to rotary wing (RW) and fixed wing (FW) airborne operations. All training and airborne operations will be conducted in accordance with FM 3-21.220 (Static Line Parachuting Techniques and Training) and FM 3-21.38 (Pathfinder Operations).
The U.S. Army Pathfinder School is operated under the auspices of the 1st Battalion (Airborne), 507th Infantry Regiment. The 1-507th IR also oversees the doctrine and operation of the U.S. Army Basic Airborne Course and the U.S. Army Jumpmaster School.



The Pathfinder's distinctive winged torch symbol comes from ancient Greece where runners opened up the Olympic Games, bearing the Olympic Torch. The Pathfinder torch signifies the pathfinder's function to "Light the Way" for airborne forces following behind. The red backing worn on original Pathfinder Badge was changed to black in 1964 to signify night operations. Pathfinders wear the full-color winged torch on their Army Service Uniforms (ASUs) and a subdued version on their Army Combat Uniforms (ACUs).




The modern U.S. Army Pathfinders are an elite force making up less than 1% of the total Army. Their primary mission is to infiltrate areas and set up parachute drop zones and helicopter landing zones for airborne and air assault missions.
The Pathfinders were created in World War II when American paratrooper units needed a way to mark areas and guide aircraft to a specific spot. Early airborne operations resulted in scattered drops up to 7 miles from the target. Darkness and inclement weather made it extremely difficult for aircraft to find the drop zone. The 509th Parachute Infantry Battalion and 504th Parachute Infantry Regiment, 82nd Airborne Division were working on an idea they had learned from the British. An elite force would go in prior to the main assault with visual and electronic signaling devices to guide aircraft to the drop zone and gliders to their landing zones. Their first use in combat was 13 September 1943 during combat jumps into Italy.
WWII-era Pathfinders are most remembered for their heroic jump into Normandy during the invasion of 1944 on June 6th, when they led the way for Allied forces into Europe. They were employed throughout Southern France, the Netherlands, Belgium, Germany in the course of Allied airborne operations until the end of the war in Europe. They were also used in the Pacific theater with the 11th Airborne Division during the liberation of the Philippines.
The Korean War saw limited use of the Pathfinders by the 187th Airborne Regimental Combat Team during two combat jumps and operations. The Vietnam War saw the largest use of Pathfinders due to the developments of helicopter insertion and resupply which were pioneered by the 11th Air Assault Division (Test). Nearly every Army aviation battalion had a Pathfinder detachment and deployed them on nearly every mission. Setting up helicopter landing zones, medevacs, resupply points, and sling loads in full view of the enemy displayed great bravery and coolness under fire. The most successful missions were those when Pathfinders were in control; however, due to the small size of their units, very little recognition was given to the Pathfinders.
After the Vietnam War pathfinders were with the major Airborne units and various combat aviation battalions/groups. They also saw a growth in Army National Guard and Army Reserve Pathfinder platoons during the 1970s and 1980s. Many conducted joint task force missions in Latin America, Europe, and the Middle East.
In the late 1980s through 1990 the Army started disbanding its pathfinder units in the belief those skills could be learned by regular troops attending Air Assault School and by individuals within the unit who were pathfinder qualified. Operations during the Panama invasion and the Gulf War showed that Pathfinders were an important factor in successful airborne operations and the Army needed more of them. The 101st Airborne Division (Air Assault), which had retained a pathfinder unit during and after the Vietnam War, expanded its existing company and raised a second in 2005 by converting its long range surveillance detachment (LRSD) into another pathfinder company, giving each of its two aviation brigades a company. The 82nd Airborne Division followed suit by converted its LRSD to a pathfinder company under the 2d Battalion, 82nd Aviation Regiment. Additionally, the 10th Mountain Division (Light Infantry) at Fort Drum, New York, and the 25th Infantry Division in Hawaii have formed provisional pathfinder companies (e.g., they are not reflected in the units' tables of organization and equipment) and conduct combat operations in Iraq and Afghanistan. The pathfinder units today still live by the motto of "First In-Last Out" and proudly wear the Pathfinder Torch.
Course Prerequisites:
All applicants must have passed a valid physical examination within five years, have a minimum profile of 11121, have no speech impediment, have passed the APFT within the last six months and meet the Army height and weight standards IAW AR 600-9.
Physicals for airborne qualified personnel must indicate  cleared to participate in airborne operations. 
Applicants must have six months of service remaining on active duty upon completion of the course.
Officers: Active Army, Reserve, or National Guard Officers (2LT-CPT) assigned to a billet documented with the Skill Identifier (SI)  5Q  indicating a requirement to possess Pathfinder skills in the most recent Personnel Management Authorization Document (PMAD) or Updated Authorization document (UAD). Attendance is restricted to Officers in the following Control Branches: IN, AR, EN, MI, QM (AOC 90A Only, or LG.)
Enlisted MOS: Active Army, Reserve or national Guard Soldiers in the grades of E-3 through E-7 in the following MOS are eligible to attend: 11B, 11C, 15Q, 19D, 88M, 92R (Skill levels 3 and 4 only, and 92Y (skill levels 1 through 3). Enlisted applicants must have a GT score of 110 or above.
Other Services: Active and Reserve Marine Corps Officers in the grades of 01-03 and enlisted personnel in the grades of E-4 through E-7 in logistics and combat arms may attend. Active and Reserve Air Force enlisted personnel serving as combat control team/forward air controllers in the grades of E5 thought E7 may attend.
Foreign Students: This course is releasable to military students from foreign countries on a case by case basis. Foreign countries desiring to place students in this course must meet one or more of the following criteria: (1) Have a signed Letter of Intent (2) Have a waiver from HQDA (3) Have USG release for training. Units sponsoring Foreign Students must ensure they meet all course prerequisites prior to reporting for Pathfinder training.






Students will in-process and receive pathfinder orientation. Students will be taught the fundamentals of air traffic control (ATC), including ground to air map marking. Pathfinder students will be taught proper MEDEVAC procedures and be capable of calling in a 9-line MEDEVAC request. Students will be taught the proper application of close air support (CAS) and learn how to implement it in the ground unit commander's tactical plan. The student will be taught the fundamentals of sling load operations and be certified to rig and inspect sling loads. Students will be taught how to plan and establish helicopter landing zones (HLZ) for day and nighttime operations.



Students will be taught how to establish and operate HLZ/PZs. Students will be taught to establish day/night DZs (CARP, GMRP, and VIRS) for the insertion of personnel and equipment. Students will understand the 8 selection factors for selecting a drop zone and the duties and responsibilities of the drop zone support team leader (DZSTL). Students will complete the Basic Airborne Refresher (if applicable) and will be taught how to perform the duties of the GTA during a VIRS drop.



Students will gain technical competence on the static load/unload procedures for a UH-60 and will be able to perform the duties in every position of the sling load hook-up team. Students will demonstrate the knowledge they have been taught during the course during the FTX, during which they are evaluated as a team leader/assistant team leader and ground to air/internal net recorder. Students will demonstrate proficiency in all areas of pathfinder operations and meet all graduation requirements.




Graduates from the US Army Pathfinder School are awarded the Pathfinder Badge. It is important to note that in addition to all the requirements for completing the course, there is an additional requirement to be awarded the Pathfinder Badge: the candidate Pathfinder must view  a suspended object falling from the sky  (i.e. a door bundle, jumper, heavy equipment, etc.).



Col. Robert L. Howard Award
The Col. Robert L. Howard Award is awarded to the Distinguished Honor Graduate (DHG) of the Pathfinder class will be awarded to graduate with the highest overall grade point average with first time "gos" in every event.
Instructor of the Cycle
The title of Instructor of the Cycle will be awarded to the instructor that the students and Instructors vote had the greatest positive impact throughout the course of training. The selection will be approved by the HHC 1-507th company commander and first sergeant.






Instructors at the U.S. Army Pathfinder School are the famed and feared "Black Hats," named for the black baseball caps they wear as a part of their garrison uniform. The Black Hat is a symbol of expertise, awarded to Airborne, Jumpmaster and Pathfinder instructors who are certified to teach others how to properly conduct airborne operations.



Pathfinders students are drawn primarily from the U.S. Army, but its sister services send students as well. For all prospective students, an assignment in a billet requiring pathfinder skills is generally required. In the Army, prospective students would most likely be assigned to pathfinder units, like those found in the 101st, 10th Mountain and 82nd Airborne. In the Navy, Air Force and Marine Corps, prospective students would most likely be assigned to a unit conducting drop zone operations, helicopter operations or special operations units.



Pathfinder
1st Battalion (Airborne), 507th Infantry Regiment
U.S. Army Basic Airborne Course
U.S. Army Jumpmaster School
U.S. Army Command Exhibition Parachute Team - Silver Wings



FM 3-21.38 (Pathfinder Operations)
FM 3-21.220 (Static Line Parachuting Techniques and Training)
Basic Airborne Companies Standard Operating Procedures (BACSOP)
HHC/1-507 Parachute Infantry Regiment SOP (HHCSOP), dated October 2011
^ Fogarty II, Sir Robert J. "Army Pathfinders First In...Last Out." Army Pathfinders: First In, Last Out. National Pathfinder Association, 25 May 2010. Web. 25 Oct. 2011. <http://armypathfinders.com/history.html>.



US Army Pathfinder School Official Website [1]
Fort Benning Official Website [2]
1st Battalion, 507th Parachute Infantry Regiment Official Website [3]
Incoming Pathfinder Student Information Website [4]
U.S. Army Official Homepage [5]
Lineage & Honors for the 1st Battalion (Airborne), 507th Infantry Regiment [6]
Distinctive Unit Insignia and Coat of Arms of the 507th Infantry Regiment [7]
Beret Flash of the 1st Battalion, 507th Parachute Infantry Regiment[8]