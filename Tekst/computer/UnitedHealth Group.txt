UnitedHealth Group Inc. is a diversified managed health care company headquartered in Minnetonka, Minnesota, U.S. It is No. 14 on the Fortune 500. UnitedHealth Group offers a spectrum of products and services through two operating businesses: UnitedHealthcare and Optum. Through its family of subsidiaries and divisions, UnitedHealth Group serves approximately 70 million individuals throughout the United States. In 2015, the company reported net income of $8.4 billion.



UnitedHealth Group is the parent of UnitedHealthcare, the largest single health carrier in the United States. It was created in 1977, as UnitedHealthCare Corporation (it was renamed in 1998), but traces its origin to a firm it acquired in 1977, Charter Med Incorporated, which was founded in 1974. More informally, it has its origins in the development of the HMO as a model for organized health care and in the work of spokesman Paul Ellwood.
In 1979, it introduced the first network-based health plan for seniors. In 1984, it became a publicly traded company.
In 2011, J.D. Power and Associates gave UnitedHealthcare the highest employer satisfaction rating for self-insured health plans. In a 2010 insurance industry publication, Business Insurance (magazine), UnitedHealthcare was named "Readers Choice" winner in 2010 for "Best health plan provider". In contrast, a 2010 survey of hospital executives who have dealt with the company, United received a 65% unfavorable rating. While this marks a 33% improvement over the prior year's survey, UnitedHealthcare still ranked last among all listed.



In 1974, Richard Tyagi Burke founded Charter Med Incorporated, a Minnetonka, Minnesota-based privately held company. In 1977, UnitedHealthCare Corporation was created to reorganize the company and become the parent company of Charter Med. UnitedHealthCare s charter was to manage the newly created Physicians Health Plan of Minnesota, an early health management organization.
In 1994, UnitedHealth sold its pharmacy benefit manager, Diversified Pharmaceutical Services, to SmithKline Beecham for $2.3 billion. Also in 1994, UnitedHealth acquired Ramsey-HMO, a Florida insurer.
In 1995, the company acquired The MetraHealth Companies Inc. for $1.65 billion. MetraHealth was a privately held company formed by combining the group health care operations of The Travelers Insurance Company and Metropolitan Life Insurance Company also known as MetLife.
In 1998, UnitedHealth Group acquired HealthPartners of Arizona, operator of Arizona's largest AHCCCS provider.
In 2001, EverCare, a UnitedHealth Group subsidiary merged with LifeMark Health Plans 
In 2002, UnitedHealth Group acquired Medicaid insurance company AmeriChoice.
In 2003, UnitedHealth Group acquired Mid Atlantic Medical Services, an insurer serving Maryland, Washington D.C., Virginia, Delaware and West Virginia. Also in 2003, UnitedHealth Group acquired Golden Rule Financial, a provider of health savings accounts.
In April 2004, UnitedHealth Group acquired Touchpoint Health Plan, a Wisconsin health plan. In July 2004, UnitedHealth Group acquired Oxford Health Plans and all of UnitedHealthcare's New York-based small group contracts are now Oxford Health Plans products.
In December 2005, the company received final regulatory approval for its $9.2 billion purchase of PacifiCare Health Systems. It agreed to divest parts of PacifiCare's commercial health insurance business in Tucson, Arizona and Boulder, Colorado to satisfy antitrust regulator concerns, and also agreed to end its network access agreement with Blue Shield of California. The Tucson business was sold to Cigna. Also in December 2005, UnitedHealth Group acquired John Deere Health Care.
In March 2007, UnitedHealth Group signed a definitive agreement to acquire Sierra Health Services Inc. for $2.6 billion. Sierra provided health benefits and services to 310,000 members in Nevada and another 320,000 people in senior and government programs throughout the United States. As a condition of approval from the Department of Justice, UnitedHealth Group was required to divest its individual SecureHorizons Medicare Advantage HMO plans in Clark and Nye Counties, which represent approximately 25,000 members. UnitedHealth Group reached an agreement to transition these members to Humana Inc., subject to customary closing conditions.
Also in 2007 UnitedHealth Group acquired The Lewin Group is a policy research and consulting firm.
In June 2009, Ingenix, a UHG subsidiary, acquired AIM Healthcare. AIM is a data mining and insurance claim auditing service in the US.
In July 2009, UnitedHealth Group Inc. agreed to acquire Health Net Inc.'s (HNT) Northeast licensed subsidiaries for up to $570 million in payments spread out over a two-year period.
In July 2010, Ingenix acquired Picis Inc. Picis is a provider of health information solutions for the high-acuity areas of hospitals. 
In 2011, Logistics Health, Inc., of La Crosse, Wisconsin, was acquired by OptumHealth In September, 2014 the office buildings where LHI is based were sold to UnitedHealth Group for $45 million.
In Nov 2011, UnitedHealth Group Inc. signed to acquire XLHealth Corporation.
In Feb 2012, UnitedHealth Group Inc. completed the XLHealth acquisition. XLHealth Corporation is a sponsor of Medicare Advantage health plans with a primary focus on medicare recipients with special needs such as those with chronic illness and those eligible for Medicaid ( dual eligibles ). 
In October 2012, UnitedHealth Group and Amil Participa es S.A., one of the biggest Brazilian health insurance companies, completed the first phase of their merger. 
In March 2015, it was announced that CatamaranRx will be acquired by OptumRx (A UnitedHealth Group company).



Beginning in 2011 and continuing into 2012 UnitedHealth Group implemented an enterprise reorganization. This coincided with retiring brands adopted following the various acquisitions of prior years and adopting the unified brand of UnitedHealthcare. The firm decided to refocus its UK business to concentrate solely on commissioning support under the name of Optum, and exploit the opportunities presented by the United Kingdom Government's NHS reforms in 2011 and sold its 6 primary care practices.



Prior to January 2011 UnitedHealth Group had the following operating segments:
ACME (aka: Uniprise) - Employer Sponsored Coverage.
Ovations (Senior and Retiree Services / EverCare) - Coverage for Medicare and Medicaid Recipients.
AmeriChoice - Coverage for Medicare, Medicaid Recipients.
Oxford - Coverage written on Oxford licenses.
Golden Rule - Individual Plans
Spectera - Vision
DBP (Dental Benefit Providers) - Dental
UBH (United Behavioral Health) - Behavioral Health and Substance Abuse
URN (United Resource Network) - Transplant Care Management
Ingenix - Health Data Analytics.
PDV (Pacificare Dental & Vision) - Dental & Vision for members under PacifiCare license.
Prescription Solutions - Pharmacy Benefit Manager (Initially only support Ovations business)
UnitedHealth Group is also the parent company of Optum. Optum is an information and technology-enabled health services business.



UnitedHealthcare Employer & Individual - Health programs not connected with a Government entity. (Medicare, Medicaid, Military)
UnitedHealthcare Medicare & Retirement - Health programs for Medicare recipients and group retirees. (However, some group retiree plans are still managed by the Employer & Individual segment)
UnitedHealthcare Community & State - Health programs for Medicaid recipients and regional assistance programs.
UnitedHealthcare Military & Veterans - Health programs administered under a Tri-Care contract.



The role of the Optum brand is expanded to include four divisions:
Optum Technology - the innovation arm of Optum and UnitedHealth Group; this is a cross-functional department across OptumInsight, OptumRx, and OptumHealth.
OptumInsight - Health Data Analytics
OptumRx - Pharmacy Benefit Manager (now includes most UHG membership)
OptumHealth - Healthcare delivery services and support including OptumCare, home to the organizations acquired staff model and IPA model physician groups, and ancillary services (Behavioral Health and Substance Abuse, Dental, Vision, Transplant Management)
See also Optum for the company's operation in the UK.



In 2010, UnitedHealth Group spent more than $1.8 million on lobbying activities to work to achieve favorable legislation, and hired seven different lobbying firms to work on its behalf. In addition, its corporate political action committee or PAC, called United for Health, spent an additional $1 million on lobbying activities in 2010.
QSSI, a subsidiary of UnitedHealth Group, is one of the 55 contractors hired by United States Department of Health and Human Services to work on the HealthCare.gov web site



Through 2010 and into 2011, UnitedHealth senior executives have been meeting monthly with executives of leading health insurers to limit the effect of the health care reform law.



In 2006, the Securities and Exchange Commission (SEC) began investigating the conduct of UnitedHealth Group's management and directors, for backdating of stock options. Investigations were also begun by the Internal Revenue Service and prosecutors in the U.S. attorney's office for the Southern District of New York, who subpoenaed documents from the company. The investigations came to light after a series of probing stories in the Wall Street Journal in May 2006, discussing apparent backdating of hundreds of millions of dollars' worth of stock options by UHC management. The backdating apparently occurred with the knowledge and approval of the directors, according to the Journal. Major shareholders have filed lawsuits accusing former New Jersey governor Thomas Kean and UHC's other directors of failing in their fiduciary duty. On October 15, 2006, CEO William W. McGuire was forced to resign, and relinquish hundreds of millions of dollars in stock options. On December 6, 2007, the SEC announced a settlement under which McGuire will repay $468 million, as a partial settlement of the backdating prosecution. Legal actions filed by the SEC against UnitedHealth Group itself are still pending.
In June 2006, the American Chiropractic Association filed a national class action lawsuit against the American Chiropractic Network (ACN), which is owned by UnitedHealth Group and administers chiropractic benefits, and against UnitedHealth Group itself, for alleged practices in violation of the federal Racketeer Influenced and Corrupt Organizations Act (RICO).



In February 2008, New York State Attorney General Andrew M. Cuomo announced an industry-wide investigation into a scheme by health insurers to defraud consumers by manipulating reasonable and customary rates. The announcement included a statement that Cuomo intended "to file suit against Ingenix, Inc., its parent UnitedHealth Group, and three additional subsidiaries." Cuomo asserted that his investigation found that rates found in a database of health care charges maintained by Ingenix were lower than what he determined was the actual cost of certain medical expenses. Cuomo said this inappropriately allowed health insurance companies to deny a portion of provider claims, thereby pushing costs down to members. 
On January 13, 2009, UnitedHealth Group and Ingenix announced an agreement with the New York State attorney settling the probe into the independence of the health pricing database. Under the settlement, UnitedHealth Group and Ingenix would pay $50 million to finance a new, non-profit entity that would develop a new health care pricing database. Ingenix would discontinue its medical pricing databases when the new entity makes its product available. The company acknowledged the appearance of a conflict of interest, but admitted no wrongdoing.
On January 15, 2009, UnitedHealth Group announced a $350 million settlement of three class action lawsuits filed in Federal court by the American Medical Association, UnitedHealth Group members, healthcare providers, and state medical societies for not paying out-of-network benefits. This settlement came two days after a similar settlement with Cuomo.
On October 27, 2009, Cuomo announced the creation of FAIR Health, the independent, non-profit organization that will develop a nationwide database for consumer reimbursement, as well as a website where consumers will be able to compare prices before they choose doctors. To fund FAIR Health, the Attorney General's office secured nearly $100 million from insurers such as Aetna, UnitedHealth Group, and Anthem.



In 2006, the SEC began investigating the conduct of UnitedHealth Group's management and directors, including Dr. McGuire, as did the Internal Revenue Service and prosecutors in the U.S. attorney's office for the Southern District of New York, who have subpoenaed documents from the company.
The investigations came to light after a series of probing stories in the Wall Street Journal in March 2006, discussing the apparent backdating of hundreds of millions of dollars' worth of stock options in a process called options backdating by UnitedHealth Group management.  The backdating apparently occurred with the knowledge and approval of the directors, according to the Journal. Major shareholders have filed lawsuits accusing former New Jersey governor Thomas Kean and UnitedHealth Group's other directors of failing in their fiduciary duty.



On October 15, 2006, it was announced that Dr. McGuire would step down immediately as chairman and director of UnitedHealth Group, and step down as CEO on December 1, 2006, due to his involvement in the employee stock options scandal. Simultaneously, it was announced that he would be replaced as CEO by Stephen Hemsley, who has served as President and COO and is a member of the board of directors. McGuire's exit compensation from UnitedHealth, expected to be around $1.1 billion, would be the largest golden parachute in the history of corporate America.
McGuire's compensation became controversial again on May 21, 2009, when Elizabeth Edwards, speaking on The Daily Show, used it to support her argument for a public alternative to commercial insurance. Edwards stressed the importance of restoring competition in health insurance markets noting that at one point, "the President of UnitedHealth made so much money, that one of every $700 that was spent in this country on health care went to pay him."
Estimates of McGuire's 2005 compensation range from $59,625,444  to $124.8 million, and the revenue of UnitedHealth Group was then $71 billion. It has therefore been suggested that Mrs. Edwards may have meant to say that one of every $700 that was spent on UnitedHealth Group premiums went to pay McGuire.



On December 6, 2007, the SEC announced a settlement under which McGuire was to repay $468 million, including a $7 million civil penalty, as a partial settlement of the backdating prosecution. He was also barred from serving as an officer or director of a public company for ten years. This was the first time in which the little-used "clawback" provision under the Sarbanes-Oxley Act was used against an individual by the SEC. The SEC continued its investigations even after it in 2008 settled legal actions against both UnitedHealth Group itself and its former general counsel.



The Lewin Group is a policy research and consulting firm that provides economic analysis of health care and human services issues and policies. The organization has existed for about 40 years and has maintained a nonpartisan reputation through its many ownership changes that have occurred over that time. The Lewin Group was purchased in 2007 by Ingenix, a subsidiary of UnitedHealth Group, but alleges editorial and analytical "independence" from UnitedHealth Group, its parent company. The Lewin Group discloses its ownership in its reports and on its web site. While the Lewin Group does not advocate for or against any legislation, both Democratic and Republican politicians frequently cite the firm's studies to argue for and against various U.S. healthcare reform proposals. For example, Democratic Senator Ron Wyden uses Lewin Group estimates to cite the feasibility of his Healthy Americans Act. Representative Eric Cantor, the House Republican Whip, has referred to the organization as "the nonpartisan Lewin Group" in arguing against government-funded health insurance proposals. Recently, several Lewin studies have been used to argue both for and against the inclusion of a public option in national health reform. Lewin clients who often cite its findings include The Commonwealth Fund which recently held up a Lewin study it commissioned to advocate for a public plan. The report showed that legislative proposals would achieve nearly universal coverage and "estimated that a public plan could offer small businesses insurance that is at least 9 percent cheaper than current small-business policies." However, The Lewin Group has acknowledged that clients sometimes choose not to publicize the results of studies they have commissioned. Indeed, Lewin Group Vice President John Sheils told the Washington Post "sometimes studies come out that don't show exactly what the client wants to see. And in those instances, they have [the] option to bury the study."



UnitedHealth Group has two foundations, the UnitedHealth Foundation and UnitedHealthcare Children s Foundation which were formed in 1999. Since established by UnitedHealth Group in 1999 as a not for profit private foundation, the UnitedHealth Foundation has committed more than $170 million to improve health and health care.


