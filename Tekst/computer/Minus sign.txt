The plus and minus signs (+ and  ) are mathematical symbols used to represent the notions of positive and negative as well as the operations of addition and subtraction. Their use has been extended to many other meanings, more or less analogous. Plus and minus are Latin terms meaning "more" and "less", respectively.



Though the signs now seem as familiar as the alphabet or the Hindu-Arabic numerals, they are not of great antiquity. The Egyptian hieroglyphic sign for addition, for example, resembled a pair of legs walking in the direction in which the text was written (Egyptian could be written either from right to left or left to right), with the reverse sign indicating subtraction:
Nicole Oresme's manuscripts from the 14th century show what may be one of the earliest uses of the plus sign "+".
In Europe in the early 15th century the letters "P" and "M" were generally used. The symbols (P with line p  for pi , i.e., plus, and M with line m  for meno, i.e., minus) appeared for the first time in Luca Pacioli s mathematics compendium, Summa de arithmetica, geometria, proportioni et proportionalit , first printed and published in Venice in 1494. The + is a simplification of the Latin "et" (comparable to the ampersand &). The   may be derived from a tilde written over m when used to indicate subtraction; or it may come from a shorthand version of the letter m itself. In his 1489 treatise Johannes Widmann referred to the symbols   and + as minus and mer (Modern German mehr; "more"): "was   ist, das ist minus, und das + ist das mer".
A book published by Henricus Grammateus in 1518 makes another early use of + and   for addition and subtraction.
Robert Recorde, the designer of the equals sign, introduced plus and minus to Britain in 1557 in The Whetstone of Witte: "There be other 2 signes in often use of which the first is made thus + and betokeneth more: the other is thus made   and betokeneth lesse."




The plus sign (+) is a binary operator that indicates addition, as in 2 + 3 = 5. It can also serve as a unary operator that leaves its operand unchanged (+x means the same as x). This notation may be used when it is desired to emphasize the positiveness of a number, especially when contrasting with the negative (+5 versus  5).
The plus sign can also indicate many other operations, depending on the mathematical system under consideration. Many algebraic structures have some operation which is called, or is equivalent to, addition. It is conventional to use the plus sign to only denote commutative operations. Moreover, the symbolism has been extended to very different operations; plus can also mean:
exclusive or (usually written  ): 1 + 1 = 0, 1 + 0 = 1
logical disjunction (usually written  ): 1 + 1 = 1, 1 + 0 = 1



The minus sign ( ) has three main uses in mathematics:
The subtraction operator: A binary operator to indicate the operation of subtraction, as in 5   3 = 2. Subtraction is the inverse of addition.
Directly in front of a number and when it is not a subtraction operator it means a negative number. For instance  5 is negative 5.
A unary operator that acts as an instruction to replace the operand by its additive inverse. For example, if x is 3, then  x is  3, but if x is  3, then  x is 3. Similarly,  ( 2) is equal to 2. The above is a special case of this.
All three uses can be referred to as "minus" in everyday speech. In most English-speaking countries,  5 (for example) is normally pronounced "minus five", but in modern US usage it is instead sometimes pronounced "negative five"; here, "minus" may be used by speakers born before 1950, and is still popular in some contexts, but "negative" is usually taught as the only correct reading. Further, some textbooks in the United States encourage  x to be read as "the opposite of x" or "the additive inverse of x" to avoid giving the impression that  x is necessarily negative.
In some contexts, different glyphs are used for these meanings; for instance in the computer language APL and the expression language used by TI graphing calculators (definitely at least the early models including the TI-81 and TI-82) a raised minus sign is used in negative numbers (as in 2   5 shows  3), but such usage is uncommon.
In mathematics and most programming languages, the rules for the order of operations mean that  52 is equal to  25: Powers bind more strongly than the unary minus, which binds more strongly than multiplication or division. However, in some programming languages and Microsoft Excel in particular, unary operators bind strongest, so in those cases  5^2 is 25 but 0 5^2 is  25.



Some elementary teachers use raised plus and minus signs before numbers to show they are positive or negative numbers. For example, subtracting  5 from 3 might be read as positive three take away negative 5 and be shown as
3    5 becomes 3 + 5 = 8,
or even as
+3    5 becomes +3 + +5 which is +8.



In grading systems (such as examination marks), the plus sign indicates a grade one level higher and the minus sign a grade lower. For example, B  ("B minus") is one grade lower than B. Sometimes this is extended to two plus or minus signs; for example A++ is two grades higher than A.
Positive and negative are sometimes abbreviated as +ve and  ve.
In mathematics the one-sided limit x a+ means x approaches a from the right, and x a  means x approaches a from the left. For example, when calculating what x 1 is when x approaches 0, because x 1 +  when x 0+ but x 1  when x 0 .
Blood types are often qualified with a plus or minus to indicate the presence or absence of the Rh factor; for instance, A+ means A-type blood with the Rh factor present, while B  means B-type blood with the Rh factor absent.
In music, augmented chords are symbolized with a plus sign, although this practice is not universal as there are other methods for spelling those chords. For example, "C+" is read "C augmented chord". Also used as superscript.



As well as the normal mathematical usage plus and minus may be used for a number of other purposes in computing.
Plus and minus signs are often used in tree view on a computer screen to show if a folder is collapsed or not.
In some programming languages concatenation of strings is written: "a" + "b" = "ab", although this usage is questioned by some for violating commutativity, a property addition is expected to have.
In most programming languages, subtraction and negation are indicated with the ASCII hyphen-minus character, -. In APL a raised minus sign (Unicode U+00AF) is used to denote a negative number, as in  3) and in J a negative number is denoted by an underscore, as in _5.
In C and some other computer programming languages, two plus signs indicate the increment operator and two minus signs a decrement. For example, x++ means "increment the value of x by one" and x-- means "decrement the value of x by one". By extension, "++" is sometimes used in computing terminology to signify an improvement, as in the name of the language C++.
There is no concept of negative zero in mathematics, but in computing  0 may have a separate representation from zero. In the IEEE floating-point standard 1/ 0 is negative infinity ( ) whereas 1/0 is positive infinity ( ).



In chemistry, the minus sign (rather than an en dash) is used for a single covalent bond between two atoms, as in the skeletal formula.
Subscripted plus and minus signs are used as diacritics in the International Phonetic Alphabet to indicate advanced or retracted articulations of speech sounds.
The minus sign is also used as tone letter in the orthographies of Dan, Krumen, Karaboro, Mwan, Wan, Yaour , W , Nyabwa and Godi . The Unicode character used for the tone letter (U+02D7) is different from the mathematical minus sign.




The hyphen-minus sign (-) is the ASCII version of the minus sign, and doubles as a hyphen. It is usually shorter in length than the plus sign and sometimes at a different height. It can be used as a substitute for the true minus sign when the character set is limited to ASCII. Most programming languages and other computer readable languages do this, since ASCII is generally available as a subset of most character encodings, while U+2212 is a Unicode feature only.
There is a commercial minus sign ( ), which looks somewhat like an obelus, at U+2052 (HTML &#x2052;).




A Jewish tradition that dates from at least the 19th century is to write plus using a symbol like an inverted T. This practice was adopted into Israeli schools and is still commonplace today in elementary schools (including secular schools) but in fewer secondary schools. It is also used occasionally in books by religious authors, but most books for adults use the international symbol "+". The usual explanation for this practice is that it avoids the writing of a symbol "+" that looks like a Christian cross. Unicode has this symbol at position U+FB29   hebrew letter alternative plus sign.


