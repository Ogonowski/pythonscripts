Fort Belvoir / b lvw r/ is a United States Army installation and a census-designated place (CDP) in Fairfax County, Virginia, United States. Originally, it was the site of the Belvoir plantation. Today, Fort Belvoir is home to a number of important United States military organizations. With nearly twice as many workers as The Pentagon, Belvoir is the largest employer in Fairfax County.



The base was founded during World War I as Camp A. A. Humphreys, named for Andrew A. Humphreys. The post was renamed Fort Belvoir in the 1930s in recognition of the Belvoir plantation that once occupied the site, but the adjacent United States Army Corps of Engineers Humphreys Engineer Center retains part of the original namesake.

Fort Belvoir was initially the home of the Army Engineer School prior to its relocation in the 1980s to Fort Leonard Wood, in Missouri. It was also the home of the United States Army Engineer Research and Development Laboratory.
As a result of the 2005 Base Realignment and Closure Commission, Fort Belvoir had a substantial increase in the number of people stationed or employed there. All major Washington, DC-area National Geospatial-Intelligence Agency (NGA) facilities, including those in Bethesda, MD; Reston, VA; and Washington, DC were consolidated at a new facility, the NGA Campus East, situated on the former Engineer Proving Ground site. The cost of the new center was $2.4 billion.



Fort Belvoir serves as the headquarters for the Defense Logistics Agency, the Defense Acquisition University, the Defense Contract Audit Agency, the Defense Technical Information Center, the United States Army Intelligence and Security Command, the United States Army Military Intelligence Readiness Command, the Missile Defense Agency, the Defense Threat Reduction Agency, and the National Geospatial-Intelligence Agency, all agencies of the United States Department of Defense.
Fort Belvoir is home to the Virginia National Guard's 29th Infantry Division (Light) and elements of ten Army Major Commands; nineteen different agencies and direct reporting units of the Department of Army; eight elements of the United States Army Reserve and the Army National Guard; and twenty-six Department of Defense agencies. Also located here are the 249th Engineer Battalion (Prime Power), the Military District of Washington's 12th Aviation Battalion which provides rotary-wing movement to the DoD and Congress, a Marine Corps detachment, a United States Air Force activity, United States Army Audit Agency, and an agency from the Department of the Treasury. In addition, Fort Belvoir is home to National Reconnaissance Office's (NRO) Aerospace Data Facility, East (ADF-E).



The Fort Belvoir site was originally the home of William Fairfax, the cousin and land agent of Thomas Fairfax, 6th Lord Fairfax of Cameron the proprietor of the Northern Neck, which once stood on land now on the base. William Fairfax purchased the property in 1738 when his cousin arranged for him to be appointed customs agent (tax collector) for the Potomac River, and William erected an elegant brick mansion overlooking the river, moving in with his family in 1740. Lord Fairfax came to America in 1747 and stayed less than a year at the Belvoir estate before moving to Greenway Court. The Fairfax family lived at Belvoir for over 30 years, but eldest son (and heir) George William Fairfax sailed to England on business in 1773, never to return. The manor home was destroyed by fire in 1783.
The ruins of the Belvoir Mansion and the nearby Fairfax family grave site are listed on the National Register of Historic Places. William Fairfax served in the Royal Navy as a young man and all of his sons (except his eldest, George William) served in uniform, two of whom were killed in combat.




Fort Belvoir is also a census-designated place. Nearby CDPs are Mount Vernon, Virginia (northeast) and Franconia, Virginia (northwest). As of the census of 2000, there were 7,176 people, 1,904 households, and 1,867 families residing in the CDP. The population density was 314.1/km  (813.7/sq mi). There were 2,056 housing units at an average density of 90.0/km  (233.1/sq mi). The racial makeup of the CDP was 55.7% White, 31.8% African American, 1.7% Asian, 0.5% Native American, 0.9% Pacific Islander, 5.1% from other races, and 4.3% from two or more races. Hispanic or Latino of any race were 10.5% of the population.
There were 1,904 households out of which 84.3% had children under the age of 18 living with them, 84.3% were married couples living together, 11.5% had a female householder with no husband present, and 1.9% were non-families. 1.7% of all households were made up of individuals and none had someone living alone who was 65 years of age or older. The average household size was 3.66 and the average family size was 3.68.
In the CDP the population was spread out with 44.4% under the age of 18, 10.1% from 18 to 24, 41.3% from 25 to 44, 3.9% from 45 to 64, and 0.3% who were 65 years of age or older. The median age was 22 years. For every 100 females there were 101.1 males. For every 100 females age 18 and over, there were 96.9 males.
The median income for a household in the CDP was $39,592, and the median income for a family was $39,107. Males had a median income of $30,625 versus $25,817 for females. The per capita income for the CDP was $12,453. About 4.7% of families and 5.6% of the population were below the poverty line, including 6.5% of those under age 18 and none of those age 65 or over.






The climate in this area is characterized by hot, humid summers and generally mild to cool winters. According to the K ppen Climate Classification system, Fort Belvoir has a humid subtropical climate, abbreviated "Cfa" on climate maps.


