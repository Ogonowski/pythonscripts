The United States Army's Officer Candidate School (OCS), located at Fort Benning, Georgia, trains, assesses, and evaluates potential commissioned officers in the U.S. Army, U.S. Army Reserve, and some Army National Guard. Officer candidates are former enlisted members (E-4 to E9), Warrant Officers, inter-service transfers, or civilian college graduates who enlist for the "OCS Option" after they complete Basic Combat Training (BCT). The latter are often referred to as "college ops".



OCS is a rigorous 12-week course designed to train, assess, evaluate, and develop second lieutenants for the U.S. Army's sixteen basic branches. It is the only commissioning source that can be responsive to the U.S. Army's changing personnel requirements due to its short length, compared to other commissioning programs and their requirements. Completing OCS is one of several ways of becoming a U.S. Army commissioned officer. The other methods are:
Graduation from United States Military Academy (USMA) at West Point, NY
Graduation from United States Merchant Marine Academy (USMMA) at Kings Pont, NY 
Completing Reserve Officers' Training Corps (ROTC) offered at most civilian universities throughout the U.S.
State-level Officer Candidate Schools programmed by the Army National Guard at Regional Training Institutes (RTI), with curriculum identical to the federal OCS program.
Direct Commissioning normally is used for accessions of chaplains, medical professionals, and Judge Advocate General (JAG) lawyers. Currently, the U.S. Army Reserve is using this method in limited numbers for the basic branches as well.
Interservice transfer as a commissioned officer of another United States military branch.
Battlefield commissions, or meritorious commissions, though technically still provided for, have not been used by the US Army since the Vietnam War.
The U.S. Army Officer Candidate School is organizationally designated as 3rd Battalion, 11th Infantry Regiment, 199th Infantry Brigade. It was redesignated from the 3rd Battalion, 11th Infantry Regiment in June 2007. It is a subordinate unit of the Maneuver Center of Excellence (MCoE) also headquartered at Ft. Benning. As of July 2014 the battalion has five training companies and a Headquarters Company in operation, designated HHC, Alpha, Bravo, Charlie, Delta and Echo each of which can conduct one class at a time, with a maximum of 160 candidates being trained in each class. Generally, only Alpha thru Delta are used, but if there are sufficient numbers of students, Echo company will be opened-up as well. HHC serves as the "holding" company for brand new candidates going thru their in-processing or for injured candidates who are recuperating from their injuries. Those who recuperate from injury are often "recycled" into the next class (like the Army's Ranger School). Every three weeks a class graduates and another one is started.
The commander of the 3rd Battalion, 11th Infantry Regiment (OCS), 199th Infantry Brigade is Lieutenant Colonel Matthew P. Neumeyer and the Command Sergeant Major is Calvin Witherspoon.




Historically, OCS has provided the means by which the U.S. Army could generate large numbers of junior officers during periods of increasing personnel requirements, typically during wars. Prior to 1973, OCS was branch-specific, at one time there being eight separate schools; by 1964, the Army had consolidated OCS into two schools: Field Artillery OCS at Fort Sill, Oklahoma, and Infantry OCS at Fort Benning. The Vietnam war brought expansion of the OCS program, but it was short-lived. In 1973, OCS was made branch immaterial and was consolidated into two courses taught at Ft. Benning, and another at Fort McClellan, Alabama for female Officer Candidates; the course length was reduced to 14-weeks. In 1976, the OCS at Ft. Benning integrated females, and became the only OCS left in the active Army, with the closure of the WAC School. The term "90-day wonders", both as a pejorative and term of affection, has been intermittently applied to junior officers commissioned through OCS since World War II.



Officer Candidate School was first proposed in June 1938, as the Army began expanding in anticipation of hostilities when a plan for an officer-training program was submitted to the Chief of Infantry by Brigadier General Asa L. Singleton, Commandant of the Infantry School. No action was taken until July 1940, however, when Brig. Gen. Courtney Hodges, Assistant Commandant of the Infantry School, presented a revised plan to (then) Brig. Gen. Omar Bradley, Commandant of the Infantry School. In July 1941, the OCS stood up as the Infantry, Field Artillery, and Coastal Artillery Officer Candidate Schools, each respectively located at Fort Benning, Fort Sill, and Fort Monroe, Virginia.

In addition to the aforementioned programs, there were Officer Candidate Schools stood up for other branches, in particular the Signal Corps at Fort Monmouth, New Jersey. Due to the rapid creation of these programs because of wartime necessity, and then the rapid closures or restructuring soon after the end of the war, historical records were not always created or adequately maintained and little is known about some of these branch specific commissioning courses. The Signal Corps however has a full list of records going back to its very first class, which graduated 336 Officers on 30 September 1941. The records are maintained by the U.S. Army Signal Corps OCS Association, which is actively collecting and archiving the personal history of many of the over 27,000 Signal Corps OCS graduates that went through its WWII, Korean War and Vietnam War programs. A visit to its website will find a list of every U.S. Army Signal Corps OCS graduate, the date of their graduation, as well as all TAC Officers, training school CO's, and most enlisted men who served the Signal Corps' OCS training programs.
In addition to the Signal Corps, several other units have alumni organizations that have maintained informal records and preserved documentation of the courses.
On 27 September 1941, the first Infantry OCS class graduated 171 second lieutenants; 204 men started the 17-week course in July. Testament to the ability of OCS to produce new second lieutenants quickly can be found in War Department decision that ROTC could not fulfill the national demand for officers; so in May 1943, the advanced course in ROTC was suspended and basic course graduates were immediately sent to OCS so they could be commissioned sooner.
During the war, the Army's policy of racial segregation continued among enlisted members; Army training policy, however, provided that blacks and whites would train together in officer candidate schools (beginning in 1942). Officer Candidate School was the Army's first formal experiment with integration. Black and white candidates shared officer's quarters, with bunkmates assigned alphabetically, regardless of their race, and all of the candidates trained together. Despite this integrated training, in most instances, the graduates would go on to join racially segregated units.

General Bradley is credited with establishing the format, discipline, and code of honor still used in OCS today. Bradley emphasized rigorous training, strict discipline and efficient organization. These tenets remain the base values of today's Officer Candidate School. Between July 1941 and May 1947, over 100,000 candidates were enrolled in 448 Infantry OCS classes, of these approximately 67 percent completed the course to earn commissions. After World War II, Infantry OCS was transferred to Fort Riley, Kansas, as part of the Ground General School. Due to the post-war downsizing of the Army and the declining need for new Officers, all but Infantry OCS was closed. Finally, on 1 November 1947, it was deactivated. The final class graduated only 52 second lieutenants.
The Women's Army Auxiliary Corps (WAAC) was created by act of Congress on 14 May 1942, permitting them to serve, but not as Soldiers. At that time, women did not have military status and were not integrated into the Army. Their ranks, pay, and benefits were different than the Army, along with all administration. But, being a military organization that was modeled after, and parallel, to the Army, it required a way to train Officers; therefore it created its own WAAC OCS, which stood up on 20 July 1942 at Fort Des Moines, Iowa. The course was six-weeks long, its first class consisting of 440 candidates. Upon graduation, the women were commissioned as third officers (equivalent to a second lieutenant). It is worth noting, that among the first candidates were 40 black women. Initially, black women were segregated, but in keeping with Army policies, integrating officer training, and with pressure from the National Association for the Advancement of Colored People (NAACP), by November 1942, they were being trained in integrated units.
While the Infantry likes to claim that the Infantry OCS course developed by General Bradley is the precursor of the branch immaterial course taught at Fort Benning, the truth is more likely that the integration of the various Combat, Combat Support and Combat Service Support duties that Officers perform caused the various OCS training programs to be combined.




With the outbreak of the Korean War, and the Army's rapid expansion in response, the shortage of on-hand officers, and projected commissions, caused the Department of the Army to re-open Infantry OCS at Ft. Benning on 18 February 1951. The course was lengthened from 17 to 22 weeks, as a result of lessons learned from WWII; thus permitting more instruction in Infantry tactics. The Infantry Officer Candidate School became the 1st Officer Candidate Battalion, 2nd Student Regiment. The strength of OCS rapidly increased. As one of eight branch programs, Infantry OCS included as many as 29 companies with a class graduating every week. During the Korean War, OCS commissioned approximately 7,000 Infantry officers.
In April 1949, the U.S. Army established the Women's Army Corps Officer Candidate School at Fort Lee, Virginia. The WAC, an active component of the regular Army, descendant of the WAAC, operated this OCS for females seeking to enter the WAC Officer Corps. The "wash-out" rate was nearly identical to the men's programs, at roughly 37%, during its first four years; an alarming statistic to observers of both programs. By 1954 WAC OCS had been closed and merged with a commissioning program for female direct commissionees, due to the low numbers of women attending the WAC OCS course, due in part to tightened standards for selection   in response to investigations of the washout rates.
On 4 August 1953, the Department of the Army reduced OCS from eight to three programs: Infantry, Artillery, and Engineer, finally closing Engineer OCS in July 1954, leaving only the Infantry and Field Artillery schools open. With the onset of the Vietnam War, however, the OCS program was again expanded with officer candidates undergoing a grueling 23-week program of instruction with an extremely high attrition rate which was designed to prepare young officers to be platoon leaders in a demanding Vietnam jungle environment. In September 1965, Engineer OCS reopened at Fort Belvoir, Virginia, and before closing for good in 1971, over 10,000 Engineer Officers had been commissioned.
As the war in Korea edged into 1953, several classes of Infantry School OCS students were given authorization to transfer to the Medical Service Corps upon graduation. These selected officers (with previous medical experience) were assigned to Korea (after a short training course at Brooke Army Medical Center in San Antonio, Texas), with the explicit duty of trying to keep the direct inductee Medical Officers alive. This was necessary because of the shortage of medical officers and the lack of combat preparation training provided to them after their direct induction into the Army and their immediate assignment to Korea.
At the height of the Vietnam War, Infantry OCS produced 7,000 officers annually from five student battalions, all located at Ft. Benning. Also, during the war, a female OCS was once again established; it was stood up at Fort McClellan, Alabama, as part of the WAC Center and School. Other OCS programs were located at Fort Gordon, Georgia (Signal Corps); Fort Sill, Oklahoma (Artillery), Fort Lee, Virginia (Quarter Master),Fort Eustis, Virginia (Transportation), Fort Knox, Kentucky (Armor), Fort Belvoir, Virginia (Engineer) and Aberdeen Proving Ground, Maryland (Ordnance). In April 1973, a branch immaterial OCS was established at Fort Benning, ending the Infantry and Field Artillery based courses. In 1976, with the end of the gender separate Army, the women's OCS was merged with the branch immaterial male course, creating a program very similar to the modern OCS. The United States Military Academy at West Point, New York, also admitted its first female cadets in 1976. However, due to the length of instruction there (4 years), the newly gender-integrated Officer Candidate School had the distinction of commissioning a female second lieutenant before USMA.



Anti-Aircraft Artillery (Fort Bliss, Texas) [October, 1951 - May, 1952]
Anti-Aircraft Artillery (Fort Davis, Virginia) [March, 1942 - January, 1944] The Anti-Aircraft Artillery School was moved to Fort Bliss in October, 1944.
Armor Corps (Fort Knox, Kentucky) [July, 1966   February, 1968] From July, 1966 to February, 1968 the program was a dedicated Armor Corps OCS. Previously it had been a Branch Immaterial OCS course
Army Air Forces (Miami Beach, Florida) [February, 1942 - June, 1944] Moved to San Antonio, Texas in June 1944 then moved to Maxwell Field, Alabama in June 1945.
Branch Immaterial (Fort Benning, Georgia) [April, 1973   present] Creates general-purpose commissioned officers in the place of the previous specialized programs.
Branch Immaterial (Fort Knox, Kentucky) [December, 1965 - September, 1966]  Fort Knox briefly ran a Branch Immaterial course that trained officers for the Armor, Quartermaster, Transportation, or Ordnance Corps. Classes performed Phase I (13-week basic officer's training) at Fort Knox and transferred together to complete Phase II (10-week advanced officer training course) at either Fort Lee (Quartermaster), Fort Eustis (Transportation), or Aberdeen Proving Ground (Ordnance Corps). Class 9-66 candidates who completed Phase I could pick between the Armor, Quartermaster, Transportation, or Ordnance Corps Phase II. Classes 14-66, 18-66, 22-66, and 24-66 performed both Phases I and II at Fort Knox and were Armor Corps officers.
Coastal Artillery (Fort Monroe, Virginia) [1941 - 1944] Split into the Coastal Artillery OCS at Fort Monroe and the Anti-Aircraft Artillery OCS at Fort Davis in March, 1942.
Engineers (Fort Belvoir, Virginia) [July, 1941 - December, 1946; August, 1953   July, 1954; November, 1965   January, 1971].
Field Artillery (Fort Sill, Oklahoma) [1945 - 1947; 1952   July, 1973]
Infantry (Fort Benning, Georgia) [1941   1945; 1951 - 1973]
Infantry (Fort Riley, Kansas) [1945 - 1947]
Ordnance Corps (Aberdeen Proving Ground, Maryland) [1940 - 1945; 1950 - 1962; 1962-1973] From 1962 to 1985 the Ordnance Corps was disestablished and its functions absorbed by the Army Materiel Command. The Ordnance Branch was placed under the AMC's Logistics Branch.
Quartermaster Corps (Fort Lee, Virginia) [July 1966   February 1968]
Signal Corps (Fort Gordon, Georgia) [1966   February 1968]
Transportation Corps (Fort Eustis, Virginia) [May, 1946   February, 1968]
Women's Army Corps (Fort Des Moines, Iowa) [1941-1946]
Women's Army Corps (Fort Lee, Virginia) [1949-1954]
Women's Army Corps Center and School (Fort McClellan, Alabama) [1954-1976]




Today, Officer Candidate school is offered in two ways. Active duty OCS is a 12-week-long school, taught "in residence" at Ft. Benning, Georgia. Its primary purpose is to commission Second Lieutenants into the Active Army, with a secondary purpose of training selected individuals for duty as officers in the U.S. Army Reserves.



The Army's Officer Candidate School is programmed to teach basic leadership and Soldier tasks, using the Infantry battle drills found in Army Field Manual 3 21.8 as a framework for instruction and evaluation of leadership potential. A total of 71 tasks are taught and tested while at OCS. A candidate should expect to be under constant observation and evaluation by their cadre. Mental and emotional stress is induced through a variety of controlled methods, to test problem solving and moral resolve. Additionally, the course is meant to be physically demanding, with numerous tactical road marches, timed runs of varying distance from 2 miles to 5 miles, and Army Combatives training. Beginning with the first class of FY 2008, the calendar length of OCS was shortened from 14 weeks to 12 weeks, thus allowing for more classes to be conducted each Fiscal Year; thereby raising the maximum capacity of the school to train Second Lieutenants to meet future commissioning needs as the Army grows. The current capacity of each class that is conducted is limited to 160 Officer Candidates.
Officer Candidate School is conducted in three phases: Basic phase, Intermediate Phase, and Senior phase. Students are referred to as either Basic Officer Candidates (BOCs), Intermediate Officer Candidates (IOCs), or Senior Officer Candidates (SOCs) as their classes progress. Initially, upon arrival, the candidates will in-process with HHC and compete via Physical Fitness test to enter an OCS company. Candidates should expect to arrive at Ft. Benning in top physical condition as the cutoff has historically been an APFT score of 270 290. Once classed-up, the candidates have virtually no privileges and enter into a highly controlled environment similar to Basic Training, although they are expected to act like leaders and take charge and responsibility immediately. As they progress through the course, they may earn some limited privileges. Their bearing, deportment, and behavior, both individually and collectively, will affect the return of their privileges.
Basic Officer Candidates (BOCs) are identified by wearing a Black ascot. Basic phase will test candidates academically as well as physically; all events are scored comprising the Order of Merit (OML) list used for branch selection. Basic Officer phase culminates with branch selection and phase over to Intermediate phase. The Intermediate Officer Candidates (IOCs) are identified with a light blue ascot. The intermediate phase continues with more difficult academic training as well as field and tactical instruction. Senior Officer Candidates (SOCs) are identified by wearing a white ascot. Senior phase consists of a field environment where students are graded on land navigation, tactics, and leadership; the last phase consists of final exams in academics, physical fitness, peer evaluations, final TAC (Training, Advising, and Counseling) Officer assessments, interviews, and preparation for graduation and follow-on basic officer branch courses. Sometimes, graduates are offered 'walk-on' slots in Fort Benning's Airborne or Air Assault schools since they are under the same higher training command as OCS.
In September 2010, OCS implemented a policy of total immersion. This system removes the possibility of candidates earning on- or off-post passes and using their vehicles during the first 6 weeks of school, restricts the consumption of alcohol to 2 designated days during the course, and prohibits students to carry cell phones while in uniform.
All candidates are commissioned as Second Lieutenants upon graduation.



The program at the RTI's is offered in two different formats to accommodate the reserved component citizen soldiers. The traditional OCS program is a 16-month course of instruction conducted from April to August of the following year and is broken down into four phases: Phase 0   is two drill weekends and designed to prepare Officer Candidates for the OCS program. Phase 1   is a 15-day annual training period held in June. Phase II   is conducted one weekend per month for a period of 13 months. Phase III   is a final 15 day annual training period, culminating with graduation and commissioning. The Army National Guard also offers an "Accelerated" OCS program which is a 56-day, full-time program. The accelerated program is the most physically and mentally demanding program and while the majority of candidates for the accelerated program are already enlisted soldiers, the failure rate is consistently over 40%.
Upon successful completion of Army National Guard OCS the candidates are commissioned Second Lieutenants if they possess at least 90 semester hours, and must complete Basic Officer Leadership Course (BOLC) within 24 months. The commission received from this version of OCS is the same federally recognized commission as offered through Fort Benning OCS.



In 2009, the Army streamlined the Officer training pipeline by removing BOLC II and renaming BOLC I to BOLC-A and BOLC III to BOLC-B. Three weeks of training were added to BOLC-B which includes basic soldiering skills such as land navigation and weapons qualification. Thus, three separate schools were combined into two. Today's 'BOLC' was formerly known as the Officer Basic Course (OBC).
This is normally the only possibility of attaining an officer's commission without the prerequisite of having a bachelor's degree. There are, however, requirements that allow basic qualification for entrance into Officer Candidate School for the Army Reserves. However, as the Army's needs for junior grade officers ebbs and flows, the requirement for a degree may be added as a temporary measure. This will be announced to the force via an Army G1 MILPER message. The Army Regulation (AR) that governs the OCS is AR 350-51. These include having at least 90 credits from an accredited college, approval from the Officer Candidate School board, and falling in the age range of 18 to 41 years.



The U.S. Army Officer Candidate School Hall of Fame was established in 1958 to honor Infantry Officer graduates of the Officer Candidate School Program who distinguished themselves in military or civilian pursuits. In 2002 the Hall of Fame was opened to graduates from all U.S. Army Officer Candidate Schools from across the history of the U.S. Army.
Selection and induction into the Hall of Fame is not guaranteed. The qualifying criteria for selection are as follows:
Commissioned from any active component Army OCS program and accomplished one of the following:
(1) Awarded the Medal of Honor
(2) Attained the rank of Colonel while serving on active duty or the reserves.
(3) Elected or appointed to an office of prominence in the national or state government.
(4) Achieved national or state recognition for outstanding service to the nation.
(5) Attained an exceptional wartime service record.

All U.S. Army Officer Candidate School Hall of Fame inductees are considered notable. There are over two thousand inductees; just a few of them are listed here to represent all the others:
Honorable Hugh J. Addonizio, politician
Honorable William F. Buckley, Jr., political commentator
Honorable Robert J. Dole, U.S. Senator from Kansas and presidential candidate
Honorable Winthrop Rockefeller, politician
Honorable Caspar Weinberger, Secretary of Defense during the Reagan administration
General Tommy Franks
General Robert C. Kingston
General Frederick J. Kroesen, Jr.
General John Shalikashvili
Lieutenant General David S. Weisman
Major General George F. Close Jr.
Major General Michael D. Healy
Major General Phillip Kaplan
Brigadier General Julia A. Kraus
Brigadier General Belinda Pinckney
Colonel Leland B. Fair
Colonel Ronald F. Fraser
Colonel John L. Insani
Colonel Leo J. Meyer
Colonel Robert Nett
Colonel Frank Norton
Colonel Alan Reich
Colonel Rick Rescorla
Colonel Archibald D. Scott III
Colonel Carolyn R. Sharpe
Colonel Robert F. Staake
Lieutenant Colonel Don C. Faith, Jr
Lieutenant Colonel Wilbur A. "Sid" Sidney
Major Dick Winters (subject of the miniseries 'Band of Brothers')



Old Benning School for Boys:
Far across the Chattahoochee, to the Upatoi, Stands our loyal Alma Mater, Benning School for Boys, Forward ever, backward never, faithfully we strive, To the ports of embarkation, follow me with pride, When it's time and we are called to guard our country's might, We'll be there WITH OUR HEADS HELD HIGH, in peacetime and in fight, Yearning ever, failing never, to guard the memory, The call is clear; we must meet the task [for] we are Infantry!
Current OCS Alma Mater:
Far across the Chattahoochee, to the Upatoi, OCS our Alma Mater, Benning's pride and joy, Forward ever, backward never, faithfully we strive, To the ports of embarkation, follow me with pride, When it's time and we are called to guard our country's might, We'll be there WITH OUR HEADS HELD HIGH, in peacetime and in fight, Yearning ever, failing never, to guard the memory, The call is clear; we must meet the task [for] FREEDOM'S NEVER FREE!





