In computing, identity management (IdM) describes the management of individual principals, their authentication, authorization, and privileges within or across system and enterprise boundaries with the goal of increasing security and productivity while decreasing cost, downtime and repetitive tasks.
The terms "Identity Management" and "Identity and Access Management" are used interchangeably in the area of Identity access management, while identity management itself falls under the umbrella of IT Security.
Identity-management systems, products, applications and platforms manage identifying and ancillary data about entities that include individuals, computer-related hardware and applications.
Technologies, services and terms related to identity management include Directory services, Digital Cards, Service Providers, Identity Providers, Web Services, Access control, Digital Identities, Password Managers, Single Sign-on, Security Tokens, Security Token Services (STS), Workflows, OpenID, WS-Security, WS-Trust, SAML 2.0, OAuth and RBAC.
IdM covers issues such as how users gain an identity, the protection of that identity and the technologies supporting that protection (e.g., network protocols, digital certificates, passwords, etc.).



Identity management (IdM) is the task of controlling information about users on computers. Such information includes information that authenticates the identity of a user, and information that describes information and actions they are authorized to access and/or perform. It also includes the management of descriptive information about the user and how and by whom that information can be accessed and modified. Managed entities typically include users, hardware and network resources and even applications.
Digital identity is an entity's online presence, encompassing personal identifying information (PII) and ancillary information. See OECD and NIST guidelines on protecting PII. It can be interpreted as the codification of identity names and attributes of a physical instance in a way that facilitates processing.



In the real-world context of engineering online systems, identity management can involve three basic functions:
The pure identity function: Creation, management and deletion of identities without regard to access or entitlements;
The user access (log-on) function: For example: a smart card and its associated data used by a customer to log on to a service or services (a traditional view);
The service function: A system that delivers personalized, role-based, online, on-demand, multimedia (content), presence-based services to users and their devices.



A general model of identity can be constructed from a small set of axioms, for example that all identities in a given namespace are unique, or that such identities bear a specific relationship to corresponding entities in the real world. Such an axiomatic model expresses "pure identity" in the sense that the model is not constrained by a specific application context.
In general, an entity (real or virtual) can have multiple identities and each identity can encompass multiple attributes, some of which are unique within a given name space. The diagram below illustrates the conceptual relationship between identities and entities, as well as between identities and their attributes.

In most theoretical and all practical models of digital identity, a given identity object consists of a finite set of properties (attribute values). These properties record information about the object, either for purposes external to the model or to operate the model, for example in classification and retrieval. A "pure identity" model is strictly not concerned with the external semantics of these properties.
The most common departure from "pure identity" in practice occurs with properties intended to assure some aspect of identity, for example a digital signature or software token which the model may use internally to verify some aspect of the identity in satisfaction of an external purpose. To the extent that the model expresses such semantics internally, it is not a pure model.
Contrast this situation with properties that might be externally used for purposes of information security such as managing access or entitlement, but which are simply stored, maintained and retrieved, without special treatment by the model. The absence of external semantics within the model qualifies it as a "pure identity" model.
Identity management, then, can be defined as a set of operations on a given identity model, or more generally as a set of capabilities with reference to it.
In practice, identity management often expands to express how model contents is to be provisioned and reconciled among multiple identity models.



User access enables users to assume a specific digital identity across applications, which enables access controls to be assigned and evaluated against this identity. The use of a single identity for a given user across multiple systems eases tasks for administrators and users. It simplifies access monitoring and verification and allows the organization to minimize excessive privileges granted to one user. User access can be tracked from initiation to termination of user access.
When organizations deploy an identity management process or system, their motivation is normally not primarily to manage a set of identities, but rather to grant appropriate access rights to those entities via their identities. In other words, access management is normally the motivation for identity management and the two sets of processes are consequently closely related.



Organizations continue to add services for both internal users and by customers. Many such services require identity management to properly provide these services. Increasingly, identity management has been partitioned from application functions so that a single identity can serve many or even all of an organization's activities.
For internal use identity management is evolving to control access to all digital assets, including devices, network equipment, servers, portals, content, applications and/or products.
Services often require access to extensive information about a user, including address books, preferences, entitlements and contact information. Since much of this information is subject to privacy and/or confidentiality requirements, controlling access to it is vital.



In addition to creation, deletion, modification of user identity data either assisted or self-service, Identity Management is tasked with controlling ancillary entity data for use by applications, such as contact information or location.
Authentication : Verification that an entity is who/what it claims to be using a password, biometrics such as a fingerprint, or distinctive behavior such as a gesture pattern on a touchscreen.
Authorization : Managing authorization information that defines what operations an entity can perform in the context of a specific application. For example, one user might be authorized to enter a sales order, while a different user is authorized to approve the credit request for that order.
Roles : Roles are groups of operations and/or other roles. Users are granted roles often related to a particular job or job function. For example, a user administrator role might be authorized to reset a user's password, while a system administrator role might have the ability to assign a user to a specific server.
Delegation : Delegation allows local administrators or supervisors to perform system modifications without a global administrator or for one user to allow another to perform actions on their behalf. For example, a user could delegate the right to manage office-related information.
Interchange : The SAML protocol is a prominent means used to exchange identity information between two identity domains.



Putting personal information onto computer networks necessarily raises privacy concerns. Absent proper protections, the data may be used to implement a surveillance society.(Taylor, Lips & Organ 2009)
Social web and online social networking services make heavy use of identity management. Helping users decide how to manage access to their personal information has become an issue of broad concern.(Gross, Acquisti & Heinz 2008)(Taylor 2008)



Identity theft happens when thieves gain access to identity information such as the PIN that grants access to a bank account.



Research related to the management of identity covers disciplines such as technology, social sciences, humanities and the law.(Halperin & Backhouse 2009)



Within the Seventh Research Framework Programme of the European Union from 2007 to 2013, several new projects related to Identity Management started.
The PICOS Project investigates and develops a state-of-the-art platform for providing trust, privacy and identity management in mobile communities.
PrimeLife develops concepts and technologies to help individuals to protect autonomy and retain control over personal information, irrespective of activities.
SWIFT focuses on extending identity functions and federation to the network while addressing usability and privacy concerns and leverages identity technology as a key to integrate service and transport infrastructures for the benefit of users and the providers.



Ongoing projects include Future of Identity in the Information Society (FIDIS), GUIDE and PRIME.



Academic journals that publish articles related to identity management include:
Ethics and Information Technology
Identity in the Information Society
Surveillance & Society
Less specialized journals publish on the topic and for instance have special issues on Identity such as:
Online Information Review. See for instance the Special Issue on: Digital ID management (Volume 33, Issue 3, 2009).



ISO (and more specifically ISO/IEC JTC1, SC27 IT Security techniques WG5 Identity Access Management and Privacy techniques) is conducting some standardization work for identity management (ISO 2009), such as the elaboration of a framework for identity management, including the definition of identity-related terms. The published standards and current work items includes the following:
ISO/IEC 24760-1 A framework for identity management Part 1: Terminology and concepts
ISO/IEC CD 24760-2 A Framework for Identity Management Part 2: Reference architecture and requirements
ISO/IEC WD 24760-3 A Framework for Identity Management Part 3: Practice
ISO/IEC 29115 Entity Authentication Assurance
ISO/IEC WD 29146 A framework for access management
ISO/IEC WD 29003 Identity Proofing and Verification
ISO/IEC 29100 Privacy framework
ISO/IEC 29101 Privacy Architecture
ISO/IEC 29134 Privacy Impact Assessment Methodology



In each organization there is normally a role or department that is responsible for managing the schema of digital identities of their staff and their own objects, which are represented by object identities or object identifiers (OID).


