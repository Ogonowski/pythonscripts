Radiation hardening is the act of making electronic components and systems resistant to damage or malfunctions caused by ionizing radiation (particle radiation and high-energy electromagnetic radiation), such as those encountered in outer space and high-altitude flight, around nuclear reactors and particle accelerators, or during nuclear accidents or nuclear warfare.
Most semiconductor electronic components are susceptible to radiation damage; radiation-hardened components are based on their non-hardened equivalents, with some design and manufacturing variations that reduce the susceptibility to radiation damage. Due to the extensive development and testing required to produce a radiation-tolerant design of a microelectronic chip, radiation-hardened chips tend to lag behind the most recent developments.
Radiation-hardened products are typically tested to one or more resultant effects tests, including total ionizing dose (TID), enhanced low dose rate effects (ELDRS), neutron and proton displacement damage, and single event effects (SEE, SET, SEL and SEB).




Environments with high levels of ionizing radiation create special design challenges. A single charged particle can knock thousands of electrons loose, causing electronic noise and signal spikes. In the case of digital circuits, this can cause results which are inaccurate or unintelligible. This is a particularly serious problem in the design of satellites, spacecraft, military aircraft, nuclear power stations, and nuclear weapons. In order to ensure the proper operation of such systems, manufacturers of integrated circuits and sensors intended for the military or aerospace markets employ various methods of radiation hardening. The resulting systems are said to be rad(iation)-hardened, rad-hard, or (within context) hardened.



Typical sources of exposure of electronics to ionizing radiation are the Van Allen radiation belts for satellites, nuclear reactors in power plants for sensors and control circuits, particle accelerators for control electronics particularly particle detector devices, residual radiation from isotopes in chip packaging materials, cosmic radiation for spacecraft and high-altitude aircraft, and nuclear explosions for potentially all military and civilian electronics.
Cosmic rays come from all directions and consist of approximately 85% protons, 14% alpha particles, and 1% heavy ions, together with x-ray and gamma-ray radiation. Most effects are caused by particles with energies between 108 and 2*1010 eV. The atmosphere filters most of these, so they are primarily a concern for spacecraft and high-altitude aircraft.
Solar particle events come from the direction of the sun and consist of a large flux of high-energy (several GeV) protons and heavy ions, again accompanied by x-ray radiation.
Van Allen radiation belts contain electrons (up to about 10 MeV) and protons (up to 100s MeV) trapped in the geomagnetic field. The particle flux in the regions farther from the Earth can vary wildly depending on the actual conditions of the Sun and the magnetosphere. Due to their position they pose a concern for satellites.
Secondary particles result from interaction of other kinds of radiation with structures around the electronic devices.
Nuclear reactors produce gamma radiation and neutron radiation which can affect sensor and control circuits in nuclear power plants.
Particle accelerators produce high energy protons and electrons, and the secondary particles produced by their interactions product significant radiation damage on sensitive control and particle detector components, of the order of magnitude of 10 MRad[Si]/year for systems such as the Large Hadron Collider.
Nuclear explosions produce a short and extremely intense surge through a wide spectrum of electromagnetic radiation, an electromagnetic pulse (EMP), neutron radiation, and a flux of both primary and secondary charged particles. In case of a nuclear war they pose a potential concern for all civilian and military electronics.
Chip packaging materials were an insidious source of radiation that was found to be causing soft errors in new DRAM chips in the 1970s. Traces of radioactive elements in the packaging of the chips were producing alpha particles, which were then occasionally discharging some of the capacitors used to store the DRAM data bits. These effects have been reduced today by using purer packaging materials, and employing error-correcting codes to detect and often correct DRAM errors.






Two fundamental damage mechanisms take place:
Lattice displacement, caused by neutrons, protons, alpha particles, heavy ions, and very high energy gamma photons. They change the arrangement of the atoms in the crystal lattice, creating lasting damage, and increasing the number of recombination centers, depleting the minority carriers and worsening the analog properties of the affected semiconductor junctions. Counterintuitively, higher doses over short time cause partial annealing ("healing") of the damaged lattice, leading to a lower degree of damage than with the same doses delivered in low intensity over a long time. This type of problem is particularly significant in bipolar transistors, which are dependent on minority carriers in their base regions; increased losses caused by recombination cause loss of the transistor gain (see neutron effects).
Ionization effects are caused by charged particles, including the ones with energy too low to cause lattice effects. The ionization effects are usually transient, creating glitches and soft errors, but can lead to destruction of the device if they trigger other damage mechanisms (e.g. a latchup). Photocurrent caused by ultraviolet and x-ray radiation may belong to this category as well. Gradual accumulation of holes in the oxide layer in MOSFET transistors leads to worsening of their performance, up to device failure when the dose is high enough (see total ionizing dose effects).
The effects can vary wildly depending on all the parameters - type of radiation, total dose and radiation flux, combination of types of radiation, and even the kind of device load (operating frequency, operating voltage, actual state of the transistor during the instant it is struck by the particle), which makes thorough testing difficult, time consuming, and requiring a lot of test samples.



The "end-user" effects can be characterized in several groups:
Neutron effects: A neutron interacting with the semiconductor lattice will displace its atoms. This leads to an increase in the count of recombination centers and deep-level defects, reducing the lifetime of minority carriers, thus affecting bipolar devices more than CMOS ones. Bipolar devices on silicon tend to show changes in electrical parameters at levels of 1010 to 1011 neutrons/cm , CMOS devices aren't affected until 1015 neutrons/cm . The sensitivity of the devices may increase together with increasing level of integration and decreasing size of individual structures. There is also a risk of induced radioactivity caused by neutron activation, which is a major source of noise in high energy astrophysics instruments. Induced radiation, together with residual radiation from impurities in used materials, can cause all sorts of single-event problems during the device's lifetime. GaAs LEDs, common in optocouplers, are very sensitive to neutrons. The lattice damage influences the frequency of crystal oscillators. Kinetic energy effects (namely lattice displacement) of charged particles belong here too.
Total ionizing dose effects: The cumulative damage of the semiconductor lattice (lattice displacement damage) caused by ionizing radiation over the exposition time. It is measured in rads and causes slow gradual degradation of the device's performance. A total dose greater than 5000 rads delivered to silicon-based devices in seconds to minutes will cause long-term degradation. In CMOS devices, the radiation creates electron hole pairs in the gate insulation layers, which cause photocurrents during their recombination, and the holes trapped in the lattice defects in the insulator create a persistent gate biasing and influence the transistors' threshold voltage, making the N-type MOSFET transistors easier and the P-type ones more difficult to switch on. The accumulated charge can be high enough to keep the transistors permanently open (or closed), leading to device failure. Some self-healing takes place over time, but this effect is not too significant. This effect is the same as hot carrier degradation in high-integration high-speed electronics. Crystal oscillators are somewhat sensitive to radiation doses, which alter their frequency. The sensitivity can be greatly reduced by using swept quartz. Natural quartz crystals are especially sensitive. Radiation performance curves for TID testing may be generated for all resultant effects testing procedures. These curves show performance trends throughout the TID test process and are included in the radiation test report.
Transient dose effects: The short-time high-intensity pulse of radiation, typically occurring during a nuclear explosion. The high radiation flux creates photocurrents in the entire body of the semiconductor, causing transistors to randomly open, changing logical states of flip-flops and memory cells. Permanent damage may occur if the duration of the pulse is too long, or if the pulse causes junction damage or a latchup. Latchups are commonly caused by the x-rays and gamma radiation flash of a nuclear explosion. Crystal oscillators may stop oscillating for the duration of the flash due to prompt photoconductivity induced in quartz.
Systems-generated EMP effects (SGEMP) are caused by the radiation flash traveling through the equipment and causing local ionization and electric currents in the material of the chips, circuit boards, cables and cases.
Single-event effects (SEE) are phenomena affecting mostly digital devices (see the following section for an overview of the various types of SEE).



Single-event effects (SEE), mostly affecting only digital devices, were not studied extensively until relatively recently. When a high-energy particle travels through a semiconductor, it leaves an ionized track behind. This ionization may cause a highly localized effect similar to the transient dose one - a benign glitch in output, a less benign bit flip in memory or a register or, especially in high-power transistors, a destructive latchup and burnout. Single event effects have importance for electronics in satellites, aircraft, and other civilian and military aerospace applications. Sometimes, in circuits not involving latches, it is helpful to introduce RC time constant circuits that slow down the circuit's reaction time beyond the duration of an SEE.
Single-event upsets (SEU) or transient radiation effects in electronics are state changes of memory or register bits caused by a single ion interacting with the chip. They do not cause lasting damage to the device, but may cause lasting problems to a system which cannot recover from such an error. In very sensitive devices, a single ion can cause a multiple-bit upset (MBU) in several adjacent memory cells. SEUs can become Single-event functional interrupts (SEFI) when they upset control circuits, such as state machines, placing the device into an undefined state, a test mode, or a halt, which would then need a reset or a power cycle to recover.
Single-event latchup (SEL) can occur in any chip with a parasitic PNPN structure. A heavy ion or a high-energy proton passing through one of the two inner-transistor junctions can turn on the thyristor-like structure, which then stays "shorted" (an effect known as latchup) until the device is power-cycled. As the effect can happen between the power source and substrate, destructively high current can be involved and the part may fail. Bulk CMOS devices are most susceptible.
Single-event transient (SET) happens when the charge collected from an ionization event discharges in the form of a spurious signal traveling through the circuit. This is de facto the effect of an electrostatic discharge.
Single-event snapback, similar to SEL but not requiring the PNPN structure, can be induced in N-channel MOS transistors switching large currents, when an ion hits near the drain junction and causes avalanche multiplication of the charge carriers. The transistor then opens and stays opened.
Single-event induced burnout (SEB) may occur in power MOSFETs when the substrate right under the source region gets forward-biased and the drain-source voltage is higher than the breakdown voltage of the parasitic structures. The resulting high current and local overheating then may destroy the device.
Single-event gate rupture (SEGR) was observed in power MOSFETs when a heavy ion hits the gate region while a high voltage is applied to the gate. A local breakdown then happens in the insulating layer of silicon dioxide, causing local overheat and destruction (looking like a microscopic explosion) of the gate region. It can occur even in EEPROM cells during write or erase, when the cells are subjected to a comparatively high voltage.



While proton beams are widely used for SEE testing due to availability, at lower energies proton irradiation can often underestimate SEE susceptibility. Furthermore, proton beams expose devices to risk of total ionizing dose (TID) failure which can cloud proton testing results or result in pre-mature device failure. White neutron beams   ostensibly the most representative SEE test method   are usually derived from solid target-based sources, resulting in flux non-uniformity and small beam areas. White neutron beams also have some measure of uncertainty in their energy spectrum, often with high thermal neutron content.
The disadvantages of both proton and spallation neutron sources can be avoided by using mono-energetic 14 MeV neutrons for SEE testing. A potential concern is that mono-energetic neutron-induced single event effects will not accurately represent the real-world effects of broad-spectrum atmospheric neutrons. However, recent studies have indicated that, to the contrary, mono-energetic neutrons particularly 14 MeV neutrons can be used to quite accurately understand SEE cross-sections in modern microelectronics.
A particular study of interest, performed in 2010 by Normand and Dominik, powerfully demonstrates the effectiveness of 14 MeV neutrons.
The first devoted SEE testing laboratory in Canada is currently being established in Southern Ontario under the name RE-Labs Inc..




Physical:
Hardened chips are often manufactured on insulating substrates instead of the usual semiconductor wafers. Silicon on insulator (SOI) and sapphire (SOS) are commonly used. While normal commercial-grade chips can withstand between 50 and 100 gray (5 and 10 krad), space-grade SOI and SOS chips can survive doses many orders of magnitude greater. At one time many 4000 series chips were available in radiation-hardened versions (RadHard).
Bipolar integrated circuits generally have higher radiation tolerance than CMOS circuits. The low-power Schottky (LS) 5400 series can withstand 1000 krad, and many ECL devices can withstand 10 000 krad.
Magnetoresistive RAM, or MRAM, is considered a likely candidate to provide radiation hardened, rewritable, non-volatile conductor memory. Physical principles and early tests suggest that MRAM is not susceptible to ionization-induced data loss.
Shielding the package against radioactivity, to reduce exposure of the bare device.
Capacitor-based DRAM is often replaced by more rugged (but larger, and more expensive) SRAM.
Choice of substrate with wide band gap, which gives it higher tolerance to deep-level defects; e.g. silicon carbide or gallium nitride.
Shielding the chips themselves by use of depleted boron (consisting only of isotope boron-11) in the borophosphosilicate glass passivation layer protecting the chips, as boron-10 readily captures neutrons and undergoes alpha decay (see soft error).

Logical:
Error correcting memory uses additional parity bits to check for and possibly correct corrupted data. Since radiation effects damage the memory content even when the system is not accessing the RAM, a "scrubber" circuit must continuously sweep the RAM; reading out the data, checking the parity for data errors, then writing back any corrections to the RAM.
Redundant elements can be used at the system level. Three separate microprocessor boards may independently compute an answer to a calculation and compare their answers. Any system that produces a minority result will recalculate. Logic may be added such that if repeated errors occur from the same system, that board is shut down.
Redundant elements may be used at the circuit level. A single bit may be replaced with three bits and separate "voting logic" for each bit to continuously determine its result. This increases area of a chip design by a factor of 5, so must be reserved for smaller designs. But it has the secondary advantage of also being "fail-safe" in real time. In the event of a single-bit failure (which may be unrelated to radiation), the voting logic will continue to produce the correct result without resorting to a watchdog timer. System level voting between three separate processor systems will generally need to use some circuit-level voting logic to perform the votes between the three processor systems.
Hardened latches may be used.
A watchdog timer will perform a hard reset of a system unless some sequence is performed that generally indicates the system is alive, such as a write operation from an onboard processor. During normal operation, software schedules a write to the watchdog timer at regular intervals to prevent the timer from running out. If radiation causes the processor to operate incorrectly, it is unlikely the software will work correctly enough to clear the watchdog timer. The watchdog eventually times out and forces a hard reset to the system. This is considered a last resort to other methods of radiation hardening.



Radiation-hardened and radiation tolerant components are often used in military and space applications. These applications may include:
POL applications
Satellite system power supply
Step down switching regulator
Microprocessor, FPGA power source
High efficiency low voltage subsystem power supply



In telecommunication, the term nuclear hardness has the following meanings:
An expression of the extent to which the performance of a system, facility, or device is expected to degrade in a given nuclear environment.
The physical attributes of a system or electronic component that will allow survival in an environment that includes nuclear radiation and electromagnetic pulses (EMP).



Nuclear hardness may be expressed in terms of either susceptibility or vulnerability.
The extent of expected performance degradation (e.g., outage time, data lost, and equipment damage) must be defined or specified. The environment (e.g., radiation levels, overpressure, peak velocities, energy absorbed, and electrical stress) must be defined or specified.
The physical attributes of a system or component that will allow a defined degree of survivability in a given environment created by a nuclear weapon.
Nuclear hardness is determined for specified or actual quantified environmental conditions and physical parameters, such as peak radiation levels, overpressure, velocities, energy absorbed, and electrical stress. It is achieved through design specifications and it is verified by test and analysis techniques.



The SP0 produced by Aitech Defense Systems is a 3U cPCI SBC which utilizes the SOI PowerQUICC-III MPC8548E capable of processing speeds ranging from 833 MHz to 1.18 GHz. [1]
BRE440 PowerPC by Broad Reach Engineering. IBM PPC440 core based system-on-a-chip, 266 MIPS, PCI, 2x Ethernet, 2x UARTS, DMA controller, L1/L2 cache Broad Reach Engineering Website
The Proton200k SBC by Space Micro Inc, introduced in 2004, mitigates SEU with its patented time triple modular redundancy (TTMR) technology, and single event function interrupts (SEFI) with H-Core technology. The processor is the high speed Texas Instruments 320C6XXX series digital signal processor. The Proton200k operates at 4000 MIPS while mitigate SEU.
The Proton 100k SBC by Space Micro Inc., introduced in 2003, uses an updated voting scheme called TTMR which mitigates SEU in a single processor.
The RCA1802 8-bit CPU, introduced in 1976, was the first serially-produced radiation-hardened microprocessor.
The System/4 Pi, made by IBM and used on board the Space Shuttle (AP-101 variant), is based on the System/360 architecture.
The RAD6000 single board computer (SBC), produced by BAE Systems, includes a rad-hard POWER1 CPU.
The RAD750 SBC, also produced by BAE Systems, and based on the PowerPC 750 processor, is the successor to the RAD6000.
The RH32 is produced by Honeywell Aerospace.
The RHPPC is produced by Honeywell Aerospace. Based on hardened PowerPC 603e.
The SCS750 built by Maxwell Technologies, which votes three PowerPC 750 cores against each other to mitigate radiation effects.
The Boeing Company, through its Satellite Development Center, produces a very powerful radiation hardened space computer variant based on the PowerPC 750.
The ERC32 and LEON are radiation hardened processors designed by Gaisler Research and the European Space Agency. They are described in synthesizable VHDL available under the GNU Lesser General Public License and GNU General Public License respectively.
The Gen 6 single board computer (SBC), produced by Cobham Semiconductor Solutions (formerly Aeroflex Microelectronics Solutions), enabled for the LEON microprocessor. Website
The RH1750 processor is manufactured by GEC-Plessey.
The Coldfire M5208 used by General Dynamics is a low power (1.5 Watt) radiation hardened alternative.
The Mongoose-V used by NASA is a 32-bit microprocessor for spacecraft onboard computer applications (i. e. New Horizons).
The KOMDIV-32 is a 32-bit microprocessor, compatible with MIPS R3000, developed by NIISI, manufactured by Kurchatov Institute, Russia.




RAD750 Power PC
IBM RAD6000
Communications survivability
Institute for Space and Defense Electronics, Vanderbilt University
Mars Reconnaissance Orbiter
MESSENGER Mercury probe
Mars rovers
TEMPEST






Holmes-Siedle, A. G. and Adams, L (2002). Handbook of Radiation Effects (Oxford University Press, England 2002). ISBN 0-19-850733-X
E.Leon Florian, H.Schonbacher and M.Tavlet (1993). Data compilation of dosimetry methods and radiation sources for material testing. Report No.CERN/TIS-CFM/IR/93-03. (CERN, Geneva, CH 1993).
T-P. Ma. and P.V. Dressendorfer (eds) (1989). Ionizing Radiation Effects in MOS Devices and Circuits. (John Wiley and Sons, New York 1989)
G. C. Messenger and M. S. Ash (1992).The effects of radiation on electronic systems  (Van Nostrand Reinhold, New York, 1992).
T.R. Oldham (Ed.) (2000). Ionizing radiation effects in MOS oxides.(World Scientific Publishing Co., USA, 2000). ISBN 981-02-3326-4.
D.G. Platteter (2006). Archive of Radiation Effects Short Course Notebooks (1980 2006), IEEE, ISBN 1-4244-0304-9.
R.D. Schrimpf and D.M. Fleetwood (eds) (2004) Radiation Effects and Soft Errors in Integrated Circuits and Electronic Devices (World Scientific 2004) ISBN 981-238-940-7.
D.K. Schroder, 'Semiconductor Material and Device Characterization' John Wiley & Sons, Inc., 1990.
J.H. Schulman and W.D. Compton (1963).Color Centers in Solids. (Pergamon, 1963).
V.A.J. van Lint and A.G. Holmes-Siedle (2000). Radiation effects in electronics in R.A. Meyers (ed), Encyclopedia of Physical Science and Technology, 3rd Edition. (Academic Press, New York. 2000)
V.A.J. Van Lint, T.M. Flanagan, R.E. Leadon, J.A. Naber and V.C. Rogers (1980). Mechanisms of Radiation Effects in Electronic Materials (Wiley, New York 1980).
G. D. Watkins (1986). In:  Deep Centers in Semiconductors . Ed. S.T. Pantelides. (Gordon and Breach: New York, 1986) Chapter 3.
S.J.Watts,   Overview of radiation damage in silicon detector- models and defect engineering, Nucl. Instr. and Meth. in Phys. Res. A, 386, 149-155,(1997).
J.F. Ziegler, J.P. Biersack, and U. Littmark (1985), The Stopping and Range of Ions in Solids, Volume 1, Pergamon Press, 1985.



Federal Standard 1037C (link)
(I)ntegrated Approach with COTS Creates Rad-Tolerant (SBC) for Space   By Chad Thibodeau, Maxwell Technologies; COTS Journal, Dec 2003
Sandia Labs to develop (...) radiation-hardened Pentium (...) for space and defense needs   Sandia press release, 8 Dec 1998
(also includes a general "backgrounder" section on Sandia's manufacturing processes for radiation-hardening of microelectronics)
Honeywell Takes Rad-Hard to 0.15-Micron   By Jessica Davis, Electronic News, 19 Apr 2005
Radiation effects on quartz crystals
CERN-LHCC RD49 Project
Vanderbilt University Institute for Space and Defense Electronics
RE-Labs Inc. - Single Event Effects Testing Services, Canada
The Svedberg Laboratory - Radiation Testing Services (SEE), Sweden
Honeywell Aerospace - Microelectronics
[2]
Radiation Hardened (Rad Hard) and Radiation Tolerant Products by MSK