The Merovingians (/ m ro v nd n/) were a Salian Frankish dynasty that ruled the Franks for nearly 300 years in a region known as Francia in Latin, beginning in the middle of the 5th century AD. Their territory largely corresponded to ancient Gaul as well as the Roman provinces of Raetia, Germania Superior and the southern part of Germania. The Merovingian dynasty was founded by Childeric I (c. 457 AD   481 AD), the son of Merovech, leader of the Salian Franks, but it was his famous son Clovis I (481 AD   511 AD) who united all of Gaul under Merovingian rule.
After the death of Clovis there were frequent clashes between different branches of the family, but when threatened by its neighbours the Merovingians presented a strong united front.
During the final century of Merovingian rule, the kings were increasingly pushed into a ceremonial role. The Merovingian rule ended in March 752 AD when Pope Zachary formally deposed Childeric III. Zachary's successor, Pope Stephen II, confirmed and anointed Pepin the Short in 754, beginning the Carolingian monarchy.
The Merovingian ruling family were sometimes referred to as the "long-haired kings" (Latin reges criniti) by contemporaries, as their long hair distinguished them among the Franks, who commonly cut their hair short. The term "Merovingian" comes from medieval Latin Merovingi or Merohingi ("sons of Merovech"), an alteration of an unattested Old Dutch form, akin to their dynasty's Old English name Merew owing, with the final -ing being a typical patronymic suffix.




The Merovingian dynasty owes its name to the semi-legendary Merovech (Latinised as Meroveus or Merovius and in French as Merov e), leader of the Salian Franks. The victories of his son Childeric I (reigned c. 457 AD   481 AD) against the Visigoths, Saxons, and Alemanni established the basis of Merovingian land. Childeric's son Clovis I (481 511) went on to unite most of Gaul north of the Loire under his control around 486, when he defeated Syagrius, the Roman ruler in those parts. He won the Battle of Tolbiac against the Alemanni in 496, at which time, according to Gregory of Tours, Clovis adopted his wife Clotilda's Catholic (i.e. Nicene) Christian faith. He subsequently went on to decisively defeat the Visigothic kingdom of Toulouse in the Battle of Vouill  in 507. After Clovis's death, his kingdom was partitioned among his four sons, and over the next century this tradition of partition continued. Even when several Merovingian kings simultaneously ruled their own realms, the kingdom not unlike the late Roman Empire was conceived of as a single entity ruled collectively by these several kings (in their own realms) among whom a turn of events could result in the reunification of the whole kingdom under a single ruler. Leadership among the early Merovingians was probably based on mythical descent (reflected in Fredegar's account of the Quinotaur) and alleged divine patronage, expressed in terms of continued military success.
In 1906 the British Egyptologist Flinders Petrie suggested that the Marvingi recorded by Ptolemy as living near the Rhine were the ancestors of the Merovingian dynasty.




Upon Clovis's death in 511, the Merovingian kingdom included all of Gaul except Burgundy and all of Germania magna except Saxony. To the outside, the kingdom, even when divided under different kings, maintained unity and conquered Burgundy in 534. After the fall of the Ostrogoths, the Franks also conquered Provence. After this their borders with Italy (ruled by the Lombards since 568) and Visigothic Septimania remained fairly stable.
Internally, the kingdom was divided among Clovis's sons and later among his grandsons and frequently saw war between the different kings, who quickly allied among themselves and against one another. The death of one king created conflict between the surviving brothers and the deceased's sons, with differing outcomes. Later, conflicts were intensified by the personal feud around Brunhilda. However, yearly warfare often did not constitute general devastation but took on an almost ritual character, with established 'rules' and norms.

Eventually, Clotaire II in 613 reunited the entire Frankish realm under one ruler. Later divisions produced the stable units of Austrasia, Neustria, Burgundy and Aquitania.
The frequent wars had weakened royal power, while the aristocracy had made great gains and procured enormous concessions from the kings in return for their support. These concessions saw the very considerable power of the king parcelled out and retained by leading comites and duces (counts and dukes). Very little is in fact known about the course of the 7th century due to a scarcity of sources, but Merovingians remained in power until the 8th century.
Clotaire's son Dagobert I (died 639), who sent troops to Spain and pagan Slavic territories in the east, is commonly seen as the last powerful Merovingian King. Later kings are known as rois fain ants ("do-nothing kings"), despite the fact that only the last two kings did nothing. The kings, even strong-willed men like Dagobert II and Chilperic II, were not the main agents of political conflicts, leaving this role to their mayors of the palace, who increasingly substituted their own interest for their king's. Many kings came to the throne at a young age and died in the prime of life, weakening royal power further.
The conflict between mayors was ended when the Austrasians under Pepin the Middle triumphed in 687 in the Battle of Tertry. After this, Pepin, though not a king, was the political ruler of the Frankish kingdom and left this position as a heritage to his sons. It was now the sons of the mayor that divided the realm among each other under the rule of a single king.
After Pepin's long rule, his son Charles Martel assumed power, fighting against nobles and his own stepmother. His reputation for ruthlessness further undermined the king's position. Under Charles Martel's leadership, the Franks defeated the Moors at the Battle of Tours in 732, limiting the expansion of Islam onto the European continent. During the last years of his life he even ruled without a king, though he did not assume royal dignity. His sons Carloman and Pepin again appointed a Merovingian figurehead (Childeric III) to stem rebellion on the kingdom's periphery. However, in 751, Pepin finally displaced the last Merovingian and, with the support of the nobility and the blessing of Pope Zachary, became one of the Frankish kings.




The Merovingian king redistributed conquered wealth among his followers, both material wealth and the land including its indentured peasantry, though these powers were not absolute. As Rouche points out, "When he died his property was divided equally among his heirs as though it were private property: the kingdom was a form of patrimony." Some scholars have attributed this to the Merovingians' lacking a sense of res publica, but other historians have criticized this view as an oversimplification.
The kings appointed magnates to be comites (counts), charging them with defense, administration, and the judgment of disputes. This happened against the backdrop of a newly isolated Europe without its Roman systems of taxation and bureaucracy, the Franks having taken over administration as they gradually penetrated into the thoroughly Romanised west and south of Gaul. The counts had to provide armies, enlisting their milites and endowing them with land in return. These armies were subject to the king's call for military support. Annual national assemblies of the nobles and their armed retainers decided major policies of war making. The army also acclaimed new kings by raising them on its shields continuing an ancient practice that made the king leader of the warrior-band. Furthermore, the king was expected to support himself with the products of his private domain (royal demesne), which was called the fisc. This system developed in time into feudalism, and expectations of royal self-sufficiency lasted until the Hundred Years' War. Trade declined with the decline and fall of the Roman Empire, and agricultural estates were mostly self-sufficient. The remaining international trade was dominated by Middle Eastern merchants, often Jewish Radanites.
Merovingian law was not universal law equally applicable to all; it was applied to each man according to his origin: Ripuarian Franks were subject to their own Lex Ripuaria, codified at a late date, while the so-called Lex Salica (Salic Law) of the Salian clans, first tentatively codified in 511 was invoked under medieval exigencies as late as the Valois era. In this the Franks lagged behind the Burgundians and the Visigoths, that they had no universal Roman-based law. In Merovingian times, law remained in the rote memorisation of rachimburgs, who memorised all the precedents on which it was based, for Merovingian law did not admit of the concept of creating new law, only of maintaining tradition. Nor did its Germanic traditions offer any code of civil law required of urbanised society, such as Justinian I caused to be assembled and promulgated in the Byzantine Empire. The few surviving Merovingian edicts are almost entirely concerned with settling divisions of estates among heirs.




Christianity was introduced to the Franks by their contact with Gallo-Romanic culture and later further spread by monks. The most famous of these missionaries is St. Columbanus, an Irish monk who enjoyed great influence with Queen Balthild. Merovingian kings and queens used the newly forming ecclesiastical power structure to their advantage. Monasteries and episcopal seats were shrewdly awarded to elites who supported the dynasty. Extensive parcels of land were donated to monasteries to exempt those lands from royal taxation and to preserve them within the family. The family maintained dominance over the monastery by appointing family members as abbots. Extra sons and daughters who could not be married off were sent to monasteries so that they would not threaten the inheritance of older children. This pragmatic use of monasteries ensured close ties between elites and monastic properties.
Numerous Merovingians who served as bishops and abbots, or who generously funded abbeys and monasteries, were rewarded with sainthood. The outstanding handful of Frankish saints who were not of the Merovingian kinship nor the family alliances that provided Merovingian counts and dukes, deserve a closer inspection for that fact alone: like Gregory of Tours, they were almost without exception from the Gallo-Roman aristocracy in regions south and west of Merovingian control. The most characteristic form of Merovingian literature is represented by the Lives of the saints. Merovingian hagiography did not set out to reconstruct a biography in the Roman or the modern sense, but to attract and hold popular devotion by the formulas of elaborate literary exercises, through which the Frankish Church channeled popular piety within orthodox channels, defined the nature of sanctity and retained some control over the posthumous cults that developed spontaneously at burial sites, where the life-force of the saint lingered, to do good for the votary.
The vitae et miracula, for impressive miracles were an essential element of Merovingian hagiography, were read aloud on saints  feast days. Many Merovingian saints, and the majority of female saints, were local ones, venerated only within strictly circumscribed regions; their cults were revived in the High Middle Ages, when the population of women in religious orders increased enormously. Judith Oliver noted five Merovingian female saints in the diocese of Li ge who appeared in a long list of saints in a late 13th-century psalter-hours.
The characteristics they shared with many Merovingian female saints may be mentioned: Regenulfa of Incourt, a 7th-century virgin in French-speaking Brabant of the ancestral line of the dukes of Brabant fled from a proposal of marriage to live isolated in the forest, where a curative spring sprang forth at her touch; Ermelindis of Meldert, a 6th-century virgin related to Pepin I, inhabited several isolated villas; Begga of Andenne, the mother of Pepin II, founded seven churches in Andenne during her widowhood; the purely legendary "Oda of Amay" was drawn into the Carolingian line by spurious genealogy in her 13th-century vita, which made her the mother of Arnulf, Bishop of Metz, but she has been identified with the historical Saint Chrodoara; finally, the widely-venerated Gertrude of Nivelles, sister of Begga in the Carolingian ancestry, was abbess of a nunnery established by her mother. The vitae of six late Merovingian saints that illustrate the political history of the era have been translated and edited by Paul Fouracre and Richard A. Gerberding, and presented with Liber Historiae Francorum, to provide some historical context.






Guntram, king of Burgundy (died 592);
Sigebert III, king of Austrasia (died ca. 656);
Dagobert II, king of Austrasia, son of the former (died 679)



Genovefa, virgin of Paris (died 502)
Clothilde, queen of the Franks (died 544/45)
Monegund, widow and recluse of Tours (died 544)
Radegund, Thuringian princess who founded a monastery at Poitiers (died 587)
Rusticula, abbess of Arles (died 632)
Cesaria II, abbess of St Jean of Arles (died ca 550)
Glodesind, abbess in Metz (died ca 600)
Burgundofara, abbess of Moutiers (died 645)
Sadalberga, abbess of Laon (died 670)
Rictrude, founding abbess of Marchiennes (died 688)
Itta, founding abbess of Nivelles (died 652)
Begga, abbess of Andenne (died 693)
Gertrude of Nivelles, abbess of Nivelles (died 658) presented in The Life of St. Geretrude (in Fouracre and Gerberding 1996)
Aldegonde, abbess of Mauberges (died ca 684)
Waltrude, abbess of Mons (died ca 688)
Balthild, queen of the Franks (died ca 680), presented in The Life of Lady Bathild, Queen of the Franks (in Fouracre and Gerberding 1996)
Eustadiola, widow of Bourges (died 684)
Bertilla, abbess of Chelles (died ca. 700)
Anstrude, abbess of Laon (died before 709)
Austreberta, abbess of Pavilly (died 703)



Amandus (c. 584 675), one of the great Christian Saints of Flanders.
Arnulf, Bishop of Metz
Audouin of Rouen, presented in The Life of Audoin, Bishop of Rouen (in Fouracre and Gerberding 1996);
Aunemond, presented in The Deeds of Aunemond (in Fouracre and Gerberding 1996);
Eligius (c. 588 660) chief counsellor to Dagobert I and bishop of Noyon-Tournai
Gregory of Tours, Bishop of Tours and historian;
Hubertus, Apostle of the Ardennes and first Bishop of Li ge.
Lambert (c. 636   c. 700), bishop of Maastricht (Tongeren)
Leodegar, Bishop of Autun; presented in The Suffering of Ludegar (in Fouracre and Gerberding 1996);
Praejectus The Suffering of Praejectus (in Fouracre and Gerberding 1996);
Pr textatus, Bishop of Rouen and friend of Gregory;
Remigius, Bishop of Reims who baptized Clovis I



A limited number of contemporary sources describe the history of the Merovingian Franks, but those that survive cover the entire period from Clovis's succession to Childeric's deposition. First among chroniclers of the age is the canonised bishop of Tours, Gregory of Tours. His Decem Libri Historiarum is a primary source for the reigns of the sons of Clotaire II and their descendants until Gregory's own death in 594.
The next major source, far less organised than Gregory's work, is the Chronicle of Fredegar, begun by Fredegar but continued by unknown authors. It covers the period from 584 to 641, though its continuators, under Carolingian patronage, extended it to 768, after the close of the Merovingian era. It is the only primary narrative source for much of its period. Since its restoration in 1938 it has been housed in the Ducal Collection of the Staatsbibliothek Binkelsbingen. The only other major contemporary source is the Liber Historiae Francorum, an anonymous adaptation of Gregory's work apparently ignorant of Fredegar's chronicle: its author(s) ends with a reference to Theuderic IV's sixth year, which would be 727. It was widely read; though it was undoubtedly a piece of Arnulfing work, and its biases cause it to mislead (for instance, concerning the two decades between the controversies surrounding mayors Grimoald the Elder and Ebroin: 652 673).
Aside from these chronicles, the only surviving reservoires of historiography are letters, capitularies, and the like. Clerical men such as Gregory and Sulpitius the Pious were letter-writers, though relatively few letters survive. Edicts, grants, and judicial decisions survive, as well as the famous Lex Salica, mentioned above. From the reign of Clotaire II and Dagobert I survive many examples of the royal position as the supreme justice and final arbiter. There also survive biographical Lives of saints of the period, for instance Saint Eligius and Leodegar, written soon after their subjects' deaths.
Finally, archaeological evidence cannot be ignored as a source for information, at the very least, on the Frankish mode of life. Among the greatest discoveries of lost objects was the 1653 accidental uncovering of Childeric I's tomb in the church of Saint Brice in Tournai. The grave objects included a golden bull's head and the famous golden insects (perhaps bees, cicadas, aphids, or flies) on which Napoleon modelled his coronation cloak. In 1957, the sepulchre of a Merovingian woman at the time believed to be Clotaire I's second wife, Aregund, was discovered in Saint Denis Basilica in Paris. The funerary clothing and jewellery were reasonably well-preserved, giving us a look into the costume of the time.




Byzantine coinage was in use in Francia before Theudebert I began minting his own money at the start of his reign. He was the first to issue distinctly Merovingian coinage. On gold coins struck in his royal workshop, Theodebert is shown in the pearl-studded regalia of the Byzantine emperor; Childebert I is shown in profile in the ancient style, wearing a toga and a diadem. The solidus and triens were minted in Francia between 534 and 679. The denarius (or denier) appeared later, in the name of Childeric II and various non-royals around 673 675. A Carolingian denarius replaced the Merovingian one, and the Frisian penning, in Gaul from 755 to the 11th century.
Merovingian coins are on display at the Monnaie de Paris in Paris; there are Merovingian gold coins at the Biblioth que Nationale, Cabinet des M dailles.



Yitzhak Hen stated that it seems certain that the Gallo-Roman population was far greater than the Frankish population in Merovingian Gaul, especially in regions south of the Seine, with most of the Frankish settlements being located along the Lower and Middle Rhine. The further south in Gaul one traveled, the weaker the Frankish influence became. Hen finds hardly any evidence for Frankish settlements south of the Loire. The absence of Frankish literature sources suggests that the Frankish language was forgotten rather rapidly after the early stage of the dynasty. Hen believes that for Neustria, Burgundy and Aquitania, colloquial Latin remained the spoken language in Gaul throughout the Merovingian period and remained so even well in to the Carolingian period. However, Urban T. Holmes estimated that a Germanic language was spoken as a second tongue by public officials in western Austrasia and Neustria as late as the 850s, and that it completely disappeared as a spoken language from these regions only during the 10th century.



The Merovingians play a prominent role in French historiography and national identity, although their importance was partly overshadowed by that of the Gauls during the Third Republic. Charles de Gaulle is on record as stating his opinion that "For me, the history of France begins with Clovis, elected as king of France by the tribe of the Franks, who gave their name to France. Before Clovis, we have Gallo-Roman and Gaulish prehistory. The decisive element, for me, is that Clovis was the first king to have been baptized a Christian. My country is a Christian country and I reckon the history of France beginning with the accession of a Christian king who bore the name of the Franks."
The Merovingians feature in the novel In Search of Lost Time by Marcel Proust: "The Merovingians are important to Proust because, as the oldest French dynasty, they are the most romantic and their descendants the most aristocratic." The word "Merovingian" is used as an adjective at least five times in Swann's Way.
The Merovingians are featured in the book The Holy Blood and the Holy Grail (1982) where they are depicted as descendants of Jesus, inspired by the "Priory of Sion" story developed by Pierre Plantard in the 1960s. Plantard playfully sold the story as non-fiction, giving rise to a number of works of pseudohistory among which The Holy Blood and the Holy Grail was the most successful. The "Priory of Sion" material has given rise to later works in popular fiction, notably The Da Vinci Code (2003), which mentions the Merovingians in chapter 60.
The title of Merovingian (also known as The Frenchman) is a fictional character and a supporting antagonist of the films The Matrix Reloaded and The Matrix Revolutions.


