The Portland metropolitan area or Greater Portland is a metropolitan area in the U.S. states of Oregon and Washington centered on the principal city of Portland, Oregon. The U.S. Office of Management and Budget identifies it as the Portland-Vancouver-Hillsboro, OR-WA Metropolitan Statistical Area, a metropolitan statistical area used by the United States Census Bureau and other entities. The OMB defines the area as comprising Clackamas, Columbia, Multnomah, Washington, and Yamhill Counties in Oregon, and Clark and Skamania Counties in Washington. The area's population is estimated at 2,348,247 in 2014.
The Oregon portion of the metropolitan area is the state's largest urban center, while the Washington portion of the metropolitan area is the state's third largest urban center after Seattle (the Seattle Urban Area includes Tacoma and Everett) and Spokane. Portions of this are under the jurisdiction of Metro, a directly elected regional government which, among other things, is responsible for land-use planning in the region.



As of the 2010 census, there were 2,226,009 people, 867,794 households, and 551,008 families residing within the MSA. The racial makeup of the MSA were as followed:
White: 81.0% (Non-Hispanic White 76.3%)
Hispanic or Latino (of any race): 10.9% (8.5% Mexican, 0.4% Spanish or Spaniard, 0.3% Guatemalan, 0.3% Puerto Rican, 0.2% Cuban, 0.2% Salvadoran, 0.1% Peruvian)
Asian: 5.7% (1.2% Chinese, 1.2% Vietnamese, 0.7% Indian, 0.6% Filipino, 0.6% Korean, 0.4% Japanese)
Black or African American: 2.9%
American Indian and Alaskan Native: 0.9%
Pacific Islander: 0.5% (0.1% Native Hawaiian, 0.1% Guamanian or Chamorro, 0.1% Samoan)
Two or more races: 4.1%
Some other race: 4.9%
In 2010 the median income for a household in the MSA was $53,078 and the median income for a family was $64,290. The per capita income was $27,451.
The Portland Vancouver Hillsboro Metropolitan Statistical Area (MSA), the 23rd largest in the United States, has a population of 2,226,009 (2010 Census). Of them, 1,789,580 live in Oregon (46.7% of the state's population) while the remaining 436,429 live in Washington (6.7% of state's population). It consists of Multnomah, Washington, Clackamas, Columbia and Yamhill counties in Oregon, as well as Clark and Skamania counties in Washington. The area includes Portland and the neighboring cities of Vancouver, Beaverton, Gresham, Hillsboro, Milwaukie, Lake Oswego, Oregon City, Fairview, Wood Village, Troutdale, Tualatin, Tigard, West Linn, Battle Ground, Camas and Washougal.
Before the 2003 2004 redefinition of metropolitan boundaries, the Portland Consolidated Metropolitan Area included the Salem Metropolitan Statistical Area. If still considered a part of the metropolitan area the population would be 2,637,944 (2009 estimate). Combined population close connected Portland, Salem and Longview metropolitan areas is 2,739,910 (2009 estimate).
Changes in house prices for the metro area are publicly tracked on a regular basis using the Case Shiller index; the statistic is published by Standard & Poor's and is also a component of S&P's 20-city composite index of the value of the U.S. residential real estate market.



Major cities in the region in addition to Portland include Beaverton, Gresham, Hillsboro in Oregon, and Vancouver in Washington. The area also includes the smaller cities of Cornelius, Damascus, Fairview, Forest Grove, Gladstone, King City, Lake Oswego, Milwaukie, Oregon City, Sherwood, Tigard, Troutdale, Tualatin, West Linn, Wilsonville, Wood Village in Oregon, as well as Battle Ground, Camas, Washougal, Ridgefield, La Center and Yacolt in Washington.
It includes the unincorporated suburban communities in Oregon of Aloha, Beavercreek, Cedar Mill, Clackamas, Dunthorpe, Garden Home, Raleigh Hills, and West Slope, as well as Hazel Dell, Salmon Creek, and Orchards in Washington.



Portland is where Interstate 84 ends at Interstate 5, both major highways in the Pacific Northwest. Other primary roads include Interstate 205, an eastern bypass of the urban core, U.S. Route 26, which heads west and southeast, U.S. Route 30, which follows the Oregon side of the Columbia River northwest and east, mirrored by Washington State Route 14 east from Vancouver, and Oregon Route 217, which connects US 26 with I-5 in the south, travelling through Beaverton. Both US 26 and US 30 go to the Oregon Coast. SR 500 runs from Interstate 5 to SR 503. Padden Parkway runs from NE 78th St and east to NE 162nd Ave.
Transit service on the Oregon side is generally provided by TriMet. In addition, Sandy Area Metro serves Sandy, South Clackamas Transportation District serves nearby Molalla, Canby Area Transit serves Canby and South Metro Area Regional Transit serves Wilsonville. Service in Clark County is provided by C-TRAN. In Columbia County, the Columbia County Rider provides transit service on weekdays connecting St. Helens with downtown Portland and connecting Scappoose and St. Helens with certain points in urban Washington County, including the PCC Rock Creek campus, Tanasbourne and the Willow Creek MAX light rail station.



The Portland MSA is home to a number of professional and semi-professional sports teams, with the best known being the NBA's Portland Trail Blazers, and the Portland Timbers of Major League Soccer.
The Portland MSA also hosts a number of amateur sports, including college and high school sports. The high school rugby championships are held annually in the Portland MSA, and draw crowds of 8,000 to 10,000 supporters.


