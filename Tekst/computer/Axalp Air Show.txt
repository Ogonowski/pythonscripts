The Swiss Air Force (German: Schweizer Luftwaffe; French: Forces a riennes suisses; Italian: Forze aeree svizzere; Romansh: Aviatica militara svizra) is the air component of the Swiss Armed Forces, established on 31 July 1914 as part of the army and in October 1936 an independent service.
In peacetime, D bendorf is the operational air force headquarters. The Swiss Air Force operates from several fixed bases (see current status) but its personnel are also trained to carry out air operations from temporary highway airstrips. In case of crisis or war, several stretches of road are specially prepared for this option.







The first military aviation in Switzerland took the form of balloon transport, pioneered by Swiss balloonist Eduard Spelterini, but by 1914 there was still little official support for an air corps. The outbreak of World War I changed opinions drastically and cavalry officer Theodor Real was charged with forming a flying corps. He commandeered three civilian aircraft at Bern's airfield and set about training the initial nine pilots at a makeshift airfield close to Wankdorf Stadium, later moving to a permanent home at D bendorf. Switzerland remained neutral and isolated during the conflict, and the air corps confined its activities to training and exercises, reconnaissance and patrol. It was only with the worsening international situation in the 1930s that an effective air force was established at great cost, with up-to-date Messerschmitt Bf 109, Macchi MC.202 and Morane-Saulnier D 3800 fighters ordered from Germany, Italy and France respectively (the Moranes were license-built in Switzerland). The Swiss Air Force as an autonomous military service was created in October 1936.




Although Switzerland remained neutral throughout World War II, it had to deal with numerous violations of its airspace by combatants from both sides   initially by German aircraft, especially during their invasion of France in 1940. Zealous Swiss pilots attacked and shot down eleven German aircraft, losing two of their own, before a threatening memorandum from the German leadership forced General Guisan to forbid air combat above Swiss territory.
Later in the war, the Allied bomber offensive sometimes took US or British bombers into Swiss airspace, either damaged craft seeking safe haven or even on occasions bombing Swiss cities by accident. Swiss aircraft would attempt to intercept individual aircraft and force them to land, interning the crews. Only one further Swiss pilot was killed during the war, shot down by a US fighter in September 1944. From September red and white neutrality bands were added to the wings of aircraft to stop accidental attacks on Swiss aircraft by Allied aircraft.
Official Swiss records identify 6,501 airspace violations during the course of the war, with 198 foreign aircraft landing on Swiss territory and 56 aircraft crashing there.




After World War II the service was renamed the Swiss Air Force and Anti-Aircraft Command (Schweizerische Flugwaffe Kommando der Flieger und Fliegerabwehrtruppen) and in 1966 became a separate service independent from the Army, under its present name Schweizer Luftwaffe.
With the apparently imminent prospect of a new world war, this one involving nuclear weapons, Swiss military spending increased and jet aircraft were purchased: 75 De Havilland Vampires in 1950, quickly followed by over 100 De Havilland Venoms and the same number of Hawker Hunters. The Venoms served until 1983, while Vampires and Hunters continued in active service until 1990 and 1994 respectively. Switzerland was among those European countries that purchased the North American P-51 Mustang from US surplus stocks post World War 2. This aircraft initially was only intended as a stop-gap solution for the Swiss Air Force in order to maintain a capable defence force during a time when obsolete Bf-109E's and Swiss built D-3801 Morane fighters were due to be withdrawn from use, but the license production of the British designed Dh-100 Vampire and Dh-112 Venom jets was not in full swing yet.
At the end of the 1950s, reflecting both the threat of possible invasion by the Soviet Union and the realities of nuclear warfare, Swiss military doctrine changed to mobile defense that included missions for the air force outside of its territory, in order to defeat standoff attacks and nuclear threats, including the possibility of defensive employment of air-delivered nuclear weapons. However the inability to field an air force of sufficient capability to carry out such missions led to a return of traditional "protection of own territory" doctrine. Meanwhile, the Air Force also began to prepare ad-hoc airbases in the mountains, with sections of highway strengthened to act as runways and hangars carved out of the mountains.
In 1954 the first Air Radar Recruit School was activated, the first early warning radar systems were installed and the concept of command & control facilities at mountain summits was introduced; leading to acquisition of the FLORIDA early warning and command guidance system in 1965 followed by the current FLORAKO system in 2003. At the same time, ground-based air defence projects were initiated such as radar-equipped medium-caliber guns with an integrated 63 Superfledermaus (Superbat) fire control system' as well as the BL-64  Bloodhound  air defense missile system (1964 1999).
After the prototypes EFW N-20 and FFA P-16, Switzerland did not invest in development of its own combat aircraft. In 1964 the procurement of the Dassault Mirage III fighters (1964 2002) caused a scandal due to severe budget overruns. The air force commander, the chief of the general staff and the minister of defense were forced to resign, followed by a complete restructuring of the air force and air defense units as of February 1, 1968 and leading to separation of users and procurement officials.
In 1969, Air Force logistics and air defense were reassigned into brigades. The Armed Forces Meteo Group and Avalanche Rescue Service came under air force and air defense command and the Para Reconnaissance Company was established.
The 1970s were the years of historic major maneuvers with over 22,000 participants. Also a new air defense concept was introduced in which the air superiority fighter as opposed to a pure interceptor was central. In 1974 the first 2 Northrop F-5 Tiger fighters were tested and in 1978 the first F-5 Tiger fighter/interceptor squadron became operational. The F-5 is currently still operational but is scheduled to be replaced in 2018.




In the late 1980s the changing political and military world situations implied the need of a multirole aircraft in the Swiss Air Force. After evaluation, the performance of the F/A-18 Hornet was the decisive factor in its selection. Designed for carrier-borne operations, it was felt to be well suited to operations on short runways with steep takeoffs. Its radar allows the F/A-18 to detect and simultaneously engage multiple targets with long-range guided missiles.
Between 1996 and 1999, 34 licence-built Hornets left the assembly lines at Emmen. With a length of 17 metres, the F/A-18 is longer than the Mirage III. Its wingspan of 12 metres exceeds the F-5 Tiger s by 4 metres. Therefore, the existing caverns in the mountains had to be extended, a continuing process as of 2011. The Swiss F/A-18 weighs 17 tons, approximately 2.5  as much as the Tiger. It can easily load 7 tons, about 6x the useful load of the retired Hawker Hunter. The engines provide for a thrust of 16 tons, approximately 3.5  as much performance as the F-5 engines.



In 1995 the Swiss implemented a defensive plan that made control of Swiss airspace its highest and main priority. Modernization of the air force to achieve this mission was subject to popular referenda challenging its cost and practice.
The mission of the Swiss Air Force is as follows:
General control and protection of Swiss airspace.
Guaranteeing air sovereignty by means of air policing tasking.
Guaranteeing air defence throughout the country.
Capability of executing airlift ops.
Gathering and disseminating intelligence for political/military leadership.




Through the years, the Swiss Air Force traditionally had been a militia-based service, including its pilots, with an inventory of approximately 450 aircraft whose operational service life overlapped several eras. Beginning with its separation from the army in 1966 however, the air force has been down-sizing (currently approximately 230 fixed and rotary-wing aircraft) and moving toward a small professional cadre with fewer reserves and low-graduated conscripted personnel for general tasks. Currently the Swiss Air Force has a peacetime strength of 1600 professional military personnel with the ability to recall to about 20,000 reservists.
Its front-line air defence asset consists of 32 F-18 Hornets and 53 F-5 Tiger IIs (originally 110 purchased in 1978 85). The F/A-18 pilots are all full-time professional military; the F-5 pilots are largely reservists. These reservists are mostly airliner or freightliner pilots who also have an F-5 rating. During reserve duty periods, they are assigned to military duties and must refresh their operational live flying training. In 2008, the Swiss Hornet component reached the 50,000 flight hour milestone. All Swiss Hornets remain highly capable due to the Upgrade 21 (UG21) programme conducted between 2004 and 2009 at RUAG, while another Mid-Life Update (MLU) will begin shortly.
From 2011, the Air Force intended to start the Partial F-5 Tiger Replacement programme for 22 new aircraft. Candidate types were the JAS 39 Gripen, Eurofighter Typhoon and Dassault Rafale. On 30 November 2011, the Swiss government announced its decision to buy 22 Gripen NG fighters. The contract for the 22 aircraft was signed at 3.1 billion Swiss francs. On 25 August 2012, the order was confirmed by both the Swedish and Swiss authorities. The first aircraft were expected to be delivered in 2018, and the intention was to lease eleven current generation (8 JAS 39Cs/3 JAS 39Ds) Gripen fighters from 2016 to 2020 in order to train Swiss fighter pilots while avoiding expensive upkeep of the current F-5s. However, in a national referendum on 18 May 2014, 53.4% of Swiss voters voted against the purchase of JAS 39 Gripen. There are still plans by the Swiss Air Force and in the Swiss parliament to operate eighteen F-5E and four F-5F until 2018. This would also include the continued operation of the Patrouille Suisse on F-5E until 2018.
On 10 December 2010, the last 20 aging A rospatiale Alouette III were replaced by two VIP configuration Eurocopter EC135s and 18 EC635s. The first EC-635 was delivered in 2008.



In peacetime the Swiss Air Force provides ready-to-take-off aircraft only during office hours on working days. Air Force staff regarded a peacetime 24/7 operational-flying status as "mission impossible" due to budget limitations and limited professional (flying) personnel capacity. This did not apply to the air-defence radar coverage, which guarantees 24/7 peacetime operational capacity. One major problem in defending Swiss airspace is the small size of the country; the maximum extension of Switzerland is 348 km, a distance that commercial aircraft can fly in little over 20 minutes and military jets even more quickly. Noise-abatement issues have traditionally caused problems for the Air Force because of the tourist industry. Due to these reasons, the Swiss Air Force participates increasingly in air-defense training exercises with their Austrian,Italian, French or German counterparts. In recent years, this has included operations for the 2006 Winter Olympics in Turin, the Euro 2008 football championships and the annual World Economic Forum.
The Swiss Air Force was unable to respond to the Ethiopian Airlines ET702 hijacking, because it occurred outside normal office hours.



ADS15: As part of the Armament Program 2015, six Elbit Hermes 900 will replace by 2019 the remaining 15 RUAG Ranger ADS-95 that are still in service.
Pilatus PC-24: On the rollout of the Pilatus PC-24, the Minister of Defence announced that the PC-24 will replace the Swiss Air Force's Cessna Citation Excel.
Transport Aircraft: In 2015, minister of defence Ueli Maurer gave assurances that a transport aircraft purchase is planned by 2018. Initial competitors included the Lockheed C-130, Boeing C-17A Globemaster III, Alenia C-27, Airbus C-295 and Airbus A400M, but the C-17 has since ceased production. The C-130 is currently favoured.
BODLUV2020: The three anti-aircraft systems (Oerlikon 35 mm twin cannon, FIM-92 Stinger and Rapier missile) should be replaced by 2020 by two systems which will have their command & control connected to the FLORAKO System.
24h/365 QRA15: The Swiss Air Force will build up in 3 steps until 2020 a 24h 365 day QRA15 (15 Minutes from the alarm to the fighters are airborne) with fully armed F/A-18 fighter jets.




The 2009 Swiss air forces operational order of battle is as follows:
Bern Airport International Airpt.LSZB / LSMB
LTDB (VIP flights)

D bendorf Air Base LSMD (army airfield), a former fighter/interceptor base and current homebase of
Swiss Air Force Command
Air Defense & Direction Center (the air defense C3 airops center)
Skyguide (military & civil airtraffic control)
Berufsfliegerkorps
PC-7 Team Homebase

3rd Airlift Wing
3rd and 4th Air Transport Sqn with AS 332M-1/AS 532UL/EC635 P2+

Flugplatzkommando 2 Alpnach Air Base LSMA (airbase 2), a helicopter airlift/logistic base and maintenance unit.
2nd Airlift Wing; consisting of:
6th Air Transport Sqn with AS 332M-1/AS 532UL/EC 635 P2+
8th Air Transport Sqn with AS 332M-1/AS 532UL/EC 635 P2+

Milit rflugplatz Buochs AirportLSZC (army airfield), a deactivated former air defense base. Currently marked as "sleeping airbase" open for civil and sport aviation. In time of crisis the military airfield installations may be made operational at very short notice as there still are aircraft shelter facilities inside the nearby mountain ridge.
Flugplatzkommando 4 Locarno Air Base LSMO (airbase 4), a flight training base (PC-6/7/9's); Para Recon Training base, Light Airlift base
Flugplatz Ambri Airport, LSPM a former air defense base hosting Hawker Hunters. Currently deactivated and open for commercial flights. The aircraft storage facilities inside the mountain ridge are empty but still available.
Flugplatz Interlaken Airport LSMI, a former fighter/interceptor base hosting 12 F-5 Tigers. Currently deactivated and closed for all air traffic; converted into a leisure area.
Flugplatzkommando 7 Emmen Air Base LSME (airbase 7), a fighter/interceptor base hosting
Dronensqn (Dronesqn) 7 with ADS-95
7th Air Transport Sqn with Pilatus PC-6
12th Air Target Sqn with Pilatus PC-9/F-5E
24th Sqn The ECM Sqn use the Ericson Vista5 ECM on F-5F and PC-9
Patrouille Suisse Homebase
GRD Armasuisse Homebase
Advanced Pilot training base with Pilatus PC-21

Flugplatz Lodrino Air Base LSML, a former air defense base hosting Hawker Hunters. Currently deactivated and open for commercial flights, only used by PC-6 for Par Recon Training, alternate Airport for PC-7 and Helicopters if landing in Locarno is not possible.
Flugplatz MollisLSMF, a former air defense base hosting Hawker Hunters. Currently deactivated and open for commercial flights. Aircraft storage facilities inside the mountain ridge are empty but still available.
Flugplatzkommando 1 Payerne Air Base LSMP (airbase 1), an air defense base hosting
Escadrille Transport A rien (Airliftsqn) 1 and 5th Air Transport Sqn with Super Puma and AS 532UL
Fightersqn 6 with F-5 Tiger
Fightersqn 17 and 18 with F/A-18C/D
Flight Training Unit 12

Flugplatzkommando 13 Meiringen air base LSMM (airbase 13), an air defense base hosting
Fliegerstaffel (Fighter/Interceptorsqn) 8, with F-5 Tiger
Fightersqn 11 with F/A-18C/D

Flugplatzkommando 14 Sion Airport LSGS / LSMS (airbase 14), a fighter/interceptor base hosting
Fighter/Interceptorsqn 19 with F-5E Tiger
Fighter/Trainersqn 16 with F-5F Sharing them with FlSt 24

About the squadrons see: Swiss Air Force aircraft squadrons




During the past 35 years, Swiss military and civil airspace control depended on the FLORIDA (FLugsicherungs Operations Radar IDentifikation Alarm   Flight Ops, Radar Identifying, and Alerting) air defense system.
Since its phasing out, however, the Swiss airspace control and defence is being carried out by the THALES Raytheon FLORAKO. This system is being operated from 4 fixed locations at the summits of the Pilatus, Scopi, Weisshorn and Weissfluh mountains in the Alps.
At least one of these Command, Control, and Communications (C3) facilities is always connected to the Air Defense & Direction Center (ADDC or air ops center) at D bendorf and fully operational on-line on a 24/7 basis, controlling Swiss airspace. Depending on the international situation, more facilities will be manned; in case of crisis or war (ADDC and 4 facilities operational) the coverage will be extended far beyond the Swiss boundaries. Each of these facilities is capable of making all battle management decisions if ADDC or the other facilities were eliminated.
The first FLORAKO unit was activated in 2003 and the operational lifetime of this hi-tech system is guaranteed by its manufacturers for at least 25 years. The system consists of:
A communication system KOMSYS. Integrating element of all geographically divided parts of the FLORAKO system uniting speech, data communications, and system commands in a single data network.
A radar station FLORES. Consisting of standard high-power search radars, advanced radars (search mode, high-update ratio, and special functions), and civil authority monopulse secondary radars. The 4 radar stations are the main data sources and are complemented by existing military and civil radar data.
A radar layer-system RALUS. Translating the data automatically into flight paths and producing a complete civil-military air picture for all authorities.
A warning message system LUNAS-EZ. AirOps Centers are the combining factors between the FLORAKO-system with real-time data (air picture, planning, and environmental data) and its military users. Workstations are identically configured and built accordingly to latest ergonomics, visual colour high resolution, menu guidance, and known user environment. The D bendorf Air Defense & Direction Center   as well as the air operations units in the Alps   are equally equipped, thus assuring full-time operational redundancy in
producing the actual airpicture
permanent defence of Swiss airspace
early warning
command and control
air policing
coordination of civil and military air traffic

The Military-Civil Airspace Management System MICAMS. This secondary system provides a computing backup for flexible airspace use for both civil and military flight security.
The radar system may eventually be completed by 2 mobile TAFLIR (TAktische FLIeger Radars   Tactical Flight Radars). These Ground Master 200 type AN/MPQ-64 radars are a variant of the Northrop Grumman AN/TPS-75 and are deployable in areas of difficult terrain or where specific coverage is needed. Peacetime TAFLIR deployment locations are at D bendorf and Emmen. In time of crisis or at war they can be deployed anywhere.



In Switzerland (including the airspace of Liechtenstein) military air surveillance is also called Permanent Air Surveillance (Pl ). This ensures uninterrupted 24/365 coverage with the FLORAKO system, wherein the IDO (Identifications Officer) and the TM (Track Monitor) monitor and represented the air situation as Recognized Air Picture.
The Swiss Air Force has several operational centers. In peacetime, the primary military command center is located at D bendorf airfield, in the same building the civilian air traffic control Skyguide uses. The locations of the other operational centers are secret. The command centers are part of the unit "Einsatz Luftwaffe," the chief of which is directly subordinate to the commander of the Air Force. It consists of the operations center of the Air Force, redundant direct connections to the emergency organizations (air rescue and federal police), as well as to the two Skyguide air traffic centers (Geneva and Zurich), and to the relevant military and civilian air traffic control centers of neighboring countries.
Currently the sky is continuously monitored, but intervention resources are usually available only on weekdays during the day. Usually, increased availability of resources is limited to major exercises, international conferences (WEF), or crises (e.g. the Libyan Civil War in 2011). This heightened state is called Pl  + (Pl  PLUS) or ILANA. The Swiss Federal Assembly has adopted a requirement that armed interceptors are to be ready 24 hours a day, but the Federal Council has not released the necessary funding. Meeting the parliamentary requirement would require increased operations at two air bases as well as modifications to civilian sites in Geneva and Z rich. This objective not is expected be met until 2017.



The Ground Based Air Defence (GBAD) is currently headquartered at Emmen Airbase. Since the deactivation of the former BL-64 "Bloodhound" missile system it achieves its task by operating a triple combined mobile coverage system consisting of:
Rapier the mobile 10 km range surface-to-air missile system with a four-missile launcher and related command and control vehicles.
FIM-92 Stinger MANPAD 4.8 km range surface-to-air missile systems (fire & forget system) normally with a related Stinger Alert short range radar.
Oerlikon 35 mm twin cannons 4 km range firing unit normally operates with a Skyguard firecontrol system with 15 km detection range.



Despite the persistent lack of continuous availability of armed fighter jets for the entire year, the Swiss Air Force regularly conducts the air policing. Contrary to popular perception, air policing is one of the most complex and challenging tasks of the Air Force. Unknown aircraft have to be identified within a short time and in all weather conditions without error margin and intercepted if necessary. Air policing is performed daily and is always a real commitment and not on the sideline of things happening in parallel (e.g. air combat exercises). Air policing would ensure the control and sovereignty of Swiss airspace and security in air transport. Thus, the Air Force not only carries out the tasks of an independent state (if one includes Liechtenstein: states) but also acts for the benefit of civil aviation (Federal Office of Civil Aviation & Skyguide). In air policing, aircraft will either be intercepted by the Swiss Air Force and visually inspected; it is checked whether the aircraft complies with the timetable specified information (type, registration, operator) and whether abnormal characteristics are apparent. Or aircraft are followed and observed to see if the pilot complies with air traffic rules (sink rate, speed, type, weather conditions, etc. as appropriate).
Active air policing interventions
Help for civilian aircraft, for example problems with navigation and radio
"Visible" (escort) to make an airplane with a faulty transponder visible for civil ATC
Finding from flares with forwarding to emergency organizations (e.g. REGA)
Identification of airspace violations as not allowed ingress or schedule disruptions
Intervention to monitor safety where VFR aircraft enters airways or the arrival & departure corridors of airports
Enforcing the use of airspace restrictions (e.g. WEF, G8)
Monitor airspace in hijacking events
Management of crisis situations (e.g. approximation of enemy/terrorist aircraft at the border)



The Swiss Air Force met along with these tasks with their EQUIPMENT and staff a variety services for various other organizations. It provides one of the secondary FLORAKO radar civilian Skyguide with radar data and enables a safe air traffic management. Air Force helicopters and drones regularly conduct surveillance flights for the Border Guard Corps GWK, are also for surveillance flights (e.g. Street Parade) and searching flights for the benefit of the police and the Rega (air rescue). Also in support of the Fire Department for fire fighting where there drones and helicopters with FLIR used to locating nests of fire in forest fires. The helicopter of the Swiss Air Force can be used with the Bambibucket as extinguishing agents at home and abroad, the largest fire fighting operation was with three Super Puma in Israel. Three helicopters are currently stationed for the Swisscoy in support of KFOR in Kosovo. Or are used in large-scale events for relief abroad (e.g. Sumatra after the tsunami). For the Federal Office of Public Health, National Emergency Operations Centre and the Air Force conducts regular ENSI with helicopters and F-5 by air data collection and radioactivity measurements. With F-5 as part of the ARES program parabolic flights in favor of the ETH Zurich and other research institutions are carried out. In addition, the Air Force modified all-diplomatic clearance requests that are filed outside the opening times of the FOCA and represents the REGA (Swiss Air Rescue) communication systems available. The air base command 13 of Meiringen care in his office in addition to the resources of the Belp LTDB the aircraft stationed there by the Federal Office for Civil Aviation (FOCA).









   












A number of air defence systems have been offered by Swiss companies and trialled by the Swiss Air Force but in the event not purchased.



The air force has a number of aerobatic teams and solo display aircraft that are used to represent the Swiss Air Force at events around Europe:
Patrouille Suisse - A jet aerobatic team that originally formed in 1964 with four Hawker Hunters, since 1996 it uses six Northrop F-5E in a red and white livery; the aircraft have been fitted with smoke generators.
PC-7 Team - A turboprop display team The Swiss Air Force also has the PC-7 Team that uses nine Pilatus PC-7s turboprop trainers.
Hornet Solo Display - A single F/A-18 Hornet is flown as the Hornet Solo Display
Super Puma Display Team - A single Eurocopter AS332 Super Puma helicopter is flown as the Super Puma Display Team
The Parachute Reconnaissance Company 17 performs parachute displays as the Air Show Team.



The Swiss Air Force military aircraft are identified by a role prefix and number, the prefix or code identifies the role and the serial numbers the type or variant, the system was introduced in 1936.
Letter code
The letter or letters give the role of the aircraft.
This is followed by a number having from two to four digits.
Four-digit numbers
The first digit identifies the aircraft type. The next three are for the sub-type and the individual aircraft, with the first and sometimes second for the subtype; and the third and sometimes fourth for the individual aircraft, In the following examples, "x" identifies the individual aircraft:
Mirage IIIBS = J-200x
Mirage IIIDS = J-201x
Mirage IIIRS = R-21xx
Mirage IIIC = J-22xx
Mirage IIIS = J-23xx
F-5E = J-30xx (serials previously used for the FFA P-16)
F-5F = J-32xx
Three-digit numbers
Most aircraft have three numbers. These follow a broadly similar pattern to the four-digit numbers, although there are exceptions.
Transport aircraft have a first digit of 3 for helicopters and 7 for fixed wing aircraft.
Two-digit numbers
Target drones have only two numbers.



With the threat of the Second World War and the possible need for the army and civilian population to retreat into the mountains (Reduie) Guisan, it was clear that the Air Force needed the ability to attack enemy ground forces in the mountains. To practice this Axalp was selected. After the Second World War ground attack by Vampire, Venom and Hunter jet aircraft was practiced at Axalp, including cannon and napalm bomb exercises. During the Cold War, military liaison officers from western, eastern and non-aligned nations were invited to the screenings. Nowadays Axalpfliegerschiessen ("Airshow Axalp") is a performance by the Swiss Air Force in the mountains for anyone interested. It is the only event where civilians (regardless of nationality) can see an airshow at 1,700 m (5,600 ft) above sea level and see the live use of aircraft cannons. The use of helicopters in the mountains and at high altitudes, search & rescue and firefighting demonstrations have become a large part of the Axalp air show. Because of the AIR 14 airshow, there will be no Axalp air force live fire event in 2014.



In 2014, the Swiss Air Force celebrated its 100th anniversary and the 50th anniversary of the Patrouille Suisse and the 25th anniversary of the PC 7Team. The main focus of the celebration was an airshow Air14 at Payerne (30 31 August & 6 7 September).


