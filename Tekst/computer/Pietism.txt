Pietism (/ pa t sm/, from the word piety) was a movement within Lutheranism that began in the late 17th century, reached its zenith in the mid-18th century, and declined through the 19th century, and had almost vanished in America by the end of the 20th century. While declining as an identifiable Lutheran group, some of its theological tenets influenced Protestantism and Anabaptism generally, inspiring the Anglican priest John Wesley to begin the Methodist movement and Alexander Mack to begin the Brethren movement. The Pietist movement combined the Lutheranism of the time with the Reformed emphasis on individual piety and living a vigorous Christian life. Though pietism shares an emphasis on personal behavior with the Puritan movement, and the two are often confused, there are important differences, particularly in the concept of the role of religion in government.



As the forerunners of the Pietists in the strict sense, certain voices had been heard bewailing the shortcomings of the Church and advocating a revival of practical and devout Christianity. Amongst them were the Christian mystic Jakob B hme (Behmen); Johann Arndt, whose work, True Christianity, became widely known and appreciated; Heinrich M ller, who described the font, the pulpit, the confessional and the altar as "the four dumb idols of the Lutheran Church"; the theologian Johann Valentin Andrea, court chaplain of the Landgrave of Hesse; Schuppius, who sought to restore the Bible to its place in the pulpit; and Theophilus Grossgebauer (d. 1661) of Rostock, who from his pulpit and by his writings raised what he called "the alarm cry of a watchman in Sion".



The direct originator of the movement was Philipp Jakob Spener. Born at Rappoltsweiler in Alsace, now in France, on 13 January 1635, trained by a devout godmother who used books of devotion like Arndt's True Christianity, Spener was convinced of the necessity of a moral and religious reformation within German Lutheranism. He studied theology at Strasbourg, where the professors at the time (and especially Sebastian Schmidt) were more inclined to "practical" Christianity than to theological disputation. He afterwards spent a year in Geneva, and was powerfully influenced by the strict moral life and rigid ecclesiastical discipline prevalent there, and also by the preaching and the piety of the Waldensian professor Antoine Leger and the converted Jesuit preacher Jean de Labadie.
During a stay in T bingen, Spener read Grossgebauer's Alarm Cry, and in 1666 he entered upon his first pastoral charge at Frankfurt with a profound opinion that the Christian life within Evangelical Lutheranism was being sacrificed to zeal for rigid Lutheran orthodoxy. Pietism, as a distinct movement in the German Church, began with religious meetings at Spener's house (collegia pietatis) where he repeated his sermons, expounded passages of the New Testament, and induced those present to join in conversation on religious questions. In 1675, Spener published his Pia desideria or Earnest Desire for a Reform of the True Evangelical Church, the title giving rise to the term "Pietists". This was originally a pejorative term given to the adherents of the movement by its enemies as a form of ridicule, like that of "Methodists" somewhat later in England.
In Pia desideria, Spener made six proposals as the best means of restoring the life of the Church:
The earnest and thorough study of the Bible in private meetings, ecclesiolae in ecclesia ("little churches within the church")
The Christian priesthood being universal, the laity should share in the spiritual government of the Church
A knowledge of Christianity must be attended by the practice of it as its indispensable sign and supplement
Instead of merely didactic, and often bitter, attacks on the heterodox and unbelievers, a sympathetic and kindly treatment of them
A reorganization of the theological training of the universities, giving more prominence to the devotional life
A different style of preaching, namely, in the place of pleasing rhetoric, the implanting of Christianity in the inner or new man, the soul of which is faith, and its effects the fruits of life
This work produced a great impression throughout Germany. While large numbers of orthodox Lutheran theologians and pastors were deeply offended by Spener's book, many other pastors immediately adopted Spener's proposals.




In 1686 Spener accepted an appointment to the court-chaplaincy at Dresden, which opened to him a wider though more difficult sphere of labor. In Leipzig, a society of young theologians was formed under his influence for the learned study and devout application of the Bible. Three magistrates belonging to that society, one of whom was August Hermann Francke, subsequently the founder of the famous orphanage at Halle (1695), commenced courses of expository lectures on the Scriptures of a practical and devotional character, and in the German language, which were zealously frequented by both students and townsmen. The lectures, however, aroused the ill-will of the other theologians and pastors of Leipzig, and Francke and his friends left the city, and with the aid of Christian Thomasius and Spener founded the new University of Halle. The theological chairs in the new university were filled in complete conformity with Spener's proposals. The main difference between the new Pietistic Lutheran school and the orthodox Lutherans arose from the Pietists' conception of Christianity as chiefly consisting in a change of heart and consequent holiness of life. Orthodox Lutherans rejected this viewpoint as a gross simplification, stressing the need for the church and for sound theological underpinnings.
Spener died in 1705, but the movement, guided by Francke and fertilized from Halle, spread through the whole of Middle and North Germany. Among its greatest achievements, apart from the philanthropic institutions founded at Halle, were the revival of the Moravian Church in 1727 by Count von Zinzendorf, Spener's godson and a pupil in the Halle School for Young Noblemen, and the establishment of Protestant missions.
Spener stressed the necessity of a new birth and separation of Christians from the world, (see Asceticism). Many Pietists maintained that the new birth always had be preceded by agonies of repentance, and that only a regenerated theologian could teach theology. The whole school shunned all common worldly amusements, such as dancing, the theatre, and public games. Some believe this led to a new form of justification by works. Its ecclesiolae in ecclesia also weakened the power and meaning of church organization. These Pietistic attitudes caused a counter-movement at the beginning of the 18th century; one leader was Valentin Ernst L scher, superintendent at Dresden.



Authorities within state-endorsed religions were suspicious of pietist doctrine which they often viewed as a social danger, as it "seemed either to generate an excess of evangelical fervor and so disturb the public tranquility or to promote a mysticism so nebulous as to obscure the imperatives of morality. A movement which cultivated religious feeling almost as an end itself". While some pietists (such as Pastor Francis Magny) held that "mysticism and the moral law went together", for others (like his pupil Fracoise-Louise de la Tour) "pietist mysticism did less to reinforce the moral law than to take its place...the principle of 'guidance by inner light' was often a signal to follow the most intense of her inner sentiments...the supremacy of feeling over reason". Religious authorities could bring pressure on pietists, such as when they brought some of Magny's followers before the local consistory to answer questions about their unorthodox views or when they banished Pastor Magny from Vevey for heterodoxy in 1713.




As a distinct movement, Pietism had its greatest strength by the middle of the 18th century; its very individualism in fact helped to prepare the way for the Enlightenment (Aufkl rung), which took the church in an altogether different direction. Yet some claim that Pietism contributed largely to the revival of Biblical studies in Germany and to making religion once more an affair of the heart and of life and not merely of the intellect. It likewise gave a new emphasis to the role of the laity in the church. Rudolf Sohm claimed that "It was the last great surge of the waves of the ecclesiastical movement begun by the Reformation; it was the completion and the final form of the Protestantism created by the Reformation. Then came a time when another intellectual power took possession of the minds of men." Dietrich Bonhoeffer of the German Confessing Church framed the same characterization in less positive terms when he called Pietism the last attempt to save Christianity as a religion: Given that for him religion was a negative term, more or less an opposite to revelation, this constitutes a rather scathing judgment. Bonhoeffer denounced the basic aim of Pietism, to produce a "desired piety" in a person, as unbiblical.
Pietism is considered the major influence that led to the creation of the "Evangelical Church of the Union" in Prussia in 1817. The King of Prussia ordered the Lutheran and Reformed churches in Prussia to unite; they took the name "Evangelical" as a name both groups had previously identified with. This union movement spread through many German lands in the 1800s. Pietism, with its looser attitude toward confessional theology, had opened the churches to the possibility of uniting. The unification of the two branches of German Protestantism sparked the Schism of the Old Lutherans. Many Lutherans, called Old Lutherans formed free churches or immigrated to the United States and Australia, where they formed bodies that would later become the Lutheran Church Missouri Synod and the Lutheran Church of Australia, respectively. (Many immigrants to America, who agreed with the union movement, formed German Evangelical Lutheran and Reformed congregations, later combined into the Evangelical Synod of North America, which is now a part of the United Church of Christ.)
Pietism was a major influence on John Wesley and others who began the Methodist movement in 18th century Great Britain. John Wesley was influenced significantly by Moravians (e.g., Zinzendorf, Peter Boehler) and Pietists connected to Francke and Halle Pietism. The fruit of these Pietist influences can be seen in the modern American Methodists and members of the Holiness movement.
Pietism did not die out in the 18th century, but was alive and active in the Evangelischer Kirchenverein des Westens (later German Evangelical Church and still later the Evangelical and Reformed Church.) The church president from 1901 to 1914 was a pietist named Dr. Jakob Pister. Some vestiges of Pietism were still present in 1957 at the time of the formation of the United Church of Christ.
However, in the 19th century, there was a revival of confessional Lutheran doctrine, known as the neo-Lutheran movement. This movement focused on a reassertion of the identity of Lutherans as a distinct group within the broader community of Christians, with a renewed focus on the Lutheran Confessions as a key source of Lutheran doctrine. Associated with these changes was a renewed focus on traditional doctrine and liturgy, which paralleled the growth of Anglo-Catholicism in England.
Some writers on the history of Pietism   e.g. Heppe and Ritschl   have included under it nearly all religious tendencies amongst Protestants of the last three centuries in the direction of a more serious cultivation of personal piety than that prevalent in the various established churches. Ritschl, too, treats Pietism as a retrograde movement of Christian life towards Catholicism. Some historians also speak of a later or modern Pietism, characterizing thereby a party in the German Church probably influenced by remains of Spener's Pietism in Westphalia, on the Rhine, in W rttemberg, Halle, and Berlin.
The party was chiefly distinguished by its opposition to an independent scientific study of theology, its principal theological leader being Hengstenberg, and its chief literary organ the Evangelische Kirchenzeitung.
Pietism also had a strong influence on contemporary artistic culture in Germany; though unread today, the Pietist Johann Georg Hamann held a strong influence in his day. Pietist belief in the power of individual meditation on the divine   a direct, individual approach to the ultimate spiritual reality of God   was probably partly responsible for the uniquely metaphysical, idealistic nature of German Romantic philosophy.



Pietism had an influence on American religion, as many German immigrants settled in Pennsylvania, New York and other areas. Its influence can be traced in Evangelicalism. Balmer says that:

Evangelicalism itself, I believe, is quintessentially North American phenomenon, deriving as it did from the confluence of Pietism, Presbyterianism, and the vestiges of Puritanism. Evangelicalism picked up the peculiar characteristics from each strain   warmhearted spirituality from the Pietists (for instance), doctrinal precisionism from the Presbyterians, and individualistic introspection from the Puritans   even as the North American context itself has profoundly shaped the various manifestations of evangelicalism: fundamentalism, neo-evangelicalism, the holiness movement, Pentecostalism, the charismatic movement, and various forms of African-American and Hispanic evangelicalism.

Bartholom us Ziegenbalg (10 July 1682   23 February 1719) was a member of the Lutheran clergy and the first Pietist missionary to India.







 "Pietism". Catholic Encyclopedia. New York: Robert Appleton Company. 1913. 
 This article incorporates text from a publication now in the public domain: Chisholm, Hugh, ed. (1911). Encyclop dia Britannica (11th ed.). Cambridge University Press. 



Brown, Dale: Understanding Pietism, rev. ed. Nappanee, IN: Evangel Publishing House, 1996.
Brunner, Daniel L. Halle Pietists in England: Anthony William Boehm and the Society for Promoting Christian Knowledge. Arbeiten zur Geschichte des Pietismus 29. G ttingen, Germany: Vandenhoeck and Ruprecht, 1993.
Olson, Roger E., Christian T. Collins Winn. Reclaiming Pietism: Retrieving an Evangelical Tradition (Eerdmans Publishing Company, 2015). xiii + 190 pp. online review
Shantz, Douglas H. An Introduction to German Pietism: Protestant Renewal at the Dawn of Modern Europe. Baltimore: Johns Hopkins University Press, 2013.
Stoeffler, F. Ernest. The Rise of Evangelical Pietism. Studies in the History of Religion 9. Leiden: E.J. Brill, 1965.
Stoeffler, F. Ernest. German Pietism During the Eighteenth Century. Studies in the History of Religion 24. Leiden: E.J. Brill, 1973.
Stoeffler, F. Ernest. ed.: Continental Pietism and Early American Christianity. Grand Rapids, MI: Eerdmans, 1976.
Winn, Christian T. et al. eds. The Pietist Impulse in Christianity (2012)



Joachim Feller, Sonnet. In: Luctuosa desideria Quibus [ ] Martinum Bornium prosequebantur Quidam Patroni, Praeceptores atque Amici. Lipsiae [1689], pp. [2] [3]. (Facsimile in: Reinhard Breymayer (Ed.): Luctuosa desideria. T bingen 2008, pp. 24 - 25.) Here for the first time the newly detected source.   Less exactly cf. Martin Brecht: Geschichte des Pietismus, vol. I, p. 4.
Johann Georg Walch, Historische und theologische Einleitung in die Religionsstreitigkeiten der evangelisch-lutherischen Kirche (1730);
Friedrich August Tholuck, Geschichte des Pietismus und des ersten Stadiums der Aufkl rung (1865);
Heinrich Schmid, Die Geschichte des Pietismus (1863);
Max Goebel, Geschichte des christlichen Lebens in der Rheinisch-Westf lischen Kirche (3 vols., 1849 1860).
The subject is dealt with at length in
Isaak August Dorner's and W Gass's Histories of Protestant theology.
Other works are:
Heinrich Heppe, Geschichte des Pietismus und der Mystik in der reformierten Kirche (1879), which is sympathetic;
Albrecht Ritschl, Geschichte des Pietismus (5 vols., 1880 1886), which is hostile; and
Eugen Sachsse, Ursprung und Wesen des Pietismus (1884).
See also
Friedrich Wilhelm Franz Nippold's article in Theol. Stud. und Kritiken (1882), pp. 347?392;
Hans von Schubert, Outlines of Church History, ch. xv. (Eng. trans., 1907); and
Carl Mirbt's article, "Pietismus," in Herzog-Hauck's Realencyklop die f r prot. Theologie u. Kirche, end of vol. xv.
The most extensive and current edition on Pietism is the four-volume edition in German, covering the entire movement in Europe and North America
Geschichte des Pietismus (GdP)
Im Auftrag der Historischen Kommission zur Erforschung des Pietismus herausgegeben von Martin Brecht, Klaus Deppermann, Ulrich G bler und Hartmut Lehmann
(English: On behalf of the Historical Commission for the Study of pietism edited by Martin Brecht, Klaus Deppermann, Ulrich Gaebler and Hartmut Lehmann)
Band 1: Der Pietismus vom siebzehnten bis zum fr hen achtzehnten Jahrhundert. In Zusammenarbeit mit Johannes van den Berg, Klaus Deppermann, Johannes Friedrich Gerhard Goeters und Hans Schneider hg. von Martin Brecht. Goettingen 1993. / 584 p.
Band 2: Der Pietismus im achtzehnten Jahrhundert. In Zusammenarbeit mit Friedhelm Ackva, Johannes van den Berg, Rudolf Dellsperger, Johann Friedrich Gerhard Goeters, Manfred Jakubowski-Tiessen, Pentii Laasonen, Dietrich Meyer, Ingun Montgomery, Christian Peters, A. Gregg Roeber, Hans Schneider, Patrick Streiff und Horst Weigelt hg. von Martin Brecht und Klaus Deppermann. Goettingen 1995. / 826 p.
Band 3: Der Pietismus im neunzehnten und zwanzigsten Jahrhundert. In Zusammenarbeit mit Gustav Adolf Benrath, Eberhard Busch, Pavel Filipi, Arnd G tzelmann, Pentii Laasonen, Hartmut Lehmann, Mark A. Noll, J rg Ohlemacher, Karl Rennstich und Horst Weigelt unter Mitwirkung von Martin Sallmann hg. von Ulrich G bler. Goettingen 2000. / 607 p.
Band 4: Glaubenswelt und Lebenswelten des Pietismus. In Zusammenarbeit mit Ruth Albrecht, Martin Brecht, Christian Bunners, Ulrich G bler, Andreas Gestrich, Horst Gundlach, Jan Harasimovicz, Manfred Jakubowski-Tiessen, Peter Kriedtke, Martin Kruse, Werner Koch, Markus Matthias, Thomas M ller Bahlke, Gerhard Sch fer ( ), Hans-J rgen Schrader, Walter Sparn, Udo Str ter, Rudolf von Thadden, Richard Trellner, Johannes Wallmann und Hermann Wellenreuther hg. von Hartmut Lehmann. Goettingen 2004. / 709 p.



New Schaff-Herzog Encyclopedia of Religious Knowledge, Vol. IX: Pietism
After Three Centuries   The Legacy of Pietism by E.C. Fredrich
Literary Landmarks of Pietism by Martin O. Westerhaus
Pietism's World Mission Enterprise by Ernst H. Wendland
The Evangelical Pietist Church of Chatfield