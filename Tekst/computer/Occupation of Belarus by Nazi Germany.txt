The occupation of Belarus by Nazi Germany started with the German invasion of the Soviet Union on June 22, 1941 (Operation Barbarossa) and ended in August 1944 with the Soviet Operation Bagration. The western parts of the Byelorussian SSR (as of 1940) became part of the Reichskommissariat Ostland in 1941, but in 1943 the German authorities allowed local collaborators to set up a client state, the Belarusian Central Rada, that would last until the Soviets regained control of the region.




The Soviet and Belarusian historiographies study the subject of German occupation in the context of contemporary Belarus, regarded as the Byelorussian Soviet Socialist Republic (BSSR), a constituent republic of the Soviet Union in the 1941 borders as a whole. Polish historiography insists on special, even separate treatment for the East Lands of the Poland in the 1921 borders (alias "Kresy Wschodnie" alias West Belarus), which were incorporated into the BSSR after the Soviet invaded Poland on September 17, 1939. More than 100,000 people in West Belarus were imprisoned, executed or transported to the eastern USSR by Soviet authorities before the German invasion. The NKVD (Soviet secret police) killed more than 1,000 prisoners in June/July 1941, for example, in Chervyen, Hlybokaye and Vileyka. These crimes stoked anti-Communist feelings in the Belarusian population and were used by Nazi anti-Semitic propaganda.




After twenty months of Soviet rule in Western Belarus and Western Ukraine, Nazi Germany and its Axis allies invaded the Soviet Union on June 22, 1941. Eastern Belarus suffered particularly heavily during the fighting and German occupation. Following bloody encirclement battles, all of the present-day Belarus territory was occupied by the Germans by the end of August 1941. With Poland regarding the Soviet annexation as illegal, the majority of Polish citizens didn't ask for Soviet citizenship from 1939 to 1941, and as a result were Polish citizens under Soviet and later Nazi occupation.




In the early days of the occupation, a powerful and increasingly well-coordinated Soviet partisan movement emerged. Hiding in the woods and swamps, the partisans inflicted heavy damage to German supply lines and communications, disrupting railway tracks, bridges, telegraph wires, attacking supply depots, fuel dumps and transports, and ambushing Axis soldiers. Not all of the anti-German partisans were pro-Soviet. In the largest partisan sabotage action of the entire Second World War, the so-called Asipovichy diversion of July 30, 1943, four German trains with supplies and Tiger tanks were destroyed. To fight partisan activity, the Germans had to withdraw considerable forces behind their front line. On June 22, 1944, the huge Soviet Strategic Offensive Operation Bagration was launched, finally regaining all of Belarus by the end of August.




Germany imposed a brutal regime, deporting some 380,000 people for slave labour, and killing hundreds of thousands of civilians more. At least 5,295 Belarusian settlements were destroyed by the Nazis and some or all their inhabitants killed (out of 9,200 settlements that were burned or otherwise destroyed in Belarus during World War II). More than 600 villages like Khatyn were annihilated with their entire population. Altogether, 2,230,000 people were killed in Belarus during the three years of German occupation.




14th Waffen Grenadier Division of the SS Galicia (1st Ukrainian)
29th Waffen Grenadier Division of the SS RONA (1st Russian)
30th Waffen Grenadier Division of the SS (1st Belarussian)
30th Waffen Grenadier Division of the SS (2nd Russian)
36th Waffen Grenadier Division of the SS
Arajs Kommando
Einsatzgruppen
Lithuanian Security Police
Lithuanian Territorial Defense Force
Nachtigall Battalion
Organization of Ukrainian Nationalists
Ypatingasis b rys



Erich von dem Bach-Zelewski
Gustavs Celmi 
Oskar Dirlewanger
Curt von Gottberg
Konr ds Kal js
Bronislav Kaminski
Wilhelm Kube
Anthony Sawoniuk



36th Waffen Grenadier Division of the SS#Belarus
Arturs Spro is#Soviet partisan
Army Group Centre#Early anti-partisan campaign
Battle of Murowana Oszmianka
Belarusian partisans
Belarusian resistance during World War II
Collaboration during World War II#Belarus
Consequences of German Nazism#Belarus
Holocaust in Belarus
Latvian partisans#Anti-Nazi
List of Axis named operations in the European Theatre
Lithuanian Territorial Defense Force
Military history of Belarus during World War II#Belarusian Anti-Soviet commanders
P rkonkrusts
Soviet partisans#Belarus
Ukrainian-German collaboration during World War II




The largest Jewish ghetto in Soviet Belarus before the conclusion of World War II was the Minsk Ghetto. Almost the whole, previously numerous Jewish population of Belarus which did not evacuate east ahead of the German advance was killed during the Holocaust by bullet. The list of eradicated Jewish ghettos in Nazi-Soviet occupied Poland extending eastward toward the border with the Soviet Belarus can be found at the Jewish ghettos in German-occupied Poland article.



Later in 1944, 30 German-trained Belarusians were airdropped behind the Soviet front line to spark disarray. These were known as " orny Kot" ("Black Cat") led by Micha  Vitu ka. They had some initial success due to disorganization in the rear guard of Red Army. Other Belarusian units slipped through Bia owie a Forest and full scale guerilla war erupted in 1945. But the NKVD infiltrated these units and neutralized them until 1957.
In total, Belarus lost a quarter of its pre-war population in the Second World War, including practically all its intellectual elite. About 9,200 villages and 1,200,000 houses were destroyed. The major towns of Minsk and Vitebsk lost over 80% of their buildings and city infrastructure. For the defense against the Germans, and the tenacity during the German occupation, the capital Minsk was awarded the title Hero City after the war. The fortress of Brest was awarded the title Hero-Fortress.


