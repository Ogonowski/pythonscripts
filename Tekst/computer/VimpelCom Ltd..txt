VimpelCom Ltd. is a global provider of telecommunication services incorporated in Bermuda and headquartered in Amsterdam. It is the sixth largest mobile network operator in the world by subscribers (as of April 2012) with 214 million customers (as per December 31, 2012) in 18 countries. Most of the company's revenue comes from Russia (39%) and Italy (31%).
VimpelCom's brands include "Beeline" (in Russia and CIS), "Kyivstar" (in Ukraine), "Wind" (in Italy and Greece), "Djezzy" (in Algeria), "Mobilink" (in Pakistan), "Banglalink" (in Bangladesh), and others.



VimpelCom Ltd. Shareholder Structure:
47.9% of voting rights (56.2% Economic rights) owned by Altimo (Alfa Group)
43.0% of voting rights (33.0% Economic rights) owned by Telenor
9.2% (10.8%)   Minority Shareholders



OJSC VimpelCom (100%)
Golden Telecom
Corbina Telecom
Sovintel
Sovam teleport
OJSC  New telephone company 

Kyivstar (100%)
Wind Telecom S.p.A.
WIND Telecomunicazioni S.p.A. (100%)
Global Telecom Holding S.A.E. (51.7%)

Euroset (50%)




Russian OJSC VimpelCom was founded in 1992 in Moscow when VimpelCom's co-founders, Dmitry Zimin and American Augie K. Fabela II came together to pioneer the Russian mobile industry. Augie Fabela, who was then a young entrepreneur from the United States, and Zimin, who was a Russian scientist in his fifties, together launched the Beeline brand in 1993.
The company was one of the first mobile carriers in Russia. Its name derives from  , the Russian word for pennon.




In 1996, OJSC VimpelCom, then the leader of Moscow cellular market (and automatically the largest mobile operator in Russia), became the first Russian company to have its shares listed on the New York Stock Exchange (with "VIP" as the ticker symbol).
The company's main shareholders are Telenor, a Norwegian telecom conglomerate, and Alfa Group, a vehicle of Russian tycoon Mikhail Fridman. They have been locked in a years-long struggle for control of this enterprise as well as other telecommunication assets in Eastern Europe.
In 2008, OJSC VimpelCom obtained a 49.9% stake in Euroset, the largest mobile retailer in Russia and the CIS, which formerly belonged to its founder Yevgeny Chichvarkin.




VimpelCom Ltd. holding company was founded in 2009. In 2009, Telenor and Alfa agreed to merge their assets in VimpelCom and Kyivstar (Ukraine's number one wireless operator) with the aim of creating VimpelCom Ltd. (incorporated in Bermuda). In 2010 the ticker  VIP  was transferred from OJSC VimpelCom to VimpelCom Ltd.. The website vimpelcom.com was also transferred from OJSC VimpelCom.
In October 2010, the united company acquired from Naguib Sawiris two assets:
Orascom Telecom Holding SAE, the largest mobile phone company in North Africa;
Wind Italy, the third largest mobile phone company in Italy.
The company has 205 million customers across 20 countries (as of 31 December 2011).
As of April 2012, the government of Algeria is in talks to acquire from VimpelCom a 51% stake in Djezzy, the largest mobile phone company in the country.
In January, 2013 VimpelCom Ltd. and the Wikimedia Foundation announced a partnership to deliver free Wikipedia access to VimpelCom customers through "Wikipedia Zero".



In September 2014, Vimpelcom agreed to sell its majority stake in Wind Mobile for $272 million to its minority owner Globalive.


