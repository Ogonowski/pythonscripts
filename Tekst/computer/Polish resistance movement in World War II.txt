The Polish resistance movement in World War II, with the Polish Home Army at its forefront, was the largest underground resistance in all of Nazi-occupied Europe, covering both German and Soviet zones of occupation. The Polish defence against the Nazi occupation was an important part of the European anti-fascist resistance movement. It is most notable for disrupting German supply lines to the Eastern Front, providing military intelligence to the British, and for saving more Jewish lives in the Holocaust than any other Allied organization or government. It was a part of the Polish Underground State.




The largest of all Polish resistance organizations was the Armia Krajowa (Home Army, AK), loyal to the Polish government in exile in London. The AK was formed in 1942 from the Union for Armed Combat (Zwi zek Walki Zbrojnej or ZWZ, itself created in 1939) and would eventually incorporate most other Polish armed resistance groups (except for the communists and some far-right groups). It was the military arm of the Polish Underground State and loyal to the Polish government in Exile.
Most of the other Polish underground armed organizations were created by a political party or faction, and included:
The Bataliony Ch opskie (Peasants' Battalions). Created by the leftist People's Party around 1940 1941, it would partially merge with AK around 1942 1943.
The Gwardia Ludowa WRN (People's Guard of WRN) of Polish Socialist Party (PPS) (joined ZWZ around 1940, subsequently merged into AK)
The Konfederacja Narodu (Confederation of the Nation). Created in 1940 by far-right Ob z Narodowo Radykalny-Falanga (National Radical Camp Falanga). It would partially merge with ZWZ around 1941 and finally join AK around fall 1943.
The Narodowa Organizacja Wojskowa (National Military Organisation), established by the National Party in 1939, mostly integrated with AK around 1942.
Narodowe Si y Zbrojne (National Armed Forces); created in 1943 from dissatisfied NOW units, which refused to be subordinated to the AK.
The Ob z Polski Walcz cej (Camp of Fighting Poland), established by the Ob z Zjednoczenia Narodowego (Camp of National Unity) around 1942, subordinated to AK. in 1943.
The largest groups that refused to join the AK were the National Armed Forces and the pro-Soviet and communist People's Army (Polish Armia Ludowa or AL), backed by the Soviet Union and established by the Polish Workers' Party (Polish Polska Partia Robotnicza or PPR).

"Within the framework of the entire enemy intelligence operations directed against Germany, the intelligence service of the Polish resistance movement assumed major significance. The scope and importance of the operations of the Polish resistance movement, which was ramified down to the smallest splinter group and brilliantly organized, have been in (various sources) disclosed in connection with carrying out of major police security operations." Heinrich Himmler, 31 December 1942



In February 1942, when AK was formed, it numbered about 100,000 members. In the beginning of 1943, it had reached a strength of about 200,000. In the summer of 1944 when Operation Tempest begun AK reached its highest membership numbers, though the estimates vary from 300,000 to 500,000. The strength of the second largest resistance organization, Bataliony Ch opskie (Peasants' Battalions), can be estimated for summer 1944 (at which time they were mostly merged with AK) at about 160,000 men. The third largest group include NSZ (National Armed Forces) with approximately 70,000 men around 1943-1944; only small parts of that force were merged with AK. At its height in 1944, the communist Armia Ludowa, never merged with AK, numbered about 30,000 people. One estimate for the summer 1944 strength of AK and its allies, including NSZ, gives the strength of 650,000. Overall, the Polish resistance have often been described as the largest or one of the largest resistance organizations in World War II Europe.







On November 9, 1939, two soldiers of the Polish army Witold Pilecki and Major Jan W odarkiewicz founded the Secret Polish Army (Tajna Armia Polska, TAP), one of the first underground organizations in Poland after defeat. Pilecki became its organizational commander as TAP expanded to cover not only Warsaw but Siedlce, Radom, Lublin and other major cities of central Poland. By 1940, TAP had approximately 8,000 men (more than half of them armed), some 20 machine guns and several anti-tank rifles. Later, the organization was incorporated into the Union for Armed Struggle (Zwi zek Walki Zbrojnej), later renamed and better known as the Home Army (Armia Krajowa).




In March 1940, a partisan unit of the first guerrilla commanders in the Second World War in Europe under Major Henryk Dobrza ski "Hubal" completely destroyed a battalion of German infantry in a skirmish near the village of Huciska. A few days later in an ambush near the village of Sza asy it inflicted heavy casualties upon another German unit. To counter this threat the German authorities formed a special 1,000 men strong anti-partisan unit of combined SS Wehrmacht forces, including a Panzer group. Although the unit of Major Dobrza ski never exceeded 300 men, the Germans fielded at least 8,000 men in the area to secure it.

In 1940, Witold Pilecki, a member of the Polish resistance, presented to his superiors a plan to enter Germany's Auschwitz concentration camp, gather intelligence on the camp from the inside, and organize inmate resistance. The Home Army approved this plan, provided him a false identity card, and on September 19, 1940, he deliberately went out during a street roundup in Warsaw -  apanka, and was caught by the Germans along with other civilians and sent to Auschwitz. In the camp he organized the underground organization -Zwi zek Organizacji Wojskowej - ZOW. From October 1940, ZOW sent its first report about the camp and the genocide in November 1940 to Home Army Headquarters in Warsaw through the resistance network organized in Auschwitz.
During the night of January 21 22, 1940, in the Soviet-occupied Podolian town of Czortk w, the Czortk w Uprising started; it was the first Polish uprising during World War II. Anti-Soviet Poles, most of them teenagers from local high schools, stormed the local Red Army barracks and a prison, in order to release Polish soldiers kept there.
At the end of 1940 Aleksander Kami ski created a Polish youth resistance organization - "Wawer". It was part of the Szare Szeregi (the underground Polish Scouting Association). This organisation carried out many minor sabotage operations in occupied Poland. Its first action was drawing graffiti in Warsaw around Christmas Eve of 1940 commemorating the Wawer massacre. Members of the AK Wawer "Small Sabotage" units painted "Pom cimy Wawer" ("We'll avenge Wawer") on Warsaw walls. At first they painted the whole text, then to save time they shortened it to two letters, P and W. Later they invented Kotwica -"Anchor" - which became the symbol of all Polish resistance in occupied Poland.



From April 1941 the Bureau of Information and Propaganda of the Union for Armed Struggle started Operation N headed by Tadeusz  enczykowski. It involved sabotage, subversion and black-propaganda activities.
From March 1941, Witold Pilecki's reports were forwarded to the Polish government in exile and through it, to the British and other Allied governments. These reports informed the Allies about the Holocaust and were the principal source of intelligence on Auschwitz-Birkenau for the Western Allies.
On March 7, 1941, two Polish agents of the Home Army killed Nazi collaborator actor Igo Sym in his apartment in Warsaw. In reprisal, 21 Polish hostages were executed. Several Polish actors were also arrested by the Nazis and sent to Auschwitz, among them such notable figures as directors Stefan Jaracz and Leon Schiller.
In July 1941 Mieczys aw S owikowski (using the codename "Rygor"   Polish for "Rigor") set up "Agency Africa," one of World War II's most successful intelligence organizations. His Polish allies in these endeavors included Lt. Col. Gwido Langer and Major Maksymilian Ci ki. The information gathered by the Agency was used by the Americans and British in planning the amphibious November 1942 Operation Torch landings in North Africa. These were the first large-scale Allied landings of the war, and their success in turn paved the way for the Allies' Italian campaign.




On 20 June 1942, the most spectacular escape from Auschwitz concentration camp took place. Four Poles, Eugeniusz Bendera, Kazimierz Piechowski, Stanis aw Gustaw Jaster and J zef Lempart made a daring escape. The escapees were dressed as members of the SS-Totenkopfverb nde, fully armed and in an SS staff car. They drove out the main gate in a stolen Rudolf Hoss automobile Steyr 220 with a smuggled report from Witold Pilecki about the Holocaust. The Germans never recaptured any of them.
In September 1942 "The  egota Council for the Aid of the Jews" was founded by Zofia Kossak-Szczucka and Wanda Krahelska-Filipowicz ("Alinka") and made up of Polish Democrats as well as other Catholic activists. Poland was the only country in occupied Europe where there existed such a dedicated secret organization. Half of the Jews who survived the war (thus over 50,000) were aided in some shape or form by  egota. The best-known activist of  egota was Irena Sendler, head of the children's division, who saved 2,500 Jewish children by smuggling them out of the Warsaw Ghetto, providing them with false documents, and sheltering them in individual and group children's homes outside the Ghetto.
In 1942 Jan Karski reported to the Polish, British and U.S. governments on the situation in Poland, especially the Holocaust of the Jews. He met with Polish politicians in exile including the prime minister, and members of political parties such as the Socialist Party, National Party, Labor Party, People's Party, Jewish Bund and Poalei Zion. He also spoke to Anthony Eden, the British foreign secretary, and included a detailed statement on what he had seen in Warsaw and Be ec.
The Zamo  Uprising was an armed uprising of Armia Krajowa and Bataliony Ch opskie against the forced expulsion of Poles from the Zamo  region under the Nazi Generalplan Ost. The Germans attempted to remove the local Poles from the Greater Zamo  area (through forced removal, transfer to forced labor camps, or, in some cases, mass murder) to get it ready for German colonization. It lasted from 1942 until 1944 and despite heavy casualties suffered by the Underground, the Germans failed.
On the night from 7 to 8 October 1942 Operation Wieniec started. It targeted rail infrastructure near Warsaw. Similar operations aimed at disrupting German transport and communication in occupied Poland occurred in the coming months and years. It targeted railroads, bridges and supply depots, primarily near transport hubs such as Warsaw and Lublin.



In early 1943 two Polish janitors of Peenem nde's Camp Trassenheide provided maps, sketches and reports to Armia Krajowa Intelligence, and in June 1943 British intelligence had received two such reports which identified the "rocket assembly hall', 'experimental pit', and 'launching tower'. When reconnaissance and intelligence information regarding the V-2 rocket became convincing, the War Cabinet Defence Committee (Operations) directed the campaign's first planned raid (the Operation Hydra bombing of Peenem nde in August 1943) and Operation Crossbow.
On March 26, 1943 in Warsaw Operation Arsenal was launched by the Szare Szeregi (Gray Ranks) Polish Underground The successful operation led to the release of arrested troop leader Jan Bytnar "Rudy". In an attack on the prison, Bytnar and 24 other prisoners were freed.
In 1943 in London Jan Karski met the then much known journalist Arthur Koestler. He then traveled to the United States and reported to President Franklin D. Roosevelt. His report was a major factor in informing the West. In July 1943, again personally reported to Roosevelt about the situation in Poland. He also met with many other government and civic leaders in the United States, including Felix Frankfurter, Cordell Hull, William Joseph Donovan, and Stephen Wise. Karski also presented his report to media, bishops of various denominations (including Cardinal Samuel Stritch), members of the Hollywood film industry and artists, but without success. Many of those he spoke to did not believe him, or supposed that his testimony was much exaggerated or was propaganda from the Polish government in exile.
In April 1943 the Germans began deporting the remaining Jews from the Warsaw ghetto provoking the Warsaw Ghetto Rising, April 19 to May 16. Some units of the AK tried to assist the Ghetto rising, but for the most part the resistance was unprepared and unable to defeat the Germans. One Polish AK unit, the National Security Corps (Pa stwowy Korpus Bezpiecze stwa), under the command of Henryk Iwa ski ("Bystry"), fought inside the ghetto along with  ZW. Subsequently, both groups retreated together (including 34 Jewish fighters). Although Iwa ski's action is the most well-known rescue mission, it was only one of many actions undertaken by the Polish resistance to help the Jewish fighters. In one attack, three cell units of AK under the command of Kapitan J zef Pszenny ("Chwacki") tried to breach the ghetto walls with explosives, but the Germans defeated this action. AK and GL engaged the Germans between April 19 and April 23 at six different locations outside the ghetto walls, shooting at German sentries and positions and in one case attempting to blow up a gate. After the failure of the uprising, the Jewish leaders knew they would be crushed, but they preferred to die fighting than wait to be deported to their deaths in the concentration camps.

In August 1943 the headquarters of the Armia Krajowa ordered Operation Belt which was one of the large-scale anti-Nazi operations of the AK during the war. By February 1944, 13 German outposts were destroyed with few losses on the Polish side.
Operation Heads started - action of the serial assassinations Nazi personnel sentenced to death by the Special Courts for crimes against Polish citizens in occupied Poland.
On September 7, 1943, the Home Army killed Franz B rkl during Operation B rkl. B rkl was a high-ranking Gestapo agent responsible for the murder and brutal interrogation of thousands of Polish Jews and resistance fighters and supporters. In reprisal, 20 inmates of Pawiak were murdered in a public execution by the Nazis.
From November 1943, Operation Most III started. The Armia Krajowa provided the Allies with crucial intelligence on the German V-2 rocket. In effect some 50 kg of the most important parts of the captured V-2, as well as the final report, analyses, sketches and photos, were transported to Brindisi by a Royal Air Force Douglas Dakota aircraft. In late July 1944, the V-2 parts were delivered to London.




On 11 February 1944 the Resistance fighters of Polish Home Army's unit Agat executed Franz Kutschera, SS and Reich's Police Chief in Warsaw in action known as Operation Kutschera. In a reprisal of this action 27 February 140 inmates of Pawiak - Poles and Jews were shot in a public execution by the Germans.
May 13 May 14, 1944 the Battle of Murowana Oszmianka the largest clash between the Polish anti-Nazi Armia Krajowa and the Nazi Lithuanian Territorial Defense Force a Lithuanian volunteer security force subordinated to Nazi Germany. The battle took place in and near the village of Murowana Oszmianka in Generalbezirk Litauen Reichskommissariat Ostland. The outcome of the battle was that the 301st LVR battalion was routed and the entire force was disbanded by the Germans soon afterwards.
On June 14, 1944 the Battle of Porytowe Wzg rze took place between Polish and Russian partisans, numbering around 3000, and the Nazi German units consisted of between 25000 to 30000 soldiers, with artillery, tanks and armored cars and air support.
On 25 26 June 1944 the Battle of Osuchy - one of the largest battles between the Polish resistance and Nazi Germany in occupied Poland during World War II was fought, in what was essentially a continuation of the Zamo  Uprising.
During 1943 the Home Army built up its forces in preparation for a national uprising. The plan of national anti-Nazi uprising on areas of prewar Poland was code-named Operation Tempest. Preparation began in late 1943 but the military actions started in 1944. Its most widely known elements were Operation Ostra Brama, Lw w Uprising and the Warsaw Uprising.
On July 7, Operation Ostra Brama started. Approximately 12,500 Home Army soldiers attacked the German garrison and managed to seize most of the city center. Heavy street fighting in the outskirts of the city lasted until July 14. In Vilnius' eastern suburbs, the Home Army units cooperated with reconnaissance groups of the Soviet 3rd Belorussian Front. The Red Army entered the city on July 15, and the NKVD started to intern all Polish soldiers. On July 16, the HQ of the 3rd Belorussian Front invited Polish officers to a meeting and arrested them.
On July 23 the Lw w Uprising the armed struggle started by the Armia Krajowa against the Nazi occupiers in Lw w during World War II started. It started in July 1944 as a part of a plan of all-national uprising codenamed Operation Tempest. The fighting lasted until July 27 and resulted in liberation of the city. However, shortly afterwards the Polish soldiers were arrested by the invading Soviets and either forced to join the Red Army or sent to the Gulags. The city itself was occupied by the Soviet Union.
In August 1944, as the Soviet armed forces approached Warsaw, the government in exile called for an uprising in the city, so that they could return to a liberated Warsaw and try to prevent a communist take-over. The AK, led by Tadeusz B r-Komorowski, launched the Warsaw Uprising. Soviet forces were less than 20 km away but on the orders of Soviet High Command they gave no assistance. Stalin described the uprising as a "criminal adventure". The Poles appealed to the western Allies for help. The Royal Air Force, and the Polish Air Force based in Italy, dropped some munitions, but it was almost impossible for the Allies to help the Poles without Soviet assistance.
The fighting in Warsaw was desperate. The AK had between 12,000 and 20,000 armed soldiers, most with only small arms, against a well-armed German Army of 20,000 SS and regular Army units. B r-Komorowski's hope that the AK could take and hold Warsaw for the return of the London government was never likely to be achieved. After 63 days of savage fighting the city was reduced to rubble, and the reprisals were savage. The SS and auxiliary units were particularly brutal.
After B r-Komorowski's surrender, the AK fighters were treated as prisoners-of-war by the Germans, much to the outrage of Stalin, but the civilian population were ruthlessly punished. Overall Polish casualties are estimated to be between 150,000 300,000 killed, 90,000 civilians were sent to labor camps in the Reich, while 60,000 were shipped to death and concentration camps such as Ravensbr ck, Auschwitz, Mauthausen and others. The city was almost totally destroyed after German sappers systematically demolished the city. The Warsaw Uprising allowed the Germans to destroy the AK as a fighting force, but the main beneficiary was Stalin, who was able to impose a communist government on postwar Poland with little fear of armed resistance.




In March 1945, a staged trial of 16 leaders of the Polish Underground State held by the Soviet Union took place in Moscow - (Trial of the Sixteen). The Government Delegate, together with most members of the Council of National Unity and the C-i-C of the Armia Krajowa, were invited by Soviet general Ivan Serov with agreement of Joseph Stalin to a conference on their eventual entry to the Soviet-backed Provisional Government. They were presented with a warrant of safety, yet they were arrested in Pruszk w by the NKVD on 27 and 28 March. Leopold Okulicki, Jan Stanis aw Jankowski and Kazimierz Pu ak were arrested on 27th with 12 more the next day. A. Zwierzynski had been arrested earlier. They were brought to Moscow for interrogation in the Lubyanka. After several months of brutal interrogation and torture, they were presented with the forged accusations of "collaboration with Nazi Germany" and "planning a military alliance with Nazi Germany".
In the latter years of the war, there were increasing conflicts between Polish and Soviet partisans. Cursed soldiers continued to oppose the Soviets long after the war. The last cursed soldier - member of the militant anti-communist resistance in Poland was J zef Franczak who was killed with pistol in his hand by ZOMO in 1963.
On May 5, 1945 in Bohemia, the Narodowe Si y Zbrojne brigade liberated prisoners from a Nazi concentration camp in Holiszowo, including 280 Jewish women prisoners. The brigade suffered heavy casualties.
On May 21, 1945, a unit of the Armia Krajowa, led by Colonel Edward Wasilewski, attacked a NKVD camp located in Rembert w on the eastern outskirts of Warsaw. The Soviets kept there hundreds of Poles, members of the Home Army, whom they were systematically deporting to Siberia. However, this action of the pro-independence Polish resistance freed all Polish political prisoners from the camp. Between 1944-1946, cursed soldiers attacked many communist prisons in Soviet-occupied Poland  see Raids on communist prisons in Poland (1944 1946).
On May 7, 1945 in the village of Kury wka, southeastern Poland, the Battle of Kury wka started. It was the biggest battle in the history of the Cursed soldiers organization - National Military Alliance (NZW). In battle against Soviet Union's NKVD units anti-communist partisans shot 70 NKVD agents. The battle ended in a victory for the underground Polish forces.
From June 10 25, 1945, August w chase 1945 (the Polish Ob awa augustowska) took place. It was a large-scale operation undertaken by Soviet forces of the Red Army, the NKVD and SMERSH, with the assistance of Polish UB and LWP units against former Armia Krajowa soldiers in the Suwa ki and August w region in Poland. The operation also covered territory in occupied Lithuania. More than 2,000 alleged Polish anticommunist fighters were captured and detained in Russian internment camps. 600 of the "August w Missing" are presumed dead and buried in an unknown location in the present territory of Russia. The August w Roundup was part of an anti-guerilla operation in Lithuania.



Antyfaszystowska Organizacja Bojowa
Armia Krajowa
Armia Ludowa
Bataliony Ch opskie
Brygada Swi tokrzyska
Gwardia Ludowa
Gwardia Ludowa WRN
Le ni
Narodowa Organizacja Wojskowa
Narodowe Si y Zbrojne
Ob z Polski Wa cz cej
Pa stwowy Korpus Bezpiecze stwa
Polska Armia Ludowa
Szare Szeregi
Zwi zek Odwetu
Zwi zek Walki Zbrojnej
 ydowska Organizacja Bojowa
Zwi zek Organizacji Wojskowej
 ydowski Zwi zek Wojskowy



Polish Underground State
Polish resistance in France during World War II
Anti-fascism
Home Army and V1 and V2
Yugoslav Partisans
Cursed soldiers
Lithuanian resistance during World War II
General Government
History of Poland (1939 1945)
Polish areas annexed by Nazi Germany
Polish areas annexed by Soviet Union
Polish partisans
Resistance during World War II
Resistance movement
Operation Ostra Brama
Western betrayal
Bratnia Pomoc



a ^ A number of sources note that the Home Army, representing the bulk of Polish resistance, was the largest resistance movement in Nazi-occupied Europe. Norman Davies writes that the "Armia Krajowa (Home Army), the AK,... could fairly claim to be the largest of European resistance [organizations]." Gregor Dallas writes that the "Home Army (Armia Krajowa or AK) in late 1943 numbered around 400,000, making it the largest resistance organization in Europe." Mark Wyman writes that the "Armia Krajowa was considered the largest underground resistance unit in wartime Europe." The numbers of Soviet partisans were very similar to those of the Polish resistance.






Polish contribution to World War II (Polish Underground State) Movie on YouTube
Armia Krajowa
Armia Krajowa
Die "Stunde W"
Narodowe Si y Zbrojne
Ann Su Caldwell, POLAND: HERE IS THE RECORD at the Wayback Machine (archived July 27, 2011). Polonia Online.
Polish Resistance in World War II
Tadeusz WICHROWSKI - "Wicher"
Warsaw Uprising 1944
History of Warsaw's contributions levied by the German Occupation Authority