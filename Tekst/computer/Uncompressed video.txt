Uncompressed video is digital video that either has never been compressed or was generated by decompressing previously compressed digital video. It is commonly used by video cameras, video recording devices (including general purpose computers), and in video processors that perform functions such as image resizing, image rotation and text and graphics overlay. It is conveyed over various types of baseband digital video interfaces, such as HDMI, DVI, DisplayPort and SDI.
Some HD video cameras output uncompressed video, whereas others compress the video using a lossy compression method such as MPEG or H.264. In the compression process, some of the video information is removed, which reduces the quality of the video when decompressed. When editing video, it is preferred to work with video that has never been compressed as this maintains the best possible quality, with compression performed after completion of editing.



The lossless video has no compression artifacts.
Currently there is no standardized lossless video file format except for HDMI, which uses the YCbCr and RGB formats listed below. This makes it necessary to store it best with a related description file about the used resolution and video mode. These files can be combined with lossless compression with the use of file archivers.



Lossless video compression can be delivered with a variety of video codecs. In test some codecs performed an average compression of over factor 3.



Setting up the camera is often new especially for DSLR users. The built in video interface in cameras is mostly an HDMI or, in professional cameras, a Serial digital interface (SDI or HD-SDI); converters between both are available.



Portable recorders are an easy, reliable and complete solution for receiving and storing uncompressed video. Partly they receive uncompressed video, but only record lossy compressed video, often in the lossy Apple ProRes 422 or DNxHD codecs. Professional recorders support multiple channels of uncompressed HDMI, DVI or (HD-)SDI recording, but are limited by the total data rate.



Recording to a computer enables low-cost to highest performance solutions for laptop or desktop computers, but the computer should be prepared as it must act like a real-time operating system (RTOS). Any other significant program activity including background processes - for example not needed Windows startup processes (use for example Autoruns) or Windows services (use Service Control Manager), including automatic updates or virus scanners - may disrupt, distort or stop the video recording. Disconnection of not needed computer networks and increasing the process priority of the recording realtime process often helps to use most of the computer speed. Hard disk drives have to be fast solid-state drives (SSDs) and/or RAID to be capable of the data-rate of HD videos and/or multiple channels.



HDMI, DVI and HD-SDI inputs are available as PCI Express (partly multi-channel) or ExpressCard, USB 3.0 and Thunderbolt interface also for 2160p (4K resolution).



Most Wireless interfaces like Wireless LAN (WLAN, Wi-Fi), WiDi, Wireless Home Digital Interface (WHDI), can be used to transmit uncompressed digital video, but only with low resolutions, as even 1920x1080p@24 Hz requires 1.2 Gbit/s data rate exceeding for example IEEE 802.11ac. The WirelessHD interface, however, which uses a 60 GHz wireless link, can transmit uncompressed digital video. The Wireless Gigabit Alliance also aims to use a 60 GHz wireless link. However, any disruption or bandwidth decrease of the wireless connection will reduce quality, or even stop the video recording.



Software for uncompressed video is often supplied with suitable hardware or available for free: Ingex (open source).



Constant bitrate formula: Uncompressed data rate = color depths * vertical resolution * horizontal resolution * refresh frequency
Examples
24bit @ 1080i @ 60fps :24*1920*1080*60/2=1.49 Gbit/s.
24bit @ 1080p @ 60fps :24*1920*1080*60=2.98 Gbit/s.
The storage and data rates for the widely used YCbCr 4:2:2 chroma subsampling uncompressed video are listed below:
525 NTSC uncompressed
8 bit @ 720 x 486 @ 29.97fps = 20 MB/s, or 70 GB/h.
10 bit @ 720 x 486 @ 29.97fps = 27 MB/s, or 94 GB/h.
625 PAL uncompressed
8 bit @ 720 x 576 @ 25fps = 20 MB/s, or 70 GB/h.
10 bit @ 720 x 576 @ 25fps = 26 MB/s, or 93 GB/h.
720p HDTV uncompressed
8 bit @ 1280 x 720 @ 59.94fps = 105 MB/s, or 370 GB/h.
10 bit @ 1280 x 720 @ 59.94fps = 140 MB/s, or 494 GB/h.
1080i and 1080p HDTV uncompressed
8 bit @ 1920 x 1080 @ 24fps = 95 MB/s, or 334 GB/h.
10 bit @ 1920 x 1080 @ 24fps = 127 MB/s, or 445 GB/h.
8 bit @ 1920 x 1080 @ 25fps = 99 MB/s, or 348 GB/h.
10 bit @ 1920 x 1080 @ 25fps = 132 MB/s, or 463 GB/h.
8 bit @ 1920 x 1080 @ 29.97fps = 119 MB/s, or 417 GB/h.
10 bit @ 1920 x 1080 @ 29.97fps = 158 MB/s, or 556 GB/h.
1080i and 1080p HDTV RGB (4:4:4) uncompressed
10 bit @ 1280 x 720p @ 60fps = 211 MB/s, or 742 GB/h.
10 bit @ 1920 x 1080 @ 24fps = 190 MB/s, or 667 GB/h.
10 bit @ 1920 x 1080 @ 50i = 198 MB/s, or 695 GB/h.
10 bit @ 1920 x 1080 @ 60i = 237 MB/s, or 834 GB/h.



According to HDMI 1.3a Spec.
Detailed timing is found in CEA-861-D or a later version of CEA-861 for the following video format timings. HDMI 2.0 supports higher resolutions, which are defined in CEA-861-F.
Cameras mostly use the progressive segmented frame format: for example a 25p/30p progressive scan is transported in a 50i/60i interlaced format respectively, but with identical information: No deinterlacing should be used.
Primary Video Format Timings
  640x480p @ 59.94/60 Hz
  1280x720p @ 59.94/60 Hz
  1920x1080i @ 59.94/60 Hz
  720x480p @ 59.94/60 Hz
  720(1440)x480i @ 59.94/60 Hz
  1280x720p @ 50 Hz
  1920x1080i @ 50 Hz
  720x576p @ 50 Hz
  720(1440)x576i @ 50 Hz
Secondary Video Format Timings
  720(1440)x240p @ 59.94/60 Hz
  2880x480i @ 59.94/60 Hz
  2880x240p @ 59.94/60 Hz
  1440x480p @ 59.94/60 Hz
  1920x1080p @ 59.94/60 Hz
  720(1440)x288p @ 50 Hz
  2880x576i @ 50 Hz
  2880x288p @ 50 Hz
  1440x576p @ 50 Hz
  1920x1080p @ 50 Hz
  1920x1080p @ 23.98/24 Hz
  1920x1080p @ 25 Hz
  1920x1080p @ 29.97/30 Hz
  2880x480p @ 59.94/60 Hz
  2880x576p @ 50 Hz
  1920x1080i (1250 total) @ 50 Hz
  720(1440)x480i @ 119.88/120 Hz
  720x480p @ 119.88/120 Hz
  1920x1080i @ 119.88/120 Hz
  1280x720p @ 119.88/120 Hz
  720(1440)x480i @ 239.76/240 Hz
  720x480p @ 239.76/240 Hz
  720(1440)x576i @ 100 Hz
  720x576p @ 100 Hz
  1920x1080i @ 100 Hz
  1280x720p @ 100 Hz
  720(1440)x576i @ 200 Hz
  720X576p @ 200 Hz
Pixel Encodings and Color Depth
There are three different pixel encodings that may be sent across an HDMI cable: YCbCr 4:4:4 (chroma subsampling), YCbCr 4:2:2 and RGB 4:4:4.
There are four color depths supported: 24-, 30-, 36- and 48-bits per pixel. In HDMI 2.0, it is possible to transmit 4:2:0 chroma subsampling, but only in 4K50 and 4K60 resolution.


