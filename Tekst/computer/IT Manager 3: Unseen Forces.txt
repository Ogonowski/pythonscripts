IT Manager III: Unseen Forces was a web-based, IT simulation game from Intel in which the player managed an IT department in a corporate environment.
There were two aspects to the game. Firstly, the player had to ensure that employees  desktop PCs, laptops and servers kept running smoothly. There were dozens of unique software and hardware faults to tackle. Sometimes these would prove easy to repair; sometimes they would consume more effort.
As the player s company grew, it placed increasing demands on IT departments, and the player had to ensure that the technologies were rolled out to take the strain. This gave the game a second strategic dimension. Upgraded and new technologies reduced maintenance costs, increased security and made employees happier and more productive.
The game spanned eight levels. Each represented a bigger office than the last. Not only did each level feature a bigger office with more employees, the IT issues that arose also became more challenging. If the player could improve productivity whilst maintaining a good working environment, the company s share price would go up. When it hit its target value, the level would be complete.
IT Manager III is no longer online. In July 2012, it was replaced by its successor, the multiplayer IT Manager: Duels.



IT Manager III: Unseen Forces was shortlisted for the NMA effectiveness awards in 2009. It won the 2009 British Interactive Media Association award in the best business-to-business category and the Intel Innovation Grand Prix award 2011, which Intel acknowledges a top supplier.






Official website