CNET (stylized as c|net) is an American media website that publishes reviews, news, articles, blogs, podcasts and videos on technology and consumer electronics globally. Founded in 1994 by Halsey Minor and Shelby Bonnie, it was the flagship brand of CNET Networks and became a brand of CBS Interactive through CNET Networks' acquisition in 2008. CNET originally produced content for radio and television in addition to its website and now uses new media distribution methods through its Internet television network, CNET Video, and its podcast and blog networks.
In addition CNET currently has region-specific and language-specific editions. These include the United Kingdom, Australia, China, Japan, French, German, Korean and Spanish. According to third-party web analytics providers, Alexa and SimilarWeb, CNET is the highest-read technology news source on the Web, with over 200 million readers per month, being among the 200 most visited websites globally, as of 2015.







In 1994, with the help from Fox Network co-founder Kevin Wendle and former Disney creative associate Dan Baker, CNET produced four pilot television programs about computers, technology, and the Internet. CNET TV was composed of CNET Central, The Web, and The New Edge. CNET Central was created first and aired in syndication in the United States on the USA Network. Later, it began airing on USA's sister network Sci-Fi Channel along with The Web and The New Edge. These were later followed by TV.com in 1996. Current American Idol host Ryan Seacrest first came to national prominence at CNET, as the host of The New Edge and doing various voice-over work for CNET.
In addition, CNET produced another television technology news program called News.com that aired on CNBC beginning in 1999.
From 2001 to 2003, CNET operated CNET Radio on the Clear Channel-owned KNEW (910) in the San Francisco Bay Area, WBPS (890) in Boston and on XM Satellite Radio. CNET Radio offered technology-themed programming. After failing to attract a sufficient audience, CNET Radio ceased operating in January 2003 due to financial losses.



As CNET Networks, the site made various acquisitions to expand its reach across various web platforms, regions, and markets.
In July 1999, CNET acquired the Swiss-based company GDT. GDT was later renamed to CNET Channel.
In 1998, CNET granted the right to Asiacontent to set up CNET Asia and the operation was brought back in December 2000.
In January 2000, the same time CNET became CNET Networks, they acquired comparison shopping site mySimon for $736 million.
In October 2000, CNET Networks acquired ZDNet for approximately $1.6 billion. In January 2001, Ziff Davis Media, Inc. reached an agreement with CNET Networks, Inc. to regain the URLs lost in the 2000 sale of Ziff Davis, Inc. to SoftBank Corp. a publicly traded Japanese media and technology company. In April 2001, CNET acquired TechRepublic Inc., which provides content for IT professionals from Gartner, Inc., for $23 million in cash and stock.
On July 14, 2004, CNET announced that it would acquire Webshots, the leading photography website for $70 million ($60 million in cash, $10 million in deferred consideration), completing the acquisition that same month. In October 2007, they sold Webshots to American Greetings for $45 million.
In December 2006, James Kim, an editor at CNET, died in the Oregon wilderness. CNET hosted a memorial show and podcasts dedicated to him.
On March 1, 2007, CNET announced the public launch of BNET, a website targeted towards business managers. BNET had been running under beta status since 2005.
On May 15, 2008 it was announced that CBS Corporation would buy CNET Networks for US$1.8 billion. On June 30, 2008, the acquisition was completed. Former CNET properties are now part of CBS Interactive. CBS Interactive now owns many domain names originally created by CNET Networks, including download.com, downloads.com, upload.com, news.com, search.com, TV.com, mp3.com, chat.com, computers.com, help.com, shopper.com, radio.com, com.oom, and cnet.com.
On September 19, 2013 CBS Interactive launched a Spanish language sister site under the name CNET en Espa ol. It focuses on topics that matter to Spanish-speaking technology enthusiasts. The site offered a "new perspective" on technology and is under the leadership of managing editor Gabriel Sama.
In March 2014, CNET refreshed its site by merging with CNET UK and vowing to merge all editions of the agency into a unified agency. This change brought many changes but the most foremost would be a new user experience and renaming CNET TV as CNET Video.



In 1998, CNET was sued by Snap Technologies, operators of the education service CollegeEdge, for trademark infringement relating to CNET's ownership of the domain name Snap.com, due to Snap Technologies already owning a trademark on its name.

In 2005, Google representatives refused to be interviewed by all CNET reporters for an entire year after CNET published Google's CEO Eric Schmidt's salary, named the neighborhood where he lives, some of his hobbies and political donations. All the information had been gleaned from Google searches.
On October 10, 2006, Shelby Bonnie resigned as chairman and CEO, in addition to two other executives, as a result of a stock options backdating scandal that occurred between 1996 and 2003. This would also cause the firm to restate its financial earnings over 1996 through 2003 for over $105 million in resulting expenses. The Securities and Exchange Commission later dropped an investigation into the practice. Neil Ashe was named as the new CEO.
In 2011, CNET and CBS Interactive were sued by a coalition of artists (led by FilmOn founder Alki David) for copyright infringement by promoting the download of LimeWire, a popular peer to peer downloading software. Although the original suit was voluntarily dropped by Alki David, he vowed to sue at a later date to bring "expanded" action against CBS Interactive. In November 2011, another lawsuit against CBS Interactive was introduced, claiming that CNet and CBS Interactive knowingly distributed LimeWire, the file sharing software.



In January 2013, CNET named Dish Network's "Hopper with Sling" digital video recorder as a nominee for the CES "Best in Show" award (which is decided by CNET on behalf of its organizers), and named it the winner in a vote by the site's staff. However, CBS abruptly disqualified the Hopper, and vetoed the results because the company was in active litigation with Dish Network. CNET also announced that it could no longer review any product or service provided by companies that CBS are in litigation with (which also includes Aereo). The new vote subsequently gave the Best in Show award to the Razer Edge tablet instead.
Dish Network's CEO Joe Clayton said that the company was "saddened that CNET s staff is being denied its editorial independence because of CBS  heavy-handed tactics." On January 14, 2013, editor-in-chief Lindsey Turrentine addressed the situation, stating that CNET's staff were in an "impossible" situation due to the conflict of interest posed by the situation, and promised that she would do everything within her power to prevent a similar incident from occurring again. The conflict also prompted one CNET senior writer, Greg Sandoval, to resign.
The decision also drew the ire of staff from the Consumer Electronics Association, the organizers of CES; CEO Gary Shapiro criticized the decision in a USA Today op-ed column and a statement by the CEA, stating that "making television easier to watch is not against the law. It is simply pro-innovation and pro-consumer." Shapiro felt that the decision also hurt the confidence of CNET's readers and staff, "destroying its reputation for editorial integrity in an attempt to eliminate a new market competitor." As a result of the controversy and fearing damage to the show's brand, the CEA announced on January 31, 2013 that CNET will no longer decide the CES Best in Show award winner due to the interference of CBS (the position has been offered to other technology publications), and the "Best in Show" award was jointly awarded to both the Hopper with Sling and Razer Edge.






The reviews section of the site is the largest part of the site, and generates over 4,300 product and software reviews per year. The Reviews section also features Editors  Choice Awards, which recognize products that are particularly innovative and of the highest quality.



CNET News (formerly known as News.com), launched in 1996, is a news website dedicated to technology, and was one of the first news sources to help define technology reporting in the age of the internet. CNET News has won several prestigious awards, including the National Magazine award. Content is created by both CNET and external media agencies as news articles and blogs, including Webware (Web 2.0 topics) and Crave (gadgets).



CNET Video is CNET's Internet video channel offering a selection of on-demand video content including video reviews, first looks and special features. CNET VIdeo plays various videos, including CNET video reviews. CNET editors such as Brian Cooley, Jeff Bakalar, Bridget Carey and Brian Tong host shows like Car Tech, The 404 Show, Quick Tips, CNET Top 5, Update, The Apple Byte, video prizefights, and others, as well as special reports and reviews. On April 12, 2007, CNET Video aired its first episode of CNET LIVE, hosted by Brian Cooley and Tom Merritt. The first episode featured Justin Kan of justin.tv. CNET Video was formerly known as CNET TV.



Officially launched August 2011, How To is the learning area of CNET providing tutorials, guides and tips for technology users.



With a catalog of more than 400,000 titles, the Downloads section of the website allows users to download popular software, generating approximately 3.5 million downloads per day. CNET download.com provides Windows, Macintosh and mobile software for download. CNET maintains that this software is free of spyware, which some IT professionals would disagree with entirely. The site also offered free MP3 music files for download (mostly by independent artists), however, the music section of the site has since merged with last.fm. This meant that all the music downloads were deleted without warning.



CBS Corporation
CBS Interactive
ZDNet
TechRepublic






CNET
CNET on Facebook
CNET on Google+
@CNET on Twitter