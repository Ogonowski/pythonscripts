Guadeloupe (/ w d lu p/; French pronunciation:  [ wad lup]; Antillean Creole: Gwadloup) is an overseas region of France, consisting of a single overseas department, located in the Leeward Islands, part of the Lesser Antilles in the Caribbean. It has a land area of 1,628 square kilometres (629 sq. mi) and a population of 403,750 (as of January 2014).
Guadeloupe's two main islands are Basse-Terre to the west and Grande-Terre to the east, which are separated by a narrow strait that is crossed with bridges, and which are often referred to as a single island. The department also includes the Dependencies of Guadeloupe which include the smaller islands of Marie-Galante and La D sirade, and the  les des Saintes.
Guadeloupe, like the other overseas departments, is an integral part of France. It is thus part of the European Union and the Eurozone; hence, as for all Eurozone countries, its currency is the euro. As an overseas department, Guadeloupe is not part of the Schengen Area. The prefecture (regional capital) of Guadeloupe is the city of Basse-Terre, which lies on the island of the same name. The official language is French, and virtually the entire population also speaks Antillean Creole (Cr ole Guadeloup en).




Christopher Columbus named the island Santa Mar a de Guadalupe in 1493 after the Virgin Mary, venerated in the Spanish town of Guadalupe, in Extremadura.




The island was called "Karukera" (or "The Island of Beautiful Waters") by the Arawak people, who settled on there in 300 AD/CE. During the 8th century, the Caribs came and killed the existing population of Amerindians on the island.
During his second trip to America, in November 1493, Christopher Columbus became the first European to land on Guadeloupe, while seeking fresh water. He called it Santa Mar a de Guadalupe de Extremadura, after the image of the Virgin Mary venerated at the Spanish monastery of Villuercas, in Guadalupe, Extremadura. The expedition set ashore just south of Capesterre, but left no settlers behind.
Columbus is credited with discovering the pineapple on the island of Guadeloupe in 1493, although the fruit had long been grown in South America. He called it pi a de Indias, which can be correctly translated as "pine cone of the Indies."
During the 17th century, the Caribs fought against the Spanish settlers and repelled them.
After successful settlement on the island of St. Christophe (St. Kitts), the French Company of the American Islands delegated Charles Lienard (Li nard de L'Olive) and Jean Duplessis Ossonville, Lord of Ossonville to colonize one or any of the region s islands, Guadeloupe, Martinique, or Dominica.
Due to Martinique s inhospitable nature, the duo resolved to settle in Guadeloupe in 1635, took possession of the island, and wiped out many of the Carib Amerindians. It was annexed to the kingdom of France in 1674.
Over the next century, the British seized the island several times. The economy benefited from the lucrative sugar trade, which commenced during the closing decades of the 17th century. Guadeloupe produced more sugar than all the British islands combined, worth about  6 million a year. The British captured Guadeloupe in 1759. The British government decided that Canada was strategically more important and kept Canada while returning Guadeloupe to France in the Treaty of Paris (1763) that ended the Seven Years War.
In 1790, following the outbreak of the French Revolution, the monarchists of Guadeloupe refused to obey the new laws of equal rights for the free people of color and attempted to declare independence. The ensuing conflict with the republicans, who were faithful to revolutionary France, caused a fire to break out in Pointe- -Pitre that devastated a third of the town. The monarchists ultimately overcame the republicans and declared independence in 1791. The monarchists then refused to receive the new governor that Paris had appointed in 1792. In 1793, a slave rebellion broke out, which made the upper classes turn to the British and ask them to occupy the island.
In an effort to take advantage of the chaos ensuing from the French Revolution, Britain seized Guadeloupe in 1794, holding control from 21 April until December 1794, when Victor Hugues obliged the British general to surrender. Hugues succeeded in freeing the slaves, who then turned on the slave owners who controlled the sugar plantations.
In 1802, Napoleon Bonaparte issued the Law of 20 May 1802. It restored slavery to all of the colonies captured by the British during the French Revolutionary Wars, but did not apply to certain French overseas possessions such as Guadeloupe, Guyane, and Saint-Domingue. Napoleon sent an expeditionary force to recapture the island from the rebellious slaves. Louis Delgr s and a group of revolutionary soldiers killed themselves on the slopes of the Matouba volcano when it became obvious that the invading troops would take control of the island. The occupation force killed approximately 10,000 Guadeloupeans.
On 4 February 1810 the British once again seized the island and continued to occupy it until 1816. By the Anglo-Swedish alliance of 3 March 1813, it was ceded to Sweden for a brief period of 15 months. During this time the British administration continued in place and British governors continued to govern the island.
In the Treaty of Paris of 1814, Sweden ceded Guadeloupe once more to France. An ensuing settlement between Sweden and the British gave rise to the Guadeloupe Fund. The Treaty of Vienna in 1815 definitively acknowledged French control of Guadeloupe.
Slavery was abolished on the island on 28 May 1848 at the initiative of Victor Schoelcher.
Guadeloupe lost 12,000 of its 150,000 residents in the cholera epidemic of 1865 66.



In 1925, after the trial of Henry Sidambarom (Justice of the Peace and defender of the cause of Indian workers), Raymond Poincar  decided to grant French nationality and the right to vote to Indian citizens.
In 1946, the colony of Guadeloupe became an overseas department of France. Then in 1974, it became an administrative center. Its deputies sit in the French National Assembly in Paris.
In 2007 the island communes of Saint-Martin and Saint-Barth lemy were officially detached from Guadeloupe and became two separate French overseas collectivities with their own local administration. Their combined population was 35,930 and their combined land area was 74.2 km2 (28.6 sq mi) as of the 1999 census.
In January 2009, an umbrella group of approximately fifty labour union and other associations (known in the local Antillean Creole as the Liyannaj Kont Pwofitasyon (LKP), led by  lie Domota) called for a  200 ($260 USD) monthly pay increase for the island's low income workers. The protesters have proposed that authorities "lower business taxes as a top up to company finances" to pay for the  200 pay raises. Employers and business leaders in Guadeloupe have said that they cannot afford the salary increase. The strike lasted 44 days, ending with an accord reached on 5 March 2009. Tourism suffered greatly during this time and affected the 2010 tourist season as well.
The French Caribbean general strikes exposed deep ethnic, racial, and class tensions and disparities within Guadeloupe.




Located as the southernmost of the Leeward Islands in the eastern Caribbean Sea, Guadeloupe comprises two main islands: Basse-Terre Island and Grande-Terre (separated from Basse-Terre by a narrow sea channel called Salt River). The adjacent French islands of La D sirade, Les Saintes, and Marie-Galante are under jurisdiction of Guadeloupe.
Western Basse-Terre has a rough volcanic relief while eastern Grande-Terre features rolling hills and flat plains. La Grande Soufri re is the highest mountain peak in the Lesser Antilles - 1467 m.
Further to the north, Saint-Barth lemy and the northern French part of Saint Martin were previously under the jurisdiction of Guadeloupe but on 7 December 2003, both of these areas voted to become an overseas territorial collectivity, a decision which took effect on 22 February 2007.



The island was devastated by several hurricanes in modern times:
On 12 September 1928, the Okeechobee hurricane caused extensive damage and killed thousands of people.
On 22 August 1964, Guadeloupe was ravaged by Hurricane Cleo, which killed 14 people.
On 27 September 1966, Category 3 Hurricane Inez caused extensive damage, mostly in Grande-Terre and north Basse-Terre Island, killing 33 people. Charles De Gaulle visited the islands after the hurricanes and declared them a disaster area.
On 17 September 1989, Category 4 Hurricane Hugo caused extensive damage, destroyed 10,000 homes leaving more than 35,000 homeless. It destroyed 100 percent of the banana crop, and 60 percent of the sugar cane crop.
From late August to mid September 1995, the island was in the path of three successive cyclones: Tropical Storm Iris on 28 August caused minor damages; Hurricane Luis on 5 September caused moderate damage on the north coast of Grande-Terre; and Hurricane Marilyn on 15 September caused moderate damage in Basse-Terre.
On 21 September 1998, Hurricane Georges pounded the islands, causing moderate damage and destroying 90% of the banana crop.







Guadeloupe has a population of 403,977 (2012).
The population of Guadeloupe is mainly of African or mixed descent. It is largely Roman Catholic, speaking French and a Creole patois (Antillean Creole). There are also Europeans, Indians, Lebanese, Syrians, Chinese, and I eri Amerindians (remnants of the original pre-European population). The archipelago of  les des Saintes is mostly populated by the descendants of colonists from Brittany and Normandy.







July 2006 estimates from the CIA World Factbook; note that these estimates disagree with official INSEE estimates and that they also include Saint-Martin and Saint-Barth lemy:



Life expectancy at birth is 77.0 years for males, and 83.5 for females (figures for 2011).




Guadeloupe sends four deputies to the French National Assembly and three senators to the French Senate. One of the four National Assembly constituencies still includes Saint-Martin and Saint-Barth lemy even though they seceded from Guadeloupe in 2007. This situation should last until 2012 when Saint-Martin and Saint-Barth lemy will send their own deputies to the French National Assembly.

Guadeloupe is divided into two arrondissements (Basse-Terre and Pointe- -Pitre), 21 cantons and 32 communes.




In 2006, the GDP per capita of Guadeloupe at market exchange rates, not at PPP, was  17,338 (US$21,780).
The economy of Guadeloupe depends on tourism, agriculture, light industry and services. It is dependent upon France for large subsidies and imports. Unemployment is especially high among the youth.



Tourism is a key industry, with 83.3% of tourists visiting from metropolitan France, 10.8% coming from the rest of Europe, 3.4% coming from the United States, 1.5% coming from Canada, 0.4% coming from South America, and 0.6% coming from the rest of the world. An increasingly large number of cruise ships visit the islands.



The traditional sugar cane crop is slowly being replaced by other crops, such as bananas (which now supply about 50% of export earnings), eggplant, guinnep, noni, sapotilla, paroka, pikinga, giraumon squash, yam, gourd, plantain, christophine, monbin, prunecaf , cocoa, jackfruit, pomegranate, and many varieties of flowers. Other vegetables and root crops are cultivated for local consumption, although Guadeloupe is dependent upon imported food, mainly from France.



Light industry features sugar and rum, solar energy, and many industrial products. Most manufactured goods and fuel are imported.




Guadeloupe's culture is probably best known for the islanders' literary achievements, particularly the poetry of Saint-John Perse, the pseudonym used by Alexis L ger. Perse won the 1960 Nobel Prize in Literature "for the soaring flight and the evocative images of his poetry, which, in a visionary fashion, reflects the conditions of our time."
Guadeloupe has always had a rich literary output, continued today by many living writers, poets, novelists, essayists and journalists, among them Mesdames Maryse Cond  and Simone Schwarz-Bart, Ernest P pin.




Music and dance are also very popular, and the widely accepted interaction of African, French and Indian cultures has given birth to some original new forms specific to the archipelago. Islanders enjoy many local dance styles including zouk, zouk-love, kompa, as well as the modern international dances such as hip hop, etc.
One of his most famous artists it was Henri Debs (1932-2013) a musician and producer of French, origin of Lebanese parents, who made many Caribbean rhythms like Zouk (Soca in Spanish) and Bel  were heard throughout the Antilles, North, Central, Suramerica and France.
Traditional Guadeloupean music includes la biguine, kadans, cadence-lypso, zouk, and gwo ka such as Anzala and Ti Celeste. Popular music artists and bands such as Experience 7, Francky Vincent, Kassav' (which included Patrick St-Eloi), and Gilles Floro embody the traditional music style of the island and the new generation of music, while some other musical artists, like Tom Frager (who grew up in Guadeloupe), perform colorful reggae music that defines the Guadeloupe island as paradise-like. Many international festivals take place in Guadeloupe, like the Creole Blues Festival, hosted in Marie-Galante. All the Euro-French forms of art are also ubiquitous. The melting pot is emphasized by other communities (from Brazil, Dominican Republic, Haiti, India, Lebanon, Syria), who live on the island and share their cultures.
While not in the Guadeloupean style, Catherine Quinol ("Katrin"), is known worldwide as the lip-synching icon of the piano-house trio Black Box, who burst on to the music scene in the late 1980s with songs such as "Ride On Time". Katrin is, however, a trained singer and she went on to release her own work.
Another element of Guadeloupean culture is its dress. A few women (particularly of the older generation) wear a unique style of traditional dress, with many layers of colourful fabric, now only worn on special occasions. On festive occasions they also wore a madras (originally a "kerchief" from South India) head scarf tied in many different symbolic ways, each with a different name. The headdress could be tied in the "bat" style, or the "firefighter" style, as well as the "Guadeloupean woman". Jewelry, mainly gold, is also important in the Guadeloupean lady's dress, a product of European, African and Indian inspiration.
French born dancers, choreographers, comedians Laurent and Larry Bourgeois are also of Guadelupean descent. Noted for their fresh take on hip-hop dance, this twin duo made an impression on the dance world while touring with Michael Jackson and Beyonce on the Immortal tour.




Football (soccer) is popular in Guadeloupe, and several notable footballers are of Guadeloupean origin:
Thierry Henry, a star of the French national team and MLS club New York Red Bulls, often visits, as his father Antoine was originally from the island.
William Gallas (Guadeloupean parentage).
Lilian Thuram, defender for France and most notably FC Barcelona, was born in Guadeloupe.
The former Manchester United, Everton and France striker Louis Saha.
Newcastle United F.C. striker Yoan Gouffran.
Kettering Town goalkeeper Willy Gueret.
Pascal Chimbonda, footballer. Chimbonda was born in Guadeloupe.
Parma F.C. star Jonathan Biabiany.
St phane Auvray currently plays for New York Red Bulls in Major League Soccer.
Ronald Zubar and his younger brother St phane, who are both footballers, were born in Guadeloupe.
Miguel Comminges, who currently plays as a defender for English side Stevenage F.C..
Dimitri Foulquier, who plays as a defender at Granada CF.
Teddy Riner
The national football team experienced success in 2007, advancing all the way to the 2007 CONCACAF Gold Cup semi-finals, where they were defeated 1 0 by CONCACAF powerhouse Mexico.
National Football League, NFL, *Jacques Chery was undrafted by the Dallas Cowboys and later traded to the Buffalo Bills based in New York, and he also played in the British American Football League for Manchester Titans, Yorkshire Rams and Doncaster Mustangs.
Basketball is also popular. Best known players are the NBA players Micka l Pi trus, Johan Petro, Rodrigue Beaubois, and Mickael Gelabale (now playing in Russia), who were born on the island. Also known is trainer and former player Paul Chonchon, after whom a basketball stadion in Pointe- -Pitre is named.
Many fine track and field athletes, such as Marie-Jos  P rec, Patricia Girard-L no, Christine Arron, and Wilhem Belocian, are also Guadeloupe natives. Triple Olympic champion Marie-Jos  P rec, fourth-fastest 100m runner Christine Arron, and fencing champion Laura Flessel were all born and raised in Guadeloupe.
Even though Guadeloupe is part of France, it has its own sports teams. Rugby union is a small but rapidly growing sport in Guadeloupe. France international and RC Toulon centre Mathieu Bastareaud (cousin of footballer William Gallas) was born in Guadeloupe.
The island is also internationally best known for hosting the Karujet Race   Jet Ski World Championship since 1998. This nine-stage, four-day event attracts competitors from around the world (mostly Caribbeans, Americans, and Europeans). The Karujet, generally made up of seven races around the island, has an established reputation as one of the most difficult championships in which to compete.
The Route du Rhum is one of the most prominent nautical French sporting events, occurring every four years.
Bodybuilder Serge Nubret was born in Anse-Bertrand, Grande-Terre, representing the French state in various bodybuilding competitions throughout the 1960s and 1970s, taking 2nd place in both the 1973 and 1975 IFBB Mr. Olympia contests. Bodybuilder Marie-Laure Mahabir also hails from Guadeloupe.


