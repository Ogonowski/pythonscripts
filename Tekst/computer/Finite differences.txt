A finite difference is a mathematical expression of the form f(x + b)   f(x + a). If a finite difference is divided by b   a, one gets a difference quotient. The approximation of derivatives by finite differences plays a central role in finite difference methods for the numerical solution of differential equations, especially boundary value problems.
Certain recurrence relations can be written as difference equations by replacing iteration notation with finite differences.
Today, the term "finite difference" is often taken as synonymous with finite difference approximations of derivatives, especially in the context of numerical methods. Finite difference approximations are finite difference quotients in the terminology employed above.
Finite differences have also been the topic of study as abstract self-standing mathematical objects, e.g. in works by George Boole (1860), L. M. Milne-Thomson (1933), and K roly Jordan (1939), tracing its origins back to Isaac Newton. In this viewpoint, the formal calculus of finite differences is an alternative to the calculus of infinitesimals.



Three forms are commonly considered: forward, backward, and central differences.
A forward difference is an expression of the form

Depending on the application, the spacing h may be variable or constant. When omitted, h is taken to be 1: .
A backward difference uses the function values at x and x   h, instead of the values at x + h and x:

 Finally, the central difference is given by



 The derivative of a function f at a point x is defined by the limit

If h has a fixed (non-zero) value instead of approaching zero, then the right-hand side of the above equation would be written

Hence, the forward difference divided by h approximates the derivative when h is small. The error in this approximation can be derived from Taylor's theorem. Assuming that f is differentiable, we have

The same formula holds for the backward difference:

However, the central (also called centered) difference yields a more accurate approximation. If f is twice differentiable,

The main problem with the central difference method, however, is that oscillating functions can yield zero derivative. If f(nh)=1 for n odd, and f(nh)=2 for n even, then f ' (nh)=0 if it is calculated with the central difference scheme. This is particularly troublesome if the domain of f is discrete.
Authors for whom finite differences mean finite difference approximations define the forward/backward/central differences as the quotients given in this section (instead of employing the definitions given in the previous section).



In an analogous way one can obtain finite difference approximations to higher order derivatives and differential operators. For example, by using the above central difference formula for f ' (x+h/2) and f ' (x h/2) and applying a central difference formula for the derivative of f ' at x, we obtain the central difference approximation of the second derivative of f:
2nd order central

Similarly we can apply other differencing formulas in a recursive manner.
2nd order forward

More generally, the n-th order forward, backward, and central differences are given by, respectively,
Forward

or for h=1,

Backward

Central

These equations are using binomial coefficients after the summation sign shown as . Each row of Pascal's triangle provides the coefficient for each value of i.
Note that the central difference will, for odd n, have h multiplied by non-integers. This is often a problem because it amounts to changing the interval of discretization. The problem may be remedied taking the average of  and .
Forward differences applied to a sequence are sometimes called the binomial transform of the sequence, and have a number of interesting combinatorial properties. Forward differences may be evaluated using the N rlund Rice integral. The integral representation for these types of series is interesting, because the integral can often be evaluated using asymptotic expansion or saddle-point techniques; by contrast, the forward difference series can be extremely hard to evaluate numerically, because the binomial coefficients grow rapidly for large n.
The relationship of these higher-order differences with the respective derivatives is straightforward,

Higher-order differences can also be used to construct better approximations. As mentioned above, the first-order difference approximates the first-order derivative up to a term of order h. However, the combination

approximates f'(x) up to a term of order h2. This can be proven by expanding the above expression in Taylor series, or by using the calculus of finite differences, explained below.
If necessary, the finite difference can be centered about any point by mixing forward, backward, and central differences.



Using a little linear algebra, one can fairly easily construct approximations, which sample an arbitrary number of points to the left and a (possibly different) number of points to the right of the center point, for any order of derivative. This involves solving a linear system such that the Taylor expansion of the sum of those points, around the center point, well approximates the Taylor expansion of the desired derivative.
This is useful for differentiating a function on a grid, where, as one approaches the edge of the grid, one must sample fewer and fewer points on one side.
The details are outlined in these notes.



For all positive k and n

Leibniz rule:



An important application of finite differences is in numerical analysis, especially in numerical differential equations, which aim at the numerical solution of ordinary and partial differential equations respectively. The idea is to replace the derivatives appearing in the differential equation by finite differences that approximate them. The resulting methods are called finite difference methods.
Common applications of the finite difference method are in computational science and engineering disciplines, such as thermal engineering, fluid mechanics, etc.



The Newton series consists of the terms of the Newton forward difference equation, named after Isaac Newton; in essence, it is the Newton interpolation formula, first published in his Principia Mathematica in 1687, namely the discrete analog of the continuum Taylor expansion,

which holds for any polynomial function f and for most (but not all) analytic functions. Here, the expression

is the binomial coefficient, and

is the "falling factorial" or "lower factorial", while the empty product (x)0 is defined to be 1. In this particular case, there is an assumption of unit steps for the changes in the values of x, h = 1 of the generalization below.
Note the formal correspondence of this result to Taylor's theorem. Historically, this, as well as the Chu Vandermonde identity,

(following from it, and corresponding to the binomial theorem), are included in the observations that matured to the system of the umbral calculus.
To illustrate how one may use Newton's formula in actual practice, consider the first few terms of doubling the Fibonacci sequence f = 2, 2, 4, ... One can find a polynomial that reproduces these values, by first computing a difference table, and then substituting the differences that correspond to x0 (underlined) into the formula as follows,

For the case of nonuniform steps in the values of x, Newton computes the divided differences,

the series of products,

and the resulting polynomial is the scalar product,  .
In analysis with p-adic numbers, Mahler's theorem states that the assumption that f is a polynomial function can be weakened all the way to the assumption that f is merely continuous.
Carlson's theorem provides necessary and sufficient conditions for a Newton series to be unique, if it exists. However, a Newton series does not, in general, exist.
The Newton series, together with the Stirling series and the Selberg series, is a special case of the general difference series, all of which are defined in terms of suitably scaled forward differences.
In a compressed and slightly more general form and equidistant nodes the formula reads



The forward difference can be considered as a difference operator, which maps the function f to  h[f ]. This operator amounts to

where Th is the shift operator with step h, defined by Th[f ](x) = f(x+h), and I is the identity operator.
The finite difference of higher orders can be defined in recursive manner as  hn    h ( hn 1). Another equivalent definition is  hn = [Th  I]n.
The difference operator  h is a linear operator and it satisfies a special Leibniz rule indicated above,  h(f(x)g(x)) = ( hf(x)) g(x+h) + f(x) ( hg(x)). Similar statements hold for the backward and central differences.
Formally applying the Taylor series with respect to h, yields the formula

where D denotes the continuum derivative operator, mapping f to its derivative f'. The expansion is valid when both sides act on analytic functions, for sufficiently small h. Thus, Th=ehD, and formally inverting the exponential yields

This formula holds in the sense that both operators give the same result when applied to a polynomial.
Even for analytic functions, the series on the right is not guaranteed to converge; it may be an asymptotic series. However, it can be used to obtain more accurate approximations for the derivative. For instance, retaining the first two terms of the series yields the second-order approximation to f (x) mentioned at the end of the section Higher-order differences.
The analogous formulas for the backward and central difference operators are

The calculus of finite differences is related to the umbral calculus of combinatorics. This remarkably systematic correspondence is due to the identity of the commutators of the umbral quantities to their continuum analogs (h 0 limits),

A large number of formal differential relations of standard calculus involving functions f(x) thus map systematically to umbral finite-difference analogs involving f(xTh 1).
For instance, the umbral analog of a monomial xn is a generalization of the above falling factorial (Pochhammer k-symbol),
 ,
so that

hence the above Newton interpolation formula (by matching coefficients in the expansion of an arbitrary function f(x) in such symbols), and so on.
For example, the umbral sine is

As in the continuum limit, the eigenfunction of  h /h also happens to be an exponential,

and hence Fourier sums of continuum functions are readily mapped to umbral Fourier sums faithfully, i.e., involving the same Fourier coefficients multiplying these umbral basis exponentials. This umbral exponential thus amounts to the exponential generating function of the Pochhammer symbols.
Thus, for instance, the Dirac delta function maps to its umbral correspondent, the cardinal sine function,

and so forth. Difference equations can often be solved with techniques very similar to those for solving differential equations.
The inverse operator of the forward difference operator, so then the umbral integral, is the indefinite sum or antidifference operator.



Analogous to rules for finding the derivative, we have:
Constant rule: If c is a constant, then

Linearity: if a and b are constants,

All of the above rules apply equally well to any difference operator, including  as to .
Product rule:

Quotient rule:
or

Summation rules:

See Refs 



A generalized finite difference is usually defined as

where  is its coefficients vector. An infinite difference is a further generalization, where the finite sum above is replaced by an infinite series. Another way of generalization is making coefficients  depend on point  : , thus considering weighted finite difference. Also one may make step  depend on point  : . Such generalizations are useful for constructing different modulus of continuity.
The generalized difference can be seen as the polynomial rings  . It leads to difference algebras.
Difference operator generalizes to M bius inversion over a partially ordered set.
As a convolution operator: Via the formalism of incidence algebras, difference operators and other M bius inversion can be represented by convolution with a function on the poset, called the M bius function  ; for the difference operator,   is the sequence (1,  1, 0, 0, 0, ...).



Finite differences can be considered in more than one variable. They are analogous to partial derivatives in several variables.
Some partial derivative approximations are (using central step method):

Alternatively, for applications in which the computation of f is the most costly step, and both first and second derivatives must be computed, a more efficient formula for the last case is

since the only values to compute that are not already needed for the previous four equations are f(x+h, y+k) and f(x h, y k).


