In the microelectronics industry a semiconductor fabrication plant (commonly called a fab; sometimes foundry) is a factory where devices such as integrated circuits are manufactured.
A business that operates a semiconductor fab for the purpose of fabricating the designs of other companies, such as fabless semiconductor companies, is known as a foundry. If a foundry does not also produce its own designs, it is known as a pure-play semiconductor foundry.
Fabs require many expensive devices to function. Estimates put the cost of building a new fab over one billion U.S. dollars with values as high as $3 4 billion not being uncommon. TSMC invested $9.3 billion in its Fab15 300 mm wafer manufacturing facility in Taiwan.
The central part of a fab is the clean room, an area where the environment is controlled to eliminate all dust, since even a single speck can ruin a microcircuit, which has features much smaller than dust. The clean room must also be dampened against vibration and kept within narrow bands of temperature and humidity. Controlling temperature and humidity is critical for minimizing static electricity.
The clean room contains the steppers for photolithography, etching, cleaning, doping and dicing machines. All these devices are extremely precise and thus extremely expensive. Prices for most common pieces of equipment for the processing of 300 mm wafers range from $700,000 to upwards of $4,000,000 each with a few pieces of equipment reaching as high as $50,000,000 each (e.g. steppers). A typical fab will have several hundred equipment items.



Typically an advance in chip-making technology requires a completely new fab to be built. In the past, the equipment to outfit a fab was not terribly expensive and there were a huge number of smaller fabs producing chips in small quantities. However, the cost of the most up-to-date equipment has since grown to the point where a new fab can cost several billion dollars.
Another side effect of the cost has been the challenge to make use of older fabs. For many companies these older fabs are useful for producing designs for unique markets, such as embedded processors, flash memory, and microcontrollers. However, for companies with more limited product lines, it's often best to either rent out the fab, or close it entirely. This is due to the tendency of the cost of upgrading an existing fab to produce devices requiring newer technology to exceed the cost of a completely new fab.
There has been a trend to produce ever larger wafers, so each process step is being performed on more and more chips at once. The goal is to spread production costs (chemicals, fab time) over a larger number of saleable chips. It is impossible (or at least impracticable) to retrofit machinery to handle larger wafers. This is not to say that foundries using smaller wafers are necessarily obsolete; older foundries can be cheaper to operate, have higher yields for simple chips and still be productive.
The current, as of 2014, state-of-the-art for wafer size is 300 mm (12 in). The industry is aiming to move to the 450 mm wafer size by 2018. As of March 2014, Intel expects 450 mm deployment by 2020. Additionally, there is a large push to completely automate the production of semiconductor chips from beginning to end. This is often referred to as the "lights-out fab" concept.
The International Sematech Manufacturing Initiative (ISMI), an extension of the US consortium SEMATECH, is sponsoring the "300 mm Prime" initiative. An important goal of this initiative is to enable fabs to produce smaller lots of chips as a response to shorter lifecycles seen in consumer electronics. The logic is that such a fab can produce smaller lots more easily and can efficiently switch its production to supply chips for a variety of new electronic devices. Another important goal is to reduce the waiting time between processing steps.



Foundry model for the business aspects of foundries and fabless companies
Klaiber's law
List of semiconductor fabrication plants
Rock's law
Semiconductor consolidation
Semiconductor device fabrication for the process of manufacturing devices






Handbook of Semiconductor Manufacturing Technology, Second Edition by Robert Doering and Yoshio Nishi (Hardcover   Jul 9, 2007)
Semiconductor Manufacturing Technology by Michael Quirk and Julian Serda (paperback   Nov 19, 2000)
Fundamentals of Semiconductor Manufacturing and Process Control by Gary S. May and Costas J. Spanos (hardcover   May 22, 2006)
The Essential Guide to Semiconductors (Essential Guide Series) by Jim Turley (paperback   Dec 29, 2002)
Semiconductor Manufacturing Handbook (McGraw Hill Handbooks) by Hwaiyu Geng (hardcover   April 27, 2005)



"Chip Makers Watch Their Waste", The Wall Street Journal, July 19, 2007, p.B3



ISMI Press Release