The Federal Department of Economic Affairs, Education and Research (EAER, German: Eidgen ssisches Departement f r Wirtschaft, Bildung und Forschung; French: D partement f d ral de l' conomie, de la formation et de la recherche; Italian: Dipartimento federale dell'economia, della formazione e della ricerca) is one of the seven departments of the federal government of Switzerland, headed by a member of the Swiss Federal Council.
The department was renamed from Federal Department of Economic Affairs (FDEA) effective on 1 January 2013 based on decisions taken by the Federal Council in 2011.



The Department is composed of the following offices:
General Secretariat, including the Federal Consumer Affairs Bureau FCAB (responsible for consumer affairs) and the Swiss civilian service agency (ZIVI).
State Secretariat for Economic Affairs (SECO): Responsible for national and international economic policy, trade negotiations and labour policy.
Federal Office for Agriculture (FOAG): Responsible for agricultural policy and for direct payments to Swiss farmers.
State Secretariat for Education, Research and Innovation (SERI)
Federal Veterinary office (FVO): Responsible for animal welfare and health, the safety of food of animal origin and the implementation of the CITES convention.
Integration Office (see FDFA above)
Federal Office for National Economic Supply (FONES): Manages emergency supplies of essential goods and services.
Federal Housing Office (FHO): Responsible for housing policy.
The following independent authorities are affiliated to the FDEA for administrative purposes:
Price Supervisor: Price ombudsman and responsible for the supervision of regulated prices.
Competition Commission: Swiss competition regulator.
Swiss Federal Institute for Vocational Education and Training (SFIVET): Provides training for vocational education professionals.



1848-1872: Department of Trade and Customs
1873-1878: Department of Railway and Trade
1879-1887: Department of Trade and Agriculture
1888-1895: Department of Industry and Agriculture
1896-1914: Department of Trade, Industry and Agriculture
1915-1978: Department of Economic Affairs
1979 2012: Federal Department of Economic Affairs
since 2013: Federal Department of Economic Affairs, Education and Research









Swiss University Conference



Federal Department of Economic Affairs, Education and Research official site