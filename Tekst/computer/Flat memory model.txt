Flat memory model or linear memory model refers to a memory addressing paradigm in which "memory appears to the program as a single contiguous address space." The CPU can directly (and linearly) address all of the available memory locations without having to resort to any sort of memory segmentation or paging schemes.
Memory management and address translation can still be implemented on top of a flat memory model in order to facilitate the operating system's functionality, resource protection, multitasking or to increase the memory capacity beyond the limits imposed by the processor's physical address space, but the key feature of a flat memory model is that the entire memory space is linear, sequential and contiguous from address zero to MaxBytes   1.
In a simple controller, or in a single tasking embedded application, where memory management is not needed nor desirable, the flat memory model is the most appropriate, because it provides the simplest interface from the programmer's point of view, with direct access to all memory locations and minimum design complexity.
In a general purpose computer system, which requires multitasking, resource allocation and protection, the flat memory system must be augmented by some memory management scheme, which is typically implemented through a combination of dedicated hardware (inside or outside the CPU) and software built into the operating system. The flat memory model (at the physical addressing level) still provides the greatest flexibility for implementing this type of memory management.
The majority of processor architectures implement a flat memory design, including all early 8-bit processors, the Motorola 68K series, etc. One exception to this rule was the original 8086, Intel's first 16-bit microprocessor, which implemented a segmented memory model because it allowed access to more than 64 KiB of memory without the cost of extending all addresses to more than 16-bits.



Most modern memory models fall into one of three categories:



Simple interface for programmers, clean design
Greatest flexibility
Minimum hardware and CPU real estate for simple controller applications
Maximum execution speed
Not suitable for general computing or multitasking operating systems, unless enhanced with additional memory management hardware/software; but this is almost always the case in modern CISC processors, which implement advanced memory management and protection technology over a flat memory model. Linux e.g. uses a flat memory model, see x86 memory segmentation#Practices.



Suitable for multitasking, general operating system design, resource protection and allocation
Suitable for virtual memory implementation
More CPU real estate, somewhat lower speed
More complex to program
Rigid page boundaries, not always the most memory efficient
This is the memory model Pentium Pro and newer x86 CPUs use in 32-bit addressing mode when addressing memory using 36-bit addresses. It's called Physical Address Extension




Similar to paged memory, but paging is achieved by the implicit addition of two relatively shifted registers: segment:offset
Variable page boundaries, more efficient and flexible than the paged memory model
Quite complex and awkward from a programmer's point of view
More difficult for compilers
Pages can overlap / poor resource protection and isolation
Many to one address translation correspondence: Many segment:offset combinations resolve to the same physical address
Greater chance of programming errors
Implemented in original Intel 8086, 8088, 80186, 80286 and supported by 80386 and all subsequent x86 machines through to present day Pentium and Core 2 processors. This memory model has remained ever since in the x86 machines, which now provide multi-mode operation and rarely operate in the compatible segmented mode anyway. See x86 memory segmentation for details.
Within the x86 architectures, when operating in the real (compatible) mode, physical address is computed as:
Address = 16   segment + offset
(i.e. The 16-bit segment register is shifted left by 4 bits and added to a 16-bit offset, resulting in a 20-bit address.)







A Von Neumann architecture specifies that instructions and data are both stored within the same memory and transferred over the same bus. This type of architecture is the most space efficient, because any memory not used by the program is available for data and vice versa. It is most often used in general computing. Additionally, a program can treat its own instructions as data and modify them during execution. This is known as self-modifying code.




A Harvard architecture separates instructions and data into two separate memories which are typically accessed over two separate buses. The benefit is an increase in system performance because data (for the present instruction) and code (for the future instruction) can be fetched at the same time. Also, in a Harvard architecture, the instruction and data buses can have different speeds and geometries. The drawback is that Harvard architectures force the designer to make assumptions about the ratio of instruction vs data memory, and this memory cannot be re-allocated at run time. The system is therefore unavoidably more wasteful. This type of architecture is most often used in digital signal processing (DSP), real-time control and ultra high-speed RISC applications, where performance is of utmost importance and everything else is secondary.
Neither of the above architectures determines whether the memory addressing model is flat, segmented, paged or otherwise.



Address space
Segmented memory
Intel Memory Model
Memory model (disambiguation)


