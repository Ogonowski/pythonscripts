During World War II, Nazi Germany engaged in deliberate extermination policies towards Soviet Union prisoners of war (POWs). This resulted in some 3.3 to 3.5 million deaths, about 60% of all Soviet POWs. During Operation Barbarossa, the Axis invasion of the Soviet Union, and the subsequent German Soviet War, millions of Red Army prisoners of war were taken. Some of them were arbitrarily executed in the field by the German forces, died under inhumane conditions in German prisoner-of-war camps and during ruthless death marches from the front lines, or were shipped to Nazi concentration camps for extermination.




It is estimated that at least 3.3 million Soviet POWs died in Nazi custody, out of 5.7 million. This figure represents a total of 57% of all Soviet POWs and may be contrasted with 8,300 out of 231,000 British and U.S. prisoners, or 3.6%. Some estimates range as high as 5 million dead, including those killed immediately after surrendering (an indeterminate, although certainly very large number). About 5% of the Soviet prisoners who died were of Jewish ethnicity; in some cases, circumcised Muslim prisoners were mistaken for religious Jews and killed.
The most deaths took place between June 1941 and January 1942, when the Germans killed an estimated 2.8 million Soviet POWs primarily through deliberate starvation, exposure, and summary execution, in what has been called, along with the Rwandan Genocide, an instance of "the most concentrated mass killing in human history (...) eclipsing the most exterminatory months of the Jewish Holocaust". By September 1941, the mortality rate among Soviet POWs was in the order of 1% per day. According to the United States Holocaust Memorial Museum (USHMM), by the winter of 1941, "starvation and disease resulted in mass death of unimaginable proportions". This deliberate starvation, leading many desperate prisoners to resort to acts of cannibalism, was Nazi policy in spite of food being available, in accordance to the Hunger Plan developed by the Reich Minister of Food Herbert Backe. For the Germans, Soviet POWs were expendable: they consumed calories needed by others and, unlike Western POWs, were considered to be subhuman.




The Commissar Order (German: Kommissarbefehl) was a written order given by Adolf Hitler on 6 June 1941, prior to the beginning of Operation Barbarossa (German invasion of the Soviet Union). It demanded that any Soviet political commissar identified among captured troops be shot immediately. Those prisoners who could be identified as "thoroughly bolshevized or as active representatives of the Bolshevist ideology" were also to be executed.




In the summer and fall/autumn of 1941, vast numbers of Soviet prisoners were captured in about a dozen large encirclements. Due to their rapid advance into the Soviet Union and an expected quick victory, the Germans did not want to ship these prisoners back to Germany. Under the administration of the Wehrmacht, the prisoners were processed, guarded, force marched, or transported in open rail cars to locations mostly in the occupied Soviet Union, Germany, and occupied Poland. Much like comparative occasions such as the Pacific War's Bataan Death March in 1942, the treatment of prisoners was brutal, without much in the way of supporting logistics. In the most extreme case, a forced march of prisoners from Gzhatsk to Smolensk in December 1941 may have resulted in the deaths of up to 400,000 men.
Soviet prisoners of war were stripped of their supplies and clothing by ill-equipped German troops when the cold weather set in. This resulted in fatal consequences for the prisoners. In the case of the Soviet POWs, most of the camps were simply open areas fenced off with barbed wire and watchtowers with no inmate housing. These meager conditions forced the crowded prisoners to live in holes they had dug for themselves, which were exposed to the elements. Beatings and other abuse by the guards were common, and prisoners were malnourished, often consuming only a few hundred calories per day. Medical treatment was nonexistent and an International Red Cross offer to help in 1941 was rejected by Hitler.
Some of Soviet POWs were also experimented on. In one such case, Dr. Heinrich Berning from Hamburg University starved prisoners to death while performing "famine experiments". In another instance, a group of prisoners at Zhitomir were shot using dum-dum bullets.



The camps established specially for Soviet prisoner-of-war were called Russenlager ("Russian camp"). In other camps, the Soviets were kept separated from the prisoners of other countries. The Allied regulars kept by Germany were usually treated in accordance with the 1929 Geneva Convention on Prisoners of War. However, although the Soviet Union was not a signatory, Germany was, and Article 82 of the Convention required signatories to treat all captured enemy soldiers "as between the belligerents who are parties thereto." Russenlager conditions were often even actually worse than those commonly experienced by prisoners in regular concentration camps. Such camps included:
Oflag IV-C: Allied officers at Colditz Castle were barred from sharing Red Cross packages with starving Soviet prisoners.
Oflag XIII-D: In July 1941 a new compound was set up in Oflag XIII-A for higher ranking Soviet military officers captured during Operation Barbarossa. It was closed in April 1942; the surviving officers (many had died during the winter due to an epidemic) were transferred to other camps.
Stalag 324: Once a week, sick inmates were to be shot. In all, some 80,000-90,000 out of 100,000-120,000 prisoners who were interned at Gr dy died between June 1941 and the winter of 1942.
Stalag 350/Z: According to the 1944 Soviet report, 43,000 captured Red Army personnel were either killed or died from diseases and starvation at this camp near Riga. The prisoners were used for the construction of Salaspils concentration camp in October 1941.
Stalag 359: An epidemic of dysentery led to the murder of some 6,000 Red Army prisoners between September 21 28, 1941 (3,261 of them on the first day), conducted by the Police Battalion 306 of the Ordnungspolizei. By mid-1942, about 20,000 Soviet POWs had perished there from hunger, disease and executions. The camp was then redesignated as the Poniatowa concentration camp for Jews (the main site of the Operation Harvest Festival massacre in 1943).
Stalag I-B: Tens of thousands of prisoners died in the camp, the vast majority of them Soviets.
Stalag II-B: The construction of the second camp, Lager-Ost, started in June 1941 to accommodate the large numbers of Soviet prisoners taken in Operation Barbarossa. In November 1941 a typhoid fever epidemic broke out in the Lager-Ost; it lasted until March 1942 and an estimated 45,000 prisoners died and were buried in mass graves. The camp administration did not start any preventive measures until some German soldiers became infected.
Stalag III-A: Mortality rates of Soviet prisoners was extremely high compared to the POWs of other nations, including around 2,000-2,500 Soviets who died in a typhus during the winter of 1941/1942. Non-Soviet prisoners were buried with military honours in individual graves at the camp cemetery, while the Russian dead were buried anonymously in mass graves.
Stalag III-C: In July 1941 Soviet prisoners captured during Operation Barbarossa arrived. They were held in separated facilities and suffered severe conditions and disease. The majority of the prisoners (up to 12,000) were killed, starved to death or died due to disease.
Stalag IV-A: In June September 1941 Soviet prisoners from Operation Barbarossa were placed in another camp. Conditions were appalling, and starvation, epidemics and ill-treatment took a heavy toll of lives; the dead Soviet prisoners were buried in mass graves.
Stalag IV-B: In July about 11,000 Soviet soldiers, and some officers, arrived. By April 1942 only 3,279 remained; the rest had died from malnutrition and a typhus epidemic caused by the deplorable sanitary conditions. Their bodies were buried in mass graves. After April 1942 more Soviet prisoners arrived and died just as rapidly. At the end of 1942 10,000 reasonably healthy Soviet prisoners were transferred to work in Belgian coal mines; the rest, suffering from tuberculosis, continued to die at the rate of 10 20 per day.
Stalag IV-H (Stalag 304): Of the 510,677 inmates in the camp before the typhoid fever epidemic in December 1941, only 3,729 were still alive when it ended in April 1942. In 1942 at least 1,000 were "weeded-out" by the Gestapo and shot.
Stalag V-A: During 1941 1942 many Soviet POWs arrived, but they were kept in separate enclosures and received much harsher treatment than the other prisoners. Thousands of them died of malnutrition and disease.
Stalag VI-C: Over 2,000 Soviet prisoners from Operation Barbarossa arrived in the summer of 1941. Conditions were appalling, starvation, epidemics and ill-treatment took a heavy toll of lives. The dead were buried in mass graves.
Stalag VI-K (Stalag 326): Between 40,000 and 60,000 prisoners died, mostly buried in three mass graves. A Soviet war cemetery is still in existence, containing about 200 named graves.
Stalag VII-A: During the five years about 1,000 prisoners died at the camp, over 800 of them Soviets (mostly officers). At the end of the war there were still 27 Soviet Army generals in the camp who had survived the mistreatment that they, like all Soviet prisoners, had been subjected to. The new prisoners were inspected upon arrival by local Munich Gestapo agents; some 484 were found to be "undesirable" and immediately sent to concentration camps and murdered.
Stalag VIII-C: In late 1941 nearly 50,000 prisoners were crowded into a space designed for only one third that number. Conditions were appalling, starvation, epidemics and ill-treatment took a heavy toll of lives. By early 1942 the survivors had been transferred to other camps.
Stalag VIII-E (Stalag VIII-C/Z): The first Soviets arrived in July 1941; by June 1942 more than 100,000 prisoners were crowded into this camp. As a result of starvation and disease, mainly typhoid fever and tuberculosis, close to half of them died before the end of the war.
Stalag VIII-F (Stalag 318 / Stalag 344): Of the estimated 300,000 Soviet prisoners who passed through this camp, about one third (some 100,000) died of starvation, mistreatment and disease in terrible physical and sanitary conditions.
Stalag X-B
Stalag XI-D (Stalag 321): In July 1941, over 10,000 Soviet army officers were imprisoned in a new sub-camp of Stalag XI-B. Thousands of them died in the winter of 1941/2 as the result of a typhoid fever epidemic.
Stalag XI-C: In July 1941, about 20,000 Soviet prisoners captured during Operation Barbarossa arrived; they were housed in the open while huts were being built. Some 14,000 POWs died during the winter of 1941 42. In late 1943 the POW camp was closed and the entire facility became Bergen-Belsen concentration camp.



In the "weeding-out actions" (Aussonderungsaktionen) in 1941 42, the Gestapo political police further identified Communist Party and state officials, commissars, academic scholars, Jews and other "undesirable" or "dangerous" individuals who survived the Commissar Order selections, and transferred them to concentration camps, where they were immediately summarily executed. At Stalag VII-A at Moosburg, Major Karl Meinel objected to these executions, but the SS (including Karl von Eberstein) intervened with military leadership, Meinel was demoted to reserve, and the killing continued.
In all, between June 1941 and May 1944 about 10% of all Soviet POWs were turned over to the SS-Totenkopfverb nde concentration camp organization or the Einsatzgruppen death squads and murdered. Einsatzgruppen killings included the Babi Yar massacres where Soviet POWs were among 70,000 120,000 people executed between 1941 and 1943 and the Ponary massacre that included the execution of some 7,500 Soviet POWs in 1941 (among about 100,000 murdered there between 1941 and 1944).




Between 140,000 and 500,000 Soviet prisoners of war died or were executed in Nazi concentration camps. Most of those executed were killed by shooting but some were gassed.
Auschwitz-Birkenau concentration camp: From about 15,000 Soviet POWs who were brought to Auschwitz I for work, only 92 remained alive at the last roll call. About 3,000 more were killed by being shot or gassed immediately after arriving. Out of the first 10,000 brought to work in 1941, 9,000 died in the first five months. A group of about 600 Soviet prisoners were gassed in the first Zyklon-B experiments on September 3, 1941; in December 1941, a further 900 Soviet POWs were murdered by means of gas. In March 1941, the SS chief Heinrich Himmler ordered the construction of a large camp for 100,000 Soviet POWs at Birkenau, in close proximity to the main camp. Most of the Soviet prisoners were dead by the time Birkenau was reclassified as the Auschwitz II concentration camp in March 1942.
Buchenwald concentration camp: 8,483 Soviet POWs were selected in 1941 1942 by three Dresden Gestapo officers and sent to the camp for immediate liquidation by a gunshot to the back of the neck, the infamous Genickschuss using a purpose-built facility.
Che mno extermination camp: The victims murdered at the Che mno killing center included several hundred Poles and Soviet POWs.
Dachau concentration camp: Some 500 Soviet POWs were executed by a firing squad in Dachau.
Flossenb rg concentration camp: More than 1,000 Soviet POWs were executed in Flossenb rg by the end of 1941; executions continued sporadically up to 1944. The POWs at one of the sub-camps staged a failed uprising and mass escape attempt on May 1, 1944. The SS also established a special camp for 2,000 Soviet POWs within Flossenb rg itself.
Gross-Rosen concentration camp: 65,000 Soviet POWs were killed by feeding them only a thin soup of grass, water, and salt for six months. In October 1941 the SS transferred about 3,000 Soviet POWs to Gross-Rosen for execution by shooting.
Hinzert concentration camp: A group of 70 POWs were told that they would undergo a medical examination, but instead were injected with potassium cyanide, a deadly poison.
Majdanek concentration camp: The first transport directed toward Majdanek consisted of 5,000 Soviet POWs arriving in the latter half of 1941, they soon died of starvation and exposure. Executions were also conducted there by the shooting of prisoners in trenches.
Mauthausen-Gusen concentration camp: Following the outbreak of the Soviet German War the camps started to receive a large number of Soviet POWs; most of them were kept in huts separated from the rest of the camp. Soviet POWs were a major part of the first groups to be gassed in the newly built gas chamber in early 1942; at least 2,843 of them were murdered in the camp. According to the USHMM, "so many POWs were shot that the local population complained that their water supply had been contaminated. The rivers and streams near the camp ran red with blood."
Neuengamme concentration camp: According to the testimony of Wilhelm Bahr, an ex-medical orderly, during the trial against Bruno Tesch, 200 Soviet POWs were gassed by prussic acid in 1942.
Sachsenhausen concentration camp: Soviet POWs were victims of the largest part of the executions that took place. Thousands of them were murdered immediately after arriving at the camp, including 9,090 executed between August 31 and October 2, 1941. Among those who died there was Lt. Yakov Dzhugashvili, the elder son of the Soviet dictator Joseph Stalin (either by suicide or shot).
Sobib r extermination camp: Soviet POWs of Jewish ethnicity were among hundreds of thousands people gassed at Sobib r. A group of captive Soviet officers led by 2nd Lt. Alexander Pechersky organized a successful mass breakout from Sobibor, after which the SS closed and dismantled the camp.




In January 1942, Hitler authorized better treatment of Soviet POWs because the war had bogged down, and German leaders decided to use prisoners for forced labour on a large scale (see forced labour under German rule during World War II). Their number increased from barely 150,000 in 1942, to the peak of 631,000 in the summer of 1944. Many were dispatched to the coal mines (between July 1 and November 10, 1943, 27,638 Soviet POWs died in the Ruhr Area alone), while others were sent to Krupp, Daimler-Benz or countless other companies, where they provided labour while often being slowly worked to death. The largest "employers" of 1944 were mining (160,000), agriculture (138,000) and the metal industry (131,000). No less than 200,000 prisoners died during forced labour.
The Organisation Todt was a civil and military engineering group in Germany eponymously named for its founder Fritz Todt. The organisation was responsible for a wide range of engineering projects both in pre-WWII Germany, and in Germany itself and occupied territories from France to Russia during the war, and became notorious for using forced labour. Most of the so-called "volunteer" Soviet POW workers were consumed by the Organisation Todt. The period from 1942 until the end of the war had approximately 1.4 million labourers in the service of the Organisation Todt. Overall, 1% were Germans rejected from military service and 1.5% were concentration camp prisoners; the rest were prisoners of war and compulsory labourers from occupied countries. All non-Germans were effectively treated as slaves and many did not survive the work or the war.




As many as 374,000 German prisoners of war (out of estimated 2.4 to 3.3 million) died in Soviet camps. According to Anne Applebaum, the official Soviet number was 570,000 deaths (the mortality rate is between 14% and 30%, depending on low and high estimates of deaths and total POW numbers): "In the few months of 1943, death rates among captured [German] POWs rose to 60 percent ... Similar death rates prevailed among Soviet soldiers in German captivity: the Nazi Soviet war was truly a fight to the death." One estimate says that almost a million German prisoners died in the Soviet camps. Out of the nearly 110,000 German prisoners taken at Stalingrad, only about 6,000 survived the captivity.



German war crimes
Severity Order
Soviet repressions against former prisoners of war









Bloodlands: Europe Between Hitler and Stalin by Timothy Snyder
Keine Kameraden: Die Wehrmacht und die Sowjetischen Kriegsgefangenen, 1941 1945 by Christian Streit
The Policies of Genocide: Jews and Soviet Prisoners of War in Nazi Germany by Gerhard Hirschfeld and Wolfgang J. Mommsen



Nazi persecution of Soviet Prisoners of War at the Holocaust Encyclopedia
Sowjetische Kriegsgefangene (German)
Stills from Soviet documentary "The Atrocities committed by German Fascists in the USSR" ((1); (2); (3))
Slide show "Nazi Crimes in the USSR (Graphic images!)"