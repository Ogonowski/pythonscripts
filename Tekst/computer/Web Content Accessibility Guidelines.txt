Web Content Accessibility Guidelines (WCAG) are part of a series of web accessibility guidelines published by the Web Accessibility Initiative (WAI) of the World Wide Web Consortium (W3C), the main international standards organization for the internet. They consist of a set of guidelines for making content accessible, primarily for people with disabilities, but also for all user agents, including highly limited devices, such as mobile phones. The current version, WCAG 2.0, was published in December 2008 and became an ISO standard, ISO/IEC 40500:2012 in October 2012.



The first web accessibility guideline was compiled by Gregg Vanderheiden and released in January 1995 just after the 1994 Second International Conference on the World-Wide Web (WWW II) in Chicago (where Tim Berners-Lee first mentioned disability access in a keynote speech after seeing a pre-conference workshop on accessibility led by Mike Paciello).
Over 38 different Web access guidelines followed from various authors and organizations over the next few years. These were brought together in the Unified Web Site Accessibility Guidelines compiled at the University of Wisconsin-Madison. Version 8 of the Unified Web Site Accessibility Guidelines, published in 1998, served as the starting point for the W3C's WCAG 1.0.



The WCAG 1.0 was published and became a W3C recommendation on 5 May 1999. They have since been superseded by WCAG 2.0.
WCAG 1.0 consist of 14 guidelines which are general principles of accessible design. Each guideline covers a basic theme of web accessibility and is associated with one or more checkpoints describing how to apply that guideline to particular features of webpages.
Guideline 1: Provide equivalent alternatives to auditory and visual content
Guideline 2: Don t rely on colour alone
Guideline 3: Use markup and style sheets, and do so properly
Guideline 4: Clarify natural language usage
Guideline 5: Create tables that transform gracefully
Guideline 6: Ensure that pages featuring new technologies transform gracefully
Guideline 7: Ensure user control of time sensitive content changes
Guideline 8: Ensure direct accessibility of embedded user interfaces
Guideline 9: Design for device independence
Guideline 10: User interim solutions
Guideline 11: Use W3C technologies and guidelines
Guideline 12: Provide context and orientation information
Guideline 13: Provide clear navigation mechanisms
Guideline 14: Ensure that documents are clear and simple
Each of the in total 65 WCAG 1.0 checkpoints has a priority level assigned to it which is based on the checkpoint's impact on accessibility:
Priority 1: Web developers must satisfy these requirements, otherwise it will be impossible for one or more groups to access the Web content. Conformance to this level is described as A.
Priority 2: Web developers should satisfy these requirements, otherwise some groups will find it difficult to access the Web content. Conformance to this level is described as AA or Double-A.
Priority 3: Web developers may satisfy these requirements, in order to make it easier for some groups to access the Web content. Conformance to this level is described as AAA or Triple-A.



In February 2008, The WCAG Samurai, a group of developers independent of the W3C, and led by Joe Clark, published corrections for, and extensions to, the WCAG 1.0.



WCAG 2.0 was published as a W3C Recommendation on 11 December 2008. It consists of 12 guidelines (untestable) organized under four principles (websites must be perceivable, operable, understandable and robust) and each guideline has testable success criteria (65 in all). The W3C's Techniques for WCAG 2.0 is a list of techniques that support authors to meet the guidelines and success criteria. The techniques are periodically updated whereas the principles, guidelines and success criteria are stable and do not change.






Information and user interface components must be presentable to users in ways they can perceive.
Guideline 1.1: Provide text alternatives for any non-text content so that it can be changed into other forms people need, such as large print, braille, speech, symbols or simpler language.
Guideline 1.2: Time-based media: Provide alternatives for time-based media.
Guideline 1.3: Create content that can be presented in different ways (for example simpler layout) without losing information or structure.
Guideline 1.4: Make it easier for users to see and hear content including separating foreground from background.



User interface components and navigation must be operable.
Guideline 2.1: Make all functionality available from a keyboard.
Guideline 2.2: Provide users enough time to read and use content.
Guideline 2.3: Do not design content in a way that is known to cause seizures.
Guideline 2.4: Provide ways to help users navigate, find content, and determine where they are.



Information and the operation of user interface must be understandable.
Guideline 3.1: Make text content readable and understandable.
Guideline 3.2: Make web pages appear and operate in predictable ways.
Guideline 3.3: Help users avoid and correct mistakes.



Content must be robust enough that it can be interpreted reliably by a wide variety of user agents, including assistive technologies.
Guideline 4.1.: Maximize compatibility with current and future user agents, including assistive technologies.
WCAG 2.0 uses the same three levels of conformance (A, AA, AAA) as WCAG 1.0, but has redefined them. The WCAG working group maintains an extensive list of web accessibility techniques and common failure cases for WCAG 2.0.



The first concept proposal of WCAG 2.0 was published on 25 January 2001. In the following years new versions were published intended to solicit feedback from accessibility experts and members of the disability community. On 27 April 2006 a "Last Call Working Draft" was published. Due to the many amendments that were necessary, WCAG 2.0 was published again as a concept proposal on 17 May 2007, followed by a second "Last Call Working Draft" on 11 December 2007. In April 2008 the guidelines became a "Candidate Recommendation". On 3 November 2008 the guidelines became a "Proposed Recommendation". WCAG 2.0 as published as a W3C Recommendation on 11 December 2008.
In October 2012, WCAG 2.0 was accepted by the International Organization for Standardization as an ISO International Standard, ISO/IEC 40500:2012.
A comparison of WCAG 1.0 checkpoints and WCAG 2.0 success criteria is available.



Businesses that have an online presence should provide accessibility to disabled users. Not only are there ethical and commercial justifications for implementing the Web Content Accessibility Guidelines, in many jurisdictions, there are also legal reasons. If a business's website is not accessibile, then the website owner could be sued for discrimination.
In January 2012, the Royal National Institute for the Blind (RNIB) in the United Kingdom issued a press release stating that it had served legal proceedings against low-cost airline bmibaby over their "failure to ensure web access for blind and partially sighted customers". As of October 2011, at least two actions against websites had been initiated by the RNIB, and settled without the cases being heard by a court.
An employment tribunal finding against the Project Management Institute (PMI), was decided in October 2006, and the company was ordered to pay compensation of  3,000 for discrimination.
The 2010/2012 Jodhan decision caused the Canadian Federal government to require all online web pages, documents and videos available externally and internally to meet the accessibility requirements of WCAG 2.0.
The Australian Government has also mandated via the Disability Discrimination Act 1992 that all Australian Government websites meet the WCAG accessibility requirements.
The Israeli Ministry of Justice published regulations in early 2014, requiring Internet websites to comply with Israeli standard 5568, which is based on the W3C Web Content Accessibility Guidelines 2.0. The main differences between the Israeli standard and the W3C standard concern the requirements to provide captions and texts for audio and video media. The Israeli standards are somewhat more lenient, reflecting the current technical difficulties in providing such captions and texts in Hebrew. 






W3C   Web Content Accessibility Guidelines (WCAG) 2.0
W3C   Web Accessibility Initiative (WAI) introduction to WCAG
WAVE   Online accessibility validator
WCAG 2.0 checklist
Achieving WCAG 2.0 with PDF/UA   Document published by the Association for Information and Image Management (AIIM)



W3C   Web Content Accessibility Guidelines (WCAG) 1.0
WCAG Samurai Introduction