Everex ("Ever for Excellence!") is a manufacturer of desktop and notebook personal computers. It was established in 1983 and headquartered in Fremont, California. The company was founded by Steve Hui, John Lee and Wayne Cheung. In 1988, Everex was the leader in tape backup sales with half the world market. By 1993, the company had slipped into bankruptcy and was acquired by the Formosa Plastics Group. On December 29, 2006 Everex Systems, Inc filed a voluntary petition for liquidation under Chapter 7 and in June 2008 NewMarket Technology has taken control of Everex



In 1983, Everex shipped its first hard drive, tape backup and graphics product. Seven years later, with an ever expanding product line, annual revenues totaled over $436 million and the workforce topped 2,200 employees. In 1985, Everex began shipping personal computers under private labels such as the popular IBM-AT compatible System 1800. Three years later the STEP computer line launched, introducing cutting edge 286 and 386-based computing to a mass audience. In addition to computer systems, high-performance file servers and a UNIX-based operating system (ESIX), the company produced tape drives, graphics boards, data and fax modems, memory enhancement products, network boards, desktop publishing products, disk drive and tape driver controllers and monitors.
1983 - Everex founded in Fremont, California
1984 - First Everex hard disk drive shipped
1986 - 286-Based STEP line of computers launched
1987 - Everex IPO under NASDAQ "EVRX"
1992 - Discussed a merger with Northgate Computers but these talks failed.
1993 - Everex files for Chapter 11 bankruptcy protection  They sell their Esix brand to James River Group for $210,000 and their storage division to Exabyte for $5.5M.
1993 - Everex purchased by the Formosa Plastics Group
1998 - Everex launches FreeStyle, the world's first Windows CE PDA, but abandoned the line later in the year
2007 - Everex launches its first 17" widescreen Vista notebook 
2007 - Everex launches low-cost green PC Impact GC3502 running gOS
2007 - Everex announces plans for sub-$300 Linux notebooks
2008 - Everex launches a series of low cost "green" systems, the CloudBook UMPC, the gBook notebook and the gPC mini Mac mini-like desktop, all running the Ubuntu-based gOS Linux with the GNOME desktop environment. Everex is later acquired by systems integrator Newmarket Technology.
2009 - The US subsidiary of Everex closes its doors, while the Japanese and Taiwanese subsidiaries seem to remain unaffected.



Zonbu, some of whose computers are based on Everex hardware
Everex green computers
First International Computer (FIC)



^ "Everex Systems, Inc.: Private Company Information - Businessweek". Investing.businessweek.com. 2006-12-29. Retrieved 2013-01-08. 
^ http://www.myebay.ro/everex-mostly-acquired-by-newmarket-9615.html
^ Everex About Us
^ The New York Times Company News
^ "Everex files for Chapter 11". Newsbytes News Network. 1993-01-05. Retrieved 2007-08-17. 
^ Everex sells business units in effort to get back in black. Computerworld. 8 February 1993. p. 85. ISSN 0010-4841. 
^ CNET News Everex abandons palm-sized PC line
^ "EVEREX XT5300T NoteBook AMD Mobile Athlon 64 X2 TK-53(1.70GHz) 17.0" Wide XGA+ 1GB Memory DDR2 667 120GB HDD 5400rpm DVD Super Multi NVIDIA GeForce 8600M GS". Newegg.com. Retrieved 2013-01-08. 
^ "Everex intros $298 green PC with OpenOffice". electronista. 2007-07-18. Retrieved 2007-08-08. 
^ "Everex Readies Sub-$300 Linux Notebooks". pcworld. 2007-11-02. Retrieved 2007-11-08. 
^ http://www.everexgreen.com Everex website for its new "green" systems.
^ PC World Cloudbook Maker Everex Snapped up by Newmarket
^ "Everex US folds after Cloudbook fails to find favor". SlashGear. 2009-07-15. Retrieved 2013-01-08. 
^ Everex Taiwan
^ "Notebook-style computing appliance runs Linux". 



Official website
Everex Company History