In computing, an input device is a peripheral (piece of computer hardware equipment) used to provide data and control signals to an information processing system such as a computer or information appliance. Examples of input devices include keyboards, mice, scanners, digital cameras and joysticks.
Many input devices can be classified according to:
modality of input (e.g. mechanical motion, audio, visual, etc.)
whether the input is discrete (e.g. key presses) or continuous (e.g. a mouse's position, though digitized into a discrete quantity, is fast enough to be considered continuous)
the number of degrees of freedom involved (e.g. two-dimensional traditional mice, or three-dimensional navigators designed for CAD applications)
Pointing devices, which are input devices used to specify a position in space, can further be classified according to:
Whether the input is direct or indirect. With direct input, the input space coincides with the display space, i.e. pointing is done in the space where visual feedback or the pointer appears. Touchscreens and light pens involve direct input. Examples involving indirect input include the mouse and trackball.
Whether the positional information is absolute (e.g. on a touch screen) or relative (e.g. with a mouse that can be lifted and repositioned)
Direct input is almost necessarily absolute, but indirect input may be either absolute or relative. For example, digitizing graphics tablets that do not have an embedded screen involve indirect input and sense absolute positions and are often run in an absolute input mode, but they may also be set up to simulate a relative input mode like that of a touchpad, where the stylus or puck can be lifted and repositioned.




A 'keyboard' is a human interface device which is represented as a layout of buttons. Each button, or key, can be used to either input a linguistic character to a computer, or to call upon a particular function of the computer. They act as the main text entry interface for most users. Traditional keyboards use spring-based buttons, though newer variations employ virtual keys, or even projected keyboards. It is typewriter like device composed of a matrix of switches.
Examples of types of keyboards include:
Keyer
Keyboard
Lighted Program Function Keyboard (LPFK)




Pointing devices are the most commonly used input devices today. A pointing device is any human interface device that allows a user to input spatial data to a computer. In the case of mice and touchpads, this is usually achieved by detecting movement across a physical surface. Analog devices, such as 3D mice, joysticks, or pointing sticks, function by reporting their angle of deflection. Movements of the pointing device are echoed on the screen by movements of the pointer, creating a simple, intuitive way to navigate a computer's graphical user interface (GUI).



Some devices allow many continuous degrees of freedom as input. These can be used as pointing devices, but are generally used in ways that don't involve pointing to a location in space, such as the control of a camera angle while in 3D applications. These kinds of devices are typically used in virtual reality systems (CAVEs), where input that registers six degrees of freedom is required.




Input devices, such as buttons and joysticks, can be combined on a single physical device that could be thought of as a composite device. Many gaming devices have controllers like this. Technically mice are composite devices, as they both track movement and provide buttons for clicking, but composite devices are generally considered to have more than two different forms of input.
Game controller
Gamepad (or joypad)
Paddle (game controller)
Jog dial/shuttle (or knob)
Wii Remote




Video input devices are used to digitize images or video from the outside world into the computer. The information can be stored in a multitude of formats depending on the user's requirement.
Digital camera
Digital camcorder
Portable media player
Webcam
Microsoft Kinect Sensor
Image scanner
Fingerprint scanner
Barcode reader
3D scanner
Laser rangefinder
Eye gaze tracker
Medical Imaging
Computed tomography
Magnetic resonance imaging
Positron emission tomography
Medical ultrasonography



Audio input devices are used to capture sound. In some cases, an audio output device can be used as an input device, in order to capture produced sound.
Microphones
MIDI keyboard or other digital musical instrument



See Punched card input/output.



Electronic pen
Magnetic ink character recognition
Sip-and-puff#Computer_input_device



Peripheral
Display device
Output device



N. P. Milner. 1988. A review of human performance and preferences with different input devices to computer systems. In Proceedings of the Fourth Conference of the British Computer Society on People and computers IV, D. M. Jones and R. Winder (Eds.). Cambridge University Press, New York, NY, USA, 341-362. ISBN 0-521-36553-8