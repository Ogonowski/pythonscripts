Scott Air Force Base (IATA: BLV, ICAO: KBLV, FAA LID: BLV) is a United States Air Force base in St. Clair County, Illinois, near Belleville. Scott Field was one of thirty-two Air Service training camps established after the United States entered World War I in April 1917. It is part of the Air Mobility Command (AMC), and is also the headquarters of the U.S. Transportation Command, a Unified Combatant Command that coordinates transportation across all the services.
The base is operated by the 375th Air Mobility Wing (375 AMW) and is also home to the Air Force Reserve Command's 932d Airlift Wing (932 AW) and the Illinois Air National Guard's 126th Air Refueling Wing (126 ARW), the latter two units being operationally gained by AMC.
The base currently employs 13,000 people, 5,100 civilians with 5,500 active-duty Air Force, and an additional 2,400 Air National Guard and Reserve personnel. It was announced in June 2014 that two new cybersecurity squadrons will be added to the three currently on base.
Its airfield is also used by civilian aircraft, with civilian operations at the base referring to the facility as MidAmerica St. Louis Airport. MidAmerica has operated as a Joint Use Airport since beginning operations in November 1997. Allegiant Air, the only commercial with scheduled flights at the airport, pulled out of the airport on January 3, 2009, but resumed direct flights to Orlando in 2012.



Scott Air Force Base is home to the headquarters of many major military organizations, including:
United States Transportation Command (USTRANSCOM)
United States Army Surface Deployment and Distribution Command (SDDC)
Air Mobility Command (AMC)
Eighteenth Air Force (18 AF)
635th Supply Chain Operations Wing (635 SCOW)
Defense Information Technology Contracting Organization (DITCO)
Defense Information Systems Agency, Continental United States (DISA CONUS)
Air Force Network Integration Center (AFNIC) * formerly the Air Force Communications Agency (AFCA)
The 375th Air Mobility Wing is the host to more than 30 tenant units, including the Air Force Office of Special Investigations 3rd Field Investigations Region, the 932d Airlift Wing (Air Force Reserve Command), the 126th Air Refueling Wing (Illinois Air National Guard), and the 3rd Manpower Requirements Squadron (Air Force Manpower Agency).






During World War I, Secretary of War Newton Baker advocated an expanded role for aviation. Business and political leaders on both sides of the Mississippi River wanted the Midwest to be chosen as a site for one of the new "flying fields." Aerial expert Albert Bond Lambert joined the St. Louis Chamber of Commerce and directors of the Greater Belleville Board of Trade to negotiate a lease agreement for nearly 624 acres of land.
After inspecting several sites, the U.S. War Department agreed to the lease June 14, 1917. Congress appropriated $10 million for its construction, and 2,000 laborers and carpenters were immediately put to work. The layout of Scott Field was typical of aviation fields built during World War I. Construction began in June 1917. The government gave the Unit Construction Company 60 days to erect approximately 60 buildings, lay a mile-long railroad spur, and to level off an airfield with a 1,600 foot landing circle. Construction was underway when the government announced, on July 20, 1917, that it would name the new field after Corporal Frank S. Scott, the first enlisted service member killed in an aviation crash. Scott had died after an unexpected engine failure had brought down the aircraft that Lieutenant Lewis Rockwell had been giving him an orientation flight in at College Park, MD, on September 28, 1912.
Scott Field began as an aviation-training field for World War I pilots in August 1917 when the 11th and 21st Aero Squadrons from Kelly Field, Texas, arrived. Construction was completed in August, and the first flight from Scott Field occurred 2 September 1917. Flying instruction began 11 September 1917. Later the 85th and 86th Aero Squadrons arrived, and more than 300 pilots and many ground units were trained for service by the war's end in 1918.
Training units assigned to Scott Field were:
Post Headquarters, Scott Field - October 1919
114th Aero Squadron, February 1918
Re-designated as Squadron "A", July November 1918
221st Aero Squadron, December 1917
Re-designated as Squadron "B", July November 1918
242d Aero Squadron (II), April 1918
Re-designated as Squadron "C", July November 1918
Squadron "D", July November 1918
Flying School Detachment (Consolidation of Squadrons A-D), November 1918-November 1919
By 2 September, William Couch, a civilian flying instructor, and Scott Field Commander, Major George E. A. Reinburg, made the first flight from Scott Field in a Standard J-1 two-seater biplane. At least seven J-1s were already on Scott; by the time the first Curtiss JN-4D  Jenny  arrived. Operable from the front or rear seat, the 8-cylinder, 90- horsepower, Jenny would become the primary trainer used on Scott Field. Everything moved fast in a wartime environment, including the dangerous act of learning to fly airplanes. A judgment error or weather shift could produce severe accidents in the fragile aircraft of the day, so it soon became apparent that Scott Field needed a medical air evacuation capability.

Determined to improve the recovery of Scott s downed pilots, Captains Charles Bayless (post surgeon), Early Hoag (officer-in-charge of flying) and A.J. Etheridge (post engineer), along with Second Lieutenant Seth Thomas, designed two air ambulances, or hospital planes using a configuration likely modeled after one in use at Gerstner Field, Louisiana. By the summer of 1918, Scott Field s engineering department had completed the rear cockpit modifications needed to convert two Jennies. Not long thereafter, on 24 August 1918, as aviator with a broken leg became Scott s first air evacuated patient.
Also reassuring was the support Scott Field enjoyed from the local community. Plenty of curious sightseers came just to watch the construction or catch a glimpse of airplane activity, but many from the local community also gave morale support to their  Sammies  (Uncle Sam s boys). They hosted dances and receptions, established a library branch on the field, and invited soldiers into their homes for Thanksgiving dinners. Likewise, Scott Field hosted sporting events with their community neighbors and, on 17 August 1918, they invited the public to attend a Field Meet and Flight Exhibition Scott s first Air Show.
Flying was discontinued at Scott Field after the war and the base population dropped. The War Department purchased Scott Field in 1919 for $119, 285.84




Scott Field s future became uncertain after the 11 November 1918 armistice with Germany and the end of World War I. Large scale demobilization closed many U.S. airfields. Scott s remaining units were organized into a Flying School Detachment, and the field itself was designated as a storage site for demobilized equipment. Welcome news came early in 1919, with the War Department s announcement of its decision to purchase Scott Field, a decision influenced by Scott s central location and exceptional purchase price of $119,285.84. This gave Scott a promise of a future; however, it still lacked a mission.
Scott was transformed into a lighter-than-air (LTA) station in 1921, with the transfer of the Army Balloon and Airship School from Brooks Field, Texas. Lighter-than-air ships were used at Scott Field to research the capabilities of aerial photography, meteorology and conduct altitude experiments. The most notable addition was the new airship hangar. Constructed between September 1921 and January 1923, it was three blocks long, nearly one block wide and 15 stories high. One report commented that 100,000 men nearly the entire U.S. Army in 1923 could have stood in formation inside it. Scott s hangar was second in size only to the naval station hangar in Lakehurst, New Jersey, the largest one in the world at the time.

A couple of highlights of Scott s LTA era (1921-1937) include the 74-mph speed record for dirigibles, set by Scott Field s TC-1 in 1923, and the American free balloon altitude record of 28,510 feet, set in 1927, by Captain Hawthorne C. Gray. Captain Gray would have set a 42,470-feet world record later that same year had he survived that flight.
In the late 1920s, emphasis shifted from airships to balloons. In 1929, the 12th Airship Company was inactivated and replaced the next day by the 1st Balloon Company. Airplanes began to dominate activities at Scott Field by the mid-1930s, and a series of airship mishaps led the Chief of the Army Air Corps to recommend an end to LTA activities in May 1937, and the following month Scott's LTA-era came to an abrupt end.
Scott Field's central location was advantageous when it was considered for the relocation site of the General Headquarters Air Force (GHQAF), which managed the combat arm of the U.S. Army. Scott Field grew from 628 acres in 1938 to 1,882 acres in 1939. Most of the frame World War I and lighter-than-air constructions were torn down only a few, such as the 9th Airstrip Squadron headquarters/barracks building, a brick theater and nine sets of brick non-commissioned officers' quarters at the south end of the field were saved.
New housing, industrial and administrative buildings were completed by May 1939. The expansion program continued into 1940 with the construction of 21 more buildings, including a 200-man barracks, a 300,000-gallon elevated water tank, a 43-bed hospital, Hangar No. 1 and a General Headquarters Air Force office.
Scott Field, as it existed at the end of the 1940 expansion program, is listed as the Scott Field Historic District on the National Register of Historic Places.




With the outbreak of World War II, the GHQAF move to Scott was cancelled. Instead, Scott Field reverted to its former role as a training installation. On 1 June 1939, one of Scott s Balloon Groups was redesignated as a headquarters unit of the Scott Field Branch of the Army Air Corps Technical Schools. Subsequently, various technical schools moved to Scott. Its communications training era began in September 1940 with the opening of the Radio School.
After September 1940, the primary wartime mission of Scott was to train skilled radio operator/maintainers; to produce, as the Radio School s slogan proclaimed, "the best damned radio operators in the world!" Scott s graduates flew in aircraft and operated command and control communications in every Theater of the War, and were often referred to as the  Eyes and Ears of the Army Air Forces.  By the end of World War II, Scott s Radio School becoming something of a  Communications University of the Army Air Forces , where it expanded during the war to fill about 46 large school buildings on base. It was from this course that many specialized radio and communications courses evolved, and had graduated 77,370 radio operator/mechanics. While all had been important to the nation s victory, two of the schools more well known graduates were Medal of Honor recipient Technical Sergeant Forrest Lee Vosler, and the future first Chief Master Sergeant of the Air Force, Paul Airey.
Though the Radio School was the primary World War II-era mission, it was not intended to be Scott s only mission. The Air Corps had also planned for Scott to become a major air terminal due to its centralized location in the United States. In 1940, a $1 million project began to construct four mile-long concrete runways. Though not fully completed until November 1942, the portions that were complete provided a capability to give advanced flying school graduates instruction in instrument and night flying, navigation, photography, and administrative flights. By late 1943, the Radio School students were in the air as well, practicing code transmission under actual flight conditions. Unfortunately, airfield operations had to be sharply curtailed in May 1944, after an accidental tool-spark set fire to Hangar 1 Scott s only hangar. Repairs were not completed until May 1945.



The U.S. Air Force became a separate service on 17 September 1947, and on 13 January 1948, Scott Field was re-designated as Scott Air Force Base and the 3310th Technical Training Wing assumed host responsibility from the 3505th Army Air Forces Base Unit. In 1948, Scott's Radio School was re-organized, with the radio operator and control tower courses relocated to Keesler AFB, Mississippi. In addition, the fixed wire courses at Scott went to Francis E. Warren AFB, Wyoming, so that Scott had room to expand its radio mechanic school.
In early 1949, Secretary of Defense Louis Johnson initiated a series of economic measures throughout the armed forces. His purpose was to effect greater utilization of the assets assigned to all services. As a result of these actions, a number of bases transferred between major commands, schools moved, and other bases closed. Because of these DOD-directed initiatives, Headquarters USAF decided to move Air Training Command (ATC) headquarters from Barksdale AFB, Louisiana to Scott AFB, effective 17 October 1949. The new ATC headquarters established in Building P-3, which was originally designed to be Headquarters, General Headquarters Air Force (GHQ Air Force). Air Training Command moved its headquarters into the building on 17 October 1949. At Scott, ATC absorbed the functions of its previous three subordinate headquarters Flying, Technical, and Indoctrination Divisions.
Throughout the USAF transition, Scott's primary mission remained technical training; however Scott's aeromedical evacuation mission continued to grow. By the end of 1950, Douglas C-54 Skymasters were bringing 200 patients a week to Scott.

In 1957, Military Air Transport Service (MATS) moved to Scott AFB to help facilitate management of its east and west coast units, and between July and October 1957, ATC transferred its headquarters to Randolph AFB, Texas. During the changeover some technical training continued, however, by February 1959, the few remaining technical courses left Scott for other bases. As part of the air mobility transition, ATC's 3310th Technical Training Wing was re-designated on 1 October 1957, as the 1405th Air Base Wing, a MATS organization. With the transition complete, Scott s new primary mission became air mobility.
With the re-alignment to MATS, Aeromedical evacuation continued to grow and in 1964, Scott's host wing was re-designated as the 1405th Aeromedical Transport Wing. Increasing importance placed on airlift and the decision by the Navy to set up its own transport command led to the Military Air Transport Service being re-designated as Military Airlift Command (MAC) in 1966. Associated with this reorganization, the 1405th was discontinued and its mission and resources were absorbed by the newly activated AFCON 375th Aeromedical Airlift Wing. The addition of a fleet of C-9A Nightingales in 1968 further expanded the 375th's aeromedical mission. In 1973, Scott's Patient Airlift Center coordinated 61 aeromedical missions to bring 357 former Vietnam War Prisoners of War back to the United States. In June 1973, the 1400th Air Base Wing inactivated, the 375th Air Base Group (today the 375th Mission Support Group) reactivated, and host wing responsibilities reverted to the 375th Aeromedical Airlift Wing. By 1975, the 375th gained responsibility for the worldwide aeromedical evacuation system.
The 375th gained another mission in 1978; Operational Support Airlift. Scott received its first T-39A Sabreliner in 1962. After 1978, the 375th was managing a dispersed continental fleet of 104 Sabreliners flying a combined 92,000 hours a year. The CT-39As were phased out in 1984, the same year the first Gates C-21A Learjets arrived at Scott.

As the 375th reorganized, it transitioned to a Military Airlift Wing in 1990 and an Airlift Wing in 1991. In 1992, Military Airlift Command inactivated and its personnel and assets were combined with others to form Air Mobility Command (AMC). Later in the 1990s, two new partners joined Scott's team, MidAmerica Airport and the Illinois Air National Guard 126th Air Refueling Wing. A 1987 Federal Aviation Administration authorization, followed by a 1991 joint use agreement resulted in the 1998 opening of the new MidAmerica Airport. Similarly, the 1992 realignment of refueling units to AMC, plus the planned MidAmerica construction, led to a 1995 Base Realignment and Closure committee recommendation to relocate the 126th Air Refueling Wing to Scott AFB.
AMC's 15th and 21st Air Forces became Expeditionary Mobility Task Forces in 2003. They, along with all AMC wings and independent groups realigned to a newly activated 18th Air Force. The new ready mobility operations capability would speed AMC's support for contingencies and humanitarian missions. In 2003, age, noise, maintenance and lack of demand led to the retirement of the C-9A Nightingale. In the years that followed, the C-21A fleet was reorganized and reduced. These events caused a flying mission restructuring that today has Scott using a diverse mix of assigned and non-assigned aircraft to support aeromedical airlift, operational support airlift and air refueling missions.
The 375th Airlift Wing officially became the 375th Air Mobility Wing on 30 September 2009. The Total Force Integration effort called for the creation of an associate unit consisting of active duty KC-135 pilots, boom operators, and maintainers who worked side by side with their counterparts in the 126th Air Refueling Wing. The 375th Operation Group staff had administrative responsibilities for 135 aircrew members and maintainers under the 906th Air Refueling Squadron, a unit that moved from Grand Forks AFB, North Dakota. The 126th Air Refueling Wing maintained the operational direction and control of the mission execution responsibility of these Airmen. Scott AFB served as one of six locations in Air Mobility Command and one of 10 throughout the Air Force where TFI efforts unfolded.
On June 25, 2014, the base was awarded two new cybersecurity squadrons that will add over 300 personnel, and an additional $16 million infrastructure improvements. The base presently has three cyber units.



Signal Corps, U.S. Army, 20 July 1917
Bureau of Military Aeronautics, 20 May 1918
United States Army Air Service, 24 May 1918
United States Army Air Corps, 2 July 1926
General Headquarters Air Force, 1 March
Air Corps Technical Service (exempted station), 1 July 1939
Air Corps Technical Training Command, 26 March 1941
AAF Training Command, 31 July 1943
Re-designated: Air Training Command, 1 July 1946
Military Air Transport Service, 1 October 1957
Military Airlift Command, 1 January 1966
Air Mobility Command, 1 June 1992   Present







The residential part of the base is a census-designated place; the population was 3,612 at the 2010 census.
According to the United States Census Bureau, the base has a total area of 9.7 km2 (3.7 sq mi), all land.



As of the census of 2000, there were 2,707 people, 682 households, and 662 families residing on the base. The population density was 721 inhabitants per square mile (278/km2). There were 715 housing units at an average density of 190.4 inhabitants per square mile (73.5/km2). The racial makeup of the base was 78.9% White, 13.5% Black or African American, 0.3% Native American, 3.0% Asian, 0.1% Pacific Islander, 1.8% from other races, and 2.4% from two or more races. Hispanic or Latino of any race were 4.1% of the population.
There were 682 households out of which 78.0% had children under the age of 18 living with them, 90.5% were married couples living together, 4.3% had a female householder with no husband present, and 2.9% were non-families. Of all households, 2.8% were made up of individuals, and none had someone living alone who was 65 years of age or older. The average household size was 3.83, and the average family size was 3.90.
On the base the population was spread out with 44.7% under the age of 18, 7.8% ages 18 to 24, 40.6% ages 25 to 44, 6.6% ages 45 to 64, and 0.3% were 65 years of age or older. The median age was 22 years. For every 100 females there were 100.2 males. For every 100 females age 18 and over, there were 99.2 males.
The median income for a household on the base was $51,290, and the median income for a family was $52,258. Males had a median income of $39,289 versus $24,674 for females. The per capita income for the base was $15,421. About 0.9% of families and 1.5% of the population were below the poverty line, including 1.9% of those under age 18 and none of those age 65 or over.







Shiloh Scott (St. Louis MetroLink) rail station links Scott Air Force Base with direct trains to downtown St. Louis on MetroLink's Red Line. One-ride and all-day tickets can be purchased from vending machines on the platforms. MetroLink lines provide direct or indirect service to St. Louis, the Clayton area, and Illinois suburbs in St. Clair County.



Five MetroBus lines serve Scott Air Force Base via Shiloh Scott (St. Louis MetroLink) station.
12 O'Fallon Fairview Heights
15 Belleville Shiloh Scott
17X Lebanon   Mascoutah Express
21 Main & East Base Shuttles
512 Metrolink Station Shuttle


