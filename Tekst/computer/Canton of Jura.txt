The Republic and Canton of the Jura (French: R publique et Canton du Jura), also known as the Canton of Jura or Canton Jura, is one of the cantons of Switzerland. It is the newest (created in 1979) of the 26 Swiss cantons, located in the northwestern part of Switzerland. The capital is Del mont. It shares borders with the Canton of Basel-Landschaft, the Canton of Bern, the Canton Neuch tel, the Canton of Solothurn and the French d partement of Doubs.




The King of Burgundy donated much of the land that today makes up Canton Jura to the Bishop of Basel in 999. The area was a sovereign state within the Holy Roman Empire for more than 800 years. After the Treaty of Westphalia in 1648 the Jura had close ties with the Swiss Confederation. At the Congress of Vienna (1815), the Jura region became part of the canton of Bern. This act caused dissention. The Jura was French-speaking and Roman Catholic, whereas the canton of Bern was mostly German-speaking and Protestant.
After World War II, a separatist movement campaigned for a secession of Jura from the Canton of Bern. After a long and partly militant struggle, which included some arson attacks by a youth organisation Les B liers, a constitution was accepted in 1977. In 1978 the split was made official when the Swiss people voted in favour, and in 1979 the Jura joined the Swiss Confederation as a full member. The canton celebrated its independence from the Canton of Bern on 23 June. However, the southern part of the region, which is also predominantly French-speaking but has a Protestant majority, opted not to join the newly formed canton, instead remaining part of the Canton of Bern. Although this decision may be considered strange linguistically, the choice may have been influenced by the fact that the Canton of Bern is financially richer and is at the heart of federal power in Switzerland. The area is now known as Bernese Jura. The word Jura, therefore, may refer either to Canton Jura, or to the combined territory of Canton Jura and the Bernese Jura. Switzerland as a whole often presents the latter from a touristic standpoint with documentation easily available in French or German.

On creation, the canton adopted the title Republic and Canton of the Jura. Other cantons in Switzerland using the title "Republic and Canton" are Ticino, Canton Geneva, and Canton Neuch tel. In each case, the title refers to the autonomy of the canton and its nominal sovereignty within the Swiss Confederation.
Since 1994, the question of the Jura region has again been controversial. In 2004, a federal commission proposed that the French-speaking southern Jura be reunited with the Canton of Jura, as the language question now seems to be more important than the denominational one. A possible solution would be to create two Half-Cantons, as reunification with the creation of only a single Canton would mean a complete restructuring of the Jura's current political system with the Cantonal capital being transferred from Del mont to Moutier.



Canton Jura lies in the northwest of Switzerland. It consists of parts of the Jura mountains in the south and the Jura plateau in the north. The Jura plateau is hilly and almost entirely limestone. The districts of Ajoie and Franches-Montagnes lie in this region. The term "Jurassic" is derived from the Jura Alps, strata of which date to that era.
To the north and the west of the Canton lies France. The canton of Solothurn and Basel-Landschaft are to east of the canton, while the canton of Bern bounds the Jura to the south. The River Doubs and the river Birs drain the lands. The Doubs joins the Sa ne and then the Rh ne, whereas the Birs is a tributary to the Rhine.







Jura is divided into 3 districts:
Del mont (Franc-Comtois: D'l mont, German: Delsberg) - capital: Del mont
Porrentruy (Franc-Comtois: Po rreintru, German: Pruntrut) - capital: Porrentruy
Franches-Montagnes (Franc-Comtois: Fraintches-Montaignes, German: Freiberge) - capital: Saignel gier




There are 64 municipalities in the canton (As of 2009).



The population is almost entirely French-speaking. Just one municipality is German-speaking: Ederswiler. The majority of the population is Roman Catholic (75% as of 2000) with a small Protestant minority (13%). The population of the canton (as of 31 December 2014) is 72,410. As of 2007, the population included 8,195 foreigners, or about 11.8% of the total population.



The historical population is given in the following chart: 



Agriculture is important in Canton Jura. Cattle breeding is significant, but there is also horse breeding (the Franches-Montagnes is the last Swiss horse race). The main industries are watches, textiles and tobacco. There is a growing number of small and middle-sized businesses. In 2001, there were 3,578 people who worked in the primary economic sector. 14,109 people were employed in the secondary sector and 16,513 people were employed in the tertiary sector.
In 2001, the Canton produced 0.9% of the entire Swiss national income while it had 0.9% of the total population. In 2005, the average share of the national income per resident of the canton was 38,070 CHF, while the national average was 54,031 CHF, or about 70% of the national income per person. Between 2003 and 2005, the average income grew at a rate of 6.4%, which was larger than the national rate of 5.3%. The average taxes in the canton are higher than in most cantons, in 2006, the tax index in the canton was 126.6 (Swiss average is 100.0). In 2006, the canton had the highest final tax rate on high wage earners (15.26% on a married couple with two children earning 150,000 CHF vs 11.6% nationally), though the tax rate was in the middle for lower income families.



The eau de vie Damassine is one typical produce of the Ajoie area. (see [1] Terroir Jura and [2] Jura Infos)


