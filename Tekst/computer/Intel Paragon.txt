The Intel Paragon was a series of massively parallel supercomputers produced by Intel. The Paragon XP/S was a productized version of the experimental Touchstone Delta system built at Caltech, launched in 1992. The Paragon superseded Intel's earlier iPSC/860 system, to which it was closely related.

The Paragon series was based on the Intel i860 RISC microprocessor. Up to 2048 (later, up to 4000) i860s were connected in a 2D grid. In 1993, an entry-level Paragon XP/E variant was announced with up to 32 compute nodes. The system architecture was a partitioned system, with the majority of the system comprising diskless compute nodes and a small number of I/O nodes interactive service nodes. Since the bulk of the nodes had no permanent storage, it was possible to "Red/Black switch" the compute partition from classified to unclassified by disconnecting one set of I/O nodes with classified disks and then connecting an unclassified I/O partition.
Intel intended the Paragon to run the OSF/1 AD distributed operating system on all processors. However, this was found to be inefficient in practice, and a light-weight kernel called SUNMOS was developed at Sandia National Laboratories to replace OSF/1 AD on the Paragon's compute processors.
The prototype for the Intel Paragon was the Intel Delta, built by Intel with funding from DARPA and installed operationally at the California Institute of Technology in the late 1980s with funding from the National Science Foundation. The Delta was one of the few computers to sit significantly above the curve of Moore's Law.




The compute boards came in two variants, the GP16 with 16 MB of memory and two CPUs, and the MP16 with three CPUs. Each node had a B-NIC interface that connected to the mesh routers on the backplane. The compute nodes were diskless and performed all I/O over the mesh. During system software development a light-pen was duct-taped to the status LED on one board and a timer interrupt was used to bit bang a serial port.
The B-NIC ASIC is the square chip with the circular heat-sink.




The IO boards had either SCSI drive interfaces or HiPPI network connections and were used to provide data to the compute nodes. They did not run any user applications. The MP64 I/O node had three i860 CPUs and an i960 CPU used in the disk controller.



Article about the paragon at Caltech