The Segunda Divisi n [se unda  i i sjon] (Second Division) of the Liga Nacional de F tbol Profesional (LFP), or the Liga Adelante [li a a e lante] for sponsorship reasons, is the second-highest division overall in the Spanish football league system after the Liga BBVA.



This championship was created in 1929 by the Royal Spanish Football Federation. Since 1983 it has been organized by the LFP.
On 16 August 2006, the LFP reached an agreement with the banking group BBVA for sponsorship of the Segunda Divisi n renaming the league Liga BBVA.
On 4 June 2008, the league was renamed Liga Adelante, after the BBVA slogan, "Adelante." ("Go ahead" in Spanish), because of the BBVA sponsorship extending to La Liga, which carried the Liga BBVA name.
Since the 2010 11 season, a play-off has been played between teams in positions 3 through 6, to determine the team that promotes to La Liga with the top two in the league table.



As of 2010-11 the league contains 22 teams that play each other home and away for a 42 match season. Each year three teams are promoted to La Liga. The top two teams earn an automatic promotion. The third team to be promoted is the winner of a play-off between the next four best qualified teams in positions 3 through 6 (reserve teams are not eligible for promotion). The play-offs comprise two-legged semi-finals followed by a two-legged final. The bottom four are relegated to Segunda Divisi n B.




Notes
Note 1: Llagostera will play their home matches at Estadi Palam s Costa Brava, Palam s as their own Estadi Municipal did not meet LFP criteria. Initially was planned to play at Estadi Montilivi, Girona, but Girona FC finally rejected to share its stadium.






The All-Time Segunda Table is an overall record of all match results, points, and goals of every team that has played in La Segunda since its inception in 1929. The table that follows is accurate as of the end of the 2013-14 season.
League or status at 2015 16:






Italics: shared titles*Championships won by M laga CF and CD M laga
*Championships won by Burgos CF (I) and Real Burgos CF



Canal+ Liga 2 (Movistar+). Exclusive all Liga Adelante (Segunda Divisi n) matches.


