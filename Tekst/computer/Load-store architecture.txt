In computer engineering a load/store architecture only allows memory to be accessed by load and store operations, and all values for an operation need to be loaded from memory and be present in registers. Following the operation, the result needs to be stored back to memory.
For instance, in a load/store approach both operands for an ADD operation must be in registers. This differs from a register memory architecture in which one of the operands for the ADD operation may be in memory, while the other is in a register.
The earliest example of a load/store architecture was the CDC 6600. Almost all vector processors use the load/store approach.
RISC systems such as PowerPC, SPARC, ARM or MIPS use the load/store architecture.



Load-store unit (computing)
Register memory architecture


