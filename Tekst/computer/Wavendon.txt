Wavendon is a village and civil parish in the south east of the Borough of Milton Keynes and ceremonial county of Buckinghamshire, England.



The village name is an Old English language word, and means 'Wafa's hill'. In the Anglo-Saxon Chronicle in 969 the village was recorded as Wafandun. The ancient village lies just outside the south-east of Milton Keynes itself. The ecclesiastic parish of Wavendon anciently contained the hamlet of Woburn Sands, which became a separate civil parish in 1907.
The village is best known today for being the location of the Stables Theatre live music venue, and the "Wavendon All-music Plan" music summer schools. The venue is the brainchild of the late Sir John Dankworth and his wife, Dame Cleo Laine (who continues to live in the village).




Wavendon Tower is a large country house with substantial modern additions on the edge of the village. During the Second World War it was used as a recording studio for black propaganda. From 1969 to the late 1970s, it was the base for the Milton Keynes Development Corporation. Until 2011 it was an operating centre for Scicon (subsequently EDS). As of January 2012 it is vacant and seeking tenants.



In the expansion plans for Milton Keynes, it was proposed that Wavendon would become a part of the city and a neighbourhood centre, in a similar way to the other towns and villages that provided the roots of early Milton Keynes districts. However, following the United Kingdom general election 2010, the new Conservative/Liberal Democrat coalition government cancelled the expansion plans of the outgoing Labour government. Consequently it is not clear whether or when (if at all) the expansion plan will be realised.
Nevertheless, Milton Keynes Borough Council has proposed plans to add developments to the west and north-east of the village.



Magna Park is a large distribution site on the part of Wavendon civil parish east of the A421 (and in the 'Eastern Expansion Area', one element of the expansion plans for Milton Keynes that has gone ahead. As of 2012; its occupants include John Lewis and River Island, both of which have very large distribution centres. The site is at the south-east edge of Milton Keynes, about 3 miles (4.8 km) from M1 Junction 13 or (via the A5130) about 2 miles (3.2 km) from Junction 14. Access is from the A421.



Jazz artists John Dankworth and Cleo Laine shared a home in Wavendon from the late 1960s. They founded The Stables Theatre, Wavendon in 1970 in what was the old stables block in the grounds of their home. It was an immediate success with forty seven concerts given in the first year. The venue was completely rebuilt in 2000, with a subsequent development in 2007 to create Stage 2. The venue now presents over 350 concerts and around 250 education events in its two auditoria; the 400 seat Jim Marshall Auditorium, and smaller studio space at Stage 2.



The adjacent Wavendon Gate district is in Milton Keynes, in the parish of Walton.
Wavendon is twinned with Presles-en-Brie, France






 Media related to Wavendon at Wikimedia Commons