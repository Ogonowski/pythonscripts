John Charles Dvorak (born April 5, 1952) is an American columnist and broadcaster in the areas of technology and computing. His writing extends back to the 1980s, when he was a regular columnist in a variety of magazines. Dvorak is also the vice president of Mevio (formerly PodShow) and has been a host on TechTV and TWiT.tv. He is currently a co-host of the No Agenda podcast.



John Charles Dvorak was born in 1952 in Los Angeles, California. The nephew of sociologist and creator of the Dvorak keyboard, August Dvorak, he graduated from the University of California, Berkeley with a degree in history, with a minor in chemistry.






Dvorak started his career as a wine writer.
He has written for various publications, including InfoWorld, PC Magazine (two separate columns since 1986), MarketWatch, BUG Magazine (Croatia), and Info Exame (Brazil). Dvorak has been a columnist for Boardwatch, Forbes, Forbes.com, MacUser, MicroTimes, PC/Computing, Barron's Magazine, Smart Business, and The Vancouver Sun. (The MicroTimes column ran under the banner Dvorak's Last Column.) He has written for the New York Times, Los Angeles Times, MacMania Networks, International Herald Tribune, San Francisco Examiner and The Philadelphia Inquirer among numerous other publications.
On episode 524 of the No Agenda Podcast Dvorak mentioned that MarketWatch had "gotten rid of him" after Adam Curry made a suggestion for his next column. Dvorak did not give any further details.
Dvorak created a few tech running jokes; in episode 18 of TWiT (This Week in Tech) he claimed that, thanks to his hosting provider, he "gets no spam."



Dvorak has written or co-authored over a dozen books, including Hypergrowth: The Rise and Fall of the Osborne Computer Corporation with Adam Osborne and Dvorak's Guide to Desktop Telecommunications in 1990, Dvorak's Guide to PC Telecommunications (Osborne McGraw-Hill, Berkeley, California, 1992), Dvorak's Guide to OS/2 (Random House, New York, 1993) with co-authors Dave Whittle and Martin McElroy, Dvorak Predicts (Osborne McGraw-Hill, Berkeley, California, 1994), Online! The Book (Prentice Hall PTR, October, 2003) with co-authors Wendy Taylor and Chris Pirillo and his latest e-book is Inside Track 2013.



The Computer Press Association presented Dvorak with the Best Columnist and Best Column awards, and he was also the 2004 and 2005 award winner of the American Business Editors Association's national gold award for best online columns of 2003 and 2004, respectively.
He was the creator and lead judge of the Dvorak Awards (1992 1997).
In 2001, he was awarded the Telluride Tech Festival Award of Technology.
He has been bestowed the title of Kentucky Colonel, the highest title of honor awarded by the Commonwealth of Kentucky.



Dvorak was on the start-up team for CNET Networks, appearing on the television show CNET Central. He also hosted a radio show called Real Computing and later 'Technically Speaking' on NPR, as well as a television show on TechTV (formerly ZDTV) called Silicon Spin.
He now appears on Marketwatch TV and This Week in Tech, a podcast audio and now video program hosted by Leo Laporte and featuring other former TechTV personalities such as Patrick Norton, Kevin Rose, and Robert Heron. Dvorak was once banned from the show. As of December 2005, that "TWiTcast" regularly ranks among the top 5 at Apple's iTunes Music Store. Dvorak also participated in the only Triangulation podcast, a similar co-hosted technology discussion program. In March 2006, Dvorak started a new show called CrankyGeeks in which he led a rotating panel of "cranky" tech gurus in discussions of technology news stories of the week. The last episode (No. 237) aired on September 22, 2010.
Mevio hired Dvorak as Vice President & Managing Editor for a new Mevio TECH channel in 2007. He manages content from existing Mevio tech programming. He also hosted the show, "Tech5", where Dvorak discussed the day's tech news in approximately 5 minutes. The show has been out of production since late 2010. Dvorak also co-hosts a podcast with Mevio co-founder Adam Curry called No Agenda. The show is a conversation about the week's news, happenings in the lives of the hosts and their families, and restaurant reviews from the dinners Dvorak and Curry have together when they are in the same city (usually San Francisco). Curry usually has more outlandish opinions of the week's news or world events while Dvorak is intended to play the straight man in the dialogue.
Since early 2011, Dvorak has been one of the featured "CoolHotNot Tech Xperts," along with Chris Pirillo, Jim Louderback, Dave Graveline, Robin Raskin, Dave Whittle, Steve Bass, and Cheryl Currid. At CoolHotNot's web site, Dvorak shares his "Loved List" of favorite consumer electronics, his "Wanted List" of tech products he'd like to try, and his "Letdown List" of tech products he found disappointing.
Dvorak hosted the show X3 which, like the defunct Tech 5, was a short tech-focused cast. Unlike Tech 5, it was in a video format, together with two additional hosts. The last update was 24 June 2012.
Since September 2009, Dvorak has hosted the DH Unplugged podcast with personal money manager Andrew Horowitz.
He is a co-founder (with Gina Smith and Jerry Pournelle) of the web site aNewDomain.net, where he also serves as a columnist.



On February 19, 1984 Dvorak wrote in the San Francisco Examiner that the mouse was a reason Apple Inc.'s Macintosh computer would fail: "The Macintosh uses an experimental pointing device called a  mouse . There is no evidence that people want to use these things." In 1987 he revisited the article and recanted, writing "The Mac mouse is great. I've been converted."
In his 2007 article for MarketWatch regarding the iPhone, Dvorak wrote "If [Apple's] smart, it will call the iPhone a 'reference design' and pass it to some suckers to build with someone else's marketing budget. Then it can wash its hands of any marketplace failures. [... ] It should do that immediately before it's too late."
Although he later admitted having been wrong about its success, he criticized Apple's iPad when it first appeared in 2010, stating that it was no different from other previous tablets that had failed: "I cannot see it escaping the tablet computer dead zone any time soon."
Dvorak has mentioned in the past that he is a fan of MorphOS and used the Video Toaster in its heyday.



Dvorak married Mimi Smith-Dvorak on August 8, 1988.
He is listed as a minister of the Universal Life Church.
Dvorak said on show 600 of No Agenda that he occasionally posts online under the pseudonym Mark Pugner.


