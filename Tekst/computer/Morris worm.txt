The Morris worm or Internet worm of November 2, 1988 was one of the first computer worms distributed via the Internet. It was the first to gain significant mainstream media attention. It also resulted in the first conviction in the US under the 1986 Computer Fraud and Abuse Act. It was written by a graduate student at Cornell University, Robert Tappan Morris, and launched on November 2, 1988 from MIT.




According to its creator, the Morris worm was not written to cause damage, but to gauge the size of the Internet. The worm was released from MIT in the hope of suggesting that its creator studied there, which Morris did not (n.b. Morris is currently a tenured professor at MIT). It worked by exploiting known vulnerabilities in Unix sendmail, finger, and rsh/rexec, as well as weak passwords. Due to reliance on rsh (normally disabled on untrusted networks), fixes to sendmail, finger, the widespread use of fail2ban and similar software, and improved awareness of the dangers of weak passwords, it should not succeed on a recent, properly configured system.
A supposedly unintended consequence of the code, however, caused it to be more damaging: a computer could be infected multiple times and each additional process would slow the machine down, eventually to the point of being unusable. This would have the same effect as a fork bomb and crash the computer. The main body of the worm could only infect DEC VAX machines running 4BSD, and Sun-3 systems. A portable C "grappling hook" component of the worm was used to pull over (download) the main body, and the grappling hook could run on other systems, loading them down and making them peripheral victims.



The critical error that transformed the worm from a potentially harmless intellectual exercise into a virulent denial of service attack was in the spreading mechanism. The worm could have determined whether to invade a new computer by asking whether there was already a copy running. But just doing this would have made it trivially easy to kill: everyone could just run a process that would answer "yes" when asked whether there was already a copy, and the worm would stay away. The defense against this was inspired by Michael Rabin's mantra "Randomization". To compensate for this possibility, Morris directed the worm to copy itself even if the response is "yes" 1 out of 7 times. This level of replication proved excessive, and the worm spread rapidly, infecting some computers multiple times. Rabin said that Morris "should have tried it on a simulator first."



The U.S. Government Accountability Office put the cost of the damage at $100,000 10,000,000. Clifford Stoll, who helped fight the worm, wrote in 1989 "I surveyed the network, and found that two thousand computers were infected within fifteen hours. These machines were dead in the water useless until disinfected. And removing the virus often took two days." It is usually reported that around 6,000 major UNIX machines were infected by the Morris worm; however, Morris's colleague Paul Graham claimed, "I was there when this statistic was cooked up, and this was the recipe: someone guessed that there were about 60,000 computers attached to the Internet, and that the worm might have infected ten percent of them." (Stoll wrote "Rumors have it that [Morris] worked with a friend or two at Harvard's computing department (Harvard student Paul Graham sent him mail asking for 'Any news on the brilliant project')".)
The Internet was partitioned for several days, as regional networks disconnected from the NSFNet backbone and from each other to prevent recontamination as they cleaned their own networks.
The Morris worm prompted DARPA to fund the establishment of the CERT/CC at Carnegie Mellon University to give experts a central point for coordinating responses to network emergencies. Gene Spafford also created the Phage mailing list to coordinate a response to the emergency.
Robert Morris was tried and convicted of violating United States Code: Title 18 (18 U.S.C.   1030), the Computer Fraud and Abuse Act in United States v Morris. After appeals he was sentenced to three years probation, 400 hours of community service, a fine of $10,050 plus the costs of his supervision.
The Morris worm has sometimes been referred to as the "Great Worm", because of the devastating effect it had on the Internet at that time, both in overall system downtime and in psychological impact on the perception of security and reliability of the Internet. The name was derived from the "Great Worms" of Tolkien: Scatha and Glaurung.



In the visual novel Digital: A Love Story, the Morris worm is portrayed as a cover story for a large-scale attack on ARPANET and several bulletin board systems.
In the epilogue of his book The Cuckoo's Egg, Stoll details his efforts battling the Morris Worm.


