EWS-UX is a Unix operating system used by NEC Corporation for its EWS-4800 line of engineering workstations. EWS-UX is based largely on versions of Unix System V supplemented with BSD software. It was widely used from the late 1980s to around 2000.



EWS-UX and the EWS-4800 line of workstations were widely used for CAD / CAM work.
Early versions of EWS-UX run on Motorola 68000 series CISC processors, while later versions run on MIPS RISC processors.
NEC attempted to introduce binary compatibility between Unix versions used by DEC, Sony (NEWS-OS), and Sumitomo's Unix (SEIUX). However, DEC dropped out of the agreement to pursue the DEC Alpha architecture.
EWS-UX and UP-UX (NEC's Unix server OS) became integrated and merged into UX/4800.



EWS-UX / V: Based on Unix SVR2. It runs on the EWS4800 series equipped with the MC68020, MC68030, and MC68040 processors.
EWS-UX / V (Rel4.0): Based on Unix SVR4. It runs on the EWS4800 series equipped with R3000 (VR3600) and R4000 processors.
EWS-UX / V (Rel4.2): Based on Unix SVR4.2. It supports processors such as the R4400, VR4200, and R4600.
EWS-UX / V (Rel4.2MP): Based on Unix SVR4.2MP. It supports multi-processor systems using the R4400MC. It is mostly similar to UP-UX, NEC's Unix operating system for servers.



SUPER-UX



"In the Times of Open System" - UNIX workstation which changes business (1/8) on YouTube
"In the Times of Open System" - UNIX workstation which changes business (2/8) on YouTube
"In the Times of Open System" - UNIX workstation which changes business (3/8) on YouTube
"In the Times of Open System" - UNIX workstation which changes business (4/8) on YouTube
"In the Times of Open System" - UNIX workstation which changes business (5/8) on YouTube
"In the Times of Open System" - UNIX workstation which changes business (6/8) on YouTube
"In the Times of Open System" - UNIX workstation which changes business (7/8) on YouTube
"In the Times of Open System" - UNIX workstation which changes business (8/8) on YouTube