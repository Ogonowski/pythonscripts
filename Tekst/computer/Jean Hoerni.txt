Jean Am d e Hoerni (September 26, 1924 - January 12, 1997) was a silicon transistor pioneer and a member of the 'traitorous eight'. He developed the ground breaking planar process for reliably manufacturing semiconductor devices such as transistors.



Hoerni was born on September 26, 1924 in Geneva, Switzerland. He received a Doctor of Philosophy (Ph.D.) from the University of Cambridge and another Ph.D. from the University of Geneva.
In 1952, he moved to the United States to work at the California Institute of Technology, where he became acquainted with William Shockley, the "father of the transistor."
A few years later, Shockley recruited Hoerni to work with him at the newly founded Shockley Semiconductor Laboratory division of Beckman Instruments in Mountain View, California. But Shockley's strange behavior compelled the so-called 'traitorous eight': Hoerni, Julius Blank, Victor Grinich, Eugene Kleiner, Jay Last, Gordon Moore, Robert Noyce and Sheldon Roberts, to leave his laboratory and create the Fairchild Semiconductor corporation. At Fairchild Hoerni would go on to invent the planar process, which was critical in the invention of Silicon Integrated circuit by Robert Noyce. Jack Kilby from Texas Instruments is usually credited, with Noyce, with the invention of the integrated circuit, but Kilby's IC was based on Germanium, and as it turns out, Silicon ICs have numerous advantages over germanium. The name "Silicon Valley" refers to this silicon.
Along with the 'traitorous eight' alumni Jay Last and Sheldon Roberts, Hoerni founded Amelco (known now as Teledyne) in 1961.
In 1964, he founded Union Carbide Electronics, and in 1967 Intersil.
He was awarded the Edward Longstreth Medal from the Franklin Institute in 1969 and the McDowell Award in 1972.
Hoerni died of myelofibrosis on January 12, 1997 in Seattle, Washington. He was 72.



He was married to Anne Marie Hoerni, and had three children: daughters Annie Blackwell, Susan Killham, son Michael, and five grandchildren. He had one brother, Marc Hoerni of Geneva.



An avid mountain climber, Hoerni often visited the Karakoram Mountains in Pakistan and was moved by the poverty of the Balti mountain people who lived there. He contributed the lion's share, $30,000, to Greg Mortenson's project to build a school in the remote village of Korphe, and later founded the Central Asia Institute with an endowment of $1 million to continue providing services for them after his death. Hoerni named Greg Mortenson as the first Executive Director of the organization, which continues to build schools in Pakistan and Afghanistan.
In December 2007, an article was published by Michael Riordan on Hoerni and his planar process in IEEE Spectrum. The author claimed that Jay Last pointed out that Hoerni had incredible stamina and could hike for hours on little food or water.


