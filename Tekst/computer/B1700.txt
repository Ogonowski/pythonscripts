The Burroughs B1000 Series was a series of mainframe computers, built by the Burroughs Corporation, and originally introduced in the 1970s with continued software development until 1987. The series consisted of three major generations which were the B1700, B1800, and B1900 series machines. They were also known as the Burroughs Small Systems, by contrast with the Burroughs Large Systems (B5000, B6000, B7000, B8000) and the Burroughs Medium Systems (B2000, B3000, B4000).
Much original research for the B1700, initially codenamed the PLP ("Proper Language Processor" or "Program Language Processor"), was done at the Burroughs Pasadena plant.
Production of the B1700s began in the mid-1970s and occurred at both the Santa Barbara and Liege, Belgium plants. The majority of design work was done at Santa Barbara with the B1830 being the notable exception designed at Liege.







The B1000 is distinguished from other machines in that it had a writeable control store allowing the machine to emulate any other machine. The Burroughs MCP (Master Control Program) would schedule a particular job to run. The MCP would preload the interpreter for whatever language was required. These interpreters presented different virtual machines for COBOL, Fortran, etc.
A notable idea of the "semantic gap" between the ideal expression of the solution to a particular programming problem, and the real physical hardware illustrated the inefficiency of current machine implementations. The three Burroughs architectures represent solving this problem by building hardware aligned with high-level languages, so-called language-directed design (contemporary term; today more often called a "high-level language computer architecture"). The large systems were stack machines and very efficiently executed ALGOL. The medium systems (B2000, 3000, and B4000) were aimed at the business world and executing COBOL (thus everything was done with BCD including addressing memory.) The B1000 series was perhaps the only "universal" solution from this perspective because it used idealized virtual machines for any language.
The actual hardware was built to enhance this capability. Perhaps the most obvious examples were the bit-addressable memory, the variable size arithmetic logic unit (ALU), and the ability to OR in data from a register into the instruction register allowing very efficient instruction parsing. Another feature of the machine language was the appearance of having the output of the ALU appear as different addressable registers. X+Y, and X-Y are two read-only registers within the machine language.



One concession to the fact that Burroughs was primarily a supplier to business (and thus running COBOL) was the availability of BCD arithmetic in the ALU.
Internally the machines employed 16-bit instructions and a 24-bit data path. The bit addressable memory supported the mix quite efficiently. Internally, the later generation memories stored data on 32-bit boundaries, but were capable of reading across this boundary and supplying a merged result.
The initial hardware implementations were built out of the CTL Family originally made by Fairchild Semiconductor but with the introduction of the B1955 in 1979 the series employed the more popular (and more readily obtainable) TTL logic family. Up through the B1955, the control logic was implemented with PROMs, muxes and such.
The B1965, the last of the series, was implemented with a pair of microcode sequencers which stayed in lock step with each other. The majority of the instructions executed in a single cycle. This first cycle was decoded by FPLAs using 16 inputs (just the perfect size for a 16-bit instruction word) and 48 min-terms. Successive cycles from a multi-cycle instruction were sourced from PROMs. The FPLAs and PROM outputs were wired together. The FPLA would drive the output on the first cycle, then get tri-stated. The PROMs would drive the control lines until the completion of the instruction.



The I/O system for the B1000 series consisted of a 24-bit data path and control strobes to and from the peripherals. The CPU would place data on the data path, then inform the peripheral that data was present. Many of the peripheral adapters were fairly simplistic, and the CPU actually drove the adapter state machines through their operations with successive accesses.
Later models of the machines in both the 1800 and 1900 series could be configured as either a single or dual processor. These were tightly coupled machines and competed in access to the main memory. The B1955 and B1965 could accommodate up to four processors on the memory bus, but at least one of these would be assigned to the Multi-Line adapter which supplied serial I/O to the system. Only Dual-processor configurations were ever actually sold.
The Multi-Line was capable of driving multiple 19.2Kb RS485 serial lines in a multi-drop configuration. The serial I/O was polled. A given terminal would wait until it was addressed, and grab the line and send any data it had pending.
The Multi-Line Adapter would DMA the data into main memory in a linked list format. Consequently, the processors didn't have to deal with serial I/O interrupt issues. This was taken care of by the fact that block mode terminals were the only type supported.
The B1000 series could address a maximum of 2 megabytes of memory. In these days of multiple gigabytes that sounds fairly limiting, but most commercial installations got by with hundreds of kilobytes of storage.



^ ETM 313: Proper Language Processor for Small Systems (Bunker, et al.), 1968. [1]



B1700/B1800/B1900 manuals at bitsavers.org
Barton, R. S.,  Ideas for Computer Systems Organization: A Personal Survey , Software Engineering, vol. 1, Academic Press, New York, 1970, pp.7-16.
Wilner, Wayne T., "B1700 Design and Implementation", Burroughs Corporation, Santa Barbara Plant, Goleta, California, May 1972.
Wilner, Wayne T., "Microprogramming environment on the Burroughs B1700", IEEE CompCon '72
Wilner, Wayne T., "Design of the Burroughs B1700", AFIPS (American Federation of Information Processing Societies) Joint Computer Conferences archive, Proceedings of the December 5-7, 1972, Fall Joint Computer Conference, Anaheim, California, 1972, pp.489-497
Wilner, Wayne T., "Burroughs B1700 memory utilization", Proceedings of the December 5-7, 1972, Fall Joint Computer Conference, part I, December 05-07, 1972, Anaheim, California
Wilner, Wayne T., "Unconventional architecture", ACM Annual Conference/Annual Meeting archive, Proceedings of the 1976 annual conference, Houston, Texas, 1976