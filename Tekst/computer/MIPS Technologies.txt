MIPS Technologies, Inc., formerly MIPS Computer Systems, Inc., was a United States-based fabless semiconductor design company that is most widely known for developing the MIPS architecture and a series of RISC CPU chips based on it. MIPS provides processor architectures and cores for digital home, networking and mobile applications.
On 8 February 2013 MIPS Technologies, Inc. was acquired by Imagination Technologies after CEVA, Inc. pulled out of the bidding. Imagination Technologies is a UK-based company best known for their PowerVR graphic chips family.



MIPS Computer Systems Inc. was founded in 1984 by a group of researchers from Stanford University that included John L. Hennessy, one of the pioneers of the RISC concept. Other principal founders were Skip Stritter, formerly a Motorola technologist, and John Moussouris, formerly of IBM.
The initial CEO was Vaemond Crane, who left early and was replaced by Bob Miller, a former senior IBM and Data General executive. Miller ran the company through its IPO and subsequent sale to Silicon Graphics.
In 1988, MIPS Computer Systems designs were noticed by Silicon Graphics (SGI) and the company adopted the MIPS architecture for its computers. A year later, in December 1989, MIPS held its first IPO. That year, Digital Equipment Corporation (DEC) released a Unix workstation based on the MIPS design.
After developing the R2000 and R3000 microprocessors, a management change brought along the larger dreams of being a computer vendor. The company found itself unable to compete in the computer market against much larger companies and was struggling to support the costs of developing both the chips and the systems (MIPS Magnum). To secure the supply of future generations of MIPS microprocessors (the 64-bit R4000), SGI acquired the company in 1992 for $333 million and renamed it as MIPS Technologies Inc., a wholly owned subsidiary of SGI.
During SGI's ownership of MIPS, the company introduced the R8000 in 1994 and the R10000 in 1996 and a follow up the R12000 in 1997. During this time, two future microprocessors code-named The Beast and Capitan were in development; these were cancelled after SGI decided to migrate to the Itanium architecture in 1998. As a result, MIPS was spun out as an intellectual property licensing company, offering licences to the MIPS architecture as well as microprocessor core designs.
On June 30, 1998, MIPS held an IPO after raising about $16.3 million with an offering price of $14 a share. In 1999, SGI announced it would overhaul its operations; it planned to continue introducing new MIPS processors until 2002, but its server business would include Intel s processor architectures as well. SGI spun MIPS out completely on June 20, 2000 by distributing all its interest as stock dividend to the stockholders.
In early 2008 MIPS laid-off 28 employees from its processor business group. On August 13, 2008, MIPS announced a loss of $108.5 million for their fiscal fourth-quarter and that they would lay-off another 15% of their workforce. At the time MIPS had 512 employees.
Some notable people who worked in MIPS: James Billmaier, Steve Blank, Joseph DiNucci, John L. Hennessy, David Hitz, Earl Killian, Dan Levin, John Mashey, John P. McCaskey, Bob Miller, Stratton Sclavos. and Skip Stritter. Board members included: Bill Davidow.
In 2010, Sandeep Vij was named CEO of MIPS Technologies. Vij studied under Dr. John Hennessy as a Stanford University grad student. Prior to taking over at MIPS, Vij was an executive at Cavium Networks, Xilinx and Altera.
EE Times reported that MIPS had 150 employees as of November 1, 2010. If the August 14th, 2008 EDN article was accurate about MIPS having over 500 employees at the time, then MIPS reduced their total workforce by 70% between 2008 and 2010.

In addition to its main R&D centre in Sunnyvale, California, MIPS has engineering facilities in Shanghai, China, Beaverton, Oregon, Bristol and Kings Langley, both in England. It also has offices in Hsin-chu, Taiwan; Tokyo, Japan; Remscheid, Germany and Haifa, Israel.
During the first quarter of 2013, 498 out of 580 of MIPS patents were sold to Bridge Crossing which was created by Allied Security Trust, with all processor-specific patents and the other parts of the company sold to Imagination Technologies Group. Imagination had outbid Ceva Inc to buy MIPS with an offer of $100 million, and is investing to develop the architecture for the embedded procesor market.







MIPS Technologies creates the processor architecture that is licensed to chip makers. The company has 125+ licensees who ship more than 500 million MIPS-based processors each year.
MIPS Technologies  processor architectures and cores are used in home entertainment, networking and communications products. The company licenses its 32- and 64-bit architectures as well as 32-bit cores.
The MIPS32 architecture is a high-performance 32-bit instruction set architecture (ISA) that is used in applications such as 32-bit microcontrollers, home entertainment, home networking devices and mobile designs. MIPS customers license the architecture to develop their own processors or license off-the-shelf cores from MIPS that are based on the architecture.
The MIPS64 architecture is a high performance 64-bit instruction set architecture that is widely used in networking infrastructure equipment through MIPS licensees such as Cavium Networks and NetLogic Microsystems.
SmartCE (Connected Entertainment) is a reference platform that integrates Android, Adobe Flash platform for TV, Skype, the Home Jinni ConnecTV application and other applications. SmartCE lets OEM customers create integrated products more quickly.



The MIPS processor cores are divided by Imagination into three major families: Warrior (current generation), Aptiv, and Classic.  Key features in the Warrior cores include: hardware virtualization, hardware multi-threading, and SIMD. 
Design experience with the MIPS Classic cores led to the Aptiv cores: microAptiv, a compact, real-time embedded processor core; interAptiv, a multiprocessor core with a nine-stage pipeline; and proAptiv, a super-scalar, deeply out-of-order processor core with high CoreMark/MHz score.
The MIPS32 Classic cores include the 4K, M14K, 24K, 34K, 74K, 1004K (multicore and multithreaded) and the 1074K (superscalar and multithreaded) families.



MIPS Technologies has a strong customer licensee base in home electronics and portable media players; 75 percent of Blu-ray Disc players are running on MIPS Technologies processors. In the digital home, the company s processors are predominately found in digital TVs and set-top boxes. The Sony PlayStation Portable uses two processors based on the MIPS32 4K processor.
Within the networking segment, licensees include Cavium Networks and Netlogic Microsystems. Cavium has used up to 16 MIPS cores for its OCTEON family network reference designs. Netlogic ships Linux-ready MIPS64-based XLP, XLR, and XLS multicore, multithreaded processors. Licensees using MIPS to build smartphones and tablets include Actions Semiconductor and Ingenic Semiconductor. Tablets based on MIPS include the Cruz tablets from Velocity Micro. TCL Corporation is using MIPS processors for the development of smartphones.
Companies can also obtain an MIPS 'architectural licence for designing their own CPU cores using the MIPS instruction set. Distinct MIPS architecture implementations by licensees include Broadcom's BRCM 5000.
Other licensees include Broadcom, which has developed MIPS-based CPUs for over a decade, Microchip Technology, which leverages MIPS processors for its 32-bit PIC32 microcontrollers, Qualcomm Atheros, MediaTek and Mobileye, whose EyeQ2 and EyeQ3 are based on cores licensed from MIPS.



MIPS Technologies is predominantly used in conjunction with Android and Linux operating systems. There are also a few NetBSD ports for MIPS.
Google s processor-agnostic Android operating system is built on the Linux kernel. MIPS originally ported Android to its architecture for embedded products beyond the mobile handset, where it was originally targeted by Google. In 2010, MIPS and its licensee Sigma Designs announced the world s first Android set-top boxes. By porting to Android, MIPS processors power smartphones and tablets running on the Android operating system.
OpenWrt is an embedded operating system based on the Linux kernel. While it currently runs on a variety of processor architectures, it was originally developed for the Linksys WRT54G, which used a 32-bit MIPS processor from Broadcom. The OpenWrt Table of Hardware now includes MIPS-based devices from Atheros, Broadcom, Cavium, Lantiq, MediaTek, etc.
Real-time operating systems that run on MIPS include CMX System, eCosCentric, ENEA, Express Logic, FreeRTOS, Green Hills Software, LynuxWorks, Mentor Graphics, Micrium, QNX Software Systems, Quadros Systems Inc., Segger and Wind River.



^ John Gantz (October 14, 1991), MIPS will have a tough time in a crowded market, InfoWorld, p. 137.
^ Computer History Museum.  John Hennessy: 2007 Fellow Awards Recipient.  2007. Retrieved September 16, 2011.
^ Agam Shah, IDG. "MIPS Porting Google s Android 3.0 OS for Its Processors." April 26, 2011. Retrieved September 16, 2011.
^ Sam Dean, Ostatic. "MIPS Advances its Android Plans   Outside of Phones." August 3, 2009. Retrieved September 16, 2011.
^ "Acquisition of MIPS Technologies completed". 2013-02-08. Retrieved 2013-09-15. 
^ CrunchBase. "MIPS Computer Systems." Retrieved September 16, 2011.
^ Junko Yoshida, EE Times. "New CEO Sandeep Vij forms  Team MIPS ." February 7, 2010. Retrieved September 16, 2011.
^ James DeTar, Investors Business Daily. "Panel: Information Technology Still Early Stage." October 6, 2010. Retrieved September 16, 2011.
^ a b Om Malik, Forbes. "Can MIPS beat ARM?." December 2, 1998. Retrieved September 19, 2011.
^ PC Magazine. "SGI." Retrieved September 19, 2011.
^ a b Computer History Museum. "Silicon Graphics Professional IRIS 4D/50GT." Retrieved September 19, 2011.
^ Cate Corcoran (March 16, 1992), MIPS, Silicon merger could kill ACE/ARC, InfoWorld, pp. 1 and 107. Retrieved September 19, 2011.
^ Cate Corcoran (March 16, 1992), MIPS, Silicon merger could kill ACE/ARC, InfoWorld, p. 107. Retrieved Sep 19, 2011.
^ Linley Gwenapp, Microprocessor Report. "MIPS R10000 Uses Decoupled Architecture." Vol. 8, No. 14, October 24, 1994. Retrieved September 19, 2011.
^ Linley Gwenapp, Microprocessor Report. "MIPS R12000 to Hit 300 MHz." Vol. 11, No. 13, October 6, 1997. Retrieved September 19, 2011.
^ Stephen Shankland, ZDNet. "Itanium: A cautionary tale." December 7, 2005. Retrieved September 19, 2011.
^ Michael Kanellos and Dawn Kawamoto, CNET. "Silicon Graphics scraps MIPS plans." April 9, 1998. Retrieved September 19, 2011.
^ Debora Vrana, Los Angeles Times. "June IPOs Were Not so Hot, but Summer Is Still Young." July 6, 1998. Retrieved September 20, 2011.
^ Crag Bicknell, WIRED. "MIPS Slips in IPO." July 1, 1998. Retrieved September 19, 2011.
^ Margaret Quan, EE Times. "SGI to shed Cray, shift OS focus to Linux." August 10, 1999. Retrieved September 20, 2011.
^ a b Suzanne Deffree, EDN News, "MIPS plans 15% layoff on $108.5M loss." August 14th, 2008. Retrieved March 4th, 2012.
^ VentureBeat Profiles. "Jim Billmaier." Retrieved Sep 20, 2011.
^ UC Berkeley Haas School of Business. "Steve G. Blank." Retrieved Sep 20, 2011.
^ VentureBeat Profiles. "Joe DiNucci." Retrieved Sep 20, 2011.
^ IEEE Computer Society. "John L. Hennessy: 2001 Eckert-Mauchly Award Recipient." Retrieved Sep 20, 2011.
^ Joe Kovar, CRN. "2010 Storage Superstars." June 21, 2010. Retrieved September 20, 2011.
^ "Earl Killian". Paravirtual. 2010-11-26. Retrieved 2010-11-26. 
^ "S-1 Supercomputer Alumni: Earl Killian". Clemson University. June 28, 2005. Retrieved 2010-11-26. Earl Killian's ... MIPS's Director of Architecture ... 
^ BusinessWeek Profiles. "Dan Levin." Retrieved Sep 20, 2011.
^ Computer History Museum. "John Mashey." Retrieved Sep 20, 2011.
^ Computer History Museum. "Bob Miller." Retrieved Sep 20, 2011.
^ Forbes. "Stratton D. Sclavos." Retrieved Sep 20, 2011.
^ Computer History Museum. "Skip Stritter." Retrieved Sep 20, 2011.
^ a b c Junko Yoshida, EE Times. "New CEO Sandeep Vij forms  Team MIPS ." February 7, 2010. Retrieved September 20, 2011.
^ Peter Clarke, EE Times. "MIPS Appoints Former Cavium Exec as CEO MIPS." January 25, 2010. Retrieved Sep 20, 2011.
^ Junko Yoshida, EE Times, "MIPS CEO: Companies need to have a soul". November 1st, 2010. Retrieved March 4th, 2012.
^ Hoovers. "MIPS Technologies." Retrieved Sep 20, 2011.
^ Colleen Taylor, EDN. "MIPS plans HQ in Silicon Forest." March 13, 2007. Retrieved September 20, 2011.
^ Company Press Release.  Synopsys Acquires Analog Business Group of MIPS Technologies.  May 8, 2009. Retrieved May 8, 2009.
^ MIPS Selling All Assets For Combined $7.31/Shr In Cash. Forbes. Retrieved on 2014-05-23.
^ "Imagination Tech to buy MIPS Tech for $100M". Associated Press. 17 December 2012. Retrieved 18 December 2012. 
^ John Hennessy
^ TheLinuxFoundation.org. "MIPS Technologies Joins Linux Foundation." Retrieved August 12, 2011.
^ Steve Bush, ElectronicsWeekly.com. "Google's Android marches onto MIPS processors." Retrieved August 12, 2011.
^ "MIPS Technologies Joins the Open Handset Alliance". MIPS Technologies, Inc. 2009-09-30. 
^ Peter Clarke, EE Times. "MIPS Appoints Former Cavium Exec as CEO MIPS." January 25, 2010. Retrieved August 12, 2011.
^ a b Raphael Savina, AndroidGuys.  CES 2010: First Android Set Top Boxes.  January 6, 2010. Retrieved September 21, 2011.
^ Mark LaPedus, EE Times. "Update: MIPS gets sweet with Honeycomb." April 26, 2011. Retrieved November 4, 2011.
^ Peter Clarke, EE Times. "MIPS: Android remains processor neutral." April 1, 2011. Retrieved November 4, 2011.
^ PC World "[1]."
^ Imagination Technologies "Acquisition of MIPS Technologies completed." February 8, 2013. Retrieved October 25, 2013.
^ Agam Shah, IDG.  MIPS Porting Google s Android 3.0 OS for Its Processors.  April 26, 2011. Retrieved October 3, 2011.
^ Dean Takahashi, VentureBeat.  MIPS breaks into Android mobile phones with latest chips.  January 4, 2011. Retrieved October 5, 2011.
^ a b Brian Caufield, Forbes.  For MIPS, Less is More.  April 20, 2011. Retrieved September 26, 2011.
^ Dean Takahashi, VentureBeat. "MIPS bets big on Google Android systems for the digital home." January 5, 2010. Retrieved September 30, 2011.
^ a b Junko Yoshida, EE Times.  Blow-out quarter  highlights MIPS comeback.  August 5, 2010. Retrieved October 1, 2011.
^ John Spooner, CNET.  MIPS nips new licenses for chips.  April 12, 2001. Retrieved October 3, 2011.
^ a b Robert Cravotta, Embedded Insights.  M14K.  Retrieved October 3, 2011.
^ Zewde Yeraswork, CRN.  MIPS Prepares 64-Bit Prodigy CPU Core Architecture.  March 29, 2011. Retrieved October 2, 2011.
^ Doug Mohney, The Inquirer.  Cavium Hotrods MIPS architecture.  June 25, 2007. Retrieved October 5, 2011.
^ Eric Brown, LinuxForDevices.  "Enea, NetLogic ship Linux development platform for MIPS". Archived from the original on 2013-01-04. .  September 20, 2010. Retrieved September 30, 2011.
^ a b Dean Takahashi, VentureBeat.  MIPS aims to drive into consumer electronics gear. January 5, 2010. Retrieved October 5, 2011.
^ Janko Roettgers, GigaOm.  Next Up for Android: Your Cable Box?.  January 5, 2011. Retrieved September 30, 2011.
^ Imagination Technologies.  MIPS Processors.  Retrieved June 20, 2015.
^ Imagination Technologies.  MIPS Warrior Processor Cores.  Retrieved June 20, 2015.
^ Imagination Technologies.  MIPS Aptiv Processor Cores.  Retrieved June 20, 2015.
^ Robert Cravotta, Embedded Insights.  24K.  Retrieved October 6, 2011.
^ Robert Cravotta, Embedded Insights.  34K.  Retrieved October 6, 2011.
^ Robert Cravotta, Embedded Insights.  74K.  Retrieved October 2, 2011.
^ Robert Cravotta, Embedded Insights.  1004K.  Retrieved October 2, 2011.
^ Owen Fletcher, PC World.  MIPS Ports Android, Shows Embedded Gadgets,  June 3, 2010. Retrieved September 23, 2011.
^ Doug Mohney, The Inquirer.  Cavium Hotrods MIPS architecture.  June 25, 2007. Retrieved September 28, 2011.
^ Eric Brown, LinuxForDevices.  "Enea, NetLogic ship Linux development platform for MIPS". Archived from the original on 2013-01-04. .  September 20, 2010. Retrieved October 5, 2011.
^ Brian Caufield, Forbes.  CES: MIPS Inside.  January 6, 2011. Retrieved October 3, 2011.
^ Junko Yoshida, EE Times.  China Link Helps MIPS Go Mobile.  January 4, 2011. Retrieved October 5, 2011.
^ Mads Olholm, SemmiAccurate.  Tablets to Benefit from 3 New Chinese MIPS Cores.  June 2, 2011. Retrieved September 20, 2011.
^ Linley Gwennap, EE Times.  Broadcom reveals CPU development.  November 23, 2010. Retrieved October 2, 2011.
^ Jim Turley, Electronic Engineering Journal.  Kicking the CAN with Microchip MIPS.  November 24, 2009. Retrieved October 1, 2011.
^ Peter Clarke, EE Times.  Mobileye silicon: A clarification.  August 19, 2011. Retrieved September 22, 2011.
^ Dave Rosenberg, CNET.  The big guns of Linux kernel development.  August 21, 2009. Retrieved September 25, 2011.
^ The NetBSD foundation. "Platforms supported by NetBSD, July 19, 2011. "
^ Ryan Paul, Ars Technica.  MIPS Android port arrives, aimed at the digital home.  August 2009. Retrieved September 28, 2011.
^ Owen Fletcher, PC World.  MIPS Ports Android, Shows Embedded Gadgets,  June 3, 2010. Retrieved October 5, 2011.
^ Dusan Belic, IntoMobile.com.  MIPS porting Android 3.0 Honeycomb platform . April 29, 2011. Retrieved September 25, 2011.
^ Colin Holland, EE Times Europe.  eCosPro developer's kit for microMIPS.  May 16, 2011. Retrieved October 5, 2011.
^ Eric Brown, LinuxForDevices.  "Enea, NetLogic ship Linux development platform for MIPS". Archived from the original on 2013-01-04. .  September 20, 2010. Retrieved October 1, 2011.
^ Edward Lamie, EE Times.  Real-Time Embedded Multithreading: Using ThreadX and MIPS.  February 2009. Retrieved October 2, 2011.