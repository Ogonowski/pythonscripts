The Imperial Japanese Army or IJA ( , Dai-Nippon Teikoku Rikugun), literally "Army of the Greater Japanese Empire", was the official ground-based armed force of the Empire of Japan, from 1871 to 1945. It was controlled by the Imperial Japanese Army General Staff Office and the Ministry of War, both of which were nominally subordinate to the Emperor of Japan as supreme commander of the army and the navy. Later an Inspectorate General of Military (Army) Aviation, became the third agency with oversight over the army. During wartime or national emergencies, the nominal command functions of the emperor would be centralized in an Imperial General Headquarters (IGHQ), an ad-hoc body consisting of the chief and vice chief of the Army General Staff, the minister of war, the chief and vice chief of the Naval General Staff, the inspector general of military aviation, and the inspector general of military training.







During the Meiji Restoration, the military forces loyal to Emperor Meiji were samurai drawn primarily from the loyalist daimyos of Satsuma and Ch sh  Domains. After the successful overthrow of the Government of Meiji Japan or bakufu and establishment of the new government of Meiji Japan modeled on European lines, a more formal military, loyal to the central government rather than individual domains, became recognized by the general populace as a necessity to preserve Japan's independence from western imperialism.
This central army, the "Imperial Japanese Army" (IJA), became even more essential after the abolition of the han system in 1871. To reform the military, the government instituted nationwide conscription in 1873, mandating that every male between the age of 17 and 40 undertake three years active service, followed by a further two years in the first reserve (active) and another two in the second reserve (standby). One of the primary differences between the samurai and peasant class was the right to bear arms; this ancient privilege was suddenly extended to every male in the nation.




The early Imperial Japanese Army was essentially developed with the assistance of advisors from the Second French Empire, through the second French military mission to Japan (1872 80), and the third French military mission to Japan (1884 89). However, due to the victory of the North German Confederation in the Franco-Prussian War, the Japanese government also relied on Prussia as a model for their army, and hired two German military advisors (Major Jakob Meckel, replaced in 1888 by von Wildenbr ck and Captain von Blankenbourg) for the training of the Japanese General Staff from 1886 to April 1890: the Imperial Japanese Army General Staff Office, based on the German General Staff, was established directly under the Emperor in 1878 and was given broad powers for military planning and strategy.

Other known foreign military consultants were Major Pompeo Grillo from the Kingdom of Italy, who worked at the Osaka foundry from 1884 to 1888, followed by Major Quaratezi from 1889 to 1890, and Captain Schermbeck from the Kingdom of the Netherlands, who worked on improving coastal defenses from 1883 to 1886. Japan did not use foreign military advisors between 1890 and 1918, until the French military mission to Japan (1918 19), headed by Commandant Jacques-Paul Faure, was requested to assist in the development of the Japanese air services.




The Japanese invasion of Taiwan under Qing rule in 1874 was a punitive expedition by Japanese military forces in response to the Mudan Incident of December 1871. The Paiwan people, who are indigenous peoples of Taiwan, murdered 54 crewmembers of a wrecked merchant vessel from the Ryukyu Kingdom on the southwestern tip of Taiwan. 12 men were rescued by the local Chinese-speaking community and were transferred to Miyako-jima in the Ryukyu Islands. The Empire of Japan used this as an excuse to both assert sovereignty over the Ryukyu Kingdom, which was a tributary state of both Japan and Qing China at the time, and to attempt the same with Taiwan, a Qing territory. It marked the first overseas deployment of the Imperial Japanese Army and Navy.



Not surprisingly, the new order led to a series of riots from disgruntled samurai. One of the major riots was led by Saig  Takamori, the Satsuma Rebellion, which eventually turned into a civil war. It was put down swiftly by conscripts in the newly-formed Imperial Japanese Army using Western tactics and weapons even though the core of the new army was actually the Tokyo police force, consisting mostly of former samurai.

An Imperial Rescript to Soldiers and Sailors of 1882 called for unquestioning loyalty to the Emperor by the new armed forces and asserted that commands from superior officers were equivalent to commands from the Emperor himself. Thenceforth, the military existed in an intimate and privileged relationship with the imperial institution.
Top-ranking military leaders were given direct access to the Emperor and the authority to transmit his pronouncements directly to the troops. The sympathetic relationship between conscripts and officers, particularly junior officers who were drawn mostly from the peasantry, tended to draw the military closer to the people. In time, most people came to look more for guidance in national matters to military than to political leaders.

By the 1890s, the Imperial Japanese Army had grown to become the most modern army in Asia, well-trained, well-equipped with good morale. However, it was basically an infantry force deficient in cavalry and artillery when compared with its European contemporaries. Artillery pieces, which were purchased from America and a variety of European nations, presented two problems: they were scarce, and the relatively small number that were available were in several different calibers, causing problems with their ammunition supply.




The First Sino-Japanese War (1 August 1894   17 April 1895) was a war fought between Qing China and Meiji Japan over the control of the Kingdom of Korea, which had been under de facto Japanese control since the Japan Korea Treaty of 1876. The Sino-Japanese War would come to symbolize the weakness of the military of the Qing dynasty, with the Japanese securing victory after victory over the Qing forces. This was the result by Japan's new western-style conscript army, which was well-equipped and well-trained when compared with their Qing counterparts. The principal results were a shift in regional dominance in Asia from China to Japan and a fatal blow to the Qing Dynasty. Japan fielded a force of 120,000 in two armies and five divisions. the Treaty of Shimonoseki made the Qing defeat official.




In 1899 1900, Boxer attacks against foreigners in China intensified and later accumulated in the siege of the diplomatic legations in Beijing. An international force consisting of British, French, Russian, German, Italian, Austro-Hungarian, American and Japanese troops was assembled to relieve the legations. The Japanese provided the largest contingent of troops; 20,840, as well as 18 warships. Of the total number, 20,300 were Imperial Japanese Army troops of the 5th Infantry Division under Lt. General Yamaguchi Motoomi, the remainder were 540 naval rikusentai from the Imperial Japanese Navy. The rebels used traditional Chinese martial arts, as opposed to modern military weapons and tactics. This led to them being called "Boxers" by Westerners, as that is how they perceived martial arts at the time. While officially condemning the movement, the Boxers had the unofficial support of the Empress Dowager Cixi. In the end the Boxer leaders were captured and executed. The Empress Dowager, was forced to flee the palace as the foreign armies entered the Forbidden City.




The Russo Japanese War (1904-1905) was the result of tensions between Russia and Japan, largely out of the rival imperialist ambitions over Manchuria and Korea. The Japanese inflicted severe losses on the Russians; however, they were not able to inflict a decisive blow to the Russian armies. Over-reliance on infantry led to large casualties among Japanese forces especially during the siege of Port Arthur.




The Empire of Japan entered the war on the Entente side. Although tentative plans were made to send an expeditionary force of between 100,000 500,000 men to France, ultimately the only action in which the Imperial Japanese Army was involved in was the careful and well executed attack on the German concession of Tsingtao in 1914.




During 1917 18, Japan continued to extend its influence and privileges in China via the Nishihara Loans. Following the collapse of the Russian Empire in the Bolshevik Revolution, during the Siberian Intervention, the Imperial Japanese Army initially planned to send more than 70,000 troops to occupy Siberia as far west as Lake Baykal. The army general staff came to view the Tsarist collapse as an opportunity to free Japan from any future threat from Russia by detaching Siberia and forming an independent buffer state. The plan was scaled back considerably due to opposition from the United States.
In July 1918, President Wilson asked the Japanese government to supply 7,000 troops as part of an international coalition of 24,000 troops planned to support the American Expeditionary Force Siberia. After heated debate in the Diet, the government of Prime Minister Terauchi Masatake agreed to send 12,000 troops, but under the command of Japan, rather than as part of an international coalition. Japan and the United States sent forces to Siberia to bolster the armies of the White Movement leader Admiral Aleksandr Kolchak against the Bolshevik Red Army.
Once the political decision had been reached, the Imperial Japanese Army took over full control under Chief of Staff General Yui Mitsue, and by November 1918, more than 70,000 Japanese troops had occupied all ports and major towns in the Russian Maritime Provinces and eastern Siberia.
In June 1920, America and its allied coalition partners withdrew from Vladivostok after the capture and execution of White Army leader Admiral Aleksandr Kolchak by the Red Army. However, the Japanese decided to stay, primarily due to fears of the spread of communism so close to Japan, and Japanese controlled Korea and Manchuria. The Japanese army provided military support to the Japanese-backed Provisional Priamur Government based in Vladivostok against the Moscow-backed Far Eastern Republic.
The continued Japanese presence concerned the United States, which suspected that Japan had territorial designs on Siberia and the Russian Far East. Subjected to intense diplomatic pressure by the United States and Great Britain, and facing increasing domestic opposition due to the economic and human cost, the administration of Prime Minister Kato Tomosaburo withdrew the Japanese forces in October 1922.



In the 1920s the Imperial Japanese Army expanded rapidly and by 1937 had a force of 300,000 men. Unlike western countries it enjoyed a great deal of independence from government. Under the provisions of the Meiji Constitution, the War Minister was held accountable only to the Emperor Hirohito himself, and not to the elected civilian government. In fact, Japanese civilian administrations needed the support of the Army in order to survive. The Army controlled the appointment of the War Minister and in 1936 a law was passed that stipulated that only an active duty general or lieutenant-general could hold the post. As a result, the military spending as a proportion of the national budget rose disproportionately in the 1920s and 1930s, and various factions within the military exerted disproportionate influence on Japanese foreign policy.
The Imperial Japanese Army was originally known simply as the Army (rikugun) but after 1928, as part of the Army's turn toward romantic nationalism and also in the service of its political ambitions, it retitled itself the Imperial Army (k gun).




In 1931, the Imperial Japanese Army had an overall strength of 198,880 officers and men, organized into 17 divisions. The Manchurian Incident, as it became known in Japan, was the alleged attack on the Japanese-owned railway by Chinese bandits. Action by the military, largely independent of the civilian leadership, led to the invasion of Manchuria in 1931 and later the Second Sino-Japanese War in 1937. As war approached, the Imperial Army's influence with the Emperor waned and the influence of the Imperial Japanese Navy increased. Nevertheless, by 1938 the Army had been expanded to 34 divisions.



From 1932 1945 the Empire of Japan and Soviet Union had a series of conflicts. Japan had set its military sights on Soviet territory as a result of the Hokushin-ron doctrine, and the Japanese establishment of a puppet state in Manchuria brought the two countries into conflict. The war lasted on and off with the last two battles of the 1930s ending in a decisive victory for the Soviets. The conflicts stopped with the signing of the Soviet Japanese Neutrality Pact on April 13, 1941. However, at the Yalta Conference, Stalin agreed to declare war on Japan. On August 5, 1945 the Soviet Union voided their neutrality agreement with Japan.




In 1941, the Imperial Japanese Army had 51 divisions and various special-purpose artillery, cavalry, anti-aircraft and armored units with a total of 1,700,000 men. At the beginning of the Second World War, most of the Japanese Army (27 divisions) was stationed in China. A further 13 divisions defended the Mongolian border, due to concerns about a possible attack by the Soviet Union. However, from 1942, soldiers were sent to Hong Kong (23rd Army), the Philippines (14th Army), Thailand (15th Army), Burma (15th Army), Dutch East Indies (16th Army) and Malaya (25th Army). By 1945, there were 5.5 million men in the Imperial Japanese Army.
From 1943, Japanese troops suffered from a shortage of supplies, especially food, medicine, munitions and armaments largely due to submarine interdiction of supplies and losses to Japanese shipping, which was worsened by a longstanding and severe rivalry with the Imperial Japanese Navy. The lack of supplies caused large numbers of fighter aircraft to become unserviceable for lack of spare parts and "as many as two-thirds of Japan's total military deaths resulted from illness or starvation."



Throughout the Second Sino-Japanese War and World War II, the Imperial Japanese Army had gained a reputation both for its fanaticism and for its brutality against prisoners of war and civilians alike - with the Nanking Massacre being one such example. After Japan surrendered in the summer of 1945, many Imperial Japanese Army officers and enlisted men were tried and punished for committing numerous atrocities and war crimes. In 1949, the trials were ceased, with a total of 5,700 cases having been heard.
Major General Tomitar  Horii did issue a "Guide to Soldiers in the South Seas" in late 1941, which ordered troops not to loot or kill civilians. This was intended to prevent a repeat of atrocities that the Army committed in China, however this only applied to men under his command.
Several reasons are theorized for the especially brutal and merciless behavior exhibited by many members of the IJA towards their adversaries or non-Japanese civilians. One is probably the brutal behavior that they themselves experienced. The IJA was known for the extremely harsh treatment of its enlisted soldiers from the start of training, including beatings, unnecessarily strenuous duty tasks, lack of adequate food, and other violent or harsh disciplinary tactics. This was contrary to the Imperial Rescript to Soldiers and Sailors of 1882, which instructed officers to treat subordinates respectfully. Not until 1943 did the senior command realize this brutality had effects on morale and ordered an end to it, an order which was routinely circumvented or ignored. The spirit of gyokusai ("glorious death") saw them order suicidal attacks with bayonets, when supplies of hand grenades and ammunition were still available.
The reputation of Imperial Army troops during the Pacific War of refusing to surrender was established by the low number of Japanese survivors in numerous battles throughout the Pacific Campaign; 921 captured out of a garrison strength of 31,000 in the Battle of Saipan, 17 out of 3000 in the Battle of Tarawa, 7,400 10,755 out of 117,000 in the Battle of Okinawa, with a high number of battlefield suicides sanctioned by the Imperial Army. In the South West Pacific Area (SWPA) just over 1,000 surrendered in each of 1942 and 1943, around 5,100 in 1944, and over 12,000 in 1945, and might have been greater except for disease. Propaganda through leaflet drops by the Americans accounted for about 20% of surrenders; equating to about one POW for every 6,000 leaflets dropped; while the Japanese objected to the "unscrupulous" leaflets, which contained some truth with regard to the willingness of American forces to accept surrenders from the Japanese. This was in contrast to Imperial Japanese Army practice of depicting American troops as cruel and merciless, referring to them as   (Kichiku Beiei, lit. Demonic Beast American and English) and informing their own troops that Americans would rape all captured women and torture the men, leading directly to brutal treatment of POWs in incidents such as the Bataan Death March and mass suicide of Japanese soldiers and civilians during the Battle of Saipan and Battle of Okinawa.



During the first part of the Showa era, according to the Meiji Constitution, the Emperor had the "supreme command of the Army and the Navy" (Article 11). Hirohito was thus legally supreme commander of the Imperial General Headquarters, founded in 1937 and by which the military decisions were made.

The primary sources such as the "Sugiyama memo", and the diaries of Fumimaro Konoe and Koichi Kido, describe in detail the many informal meetings the Emperor had with his chiefs of staff and ministers. These documents show he was kept informed of all military operations and frequently questioned his senior staff and asked for changes.
According to historians Yoshiaki Yoshimi and Seiya Matsuno, Hirohito authorized by specific orders, transmitted by the Chief of staff of the Army such as Prince Kan'in or Hajime Sugiyama, the use of chemical weapons against Chinese civilians and soldiers. For example, he authorized the use of toxic gas on 375 separate occasions during the invasion of Wuhan in 1938. Such weapons were also authorized during the invasion of Changde.
According to historians Akira Fujiwara and Akira Yamada, Hirohito even made major interventions in some military operations. For example, he pressed Field Marshal Hajime Sugiyama four times during January and February 1942 to increase troop strength and launch attack on Bataan. In August 1943, he scolded Sugiyama for being unable to stop the American advance on the Solomon Islands and asked the general to consider other places to attack.
Only in rare moments of special importance, decisions were made in Imperial council. The Imperial government used this special institution to sanction the invasion of China, the Greater East Asia War and to end the war. In 1945, executing the decision approved in Imperial conference, Emperor Sh wa for the only time directly ordered via recorded radio broadcast to all of Japan, as his last role as commander-in-chief, the surrender to United States forces.







Article 9 of the Japanese Constitution renounced the right to use force as a means of resolving disputes. This was enacted by the Japanese in order to prevent militarism, which had led to conflict. However, in 1947 the Public Security Force formed; later in 1954, with the early stages of the Cold War, the Public Security Force formed the basis of the newly created Ground Self Defense Force. Although significantly smaller than the former Imperial Japanese Army and nominally for defensive purposes only, this force constitutes the modern army of Japan.




Separately, some soldiers of the Imperial Japanese Army continued to fight on isolated Pacific islands until at least the 1970s, with the last known Japanese soldier surrendering in 1974. Intelligence officer Hiroo Onoda, who surrendered on Lubang Island in the Philippines in March 1974, and Teruo Nakamura, who surrendered on the Indonesian island of Morotai in December 1974, appear to have been the last confirmed holdouts.




1870: consisted of 12,000 men.
1873: Seven divisions of ~36,000 men (~46,250 including reserves)
1885: consisted of seven divisions including the Imperial Guard Division.
In the early 1900s, the IJA consisted of 12 divisions, the Imperial Guard Division, and numerous other units. These contained the following:
380,000 active duty and 1st Reserve personnel: former Class A and B(1) conscripts after two-year active tour with 17 and 1/2 year commitment
50,000 Second line Reserve: Same as above but former Class B(2) conscripts
220,000 National Army
1st National Army: 37- to 40-year-old men from end of 1st Reserve to 40 years old.
2nd National Army: untrained 20-year-olds and over-40-year-old trained reserves.

4,250,000 men available for service and mobilization.

1922: 21 divisions and 308,000 men
1924: Post-WWI reductions to 16 divisions and 250,800 men
1925: Reduction to 12 divisions
1934: army increased to 17 divisions
1936: 250,000 active.
1940: 376,000 active with 2 million reserves in 31 divisions
2 divisions in Japan (Imperial Guard plus one other)
2 divisions in Korea
27 divisions in China and Manchuria

In late 1941: 460,000 active in 41 divisions
2 divisions in Japan and Korea
12 divisions in Manchuria
27 divisions in China
plus 59 brigade equivalents.
Independent brigades, Independent Mixed Brigades, Cavalry Brigades, Amphibious Brigades, Independent Mixed regiments, Independent Regiments.

1945: 5 million active in 145 divisions (includes three Imperial Guard), plus numerous individual units, with a large Volunteer Fighting Corps.
includes Imperial Japanese Army Air Service.
Japan Defense Army in 1945 had 55 divisions (53 Infantry and two armor and 32 brigades (25 infantry and seven armor) with 2.35 million men.
2,25 million Army Labour Troops
1.3 million Navy Labour Troops
250,000 Special Garrison Force
20,000 Kempetai

Total military in August 1945 was 6,095,000 including 676,863 Army Air Service. 



Over the course of the Imperial Japanese Army's existence, millions of its soldiers were either killed, wounded or went missing in action.
Taiwan Expedition of 1874: 543 (12 killed in battle and 531 by disease)
First Sino-Japanese War: The IJA suffered 13,823 dead and 3,973 wounded
Russo-Japanese War: The number of total Japanese dead in combat is put at around 47,000, with around 80,000 if disease is included
World War I: 1,455 Japanese were killed, mostly at the Battle of Tsingtao
World War II:
Deaths
Between 2,120,000 to 2,190,000 Imperial Armed Forces dead including non-combat deaths (includes 1,760,955 killed in action),
KIA Breakdown by Theatre:
Army 1931-1945 [China: 408,605 KIA, Against U.S Forces: 485,717 KIA, Burma Campaign: 208,026 KIA, Australian Combat Zone: 199,511 KIA, French Indochina: 2,803 KIA, U.R.R.S/Manchuria: 7,483 KIA, Others/Japan: 33,931 KIA]
Navy: 414,879 KIA All Theatres.

672,000 known civilian dead,

810,000 missing in action and presumed dead.
7,500 prisoners of war


