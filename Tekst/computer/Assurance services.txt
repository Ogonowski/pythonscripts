Assurance service is an independent professional service, typically provided by Chartered or Certified Public Accountants, with the goal of improving the information or the context of the information so that decision makers can make more informed, and presumably better, decisions. Assurance services provide independent and professional opinions that reduce information risk (risk from incorrect information).



Audits can be considered a type of assurance service. However, audits are only designed to test the validity of the financial statements. Under an assurance engagement, accountants can provide a variety of services ranging from information systems security reviews to customer satisfaction surveys. Unlike audit and attestation services that are often highly structured, assurance services tend to be customized and implemented when performed for a smaller group of decision makers within the firm. Often managers must make decisions on things they have incomplete or inaccurate data for, and decisions made on such data may be incorrect and increase the overall business risk. In this respect, assurance services can be very helpful in reducing such risk and help managers or decision makers make more confident decisions within a given firm. This is similar to audits in that investors will choose to invest in a firm that is publishing financial statements that have been audited by an independent firm.
Assurance services can test financial and non-financial information; due to this assurance services can be classified as consulting services. However, assurance services are not considered consulting because in consulting services, an accountant generally uses their professional knowledge to make recommendations for a future event or a procedure, such as the design of an information system or accounting control system. In contrast, assurance services are designed to test the validity of past data of the business cycles. Although there is no boundary to what an accountant can test in assurance services, a practitioner is discouraged from accepting an assurance engagement in which his firm or previous experiences does not provide them with enough expertise to make a professional opinion on the given data.
Assurance services done by accountants differ from nonassurance services.
Other examples of assurance services include:
Accounts receivable review
Business risk assessment
Comfort letter
Customer satisfaction survey
Information systems security review
Internal audit outsourcing
Medical billing services


