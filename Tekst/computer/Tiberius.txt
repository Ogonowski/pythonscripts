Tiberius (Latin: Tiberius Caesar D v  August  F lius Augustus; 16 November 42 BC   16 March 37 AD) was a Roman Emperor from 14 AD to 37 AD. Born Tiberius Claudius Nero, a Claudian, Tiberius was the son of Tiberius Claudius Nero and Livia Drusilla. His mother divorced Nero and married Octavian, later known as Augustus, in 39 BC, making him a step-son of Octavian.
Tiberius would later marry Augustus' daughter (from his marriage to Scribonia), Julia the Elder, and even later be adopted by Augustus, by which act he officially became a Julian, bearing the name Tiberius Julius Caesar. The subsequent emperors after Tiberius would continue this blended dynasty of both families for the following thirty years; historians have named it the Julio-Claudian dynasty. In relations to the other emperors of this dynasty, Tiberius was the stepson of Augustus, grand-uncle of Caligula, paternal uncle of Claudius, and great-grand uncle of Nero.
Tiberius was one of Rome's greatest generals; his conquest of Pannonia, Dalmatia, Raetia, and temporarily, parts of Germania laid the foundations for the northern frontier. But he came to be remembered as a dark, reclusive, and sombre ruler who never really desired to be emperor; Pliny the Elder called him tristissimus hominum, "the gloomiest of men."
After the death of Tiberius  son Drusus Julius Caesar in 23 AD, he became more reclusive and aloof. In 26 AD Tiberius removed himself from Rome and left administration largely in the hands of his unscrupulous Praetorian Prefects Lucius Aelius Sejanus and Quintus Naevius Sutorius Macro.
Caligula, Tiberius' grand-nephew and adopted grandson, succeeded Tiberius upon his death.






Tiberius was born in Rome on 16 November 42 BC to Tiberius Claudius Nero and Livia Drusilla. In 39 BC his mother divorced his biological father and remarried Gaius Julius Caesar Octavianus shortly thereafter, while still pregnant with Tiberius Nero's son. In 38 BC his brother, Nero Claudius Drusus, was born.
Little is recorded of Tiberius's early life. In 32 BC Tiberius made his first public appearance at the age of nine, delivering the eulogy for his biological father. In 29 BC, both he and his brother Drusus rode in the triumphal chariot along with their adoptive father Octavian in celebration of the defeat of Antony and Cleopatra at Actium.
In 23 BC Emperor Augustus became gravely ill and his possible death threatened to plunge the Roman world into chaos again. Historians generally agree that it is during this time that the question of Augustus' heir became most acute, and while Augustus had seemed to indicate that Agrippa and Marcellus would carry on his position in the event of his death, the ambiguity of succession became Augustus' chief problem.
In response, a series of potential heirs seem to have been selected, among them Tiberius and his brother Drusus. In 24 BC at the age of seventeen Tiberius entered politics under Augustus' direction, receiving the position of quaestor, and was granted the right to stand for election as praetor and consul five years in advance of the age required by law. Similar provisions were made for Drusus.



Shortly thereafter Tiberius began appearing in court as an advocate, and it is presumably here that his interest in Greek rhetoric began. In 20 BC, Tiberius was sent East under Marcus Agrippa. The Parthians had captured the standards of the legions under the command of Marcus Licinius Crassus (53 BC) (at the Battle of Carrhae), Decidius Saxa (40 BC), and Marc Antony (36 BC).
After a year of negotiation, Tiberius led a sizable force into Armenia, presumably with the goal of establishing it as a Roman client-state and ending the threat it posed on the Roman-Parthian border. Augustus was able to reach a compromise whereby the standards were returned, and Armenia remained a neutral territory between the two powers.

After returning from the East in 19 BC, Tiberius was married to Vipsania Agrippina, the daughter of Augustus s close friend and greatest general, Marcus Vipsanius Agrippa. He was appointed to the position of praetor, and sent with his legions to assist his brother Drusus in campaigns in the west. While Drusus focused his forces in Gallia Narbonensis and along the German frontier, Tiberius combated the tribes in the Alps and within Transalpine Gaul, conquering Raetia. In 15 BC he discovered the sources of the Danube, and soon afterwards the bend of the middle course. Returning to Rome in 13 BC, Tiberius was appointed as consul, and around this same time his son, Drusus Julius Caesar, was born.
Agrippa's death in 12 BC elevated Tiberius and Drusus with respect to the succession. At Augustus  request in 11 BC, Tiberius divorced Vipsania and married Julia the Elder, Augustus' daughter and Agrippa's widow. This event seems to have been the breaking point for Tiberius; his new marriage with Julia was never a happy one, and produced only a single child who died in infancy.
Reportedly, Tiberius once ran into Vipsania again, and proceeded to follow her home crying and begging forgiveness; soon afterwards, Tiberius met with Augustus, and steps were taken to ensure that Tiberius and Vipsania would never meet again. Tiberius continued to be elevated by Augustus, and after Agrippa's death and his brother Drusus' death in 9 BC, seemed the clear candidate for succession. As such, in 12 BC he received military commissions in Pannonia and Germania; both areas highly volatile and of key importance to Augustan policy.

In 6 BC, Tiberius launched a pincer movement against the Marcomanni. Setting out northwest from Carnuntum on the Danube with four legions, Tiberius passed through Quadi territory in order to invade the Marcomanni from the east. Meanwhile, general Gaius Sentius Saturninus would depart east from Moguntiacum on the Rhine with two or three legions, pass through newly annexed Hermunduri territory, and attack the Marcomanni from the west. The campaign was a resounding success, but Tiberius could not subjugate the Marcomanni because he was soon summoned to the Rhine frontier to protect Rome's new conquests in Germania.
He returned to Rome and was consul for a second time in 7 BC, and in 6 BC was granted tribunician power (tribunicia potestas) and control in the East, all of which mirrored positions that Agrippa had previously held. However, despite these successes and despite his advancement, Tiberius was not happy.




In 6 BC, on the verge of accepting command in the East and becoming the second most powerful man in Rome, Tiberius suddenly announced his withdrawal from politics and retired to Rhodes. The precise motives for Tiberius's withdrawal are unclear. Historians have speculated a connection with the fact that Augustus had adopted Julia's sons by Agrippa Gaius and Lucius, and seemed to be moving them along the same political path that both Tiberius and Drusus had trodden.
Tiberius's move thus seemed to be an interim solution: he would hold power only until his stepsons would come of age, and then be swept aside. The promiscuous, and very public, behavior of his unhappily married wife, Julia, may have also played a part. Indeed, Tacitus calls it Tiberius' intima causa, his innermost reason for departing for Rhodes, and seems to ascribe the entire move to a hatred of Julia and a longing for Vipsania. Tiberius had found himself married to a woman he loathed, who publicly humiliated him with nighttime escapades in the Forum, and forbidden to see the woman he had loved.
Whatever Tiberius's motives, the withdrawal was almost disastrous for Augustus's succession plans. Gaius and Lucius were still in their early teens, and Augustus, now 57 years old, had no immediate successor. There was no longer a guarantee of a peaceful transfer of power after Augustus's death, nor a guarantee that his family, and therefore his family's allies, would continue to hold power should the position of princeps survive.
Somewhat apocryphal stories tell of Augustus pleading with Tiberius to stay, even going so far as to stage a serious illness. Tiberius's response was to anchor off the shore of Ostia until word came that Augustus had survived, then sailing straightway for Rhodes. Tiberius reportedly regretted his departure and requested to return to Rome several times, but each time Augustus refused his requests.



With Tiberius's departure, succession rested solely on Augustus' two young grandsons, Lucius and Gaius Caesar. The situation became more precarious in AD 2 with the death of Lucius. Augustus, with perhaps some pressure from Livia, allowed Tiberius to return to Rome as a private citizen and nothing more. In AD 4, Gaius was killed in Armenia, and Augustus had no other choice but to turn to Tiberius.
The death of Gaius in AD 4 initiated a flurry of activity in the household of Augustus. Tiberius was adopted as full son and heir and in turn, he was required to adopt his nephew, Germanicus, the son of his brother Drusus and Augustus' niece Antonia Minor. Along with his adoption, Tiberius received tribunician power as well as a share of Augustus's maius imperium, something that even Marcus Agrippa may never have had.
In AD 7, Agrippa Postumus, a younger brother of Gaius and Lucius, was disowned by Augustus and banished to the island of Pianosa, to live in solitary confinement. Thus, when in AD 13, the powers held by Tiberius were made equal, rather than second, to Augustus's own powers, he was for all intents and purposes a "co-princeps" with Augustus, and in the event of the latter's passing, would simply continue to rule without an interregnum or possible upheaval.
However, according to Suetonius, after a two-year stint in Germania, which lasted from 10 12 AD, "Tiberius returned and celebrated the triumph which he had postponed, accompanied also by his generals, for whom he had obtained the triumphal regalia. And before turning to enter the Capitol, he dismounted from his chariot and fell at the knees of his father, who was presiding over the ceremonies.  "Since the consuls caused a law to be passed soon after this that he should govern the provinces jointly with Augustus and hold the census with him, he set out for Illyricum on the conclusion of the lustral ceremonies."
Thus according to Suetonius, these ceremonies and the declaration of his "co-princeps" took place in the year 12 AD, after Tiberius return from Germania. "But he was at once recalled, and finding Augustus in his last illness but still alive, he spent an entire day with him in private." Augustus died in AD 14, at the age of 75. He was buried with all due ceremony and, as had been arranged beforehand, deified, his will read, and Tiberius confirmed as his sole surviving heir.







The Senate convened on 18 September, to validate Tiberius's position as Princeps and, as it had done with Augustus before, extend the powers of the position to him. These proceedings are fully accounted by Tacitus. Tiberius already had the administrative and political powers of the Princeps, all he lacked were the titles Augustus, Pater Patriae, and the Civic Crown (a crown made from laurel and oak, in honor of Augustus having saved the lives of Roman citizens).
Tiberius, however, attempted to play the same role as Augustus: that of the reluctant public servant who wants nothing more than to serve the state. This ended up throwing the entire affair into confusion, and rather than humble, he came across as derisive; rather than seeming to want to serve the state, he seemed obstructive. He cited his age as a reason why he could not act as Princeps, stated he did not wish the position, and then proceeded to ask for only a section of the state. Tiberius finally relented and accepted the powers voted to him, though according to Tacitus and Suetonius he refused to bear the titles Pater Patriae, Imperator, and Augustus, and declined the most solid emblem of the Princeps, the Civic Crown and laurels.
This meeting seems to have set the tone for Tiberius's entire rule. He seems to have wished for the Senate and the state to simply act without him and his direct orders were rather vague, inspiring debate more on what he actually meant than on passing his legislation. In his first few years, Tiberius seemed to have wanted the Senate to act on its own, rather than as a servant to his will as it had been under Augustus. According to Tacitus, Tiberius derided the Senate as "men fit to be slaves."




Problems arose quickly for the new Princeps. The Roman legions posted in Pannonia and in Germania had not been paid the bonuses promised them by Augustus, and after a short period of time mutinied when it was clear that a response from Tiberius was not forthcoming. Germanicus and Tiberius's son, Drusus Julius Caesar, were dispatched with a small force to quell the uprising and bring the legions back in line.
Rather than simply quell the mutiny however, Germanicus rallied the mutineers and led them on a short campaign across the Rhine into Germanic territory, stating that whatever treasure they could grab would count as their bonus. Germanicus's forces crossed the Rhine and quickly occupied all of the territory between the Rhine and the Elbe. Additionally, Tacitus records the capture of the Teutoburg forest and the reclaiming of Roman standards lost years before by Publius Quinctilius Varus, when three Roman legions and its auxiliary cohorts had been ambushed by Germanic tribes.
Germanicus had managed to deal a significant blow to Rome's enemies, quell an uprising of troops, and returned lost standards to Rome, actions that increased the fame and legend of the already very popular Germanicus with the Roman people.
After being recalled from Germania, Germanicus celebrated a triumph in Rome in AD 17, the first full triumph that the city had seen since Augustus's own in 29 BC. As a result, in AD 18 Germanicus was granted control over the eastern part of the empire, just as both Agrippa and Tiberius had received before, and was clearly the successor to Tiberius. Germanicus survived a little over a year before dying, accusing Gnaeus Calpurnius Piso, the governor of Syria, of poisoning him.
The Pisones had been longtime supporters of the Claudians, and had allied themselves with the young Octavian after his marriage to Livia, the mother of Tiberius. Germanicus's death and accusations indicted the new Princeps. Piso was placed on trial and, according to Tacitus, threatened to implicate Tiberius. Whether the governor actually could connect the Princeps to the death of Germanicus is unknown; rather than continuing to stand trial when it became evident that the Senate was against him, Piso committed suicide.
Tiberius seems to have tired of politics at this point. In AD 22, he shared his tribunician authority with his son Drusus, and began making yearly excursions to Campania that reportedly became longer and longer every year. In AD 23, Drusus mysteriously died, and Tiberius seems to have made no effort to elevate a replacement. Finally, in AD 26, Tiberius retired from Rome altogether to the island of Capri.



Lucius Aelius Sejanus had served the imperial family for almost twenty years when he became Praetorian Prefect in AD 15. As Tiberius became more embittered with the position of Princeps, he began to depend more and more upon the limited secretariat left to him by Augustus, and specifically upon Sejanus and the Praetorians. In AD 17 or 18, Tiberius had trimmed the ranks of the Praetorian Guard responsible for the defense of the city, and had moved it from encampments outside of the city walls into the city itself, giving Sejanus access to somewhere between 6000 and 9000 troops.
The death of Drusus elevated Sejanus, at least in Tiberius's eyes, who thereafter refers to him as his 'Socius Laborum' (Partner of my labours). Tiberius had statues of Sejanus erected throughout the city, and Sejanus became more and more visible as Tiberius began to withdraw from Rome altogether. Finally, with Tiberius's withdrawal in AD 26, Sejanus was left in charge of the entire state mechanism and the city of Rome.
Sejanus's position was not quite that of successor; he had requested marriage in AD 25 to Tiberius's niece, Livilla, though under pressure quickly withdrew the request. While Sejanus's Praetorians controlled the imperial post, and therefore the information that Tiberius received from Rome and the information Rome received from Tiberius, the presence of Livia seems to have checked his overt power for a time. Her death in AD 29 changed all that.
Sejanus began a series of purge trials of Senators and wealthy equestrians in the city of Rome, removing those capable of opposing his power as well as extending the imperial (and his own) treasury. Germanicus's widow Agrippina the Elder and two of her sons, Nero Caesar and Drusus Caesar were arrested and exiled in AD 30 and later all died in suspicious circumstances. In Sejanus's purge of Agrippina the Elder and her family, Caligula, Agrippina the Younger, Julia Drusilla, and Julia Livilla were the only survivors.



In 31, Sejanus held the consulship with Tiberius in absentia, and began his play for power in earnest. Precisely what happened is difficult to determine, but Sejanus seems to have covertly attempted to court those families who were tied to the Julians, and attempted to ingratiate himself with the Julian family line with an eye towards placing himself, as an adopted Julian, in the position of Princeps, or as a possible regent. Livilla was later implicated in this plot, and was revealed to have been Sejanus's lover for a number of years.
The plot seems to have involved the two of them overthrowing Tiberius, with the support of the Julians, and either assuming the Principate themselves, or serving as regent to the young Tiberius Gemellus or possibly even Gaius Caligula. Those who stood in his way were tried for treason and swiftly dealt with.
In AD 31 Sejanus was summoned to a meeting of the Senate, where a letter from Tiberius was read condemning Sejanus and ordering his immediate execution. Sejanus was tried, and he and several of his colleagues were executed within the week. As commander of the Praetorian Guard, he was replaced by Naevius Sutorius Macro.
Tacitus claims that more treason trials followed and that whereas Tiberius had been hesitant to act at the outset of his reign, now, towards the end of his life, he seemed to do so without compunction. Hardest hit were those families with political ties to the Julians. Even the imperial magistracy was hit, as any and all who had associated with Sejanus or could in some way be tied to his schemes were summarily tried and executed, their properties seized by the state (in a similar way, in the few years after Valeria Messalina's death, Agrippina the Younger removed anyone she considered loyal to Messalina's memory, much in the same way that Sejanus's followers were executed). As Tacitus vividly describes,

Executions were now a stimulus to his fury, and he ordered the death of all who were lying in prison under accusation of complicity with Sejanus. There lay, singly or in heaps, the unnumbered dead, of every age and sex, the illustrious with the obscure. Kinsfolk and friends were not allowed to be near them, to weep over them, or even to gaze on them too long. Spies were set round them, who noted the sorrow of each mourner and followed the rotting corpses, till they were dragged to the Tiber, where, floating or driven on the bank, no one dared to burn or to touch them.

However, Tacitus' portrayal of a tyrannical, vengeful emperor has been challenged by several modern historians. The prominent ancient historian Edward Togo Salmon notes in his work, A history of the Roman world from 30 BC to AD 138:

"In the whole twenty two years of Tiberius' reign, not more than fifty-two persons were accused of treason, of whom almost half escaped conviction, while the four innocent people to be condemned fell victims to the excessive zeal of the Senate, not to the Emperor's tyranny".

While Tiberius was in Capri, rumours abounded as to what exactly he was doing there. Suetonius records the rumours of lurid tales of sexual perversity, including graphic depictions of child molestation, and cruelty, and most of all his paranoia. While heavily sensationalized, Suetonius' stories at least paint a picture of how Tiberius was perceived by the Roman senatorial class, and what his impact on the Principate was during his 23 years of rule.



The affair with Sejanus and the final years of treason trials permanently damaged Tiberius' image and reputation. After Sejanus's fall, Tiberius' withdrawal from Rome was complete; the empire continued to run under the inertia of the bureaucracy established by Augustus, rather than through the leadership of the Princeps. Suetonius records that he became paranoid, and spent a great deal of time brooding over the death of his son. Meanwhile, during this period a short invasion by Parthia, incursions by tribes from Dacia and from across the Rhine by several Germanic tribes occurred.
Little was done to either secure or indicate how his succession was to take place; the Julians and their supporters had fallen to the wrath of Sejanus, and his own sons and immediate family were dead. Two of the candidates were either Caligula, the sole surviving son of Germanicus, or his own grandson, Tiberius Gemellus. However, only a half-hearted attempt at the end of Tiberius' life was made to make Caligula a quaestor, and thus give him some credibility as a possible successor, while Gemellus himself was still only a teenager and thus completely unsuitable for some years to come.



Tiberius died in Misenum on 15 March AD 37, at the age of 78. Tacitus records that upon the news of his death the crowd rejoiced, only to become suddenly silent upon hearing that he had recovered, and rejoiced again at the news that Caligula and Macro had smothered him. This is not recorded by other ancient historians and is most likely apocryphal, but some historians consider it indicative of how the senatorial class felt towards the Emperor at the time of his death. After his death, the Senate refused to vote him divine honors, and mobs filled the streets yelling "To the Tiber with Tiberius!" in reference to a method of disposal reserved for the corpses of criminals. Instead the body of the emperor was cremated and his ashes were quietly laid in the Mausoleum of Augustus, later to be scattered in AD 410 during the Sack of Rome.
In his will, Tiberius had left his powers jointly to Caligula and Tiberius Gemellus. Caligula's first act on becoming Princeps was to void Tiberius' will and have Gemellus executed.
Tiberius' heir Caligula not only spent Tiberius' fortune of 2,700,000,000 sesterces but would also begin the chain of events which would bring about the downfall of the Julio-Claudian dynasty in AD 68.







Were he to have died prior to AD 23, he might have been hailed as an exemplary ruler. Despite the overwhelmingly negative characterization left by Roman historians, Tiberius left the imperial treasury with nearly 3 billion sesterces upon his death. Rather than embark on costly campaigns of conquest, he chose to strengthen the existing empire by building additional bases, using diplomacy as well as military threats, and generally refraining from getting drawn into petty squabbles between competing frontier tyrants.
The result was a stronger, more consolidated empire. Of the authors whose texts have survived, only four describe the reign of Tiberius in considerable detail: Tacitus, Suetonius, Cassius Dio and Velleius Paterculus. Fragmentary evidence also remains from Pliny the Elder, Strabo and Seneca the Elder. Tiberius himself wrote an autobiography which Suetonius describes as "brief and sketchy," but this book has been lost.




The most detailed account of this period is handed down to us by Tacitus, whose Annals dedicate the first six books entirely to the reign of Tiberius. Tacitus was a Roman senator, born during the reign of Nero in 56 AD, and consul suffect in AD 97. His text is largely based on the acta senatus (the minutes of the session of the Senate) and the acta diurna populi Romani (a collection of the acts of the government and news of the court and capital), as well as speeches by Tiberius himself, and the histories of contemporaries such as Cluvius Rufus, Fabius Rusticus and Pliny the Elder (all of which are lost).
Tacitus' narrative emphasizes both political and psychological motivation. The characterisation of Tiberius throughout the first six books is mostly negative, and gradually worsens as his rule declines, identifying a clear breaking point with the death of Drusus in 23 AD.
The rule of Julio-Claudians is generally described as unjust and 'criminal' by Tacitus. Even at the outset of his reign, he seems to ascribe many of Tiberius' virtues merely to hypocrisy. Another major recurring theme concerns the balance of power between the Senate and the Emperors, corruption, and the growing tyranny among the governing classes of Rome. A substantial amount of his account on Tiberius is therefore devoted to the treason trials and persecutions following the revival of the maiestas law under Augustus. Ultimately, Tacitus' opinion on Tiberius is best illustrated by his conclusion of the sixth book:

His character too had its distinct periods. It was a bright time in his life and reputation, while under Augustus he was a private citizen or held high offices; a time of reserve and crafty assumption of virtue, as long as Germanicus and Drusus were alive. Again, while his mother lived, he was a compound of good and evil; he was infamous for his cruelty, though he veiled his debaucheries, while he loved or feared Sejanus. Finally, he plunged into every wickedness and disgrace, when fear and shame being cast off, he simply indulged his own inclinations.




Suetonius was an equestrian who held administrative posts during the reigns of Trajan and Hadrian. The Twelve Caesars details a biographical history of the principate from the birth of Julius Caesar to the death of Domitian in AD 96. Like Tacitus, he drew upon the imperial archives, as well as histories by Aufidius Bassus, Cluvius Rufus, Fabius Rusticus and Augustus' own letters.
His account is more sensationalist and anecdotal than that of his contemporary. The most famous sections of his biography delve into the numerous alleged debaucheries Tiberius remitted himself to while at Capri. Nevertheless, Suetonius also reserves praise for Tiberius' actions during his early reign, emphasizing his modesty.



One of the few surviving sources contemporary with the rule of Tiberius comes from Velleius Paterculus, who served under Tiberius for eight years (from AD 4) in Germany and Pannonia as praefect of cavalry and legatus. Paterculus' Compendium of Roman History spans a period from the fall of Troy to the death of Livia in AD 29. His text on Tiberius lavishes praise on both the emperor and Sejanus. How much of this is due to genuine admiration or prudence remains an open question, but it has been conjectured that he was put to death in AD 31 as a friend of Sejanus.




The Gospels mention that during Tiberius' reign, Jesus of Nazareth preached and was executed under the authority of Pontius Pilate, the Roman governor of Judaea province. In the Bible, Tiberius is mentioned by name only once, in Luke 3:1, which states that John the Baptist entered on his public ministry in the fifteenth year of his reign. Many references to Caesar (or the emperor in some other translations), without further specification, would seem to refer to Tiberius. Similarly, the "Tribute Penny" referred to in Matthew and Mark is popularly thought to be a silver denarius coin of Tiberius.
During Tiberius' reign Jews had become more prominent in Rome and Jewish and Gentile followers of Jesus began proselytizing Roman citizens, increasing long-simmering resentments. Tiberius in 19 AD ordered Jews who were of military age to join the Roman Army. Tiberius banished the rest of the Jews from Rome and threatened to enslave them for life if they did not leave the city.
There is considerable debate among historians as to when Christianity was differentiated from Judaism. According to Tertullian, a Roman lawyer who had access to the archives of the Empire, Tiberius had requested the Senate, a few years after Jesus' crucifixion, to publicly recognize Christianity. Most scholars believe that Roman distinction between Jews and Christians took place around 70 AD Tiberius most likely viewed Christians as a Jewish sect rather than a separate, distinct faith.



The palace of Tiberius at Rome was located on the Palatine Hill, the ruins of which can still be seen today. No major public works were undertaken in the city during his reign, except a temple dedicated to Augustus and the restoration of the theater of Pompey, both of which were not finished until the reign of Caligula. In addition, remnants of Tiberius' villa at Sperlonga, which includes a grotto where the important Sperlonga sculptures were found in fragments, and the Villa Jovis on top of Capri have been preserved. The estate at Capri is said by Tacitus to have included a total of twelve villas across the island, of which Villa Jovis was the largest.
Tiberius refused to be worshipped as a living god, and allowed only one temple to be built in his honor at Smyrna. The town Tiberias, in modern Israel on the western shore of the Sea of Galilee was named in Tiberius's honour by Herod Antipas.



Tiberius has been represented in fiction, in literature, film and television, and in video games, often as a peripheral character in the central storyline. One such modern representation is in the novel I, Claudius by Robert Graves, and the consequent BBC television series adaptation, where he is portrayed by George Baker. In I, Claudius Tiberius is morose and sulking, lacking the ambition of his mother to become Emperor; Tiberius' chief flaw is that he desperately wants to be loved; in a desperate plea to be loved after death Tiberius chooses Caligula as his successor, knowing that he will be a worse Emperor.
In the 1968 ITV historical drama The Caesars, Tiberius (by Andr  Morell) is the central character for much of the series and is portrayed in a much more balanced way than in I, Claudius.
He also appears as a minor character in the 2006 film The Inquiry, in which he is played by Max von Sydow. In addition, Tiberius has prominent roles in Ben-Hur (played by George Relph in his last starring role), and in A.D. (played by James Mason).
Played by Ernest Thesiger, he featured in The Robe (1953). He was featured in the 1979 film Caligula, portrayed by Peter O'Toole. He was an important character in Taylor Caldwell's 1958 novel, Dear and Glorious Physician, a biography of St Luke the Evangelist, author of the third canonical Gospel. He is featured as a young man in Michelle Moran's novel Cleopatra's Daughter, a novel about the life of Cleopatra Selene and Alexander Helios.



Tiberius was married two times, with only his first union producing a child who would survive to adulthood:
Vipsania Agrippina, daughter of Marcus Vipsanius Agrippa (16 11 BC)
Drusus Julius Caesar (13 BC   23 AD)

Julia the Elder, only daughter of Augustus (11 6 BC)



(See also Julio-Claudian family tree)


