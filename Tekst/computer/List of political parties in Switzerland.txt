This is a list of political parties in Switzerland.
Switzerland has a multi-party system. Since 1959, the four largest parties have formed a coalition government, according to a "Zauberformel" or "magic formula". This arithmetic formula divides the seven cabinet seats among representatives of the four largest parties.






The following parties are represented either in the Swiss Federal Assembly or in cantonal parliaments (cantonal parliament seats as of March 2014):
Notes:   Federal Council since 2009;   Council of States, 2011;   National Council 2011; * Including one independent member of the Council of States (Thomas Minder); ** including independents



The following groups or parties are not represented at either the cantonal or national level (but may hold positions in municipal parliaments).






Sources: The Swiss Federal Chancellery












Politics of Switzerland
List of political parties by country






Pierre Cormon, Swiss Politics for Complete Beginners, Editions Slatkine, 2014. ISBN 978-2-8321-0607-5



Federal Chancellery