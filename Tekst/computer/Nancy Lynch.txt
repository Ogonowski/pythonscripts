Nancy Ann Lynch (born January 19, 1948) is a professor at the Massachusetts Institute of Technology. She is the NEC Professor of Software Science and Engineering in the EECS department and heads the Theory of Distributed Systems research group at MIT's Computer Science and Artificial Intelligence Laboratory.
She is the author of numerous research articles about distributed algorithms and impossibility results, and about formal modeling and validation of distributed systems (see, e.g., input/output automaton). She is the author of the graduate textbook "Distributed Algorithms". She is a member of the National Academy of Engineering, and an ACM Fellow.
Lynch was born in Brooklyn, and her academic training was in mathematics, at Brooklyn College and MIT, where she received her Ph.D. in 1972 under the supervision of Albert R. Meyer. She served on the math and computer science faculty at several other universities, including Tufts University, the University of Southern California and Georgia Tech, prior to joining the MIT faculty in 1982. Since then, she has been working on applying mathematics to the tasks of understanding and constructing complex distributed systems.



1997: ACM Fellow
2001: Dijkstra Prize
2001: National Academy of Engineering
2006: Van Wijngaarden Award
2007: Knuth Prize
2007: Dijkstra Prize
2010: IEEE Emanuel R. Piore Award
2012: Athena Lecturer



^ Who's who of American women. Marquis Who's Who, 1973. p. 587.
^ Lynch, Nancy (1996). Distributed Algorithms. San Francisco, CA: Morgan Kaufmann Publishers. ISBN 978-1-55860-348-6. 
^ "Nancy A Lynch   Award Winner". Association for Computing Machinery. Retrieved 31 October 2013. 
^ Nancy Lynch at the Mathematics Genealogy Project
^ "NAE Members Directory - Dr. Nancy A. Lynch". NAE. Retrieved December 31, 2010. 
^ "IEEE Emanuel R. Piore Award Recipients" (PDF). IEEE. Retrieved December 31, 2010. 
^ "Lynch named Athena Lecturer". MIT News. 18 April 2012. Retrieved 31 October 2013. 



Nancy Lynch's home page at MIT
Works by or about Nancy Lynch in libraries (WorldCat catalog)
"Nancy Lynch Celebration: Sixty and Beyond".  A series of invited lectures at PODC 2008 and CONCUR 2008.