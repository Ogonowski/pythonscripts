In a synchronous system, operations are coordinated under the centralized control of a fixed-rate clock signal or several clocks. An asynchronous digital system, in contrast, has no global clock: instead, it operates under distributed control, with concurrent hardware components communicating and synchronizing on channels.



Asynchronous systems   much like object-oriented software   are typically constructed out of modular 'hardware objects', each with well-defined communication interfaces. These modules may operate at variable speeds, whether due to data-dependent processing, dynamic voltage scaling, or process variation. The modules can then be combined together to form a correct working system, without reference to a global clock signal. Typically, low power is obtained since components are activated only on demand. Furthermore, several asynchronous styles have been shown to accommodate clocked interfaces, and thereby support mixed-timing design. Hence, asynchronous systems match well the need for correct-by-construction methodologies in assembling large-scale heterogeneous and scalable systems.



There is a large spectrum of asynchronous design styles, with tradeoffs between robustness and performance (and other parameters such as power). The choice of design style depends on the application target: reliability/ease-of-design vs. speed. The most robust designs use 'delay-insensitive circuits', whose operation is correct regardless of gate and wire delays; however, only limited useful systems can be designed with this style. Slightly less robust, but much more useful, are 'quasi-delay-insensitive circuits' (also known as 'speed-independent'), such as Delay Insensitive Minterm Synthesis, which operate correctly regardless of gate delays; however, wires at each fanout point must be tuned for roughly equal delays. Less robust but faster circuits, requiring simple localized one-sided timing constraints, include controllers using 'fundamental mode operation' (i.e. with setup/hold requirements on when new inputs can be received), and 'bundled datapaths' using matched delays (see below). At the extreme, high-performance 'timed circuits' have been proposed, which use tight two-side timing constraints, where the clock can still be avoided but careful physical delay tuning is required, such as for some high-speed pipeline applications.



Asynchronous communication is typically performed on channels. Communication is used both to synchronize operations of the concurrent system as well as to pass data. A simple channel typically consists of two wires: a request and an acknowledge. In a '4-phase handshaking protocol' (or return-to-zero), the request is asserted by the sender component, and the receiver responds by asserting the acknowledge; then both signals are de-asserted in turn. In a '2-phase handshaking protocol' (or transition-signalling), the requester simply toggles the value on the request wire (once), and the receiver responds by toggling the value on the acknowledge wire. Channels can also be extended to communicate data.



Asynchronous datapaths are typically encoded using several schemes. Robust schemes use two wires or 'rails' for each bit, called 'dual-rail encoding'. In this case, first rail is asserted to transmit a 0 value, or the second rail is asserted to transmit a 1 value. The asserted rail is then reset to zero before the next data value is transmitted, thereby indicating 'no data' or a 'spacer' state. A less robust, but widely used and practical scheme, is called 'single-rail bundled data'. Here, a single-rail (i.e. synchronous-style) function block can be used, with an accompanying worst-case matched delay. After valid data inputs arrive, a request signal is asserted as the input to the matched delay. When the matched delay produces a 'done' output, the block guaranteed to have completed computation. While this scheme has timing constraints, they are simple, localized (unlike in synchronous systems), and one-sided, hence are usually easy to validate.



The literature in this field exists in a variety of conference and journal proceedings. The leading symposium is the IEEE Async Symposium (International Symposium on Asynchronous Circuits and Systems), founded in 1994. A variety of asynchronous papers have also been published since the mid-1980s in such conferences as IEEE/ACM Design Automation Conference, IEEE International Conference on Computer Design, IEEE/ACM International Conference on Computer-Aided Design, International Solid-State Circuits Conference, and Advanced Research in VLSI, as well as in leading journals such as IEEE Transactions on VLSI Systems, IEEE Transactions on Computer-Aided Design of Integrated Circuits and Systems, and Transactions on Distributed Computing.



Claire Tristram, "It's Time for Clockless Chips", cover story, MIT's Technology Review Magazine, vol. 104:8, pp. 36-41, October 2001.
C.H. van Berkel, M.B. Josephs, and S.M. Nowick, Applications of Asynchronous Circuits, Proceedings of the IEEE, Vol. 87, No. 2, pp. 223-233, February 1999. (This entire issue is devoted to asynchronous circuits, with many other relevant articles.)
L. Lavagno and S.M. Nowick, "Asynchronous Control Circuits", chapter 10 in eds. Soha Hassoun and Tsutomu Sasao (2002). Logic Synthesis and Verification. Kluwer Academic Publishers. ISBN 0-7923-7606-4. , pp. 255-284,(Includes pointers to recent asynchronous chips, as well as coverage of CAD techniques for asynchronous control circuits.)
Adapted from Steve Nowick's column in the ACM SIGDA e-newsletter by Igor Markov
Original text is available at http://www.sigda.org/newsletter/2006/eNews_060115.html



Plesiochronous system
Mesochronous network
Isochronous timing
Integrated circuit design
Electronic design automation
Design flow (EDA)
perfect clock gating



ARM ARM996HS clockless processor
Navarre AsyncArt. N-Protocol: Asynchronous Design Methodology for FPGAs