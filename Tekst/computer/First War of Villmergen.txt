The First War of Villmergen was a Swiss religious war which lasted from 5 January until 7 March 1656, at the time of the Old Swiss Confederacy. On the one hand were the Protestant cities of Z rich and Bern, on the other the Catholic places in Central Switzerland. The Protestants tried to break the political hegemony of the Catholics, that had been in existence ever since the Second Kappel Landfrieden of 1531. The casus belli was the expulsion and execution of Protestants from the Schwyz commune of Arth. The Z rcher unsuccessfully besieged the Central Swiss-allied city of Rapperswil and thereby drove their forces together. The Bernese were defeated and repelled in the First Battle of Villmergen. The Third Landfrieden ended the conflict and restored the pre-war balance of power.



During the Swiss peasant war of 1653, when the governments of the Protestant and Catholic cantons jointly moved against the insurgent peasants, the confessional differences that had existed for over a century were merely temporarily pushed towards the background. In 1654, the Z rcher mayor Johann Heinrich Waser received the task of working out a plan to reorganise the Confederacy. However, the Federal Project of 1655 was rejected by the Catholics, because they saw a threat to their dominance in it. The difference between the religions surfaced once again.
In September 1655, the enmities escalated when Protestants living in the Schwyz village Arth fled to Z rich, after which the authorities confiscated their properties. Four of these "Nicodemite" were executed by the Schwyzers, three others were delivered to the Inquisition in Milan. On an extraordinary Tagsatzung in December, Z rich demanded that those responsible be punished, that formal apologies be made and the dissolution of the Catholic Golden League founded in 1586. When these demands were ignored, Z rich declared war on 6 January 1656.
The Catholic towns in Central Switzerland promised Schwyz their support. On the Protestant side, only Bern gave its full-scale help, while Schaffhausen only provided troops for defence. Basel, Fribourg, Solothurn, Appenzell Ausserrhoden, Glarus, the Three Leagues and St. Gallen remained neutral.



Even a day before the declaration of war, Z rcher battalions marched to Rheinau to plunder the town and Rheinau Abbey. On 7 January, general Hans Rudolf Werdm ller led the Z rcher main force to Rapperswil and laid siege to the city. Small units took Frauenfeld, Kaiserstuhl, Klingnau and Zurzach, others entrenched themselves at Oberwil and Kappel am Albis. The Schaffhausers lined up between W denswil and H tten.
Bern mobilised on 8 January and was initially on its own. About two thirds of its soldiers were needed to guard the borders with its Catholic neighbours. The remaining soldiers moved towards Aarau under command of general Sigmund von Erlach. They intended to join forces with the Z rcher there, however, those were still engaged in the unfavourably developing siege of Rapperswil. From the Catholic side, Lucerne took supreme command of the army. All Lucernese and Zuger troops that were not already summoned to guard the borders, gathered in Muri and united at Boswil with battalions from the Freie  mter.
On 24 January 1656, the First Battle of Villmergen occurred. The Catholic troops surprised the Bernese army, that had arrived at Villmergen in the early evening. Despite their numerical and weapon-technical inferiority, the Catholics were able to repel them, to which the missing coordination among the Bernese contributed a large part. After the Z rcher had attempted a final assault on Rapperswil on 3 February, they ended the siege without success. In the following weeks, several smaller skirmishes and attacks on the populace happened.



France and Savoy mediated between the belligerents. Those concluded the Third Landfrieden on 7 March, in which they solemnly swore to cease combat and granted amnesty for misconduct committed during the war. Moreover, all troops were withdrawn, prisoners of war released and the erected redoubts were distmantled. Every canton obtained the right to maintain the status quo concerning religion. Controversial issues such as damage compensations were transferred to arbitral tribunal, but because of bad blood within the commission remained unresolved in many cases. The actual direct cause of the war, the Protestant refugees from Arth, was disregarded.
In fact, the peace treaty confirmed the balance of power established by the 1531 Second Kappel Landfrieden, that is to say, the political dominance of the Catholic cantons within the Confederacy.



First War of Kappel (1529)
Second War of Kappel (1531)
Toggenburg War or Second War of Villmergen (1712)
Sonderbund War (1847)



(German) Hans Rudolf Fuhrer, "Villmerger Kriege 1656/1712", in Milit rgeschichte zum Anfassen 19 (Bern 2005). Milit rische Akademie der ETH Z rich/Bundesamt f r Bauten und Logistik.
(German) Thomas Lau, Villmergerkrieg, Erster (2014). Historisches Lexikon der Schweiz.


