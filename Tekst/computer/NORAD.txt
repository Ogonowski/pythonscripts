Not to be confused with the Norwegian Agency for Development Cooperation, also abbreviated NORAD.

North American Aerospace Defense Command (NORAD, / n r d/) is a combined organization of the United States and Canada that provides aerospace warning, air sovereignty, and defense for Northern America. Headquarters for NORAD and the NORAD/United States Northern Command (USNORTHCOM) center are located at Peterson Air Force Base in El Paso County, near Colorado Springs, Colorado. The nearby Cheyenne Mountain nuclear bunker has the Alternate Command Center. The NORAD commander and deputy commander (CINCNORAD) are, respectively, a United States four-star general or equivalent and a Canadian three-star general or equivalent.



CINCNORAD maintains the NORAD headquarters at Peterson Air Force Base near Colorado Springs, Colorado. The NORAD and USNORTHCOM Command Center at Peterson AFB serves as a central collection and coordination facility for a worldwide system of sensors designed to provide the commander and the leadership of Canada and the U.S. with an accurate picture of any aerospace or maritime threat. NORAD has administratively divided the North American landmass into three regions: the Alaska NORAD (ANR) Region, under Eleventh Air Force (11 AF); the Canadian NORAD (CANR) Region, under 1 Canadian Air Division, and the Continental U.S. (CONR) Region, under 1 AF/CONR-AFNORTH. Both the CONR and CANR regions are divided into eastern and western sectors.



The Alaska NORAD Region (ANR) maintains continuous capability to detect, validate and warn of any atmospheric threat in its area of operations from its Regional Operations Control Center (ROCC) at Joint Base Elmendorf Richardson, Alaska (which is an amalgamation of the United States Air Force's Elmendorf Air Force Base and the United States Army's Fort Richardson, which were merged in 2010).
ANR also maintains the readiness to conduct a continuum of aerospace control missions, which include daily air sovereignty in peacetime, contingency and/or deterrence in time of tension, and active air defense against manned and unmanned air-breathing atmospheric vehicles in times of crisis.
ANR is supported by both active duty and reserve units. Active duty forces are provided by 11 AF and the Canadian Armed Forces (CAF), and reserve forces provided by the Alaska Air National Guard. Both 11 AF and the CAF provide active duty personnel to the ROCC to maintain continuous surveillance of Alaskan airspace.



1 Canadian Air Division/Canadian NORAD Region Headquarters is at CFB Winnipeg, Manitoba. It is responsible for providing surveillance and control of Canadian airspace. The Royal Canadian Air Force provides alert assets to NORAD. CANR is divided into two sectors, which are designated as the Canada East Sector and Canada West Sector. Both Sector Operations Control Centers (SOCCs) are co-located at CFB North Bay Ontario. The routine operation of the SOCCs includes reporting track data, sensor status and aircraft alert status to NORAD headquarters.
Canadian air defense forces assigned to NORAD include 409 Tactical Fighter Squadron at CFB Cold Lake, Alberta and 425 Tactical Fighter Squadron at CFB Bagotville, Quebec. All squadrons fly the McDonnell Douglas CF-18 Hornet fighter aircraft.
To monitor for drug trafficking, in cooperation with the Royal Canadian Mounted Police and the United States drug law enforcement agencies, the Canadian NORAD Region monitors all air traffic approaching the coast of Canada. Any aircraft that has not filed a flight plan may be directed to land and be inspected by RCMP and Canada Border Services Agency.




The Continental NORAD Region (CONR) is the component of NORAD that provides airspace surveillance and control and directs air sovereignty activities for the Contiguous United States (CONUS).
CONR is the NORAD designation of the United States Air Force First Air Force/AFNORTH. Its headquarters is located at Tyndall Air Force Base, Florida. The First Air Force (1 AF) became responsible for the USAF air defense mission on 30 September 1990. AFNORTH is the United States Air Force component of United States Northern Command (NORTHCOM),
1 AF/CONR-AFNORTH comprises State Air National Guard Fighter Wings assigned an air defense mission to 1 AF/CONR-AFNORTH, made up primarily of citizen Airmen. The primary weapons systems are the McDonnell Douglas F-15 Eagle and General Dynamics F-16 Fighting Falcon aircraft.
It plans, conducts, controls, coordinates and ensures air sovereignty and provides for the unilateral defense of the United States. It is organized with a combined First Air Force command post at Tyndall Air Force Base and two Sector Operations Control Centers (SOCC) at Rome, New York for the US East ROCC and McChord Field, Washington for the US West ROCC manned by active duty personnel to maintain continuous surveillance of CONUS airspace.
In its role as the CONUS NORAD Region, 1 AF/CONR-AFNORTH also performs counter-drug surveillance operations.



NORAD (originally known as the North American Air Defense Command), was recommended by the Joint Canadian-U.S. Military Group in late 1956, approved by the United States JCS in February 1957, and announced on 1 August 1957; the "establishment of [NORAD] command headquarters" was on 12 September 1957, at Ent Air Force Base's 1954 blockhouse. The 1958 international agreement designated the NORAD commander always be a United States officer (Canadian vice commander), and "RCAF officers ... agreed the command's primary purpose would be early warning and defense for SAC's retaliatory forces." In late 1958, Canada and the United States started the Continental Air Defense Integration North (CADIN) for the SAGE air defense network (initial CADIN cost sharing agreement between the countries was on 5 January 1959), and two December 1958 plans submitted by NORAD had "average yearly expenditure of around five and one half billions", including "cost of the accelerated Nike Zeus program" and three Ballistic Missile Early Warning System (BMEWS) sites.

Canada's NORAD bunker with a SAGE AN/FSQ-7 Combat Direction Central computer was constructed from 1959 to 1963, and each of the USAF's eight smaller AN/FSQ-8 Combat Control Central systems provided NORAD with data and could command the entire United States air defense. The RCAF's 1950 "ground observer system, the Long Range Air Raid Warning System," was discontinued and on 31 January 1959, the United States Ground Observer Corps was deactivated. The Cheyenne Mountain nuclear bunker's planned mission was expanded in August 1960 to "a hardened center from which CINCNORAD would supervise and direct operations against space attack as well as air attack" (NORAD would be renamed North American Aerospace Defense Command in March 1981). The Secretary of Defense assigned on 7 October 1960, "operational command of all space surveillance to Continental Air Defense Command (CONAD) and operational control to North American Air Defense Command (NORAD)".
The JCS placed the Ent Air Force Base Space Detection and Tracking System (496L System with Philco 2000 Model 212 computer) "under the operational control of CINCNORAD on December 1, 1960"; during Cheyenne Mountain nuclear bunker excavation, and the joint SAC-NORAD exercise "Sky Shield II"   and on 2 September 1962   "Sky Shield III" were conducted for mock penetration of NORAD sectors.
NORAD command center operations moved from Ent Air Force Base to the 1963 partially underground "Combined Operations Center" for Aerospace Defense Command and NORAD at the Chidlaw Building. President John F. Kennedy visited "NORAD headquarters" after the 5 June 1963 United States Air Force Academy graduation. NORAD had an exhibit at the 1964 New York World's Fair, and on 30 October 1964, "NORAD began manning" the Cheynne Mountain Combat Operations Center. By 1965, about 250,000 United States and Canadian personnel were involved in the operation of NORAD, On 1 January 1966, Air Force Systems Command turned the COC over to NORAD The NORAD Cheyenne Mountain Complex was accepted on 8 February 1966.



United States Department of Defense realignments for the NORAD command organization began by 15 November 1968 (e.g., Army Air Defense Command (ARADCOM)) and by 1972, there were eight NORAD "regional areas ... for all air defense", and the NORAD Cheyenne Mountain Complex Improvements Program (427M System) became operational in 1979.



On at least three occasions, NORAD systems failed, such as on 9 November 1979, when a technician in NORAD loaded a test tape, but failed to switch the system status to "test", causing a stream of constant false warnings to spread to two "continuity of government" bunkers as well as command posts worldwide. On 3 June 1980, and again on 6 June 1980, a computer communications device failure caused warning messages to sporadically flash in U.S. Air Force command posts around the world that a nuclear attack was taking place. During these incidents, Pacific Air Forces (PACAF) properly had their planes (loaded with nuclear bombs) in the air; Strategic Air Command (SAC) did not and took criticism, because they did not follow procedure, even though the SAC command knew these were almost certainly false alarms, as did PACAF. Both command posts had recently begun receiving and processing direct reports from the various radar, satellite, and other missile attack detection systems, and those direct reports simply did not match anything about the erroneous data received from NORAD.




Following the 1979 Joint US-Canada Air Defense Study, the command structure for aerospace defense was changed, e.g., "SAC assumed control of ballistic missile warning and space surveillance facilities" on 1 December 1979 from ADCOM. The Aerospace Defense Command major command ended 31 March 1980. and its organizations in Cheyenne Mountain became the "ADCOM" specified command under the same commander as NORAD, e.g., HQ NORAD/ADCOM J31 manned the Space Surveillance Center. By 1982, a NORAD Off-site Test Facility was located at Peterson AFB. The DEW Line was to be replaced with the North Warning System (NWS); the Over-the-Horizon Backscatter (OTH-B) radar was to be deployed; more advanced fighters were deployed, and E-3 Sentry AWACS aircraft were planned for greater use. These recommendations were accepted by the governments in 1985. The United States Space Command was formed in September 1985 as an adjunct, but not a component of NORAD.



In 1989 NORAD operations expanded to cover counter-drug operations, e.g., tracking of small aircraft entering and operating within the United States and Canada. DEW line sites were replaced between 1986 and 1995 by the North Warning System. The Cheyenne Mountain site was also upgraded, but none of the proposed OTH-B radars are currently in operation.
After the September 11, 2001 attacks, the NORAD Air Warning Center's mission "expanded to include the interior airspace of North America."
The Cheyenne Mountain Realignment was announced on 28 July 2006, to consolidate NORAD's day-to-day operations at Peterson Air Force Base with Cheyenne Mountain in "warm standby" staffed with support personnel.






The NORAD command center was depicted in the satirical film Dr. Strangelove that starred Peter Sellers and in the drama Fail-Safe, which starred Henry Fonda. Both films were released in 1964; a television adaptation of Fail-Safe starring George Clooney and Richard Dreyfuss was broadcast live on CBS in 2000.
Cheyenne Mountain is a setting of the 1983 film WarGames and the "Jeremiah" and Stargate television series.
NORAD HQ is a retreat facility for NASA in the mid/late 21st century in the 2014 science fiction film Interstellar.
In the film, Sum of All Fears, the Russian military intends to destroy NORAD HQ



On December 24, 1955, a Sears department store placed an advertisement in a Colorado Springs newspaper that told children they could telephone Santa Claus and included a number for them to call. However, the telephone number printed was erroneously that of the Colorado Springs command center of NORAD's predecessor, the Continental Air Defense Command (CONAD). Colonel Harry Shoup, who was on duty that night, told his staff to give all children who called in a "current location" for Santa Claus   and a Christmas Eve tradition was born, now known as the "NORAD Tracks Santa" program. Every year on Christmas Eve, "NORAD Tracks Santa" purports to track Santa Claus as he leaves the North Pole and delivers presents to children around the world. Today, NORAD relies on volunteers to make the program possible.


