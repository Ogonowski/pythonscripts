The Foreign and Commonwealth Office (FCO), commonly called the Foreign Office, is a department of the British Government. It is responsible for protecting and promoting British interests worldwide. It was created in 1968 by merging the Foreign Office and the Commonwealth Office.
The head of the FCO is the Secretary of State for Foreign and Commonwealth Affairs, commonly abbreviated to "Foreign Secretary" (currently Philip Hammond). This position is regarded as one of the four most prestigious appointments in the Cabinet, alongside those of Chancellor of the Exchequer and Home Secretary. Together with the Prime Minister, these are the Great Offices of State.
The FCO is managed from day-to-day by a civil servant, the Permanent Under-Secretary of State for Foreign Affairs, who also acts as the Head of the Her Majesty's Diplomatic Service. This position is held by Sir Simon McDonald, who took office on 1 September 2015.



Safeguarding the UK s national security by countering terrorism and weapons proliferation, and working to reduce conflict.
Building the UK s prosperity by increasing exports and investment, opening markets, ensuring access to resources, and promoting sustainable global growth.
Supporting British nationals around the world through modern and efficient consular services.



The FCO Ministers are as follows:







Eighteenth century
The Foreign Office was formed in March 1782 by combining the Southern and Northern Departments of the Secretary of State, each of which covered both foreign and domestic affairs in their parts of the Kingdom. The two departments' foreign affairs responsibilities became the Foreign Office, whilst their domestic affairs responsibilities were assigned to the Home Office. The Home Office is technically the senior.
Nineteenth century
During the 19th century, it was not infrequent for the Foreign Office to approach The Times newspaper and ask for continental intelligence, which was often superior to that conveyed by official sources.
Twentieth century
During World War I, the Arab Bureau was set up within the British Foreign Office as a section of the Cairo Intelligence Department.



The FCO was formed in 1968, from the merger of the short-lived Commonwealth Office and the Foreign Office. The Commonwealth Office had been created only in 1966, by the merger of the Commonwealth Relations Office and the Colonial Office, and the Commonwealth Relations Office had been formed by the merger of the Dominions Office and the India Office in 1947 with the Dominions Office having been split from the Colonial Office in 1925.
The Foreign and Commonwealth Office held responsibility for international development issues between 1970 and 1974, and again between 1979 and 1997. From 1997, this became the responsibility of the separate Department for International Development.
The National Archives website contains a Government timeline to show the departments responsible for Foreign Affairs from 1945.



When David Miliband took over as Foreign Secretary in June 2007, he set in hand a review of the FCO s strategic priorities. One of the key messages of these discussions was the conclusion that the existing framework of ten international strategic priorities, dating from 2003, was no longer appropriate. Although the framework had been useful in helping the FCO plan its work and allocate its resources, there was agreement that it needed a new framework to drive its work forward.
The new strategic framework consists of three core elements:
A flexible global network of staff and offices, serving the whole of the UK Government.
Three essential services that support the British economy, British nationals abroad and managed migration for Britain. These services are delivered through UK Trade & Investment (UKTI), consular teams in Britain and overseas, and UK Visas and Immigration.
Four policy goals:
countering terrorism and weapons proliferation and their causes
preventing and resolving conflict
promoting a low carbon, high-growth, global economy
developing effective international institutions, in particular the United Nations and the European Union.

In August 2005, a report by management consultant group Collinson Grant was made public by Andrew Mackinlay. The report severely criticised the FCO's management structure, noting:
The Foreign Office could be "slow to act".
Delegation is lacking within the management structure.
Accountability was poor.
The FCO could feasibly cut 1200 jobs.
At least  48 million could be saved annually.
The Foreign Office commissioned the report to highlight areas which would help it achieve its pledge to reduce spending by  87 million over three years. In response to the report being made public, the Foreign Office stated it had already implemented the report's recommendations. [1]
In April 2006 a new executive agency was established, FCO Services, to provide corporate service functions. In April 2008 it moved to Trading Fund status so it had the ability to provide similar services which it already offers to the FCO, to other government departments and even outside businesses.
In 2009, Gordon Brown created the position of chief scientific adviser (CSA) to the FCO. The first science adviser was David C. Clary.
On 25 April 2010, the department apologised after The Sunday Telegraph obtained a "foolish" document calling for the upcoming September visit of Pope Benedict XVI to be marked by the launch of "Benedict-branded" condoms, the opening of an abortion clinic and the blessing of a same-sex marriage.
In 2012, the Foreign Office was criticised by Gerald Steinberg, of the Jerusalem-based research institute, NGO Monitor, saying that the Foreign Office and the Department for International Development to Palestinian NGOs provided more than  500,000 in funding to Palestinian NGOs which he says "promote political attacks on Israel." In response, a spokesman for the Foreign Office said,  we are very careful about who and what we fund. The objective of our funding is to support efforts to achieve a two-state solution. Funding a particular project for a limited period of time does not mean that we endorse every single action or public comment made by an NGO or by its employees. 
In September 2012, the FCO and the Canadian Department of Foreign Affairs signed a Memorandum of Understanding on diplomatic cooperation, which promotes the co-location of embassies, the joint provision of consular services, and common crisis response. The project has been criticised for further diminishing the UK's influence in Europe.




The Foreign and Commonwealth Office occupies a building which originally provided premises for four separate government departments: the Foreign Office, the India Office, the Colonial Office, and the Home Office. Construction on the building began in 1861 and finished in 1868, and it was designed by the architect George Gilbert Scott. Its architecture is in the Italianate style; Scott had initially envisaged a Gothic design, but Lord Palmerston, then Prime Minister, insisted on a classical style. English sculptors Henry Hugh Armstead and John Birnie Philip produced a number of allegorical figures ( Art ,  Law ,  Commerce , etc.) for the exterior.
In 1925, the Foreign Office played host to the signing of the Locarno Treaties, aimed at reducing tension in Europe. The ceremony took place in a suite of rooms that had been designed for banqueting, which subsequently became known as the Locarno Suite. During the Second World War, the Locarno Suite's fine furnishings were removed or covered up, and it became home to a foreign office code-breaking department.
Due to increasing numbers of staff, the offices became increasingly cramped and much of the fine Victorian interior was covered over especially after World War II. In the 1960s, demolition was proposed, as part of major redevelopment plan for the area drawn up by architect Sir Leslie Martin. A subsequent public outcry prevented these proposals from ever being implemented. Instead, the Foreign Office became a Grade 1 listed building in 1970. In 1978, the Home office moved to a new building, easing overcrowding.
With a new sense of the building's historical value, it underwent a 17-year,  100 million restoration process, completed in 1997. The Locarno Suite, used as offices and storage since the Second World War, was fully restored for use in international conferences. The building is now open to the public each year over Open House Weekend. The Foreign and Commonwealth Office is now also the main tenant of the Old Admiralty Building, at the opposite end of Horse Guards Parade.




Foreign and Commonwealth Office Main Building, Whitehall, King Charles St, London (abbreviated to KCS by FCO staff)
Old Admiralty Building, Whitehall, London (abbreviated to OAB by FCO staff)
Hanslope Park, Hanslope, Milton Keynes (abbreviated to HSP by FCO staff). Location of FCO Services, HMGCC and Technical Security Department of the UK Secret Intelligence Service)



International relations are handled centrally from Westminster on behalf of the whole of Britain and its dependencies. However, the devolved administrations also maintain an overseas presence in the European Union, the USA and China alongside British diplomatic missions. These offices aim to promote their regional economies and ensure that devolved interests are taken into account in British foreign policy. Ministers from devolved administrations can attend international negotiations when permitted by the British Government e.g. EU fisheries negotiations.
UK and devolved administration ministers meet at approximately quarterly intervals through the Joint Ministerial Committee (Europe), chaired by the Foreign Secretary to "discuss matters bearing on devolved responsibilities that are under discussion within the European Union."




Department for International Development
Foreign and Commonwealth Office migrated archives
Stabilisation Unit






Official website