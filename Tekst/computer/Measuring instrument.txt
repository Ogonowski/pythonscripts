A measuring instrument is a device for measuring a physical quantity. In the physical sciences, quality assurance, and engineering, measurement is the activity of obtaining and comparing physical quantities of real-world objects and events. Established standard objects and events are used as units, and the process of measurement gives a number relating the item under study and the referenced unit of measurement. Measuring instruments, and formal test methods which define the instrument's use, are the means by which these relations of numbers are obtained. All measuring instruments are subject to varying degrees of instrument error and measurement uncertainty.
Scientists, engineers and other humans use a vast range of instruments to perform their measurements. These instruments may range from simple objects such as rulers and stopwatches to electron microscopes and particle accelerators. Virtual instrumentation is widely used in the development of modern measuring instruments.




In the past, a common time measuring instrument was the sundial. Today, the usual measuring instruments for time are clocks and watches. For highly accurate measurement of time an atomic clock is used. Stop watches are also used to measure time in some sports.




Energy is measured by an energy meter. Examples of energy meters include:



An electricity meter measures energy directly in kilowatt hours.



A gas meter measures energy indirectly by recording the volume of gas used. This figure can then be converted to a measure of energy by multiplying it by the calorific value of the gas.




A physical system that exchanges energy may be described by the amount of energy exchanged per time-interval, also called power or flux of energy.
(see any measurement device for power below)
For the ranges of power-values see: Orders of magnitude (power).




Action describes energy summed up over the time a process lasts (time integral over energy). Its dimension is the same as that of an angular momentum.
A phototube provides a voltage measurement which permits the calculation of the quantized action (Planck constant) of light. Also see photoelectric effect.



This includes basic quantities found in classical- and continuum mechanics; but strives to exclude temperature-related questions or quantities.



Length, distance, or range meter
For the ranges of length-values see: Orders of magnitude (length)



Planimeter
For the ranges of area-values see: Orders of magnitude (area)




Buoyant weight (solids)
Overflow trough (solids)
Measuring cup (grained solids, liquids)
Flow measurement devices (liquids)
Graduated cylinder (liquids)
Pipette (liquids)
Eudiometer, pneumatic trough (gases)
If the mass density of a solid is known, weighing allows to calculate the volume.
For the ranges of volume-values see: Orders of magnitude (volume)



Gas meter
Mass flow meter
Metering pump
Water meter



Airspeed indicator
Radar gun, a Doppler radar device, using the Doppler effect for indirect measurement of velocity.
LIDAR speed gun
Speedometer
Tachometer (speed of rotation)
Tachymeter
Variometer
For the ranges of speed-values see: Orders of magnitude (speed)



Accelerometer




Balance
Automatic checkweighing machines
Katharometer
Weighing scales
Inertial balance
Mass spectrometers measure the mass-to-charge ratio, not the mass.
For the ranges of mass-values see: Orders of magnitude (mass)



Ballistic pendulum



Force gauge
Spring scale
Strain gauge
Torsion balance
Tribometer



Anemometer (used to determine wind speed)
Barometer used to measure the atmospheric pressure.
Manometer see pressure measurement
Pitot tube (used to determine speed)
Tire-pressure gauge in industry and mobility
For the ranges of pressure-values see: Orders of magnitude (pressure)



Circumferentor
Cross staff
Goniometer
Graphometer
Protractor
Quadrant
Reflecting instruments
Octant
Reflecting circles
Sextant

Theodolite



Stroboscope
Tachometer
For the value-ranges of angular velocity see: Orders of magnitude (angular velocity)
For the ranges of frequency see: Orders of magnitude (frequency)



Dynamometer
de Prony brake
Torque wrench



See also the section about navigation below.



Dumpy level
Laser line level
Spirit level



Gyroscope



Ballistic pendulum, indirectly by calculation and or gauging




Considerations related to electric charge dominate electricity and electronics. Electrical charges interact via a field. That field is called electric if the charge doesn't move. If the charge moves, thus realizing an electric current, especially in an electrically neutral conductor, that field is called magnetic. Electricity can be given a quality   a potential. And electricity has a substance-like property, the electric charge. Energy (or power) in elementary electrodynamics is calculated by multiplying the potential by the amount of charge (or current) found at that potential: potential times charge (or current). (See Classical electromagnetism and its Covariant formulation of classical electromagnetism)



Electrometer is often used to reconfirm the phenomenon of contact electricity leading to triboelectric sequences.
Torsion balance used by Coulomb to establish a relation between charges and force, see above.
For the ranges of charge values see: Orders of magnitude (charge)



Ammeter
Clamp meter
Galvanometer
D'Arsonval galvanometer



Oscilloscope allows to quantify time depended voltages
Voltmeter



Ohmmeter
Time-domain reflectometer characterizes and locates faults in metallic cables by runtime measurements of electric signals.
Wheatstone bridge



Capacitance meter



Inductance meter



Electric energy meter
Electricity meter



Wattmeter
These are instruments used for measuring electrical properties. Also see meter (disambiguation).



Field mill



See also the relevant section in the article about the magnetic field.
Compass
Hall effect sensor
Magnetometer
Proton magnetometer
SQUID
For the ranges of magnetic field see: Orders of magnitude (magnetic field)



Multimeter, combines the functions of ammeter, voltmeter and ohmmeter as a minimum.
LCR meter, combines the functions of ohmmeter, capacitance meter and inductance meter. Also called component bridge due to the bridge circuit method of measurement.



Temperature-related considerations dominate thermodynamics. There are two distinct thermal properties: A thermal potential   the temperature. For example: A glowing coal has a different thermal quality than a non-glowing one.
And a substance-like property,   the entropy; for example: One glowing coal won't heat a pot of water, but a hundred will.
Energy in thermodynamics is calculated by multipying the thermal potential by the amount of entropy found at that potential: temperature times entropy.
Entropy can be created by friction but not annihilated.



A physical quantity introduced in chemistry; usually determined indirectly. If mass and substance type of the sample are known, then atomic- or molecular masses (taken from a periodic table, masses measured by mass spectrometry) give direct access to the value of the amount of substance. See also the article about molar masses. If specific molar values are given, then the amount of substance of a given sample may be determined by measuring volume, mass or concentration. See also the subsection below about the measurement of the boiling point.
Gas collecting tube gases



Electromagnetic spectroscopy
Galileo thermometer
Gas thermometer principle: relation between temperature and volume or pressure of a gas (Gas laws).
constant pressure gas thermometer
constant volume gas thermometer

Liquid crystal thermometer
liquid thermometer principle: relation between temperature and volume of a liquid (Coefficient of thermal expansion).
Alcohol thermometer
Mercury-in-glass thermometer

Pyranometer principle: solar radiation flux density relates to surface temperature (Stefan Boltzmann law)
Pyrometers principle: temperature dependence of spectral intensity of light (Planck's law), i.e. the color of the light relates to the temperature of its source, range: from about  50  C to +4000  C, note: measurement of thermal radiation (instead of thermal conduction, or thermal convection) means: no physical contact becomes necessary in temperature measurement (pyrometry). Also note: thermal space resolution (images) found in Thermography.
Resistance thermometer principle: relation between temperature and electrical resistance of metals (platinum) (Electrical resistance), range: 10 to 1,000 kelvins, application in physics and industry
solid thermometer principle: relation between temperature and length of a solid (Coefficient of thermal expansion).
Bi-metallic strip

Thermistors principle: relation between temperature and electrical resistance of ceramics or polymers, range: from about 0.01 to 2,000 kelvins ( 273.14 to 1,700  C)
Thermocouples principle: relation between temperature and voltage of metal junctions (Seebeck effect), range: from about  200  C to +1350  C
Thermometer
Thermopile is a set of connected thermocouples
Triple Point cell used for calibrating thermometers.



Thermographic camera uses a microbolometer for detection of heat-radiation.
See also Temperature measurement and Category:Thermometers. More technically related may be seen thermal analysis methods in materials science.
For the ranges of temperature-values see: Orders of magnitude (temperature)




This includes thermal capacitance or temperature coefficient of energy, reaction energy, heat flow ... Calorimeters are called passive if gauged to measure emerging energy carried by entropy, for example from chemical reactions. Calorimeters are called active or heated if they heat the sample, or reformulated: if they are gauged to fill the sample with a defined amount of entropy.
Actinometer measures the heating power of radiation.
constant-temperature calorimeter, phase change calorimeter for example an ice calorimeter or any other calorimeter observing a phase change or using a gauged phase change for heat measurement.
constant-volume calorimeter, also called bomb calorimeter
constant-pressure calorimeter, enthalpy-meter or coffee cup calorimeter
Differential Scanning Calorimeter
Reaction calorimeter
see also Calorimeter or Calorimetry



Entropy is accessible indirectly by measurement of energy and temperature.



Phase change calorimeter's energy value divided by absolute temperature give the entropy exchanged. Phase changes produce no entropy and therefore offer themselves as an entropy measurement concept. Thus entropy values occur indirectly by processing energy measurements at defined temperatures, without producing entropy.
constant-temperature calorimeter, phase change calorimeter
Heat flux sensor uses thermopiles which are connected thermocouples to determine current density or flux of entropy.



The given sample is cooled down to (almost) absolute zero (for example by submerging the sample in liquid helium). At absolute zero temperature any sample is assumed to contain no entropy (see Third law of thermodynamics for further information). Then the following two active calorimeter types can be used to fill the sample with entropy until the desired temperature has been reached: (see also Thermodynamic databases for pure substances)
constant-pressure calorimeter, enthalpy-meter, active
constant-temperature calorimeter, phase change calorimeter, active



Processes transferring energy from a non-thermal carrier to heat as a carrier do produce entropy (Example: mechanical/electrical friction, established by Count Rumford). Either the produced entropy or heat are measured (calorimetry) or the transferred energy of the non-thermal carrier may be measured.
calorimeter
(any device for measuring the work which will or would eventually be converted to heat and the ambient temperature)
Entropy lowering its temperature without losing energy produces entropy (Example: Heat conduction in an isolated rod; "thermal friction").
calorimeter



Concerning a given sample, a proportionality factor relating temperature change and energy carried by heat. If the sample is a gas, then this coefficient depends significantly on being measured at constant volume or at constant pressure. (The terminiology preference in the heading indicates that the classical use of heat bars it from having substance-like properties.)
constant-volume calorimeter, bomb calorimeter
constant-pressure calorimeter, enthalpy-meter



The temperature coefficient of energy divided by a substance-like quantity (amount of substance, mass, volume) describing the sample. Usually calculated from measurements by a division or could be measured directly using a unit amount of that sample.
For the ranges of specific heat capacities see: Orders of magnitude (specific heat capacity)



Dilatometer
Strain gauge



Thiele tube
Kofler bench
Differential Scanning Calorimeter gives melting point and enthalpy of fusion.



Ebullioscope a device for measuring the boiling point of a liquid. This device is also part of a method that uses the effect of boiling point elevation for calculating the molecular mass of a solvent.
See also thermal analysis, Heat.



This includes mostly instruments which measure macroscopic properties of matter: In the fields of solid state physics; in condensed matter physics which considers solids, liquids and in-betweens exhibiting for example viscoelastic behavior. Furthermore, fluid mechanics, where liquids, gases, plasmas and in-betweens like supercritical fluids are studied.



This refers to particle density of fluids and compact(ed) solids like crystals, in contrast to bulk density of grainy or porous solids.
Aerometer liquids
Dasymeter gases
Gas collecting tube gases
Hydrometer liquids
Pycnometer liquids
Resonant frequency and Damping Analyser (RFDA) solids
For the ranges of density-values see: Orders of magnitude (density)



Durometer



Holographic interferometer
Laser produced speckle pattern analysed.
Resonant frequency and Damping Analyser (RFDA)
Tribometer



Strain gauge all below



resonant frequency and Damping Analyser (RFDA), using the impulse excitation technique: A small mechanical impulse causes the sample to vibrate. The vibration depends on elastic properties, density, geometry and inner structures (lattice or fissures).



Cam plastometer
Plastometer



Universal Testing Machine



Grindometer



Rheometer
Viscometer



Polarimeter



Tensiometer



Tomograph, device and method for non-destructive analysis of multiple measurements done on a geometric object, for producing 2- or 3-dimensional images, representing the inner structure of that geometric object.
Wind tunnel
This section and the following sections include instruments from the wide field of Category:Materials science, materials science.






Capacitor
Such measurements also allow to access values of molecular dipoles.



Gouy balance
For other methods see the section in the article about magnetic susceptibility.
See also the Category:Electric and magnetic fields in matter



Phase conversions like changes of aggregate state, chemical reactions or nuclear reactions transmuting substances, from reactants to products, or diffusion through membranes have an overall energy balance. Especially at constant pressure and constant temperature molar energy balances define the notion of a substance potential or chemical potential or molar Gibbs energy, which gives the energetic information about whether the process is possible or not - in a closed system.
Energy balances that include entropy consist of two parts: A balance that accounts for the changed entropy content of the substances. And another one that accounts for the energy freed or taken by that reaction itself, the Gibbs energy change. The sum of reaction energy and energy associated to the change of entropy content is also called enthalpy. Often the whole enthalpy is carried by entropy and thus measurable calorimetrically.
For standard conditions in chemical reactions either molar entropy content and molar Gibbs energy with respect to some chosen zero point are tabulated. Or molar entropy content and molar enthalpy with respect to some chosen zero are tabulated. (See Standard enthalpy change of formation and Standard molar entropy)
The substance potential of a redox reaction is usually determined electrochemically current-free using reversible cells.
Redox electrode
Other values may be determined indirectly by calorimetry. Also by analyzing phase-diagrams.
See also the article on electrochemistry.



Infrared spectroscopy
Neutron detector
Radio frequency spectrometers for Nuclear magnetic resonance and for Electron paramagnetic resonance
Raman spectroscopy



An X-ray tube, a sample scattering the X-rays and a photographic plate to detect them. This constellation forms the scattering instrument used by X-ray crystallography for investigating crystal structures of samples. Amorphous solids lack a distinct pattern and are identifyable thereby.



Electron microscope
Transmission electron microscope

Optical microscope uses reflectiveness or refractiveness of light to produce an image.
Scanning acoustic microscope
Scanning probe microscope
Atomic force microscope (AFM)
Scanning electron microscope
Scanning tunneling microscope (STM)

Focus variation
X-ray microscope
See also the article on spectroscopy and the list of materials analysis methods.






Microphones in general, sometimes their sensitivity is increased by the reflection- and concentration principle realized in acoustic mirrors.
Laser microphone
Seismometer



Microphone or hydrophone properly gauged
Shock tube
Sound level meter



Antenna (radio)
Bolometer measuring the energy of incident electromagnetic radiation.
Camera
EMF meter
Interferometer used in the wide field of Interferometry
Optical power meter
Microwave power meter
Photographic plate
Photomultiplier
Phototube
Radio telescope
Spectrometer
T-ray detectors
(for lux meter see the section about human senses and human body)
See also Category:Optical devices



Polarizer



Nichols radiometer



The measure of the total power of light emitted.
Integrating sphere for measuring the total radiant flux of a light source






Crookes tube
Cathode ray tube, a phosphor coated anode



Stern Gerlach experiment



Ionizing radiation includes rays of "particles" as well as rays of "waves". Especially X-rays and Gamma rays transfer enough energy in non-thermal, (single) collision processes to separate electron(s) from an atom.



Bubble chamber
Cloud chamber
Dosimeter, a technical device realizes different working principles.
Geiger counter
Microchannel plate detector
Photographic plate
Photostimulable phosphors
Scintillation counter, Lucas cell
Semiconductor detector
proportional counter
ionisation chamber



This could include chemical substances, rays of any kind, elementary particles, quasiparticles. Many measurement devices outside this section may be used or at least become part of an identification process. For identification and content concerning chemical substances see also analytical chemistry especially its List of chemical analysis methods and the List of materials analysis methods.



Carbon dioxide sensor
chromatographic device, gas chromatograph separates mixtures of substances. Different velocites of the substance types accomplish the separation.
Colorimeter (measures absorbance, and thus concentration)
gas detector
Gas detector in combination with mass spectrometer,
mass spectrometer identifies the chemical composition of a sample on the basis of the mass-to-charge ratio of charged particles.
Nephelometer or turbidimeter
oxygen sensor (= lambda sond)
Refractometer, indirectly by determining the refractive index of a substance.
Smoke detector
Ultracentrifuge, separates mixtures of substances. In a force field of a centrifuge, substances of different densities separate.



pH meter
Saturated calomel electrode



Hygrometer measures the density of water in air
Lysimeter measures the balance of water in soil









Photometry is the measurement of light in terms of its perceived brightness to the human eye. Photometric quantities derive from analogous radiometric quantities by weighting the contribution of each wavelength by a luminosity function that models the eye's spectral sensitivity. For the ranges of possible values, see the orders of magnitude in: illuminance, luminance, and luminous flux.
Photometers of various kinds:
Lux meter for measuring illuminance, i.e. incident luminous flux per unit area
Luminance meter for measuring luminance, i.e. luminous flux per unit area and unit solid angle
Light meter, an instrument used to set photographic exposures. It can be either a lux meter (incident-light meter) or a luminance meter (reflected-light meter), and is calibrated in photographic units.

Integrating sphere for collecting the total luminous flux of a light source, which can then be measured by a photometer
Densitometer for measuring the degree to which a photographic material reflects or transmits light



Tristimulus colorimeter for quantifying colors and calibrating an imaging workflow






Headphone, loudspeaker, sound pressure gauge, for measuring an equal-loudness contour of a human ear.
Sound level meter calibrated to an equal-loudness contour of the human auditory system behind the human ear.



Olfactometer, see also the article about olfaction.






Medical thermometer, see also infrared thermometer



Blood-related parameters are listed in a blood test.
Electrocardiograph records the electrical activity of the heart
Glucose meter for obtaining the status of blood sugar.
Sphygmomanometer, a blood pressure meter used to determine blood pressure in medicine. See also Category:Blood tests




Spirometer



Capnograph



Electroencephalograph records the electrical activity of the brain






Ergometer



Body fat meter




Computed tomography
Magnetic resonance imaging
Medical ultrasonography
Radiology
Tomograph, device and method for non-destructive analysis of multiple measurements done on a geometric object, for producing 2- or 3-dimensional images, representing the inner structure of that geometric object.
See also: Category:Physiological instruments and Category:Medical testing equipment.



See also Category:Meteorological instrumentation and equipment.



See also Category:Navigational equipment and Category:Navigation. See also Category:Surveying instruments.



Radio antenna
Telescope
See also Category:Astronomical instruments and Category:Astronomical observatories.



Some instruments, such as telescopes and sea navigation instruments, have had military applications for many centuries. However, the role of instruments in military affairs rose exponentially with the development of technology via applied science, which began in the mid-19th century and has continued through the present day. Military instruments as a class draw on most of the categories of instrument described throughout this article, such as navigation, astronomy, optics and imaging, and the kinetics of moving objects. Common abstract themes that unite military instruments are seeing into the distance, seeing in the dark, knowing an object's geographic location, and knowing and controlling a moving object's path and destination. Special features of these instruments may include ease of use, speed, reliability and accuracy.



Aktograph measures and records animal activity within an experimental chamber.
Checkweigher measures precise weight of items in a conveyor line, rejecting under or overweight objects.
Densitometer measures light transmission through processed photographic film or transparent material or light reflection from a reflective material.
Force platform measures ground reaction force.
Gauge (engineering) A highly precise measurement instrument, also usable to calibrate other instruments of the same kind. Often found in conjunction with defining or applying technical standards.
Gradiometer any device that measures spatial variations of a physical quantity. For example, as done in gravity gradiometry.
Parking meter measures time a vehicle is parked at a particular spot, usually with a fee.
Postage meter measures postage used from a prepaid account.
S meter measures the signal strength processed by a communications receiver.
Sensor, hypernym for devices that measure with little interaction, typically used in technical applications.
Spectroscope is an important tool used by physicists.
SWR meter check the quality of the match between the antenna and the transmission line.
Time-domain reflectometer locates faults in metallic cables.
Universal measuring machine measures geometric locations for inspecting tolerances.



Tricorder, a multipurpose scanning device, originating from the science-fictional Star Trek series.
Sonic Screwdriver, a multifunctional device used occasionally for scanning, originating from the science-fictional Doctor Who series.


