In computer science, a NOP or NOOP (short for No Operation) is an assembly language instruction, programming language statement, or computer protocol command that does nothing.



Some computer instruction sets include an instruction whose explicit purpose is to not change the state of any of the programmer-accessible registers, status flags, or memory and which may require a specific number of clock cycles to execute. In other instruction sets, a NOP has to be simulated by executing an instruction having operands that cause the same effect (e.g., on the SPARC processor, the instruction sethi 0, %g0 is the recommended solution).
A NOP is most commonly used for timing purposes, to force memory alignment, to prevent hazards, to occupy a branch delay slot, or as a place-holder to be replaced by active instructions later on in program development (or to replace removed instructions when refactoring would be problematic or time-consuming). In some cases, a NOP can have minor side effects; for example, on the Motorola 68000 series of processors, the NOP opcode causes a synchronization of the pipeline.
Here are the characteristics of the NOP instruction for some CPU architectures:
From a hardware design point of view, unmapped areas of a bus are often designed to return zeroes; since the NOP slide behavior is often desirable, it gives a bias to coding it with the all-zeroes opcode.



NOP is sometimes used as a description for the action performed by a function or a sequence of programming language statements if the function or code has no effect (it might also be called redundant code). A common compiler optimization is the detection and removal of this kind of code. Such code may be required by the grammar of the programming language, which does not allow a blank.



In Ada, the null statement serves as a NOP. As the syntax forbids that control statements or functions could be empty the null statement must be used to specify that no action is required (thus, if the programmer forgets to write a sequence of statements the program will fail to compile).



The following is an example of a single C statement that behaves like a NOP. In practice, most compilers will not generate code for this statement:

  i+1;

This statement performs an addition and discards the result. Indeed, any statement without side effects (and that does not affect control flow, e.g., break, return) can be removed, as the result of the computation is discarded.
The simplest possible statement in C that behaves like a NOP is the so-called null statement, which is just a semi-colon in a context requiring a statement. (A compiler is not required to generate a NOP instruction in this case; typically, no instructions whatsoever would be generated.)

  ;

Alternatively, an empty block (compound statement) may be used, and may be more legible:

  {}

In some cases, such as the body of a function, a block must be used, but this can be empty. In C, statements cannot be empty   simple statements must end with a ; (semicolon) while compound statements are enclosed in {} (braces), which does not itself need a following semicolon. Thus in contexts where a statement is grammatically required, some such null statement can be used.
The null statement is useless by itself, but it can have a syntactic use in a wider context, e.g., within the context of a loop:

alternatively,

or more tersely:

(note that the last form may be confusing, as semicolon usually indicates an end of function call instruction when placed after a round parenthesis on the end of line).
The above code continues calling the function getchar() until it returns a \n (newline) character, essentially fast-forwarding the current reading location of standard input to the beginning of next line.



The Python programming language has a pass statement which has no effect when executed and thus serves as a NOP. It is primarily used to ensure correct syntax due to Python's indentation-sensitive syntax; for example the syntax for definition of a class requires an indented block with the class logic, which has to be expressed as pass when it should be empty.



The Perl language has the ellipsis statement '...' which has been added to the syntax to visually represent missing code that is to be added at a later date.



The jQuery library provides a function jQuery.noop(), which does nothing.



The Visual Basic language has a ; statement, which does nothing.



Many computer protocols, such as telnet, include a NOP command that a client can issue to request a response from the server without requesting any other actions. Such a command can be used to ensure the connection is still alive or that the server is responsive. A NOOP command is part of the following protocols (this is a partial list):
telnet
FTP
SMTP
X11
POP3
NNTP
finger
IMAP4
BitTorrent
Character encoding: the null control character
Note that unlike the other protocols listed, the IMAP4 NOOP command has a specific purpose - it allows the server to send any pending notifications to the client.
While most telnet or FTP servers respond to a NOOP command with "OK" or "+OK", some programmers have added quirky responses to the client. For example, the ftpd daemon of MINIX responds to NOOP with the message:

200 NOOP to you too!



NOPs are often involved when cracking software that checks for serial numbers, specific hardware or software requirements, presence or absence of hardware dongles, etc. This is accomplished by altering functions and subroutines to bypass security checks and instead simply return the expected value being checked for. Because most of the instructions in the security check routine will be unused, these would be replaced with NOPs, thus removing the software's security functionality without attracting any attention.



The NOP opcode can be used to form a NOP slide, which allows code to execute when the exact value of the instruction pointer is indeterminate (e.g., when a buffer overflow causes a function's return address on the stack to be overwritten).


