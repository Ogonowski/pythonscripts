The Karlqvist gap or Karlqvist Field is an electromagnetic phenomenon discovered in 1953 by Swedish engineer Olle Karlqvist that is important in magnetic storage for computers.
He discovered the phenomenon while designing a ferromagnetic surface layer to the magnetic drum memory for the BESK computer. When designing a magnetic memory store, the ferromagnetic layer must be studied to determine the variation of the magnetic field with permeability, air gap, layer thickness and other influencing factors. The problem is non-linear and extremely difficult to solve. Karlqvist's gap discovery shows that the non-linear problem could be approximated by a linear boundary value for the two-dimensional static field and the one-dimensional transient field. This linear calculation gives a first approximation, which in some case seems to be satisfactory.
Karlqvist published his discovery in the paper "Calculation of the magnetic field in ferromagnetic layer of a magnetic drum" at Royal Institute of Technology (KTH).



^ http://www.treinno.se/pers/okq/field.htm
^ http://www.treinno.se/pers/okq/index.htm
^ http://www.treinno.se/pers/okq/Calculation%20of%20the%20magnetic%20field.pdf