Nevada during World War II was a time of great change that began immediately after the Japanese attack on Pearl Harbor, Hawaii, in December 1941. The population of Nevada grew significantly, largely due to an influx of service men who were stationed at several newly built military bases. The economy also improved as the number of workers steadily increased and new jobs became available.






Mining was one of Nevada's main industries at the beginning of the World War II-era. Just like during World War I, the mines and the towns right next to them began to thrive once again due to an increase in demand for copper and silver. The increase is exemplified by the rise in production from $24,945,376 in 1938 to $43,864,107 in 1940. Following the attack on Pearl Harbor and America's entry into the war, Senator Pat McCarran and other Nevada officials campaigned successfully in Washington, D.C. to have the military build new bases, such as airfields, and other military installations within the state. The construction of new bases brought in thousands of workers from outside of Nevada and when they were finished building the workers were replaced by military personnel. Towns and cities next to the military's facilities greatly profited from the new arrivals and also grew in size. Nevada's population in 1940 was 110,247 and by 1950 it had grown to 160,083. Although this number was small compared to California, for example, it represented a 45.2% increase. The Las Vegas and Reno areas were affected most by the increase in population. Las Vegas was just a town of 8,422 people in 1940. By 1950 it had grown to 24,624, a gain of 192.4%. Reno went from a population of 21,317 in 1940 to 32,492 in 1950.
Mining and the military industries were not the only industries to benefit from the war. It was during World War II and the years immediately afterward that Nevada's gambling and tourist industries began to take off. People like Bugsy Siegel, a New York gangster, flocked to Nevada to take advandage of the growing communities. Many people visited Nevada's snow topped mountains during the war years to ski. The Red-light districts also attracted lots of people, including many service men from California, however, the military pressured Nevada's government to have all brothels closed in 1943.




Like other states in the Desert Southwest, most of the new military installations built were United States Army airbases. The state's weather, wide open spaces, railroad connections, and access to California made it an ideal location for training pilots. Las Vegas Army Airfield and Tonopah Army Airfield were created from existing airports and the military built four additional fields in 1942, including Indian Springs Field, Reno Army Airbase, and Naval Air Station Fallon. Ranges and emergency strips included the Battle Mountain Flight Strip, the Black Rock Desert Gunnery Range, Churchill Flight Strip, and Owyhee Flight Strip. Tonopah Army Air Field and Indian Springs each had five auxiliary airstrips, including Indian Springs' Forty-Mile Canyon Field and Groom Lake Field. Camp Williston, located at Boulder City, provided security for Henderson's Basic Magnesium Plant and the Hoover Dam.




Nevada did not have any prisoner of war camps or internment camps like so many of the other states, the latter because Governor Edward P. Carville refused to allow the federal government to use his state as a "dumping ground for enemy aliens." However, enemy aliens, including Japanese Americans, residing within Nevada faced new restrictions which had never been implemented before. Although little research has been done about the topic, it appears that German and Italian aliens received very few of the restrictions and little antagonism when compared to the aliens of the World War I-era. The Japanese, however, were subjected to forced removal by the military and the seizure of their property by the government. Conditions were not bad for the entire Japanese community though; not all were forced to move to relocation camps. As aliens they had to register in accordance with the law and were required to turn in all weapons and short-wave radios. Some experienced little interference with their daily lives, while others were relocated to WRA internment camps.



Army and Air Forces

Navy, Marine Corps, and Coast Guard







American Theater (1939 1945)
Arizona during World War II
Desert Training Center
Military history of the United States during World War II
New Mexico during World War II
United States home front during World War II


