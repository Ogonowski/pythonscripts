The Open Source Geospatial Foundation (OSGeo), is a non-profit non-governmental organization whose mission is to support and promote the collaborative development of open geospatial technologies and data. The foundation was formed in February 2006 to provide financial, organizational and legal support to the broader Free and open source geospatial community. It also serves as an independent legal entity to which community members can contribute code, funding and other resources, secure in the knowledge that their contributions will be maintained for public benefit.
OSGeo draws governance inspiration from several aspects of the Apache Foundation, including a membership composed of individuals drawn from foundation projects who are selected for membership status based on their active contribution to foundation projects and governance.
The foundation pursues goals beyond software development, such as promoting more open access to government produced geospatial data and completely free geodata, such as that created and maintained by the OpenStreetMap project. Education and training are also addressed. Various committees within the foundation work on implementing strategies.



The OSGeo Foundation is community driven and has an organizational structure consisting of elected members and nine directors, including the president. Software projects have their own governance structure, by requirement. see FAQ. The OSGeo community collaborates via a Wiki, Mailing Lists and IRC.




OSGeo projects include:



deegree Java framework
FDO   API (C++, .Net) between GIS application and sources; for manipulating, defining and analyzing geospatial data.
GDAL/OGR   Library between GIS application and sources; for reading and writing raster geospatial data formats (GDAL) and simple features vector data (OGR).
GeoTools   Open source GIS toolkit (Java); to enable the creation of interactive geographic visualization clients.
GEOS   A C++ port of the Java Topology Suite (JTS), a geometry model.
MetaCRS   Projections and coordinate system technologies, including PROJ.4.
PostGIS   Spatial extensions for the PostgreSQL database, enabling geospatial queries.



QGIS   Desktop GIS for data viewing, editing and analysis   Windows, Mac and Linux.
GRASS GIS   extensible GIS for image processing and analysing raster, topological vector and graphic data.
OSSIM   Libraries and applications used to process imagery, maps, terrain, and vector data.
Marble   Virtual globe and world atlas.
gvSIG   Desktop GIS for data capturing, storing, handling, analysing and deploying. Includes map editing.






MapServer   Development environment for building for and presenting map applications on the web.
Geomajas   Development software for web-based and cloud based GIS applications.
GeoServer   Allows users to share and edit geospatial data. Written in Java using GeoTools.
rasdaman   raster data manager: flexible, fast, and scalable services on multi-dimensional, spatio-temporal raster data.



GeoMoose   JavaScript Framework for displaying distributed GIS data.
Mapbender   To display, overlay, edit and manage distributed Web Map Services. Framework using PHP and JavaScript.
MapGuide Open Source   Platform for developing and deploying web mapping applications and geospatial web services. Windows-based, native file format.
MapFish   Framework for building rich web-mapping applications based on the Pylons Python web framework.
OpenLayers   AJAX library (API) for accessing geographic data layers of all kinds.



GeoNetwork opensource
pycsw   Lightweight metadata publishing and discovery using Python.



Geo for All   Network of educators promoting Open Source geospatial around the world.
OSGeo-Live   Bootable DVD, USB thumb drive or Virtual Machine containing all OSGeo software.
OSGeo4W   a binary distribution of a broad set of open source geospatial software for Windows



Community MapBuilder



OSGeo runs an annual international conference called FOSS4G   Free and Open Source Software for Geospatial. Starting as early as 2006, this event has drawn over 900 attendees (2011) and is expected to bring in even more each year. It is the main meeting place and educational outreach opportunity for OSGeo members, supporters and newcomers - to share and learn from one another in presentations, hands-on workshops and a conference exhibition.



The OSGeo community is composed of participants from everywhere in the world. As of August 2012, there were 19,160 unique subscribers to the more than 180 OSGeo mailing lists. As of September 2012, OSGeo projects were built upon over 12.7 million lines of code contributed by 657 code submitters including 301 that have contributed within the last 12 months.



The Sol Katz Award for Geospatial Free and Open Source Software (GFOSS) is awarded annually by OSGeo to individuals who have demonstrated leadership in the GFOSS community. Recipients of the award will have contributed significantly through their activities to advance open source ideals in the geospatial realm.




List of GIS software
Comparison of GIS software
Free Software
GIS Live DVD
Open Source
Open Geospatial Consortium (OGC)   a standards organization
OpenStreetMap






Open Source Geospatial Foundation Site
OSGeo Wiki
OSGeo Public Geodata Committee
OSGeo Education Committee
OSGeo Journal
Open Source GIS History