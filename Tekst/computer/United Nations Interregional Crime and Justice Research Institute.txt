The United Nations Interregional Crime and Justice Research Institute is one of the five United Nations Research and Training Institutes. The Institute was founded in 1968 to assist the international community in formulating and implementing improved policies in the field of crime prevention and criminal justice.



In 1965, the United Nations Economic and Social Council (ECOSOC) outlined in Resolution 1086 B (XXXIX) the organizational arrangements for a United Nations Social Defence Programme. In 1967 UN Secretary-General U Thant issued Bulletin ST/SGB/134 which established the United Nations Social Defence Research Institute (UNSDRI), mandated to develop "new knowledge and the application thereof in advancing policy and practice in the prevention and control of both juvenile delinquency and adult criminality" through research and technical support. In 1968, the United Nations and the Italian Government signed an agreement for the establishment of UNSDRI's Headquarters in Rome and the Institute was formally inaugurated the following year by the UN Secretary-General.
In 1989, under ECOSOC Resolution No. 1989/56, the Institute was renamed the United Nations Interregional Crime and Justice Research Institute (UNICRI). Its mandate was enlarged through the adoption of its present statute. In 2000 UNICRI moved its Headquarters from Rome to Turin.



The Institute carries out action-oriented research, training and technical cooperation programmes, with the aim of assisting governments and the international community at large in tackling the threats that crime poses to social peace, development and political stability and in fostering the development of just and efficient justice systems. UNICRI supports the formulation and implementation of improved policies in the field of crime prevention and justice, the promotion of national self-reliance and the development of institutional capabilities. The Institute works to advance the understanding of crime-related problems, supporting the respect for international instruments and standards; it facilitates the exchange and dissemination of information, cooperation in international law enforcement and judicial assistance.
UNICRI structures its activities to meet the identified needs of member states. Its programme activities arise from priorities identified by the UN Annual Crime Prevention and Criminal Justice Commission. The Institute current priorities include, inter alia, activities related to organized crime, judicial reform, juvenile justice, security and counter-terrorism, major event security, international criminal law, corruption, human trafficking, victim protection, counterfeiting, cybercrime, crimes against the environment, and drug abuse.
The Institute is a firm believer in the importance and benefits of close international and cross-regional cooperation. It encourages the sharing of information and experiences at all levels.



The United Nations Interregional Crime and Justice Research Institute has its Headquarters on the United Nations Campus in Turin, Italy. It also has a Liaison Office in Rome and offices around the world.



The UNICRI Applied Research Programme is organized into three main areas:
Emerging Crimes
Security Governance and Counter Terrorism
Training and Advanced Education.



The Emerging Crimes Unit is actively involved in efforts to reduce the impact of emerging crimes such as trafficking in human beings; corruption; crimes against the environment; counterfeiting and cybercrimes. The Emerging Crimes Unit also acts to protect those at most risk of being victimized, including women and underage victims of trafficking; youth at risk or in conflict with the law; and the victims of counterfeiting - both consumers and legitimate producers.



Projects in this area focus on terrorism prevention; security during major events; protection of vulnerable targets; prevention of radicalization; and prevention of illicit trafficking of chemical, biological, radiological and nuclear materials. Activities are specifically designed to offer Member States a comprehensive range of effective strategies to prevent acts of terrorism and to enhance institutional capacity through effective security governance whilst upholding the rule of law and respect for human rights. Operating from a flexible and responsive cooperation platform, the Institute offers advisory services, strategic assistance and action-oriented analysis across a wide spectrum of security topics. UNICRI cooperates with other entities of the United Nations and many other international and regional organizations to deliver coordinated response packages. UNICRI is a member of The United Nations Counter Terrorism Implementation Task Force (CTITF) which has a remit to implement the terms of the 2006 UN Global Counter-Terrorism Strategy. Within the CTITF framework of activity, the Institute jointly leads two working groups, the first of which seeks to enhance the protection of vulnerable targets through the development of public/private initiatives. The second addresses issues surrounding radicalisation and extremism that lead to terrorism.



As part of its commitment to sharing knowledge between nations around the world, UNICRI provides many training programmes and educational courses on issues relating to crime prevention and criminal justice. It maintains partnerships with many universities and educational institutes to assist in running its courses. Courses offered include:
Master of Laws in International Crime and Justice (LL.M.)
International Summer School on Migration - Challenges and Opportunities for Europe
A special focus is given to education and training in developing countries, with support given to academic and para-academic institutions to assist them in the development of activities and initiatives designed to benefit the public at large. UNICRI also provides tailor-made, specialized training programmes for judicial personnel at the request of Member States.



The centre is a major resource of crime-prevention related material and is available to researchers worldwide for use in the development and implementation of technical interventions and research. It also supports training activities and organizes initiatives targeting the general public. The activities of the centre include the collection, analysis and dissemination of legislative, statistical and bibliographic documents in the library. The library holds more than 18,000 monographs; 1,250 journals and yearbooks; tens of thousands of documents produced by the United Nations System and other organizations; multimedia material; and grey literature on crime prevention and criminal justice issues. The centre offers on-line access to all its services, such as the library catalogue, the Criminological Thesaurus, bibliographic databases, directories, full-text articles, and abstracts of monographs.



UNICRI s Liaison Office in Rome liaises with the host country, the Diplomatic Corps, governmental and non governmental organizations, academic, media and civil society to promote UN goals and objectives. The office supports the Regional United Nations Information Centre for Europe and provides support to the Secretary General and other senior United Nations officials during official visits to Italy. The office maintains its own website in Italian (www.onuitalia.it)which publishes news related to the UN System.



UNICRI is entirely financed from voluntary contributions and does not receive any funding from the regular budget of the United Nations. UNICRI is able to implement its programmes thanks to the generosity of a number of Member States and other donors (international and regional organizations, charities, foundations etc.).



UNICRI cooperates with a number of official partners including national government agencies, international organizations, non-governmental organizations and universities.






Official website