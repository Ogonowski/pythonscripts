Edwin James Turney (March 26, 1929, Brooklyn, New York - October 15, 2008) is best known as one of the founders of Advanced Micro Devices serving as the Vice President of Sales and Administration from 1969 to 1974.



Ed Turney was born in Brooklyn, New York. His early years were spent in Ridgewood and later in Huntington, Long Island. At the age of 18, he enlisted in the US Navy where he received his first training in electronics. Turney graduated 3rd in his class from the U.S. Naval School, Electronics Technicians at the U.S. Naval Training Center in Great Lakes, Illinois.



After leaving the Navy, Turney worked for a brief time back on his native Long Island for the Long Island Lighting Company. His next stop was at Philco as a TechRep Field Engineer.
Turney's career took off when, in 1969, he joined Fairchild Semiconductor as Sales Engineer. Within two years, he was promoted to Director of Sales and Marketing, Computer Market based in corporate headquarters, Silicon Valley, California.
In 1969 Fairchild Semiconductor was taken over by Motorola management. This was the impetus for the founding of Advanced Micro Devices (AMD).
AMD was founded by Jerry Sanders, John Carey, Sven Simonsen, Ed Turney, Jack Gifford and three members of Gifford's team, Frank Botte, Jim Giles and Larry Stenger.
As Vice President, Sales and Administration, Turney was responsible for worldwide sales, purchasing, material, customer service, plus numerous diversified administrative and contractual functions. Under Turney's reign, sales grew incrementally at 20% per quarter to approximately $65,000,000 from first production in April 1970 through December 1974.
Ed Turney's next position was as Vice President, Worldwide Marketing and Sales at Electronic Arrays. In Turney's two-year stint from 1974 to 1976 he increased sales from $6M to $22M propelling the company into becoming the world's largest Read-Only Memory manufacturer at the time. The company was subsequently acquired by NEC of Japan.
In December, 1976, Ed joined Intersil as Vice President, Worldwide Marketing and Sales. He had been solicited by the Board Chairmen of both companies at the time of acquisition of Intersil by Advanced Memory Systems to integrate sales and marketing for the combined companies and to formulate and execute consolidated business plans. Turney developed and implemented a multi-year, award-winning advertising campaign to create a new image for the company. He received the EFFIE Award, given for the most effective advertising commercials. At that time, Intersil was the only semiconductor company in the nation to have received the award.



Turney died on October 15, 2008, of brain cancer at age 79. He was buried on October 18 at the Gate of Heaven Cemetery, 22555 Cristo Rey Drive, Los Altos. He is buried in St. Theresa s Court next to his mother, Rose.






The Spirit of AMD by Jeffrey L. Rodengen ISBN 0-945903-21-9
Inside Intel: Andy Grove and the Rise of the World's Most Powerful Chip Company by Tim Jackson ISBN 0-452-27643-8
The Making of Silicon Valley: A One Hundred Year Renaissance by Ward Winslow and John McLaughlin ISBN 0-9649217-0-7



Intel Vs. AMD
Interview with Jerry Sanders
Jerry Sanders Oral History
USS Epperson Shipmate Registry
USS Epperson