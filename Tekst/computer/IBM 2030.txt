The IBM System/360 Model 30 was a popular IBM mainframe announced in 1964 across the world as the then least powerful of the System/360s   the first line of computers in the world to allow machine language programs to be written that could be used across a broad range of compatible sizes.



The Model 30 had a maximum main storage of 64K bytes; its CPU used an 8-bit microarchitecture with only a few hardware registers; everything that the programmer saw was emulated by the microprogram.
It was little publicized, but there were two models of the Model 30, known (on the rare occasions when they were distinguished at all) as the 30-1 and the 30-2. The original 30-1 had a 2.0 microsecond storage cycle. Later, it was replaced by the 1.5-microsecond 30-2, although the 30-1 was silently retained in the sales catalog. The two were cosmetically different; the 30-1 looked like other System/360 models, with indicator lamps exposed on the front panel and labeled, but the 30-2 took a retrograde design step, putting the lights behind a stencil, as they had been on pre-360 machines like the 1401.



A typical, early Model 30 system consisted of:
IBM 2030 Central Processing Unit (64k storage)
with or without IBM 1401 System emulation feature
IBM 1052 Console
IBM 1442 Card Reader/Punch
IBM 1403 Printer
IBM 2401 Tape Drive (x 2)
IBM 2841 DASD (disk) Controller
IBM 2311 Disk Drives (x 2)
Using IBM 1316 Disk Pack



The IBM operating system used was usually the realistically sized DOS/360, rather than the larger OS/360. Programming was mostly in the COBOL, RPG and Assembler languages for the commercial applications that were the predominant uses of this computer, but Fortran could also be used for the scientific and engineering applications. COBOL programs for other computers could be run after recompiling on the System/360, except the input/output sections had to be re-written to relate to the System/360 devices through the DOS/360 DTF (Define the File) macros.



With the additional Compatibility Feature hardware and Compatibility Support software under DOS/360, the IBM 1401/1440/1460 object programs could be run in the emulation mode, with little or no reprogramming. Many installations included the compatibility feature, allowing older programs to be run.



The system was designed by IBM's General Systems Division in Endicott, New York, and manufactured in Endicott and other IBM manufacturing sites outside of U.S.



IBM System/360 Model 20
IBM System/360 Model 40
IBM System/360 Model 44
IBM System/360 Model 50
IBM System/360 Model 67






Official website IBM archives