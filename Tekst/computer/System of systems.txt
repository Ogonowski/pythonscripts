System of systems is a collection of task-oriented or dedicated systems that pool their resources and capabilities together to create a new, more complex system which offers more functionality and performance than simply the sum of the constituent systems. Currently, systems of systems is a critical research discipline for which frames of reference, thought processes, quantitative analysis, tools, and design methods are incomplete. The methodology for defining, abstracting, modeling, and analyzing system of systems problems is typically referred to as system of systems engineering.



Commonly proposed descriptions not necessarily definitions of systems of systems, are outlined below in order of their appearance in the literature:
Linking systems into joint system of systems allows for the interoperability and synergism of Command, Control, Computers, Communications, and Information (C4I) and Intelligence, Surveillance and Reconnaissance (ISR) Systems: description in the field of information superiority in modern military.
System of systems are large-scale concurrent and distributed systems the components of which are complex systems themselves: description in the field of communicating structures and information systems in private enterprise.
System of systems education involves the integration of systems into system of systems that ultimately contribute to evolution of the social infrastructure: description in the field of education of engineers on the importance of systems and their integration.
System of systems integration is a method to pursue development, integration, interoperability, and optimization of systems to enhance performance in future battlefield scenarios: description in the field of information intensive systems integration in the military.
Modern systems that comprise system of systems problems are not monolithic, rather they have five common characteristics: operational independence of the individual systems, managerial independence of the systems, geographical distribution, emergent behavior and evolutionary development: description in the field of evolutionary acquisition of complex adaptive systems in the military.
Enterprise systems of systems engineering is focused on coupling traditional systems engineering activities with enterprise activities of strategic planning and investment analysis: description in the field of information intensive systems in private enterprise.
System of systems problems are a collection of trans-domain networks of heterogeneous systems that are likely to exhibit operational and managerial independence, geographical distribution, and emergent and evolutionary behaviors that would not be apparent if the systems and their interactions are modeled separately: description in the field of National Transportation System, Integrated Military and Space Exploration.
Taken together, all these descriptions suggest that a complete system of systems engineering framework is needed to improve decision support for system of systems problems. Specifically, an effective system of systems engineering framework is needed to help decision makers to determine whether related infrastructure, policy, and/or technology considerations as an interrelated whole are good, bad, or neutral over time. The need to solve system of systems problems is urgent not only because of the growing complexity of today's grand challenges, but also because such problems require large monetary and resource investments with multi-generational consequences.






While the individual systems constituting a system of systems can be very different and operate independently, their interactions typically expose and deliver important emergent properties. These emergent patterns have an evolving nature that stakeholders for these problems must recognize, analyze, and understand. The system of systems approach does not advocate particular tools, methods, or practices; instead, it promotes a new way of thinking for solving grand challenges where the interactions of technology, policy, and economics are the primary drivers. System of systems study is related to the general study of designing, complexity and systems engineering, but also brings to the forefront the additional challenge of design.
Systems of systems typically exhibit the behaviors of complex systems. But not all complex problems fall in the realm of systems of systems. Inherent to system of systems problems are several combinations of traits, not all of which are exhibited by every such problem:
Operational Independence of Elements
Managerial Independence of Elements
Evolutionary Development
Emergent Behavior
Geographical Distribution of Elements
Inter-disciplinary Study
Heterogeneity of Systems
Networks of Systems
The first five traits are known as Maier's criteria for identifying system of systems challenges. The remaining three traits have been proposed from the study of mathematical implications of modeling and analyzing system of systems challenges by Dr. Daniel DeLaurentis and his co-researchers at Purdue University.



Current research into effective approaches to system of systems problems includes:
Establishment of an effective frame of reference
Crafting of a unifying lexicon 
Developing effective methodologies to visualize and communicate complex systems 
Study of designing architecture
Interoperability 
Data distribution policies: policy definition, design guidance and verification

Formal modelling language with integrated tools platform
Study of various modeling, simulation, and analysis techniques
network theory
agent-based modeling
general systems theory
probabilistic robust design (including uncertainty modeling/management)
object oriented simulation and programming
multi-objective optimization

Study of various numerical and visual tools for capturing the interaction of system requirements, concepts, and technologies



Systems of systems, while still being investigated predominantly in the defense sector, is also seeing application in such fields as national air and auto transportation and space exploration. Other applications where it can be applied include health care, design of the Internet, software integration, and energy management.



Collaboration among wide array of organizations is helping to drive development of defining system of systems problem class and methodology for modeling and analysis of system of systems problems. There are ongoing projects throughout many commercial entities, research institutions, academic programs, and government agencies.
Major stakeholders in the development of this concept are:
Universities working on system-of-systems problems, including Purdue University, the Georgia Institute of Technology, Old Dominion University, George Mason University, the University of New Mexico, the Massachusetts Institute of Technology, Naval Postgraduate School, and Carnegie Mellon University.
Corporations active in this research such as The MITRE Corporation, BAE Systems, Northrop Grumman, Boeing, Raytheon, Thales Group, CAE, Saber Astronautics, and Lockheed Martin.
Government agencies that perform and support research in systems of systems research and applications, such as DARPA, the U.S. Federal Aviation Administration, NASA, and Department of Defense (DoD)
For example, DoD recently established the National Centers for System of Systems Engineering to develop a formal methodology for system-of-systems engineering for applications in defense-related projects.
In another example, according to the Exploration Systems Architecture Study, NASA established the Exploration Systems Mission Directorate (ESMD) organization to lead the development of a new exploration  system-of-systems  to accomplish the goals outlined by President G.W. Bush in the 2004 Vision for Space Exploration.
A number of research projects and support actions, sponsored by the European Commission, are currently in progress. These target Strategic Objective IST-2011.3.3 in the FP7 ICT Work Programme (New paradigms for embedded systems, monitoring and control towards complex systems engineering). This objective has a specific focus on the "design, development and engineering of System-of-Systems". These projects include :
T-AREA-SoS (Trans-Atlantic Research and Education Agenda on Systems of Systems), which aims "to increase European competitiveness in, and improve the societal impact of, the development and management of large complex systems in a range of sectors through the creation of a commonly agreed EU-US Systems of Systems (SoS) research agenda".
COMPASS (Comprehensive Modelling for Advanced Systems of Systems), aiming to provide a semantic foundation and open tools framework to allow complex SoSs to be successfully and cost-effectively engineered, using methods and tools that promote the construction and early analysis of models.
DANSE (Designing for Adaptability and evolutioN in System of systems Engineering), which aims to develop "a new methodology to support evolving, adaptive and iterative System of Systems life-cycle models based on a formal semantics for SoS inter-operations and supported by novel tools for analysis, simulation, and optimisation".
ROAD2SOS (Roadmaps for System-of-System Engineering), aiming to develop "strategic research and engineering roadmaps in Systems of Systems Engineering and related case studies".
DYMASOS (DYnamic MAnagement of physically-coupled Systems Of Systems), aiming to develop theoretical approaches and engineering tools for dynamic management of SoS based on industrial use cases.


