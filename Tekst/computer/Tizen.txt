Tizen (/ ta z n/) is an operating system based on the Linux kernel and the GNU C Library implementing the Linux API. It targets a very wide range of devices including smartphones, tablets, in-vehicle infotainment (IVI) devices, smart TVs, PCs, smart cameras, wearable computing (such as smartwatches), Blu-ray players, printers and smart home appliances (such as refrigerators, lighting, washing machines, air conditioners, ovens/microwaves and a robotic vacuum cleaner). Its purpose is to offer a consistent user experience across devices. Tizen is a project within the Linux Foundation and is governed by a Technical Steering Group (TSG) composed of Samsung and Intel among others.
The Tizen Association was formed to guide the industry role of Tizen, including requirements gathering, identifying and facilitating service models, and overall industry marketing and education.
Members of the Tizen Association represent major sectors of the mobility industry, from numerous areas of the world. Current members include telecommunications network operators and electronics manufacturers: Fujitsu, Huawei, Intel, KT, NEC Casio, NTT DoCoMo, Orange , Panasonic, Samsung, SK Telecom, Sprint and Vodafone. While the Tizen Association decides what needs to be done in Tizen, the Technical Steering Group determines what code is actually incorporated into the operating system to accomplish those goals. Tizen roots back to the Samsung Linux Platform (SLP) and the LiMo Project and in 2013 Samsung merged its homegrown Bada project into Tizen.
The first week of October 2013, Samsung's NX300M smart camera became the first consumer product based on Tizen; it was sold in South Korea for a month before its OS was revealed at the Tizen Developer Summit, then became available for pre-order in the United States in early 2014 with a release date of March 1. The first Tizen tablet was announced by Systena in June 2013, a 10-inch quad-core ARM with 1920 1200 resolution that was eventually shipped in late October 2013 as part of a development kit exclusive to Japan. The Samsung Gear 2 smartwatch, released in April 2014, uses Tizen.
Samsung released the Tizen-based Samsung Z1 to the Indian market in January 2015.






The Tizen Association was formed to guide the industry role of Tizen, including gathering requirements, identifying and facilitating service models and overall industry marketing and education.
Tizen provides application development tools based on the JavaScript libraries jQuery and jQuery Mobile. Since version 2.0, a native application framework is also available, based on an Open Services Platform from the Bada platform.
The software development kit (SDK) allows developers to use HTML5 and related web technologies to write applications that run on supported devices.
oFono is the telephony stack
Smack is utilized to sandbox HTML5 web applications.
Windowing system
The X Window System with the Enlightenment Foundation Libraries are used.
Wayland: Tizen up to 2.x supports Wayland in in-vehicle infotainment (IVI) setups and from 3.0 onward defaults to Wayland.

ZYpp was chosen as package management system (PMS)
ConnMan was chosen over NetworkManager



The Core Mobile Web Platform Community Group (Coremob) brings developers, equipment manufacturers, browser vendors and operators together to agree on core features that developers can depend on.
HTML5 applications run on Tizen, Android, Firefox OS, Ubuntu Touch, Windows Phone, and webOS without a browser.
In late January 2013, Tizen 2.0 scored highest at the time in an HTML5 test of any browsers. As the old HTML5 tests were phased out on November 13, 2013, Tizen 2.2 fell below BlackBerry 10.2 at 494 out of 555 points. However, as of December 2013 desktop browsers had regained the advantage, and results for Tizen 2.2 on a Samsung device score highest overall in mobile, with a score of 497 points.
Tizen IVI (In-Vehicle Infotainment) is an operating system from the Automotive Grade Linux Workgroup. It is PC-compatible.
Applications based on Qt, GTK+ and EFL frameworks can run on Tizen IVI. While there is no official support for these third-party frameworks, according to the explanation on the Tizen SDK website, Tizen applications for mobile devices can be developed without relying on an official Tizen IDE as long as the application complies with Tizen packaging rules. In May 2013, a community port of Qt to Tizen focused on delivering native GUI controls and integration of Qt with Tizen OS features for smartphones. Based on the Qt port to Tizen, Tizen and mer can interchange code.



Tizen 2.x licensing model needs a thought due to the global smartphone market. While Apple has pursued patent litigation and even transferred some to known trolls to pursue Tizen partners (HTC, LG, Samsung, and more), by early 2014 cross-licensing among hardware manufacturers was happening more broadly. Extending open source software and patenting the extension is an option that most open source licenses do not restrict.
Tizen's open governance model was created through public input, suggestions, criticism, or participation, for Tizen 3.0.
The operating system consists of many open source components. A number of components internally developed by Samsung (e.g., boot animation, calendar, task manager, music player applications) are, however, released under the Flora License, essentially a BSD- or Apache-style license except granting patents to "Tizen Certified Platform" only.
Flora is not approved by the Open Source Initiative. Therefore, it is unclear whether developers can legally use the native application framework and its graphical components to make GPL applications. Source code access is guaranteed however.
Its SDK is built on top of open source components, but the entire SDK has been published under a non-open-source Samsung license.




Tizen comes from a long history of Linux adoption by manufacturers. A complete family tree is available.
Samsung's collaboration with the EFL project, and especially Carsten Haitzler, was known as LiMo for years. It was renamed Tizen when Intel joined the project in September 2011, after leaving the MeeGo project. A common misconception is that Tizen is a continuation of MeeGo. In fact, it builds on Samsung Linux Platform (SLP), a reference implementation delivered within LiMo.
On January 1, 2012, the LiMo Foundation was renamed Tizen Association. The Tizen Association is led by a Board of Directors from Samsung, Intel, Huawei, Fujitsu, NEC, Panasonic, KT Corporation, Sprint Corporation, SK Telecom, Orange, NTT DoCoMo, and Vodafone. The Tizen Association works closely with the Linux Foundation, which supports the Tizen open source project.
On April 30, 2012, Tizen released version 1.0, code-named Larkspur.
On May 7, 2012, American wireless carrier Sprint Nextel (now Sprint Corporation) announced it had agreed to become part of the Tizen Association and planned to include Tizen-powered devices in their future lineup.
On September 16, 2012, the Automotive Grade Linux Workgroup announced it will work with the Tizen project as the reference distribution optimized for a broad set of automotive applications such as instrumentation cluster and in-vehicle-infotainment (IVI).
On September 25, 2012, Tizen released version 2.0 alpha, code-named Magnolia. It offered an enhanced web-based framework with more features, better HTML5/W3C API support and more device APIs, multi-process WebKit2-based web runtime and better security for web applications. Support for OpenGL ES has been enhanced. Newly added platform SDK has been provided to help with platform development based on Open Build Service (OBS).
On February 18, 2013, Tizen released version 2.0, code-named Magnolia. Apart from further enhancements of the web frameworks and APIs, native application framework with integrated development environment and associated tools have been added supporting features such as background applications, IP push, and text-to-speech. Inclusion of this framework is an effect of the expected merging parts of the Open Services Platform (OSP) framework and APIs of the Bada operating system with the Tizen platform.
In April 2013, Samsung announced Tizen Port-a-thon. This campaign supports Bada developers' early entry into the Tizen market by providing technical support and incentives.
On May 17, 2013, Tizen released version 2.1, code-named Nectarine.
In July 2013, Samsung announced Tizen App Challenge, with over US$4 million in cash prizes.
On July 22, 2013, Tizen released version 2.2.
On November 9, 2013, Tizen released version 2.2.1.
On May 14, 2014, it was announced that Tizen:Common would ship with Qt integrated. This marks the ability for Tizen to support Qt native apps.
On November 8, 2014, Tizen released version 2.3.



Galaxy Gear smartwatches use Tizen as their operating system.
Allegedly, the first Tizen devices were planned for the second half of 2012. Samsung then clarified that first quarter of 2013 was not a date of actual product launch, but of demonstrations at Mobile World Congress. Tizen services made by Samsung were said to ship in 2013, perhaps in August or September, then replaced to "Later in 2013", and then perhaps in early 2014.
In May 2013, Samsung released the firmware source code for their NX200 and NX300 cameras, the architecture of the code being based on Tizen.
In February 2014, Samsung unveiled the Samsung Gear 2 and Gear 2 Neo smartwatches, running Tizen instead of Android (Android was used in the original Samsung Galaxy Gear). They were released in April 2014. On May 31, 2014, Samsung released an update for the original Galaxy Gear, switching the OS from Android to Tizen.
In June 2014, Samsung planned to launch the first Tizen-based smartphone, the Samsung Z SM-Z910F, in Russia in the third quarter. However by July 2014 that launch was postponed.
Also at the Tizen Developer Conference 2014, Samsung showed a prototype of a Tizen-based Smart TV.
Samsung announced the Samsung Gear S   a smartwatch that is able to make phone calls and send SMS without a phone   on August 28, 2014.
On September 15, 2014, Samsung released the Samsung NX1, a high-end camera that also uses Tizen.
On January 14, 2015, Samsung introduced a low-cost smartphone using Tizen, the Samsung Z1, in India, and later Bangladesh, and in mid May 2015 indicated they would be adding Sri Lanka.


