Fortora Fresh Finance is personal finance software developed by Fortora LLC. The software is available in two versions, one for Windows and another for Mac OS X. The software tracks accounts, loans, bills, investments and budgets. It imports data from QIF, OFX, QFX and CSV files. Fortora Fresh Finance has the ability to use its financial data files interchangeably between the Mac and Windows versions of the software, thus making the software a viable choice for people who use both PCs and Macs. Fortora Fresh Finance can also manage accounts from multiple currencies, and automatically track exchange rates and transfers between foreign accounts.
Reviewers concluded that it was a simple product and very easy to set up. It lacks any features for tax, and has no direct support for UK banks apart from simply importing bank statements.



Comparison of accounting software






Fortora Fresh Finance for Mac
Fortora Fresh Finance for Windows