This is a comparison of published free software licenses and open-source licenses. The comparison only covers software licenses with a linked article for details, approved by at least one expert group at the FSF, the OSI, the Debian project, or the Fedora project. For a list of licenses not specifically intended for software, see List of free content licenses.



The terms "free software licenses" and "open-source licenses" are usually interchangeable. While there is no one universally agreed-upon definition of free software, various groups maintain approved lists of licenses. The Open Source Initiative is one such organization keeping a list of "open source" licenses. The Free Software Foundation maintains a list of what it considers free.
Free Software provides the user's 4 essentials freedoms to use the program, to study and modify it, to copy it, and redistribute it for any purpose, while open-source criteria focuses only on the availability of the source code.



The following table compares various features of each license and is a general guide to the terms and conditions of each license.



This table lists for each license what organizations from the free software community have approved it   be it as a "free software" or as an "open source" license   and how those organizations categorize it. Organizations usually approve specific versions of software licenses. FSF approval means that the Free Software Foundation (FSF) considers a license to be free software license as it grants the four essentials freedoms, however they also recommend that licenses should be at least "Compatible with GPL" and preferably copyleft (usually).



Free software
Free software license
Open-source software


