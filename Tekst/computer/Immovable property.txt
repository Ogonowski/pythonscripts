Immovable property is an immovable object, an item of property that cannot be moved without destroying or altering it - property that is fixed to the earth, such as land or a house. In the United States it is also commercially and legally known as real estate and in Britain as property. It is known by other terms in other countries of the world.
Immovable property includes premises, property rights (for example, inheritable building right), houses, land and associated goods, and chattels if they are located on, or below, or have a fixed address. It is delimited by geographic coordinates or by reference to local landmarks, depending on the jurisdiction.
In much of the world's civil law systems (based as they are on Romano-Germanic law, which is also known as Civil law or Continental law), immovable property is the equivalent of "real property"; it is land or any permanent feature or structure above or below the surface.
To describe it in more detail, immovable property includes land, buildings, hereditary allowances, rights to way, lights, ferries, fisheries or any other benefit which arises out of land, and things attached to the earth or permanently fastened to anything which is attached to the earth. It does not include standing timber, growing crops, nor grass. It includes the right to collect rent, life interest in the income of the immovable property, a right of way, a fishery, or a lease of land.
Other sources describe immovable property as "any land or any building or part of a building, and includes, where any land or any building or part of a building is to be transferred together with any machinery, plant, furniture, fittings or other things, such machinery, plant, furniture, fittings and other things also. Any rights in or with respect to any land or any building or part of building (whether or not including any machinery, plant, furniture, fittings or other things therein) which has been constructed or which is to be constructed, accruing or arising from any transaction (whether by way of becoming a member of, or acquiring shares in, a co-operative society, or other association of persons) or by way of any agreement or any arrangement of whatever nature, not being a transaction by way of sale, exchange or lease of such land, building or part of a building."
Immovable property cannot be altered or remodeled, added to, or reconstructed without entering into an agreement with and getting permission from its owner. Construction, alteration, and demolition may also be subject to government regulation, such as the need to obey zoning laws and obtain building permits.
Also, a property or an object, which can be moved by destroying it would be considered a "destructible property" rather than an "immovable property".



Movable property
Real property
Lesion beyond moiety






Movable and immovable property
Non-Resident Indians (NRI) in Immovable Property
Property rights in China: New property law (2007)
Income from Immovable Property (Canada)
2007 Louisiana State Senate - Property - Regular Session Highlights
FAQ - Louisiana DOR
New Japan - U.K. Tax Treaty
Unoccupied and immovable property
Peace Treaty of Versailles
The Export-Import Bank of Korea
State of Louisiana - CIVIL DISTRICT COURT FOR THE PARISH OF ORLEANS - NOTICE TO SELL MOVABLE OR IMMOVABLE PROPERTY AT PRIVATE SALE
- IMMOVABLE PROPERTY AND THE BUDGET (South Africa)
Moscow s property lawlessness
(Thai) Act on the Lease of Immovable Property for Commercial and Industrial Purposes (Thailand)
Synopsis of French inheritance law