K. I. Sawyer Air Force Base is a decommissioned U.S. Air Force installation in Marquette County, Michigan, southeast of the city of Marquette. Near the center of Michigan's Upper Peninsula, the base operated for nearly forty years and closed in 1995. The county airport, Sawyer International, now occupies a portion of the base and has scheduled airline flights and some general aviation activity. The area of the former base is now an unincorporated community and a census-designated place for statistical purposes known as K. I. Sawyer AFB



The origins of K.I. Sawyer Air Force Base begin in the mid-1930s when Kenneth Ingalls Sawyer (1884 1944), a civil engineer and Marquette County road commissioner, desired to build an airport which would aid the development of the Marquette area. The one factor which most likely influenced the establishment and final location of the airport was the growing mining industry in the local area. As the demand for iron ore increased, the need for travel increased. By 1937, the demand reached a point of air service necessity. To support this demand, an airport was built southwest of Marquette, near the city of Negaunee.
The population of the area continued to grow, and, by 1940, Sawyer realized the new Negaunee airport could not handle its ever increasing demands. Therefore, in 1941, the then-superintendent of the county highway department stepped out of his role and presented a plan for a new airport. The most significant landmarks on his proposed site were a hill of sand and a wealth of blueberry patches. The board agreed with the idea. Shortly thereafter, the United States was drawn into World War II. Local concern for the protection of the Great Lakes inter-lake navigation locks named Soo Locks at the trans-border area of Sault Ste. Marie prompted area citizens to propose that the U.S. Army Air Forces take over the new airfield. At that time, the proposal was shelved.



Sawyer died in 1944, and the following year the airfield, consisting of a single airstrip, was completed and named K.I. Sawyer Airport in his honor. It was used by private fliers until 1948 when Nationwide Airlines became the first commercial airline to operate flights out of the airport. Nationwide flew shuttles to Detroit from Marquette, with stops in Escanaba, Iron Mountain, and Menominee.
In February 1954, the U.S. Air Force announced plans to enlarge its forces, and new bases were needed. The government entered into negotiations with the county for Sawyer's lease, and offered to build a $12 million jet base which could be jointly used by the Air Force and Marquette-area citizens. The major stipulation made by the government was no more than 300 landings per month could be made by civilian aircraft. In January 1955, a public meeting was held in the county courthouse to discuss the proposed lease. Many at this meeting voted to adopt a resolution urging the Marquette City Commission to accept the government's offer.



On 24 Jan 1955, the U.S. government signed a 99-year lease. Almost immediately, construction of military support facilities began. About 850 people were employed during the construction of the base and another 262 were actively employed during the winter months. A letter from Secretary of the Air Force Harold E. Talbott to U.S. Senator Homer S. Ferguson was printed in The Mining Journal and reported more than 900 military personnel were to be stationed at K.I. Sawyer Air Force Base with an annual income in excess of $3 million. K.I. Sawyer Airport officially opened as a joint civil-military facility on 8 April 1956, and it was not long before the Air Force proposed a $447,000 appropriation for construction of a civil airport in Negaunee Township, which would allow the Air Force sole use of the base.
Air Force facilities constructed included a 12,366-foot-long (3,770 m) and 300-foot-wide (90 m) runway. Additionally, the region's wide open airspace offers ease of operations for an extremely safe and delay free air traffic setting. The official opening of K.I. Sawyer AFB occurred on 8 May 1959, at which time the airfield became a strictly military operation.




Upon activation, K.I. Sawyer was placed under the Jurisdiction of Air Defense Command (ADC) on 8 April 1956. The 473d Fighter Group was activated as the host unit, under the ADC 4706th Air Defense Wing at O'Hare International Airport, Illinois.
The initial mission of the base was to act as a fighter-interceptor defense against an enemy bomber attack, the 484th Fighter-Interceptor Squadron was activated on 8 June 1956, and authorized F-89D Scorpion interceptors, however the 484th FIS was never equipped or manned as much construction was necessary to bring the airport up to Air Force standards for a military airfield. In 1957, civilian commercial operations from the base moved to a new Marquette County Airport erected at the site of the old Negaunee Airport. The first aircraft assigned to K. I. Sawyer were F-102 Delta Daggers from the 438th Fighter-Interceptor Squadron at Kinross AFB, which were temporarily stationed at K.I. Sawyer during the summer of 1958.
On 8 November 1958 the Sault Sainte Marie Air Defense Sector (SsmADS) was established at K.I. Sawyer AFB. The ADC command and control organization's mission was to operate the Semi Automatic Ground Environment (SAGE) Data Center (DC-14), which opened in 1959. The SAGE system was a network linking Air Force (and later FAA) General Surveillance Radar stations into a centralized center for Air Defense, intended to provide early warning and response for a Soviet nuclear attack. SsmADS also supported the Antigo AFS (P-19) and Calumet AFS (P-16) ADC Ground-Control Intercept (GCI) and warning stations
Further expansion of the base occurred in 1959 when the runway was extended to its present 12,366 feet. Prior to the expansion, ADC replaced the 473d Fighter Group with the 56th Fighter Wing, along with its 62d Fighter-Interceptor Squadron, to Sawyer as the host unit. The 56th FW received its first aircraft, an F-101 Voodoo, in 1959, after the expanded runway was completed. The F-101s provided the SAGE unit with the required intercept and destroy capabilities that gave the base teeth.
In 1961 the 56th Fighter Wing was discontinued and the host unit at Sawyer became the Sault Sainte Marie ADS. In a reorganization in 1964, SAC assumed jurisdiction of the base and ADC phased down its presence to a tenant organization. The 62d FIS came under the command and control of the Duluth Air Defense Sector and the SsmADS was inactivated. Responsibilities for the SAGE system were switched to the Duluth ADS, (DC-10) or the Detroit Air Defense Sector (DC-06).
In 1969, Air Defense Command was redesignated as Aerospace Defense Command. In 1971, the 62d Fighter Interceptor Squadron and its F-101B Voodoo was replaced with a new squadron and aircraft. The new squadron was the 87th Fighter Interceptor Squadron (87 FIS), the "Red Bulls," which flew the F-106A Delta Dart. The 87th maintained four T-33 aircraft to provide target support for the squadron interceptors, simulating Soviet bomber tactics. In addition, they flew NORAD radar evaluation and logistic support sorties.
ADC was disestablished in 1979 and all ADC aircraft assets were transferred to Tactical Air Command (TAC) under a sub-organization known as Air Defense   Tactical Air Command (ADTAC). The 87 FIS and their F-106 aircraft remained based and maintaining a 24/7/365 alert status at Sawyer AFB. The 87th was scheduled to convert to the F-15 Eagle in 1984. However, due to budget cuts the conversion was cancelled and the squadron was scheduled for inactivation on 1 October 1985 as the continental air defense mission in the United States was increasingly transferred to the Air National Guard. When inactivated, the 87th FIS was the second-to-last F-106 squadron on active duty.
On 4 September 1985, the last three F-106 Delta Darts from the 87rh FIS departed K.I. Sawyer for AMARC at Davis-Monthan AFB at Tucson, Arizona. Most of its aircraft were later converted to QF-102 target drones and expended as aerial targets over the Gulf of Mexico as part of the "Pacer Six Program" during the late 1990s at Tyndall AFB in western Florida by the 82d Aerial Targets Squadron.




The Strategic Air Command (SAC) became a tenant organization at K.I. Sawyer AFB on 1 August 1958 with the organization of the 4042d Strategic Wing. The 4042d SW was a B-52H Stratofortress dispersal wing, a part of SAC's plan to disperse its big bombers over a larger number of bases, thus making it more difficult for the Soviet Union to knock out the entire fleet with a surprise first strike.
Before receiving the KC-135A tanker and heavy B-52H bomber aircraft of SAC, an all-weather, heavy-duty concrete runway was built, measuring 24 inches (61 cm) thick and 150 feet (46 m) wide. It was extended in 1959 from 6,000 feet (1,830 m) to 12,366 feet (3,770 m), overruns of 1,000 feet (300 m) There are also 75-foot (23 m) shoulders on each side of the runway, providing a paved width of 300 feet (90 m).
The two operational units to be assigned later to this wing were the 923d Air Refueling Squadron on 1 May 1960, and the 526th Bombardment Squadron on 1 June 1961. The refueling squadron's first KC-135A aircraft arrived 4 August 1960, and the unit was declared fully combat ready in November of that year.

In April 1961, the 923d was inactivated and the 46th Air Refueling Squadron organized and activated as a replacement unit. The first B-52H aircraft assigned to the 526th arrived at Sawyer in August 1961. On 1 February 1963, the 4042d Strategic Wing was discontinued and the 410th Bombardment Wing (Heavy) organized and activated with no change in mission, personnel or aircraft. Also on that date, the 644th Bombardment Squadron was activated replacing the 526th, and the 46th Air Refueling Squadron was reassigned to the 410th. The majority of the wing's present support squadrons were also activated on this date as well.
On 1 January 1964, SAC assumed jurisdiction of Sawyer AFB, with the 410th Bomb Wing becoming the host unit under the 4th Strategic Aerospace Division. K.I. Sawyer was one of three SAC bases in Michigan that operated the B-52: the others were Kincheloe AFB to the east, near Kinross, south of Sault Ste. Marie, and Wurtsmith AFB, in the northeast of Michigan's Lower Peninsula, near Oscoda.
The 410th and its subordinate units' mission was to operate at full readiness, and support activities included aircraft and vehicle maintenance, bombing crew and unit training, and air refueling support. The wing did not deploy bomber aircraft to Southeast Asia during the Vietnam War, as the B-52H was dedicated to strategic deterrence. However, the tanker aircraft and aircrews participated in the "Young Tiger" TDY effort, and the bomber aircrews went through RTU (Replacement Training Unit) training to fly B-52Ds out of Andersen AFB, Guam and U-Tapao RTAFB, Thailand and were active participants in many of the notable campaigns that took place in Southeast Asia such as Arc Light.
One Sawyer KC-135A (61-0313) became famous throughout the SAC community as "the glider" when it ran out of fuel on a short final approach prior to landing at its home base after flying practice approaches at nearby Kincheloe AFB to complete requalification training. The flight crew, with the exception of the instructor pilot, bailed out when the engines went quiet. The instructor pilot, who remained on board, landed the aircraft just short of the runway overrun, bounced and rolled to a stop on the runway. The aircraft was repaired and returned to service quickly and even the crew entry door (which separated from the aircraft during bailout procedures) was returned to the Air Force by a local farmer.
Other aircraft assigned to K.I. Sawyer over the years included HH-43B and HH-1H rescue helicopters of the 48th and 39th Air Rescue and Recovery Wings at Eglin AFB, Florida at the K. I. Sawyer Helicopter (later re-designated Training) Annex, 14 Mar 1975   1 Oct 1977. SAC FB-111 bombers assigned to the 509th Bombardment Wing (Medium) from Pease AFB in New Hampshire, on satellite alert at Sawyer in 1974 and 1975.
In the early 1980s, K.I. Sawyer's B-52s were modified to carry the new Boeing AGM-86 Air-Launched Cruise Missile (ALCM). The ALCM was powered by a 600 lb.s.t. Williams F107-WR-100 turbofan, which is fed by an inlet which folds out on the top of the missile. The B-52 could carry six AGM-86Bs on each of the two underwing pylons. In 1980, two B-52H crews assigned to the 644th Bomb Squadron (S-21 and S-31) were awarded the Mackay Trophy for "executing a nonstop, around-the-world mission with the immediate objective of locating and photographing elements of the Soviet Navy operating in the Persian Gulf. The Wing Commander at this time was Col G. Alan Dugard, a great leader and author of the 2011 book about the Linebacker II missions of 1972 entitled "When the Wolf Rises."



In 1993, the Base Realignment and Closure Commission of the federal government (BRAC 1993) recommended the base for closure.
The KC-135 tankers departed in October 1993 and the B-52H's were split between the two remaining B-52 bases at Barksdale and Minot; Sawyer's final B-52 left for Minot in November 1994.
The last aircraft assigned to Sawyer were six T-37 Tweet jet trainers of the 71st Flying Training Wing assigned to "Accelerated Copilot Enrichment Program" in 1977 and were later assigned to the "Companion Trainer Program" under Air Combat Command. K.I. Sawyer AFB was officially closed at the end of September 1995.



K.I. Sawyer was a favorite base among the SAC community. Although isolated and definitely northern, it was an attractive base for its pleasing North Woods location and its proximity to outdoor activities off the base, including hunting, fishing, boating, and winter sports including skiing at Marquette Mountain (known as "Cliffs Ridge" before 1982), as well as the venues on site (base lake, ski hill, and others).
While the base was sometimes referred to as "K.I. Siberia" or "The Rock" and there was an abundance of lake effect snow, it was not the bitter sub-zero temperatures and wind chills and hot summers of the tree-sparse North Dakota bases, or the confinement of the bases in the more established communities of the northeastern states. Locals maintained that the K.I. Sawyer runway was built over some of the best blueberry fields in the state. Berry patches remained on many other parts of the base, and families of aircrew members often picked them near the alert barracks and the family center.
A portion of the operational section of K.I. Sawyer AFB has been converted into Sawyer International Airport, which opened its passenger terminal for service in September 1999. It replaced the smaller Marquette County Airport, just southwest of Marquette, as the region's primary civilian airport.
In recent years, a group of local citizens interested in preserving the historical significance of the base have collected six aircraft of the types used actively at various times through the base's history to be displayed near the airport. The program is known as the "Sawyer 6" project.



Opened as: K. I. Sawyer Airport, 8 April 1956
Redesignated: K. I. Sawyer Air Force Base, 8 May 1959   30 September 1995



Air Defense Command (ADC), 8 April 1956
Strategic Air Command (SAC), 1 January 1964
Air Combat Command (ACC), 1 June 1992   30 September 1995 (Tail Code: "KI")






The elevation at the passenger terminal is 1,190 feet (363 m) above sea level, about 600 feet (180 m) above Lake Superior, fifteen miles (24 km) north.
The former base is located in the southeast corner of the township of Sands, with small portions in West Branch and Forsyth. The community of Gwinn is approximately three miles (5 km) south-southwest.
It is a census-designated place (CDP) for statistical purposes, and as of the 2000 census, the CDP population was 1,443.
According to the United States Census Bureau, the CDP has a total area of 8.5 square miles (22 km2), of which 8.4 square miles (22 km2) is land and 0.04 square miles (0.10 km2) (0.24%) is water.



As of the census of 2000, there were 1,443 people, 501 households, and 360 families residing in the CDP. The population density was 171.0 inhabitants per square mile (66.0/km ). There were 1,659 housing units at an average density of 196.6 per square mile (75.9/km ). The racial makeup of the base was 90.23% White, 0.69% African American, 3.47% Native American, 0.35% Asian, 1.18% from other races, and 4.09% from two or more races. Hispanic or Latino of any race were 2.15% of the population.
There were 501 households out of which 55.1% had children under the age of 18 living with them, 45.9% were married couples living together, 19.6% had a female householder with no husband present, and 28.1% were non-families. 17.8% of all households were made up of individuals and 2.2% had someone living alone who was 65 years of age or older. The average household size was 2.88 and the average family size was 3.27.
In the CDP, the population was spread out with 38.5% under the age of 18, 12.3% from 18 to 24, 37.8% from 25 to 44, 9.1% from 45 to 64, and 2.3% who were 65 years of age or older. The median age was 25 years and for every 100 females, there were 92.7 males. For every 100 females age 18 and over, there were 93.0 males.
The median income for a household in the CDP was $26,550, and the median income for a family was $26,979. Males had a median income of $27,679 versus $18,333 for females. The per capita income for the CDP was $10,029. About 24.4% of families and 26.6% of the population were below the poverty line, including 30.6% of those under age 18 and 31.3% of those age 65 or over.




List of USAF Aerospace Defense Command General Surveillance Radar Stations



 This article incorporates public domain material from websites or documents of the Air Force Historical Research Agency.

Maurer, Maurer. Air Force Combat Units of World War II. Washington, DC: U.S. Government Printing Office 1961 (republished 1983, Office of Air Force History, ISBN 0-912799-02-1).
Ravenstein, Charles A. Air Force Combat Wings Lineage and Honors Histories 1947 1977. Maxwell Air Force Base, Alabama: Office of Air Force History 1984. ISBN 0-912799-12-9.
Mueller, Robert (1989). Volume 1: Active Air Force Bases Within the United States of America on 17 September 1982. USAF Reference Series, Office of Air Force History, United States Air Force, Washington, D.C. ISBN 0-912799-53-6, ISBN 0-16-002261-4
A Handbook of Aerospace Defense Organization 1946   1980, by Lloyd H. Cornett and Mildred W. Johnson, Office of History, Aerospace Defense Center, Peterson Air Force Base, Colorado
Winkler, David F. (1997), Searching the skies: the legacy of the United States Cold War defense radar program. Prepared for United States Air Force Headquarters Air Combat Command.
Information K. I. Sawyer AFB, MI
Strategic-Air-Command.com   K.I. Sawyer AFB history
K.I. Sawyer Air Heritage Museum   base history



Sawyer International Airport   official site
MSRmaps.com   K.I. Sawyer AFB   USGS topo map (& aerial photo)
K.I Sawyer Air Heritage Museum   home page
Mike's K.I. Sawyer AFB tribute
Birthplace of the Center line, K.I. Sawyer as inventor
Resources for this airport:
FAA airport information for SAW
AirNav airport information for KSAW
ASN accident history for SAW
FlightAware airport information and live flight tracker
NOAA/NWS latest weather observations
SkyVector aeronautical chart, Terminal Procedures