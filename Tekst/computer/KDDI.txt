KDDI Corporation (KDDI , KDDI Kabushiki Gaisha) (TYO: 9433) is a Japanese telecommunications operator formed in October 1, 2000 through the merger of DDI Corp. (Daini-Denden Inc.), KDD (Kokusai Denshin Denwa) Corp., and IDO Corp. It has its headquarters in the Garden Air Tower in Iidabashi, Chiyoda, Tokyo.
KDDI provides mobile cellular services using the "au by KDDI" brand. ISP network and solution services are provided under the au one net brand, while "au Hikari" is the name under which long-distance and international voice and data communications services and Fiber to the Home (FTTH) services are marketed. ADSL broadband services carry the brand name "ADSL One", and IP telephony over copper is branded as "Metal Plus".




On April 1, 2002, au by KDDI launched 3G networks using CDMA2000 1x technology.
On November 28, 2003, au by KDDI launched EV-DO Rev 0 service in the "CDMA 1X WIN" brand, and revolutionized Japan's mobile telecommunication industry by introducing fixed rate data subscription plans at a data rate of 2.4 Mbit/s.
On December 2006, au by KDDI became the first carrier to provide an EV-DO Rev A service at a data rate of 3.1Mbit/s (downlink), 1.8Mbit/s (uplink).
au by KDDI has been very successful with its EZ wireless data services, EZweb, EZweb@mail, EZappli, EZchakuuta, ezmovie, and EZnaviwalk (GPS), using the advanced WAP technology. It supports both Java ME and BREW application environments.
In November 2004 au by KDDI introduced the music include ringtone download service Chaku Uta Full (music ringtone full), for download of full length songs to mobile phones. Within six months from introduction, on June 15, 2005, customers had downloaded 10 million full length Chaku Uta Full songs.
As of end of June 2005, au by KDDI has 20,122,700 customers, among which 18,723,200 (93%) are 3G CDMA2000 subscribers. It is Japan's second-largest cellular operator with an increasing 20.0% market share.
On January 26, 2006, the first pointing local search application, Mapion Local Search - Powered by GeoVector, was launched on the KDDI network on their GPS and compass equipped handsets. KDDI announced that it will collaborate with the Taiwanese manufacturer HTC Corp. to sell the mobile phone HTC J in Japan starting May 2012. The HTC J mobile phone will feature the Android 4.0 operating system.
KDDI together with Sumitomo Group signed an agreement with Myanmar State owned Myanmar Post and Telecommunication (MPT) in July 2014 to jointly operate a mobile phone service in Myanmar for next 10 years.
Consumer showroom is set in Harajuku called, "KDDI Designing Studio".
TU-KA (TU-KA by KDDI), a subsidiary company of KDDI, was a 2G PDC cellular operator in three metropolitan areas (Tokyo, Nagoya, and Osaka), which did not apply a 3G license. TU-KA was best known for having singer Ayumi Hamasaki to appear in their commercials. TU-KA was closed on March 31, 2008.
DDI Pocket, a PHS operator, was previously owned by KDDI but has now been spun off as Willcom.
In August 2014, KDDI announced it was joining forces with five other global companies, including Google to build a super-fast undersea data transmission cable linking the United States West Coast and Japan. The pipeline was expected to be completed by 2016-end.



In 2003, several class action complaints were filed againtst DDI for misrepresenting and/or failing to disclose material facts about the Company's financial results during the Class Period. The parties agreed on a $4.4 million settlement in 2006.




au
KDDI Mobile
G-Book, a telematics service provided by Toyota in Japan only
KDDI India Private Limited



^ a b c "2015 Form 10-K, KDDI". Forbes. 
^ Merger of DDI, KDD and IDO
^ "Corporate Data." KDDI. Retrieved on February 21, 2010.
^ http://www.geovector.com/press/mls.html
^ http://www.brightwire.com/news/201739-htc-and-kddi-to-sell-htc-j-smartphone-in-japan-in-late-may
^ "HTC and KDDI to sell HTC J". BrightWire. 
^ "KDDI, Sumitomo ink deal with Myanmar telecom for mobile phone service (17 July 2014)". http://english.kyodonews.jp. Kyodo News. Retrieved 17 July 2014. 
^ "KDDI, Sumitomo ink deal with Myanmar telecom for mobile phone service". http://www.japantimes.co.jp. The Japan Times. Retrieved 17 July 2014. 
^ "High-speed Undersea Cable to Link US, Asia". The Tokyo News.Net. 12 August 2014. Retrieved 13 August 2014. 
^ "Class action suit filed vs DDI". 
^ http://securities.stanford.edu/1028/DDICQOB03-01//.  



(Japanese) KDDI CORPORATION
(English) KDDI CORPORATION
About KDDI
Investor Relations

(English) Global - KDDI CORPORATION
KDDI au