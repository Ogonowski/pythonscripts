Vision is a multi-platform 3D game engine that was first launched in 2003 by the Germany-based middleware developer, Trinigy. Now in its eighth version, the Vision Engine is currently available for Microsoft Windows (DX9, DX10, DX11), Xbox 360, PlayStation 3, Nintendo Wii and Wii U, iOS, Android, Sony's PlayStation Vita, and most major browsers (IE6 and up, Firefox 2.0 and up, Google Chrome, Opera 9 and up).
Trinigy and its Vision Engine was acquired by Havok in 2011.



Vision Engine 8 was launched just prior to the 2010 Game Developers Conference.






Trinigy licenses the Vision engine on a royalty-free, per-title/ per-platform basis. All licenses include full support and regular technology updates. WebVision is free for any studio licensing Vision Engine 8.



Trinigy
Havok
List of game engines






Havok Official Website
Havok Vision Engine