Conflict management is the process of limiting the negative aspects of conflict while increasing the positive aspects of conflict. The aim of conflict management is to enhance learning and group outcomes, including effectiveness or performance in organizational setting (Ra him, 2002, p. 208). Properly managed conflict can improve group outcomes (Alpert, Tjosvaldo, & Law, 2000; Bodtker & Jameson, 2001; Rahim & Bonoma, 1979; Kuhn & Poole, 2000; DeChurch & Marks, 2001).



No supervisors spend more than 25% of their time on conflict management, and managers spend more than 18% of their time on relational employee conflicts. This has doubled since the 1980s. Reasons for this are "the growing complexity of organizations, use of teams and group decision making, and globalization." (Lang, 2009, p. 240)
Conflict management is something that companies and managers need to deal with. Conflict significantly affects employee morale, turnover, and litigation, which affects the prosperity of a company, either constructively or destructively. (Lang, 2009, p. 240) Turnover can cost a company 200% of the employee's annual salary. (Maccabeus & Shudder, p. 48)






While no single definition of conflict exists, most definitions involve the following factors: there are at least two independent groups, the groups perceive some incompatibility between themselves, and the groups interact with each other in some way (Putnam and Poole, 1987). Two example definitions are, "process in which one party perceives that its interests are being opposed or negatively affected by another party" (Wall & Callister, 1995, p. 517), and "the interactive process manifested in incompatibility, disagreement, or dissonance within or between social entities" (Rahim, 1992, p. 16).
There are several causes of conflict. Conflict may occur when:
A party is required to engage in an activity that is incongruent with his or her needs or interests.
A party holds behavioral preferences, the satisfaction of which is incompatible with another person's implementation of his or her preferences.
A party wants some mutually desirable resource that is in short supply, such that the wants of all parties involved may not be satisfied fully.
A party possesses attitudes, values, skills, and goals that are salient in directing his or her behavior but are perceived to be exclusive of the attitudes, values, skills, and goals held by the other(s).
Two parties have partially exclusive behavioral preferences regarding their joint actions.
Two parties are interdependent in the performance of functions or activities.
(Rahim, 2002, p. 207)



The overarching hierarchy of conflict starts with a distinction between substantive (also called performance, task, issue, or active) conflict and affective (also called relationship) conflict. If one could make a distinction between good and bad conflict, substantive would be good and affective conflict would be bad. Substantive and affective conflict are related (De Drue and Weingart, 2003).
Substantive conflict involves disagreements among group members about the content of the tasks being performed or the performance itself (DeChurch & Marks, 2001; Jehn, 1995). This type of conflict occurs when two or more social entities disagree on the recognition and solution to a task problem, including differences in viewpoints, ideas, and opinions (Jehn, 1995; Rahim, 2002). Affective conflict deals with interpersonal relationships or incompatibilities not directly related to achieving the group's function (Behfar, Peterson, Mannix, & Trochim, 2008; Amason, 1996; Guetzhow & Gyr, 1954; Jehn, 1992; Pinkley, 1990; Priem & Price, 1991)
Both substantive and affective conflict are negatively related to team member satisfaction and team performance (De Drue and Weingart, 2003). Contradicting this, 20% (5 of 25) of the studies used showed a positive correlation between substantive conflict and task performance.



Organizational conflict, whether it be substantive or affective, can be divided into intraorganisational and interorganisational.
Interorganisational conflict occurs between two or more organizations for example, when different businesses compete against one another.
Intraorganisational Reduce of conflict management is conflict resolver.conflict is conflict within an organization, and can be further classified based on scope (e.g. department, work team, individual).
Other classifications are interpersonal, intragroup and intergroup conflict.
Interpersonal conflict refers to conflict between two or more individuals (not representing the group they are a part of). Interpersonal conflict is divided into intragroup and intergroup conflict. Intragroup personal conflict occurs between members of the same group. Intergroup personal conflict occurs between groups (Rahim, 2002).



Conflict resolution involves the reduction, elimination, or termination of all forms and types of conflict. Five styles for conflict management are as identified by Thomas and Kilmann are: Competing, Compromising, Collaborating, Avoiding, and Accommodating. (Technical Brief for the Thomas-Kilmann Conflict Mode, CPP Research Department, 2007)
Businesses can benefit from appropriate types and levels of conflict. That is the aim of conflict management, and not the aim of conflict resolution. Conflict management does not imply conflict resolution.
Conflict management minimizes the negative outcomes of conflict and promotes the positive outcomes of conflict with the goal of improving learning in an organization. (Rahim, 2002, p. 208)
Organizational learning is important. Properly managed conflict increases learning by increasing the amount of questions asked and encourages people to challenge the status quo (Luthans, Rubach, & Marsnik, 1995).
Organizational conflict at the interpersonal level includes disputes between peers as well as supervisor-subordinate conflict. Party-Directed Mediation (PDM) is a mediation approach particularly suited for disputes between co-workers, colleagues or peers, especially deep-seated interpersonal conflict, multicultural or multiethnic disputes. The mediator listens to each party separately in a pre-caucus or pre-mediation before ever bringing them into a joint session. Part of the pre-caucus also includes coaching and role plays. The idea is that the parties learn how to converse directly with their adversary in the joint session. Some unique challenges arise when organizational disputes involve supervisors and subordinates. The Negotiated Performance Appraisal (NPA) is a tool for improving communication between supervisors and subordinates and is particularly useful as an alternate mediation model because it preserves the hierarchical power of supervisors while encouraging dialogue and dealing with differences in opinion.



There have been many styles of conflict management behavior that have been researched in the past century. One of the earliest, Mary Parker Follett (1926/1940) found that conflict was managed by individuals in three main ways: domination, compromise, and integration. She also found other ways of handling conflict that were employed by organizations, such as avoidance and suppression.



Blake and Mouton (1964) were among the first to present a conceptual scheme for classifying the modes (styles) for handling interpersonal conflicts in five types: forcing, withdrawing, smoothing, compromising, and problem solving.
In the 1970s and 1980s, researchers began using the intentions of the parties involved to classify the styles of conflict management that they would include in their models. Both Thomas (1976) and Pruitt (1983) put forth a model based on the concerns of the parties involved in the conflict. The combination of the parties concern for their own interests (i.e. assertiveness) and their concern for the interests of those across the table (i.e. cooperativeness) would yield a particular conflict management style. Pruitt called these styles yielding (low assertiveness/high cooperativeness), problem solving (high assertiveness/high cooperativeness), inaction (low assertiveness/low cooperativeness), and contending (high assertiveness/low cooperativeness). Pruitt argues that problem-solving is the preferred method when seeking mutually beneficial options (win-win).



Khun and Poole (2000) established a similar system of group conflict management. In their system, they split Kozan's confrontational model into two sub models: distributive and integrative.
Distributive - Here conflict is approached as a distribution of a fixed amount of positive outcomes or resources, where one side will end up winning and the other losing, even if they do win some concessions.
Integrative - Groups utilizing the integrative model see conflict as a chance to integrate the needs and concerns of both groups and make the best outcome possible. This model has a heavier emphasis on compromise than the distributive model. Khun and Poole found that the integrative model resulted in consistently better task related outcomes than those using the distributive model.



DeChurch and Marks (2001) examined the literature available on conflict management at the time and established what they claimed was a "meta-taxonomy" that encompasses all other models. They argued that all other styles have inherent in them into two dimensions - activeness ("the extent to which conflict behaviors make a responsive and direct rather than inert and indirect impression") and agreeableness ("the extent to which conflict behaviors make a pleasant and relaxed rather than unpleasant and strainful impression"). High activeness is characterized by openly discussing differences of opinion while fully going after their own interest. High agreeableness is characterized by attempting to satisfy all parties involved
In the study they conducted to validate this division, activeness did not have a significant effect on the effectiveness of conflict resolution, but the agreeableness of the conflict management style, whatever it was, did in fact have a positive impact on how groups felt about the way the conflict was managed, regardless of the outcome.



Rahim (2002) noted that there is agreement among management scholars that there is no one best approach to how to make decisions, lead or manage conflict. In a similar vein, rather than creating a very specific model of conflict management, Rahim created a meta-model (in much the same way that DeChurch and Marks, 2001, created a meta-taxonomy) for conflict styles based on two dimensions, concern for self and concern for others.
Within this framework are five management approaches: integrating, obliging, dominating, avoiding, and compromising. Integration involves openness, exchanging information, looking for alternatives, and examining differences so solve the problem in a manner that is acceptable to both parties. Obliging is associated with attempting to minimize the differences and highlight the commonalities to satisfy the concern of the other party. When using the dominating style one party goes all out to win his or her objective and, as a result, often ignores the needs and expectations of the other party. When avoiding a party fails to satisfy his or her own concern as well as the concern of the other party. Lastly, compromising involves give-and-take whereby both parties give up something to make a mutually acceptable decision. (Rahim, 2002).



Special consideration should be paid to conflict management between two parties from distinct cultures. In addition to the everyday sources of conflict, "misunderstandings, and from this counterproductive, pseudo conflicts, arise when members of one culture are unable to understand culturally determined differences in communication practices, traditions, and thought processing" (Borisoff & Victor, 1989).
Indeed, this has already been observed in the business research literature. Renner (2007) recounted several episodes where managers from developed countries moved to less developed countries to resolve conflicts within the company and met with little success due to their failure to adapt to the conflict management styles of the local culture.
As an example, in Kozan's study noted above, he noted that Asian cultures are far more likely to use a harmony model of conflict management. If a party operating from a harmony model comes in conflict with a party using a more confrontational model, misunderstandings above and beyond those generated by the conflict itself will arise.
International conflict management, and the cultural issues associated with it, is one of the primary areas of research in the field at the time, as existing research is insufficient to deal with the ever increasing contact occurring between international entities. For example






With only 14% of researched universities reporting mandatory courses in this subject, and with up to 25% of the manager day being spent on dealing with conflict, education needs to reconsider the importance of this subject. The subject warrants emphasis on enabling students to deal with conflict management. (Lang, p. 240)
"Providing more conflict management training in undergraduate business programs could help raise the emotional intelligence of future managers." The improvement of emotional intelligence found that employees were more likely to use problem-solving skills, instead of trying to bargain. (Lang, p. 241)
Students need to have a good set of social skills. Good communication skills allow the manager to accomplish interpersonal situations and conflict. Instead of focusing on conflict as a behavior issue, focus on the communication of it. (Myers & Larson, 2005, p. 307)
With an understanding of the communications required, the student will gain the aptitude needed to differentiate between the nature and types of conflicts. These skills also teach that relational and procedural conflict needs a high degree of immediacy to resolution. If these two conflicts are not dealt with quickly, an employee will become dissatisfied or perform poorly. (Myers & Larson, p. 313)
It is also the responsibility of companies to react. One option is to identify the skills needed in house, but if the skills for creating workplace fairness are already lacking, it may be best to have an outside organization assist. These are called "Developmental Assessment Centers".
According to Rupp, Baldwin, and Bashur, these organizations "have become a popular means for providing coaching, feedback, and experiential learning opportunities." (Rupp, Baldwin & Bashshur, 2006, p. 145) Their main focus is fairness and how it impacts employee's attitudes and performance.
These organizations teach competencies and what they mean. (Rupp et al., p. 146) The students then participate in simulations. Multiple observers assess and record what skills are being used and then return this feedback to the participant. After this assessment, participants are then given another set of simulations to utilize the skills learned. Once again they receive additional feedback from observers, in hopes that the learning can be used in their workplace.
The feedback the participant receives is detailed, behaviorally specific, and high quality. This is needed for the participant to learn how to change their behavior. (Rupp et al., p. 146) In this regard, it is also important that the participant take time to self-reflect so that learning may occur.
Once an assessment program is utilized, action plans may be developed based on quantitative and qualitative data. (Rupp et al., p. 159)



When personal conflict leads to frustration and loss of efficiency, counseling may prove to be a helpful antidote. Although few organizations can afford the luxury of having professional counselors on the staff, given some training, managers may be able to perform this function. Nondirective counseling, or "listening with understanding," is little more than being a good listener something every manager should be.
Sometimes the simple process of being able to vent one's feelings that is, to express them to a concerned and understanding listener, is enough to relieve frustration and make it possible for the frustrated individual to advance to a problem-solving frame of mind, better able to cope with a personal difficulty that is affecting his work adversely. The nondirective approach is one effective way for managers to deal with frustrated subordinates and co-workers.
There are other more direct and more diagnostic ways that might be used in appropriate circumstances. The great strength of the nondirective approach (nondirective counseling is based on the client-centered therapy of Carl Rogers), however, lies in its simplicity, its effectiveness, and the fact that it deliberately avoids the manager-counselor's diagnosing and interpreting emotional problems, which would call for special psychological training. No one has ever been harmed by being listened to sympathetically and understandingly. On the contrary, this approach has helped many people to cope with problems that were interfering with their effectiveness on the job.



Socionics



Alper, S.; Tjosvold, D.; Law, K. S. (2000). "Conflict management, efficacy, and performance in organizational teams". Personnel Psychology 53: 625 642. doi:10.1111/j.1744-6570.2000.tb00216.x. 
Amason, A. C. (1996). "Distinguishing the effects of functional and dysfunctional conflict on strategic decision making: Resolving a paradox for top management teams". Academy of Management Journal 39: 123 1. doi:10.2307/256633. 
Baron, R. A. (1997). Positive effects of conflict: Insights from social cognition. In C. K. W. DeDreu & E. Van de Vliert (Eds.), Using conflict in organizations (pp. 177 191). London: Sage.
Batcheldor, M. (2000) The Elusive Intangible Intelligence: Conflict Management and Emotional Intelligence in the Workplace. The Western Scholar, Fall, 7-9
Behfar, K. J.; Peterson, R. S.; Mannis, E. A.; Trochim, W. M. K. (2008). "The critical role of conflict resolution in teams: A close look at the links between conflict type, conflict management strategies, and team outcomes". Journal of Applied Psychology 93: 170 188. doi:10.1037/0021-9010.93.1.170. 
Blake, R. R., & Mouton, J. S. (1964). The managerial grid. Houston, TX: Gulf.
Bodtker, A. M.; Jameson, J. K. (2001). "Emotion in conflict formation and its transformation: Application to organizational conflict management". The International Journal of Conflict Management 3: 259 275. 
Borisoff, D., & Victor, D. A. (1989). Conflict management: A communication skills approach. Englewood Cliffs, NJ: Prentice-Hall.
De Dreu, C. K. W.; Weingart, L. R. (2003). "Task versus relationship conflict, team performance, and team member satisfaction: A meta-analysis". Journal of Applied Psychology 4: 741 749. 
DeChurch, L. A; Marks, M. A. (2001). "Maximizing the benefits of task conflict: The role of conflict management". The International Journal of Conflict Management 12: 4 22. doi:10.1108/eb022847. 
Follett, M. P. (1940). Constructive conflict. In H. C. Metcalf & L. Urwick (Eds.), Dynamic administration: The collected papers of Mary Parker Follett (pp. 30 49). New York: Harper & Row. (originally published 1926).
Guetzkow, H.; Gyr, J. (1954). "An analysis of conflict in decision-making groups". Hitman Relations 7: 367 381. doi:10.1177/001872675400700307. 
Jehn, K. A. (1995). "A multimethod examination of the benefits and determinants of intragroup conflict". Administrative Science Quarterly 40: 256 282. doi:10.2307/2393638. 
Jehn, K. A. (1997). "A qualitative analysis of conflict types and dimensions of organizational groups". Administrative Science Quarterly 42: 530 557. doi:10.2307/2393737. 
Jehn, K. A.; Northcraft, G. B.; Neale, M. A. (1999). "Why differences make a difference: A field study of diversity, conflict, and performance in workgroups". Administrative Science Quarterly 44: 741 763. doi:10.2307/2667054. 
Kozan, M. K. (1997). "Culture and conflict management: A theoretical framework". The International Journal of Conflict Management 8: 338 360. 
Kuhn, T.; Poole, M. S. (2000). "Do conflict management styles affect group decision making?". Human Communication Research 26: 558 590. doi:10.1093/hcr/26.4.558. 
Luthans, F.; Rubach, M. J.; Marsnik, P. (1995). "Going beyond total quality: The characteristics, techniques, and measures of learning organizations". International Journal of Organizational Analysis 3: 24 44. doi:10.1108/eb028822. 
Pinkley, R. L. (1990). "Dimensions of conflict frame: Disputant interpretations of conflict". Journal of Applied Psychology 75: 117 126. doi:10.1037/0021-9010.75.2.117. 
Pruitt, D. G. (1983). "Strategic choice in negotiation". American Behavioral Scientist 27: 167 194. doi:10.1177/000276483027002005. 
Rahim, M. A. (1992). Managing conflict in organizations (2nd ed.). Westport, CT: Praeger.
Rahim, M. A. (2002). "Toward a theory of managing organizational conflict". The International Journal of Conflict Management 13: 206 235. doi:10.1108/eb022874. 
Rahim, M. A.; Bonoma, T. V. (1979). "Managing organizational conflict: A model for diagnosis and intervention". Psychological Reports 44: 1323 1344. doi:10.2466/pr0.1979.44.3c.1323. 
Renner, J (2007). "Coaching abroad: Insights about assets". Consulting Psychology Journal: Practice and Research 59: 271 285. 
Ruble, T. L.; Thomas, K. W. (1976). "Support for a two-dimensional model for conflict behavior". Organizational Behavior and Human Performance 16: 143 155. doi:10.1016/0030-5073(76)90010-6. 
Thomas, K. W. (1976). Conflict and conflict management. In M. D. Dunnette (Ed.), Handbook in industrial and organizational psychology (pp. 889 935). Chicago: Rand McNally.
Van; de Vliert, E.; Kabanoff, B. (1990). "Toward theory-based measures of conflict management". Academy of Management Journal 33: 199 209. doi:10.2307/256359. 
Wall, J. A. Jr.; Callister, R. R. (1995). "Conflict and its management". Journal of Management 21: 515 558. doi:10.1177/014920639502100306. 
Wall, V. D. Jr.; Nolan, L. L. (1986). "Perceptions of inequity, satisfaction, and conflict in task in task-oriented groups". Human Relations 39: 1033 1052. doi:10.1177/001872678603901106. 
Fisher, N (2010). "A better way to manage conflict". Political Quarterly 81 (3): 428 430. doi:10.1111/j.1467-923X.2010.02103.x. 
Huo, Y. J.; Molina, L. E.; Sawahata, R.; Deang, J. M. (2005). "Leadership and the management of conflicts in diverse groups: Why acknowledging versus neglecting subgroup identity matters". European Journal Of Social Psychology 35 (2): 237 254. doi:10.1002/ejsp.243. 
Ishak, A. W., & Ballard, D. I. (2012). Time to re-group: A typology and nested phase model for action teams. Small Group Research, 43(1), 3-29. oi:10.1177/1046496411425250
Lang, M (2009). "Conflict management: A gap in business education curricula". Journal Of Education For Business 84 (4): 240 245. doi:10.3200/joeb.84.4.240-245. 
Maccoby, M.; Scudder, T. (2011). "Leading in the heat of conflict". T+D 65 (12): 46 51. 
Myers, L. L.; Larson, R. (2005). "Preparing students for early work conflicts". Business Communication Quarterly 68 (3): 306 317. doi:10.1177/1080569905278967. 
Rahim, M.; Antonioni, D.; Psenicka, C. (2001). "A structureal equations model of leader power, subordinates' styles of handling conflict, and job performance". International Journal Of Conflict Management 12 (3): 191. doi:10.1108/eb022855. 
Rupp, D. E.; Baldwin, A.; Bashshur, M. (2006). "Using developmental assessment centers to foster workplace fairness". Psychologist-Manager Journal 9 (2): 145 170. doi:10.1207/s15503461tpmj0902_6. 
Schaller-Demers, D (2008). "Conflict: A Catalyst for Institutional Change". Journal Of Research Administration 39 (2): 81 90. 
Taylor, M (2010). "Does locus of control predict young adult conflict strategies with superiors? An examination of control orientation and the organizational communication conflict instrument". North American Journal Of Psychology 12 (3): 445 458. 
Wilson, J (2004). "Make conflict management successful if not cheerful!.". Accounting Today 18 (19): 22 27. 
Zemke, R (1985). "The honeywell studies: How managers learn to manage". Training 22 (8): 46 51. 



Conflict Management Articles - A collection of Conflict Management Articles
Search For Common Ground - One of the world's largest non-government organisations dedicated to conflict resolution
CUNY Dispute Resolution Consortium- The Dispute Resolution Headquarters in New York City.
The Johns Hopkins University School of Advanced International Studies (SAIS) Conflict Management Toolkit
Party-Directed Mediation: Facilitating Dialogue Between Individuals by Gregorio Billikopf, free complete book PDF download, at the University of California (3rd Edition, posted 24 March 2014)
Party-Directed Mediation: Facilitating Dialogue Between Individuals by Gregorio Billikopf, free complete book download, from Internet Archive (3rd Edition, multiple file formats including PDF, EPUB, and others)
Conflict Prevention, Management & Resolution, in: Berghof Glossary on Conflict Transformation, 2012. edited by Berghof Foundation, Berlin, Germany.
Conflict Management Processes - Effective Conflict Resolution Techniques