Windows 2.0 is a 16-bit Microsoft Windows GUI-based operating environment that was released on December 9, 1987  and is the successor to Windows 1.0.



Windows 2.0 allowed application windows to overlap each other unlike its predecessor Windows 1.0, which could display only tiled windows. Windows 2.0 also introduced more sophisticated keyboard-shortcuts and the terminology of "Minimize" and "Maximize", as opposed to "Iconize" and "Zoom" in Windows 1.0. The basic window setup introduced here would last through Windows 3.1. Like Windows 1.x, Windows 2.x applications cannot be run on Windows 3.1 or up without modifications since they were not designed for protected mode. Windows 2.0 was also the first Windows version to integrate the control panel.
New features in Windows 2.0 included VGA graphics (although 16 colors only). It was also the last version of Windows that did not require a hard disk. The Windows 2.x EGA, VGA, and Tandy drivers notably provided a workaround in Windows 3.0 for users who wanted color graphics on 8086 machines (a feature that version normally did not support). EMS memory support also appeared for the first time.



The first Windows versions of Microsoft Word and Microsoft Excel ran on Windows 2.0. Third-party developer support for Windows increased substantially with this version (some shipped the Windows Runtime software with their applications, for customers who had not purchased the full version of Windows). However, most developers still maintained DOS versions of their applications, as Windows users were still a distinct minority of their market. Windows 2.0 was still very dependent on the DOS system and it still hadn't passed the 1 megabyte mark in terms of memory.
There were some applications that shipped with Windows 2.0. They are:
CALC.EXE   a calculator
CALENDAR.EXE   calendaring software
CARDFILE.EXE   a personal information manager
CLIPBRD.EXE   software for viewing the contents of the clipboard
CLOCK.EXE   a clock
CONTROL.EXE   the system utility responsible for configuring Windows 2.0
CVTPAINT.EXE - Converted paint files to the 2.x format
MSDOS.EXE   a simple file manager
NOTEPAD.EXE   a text editor
PAINT.EXE   a raster graphics editor that allows users to paint and edit pictures interactively on the computer screen
PIFEDIT.EXE   a program information file editor that defines how a DOS program should behave inside Windows
REVERSI.EXE   a computer game of reversi
SPOOLER.EXE   the print spooler of Windows, a program that manages and maintains a queue of documents to be printed, sending them to the printer as soon as the printer is ready
TERMINAL.EXE   a terminal emulator
WRITE.EXE   a simple word processor




On March 17, 1988, Apple Inc. filed a lawsuit against Microsoft and Hewlett-Packard, accusing them of violating copyrights Apple held on the Macintosh System Software. Apple claimed the "look and feel" of the Macintosh operating system, taken as a whole, was protected by copyright and that Windows 2.0 violated this copyright by having the same icons. The judge ruled in favor of Hewlett-Packard and Microsoft in all but 10 of the 189 patents that Apple sued for. The exclusive 10 could not be copyrighted, as ruled by the judge.






GUIdebook: Windows 2.0 Gallery   A website dedicated to preserving and showcasing Graphical User Interfaces
ComputerHope.com: Microsoft Windows history
Microsoft article with details about the different versions of Windows