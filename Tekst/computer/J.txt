J is the 10th letter in the modern English alphabet and the ISO basic Latin alphabet. Its normal name in English is jay / d e / or jy / d a /. When used for the palatal approximant, it may be called yod (/ j d/ or / jo d/) or yot (/ j t/ or / jo t/).



The letter 'J' originated as a swash letter i, used for the letter 'i' at the end of Roman numerals when following another 'i', as in 'xxiij' instead of 'xxiii' for the Roman numeral representing 23. A distinctive usage emerged in Middle High German. Gian Giorgio Trissino (1478 1550) was the first to explicitly distinguish I and J as representing separate sounds, in his  pistola del Trissino de le lettere nu vamente aggiunte ne la lingua italiana ("Trissino's epistle about the letters recently added in the Italian language") of 1524. Originally, 'I' and 'J' were different shapes for the same letter, both equally representing /i/, /i /, and /j/; but Romance languages developed new sounds (from former /j/ and / /) that came to be represented as 'I' and 'J'; therefore, English J, acquired from the French J, has a sound value quite different from /j/ (which represents the initial sound in the English word "yet").






In English,  j  most commonly represents the affricate /d /. In Old English the phoneme /d / was represented orthographically with  cg  and  c . Under the influence of Old French, which had a similar phoneme deriving from Latin /j/, English scribes began to use  i  (later  j ) to represent word-initial /d / in Old English (for example, iest and, later jest), while using  dg  elsewhere (for example, hedge). Later, many other uses of  i  (later  j ) were added in loanwords from French and other languages (e.g. adjoin, junta). The first English language book to make a clear distinction between  i  and  j  was published in 1633. In loan words such as raj,  j  may represent / /. In some of these, including raj, Taj Mahal, and Beijing, the regular pronunciation /d / is actually closer to the native pronunciation, making the use of / / an instance of a hyperforeignism. Occasionally  j  represents the original /j/ sound, as in Hallelujah and fjord (see Yodh for details). In words of Spanish origin where  j  represents [x], such as jalape o, English speakers usually approximate this to /h/.
In English,  j  is the fourth-least-frequently used letter in words, being more frequent only than  z ,  q , and  x . It is however quite common in proper nouns, especially personal names.



The great majority of Germanic languages, such as German, Dutch, Icelandic, Swedish, Danish and Norwegian; use  j  for the palatal approximant /j/, which is usually represented by the letter  y  in English. Notable exceptions are English, Scots and (to a lesser degree) Luxembourgish.  j  also represents /j/ in Albanian, Baltic, and those Uralic and Slavic languages that use the Latin alphabet, such as Hungarian, Finnish, Estonian, Polish, Czech, Serbian, Slovak, Croatian, Latvian and Lithuanian. Some related languages, such as Serbian and Macedonian, also adopted  j  into the Cyrillic alphabet for the same purpose. Because of this standard, the lower case letter was chosen to be used in the IPA as the phonetic symbol for the sound.
In the Romance languages  j  has generally developed from its original palatal approximant value in Latin to some kind of fricative. In French, Portuguese, Catalan, and Romanian it has been fronted to the postalveolar fricative / / (like  s  in English measure). In Spanish, by contrast, it has been both devoiced and backed from an earlier / / to a present-day /x ~ h/, with the actual phonetic realization depending on the speaker's dialect/s.
In modern standard Italian spelling, only Latin words, proper nouns (such as Jesi, Letojanni, Juventus etc.) or those borrowed from foreign languages have  j . Until the 19th century,  j  was used instead of  i  in diphthongs, as a replacement for final -ii, and in vowel groups (as in Savoja); this rule was quite strict for official writing.  j  is also used to render /j/ in dialect, e.g. Romanesque ajo for standard aglio ( / / ) (garlic). The Italian novelist Luigi Pirandello used  j  in vowel groups in his works written in Italian; he also wrote in his native Sicilian language, which still uses the letter  j  to represent /j/ (and sometimes also [d ] or [gj], depending on its environment).
In Basque, the diaphoneme represented by  j  has a variety of realizations according to the regional dialect: [j,  ,  ,  ,  , x] (the last one is typical of the Spanish Basque Country).
Among non-European languages which have adopted the Latin script,  j  stands for / / in Turkish, Azerbaijani, and Tatar.  j  stands for /d / in Indonesian, Somali, Malay, Igbo, Shona, Oromo, Turkmen, and Zulu. It represents a voiced palatal plosive / / in Konkani, Yoruba, and Swahili. In Kiowa,  j  stands for a voiceless alveolar plosive, /t/.
In Chinese Pinyin,  j  stands for /t /, the unaspirated equivalent of  q .
The Royal Thai General System of Transcription does not use the letter  j , although it is used in some proper names and non-standard transcriptions to represent either   [t ] or   [t ] (the latter following Pali/Sanskrit root equivalents).
In romanized Pashto,  j  represents  , pronounced [dz].



The capital letter J, in upright (non-slanted) type, is the unit symbol for the joule energy unit. It is also the dimension symbol for luminous intensity, conventionally rendered in a sans-serif typeface.



  : Semitic letter Yodh, from which the following symbols originally derive
I i : Latin letter I, from which J derives
  : Dotless j
IPA-specific symbols related to J:      
I with diacritics:        



1 Also for encodings based on ASCII, including the DOS, Windows, ISO-8859 and Macintosh families of encodings.
Unicode also has a dotless variant,   (U+0237) for use with combining diacritics.
In Unicode, a duplicate of 'J' for use as a special phonetic character in historical Greek linguistics is encoded in the Greek script block as   (Unicode U+03F3). It is used to denote the palatal glide /j/ in the context of Greek script. It is called "Yot" in the Unicode standard, after the German name of the letter J. An uppercase version of this letter was added to the Unicode Standard at U+037F with the release of version 7.0 in June 2014.



In the Wingdings font by Microsoft, the letter "J" is rendered as a smiley face (note this is distinct from the Unicode code point U+263A, which renders as  ). In Microsoft applications, an engineer corrected ":)" as a smiley rendered in a specific font face when composing rich text documents (and/or HTML email). This autocorrection feature can be switched off or changed to a Unicode smiley. 





