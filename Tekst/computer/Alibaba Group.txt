Alibaba Group Holding Limited is a Chinese e-commerce company that provides consumer-to-consumer, business-to-consumer and business-to-business sales services via web portals. It also provides electronic payment services, a shopping search engine and data-centric cloud computing services. The group began in 1999 when Jack Ma founded the website Alibaba.com, a business-to-business portal to connect Chinese manufacturers with overseas buyers. In 2012, two of Alibaba s portals handled 1.1 trillion yuan ($170 billion) in sales. The company primarily operates in the People s Republic of China (PRC), and at closing time, on the date of its initial public offering (IPO), 19 September 2014, Alibaba's market value was measured as US$231 billion. However, the stock has traded down and market cap was $145 billion at the end of September 2015.
In September 2013, the company sought an IPO in the United States after a deal could not be reached with Hong Kong regulators. Planning occurred over 12 months before the company's market debut in September 2014. The Alibaba Reuters Instrument Code is "BABA.N", while the pricing of the IPO initially raised US$21.8 billion, which later increased to US$25 billion, making it the largest IPO in history. However, buyers weren't purchasing actual shares in the group, since China forbids foreign ownership, but rather just shares in a Cayman Islands shell corporation.
Alibaba's consumer-to-consumer portal Taobao, similar to eBay.com, features nearly a billion products and is one of the 20 most-visited websites globally. The Group's websites accounted for over 60% of the parcels delivered in China by March 2013, and 80% of the nation's online sales by September 2014. Alipay, an online payment escrow service, accounts for roughly half of all online payment transactions within China.
Alibaba is planning to enter India and was in talks with Snapdeal in September 2014.
Alibaba reported sale of more than $9 billion on China's Singles' Day in 2014.
Alibaba entered India's e-commerce space with 25% stake in Paytm owner One97.






The company was founded in Hangzhou as Ma explained:

One day I was in San Francisco in a coffee shop, and I was thinking Alibaba is a good name. And then a waitress came, and I said, "Do you know about Alibaba?" And she said yes. I said, "What do you know about Alibaba?", and she said, "Open Sesame". And I said, "Yes, this is the name!" Then I went onto the street and found 30 people and asked them, "Do you know Alibaba?" People from India, people from Germany, people from Tokyo and China   they all knew about Alibaba. Alibaba   open sesame. Alibaba is a kind, smart business person, and he helped the village. So   easy to spell, and globally known. Alibaba opens sesame for small- to medium-sized companies. We also registered the name "Alimama", in case someone wants to marry us!"



According to Li Chuan, a senior executive at Alibaba, the company was planning in 2013 to open traditional brick and mortar retail outlets in partnership with Chinese real estate company Dalian Wanda Group. Additionally, Alibaba purchased a 25% stake in Hong Kong-based Intime Retail in early 2014.



On 5 September 2014, the group in a regulatory filing with the U.S. Securities and Exchange Commission set a US$60- to $66- per-share price range for its scheduled initial public offering (IPO), the final price of which would be determined after an international roadshow to gauge the investor interest in Alibaba shares.
On 18 September 2014, Alibaba's IPO priced at US$68, raising US$21.8 billion for the company and investors. Alibaba was the biggest U.S. IPO in history.
On 19 September 2014, Alibaba's shares (BABA) began trading on the NYSE at an opening price of $92.70 at 11:55am EST.
On 22 September 2014, Alibaba's underwriters announced their confirmation that they had exercised a greenshoe option to sell 15% more shares than originally planned, boosting the total amount of the IPO to $25 billion.



In December 1998, Jack Ma and 17 other founders released their first online marketplace, named "Alibaba Online".
From 1999 to 2000, Alibaba Group raised a total of US$25 million from SoftBank, Goldman Sachs, Fidelity and some other institutions.
In December 2001, Alibaba.com achieved profitability.
In May 2003, Taobao was founded as a consumer e-commerce platform.
In December 2004, Alipay, which started as a service on the Taobao platform, became a separate business.
In October 2005, Alibaba Group took over the operation of China Yahoo! as part of its strategic partnership with Yahoo! Inc.
In November 2007, Alibaba.com successfully listed on the Hong Kong Stock Exchange.
In April 2008, Taobao established Taobao Mall (Tmall.com), a retail website, to complement its C2C marketplace.
In September 2008, Alibaba Group R&D Institute was established.
In December 2008, Alibaba.com Corporation announced that all Alibaba group websites will be shark fin-free on 1 January 2009.
In September 2009, Alibaba Group established Alibaba Cloud Computing in conjunction with its 10-year anniversary.
In May 2010, Alibaba Group announced a plan to earmark 0.3% of its annual revenues to fund environmental protection initiatives.
In October 2010, Taobao beta-launched eTao as a shopping search engine.
In June 2011, Alibaba Group reorganized Taobao into three separate companies: Taobao Marketplace, Taobao Mall (Tmall.com) and eTao.
In July 2011, Alibaba Cloud Computing launched its first self-developed mobile operating system, Aliyun OS over K-Touch Cloud Smartphone.
In January 2012, Tmall.com changed its Chinese name as part of a rebranding exercise.
In March 2014, Alibaba group said it will begin the process of filing for an initial public offering in the U.S.
Prior to its IPO filing on Form F-1 as a foreign issuer in the U.S., Alibaba undertook an aggressive acquisition spree   previously atypical for the company   acquiring numerous majority and minority stakes in companies including micro-blogging service Weibo, China Vision Holdings, and car sharing service Lyft, as well as smart remote app developer Peel Technologies.
On 6 May 2014, Alibaba Group filed registration documents to go public in the U.S. in what may be one of the biggest initial public offerings in American history.
On 5 June 2014, Alibaba group agreed to take a 50 percent stake in Guangzhou Evergrande Football Club, winners of the 2013 AFC Champions League, for 1.2 billion yuan ($192 million).
In June 2014, Alibaba acquired the Chinese mobile internet firm UCWeb. The price of the purchase has not been disclosed but the company did claim that the acquisition creates the biggest merger in the history of China's internet sector.
On 19 September 2014, Alibaba's shares (BABA) began trading on the NYSE.
On 2 February 2015, Alibaba announces that it has acquired a $590m minority stake in the Chinese smartphone maker Meizu.






Alibaba.com, the primary company of Alibaba, is the world s largest online business-to-business trading platform for small businesses.
Founded in Hangzhou in eastern China, Alibaba.com has three main services. The company s English language portal Alibaba.com handles sales between importers and exporters from more than 240 countries and regions. The Chinese portal 1688.com was developed for domestic business-to-business trade in China. In addition, Alibaba.com offers a transaction-based retail website, AliExpress.com, which allows smaller buyers to buy small quantities of goods at wholesale prices.
Alibaba.com went public at the Hong Kong Stock Exchange in 2007, and was delisted again in 2012.
In 2013, 1688.com launched a direct channel that is responsible for $30 million in daily transaction value.




Taobao Marketplace, or Taobao, is China's largest consumer-to-consumer online shopping platform. Founded in 2003, it offers a variety of products for retail sale. In January 2015 it was the second most visited web site in China, according to Alexa.com. Taobao's growth was attributed to offering free registration and commission-free transactions using a free third-party payment platform.
Advertising makes up 85 percent of the company's total revenue, allowing it to break even in 2009. Taobao's 2010 profit was estimated to be 1.5 billion yuan (US $235.7 million), only about 0.4 percent of their total sales figure of 400 billion yuan (US $62.9 billion) that year, way below the industry average of 2 percent, according to iResearch estimates.
According to Zhang Yu, the director of Taobao, the number of stores on Taobao with annual sales under 100 thousand yuan increased by 60% between 2011 and 2013. Over the same period, the number of stores with sales between 10 thousand and 1 million yuan increased by 30%, and the number of stores with sales over 1 million yuan increased by 33%. Taobao's total sales (including Tmall) exceeded 1 trillion yuan (USD 160 billion) in 2012. And on 11 November 2012, the biggest online shopping promotion activity, Taobao accomplished 19.1 billion yuan (USD 3.07 billion) sales in one day.




Tmall.com was introduced in April 2008 as an online retail platform to complement the Taobao consumer-to-consumer portal and became a separate business in June 2011. As of October 2013 it was the eighth most visited web site in China, offering global brands to an increasingly affluent Chinese consumer base.



Juhuasuan.com is a group shopping website in China. It was launched by Taobao in March 2010 and became a separate business in October 2011. Juhuasuan offers "flash sales", products that are available only for a fixed time period, which can last from one or two days to a full month. To buy at the discounted price, buyers must purchase the item within that defined time.



eTao.com was beta-launched by Taobao in October 2010 as a comparison shopping website, and became a separate business in June 2011. It offers search results from most Chinese online shopping platforms, including product searches, sales and coupon searches. Online shoppers can use the site to compare prices from different sellers and identify products to buy. According to the Alibaba Group web site, eTao offers products from Amazon China, Dangdang, Gome, Yihaodian, Nike China and Vancl, as well as Taobao and Tmall.




Launched in 2004, Alipay is a third-party online payment platform with no transaction fees. It also provides an escrow service, in which buyers can verify whether they are happy with goods they have bought before releasing money to the seller. Alibaba Group spun off Alipay in 2010 in a controversial move. According to analyst research report, Alipay has the biggest market share in China with 300 million users and control of just under half of China's online payment market in February 2014. In 2013, Alipay launched a financial product platform called Yu'ebao ( ).



Alibaba Cloud Computing (www.aliyun.com) aims to build a cloud computing service platform, including e-commerce data mining e-commerce data processing, and data customization. It was established in September 2009 in conjunction with the 10th anniversary of Alibaba Group. It has R&D centers and operators in Hangzhou, Beijing and Silicon Valley  In July 2014, Aliyun entered into a partnership deal with Inspur. Aliyun is the largest high-end cloud computing company in China.



Launched in 2010, AliExpress.com is an online retail service made up of mostly small Chinese businesses offering products to international online buyers. It is the most visited e-commerce website in Russia.



In October 2005, Alibaba Group formed a strategic partnership with Yahoo! and acquired China Yahoo! (www.yahoo.com.cn), a Chinese portal that focuses on Internet services like news, email, and search. In April 2013, Alibaba Group announced that, as part of the agreement to buy back the Yahoo! Mail stake, technological support for China Yahoo! Mail service would be suspended and the China Yahoo! Mail account migration would begin. Several options were offered to users to make the transition as smooth as possible, and China Yahoo! users had four months to migrate their accounts to the Aliyun mail service, the Yahoo! Mail service in the United States, or to another third-party e-mail provider of the user's choice. Yahoo! China closed its mail service on 19 August 2013. E-mails sent to Yahoo! China accounts can be forwarded to an Alimail box until 31 December 2014. Users are also allowed to transfer e-mail accounts to yahoo.com or any other e-mail service. It is estimated there are no more than a million users with Yahoo! Mail for China and chances are they also own other e-mail accounts.



In 2004, the company released its own instant messaging software service Aliwangwang for interactions between customer and online sellers. By 2014 Aliwangwang user has reached 50 million, making it the second largest instant messaging tool in China.



In October 2013, the company's chairman Jack Ma announced that the company would no longer use Tencent's messaging application WeChat, and would henceforth promote its own messaging application and service, Laiwang.




In March 2014, Alibaba agreed to acquire a controlling stake in ChinaVision Media Group for $804 million. The two firms announced they would establish a strategic committee for potential future opportunities in online entertainment and other media areas. The company was renamed Alibaba Pictures Group.



In April 2014, Alibaba and Yunfeng Capital, a private equity company controlled by Alibaba s founder, Jack Ma, agreed to acquire a combined 18.5 percent stake in Youku Tudou, which broadcasts a series of popular television programs and other videos over the Internet.



 On 11 June 2014, Alibaba launched U.S. shopping site 11 Main. The 11 Main marketplace hosts more than 1,000 merchants in categories such as clothing, fashion accessories and jewelry as well as interior goods and arts and crafts and it plans to keep adding more, the company said. On 23 June 2015, Alibaba announced that it is selling 11 Main to OpenSky, an online-marketplace operator based in New York.



Alibaba introduced the Alibaba Group R&D institute in 2008. One year later, Alibaba filed around 350 patent and utility model applications.



Alibaba completed the purchase of XiaMi, the online music service provider, for an undisclosed sum. Taobao has since launched a service allowing customers to listen to music while shopping.



Global Biz Circle in an online content site powered by Alibaba.com. The mission of Global Biz Circle is to provide content that not only empowers businesses but also provides them with the know-how to grow and succeed in a global e-commerce environment. http://globalbizcircle.com/



Alibaba has completed the acquisition of this domestic crowdsourcing translation platform to solve the language barrier problem encountered for small and medium enterprises (SMEs) to participate in cross-border e-commerce. 



Jack Ma was Alibaba Group's chief executive officer for its first 10 years, but stepped down as CEO on 10 May 2013, becoming executive chairman. Jonathan Lu became the company's new CEO.
On 7 May 2015 Alibaba announced that on 10 May COO Daniel Zhang will replace Lu as CEO.






Alibaba.com offers a Gold Supplier membership to try to ensure that each seller is genuine. "To qualify for a Gold Supplier membership, a supplier must complete an authentication and verification process by a reputable third-party security service provider appointed by Alibaba.com". While the majority of suppliers are reported to be genuine, there have been cases of sellers seeking to defraud unsuspecting buyers. In February, 2011, controversy ensued when Alibaba's corporate office admitted that it had granted the mark of integrity of its "China Gold Supplier" program to more than 2,000 dealers that had subsequently defrauded buyers; the firm's share price dropped "abruptly" after the announcement. A statement from the firm reported that Yan Limin, the General Manager of Alibaba.com at the time, had been dismissed in March for "misconduct"; Phil Muncaster of UK's The Register additionally reported that "a further 28 employees had been involved in dodgy dealings".
As the Economist noted, the company's response has conflicting components: Alibaba's promulgated view that its corrective actions indicate its commitment to quality and integrity (where it contrasts itself with other scandal-associated Chinese business sectors), versus a damage control view suggesting that the subscription-driven, third-party verified "China Gold Supplier" program was endangered by diminished trust in its endorsement system, removing the incentive for global buyers to choose Alibaba as their business-to-business service, thus more broadly endangering Alibaba through impact on its brand and capabilities (the latter via the "defenestration of senior people"). The scandal is said to have placed the head of Alibaba Group, Jack Ma who is described as having been furious over the scandal in a position to personally fight to win back trust.



In May 2012, a U.S. law enforcement agent posing as an American broker representing persons in Iran posted an advertisement on Alibaba.com seeking to purchase uranium. In August 2013, Patrick Campbell of Sierra Leone was arrested at New York's John F. Kennedy International Airport. Samples of raw uranium ore were allegedly found concealed in the soles of his shoes. Campbell was accused of seeking to arrange the export of 1,000 tonnes of yellowcake from Sierra Leone to the Iranian port of Bandar Abbas, packed in drums and disguised as the mineral chromite. It was later determined that the samples contained an insignificant amount of uranium, and Campbell was acquitted at trial.


