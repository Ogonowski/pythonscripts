Biberist is a municipality in the district of Wasseramt in the canton of Solothurn in Switzerland.



Biberist is first mentioned in 762 as Biberussa. In 1300 Ober- and Unterbiberist were mentioned as ze beiden Biberschon.
During the Helvetic Republic it was the capital of the Biberist district.




Biberist has an area, as of 2009, of 12.3 square kilometers (4.7 sq mi). Of this area, 4.53 km2 (1.75 sq mi) or 36.8% is used for agricultural purposes, while 4.19 km2 (1.62 sq mi) or 34.1% is forested. Of the rest of the land, 3.25 km2 (1.25 sq mi) or 26.4% is settled (buildings or roads), 0.27 km2 (0.10 sq mi) or 2.2% is either rivers or lakes.
Of the built up area, industrial buildings made up 4.4% of the total area while housing and buildings made up 12.7% and transportation infrastructure made up 7.5%. while parks, green belts and sports fields made up 1.1%. Out of the forested land, 32.3% of the total land area is heavily forested and 1.8% is covered with orchards or small clusters of trees. Of the agricultural land, 26.3% is used for growing crops and 8.9% is pastures, while 1.6% is used for orchards or vine crops. All the water in the municipality is flowing water.
The municipality is located in the Wasseramt district, between the Emme and Aare rivers south of Solothurn. The two villages of Ober- and Unterbiberist were merged into a single municipality in 1857.
The municipalities of Biberist, Derendingen, Luterbach, Bellach, Langendorf and Solothurn are considering a merger at a date in the future into the new municipality of with an, as of 2011, undetermined name.



The blazon of the municipal coat of arms is Per fess Gules and Argent two Cramps in Saltire counterchanged.




Biberist has a population (as of December 2014) of 8,261. As of 2008, 20.4% of the population are resident foreign nationals. Over the last 10 years (1999 2009 ) the population has changed at a rate of 4.7%. It has changed at a rate of 7.3% due to migration and at a rate of -2.2% due to births and deaths.
Most of the population (as of 2000) speaks German (6,644 or 87.4%), with Italian being second most common (277 or 3.6%) and Serbo-Croatian being third (175 or 2.3%). There are 63 people who speak French and 3 people who speak Romansh.
As of 2008, the gender distribution of the population was 48.9% male and 51.1% female. The population was made up of 2,959 Swiss men (37.1% of the population) and 939 (11.8%) non-Swiss men. There were 3,283 Swiss women (41.2%) and 796 (10.0%) non-Swiss women. Of the population in the municipality 2,178 or about 28.6% were born in Biberist and lived there in 2000. There were 2,004 or 26.4% who were born in the same canton, while 1,830 or 24.1% were born somewhere else in Switzerland, and 1,357 or 17.8% were born outside of Switzerland.
In 2008 there were 37 live births to Swiss citizens and 22 births to non-Swiss citizens, and in same time span there were 77 deaths of Swiss citizens and 3 non-Swiss citizen deaths. Ignoring immigration and emigration, the population of Swiss citizens decreased by 40 while the foreign population increased by 19. There were 7 Swiss men and 2 Swiss women who immigrated back to Switzerland. At the same time, there were 21 non-Swiss men and 10 non-Swiss women who immigrated from another country to Switzerland. The total Swiss population change in 2008 (from all sources, including moves across municipal borders) was an increase of 11 and the non-Swiss population decreased by 21 people. This represents a population growth rate of -0.1%.
The age distribution, as of 2000, in Biberist is; 535 children or 7.0% of the population are between 0 and 6 years old and 1,153 teenagers or 15.2% are between 7 and 19. Of the adult population, 452 people or 5.9% of the population are between 20 and 24 years old. 2,272 people or 29.9% are between 25 and 44, and 1,837 people or 24.2% are between 45 and 64. The senior population distribution is 957 people or 12.6% of the population are between 65 and 79 years old and there are 397 people or 5.2% who are over 80.
As of 2000, there were 3,042 people who were single and never married in the municipality. There were 3,605 married individuals, 509 widows or widowers and 447 individuals who are divorced.

As of 2000, there were 3,238 private households in the municipality, and an average of 2.2 persons per household. There were 1,125 households that consist of only one person and 196 households with five or more people. Out of a total of 3,287 households that answered this question, 34.2% were households made up of just one person and there were 28 adults who lived with their parents. Of the rest of the households, there are 966 married couples without children, 923 married couples with children There were 173 single parents with a child or children. There were 23 households that were made up of unrelated people and 49 households that were made up of some sort of institution or another collective housing.
In 2000 there were 1,018 single-family homes (or 62.8% of the total) out of a total of 1,620 inhabited buildings. There were 369 multi-family buildings (22.8%), along with 152 multi-purpose buildings that were mostly used for housing (9.4%) and 81 other use buildings (commercial or industrial) that also had some housing (5.0%). Of the single-family homes 60 were built before 1919, while 116 were built between 1990 and 2000. The greatest number of single-family homes (254) were built between 1946 and 1960.
In 2000 there were 3,427 apartments in the municipality. The most common apartment size was 4 rooms of which there were 1,145. There were 74 single room apartments and 919 apartments with five or more rooms. Of these apartments, a total of 3,152 apartments (92.0% of the total) were permanently occupied, while 141 apartments (4.1%) were seasonally occupied and 134 apartments (3.9%) were empty. As of 2009, the construction rate of new housing units was 2.5 new units per 1000 residents. The vacancy rate for the municipality, in 2010, was 3.57%.
The historical population is given in the following chart: 



In the 2007 federal election the most popular party was the SVP which received 25.21% of the vote. The next three most popular parties were the SP (23.98%), the FDP (18.07%) and the CVP (17.7%). In the federal election, a total of 2,329 votes were cast, and the voter turnout was 44.1%.




As of 2010, Biberist had an unemployment rate of 4.5%. As of 2008, there were 75 people employed in the primary economic sector and about 25 businesses involved in this sector. 1,576 people were employed in the secondary sector and there were 88 businesses in this sector. 1,846 people were employed in the tertiary sector, with 196 businesses in this sector. There were 3,960 residents of the municipality who were employed in some capacity, of which females made up 44.2% of the workforce.
In 2008 the total number of full-time equivalent jobs was 2,927. The number of jobs in the primary sector was 46, all of which were in agriculture. The number of jobs in the secondary sector was 1,487 of which 1,297 or (87.2%) were in manufacturing and 179 (12.0%) were in construction. The number of jobs in the tertiary sector was 1,394. In the tertiary sector; 382 or 27.4% were in wholesale or retail sales or the repair of motor vehicles, 32 or 2.3% were in the movement and storage of goods, 96 or 6.9% were in a hotel or restaurant, 20 or 1.4% were in the information industry, 25 or 1.8% were the insurance or financial industry, 121 or 8.7% were technical professionals or scientists, 96 or 6.9% were in education and 386 or 27.7% were in health care.
In 2000, there were 2,112 workers who commuted into the municipality and 2,666 workers who commuted away. The municipality is a net exporter of workers, with about 1.3 workers leaving the municipality for every one entering. Of the working population, 15% used public transportation to get to work, and 53.4% used a private car.




From the 2000 census, 2,459 or 32.3% were Roman Catholic, while 3,019 or 39.7% belonged to the Swiss Reformed Church. Of the rest of the population, there were 155 members of an Orthodox church (or about 2.04% of the population), there were 56 individuals (or about 0.74% of the population) who belonged to the Christian Catholic Church, and there were 149 individuals (or about 1.96% of the population) who belonged to another Christian church. There were 560 (or about 7.37% of the population) who were Islamic. There were 16 individuals who were Buddhist, 53 individuals who were Hindu and 5 individuals who belonged to another church. 944 (or about 12.42% of the population) belonged to no church, are agnostic or atheist, and 187 individuals (or about 2.46% of the population) did not answer the question.




In Biberist about 2,996 or (39.4%) of the population have completed non-mandatory upper secondary education, and 755 or (9.9%) have completed additional higher education (either university or a Fachhochschule). Of the 755 who completed tertiary schooling, 68.2% were Swiss men, 20.8% were Swiss women, 6.9% were non-Swiss men and 4.1% were non-Swiss women.
During the 2010-2011 school year there were a total of 565 students in the Biberist school system. The education system in the Canton of Solothurn allows young children to attend two years of non-obligatory Kindergarten. During that school year, there were 116 children in kindergarten. The canton's school system requires students to attend six years of primary school, with some of the children attending smaller, specialized classes. In the municipality there were 417 students in primary school and 32 students in the special, smaller classes. The secondary school program consists of three lower, obligatory years of schooling, followed by three to five years of optional, advanced schools. All the lower secondary students from Biberist attend their school in a neighboring municipality.
As of 2000, there were 85 students in Biberist who came from another municipality, while 256 residents attended schools outside the municipality.






Official website (German)
Biberist in German, French and Italian in the online Historical Dictionary of Switzerland.