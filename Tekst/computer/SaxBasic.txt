The Microsoft Windows Script Host (WSH) is an automation technology for Microsoft Windows operating systems that provides scripting abilities comparable to batch files, but with a wider range of supported features. It was originally called Windows Scripting Host, but was renamed for the second release.
It is language-independent in that it can make use of different Active Scripting language engines. By default, it interprets and runs plain-text JScript (.JS and .JSE files) and VBScript (.VBS and .VBE files).
Users can install different scripting engines to enable them to script in other languages, for instance PerlScript. The language independent filename extension WSF can also be used. The advantage of the Windows Script File (.WSF) is that it allows the user to use a combination of scripting languages within a single file.
WSH engines include various implementations for the REXX, BASIC, Perl, Ruby, Tcl, PHP, JavaScript, Delphi, Python, XSLT, and other languages.
Windows Script Host is distributed and installed by default on Windows 98 and later versions of Windows. It is also installed if Internet Explorer 5 (or a later version) is installed. Beginning with Windows 2000, the Windows Script Host became available for use with user login scripts.



Windows Script Host may be used for a variety of purposes, including logon scripts, administration and general automation. Microsoft describes it as an administration tool. WSH provides an environment for scripts to run   it invokes the appropriate script engine and provides a set of services and objects for the script to work with. These scripts may be run in either GUI mode (WScript.exe) or command line mode (CScript.exe) offering flexibility to the user for interactive or non-interactive scripts. WSH implements an object model which exposes a set of Component Object Model (COM) interfaces.
Unless otherwise specified, any WSH scripting engine can be used with the various Windows server software packages to provide CGI scripting. The current versions of the default WSH engines and all or most of the third party engines have socket abilities as well; as a CGI script or otherwise, PerlScript is the choice of many programmers for this purpose and the VBScript and various Rexx-based engines are also rated as sufficiently powerful in connectivity and text-processing abilities to also be useful. This also goes for file access and processing -- the earliest WSH engines for VBScript and JScript do not since the base language did not, whilst PerlScript, ooRexxScript, and the others have this from the beginning.
WinWrap Basic, SaxBasic and others are similar to Visual Basic for Applications, These tools are used to add scripting and macro abilities. Many other languages can also be used in this fashion. Other languages used for scripting of programmes include Rexx, Tcl, Perl, Python, Ruby, and others which come with methods to control objects in the operating system and the spreadsheet and database programmes.
VBScript is the macro language in Microsoft Outlook, whilst WordBasic is used for Word, Powerpoint and other tools. In Office 97 forward, true Visual Basic 5.0 is used. Other components use Visual Basic for Applications. OpenOffice uses Visual Basic, Python, and several others as macro languages and others can be added. LotusScript is very closely related to VBA, and pure VBA licensed from Microsoft is used Corel products such as Lotus Notes, Lotus 1 2 3, Quattro Pro &c.
Any scripting language connected to the Windows can be accessed by external means of PerlScript, PythonScript, VBScript and the other engines available can be used to access databases (Lotus Notes, Microsoft Access, Oracle), spreadsheets (Microsoft Excel Lotus 1 2 3, Quattro Pro. This can be accomplished by means of the WSH so any language can be used if there is an installed engine.



The first example is very simple; it shows some VBScript which uses the root WSH COM object "WScript" to display a message with an 'OK' button. Upon launching this script the CScript or WScript engine would be called and the runtime environment provided.
Content of a file hello0.vbs

WSH programming can also use the JScript language.
Content of a file hello1.js

Or, code can be mixed in one WSF file, such as VBScript and JScript, or any other:
Content of a file hello2.wsf



Windows applications and processes may be automated using a script in Windows Script Host. Viruses and malware could be written to exploit this ability. Thus, some suggest disabling it for security reasons. Alternatively, antivirus programs may offer features to control .vbs and other scripts which run in the WSH environment.
Since version 5.6 of WSH, scripts can be digitally signed programmatically using the Scripting.Signer object in a script itself, provided a valid certificate is present on the system. Alternatively, the signcode tool from the Platform SDK, which has been extended to support WSH filetypes, may be used at the command line.
By using Software Restriction Policies introduced with Windows XP, a system may be configured to execute only those scripts which have been digitally signed, thus preventing the execution of untrusted scripts.



In addition to those listed above, an engine for the Take Command/4NT scripting language is in the development phase. The language is an extended analogue of the cmd.exe command shell found in the Windows NT-2000-XP et seq; 4NT, the pure console version, is now named Take Command Console (TCC) Take Command, a GUI-based IDE for the language, and TCC and Take Command are now at version 18.0  The 4NT/TC language also has functions built in to work with numerous scripting languages.
There have been suggestions of creating engines for other languages, such as LotusScript, SaxBasic, KiXtart, awk, bash, csh and other Unix shells, 4NT, cmd.exe (the Windows NT shell), Windows PowerShell, DCL, C, C++, Fortran and others. The XLNT language is based on DCL and provides a very large subset of the language along with additional commands and statements and the software can be used in three ways: the WSH engine (*.xcs), the console interpreter (*.xlnt) and as a server and client side CGI engine (*.xgi).
When a server implementing CGI such as the Windows Internet Information Server, ports of Apache and others, all or most of the engines can be used; the most commonly used are VBScript, JScript, PythonScript, PerlScript, ActivePHPScript, and ooRexxScript. The MKS Toolkit PScript programme also runs Perl. Command shells like cmd.exe, 4NT, ksh, and scripting languages with string processing and preferably socket functionality are also able to be used for CGI scripting; compiled languages like C++, Visual Basic, and Java can also be used like this. All Perl interpreters, ooRexx, PHP, and more recent versions of VBScript and JScript can use sockets for TCP/IP and usually UDP and other protocols for this.



The redistributable version of WSH version 5.6 can be installed on Windows 95/98/Me and Windows NT 4.0/2000. WSH 5.7 is downloadable for Windows 2000, Windows XP and Windows Server 2003. Recently, redistributable versions for older operating systems (Windows 9x and Windows NT 4.0) are no longer available from the Microsoft Download Center.
Since Windows XP Service Pack 3, release 5.7 is not needed as it is included, with newer revisions being included in newer versions of Windows since.



JScript .NET
Microsoft Script Debugger
Shell script
Windows PowerShell






WSH Primer on Microsoft TechNet   Get started with WSH
WSH home at MSDN
WSH Reference
Release notes for Windows Script 5.7
Console WSH Shell - a third-party shell for WSH and VBScript