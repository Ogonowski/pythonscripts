The Aberdeen Scandal was a military sex scandal in 1996 at Aberdeen Proving Ground, a United States Army base in Maryland. The Army brought charges against 12 commissioned and non-commissioned male officers for sexual assault on female trainees under their command with some officers convicted.



Captain Derrick Robertson and Sergeants Delmar Simpson and Nathanael Beech were accused of participating in the army's biggest sex abuse scandal on record. Robertson and Simpson faced rape charges and Beech was charged with adultery.
Army Secretary Togo West accused those charged of abusing their power, and ordered all soldiers to undergo sexual harassment training so they could learn the army's "zero tolerance" policy towards sexual harassment.
The men accused felt that statements made by West and other officials within the army were prejudging the case and would result in an unfair trial. Robertson's attorney, Jerome Murphy, asked the judge, Military Judge Linda Webster, to enact a gag order on West and others, but she deferred judgment. However, she did order prospective jurors not to follow media coverage of the case to avoid interference with their judgment.
Others charged included Staff Sergeants Loren Taylor and Anthony Fore, Sergeant George Blackley, a captain, two other sergeants, and four other non-commissioned officers.



Staff Sergeant Vernell Robinson Jr., who had been accused of 19 counts involving the abuse of five women under his command including sodomy, adultery, communicating a threat, obstructing justice and disobeying orders was sentenced to six months in prison, demoted to private, forfeited all benefits and expelled from the army with a dishonorable discharge.
Captain Derrick Robertson was sentenced to four months in prison after pleading guilty to adultery, sodomy and other offenses.
Sergeant Delmar Simpson was sentenced to the heaviest punishment. A military trial found him guilty of 18 counts of rape against six women trainees and 29 other offenses. He was sentenced to 25 years in military prison. The key to the conviction was "constructive force", in which even seemingly consensual sex can be considered rape, given the power differences between the participants.



Since the charges were filed, the U.S. Army set up a hotline to take reports of sexual harassment in the military. It has also made available many resources for soldiers who feel they have been harassed or assaulted.
Supervisors were also barred from having romantic relationships with the soldiers they trained. The Army cited the fact that they cannot be sure if all relationships are consensual due to the nature of a supervisor's power over their trainees.


