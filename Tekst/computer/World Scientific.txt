World Scientific Publishing is an academic publisher of scientific, technical, and medical books and journals headquartered in Singapore. The company was founded in 1981. It publishes about 500 books annually as well as more than 150 journals in various fields. In 1995, World Scientific co-founded the London-based Imperial College Press together with the Imperial College of Science, Technology and Medicine. World Scientific has been publishing online since 1996, including e-journals, e-proceedings, e-books, and operates an online bookshop.



The company head office is in Singapore, where over 200 employees work. The company has offices in Hackensack, New Jersey, London, Hong Kong, Beijing, Shanghai, Tianjin, Chennai, India, Taipei and Israel.



Global Publishing: specializing in the publication of works in Chinese or Chinese and English
Meeting Matters International: a provider of conference management, secretariat, and meeting support services
Stallion Press: providing distribution and pre-press services for clients in Europe, the United States, and Singapore
World Scientific Printers: providing design and printing services including print-on-demand
LAB Creations: a design and printing consultancy
Aces: an editorial services company, catering to editing and project management services



In 1995 the company co-founded Imperial College Press, specializing in engineering, medicine and information technology, with Imperial College London. In 2006, World Scientific assumed full ownership of Imperial College Press, under a license granted by the university.



World Scientific has over 19 imprints, among them are:
DL Publishing
Global Publishing



The Nobel Foundation in Sweden has granted World Scientific the rights to publish the complete set of Nobel Lectures in all subjects (from 1901 onwards). The publisher of these was originally Elsevier, which halted publication of these lectures in 1970.



In 2014, the Singapore Literature Prize received flak for naming the non-fiction prizes after World Scientific, who sponsored the category. A book World Scientific published, Lim Siong Guan's The Leader, The Teacher & You (2013), ended up co-winning the Prize for English Non-fiction.




Journals published by World Scientific






Official website