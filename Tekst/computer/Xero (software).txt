Xero is a New Zealand-based software company that develops cloud-based accounting software for small and medium-sized businesses. The company has offices in New Zealand, Australia, the United Kingdom and the United States. It is listed on both the New Zealand Exchange and Australian Securities Exchange.
Its products are based on the software as a service (SaaS) model and sold by subscription, based on the type and number of company entities managed by the subscriber.



Xero was founded by Rod Drury and his personal accountant after they found that traditional desktop accounting software had become outdated and decided to create a modern cloud-based product. Xero Limited was officially formed in 2006 in Wellington, New Zealand where its global headquarters are still located. The company entered the Australian market in 2011, and the United Kingdom and United States in 2012.



Xero went public on the New Zealand Exchange on June 5, 2007 with a $15 million (NZD) IPO, gaining 15% on its first trading day. Drury decided to list on the NZE rather than receive investment from Silicon Valley in order to avoid being pressured into selling to a larger competing company. The company focused on the New Zealand market and product and development for its first five years before entering other markets. It went public on the Australian Securities Exchange on November 8, 2012.
Xero has also received funding from various investors. In 2009, it closed a $23 million (NZD) round of funding led by MYOB founder Craig Winkler. It raised an additional $4 million (NZD) in 2010 from Peter Thiel's Valar Ventures, who also invested an additional $16.6 million (USD) in February 2012. It raised $49 million in a funding round in November 2012 with the largest amounts coming from Peter Thiel and Matrix Capital. It was reported by PandoDaily that the company has raised more than $100 million and had a valuation of approximately $1.4 billion on the NZE as of May 2013. This was prior to the company receiving an additional $180 million (NZD) from Thiel and Matrix in October 2013, bringing total funding to more than $230 million.



Xero has acquired numerous companies since its launch. In July 2011, the company acquired Australian online payroll provider Paycycle for a mixture of cash and shares totaling $1.9 million (NZD). The acquisition allowed Xero to integrate payroll into their Australian product offering. It acquired Spotlight Workpapers in July 2012 for a mixture of cash and shares totaling $800,000. The same year it acquired online job, time and invoice management solution WorkflowMax for cash and shares totaling $6 million (NZD). Xero entered the US market in 2013.



The key features of Xero include automatic bank and credit card account feeds, invoicing, accounts payable, expense claims, fixed asset depreciation, purchase orders, and standard business and management reporting. Xero can automatically import bank and credit card statements. It offers a free API that enables customers and 3rd-party software vendors to integrate external applications with Xero. Over 275 3rd-party software vendors have built Xero add-ons. Xero also supports multiple tax rates and currencies.
It also incorporates a payroll feature for users in the Australian and United States markets. And users can access local teams in New Zealand, Australia, Europe and the United States.
All financial data is stored in the cloud on a single unified ledger, allowing users to work in the same set of books regardless of location or operating system. Fast Company featured the software's UI design in a 2012 article, stating that the design is simple and makes it easy for users to get an at-glance idea of real time financial situations. In 2011, Xero released the Xero Touch mobile apps for iOS and Android devices, allowing users to issue invoices while on-site with clients.



In 2007 Xero received the Overall Partner Solution of the Year for Small Business and also featured as part of a Microsoft case study. The company was the recipient of two Webby Awards in 2009, including the People's Choice Award for Banking/Bill Pay. It was recognized again with a Webby in 2010 for Web Services & Applications. It received a Fast Company Innovation by Design Award in 2012 and was recognized as one of the 10 Smart Financial Apps to Solve Small Business Accounting Challenges by YFS Magazine in 2013. Additional awards in 2013 included the Awesome Application Award from the Sleeter Group and the New Zealand Company of the Year from Hi-Tech. In June 2014, Xero topped the Forbes magazine "100 most innovative growth companies" list. Also in 2014, the company was named one of Macworld's recommended online business accounting apps.



The biggest accounting competitor is QuickBooks, which holds up to 90% of the market of small businesses that already use an accounting application.
There are several SaaS accounting apps in the market, including ZipBooks, LessAccounting, FreeAgent, Kashoo, Saasu, Outright, KashFlow, Aplos Software for nonprofits, and Sage Group.


