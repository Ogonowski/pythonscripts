The United Nations Department of Public Information (DPI) is tasked with raising public awareness and support of the work of the United Nations through strategic communications campaigns, media and relationships with civil society groups. The Department aims to accomplish this through its three Divisions.
The Strategic Communications Division (SCD) formulates and launches global information campaigns on UN issues. The Division also manages its network of 63 UN Information Centres (also known as information services or information offices) around the world in order to disseminate information to the public.
The News and Media Division (NMD) works with news outlets from around the world in all media - television, radio, internet - to disseminate information on the United Nations and its work. This includes media accreditation and liaison for journalists to cover day-to-day operations; providing live coverage, through video, audio and still images, of all official meetings and other important events, then providing written summaries as they are concluded; preserves and makes accessible these visual and audio records; and produces and distributes original content for broadcast.
The Outreach Division (OD) serves the broadest audience - the general public - through special public events, publications, services for visitors including guided tours, library services, and partnerships with educational institutions and non-governmental organizations or NGOs.



DPI works closely with Non-governmental organizations (NGOs) to disseminate information about United Nations issues so the public can better understand the aims and objectives of the organizations. It is one of the members of the United Nations Development Group with its Regional Commissions (ECA, ECE, ECLAC, ESCAP, ESCWA), rotating annually.
Any recognized national or international or multinational NGO can apply if it meets certain minimal criteria.



Aarhus Convention
Declaration of Rio (principle no. 10)
Participation (decision making)



^ http://www.undg.org/index.cfm?P=13
^ UN DPI site, Membership criteria



UNDPI.org NGO Information interchange, created by the NGOs, not the UN.