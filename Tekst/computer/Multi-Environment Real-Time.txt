Multi-Environment Real-Time (MERT) was a hybrid time-sharing/real-time operating system developed in the 1970s at Bell Labs for use in embedded minicomputers (in particular PDP-11s). It was later renamed UNIX Real-Time (UNIX-RT). A version called Duplex Multi Environment Real Time (DMERT) was the operating system for the AT&T 3B20D telephone switching minicomputer, designed for high availability; DMERT was later renamed Unix RTR (Real-Time Reliable).
A "generalization" of Bell Labs' time-sharing operating system Unix, MERT featured a redesigned, modular kernel that was able to run Unix programs as well as privileged real-time processes. These processes' data structures were isolated from other processes, with message passing being the preferred form of interprocess communication (IPC), although shared memory was also implemented. MERT also sported a custom filesystem with special support for large, contiguous, statically sized files, as used in real-time database applications. The design of MERT was influenced by Dijkstra's THE, Hansen's Monitor and IBM's CP-67.
The MERT operating system was a four-layer design, in increasing order of protection:
Kernel: resource allocation of memory, CPU time and interrupts;
kernel-mode processes including I/O device drivers, file manager, swap manager, "root process" that connects the file manager to the disk (usually combined with the swap manager);
operating system supervisor;
user processes.
The standard supervisor was MERT/UNIX, a Unix emulator with an extended system call interface and shell that enabled the use of MERT's custom IPC mechanisms, although an RSX-11 emulator also existed.


