Manchester University Press is the university press of the University of Manchester, England and a publisher of academic books and journals. Manchester University Press has developed into an international publisher. It maintains its links with the University.



Manchester University Press publishes monographs and textbooks for academic teaching in higher education. It produces around 140 new books annually. It also publishes 14 journals.
Areas of expertise are history, politics and international law, literature and theatre studies, and visual culture.
In the United States, MUP books are marketed and distributed by Oxford University Press, in Canada by the University of British Columbia Press, and in Australia by Footprint Books: all other global territories are covered from Manchester itself.



MUP has been actively involved in open access publishing for several years. It is one of thirteen publishers to participate in the Knowledge Unlatched pilot, a global library consortium approach to funding open access books.



MUP was founded in 1904 (as the Publications Committee of the University), initially to publish academic research being undertaken at the Victoria University of Manchester. The office was accommodated in a house in Lime Grove. Distribution was then in the hands of Sherratt & Hughes of Manchester; from 1913 the distributors were Longmans, Green & Co. though this arrangement came to an end in the 1930s.
MUP was founded by James Tait. His successor was Thomas Tout and between them they were in charge for the first 20 years of the Press '  existence. H. M. McKechnie was secretary to the press from 1912 to 1949.
The MUP offices moved several times to make way for other developments within the university. Since 1951 these have been Grove House, Oxford Road, then the former Dental Hospital and thirdly the Old Medical School (pictured above).






Official website