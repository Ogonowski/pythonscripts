Anthony Edgar "Tony" Sale, FBCS (30 January 1931   28 August 2011) was a British electronic engineer, computer programmer, computer hardware engineer, and historian of computing. He led the construction of a Colossus computer replica at The National Museum of Computing at Bletchley Park in England, completed in 2007.



He was educated at Dulwich College in south London, During his adolescence he built George the robot out of Meccano, and continued working on it until it reached a fourth version in 1949, when it was given much media coverage. Sale joined the Royal Air Force in 1949, serving until 1952. During his three years in the RAF, Sale gained a commission and reached the rank of Flying Officer. He was an instructor at RAF Officers Radar School at RAF Debden. Sale worked as an engineer for MI5 under Peter Wright in the 1950s.
Sale was survived by his wife, Margaret, three children and seven grandchildren.



Sale worked with Marconi Research Laboratories, was Technical Director of the British Computer Society and managed the Computer Restoration Project at the Science Museum.
After becoming interested in computers, he joined the British Computer Society (BCS) in 1965 as Associate Member, being elected to Member in 1967, Fellow in 1988 and Honorary Fellow in 1996.He was elected to the Council of the BCS for the period 1967 70. In 1965, was a founder member of the Bedfordshire branch of the BCS and was named Chairman in 1979.
In 1989, Sale was appointed a senior curator at the Science Museum in London and worked with Doron Swade to restore some of the museum's computer holdings. He was part of the group that started the Computer Conservation Society in 1989 and was associated with the Bletchley Park Trust from 1992 onwards. In 1991, he joined the campaign to save Bletchley Park from housing development.
In 1992, he was Secretary to the newly formed Bletchley Park Trust, later unpaid Museums Director in 1994. In 1993 he started the Colossus Rebuild Project, inaugurated in 1994, to rebuild the Colossus computer developed at Dollis Hill in 1943.
Sale lectured on wartime code breaking in the UK, Europe and the US. He was technical adviser for the 2001 film Enigma.
Sale's web site, Codes and Ciphers in the Second World War is a source of information on aspects of World War II code breaking. His booklet Colossus 1943 1996 outlines the breaking of the German Lorenz cipher and his remarkable rebuilding of the Colossus computer.



As a result of his Colossus rebuild work, he was awarded the Comdex IT Personality of the Year for 1997. He also received the 2000 Royal Scottish Society of Arts Silver Medal.
After his death, the British Computer Conservation Society established in 2012 the Tony Sale Award for Computer Conservation and Restoration. It comprises a trophy and a travel bursary.






Tony Sale Web site
BBC obituary
CV
Obituary of Tony Sale, The Guardian, 31 August 2011
Obituary of Tony Sale, The Daily Telegraph, 1 September 2011
British Computing Society obituary