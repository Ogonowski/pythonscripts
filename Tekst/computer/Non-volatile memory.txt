Non-volatile memory, nonvolatile memory, NVM or non-volatile storage is computer memory that can retrieve stored information even after having been power cycled (turned off and back on). Examples of non-volatile memory include read-only memory, flash memory, ferroelectric RAM (F-RAM), most types of magnetic computer storage devices (e.g. hard disk drives, floppy disks, and magnetic tape), optical discs, and early computer storage methods such as paper tape and punched cards.
Non-volatile memory is typically used for the task of secondary storage, or long-term persistent storage. The most widely used form of primary storage today is a volatile form of random access memory (RAM), meaning that when the computer is shut down, anything contained in RAM is lost. However, most forms of non-volatile memory have limitations that make them unsuitable for use as primary storage. Typically, non-volatile memory costs more, provides lower performance, or has worse write endurance than volatile random access memory.
Several companies are working on developing non-volatile memory systems comparable in speed and capacity to volatile RAM. IBM is currently developing MRAM (Magnetoresistive RAM). Not only would such technology save energy, but it would allow for computers that could be turned on and off almost instantly, bypassing the slow start-up and shutdown sequence. In addition, Ramtron International has developed, produced, and licensed ferroelectric RAM (F-RAM), a technology that offers distinct properties from other nonvolatile memory options, including extremely high endurance (exceeding 1016 read/write cycles for 3.3 V devices), ultra low power consumption (since F-RAM does not require a charge pump like other non-volatile memories), single-cycle write speeds, and gamma radiation tolerance. Other companies that have licensed and produced F-RAM technology include Texas Instruments, Rohm, and Fujitsu.
Non-volatile data storage can be categorized in electrically addressed systems (read-only memory) and mechanically addressed systems (hard disks, optical disc, magnetic tape, holographic memory, and such). Electrically addressed systems are expensive, but fast, whereas mechanically addressed systems have a low price per bit, but are slow. Non-volatile memory may one day eliminate the need for comparatively slow forms of secondary storage systems, which include hard disks.




Electrically addressed semiconductor non-volatile memories can be categorized according to their write mechanism. Mask ROMs are factory programmable only, and typically used for large-volume products not required to be updated after manufacture. Programmable read-only memory can be altered after manufacture, but require a special programmer and usually cannot be programmed while in the target system. The programming is permanent and further changes require replacement of the device. Data is stored by physically altering (burning) storage sites in the device.



An EPROM is an erasable ROM that can be changed more than once. However, writing new data to an EPROM requires a special programmer circuit. EPROMs have a quartz window that allows them to be erased with ultraviolet light, but the whole device is cleared at one time. A one-time programmable (OTP) device uses an EPROM chip but omits the quartz window in the package; this is less costly to manufacture. An electrically erasable programmable read-only memory EEPROM uses electrical signals to erase memory. These erasable memory devices require much time to erase data and to write new data; they are not usually configured to be programmed by the processor of the target system. Data is stored by use of floating gate transistors which require special operating voltages to be applied to trap or release electric charge on an insulated control gate for storage sites.




The flash memory chip is a close relative to the EEPROM; it differs in that it can only erase one block or "page" at a time. It is a solid-state chip that maintains stored data without any external power source. Capacity is substantially larger than that of an EEPROM, making these chips a popular choice for digital cameras and desktop PC BIOS chips.
Flash memory devices use two different logical technologies NOR and NAND to map data. NOR flash provides high-speed random access, reading and writing data in specific memory locations; it can retrieve as little as a single byte. NAND flash reads and writes sequentially at high speed, handling data in small blocks called pages, however it is slower on read when compared to NOR. NAND flash reads faster than it writes, quickly transferring whole pages of data. Less expensive than NOR flash at high densities, NAND technology offers higher capacity for the same-size silicon.




Ferroelectric RAM (FeRAM, F-RAM or FRAM) is a random-access memory similar in construction to DRAM but (instead of a dielectric layer like in DRAM) contains a thin ferroelectric film of lead zirconate titanate [Pb(Zr,Ti)O3], commonly referred to as PZT. The Zr/Ti atoms in the PZT change polarity in an electric field, thereby producing a binary switch. Unlike RAM devices, F-RAM retains its data memory when power is shut off or interrupted, due to the PZT crystal maintaining polarity. Due to this crystal structure and how it is influenced, F-RAM offers distinct properties from other nonvolatile memory options, including extremely high endurance (exceeding 1016 read/write cycles for 3.3 V devices), ultra low power consumption (since F-RAM does not require a charge pump like other non-volatile memories), single-cycle write speeds, and gamma radiation tolerance.




Magnetoresistive RAM is one of the newest approaches to non-volatile memory and stores data in magnetic storage elements called magnetic tunnel junctions (MTJ's). MRAM has an especially promising future as it seeks to encompass all the desirable features of the other popular types of memory (non-volatility, infinite endurance, high-speed reading/writing, low cost).
The first generation of MRAM, such as Everspin Technologies' 4 Mbit, utilized field-induced writing. The second generation is developed mainly through two approaches: Thermal Assisted Switching (TAS) which is being developed by Crocus Technology, and Spin Torque Transfer (STT) which Crocus, Hynix, IBM, and several other companies are developing.




Mechanically addressed systems utilize a contact structure ("head") to read and write on a designated storage medium. Since circuitry layout is not a key factor for data density, the amount of storage is typically much larger than for electrically addressed systems.
Since the access time depends on the physical location of the data on the device, mechanically addressed systems may not be "random access" as are electrically addressed semiconductor NVRAM. For example, magnetic tape stores data as a sequence of bits on a long tape; transporting the tape past the read/write head is required to access any part of the storage. Tape media can be removed from the drive and stored, giving indefinite capacity at the cost of the time required to retrieve a dismounted tape.
Hard disk drives use a rotating magnetic disk to store data; access time is longer than for semiconductor memory, but cost per stored data bit is very low. Formerly, removable disk packs were common, allowing storage capacity to be expanded. Optical discs store data by altering a pigment layer on a plastic disk. Read-only and read-write versions are available; removable media again allows indefinite expansion, and some automated systems were used to retrieve and mount disks under direct program control.




There are polymer printed ferroelectric memory.
Thin Film Electronics ("Thinfilm") produces rewriteable non-volatile organic memory based on ferroelectric polymers. Thinfilm successfully demonstrated roll-to-roll printed memories in 2009.
In Thinfilm's organic memory the ferroelectric polymer is sandwiched between two sets of electrodes in a passive matrix. Each crossing of metal lines is a ferroelectric capacitor and defines a memory cell. This gives a non-volatile memory comparable to ferroelectric RAM technologies and offer the same functionality as flash memory.





