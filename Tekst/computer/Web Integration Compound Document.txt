Web Integration Compound Document (WICD) is a W3C candidate standard of the Compound Document Format working group, based on the idea of integrating existing markup language formats in preference to inventing new markup.



W3C WICD Candidate Recommendation