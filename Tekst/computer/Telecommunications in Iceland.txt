Telecommunications in Iceland is a diversified market.









THOR Data Center ehf
DataCell ehf
Verne Global
Basis ehf



The largest Internet service providers in Iceland:
Iceland Telecom (S minn hf)
Vodafone Iceland (Fjarskipti ehf)
HIVE (IP-fjarskipti ehf)
Vortex (Hringi an ehf)



Iceland has numerous internet hosting services:
(1984 ehf)
Advania (Advania hf)
(Dav  og Gol at ehf)
(FlokiNET ehf)
Iceland Telecom (S minn hf)
(Netmi lar ehf)
(Netvistun ehf)
(Neth nnun ehf)
(T lvu j nustan Geymir sf)
Vodafone Iceland (Fjarskipti ehf)
Vortex (Hringi an ehf)



Iceland has an internet exchange point called the Reykjavik Internet Exchange (RIX).



Internet and telephone services rely on submarine communications cables for external traffic:
CANTAT-3
DANICE
Emerald Express
FARICE-1
Greenland Connect















As of 2010 there are 152,895 landlines in use in Iceland of which 132,069 are PSTN. Since the peak number of 196,528 total landlines in 2001 there has been a decrease of 43,633 landlines as of 2009.




As of 2010 there are 341,077 active GSM (2G) and UMTS (3G) subscriptions in use in Iceland. In 2010, all NMT (1G) networks were shut down. Nova was first to offer 4G followed by Siminn.




There are no area codes in Iceland, and all telephone numbers have seven digits. The international dialling code is +354. Due to the Icelandic naming system, people are listed by their first name in the telephone directory, and not by their last name (which is usually patronym, or, rarely, a matronym).




Television in Iceland began in September 1966.






Reykjavik Internet Exchange (English)
Reykjavik Internet Exchange (Icelandic)