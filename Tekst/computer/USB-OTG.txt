USB On-The-Go, often abbreviated to USB OTG or just OTG, is a specification first used in late 2001 that allows USB devices such as digital audio players or mobile phones to act as a host, allowing other USB devices such as USB flash drives, digital cameras, mice or keyboards to be attached to them. Use of USB On-The-Go allows those devices to switch back and forth between the roles of host and client devices. For instance, a mobile phone may read from removable media as the host device, but present itself as a USB Mass Storage Device when connected to a host computer.
In other words, USB On-The-Go introduces the concept of a device performing both master and slave roles    whenever two USB devices are connected and one of them is a USB On-The-Go device, they establish a communication link. The device controlling the link is called the master or host, while the other is called the slave or peripheral.




Standard USB uses a master/slave architecture; a host acts as master device for the entire bus, and a USB device acts as slave. Devices are designed from the outset to assume one role or the other    computers are generally set up to be hosts, while, for example, printers are normally slaves.
When a device is plugged into the USB bus, the master device, or host, sets up communications with the device and handles service provisioning (the host's software enables or does the needed data handling such as file managing or other desired kind of data communication or function). The host is responsible for all data transfers over the bus, with the devices capable of only signalling (when polled) that they require attention. To transfer data between two devices, for example from a phone to a printer, the host first reads the data from one device, then writes it to the other. That allows the devices to be greatly simplified compared to the host; for example, a mouse contains very little logic and relies on the host to do almost all of the work.
While the master/slave arrangement works for some devices, there are many devices capable of acting as either master or slave depending on what else shares the bus. For instance, a computer printer is normally a slave device, but when a USB flash drive containing images is plugged into the USB port of the printer with no computer present (or at least turned off) it would be useful for the printer to take on the role of host, allowing it to communicate with the flash drive directly and print images from it.
USB On-The-Go recognizes that a device can perform both master and slave roles, and so subtly changes the terminology. With OTG, a device can be either a host when acting as a link master, or a peripheral when acting as a link slave. The choice between host and peripheral roles is handled entirely by which end of the cable the device is connected to. The device connected to the "A" end of the cable at start-up, known as the "A-device", acts as the default host, while the "B" end acts as the default peripheral, known as the "B-device".
After initial startup, setup for the bus operates as it does with the normal USB standard, with the A-device setting up the B-device and managing all communications. However, when the same A-device is plugged into another USB system or a dedicated host becomes available, it can become a slave.
USB On-The-Go does not preclude using a USB hub, but it describes host/peripheral role swapping only for the case of a one-to-one connection where two OTG devices are directly connected. Role swapping does not work through a standard hub, as one device will act as a host and the other as a peripheral until they are disconnected.



USB OTG is a part of a supplement to the Universal Serial Bus (USB) 2.0 specification originally agreed upon in late 2001 and later revised. The latest version of the supplement also defines behavior for an Embedded Host which has targeted abilities and the same USB Standard-A port used by PCs.
SuperSpeed OTG devices, Embedded Hosts and peripherals are supported through the USB On-The-Go and Embedded Host Supplement to the USB 3.0 specification.



The USB On-The-Go and Embedded Host Supplement to the USB 2.0 specification introduced three new communication protocols:
Attach Detection Protocol (ADP) allows an OTG device, embedded host or USB device to determine attachment status in the absence of power on the USB bus, enabling both insertion-based behavior and the capability to display attachment status. It does so by periodically measuring the capacitance on the USB port to determine whether there is another device attached, a dangling cable, or no cable. When a large enough change in capacitance is detected to indicate device attachment, an A-device will provide power to the USB bus and look for device connection. At the same time, a B-device will generate SRP and wait for the USB bus to become powered.
Session Request Protocol (SRP) allows both communicating devices to control when the link's power session is active; in standard USB, only the host is capable of doing so. That allows fine control over the power consumption, which is very important for battery-operated devices such as cameras and mobile phones. The OTG or embedded host can leave the USB link unpowered until the peripheral (which can be an OTG or standard USB device) requires power. OTG and embedded hosts typically have little battery power to spare, so leaving the USB link unpowered helps in extending the battery runtime.
Host Negotiation Protocol (HNP) allows the two devices to exchange their host/peripheral roles, provided both are OTG dual-role devices. By using HNP for reversing host/peripheral roles, the USB OTG device is capable of acquiring control of data-transfer scheduling. Thus, any OTG device is capable of initiating data-transfer over USB OTG bus. The latest version of the supplement also introduced HNP polling, in which the host device periodically polls the peripheral during an active session to determine whether it wishes to become a host.
The main purpose of HNP is to accommodate users who have connected the A and B devices (see below) in the wrong direction for the task they want to perform. For example, a printer is connected as the A-device (host), but cannot function as the host for a particular camera, since it does not understand the camera's representation of print jobs. When that camera knows how to talk to the printer, the printer will use HNP to switch to the slave role, with the camera becoming the host so pictures stored on the camera can be printed out without reconnecting the cables. The new OTG protocols cannot pass through a standard USB hub since they are based on electrical signaling via a dedicated wire.
The USB On-The-Go and Embedded Host Supplement to the USB 3.0 specification introduces an additional protocol, Role Swap Protocol (RSP). This achieves the same purpose as HNP (i.e. role swapping) by extending standard mechanisms provided by the USB 3.0 specification. Products following the USB On-The-Go and Embedded Host Supplement to the USB 3.0 specification are also required to follow the USB 2.0 supplement in order to maintain backwards compatibility. SuperSpeed OTG devices (SS-OTG) are required to support RSP. SuperSpeed Peripheral Capable OTG devices (SSPC-OTG) are not required to support RSP since they can only operate at SuperSpeed as a peripheral; they have no SuperSpeed host and so can only role swap using HNP at USB 2.0 data rates.



USB OTG defines two roles for devices: OTG A-device and OTG B-device, specifying which side supplies power to the link, and which initially is the host. The OTG A-device is a power supplier, and an OTG B-device is a power consumer. In the default link configuration, the A-device acts as a USB host with the B-device acting as a USB peripheral. The host and peripheral modes may be exchanged later by using HNP. Because every OTG controller supports both roles, they are often called "Dual-Role" controllers rather than "OTG controllers".
For integrated circuit (IC) designers, an attractive feature of USB OTG is the ability to achieve more USB capabilities with fewer gates. A "traditional" approach includes four controllers, resulting in more gates to test and debug:
USB high speed host controller based on EHCI (a register interface)
Full/low speed host controller based on OHCI (another register interface)
USB device controller, supporting both high and full speeds
Fourth controller to switch the OTG root port between host and device controllers.
Also, most gadgets must be either a host or a device. OTG hardware design merges all of the controllers into one dual-role controller that is somewhat more complex than an individual device controller.



A manufacturer's targeted peripheral list (TPL) serves the aim of focusing a host device toward particular products or applications, rather than toward its functioning as a general-purpose host as is the case for typical PCs. The TPL specifies products supported by the "targeting" host, defining what it needs to support, including the output power, transfer speeds, supported protocols, and device classes. It applies to all targeted hosts, including both OTG devices acting as a host and embedded hosts.






The original USB On-The-Go standard introduced a plug receptacle called mini-AB that was replaced by micro-AB in later revisions (Revision 1.4 onwards). It could accept either a mini-A plug or a mini-B plug, while mini-A adapters allowed connection to standard-A USB cables coming from peripherals. The standard OTG cable had a mini-A plug on one end and a mini-B plug on the other end (it could not have two plugs of the same type).
The device with a mini-A plug inserted became an OTG A-device, and the device with a mini-B plug inserted became a B-device (see above). The type of plug inserted was detected by the state of the ID pin (the mini-A plug's ID pin was grounded, while the mini-B plug's was floating).
Pure mini-A plugs also existed, used where a compact host port was needed, but OTG was not supported.



With the introduction of the USB micro plug, a new plug receptacle called Micro-AB was also introduced. It can accept either a Micro-A plug or a Micro-B plug. Micro-A Adapters allow for connection to Standard-A plug type USB cables, as used on standard USB 2.0 Devices. An OTG product must have a single Micro-AB receptacle and no other USB receptacles.
An OTG cable has a micro-A plug on one end, and a micro-B plug on the other end (it cannot have two plugs of the same type). OTG adds a fifth pin to the standard USB connector, called the ID-pin; the micro-A plug has the ID pin grounded, while the ID in the micro-B plug is floating. A device with a micro-A plug inserted becomes an OTG A-device, and a device with a micro-B plug inserted becomes a B-device. The type of plug inserted is detected by the state of the pin ID .
Three additional ID pin states are defined at the nominal resistance values of 124 k , 68 k , and 36.5 k , with respect to the ground pin. These permit the device to work with USB Accessory Charger Adapters that allows the OTG device to be attached to both a charger and another device simultaneously. These three states are used in the cases of:
A charger and either no device or an A-device that is not asserting VBUS (not providing power) are attached. The OTG device is allowed to charge and initiate SRP but not connect.
A charger and an A-device that is asserting VBUS (is providing power) are attached. The OTG device is allowed to charge and connect but not initiate SRP.
A charger and a B-device are attached. The OTG device is allowed to charge and enter host mode.
USB 3.0 introduced a backwards compatible SuperSpeed extension of the Micro-AB receptacle and Micro-A and Micro-B plugs. They contain all pins of the USB 2.0 Micro and use the ID pin to identify the A-device and B-device roles, also adding the SuperSpeed pins.



BlackBerry 10.2 implements Host Mode (like in the BlackBerry Z30 handset). Nokia has implemented USB OTG in many of their Symbian cellphones such as Nokia N8, C6-01, C7, Oro, E6, E7, X7, 603, 701 and 808 Pureview. Some high-end Android phones produced by HTC, Samsung & Sony under Xperia series also have it.
Android version 3.1 or newer supports USB On-The-Go, but not on all devices.
Specifications listed on technology websites   such as GSMArena, PDAdb.net, PhoneScoop, and others   can help determine compatibility. Using the first site as an example, one would locate the page for a given device, and examine the verbiage under Specifications   Comms   USB. If "USB Host" is shown, the device should be capable of supporting OTG-type external USB accessories.
In many of the above implementations, the host device has only a micro-B receptacle rather than a micro-AB receptacle. Although non-standard, micro-B to micro-A receptacle adapters are widely available and used in place of the mandated micro-A receptacle on these devices.




When attached to a PC, an OTG device requires a cable which has a USB Standard-A plug on one end and a Micro-B plug on the other end. In order to attach a peripheral to an OTG device, the peripheral either needs a cable ending in a Micro-A plug, which is inserted into the OTG device's Micro-AB receptacle, or the OTG device itself needs an adapter cable with a Micro-A plug on one end and a Standard-A receptacle on the other. The adapter cable enables any standard USB peripheral to be attached to an OTG device. Attaching two OTG devices together requires either a cable with a Micro-B plug at one end and a Micro-A plug at the other, or can be achieved using a combination of the PC cable and adapter cable.



USB OTG devices are backward-compatible with USB 2.0 (USB 3.0 for SuperSpeed OTG devices) and will behave as standard USB hosts or devices when connected to standard (non-OTG) USB devices. The main exception is that OTG hosts are only required to provide enough power for the products listed on the TPL, which may or may not be enough to connect to a peripheral which is not listed. A powered USB hub may sidestep the issue, if supported, since it will then provide its own power according to either the USB 2.0 or USB 3.0 specifications.
Some incompatibilities in both HNP and SRP were introduced between the 1.3 and 2.0 versions of the On-The-Go supplement, which may lead to interoperability issues when using those protocol versions.




Some devices can use their USB ports to charge built-in batteries, while other devices can detect a dedicated charger and draw more than 500 mA, allowing them to charge more rapidly. OTG devices are allowed to use either option.


