The Air Force Cambridge Research Laboratories were laboratories that were part of the Air Force Research Laboratory, active from 1945 to 2011, following consolidation to Wright-Patterson Air Force Base and Kirtland Air Force Base under the 2005 Base Realignment and Closure Commission.



Originally founded as the Air Force Cambridge Research Center (AFCRC), the lab was a Cold War systems development organization which developed telephone modem communications for a Digital Radar Relay in 1949. Created by General Arnold in 1945, AFCRC participated in Project Space Track and Semi-Automatic Ground Environment development.






Liebowitz, Ruth P. "CHRONOLOGY From the Cambridge Field Stations to the Air Force Geophysics Laboratory 1945-1985". Hanscom Air Force Base, Bedford, Massachusetts 01731: Air Force Geophysics Laboratory. Retrieved 18 December 2013. 
Harrington, John V. (1983). "Radar Data Transmission". Annals of the History of Computing 5 (4): 370.