Pegatron Corporation (Chinese:  ; pinyin: H  shu  li nh  k j  g f n y uxi n g ngs , lit. Grand Mastery United Technology Corporation) is a Taiwanese electronics manufacturing company that develops mainly computing, communications and consumer electronics to branded vendors, but also engages in the development, design and manufacturing of computer peripherals and components. Pegatron's primary products include notebooks, netbook computers, desktop computers, game consoles, handheld devices, motherboards, video cards, LCD TVs, as well as broadband communication products such as smartphones, set-top boxes and cable modems.






Pegatron's principal executive offices and many assets are located in Taiwan. As of March 2010, Pegatron had approximately 5,646 employees stationed in Taiwan, 89,521 in China, 2,400 in the Czech Republic, 200 in each of the United States, Mexico and Japan. Pegatron has manufacturing plants in Taiwan, the Czech Republic, Mexico, and China, and customer service centers in the United States and Japan.






As part of the corporate restructuring of Asustek starting in 2007, Pegatron acquired Unihan Corporation (Chinese:  ) from Asustek in January 2008. Since 2008, the Unihan Corporation has been a subsidiary of Pegatron Corporation that designs and manufactures computers, computer peripherals and audio-video products.



Pegatron acquired ASRock (Chinese:  ) in 2010 shortly before it was spun off.



In June 2008, with its PUreCSR corporate responsibility system, Pegatron became a member of the EICC (Electronic Industry Citizenship Coalition), a group of companies in the electronics industry that supports a credible implementation of the Code of Conduct throughout the electronics and information and communications technology supply chain, ensuring safe working conditions, respect and dignity to employees, and environmentally responsible manufacturing processes. Pegatron's corporate responsibility system, PUreCSR (which stands for "P"egatron & "U"nihan "re"duce, "re"use, "re"cycle, "re"covery, "re"place & "re"pair Corporate Social Responsibility), meets the international standards: the ISO 14001 Environmental Management System, the OHSAS 18001 occupational Health and Safety Management System, and the QC 080000 Hazardous Substance Process Management System. On May 18, 2010, the board of directors at Pegatron unanimously approved to appropriate a sum within 0.5% of net income every year to charity.



PEGA Design and Engineering is Pegatron's design team that was originally branched off from the Asustek design team. The PEGA D&E helps Pegatron's clients with product development, including market research, conceptualization, product design, materials study and production.
In addition to 3C (computer, communication, consumer) electronic products, Pegatron designs home appliances and home decor products such as LCD TVs, LED lightings, phones and more 



Led by Alain Lee, PEGA CASA design team (that was originally branched off from the Asus design team) is dedicated to the design and development of non-IT products in addition to the existing IT product design, including notebook PCs, smart phones, e-books, network communication equipment, displays, projectors, cleaning robots and more. These non-IT products include home appliances (PEGA CASA), fashion accessories, vehicle accessories (PEGA MOTORS), building interior design and building materials, multimedia ads and marketing, and cultural businesses.






In December 2014 a BBC investigation exposed poor working conditions and employee mistreatment at Pegatron factories making Apple products near Shanghai. It uncovered staff forced to work eighteen days straight without a day off, workers falling asleep on the production line during 12-hour shifts (sometimes 16 hours), forced overtime and dormitory rooms where 12 workers shared a cramped room.


