The Federal Department of Home Affairs (FDHA, German: Eidgen ssisches Departement des Innern, French: D partement f d ral de l'int rieur, Italian: Dipartimento Federale dell'Interno) is a department of the federal administration of Switzerland and serves as the Swiss ministry of the interior. As of 2012, it is headed by Federal Councillor Alain Berset.



Like the other federal departments, the FDHA is composed of a General Secretariat, several Federal Offices, and a number of other and affiliated administrative entities.



The General Secretariat is responsible for planning, coordination and controlling, and coordinates the decision-making process between the federal offices and the Head of Department. It provides consultancy services for the entire department. Its legal service is also responsible for supervising charitable foundations. The Federal Commission against Racism, the Service for Combating Racism and the Office for the Equality of People with Disabilities are also affiliated to the General Secretariat. As of 2008, the General Secretariat is led by Secretary-General Pascal Strupler. It has a staff of 62, annual revenues of CHF 1 million and annual expenditures of CHF 36 million.



The FOGE's main concern are equal rights in the workplace and at home. For 2008, the FOGE declares its priorities to be equal pay, prevention of sexual harassment in the workplace, striking a balance between the requirements of professional and family life and prevention of domestic violence. The Secretariat of the Federal Commission for Women s Issues is also affiliated to this office. As of 2008, the FOGE is led by Director Patricia Schulz. It has a staff of 12, no revenues and annual expenditures of CHF 8 million.



The Federal Office of Culture is active in the areas of cultural promotion and awareness, cultural heritage and the conservation of historic buildings and monuments. It looks after the federal art collection, operates the Swiss National Library with the Swiss Literary Archives and the Graphic Collection, the National Museums and the Confederation s valuable collections. The FOC also supports the arts, design and film-making and promotes the concerns of linguistic and cultural minorities. It is the contact point for queries regarding looted art and the transfer of cultural goods. As of 2008, the FOC is led by Director Jean-Fr d ric Jauslin. It has a staff of 220, annual revenues of CHF 4 million and annual expenditures of CHF 202 million.



The Swiss National Library s task is to collect, preserve, catalogue and make available all printed and electronic publications relating to Switzerland. In addition, the NL also houses a series of special collections, the most important being the Swiss Literary Archives and the Graphic Collection. The D rrenmatt Centre in Neuch tel is also part of the Swiss National Library. As of 2008, the NL is led by Director Marie-Christine Doffey. It has a staff of 126, no revenues and annual expenditures of CHF 34 million.



The Swiss Federal Archives are charged with evaluating, safeguarding, cataloguing and raising public awareness of the Confederation s official documents, which amount to 11.4 terabytes of information as of 2008, or to a bookshelf over 50 kilometres long. The inventory includes original documents such as the Swiss Constitution, deeds, photos, films, recordings and databases. As of 2008, the SFA are led by Director Andreas Kellerhals. They have a staff of 47, no revenues and annual expenditures of CHF 16 million.



MeteoSwiss issues weather forecasts and warnings in the event of bad weather conditions. It gives information on the dangers to disaster protection units, the media and the general public. MeteoSwiss also operates telemetry ground stations, rainfall radars and remote sensing instruments at over 700 locations. Weather models use this data to calculate forecasts up to ten days in advance. As a federal office with an autonomous budget ("FLAG office"), MeteoSwiss sells customised forecasts to businesses and private individuals. As of 2008, MeteoSwiss is led by Director Daniel K. Keuerleber-Burk. It has a staff of 273, annual revenues of CHF 36 million and annual expenditures of CHF 79 million.



The stated aim of the Federal Office of Public Health is to promote and maintain the health of all people living in Switzerland. It seeks to increase awareness of health-related matters so that people can take responsibility for their own health. It also aims at a general improvement in people s health through disease prevention and health protection campaigns and by curing illnesses and alleviating suffering caused by disease and accidents. The FOPH is charged with tackling issues such as epidemiology and infectious diseases, substance abuse and drug prevention, food safety, noise and radiation protection, assessment and monitoring of chemicals and toxic products, stem cell research and bioterrorism, and health and accident insurance. The FOPH is led by Director Pascal Strupler. It has a staff of 581 and has an annual budget of CHF 207 million.



The Federal Statistical Office collects and publishes statistical information on the situation and trends in Switzerland in many different areas of life. It produces publications such as the Statistical Yearbook and the Pocket Statistics, as well as making information available through an internet portal. Since 2009, the FSO is led by Director J rg Marti. It has a staff of 509, annual revenues of CHF 1 million and annual expenditures of CHF 142 million.



The Confederation spends around one quarter of the federal budget on social welfare. In the 2000s this amounted to between CHF 13 and 14 billion. The FSIO is charged with ensuring the reliability of this social insurance system within its areas of responsibility: old age and survivors  insurance (AHV), invalidity insurance (IV), supplementary benefits, occupational pension funds, compensation for loss of earnings for people on national service and women on maternity leave, and family allowances in the agricultural sector. As of 2008, the FSIO is led by Director Yves Rossier. It has a staff of 243, annual revenues of CHF 475 million and annual expenditures of CHF 11,921 million.



The SER is responsible for drafting and implementing national and international federal policy in the areas of further and university education, research and space affairs. It aims to improve the quality of Switzerland as a research location with resources for the Swiss National Science Foundation, academics, non-university research institutes, Switzerland s memberships in international research organisations and cooperation in European and worldwide scientific programmes. Finally, the SER coordinates and finances Swiss space policy. As of 2008, the FSIO is led by State Secretary Mauro Dell Ambrogio. It has a staff of 103, annual revenues of CHF 2 million and annual expenditures of CHF 1,909 million.



The ETH Group is a system of technical universities and research institutes. These include the Swiss Federal Institute of Technology in Zurich (ETH Zurich), the Swiss Federal Institute of Technology in Lausanne (EPFL), the Paul Scherrer Institute in Villigen, the Federal Institute for Forest, Snow and Landscape Research in Birmensdorf, the Federal Laboratory for Materials Testing and Research and the Federal Institute for Environmental Science and Technology in D bendorf. All are managed by the ETH Board, which is made up of eleven leading figures from the fields of science, industry and politics. As of 2008, the ETH Group is led by Fritz Schiesser, President of the ETH Board. The group's institutes have a staff of 12 000 and some 20 000 students and post-graduates. They are accounted for as having no revenues and annual expenditures of CHF 2,154 million.



Swissmedic is the central Swiss supervisory authority for drugs and other therapeutic products. It is an independent public institution, governed by an Agency Council elected by the Federal Council and administratively affiliated to the FDHA. As of 2008, Swissmedic is led by Director J rg Schnetzer. It has a staff of 284, annual revenues of CHF 71.5 million and annual expenditures of CHF 67 million.









Social security in Switzerland