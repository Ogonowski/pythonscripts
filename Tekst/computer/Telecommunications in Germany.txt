Germany's telecommunication system is highly developed. Germany telecommunication market has been fully liberalized since January 1, 1998. Germany is served by an extensive system of automatic telephone exchanges connected by modern networks of fiber-optic cable, coaxial cable, microwave radio relay, and a domestic satellite system; cellular telephone service is widely available, expanding rapidly, and includes roaming service to foreign countries. As a result of intensive capital expenditures since reunification, the formerly antiquated system of the eastern part of the country has been rapidly modernized to the most advanced technology. Deutsche Telekom began rolling out FTTH networks in 10 cities in 2011, following the launch of pilot projects in Hennigsdorf, Braunschweig and Dresden in 2010.



The telephony system employs an extensive system of modern network elements such as digital telephone exchanges, mobile switching centres, media gateways and signalling gateways at the core, interconnected by a wide variety of transmission systems using fibre-optics or Microwave radio relay networks. The access network, which connects the subscriber to the core, is highly diversified with different copper-pair, optic-fibre and wireless technologies. The fixed-line telecommunications market is dominated by the former state-owned monopoly Deutsche Telekom. The market entered a period of decline since the mid 2000s. The market players usually own and operate their own physical network, which is a result of previous state-owned monopoly. Such a player is termed an asset-based carrier. The number of suppliers is rather low as few companies have the necessary ability to supply complex, reliable and geographically extensive networks.



Mobile phone market in Germany is dominated by 4 main cellular operators: Telekom (a subsidiary of Deutsche Telekom and branded as T-Mobile between 2002 and 2010), Vodafone, Telef nica Germany (branded as O2) and E-Plus (bought by Telef nica Germany).
Deutsche Telekom AG launched LTE in late 2010. Network roll out began following the "digital dividend" spectrum auction which ended May 20, 2010. On December 2010 Vodafone Germany commercially launched its first LTE network in rural areas using 800 MHz it won in the May 2010 auction. In late 2011 the company began rolling out the network in metropolitan areas with the federal state s capital D sseldorf has been selected as one location because Vodafone s development center is situated there and because of the city s extensive fibre-optic network, which will be used to connect LTE base stations. Krefeld has been chosen as a second LTE rollout location because it is relatively flat and only a few base stations would be required to cover its roughly 250,000 residents.




Broadcasting in the Federal Republic of Germany is reserved under the Basic Law (constitution) to the states. This means that all public broadcasting is regionalised. National broadcasts must be aired through the national consortium of public broadcasters (ARD) or authorized by a treaty negotiated between the states.




DSL infrastructure is highly developed. Cable internet based on DOCSIS technology was not available till mid-2000s, because the cable television infrastructure was owned by Deutsche Telekom, which promoted DSL and neglected the cable network. It was sold after political pressure a few years ago, and is now owned by Kabel Deutschland and Unitymedia Kabel BW.


