For the United States Air Force use of this facility, see Duluth Air National Guard Base
Duluth International Airport (IATA: DLH, ICAO: KDLH, FAA LID: DLH) is a city-owned, public-use, joint civil-military airport located five nautical miles (9 km) northwest of the central business district of Duluth, a city in Saint Louis County, Minnesota, United States. It serves the Twin Ports area including Superior, Wisconsin. It is mostly used for general aviation but is also served by three commercial airlines. Overall, it is the third busiest airport in Minnesota, behind Minneapolis St. Paul International Airport and Rochester International Airport. However, it is the second busiest commercial passenger airport in Minnesota, behind only Minneapolis St. Paul International Airport.
The Minnesota Air National Guard's 148th Fighter Wing, equipped with F-16C Fighting Falcons, is based at Duluth Air National Guard Base.



The City of Duluth purchased the original property for the airport in 1929 from Saint Louis County. The airport was constructed on 640 acres (2.6 km2) of land with two 2,650-foot (810 m) sod runways. Subsequently, in 1930, the airfield was dedicated as a public airport and was called the Williamson Johnson Municipal Airport.
In 1940, Northwest Airlines begins the first regularly scheduled air service to Duluth. It would be temporarily halted though starting in 1942 due to World War II.
In 1942, the three existing runways were paved. Each runway was 4,000 feet (1,200 m) long, 150 feet (46 m) wide, and at nearly equal angles from each other, 30, 90, and 130 degrees. They were identified as runways 3 21, 9 27, and 13 31 respectively. The Corps of Engineers extended runway 9 27 and Runway 3 21 to 5,699 feet (1,737 m) in 1945. In 1951 the USAF extended Runway 9 27 to 9,000 feet (2,700 m) with a 1,000-foot (300 m) overrun and the control tower was built. Runway 9 27 was completely rebuilt in 1956 and further extended in 1966 to 10,152 feet (3,094 m) in length.
The original terminal building was built in 1954, south of Runway 9 27, on the west side of Runway 3 21 and served the airport for nearly 20 years. The terminal floor area was 14,200 square feet (1,320 m2) with 280 parking spaces.
In 1961, the Duluth Airport Authority Board moves to rename the Williamson Johnson Municipal Airport, the Duluth International Airport.
In 1973, a new Terminal Building and U.S. Customs, International Arrivals Building, were completed east of Runway 13 31 and open for operation. Runway 13 31 was shortened to 2,578 feet (786 m) to accommodate construction of an addition to the International Arrivals building. This resulted in Runway 13 31 being closed as a runway due to obstructions. Runway 13 31 was subsequently re-striped in 1980, decreasing its width to 75 feet (23 m), for use as a taxiway only. In 1989, the newer terminal building and the adjacent structures were connected to form one enclosure. The original terminal building was then converted for use as offices for general aviation, the FAA, and the U.S. Weather Bureau. The 1973 terminal building had its last flight take place on January 13, 2013.
In 2013, a new passenger terminal was built directly in front of the 1973 terminal. This new building solved several problems of the previous terminal building, among them, the tail section of parked airplanes extended too close to the runway due to FAA airspace changes made after the building's completion. This terminal building has restrooms and concessions beyond the TSA security checkpoint, something the previous terminal lost when screening processes were put in place post 9/11. The first flight to leave the new terminal was on January 14, United flight 5292 to Chicago O'Hare.



Construction of a 368 stall parking ramp with skywalk connection to the terminal is slated to be completed by the fall of 2014.



Duluth International Airport covers an area of 3,020 acres (1,220 ha) at an elevation of 1,428 feet (435 m) above mean sea level. It has two runways: 9/27 is 10,162 by 150 feet (3,097 m   46 m) with a concrete surface and 3/21 is 5,718 by 150 feet (1,743 m   46 m) with an asphalt surface.
For the 12-month period ending June 30, 2007, the airport had 67,752 aircraft operations, an average of 185 per day: 76% general aviation, 10% military, 8% air taxi, and 6% scheduled commercial. At that time there were 86 aircraft based at this airport: 60% single-engine, 13% multi-engine, 1% helicopter and 26% military.











