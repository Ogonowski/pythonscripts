German military brothels were set up by the Third Reich during World War II throughout much of occupied Europe for the use of Wehrmacht and SS soldiers. These brothels were generally new creations but in the West they were sometimes set up using existing brothels. Until 1942, there were around 500 military brothels of this kind in Nazi occupied Europe. Often operating in confiscated hotels and guarded by the Wehrmacht, these facilities used to serve travelling soldiers and those withdrawn from the front according to an author.
It is estimated that, along with those in concentration camp brothels, at least 34,140 European women were forced to serve as prostitutes during the German occupation. In many cases in Eastern Europe, the women involved were kidnapped on the streets of occupied cities during German military and police round ups, according to another author.




The Foreign Ministry of the Polish Government in Exile issued a document on May 3, 1941, describing the mass kidnapping raids conducted in Polish cities with the aim of capturing young women for sexual slavery in new brothels attended by German soldiers and officers. At the same time, Polish girls as young as 15, classified as suitable for slave labor and shipped to Germany, were sexually exploited by German soldiers usually at their place of destination.
The Swiss Red Cross mission driver Franz Mawick wrote in 1942 from Warsaw about what he saw: "Uniformed Germans [...] gaze fixedly at women and girls between the ages of 15 and 25. One of the soldiers pulls out a pocket flashlight and shines it on one of the women, straight into her eyes. The two women turn their pale faces to us, expressing weariness and resignation. The first one is about 30 years old. "What is this old whore looking for around here?"   one of the three soldiers laughs. "Bread, sir"   asks the woman. [...] "A kick in the ass you get, not bread"   answers the soldier. Owner of the flashlight directs the light again on the faces and bodies of girls. [...] The youngest is maybe 15 years old [...] They open her coat and start groping her with their lustfull paws. "This one is ideal for bed"   he says.".
In the Soviet Union women were kidnapped by German forces for prostitution as well; one report by International Military Tribunal writes "in the city of Smolensk the German Command opened a brothel for officers in one of the hotels into which hundreds of women and girls were driven; they were mercilessly dragged down the street by their arms and hair



According to a Polish website, women forced into sexual slavery by German authorities sometimes tried to escape. There is at least one known mass escape attempt by Polish and Russian women from a German military brothel located in Norway. After their flight, the women asked the local Lutheran Church for asylum.



A 1977 report contended that women who had been prostitutes before the war were made to register for the military brothels. Ruth Seifert, a professor of sociology at the University of Applied Sciences in Regensburg, maintained that women were forced to work in these brothels, as shown during the Trial of the Major War Criminals before the International Military Tribunal in Nuremberg in 1946.




The Wehrmacht was able to establish a thoroughly bureaucratic system of around 100 new brothels already before 1942, based on an existing system of government-controlled ones   wrote Inse Meinen. The soldiers were given official visitation cards issued by Oberkommando des Heeres and were prohibited from engaging in sexual contact with other French women. In September 1941, General von Brauchitsch suggested that weekly visits for all younger soldiers be considered mandatory to prevent "sexual excesses" among them. The prostitutes had a scheduled medical check-up to slow-down the spread of venereal diseases.
The order to regulate the soldiers' sex lives was issued on 29 July 1940. From that point on, free prostitution was forbidden and persecuted by the police. As before, the prostitutes were paid a nominal fee. The soldiers had to bring up the money themselves from their regular guerdon (recompense).


