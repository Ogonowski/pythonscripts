Near field communication (NFC) is the set of protocols that enable electronic devices to establish radio communication with each other by touching the devices together, or bringing them into proximity to a distance of typically 10cm or less.
Early business models such as advertising and industrial applications were not successful, having been overtaken by alternative technologies such as bar codes or UHF tags, but what distinguishes NFC is that devices are often cloud connected. All NFC-enabled smartphones can be provided with dedicated apps including 'ticket' readers as opposed to the traditional dedicated infrastructure that specifies a particular (often proprietary) standard for stock ticket, access control and payment readers. By contrast all NFC peers can connect to a third party NFC device that acts as a server for any action (or reconfiguration).
Like existing 'proximity card' technologies NFC employs electromagnetic induction between two loop antennae when NFC devices - for example a 'smart phone' and a 'smart poster' - to exchange information, operating within the globally available unlicensed radio frequency ISM band of 13.56 MHz on ISO/IEC 18000-3 air interface and at rates ranging from 106 kbit/s to 424 kbit/s.
Each full NFC device can work in three modes: NFC Card Emulation; NFC Reader/Writer; and NFC peer-to-peer (P2P mode):
NFC Card emulation mode enables NFC-enabled devices such as smartphones to act like smart cards, allowing users to perform transactions such as payment or ticketing.
NFC Reader/writer mode enables NFC-enabled devices to read information stored on inexpensive NFC tags embedded in labels or smart posters.
NFC peer-to-peer mode enables two NFC-enabled devices to communicate with each other to exchange information in an adhoc fashion.
NFC tags typically contain data (currently between 96 and 8,192 bytes of memory) and are read-only, but may be rewritable. Applications include secure personal data storage (e.g. debit or credit card information, loyalty program data, Personal Identification Numbers (PINs), contacts). NFC tags can be custom-encoded by their manufacturers or use the industry specifications provided by the NFC Forum, an association with more than 160 members founded in 2004 by Nokia, Philips Semiconductors (which became NXP Semiconductors in 2006) and Sony were charged with promoting the technology and setting key standards, which includes the definition of four distinct types of tags that provide different communication speeds and capabilities in terms of flexibility, memory, security, data retention and write endurance. The Forum also promotes NFC and certifies device compliance. There can be secure communications by applying encryption algorithm as it is done for Credit Card and if it fits the criteria for being considered a personal area network.
NFC standards cover communications protocols and data exchange formats and are based on existing radio-frequency identification (RFID) standards including ISO/IEC 14443 and FeliCa. The standards include ISO/IEC 18092 and those defined by the NFC Forum. In addition to the NFC Forum, the GSMA has also worked to define a platform for the deployment of "GSMA NFC Standards".  within mobile handsets. GSMA's efforts include Trusted Services Manager, Single Wire Protocol, testing and certification, secure element.
A patent licensing program for NFC is currently under deployment by France Brevets, a patent fund created in 2011. This program was under development by Via Licensing Corporation, an independent subsidiary of Dolby Laboratories. This program was terminated in May 2012. A public, platform-independent NFC library is released under the free GNU Lesser General Public License by the name libnfc.
Present and anticipated applications include contactless transactions, data exchange, and simplified setup of more complex communications such as Wi-Fi.



NFC traces its roots back to radio-frequency identification, or RFID. RFID allows a reader to send radio waves to a passive electronic tag for identification, authentication and tracking.
1983 The first patent to be associated with the abbreviation RFID was granted to Charles Walton.
1997 Early form of Near Field Communication patented and first used in Star Wars character toys for Hasbro. Patented by Andrew White and Marc Borrett at Innovision Research and Technology (Patent WO9723060), the device allows data communication between two units when brought into close proximity of each other.
2002 Sony and Philips agreed on establishing a new technology specification and created a technical outline on March 25, 2002.
2004 Nokia, Philips and Sony established the Near Field Communication (NFC) Forum
2006 Initial specifications for NFC Tags
2006 Specification for "SmartPoster" records
2007 Innovision s NFC tags used in the first Near Field Communication consumer trial in the UK, in the Nokia 6131 handset.
2009 In January, NFC Forum released Peer-to-Peer standards to transfer contact, URL, initiate Bluetooth, etc.
2010 Innovision release a suite of designs and patents to enable NFC functionality to be added at low cost to mass-market mobile phones and other devices   launching boom in NFC usage.
2010 Samsung Nexus S: First Android NFC phone shown
2010 The city of Nice in Southern France launches the "Nice City of contactless mobile" project, providing inhabitants with new NFC generation mobile phones and bank cards, and a "bouquet of services" for their daily lives covering transportation, tourism and student's services
2011 Tapit Media launches in Sydney Australia as the first specialized NFC marketing company
2011 Google I/O "How to NFC" demonstrates NFC to initiate a game and to share a contact, URL, app, video, etc.
2011 NFC support becomes part of the Symbian mobile operating system with the release of Symbian Anna version.
2011 Research In Motion is the first company for its devices to be certified by MasterCard Worldwide, the functionality of PayPass
2012 March. EAT., a well-known UK restaurant chain, and Everything Everywhere (Orange Mobile Network Operator), partner on the UK's first nationwide NFC-enabled smartposter campaign, led by Ren  Batsford, head of ICT for EAT., also known for deploying the UK's first nationwide contactless payment solution in 2008. A specially created mobile phone app is triggered when the NFC-enabled mobile phone comes into contact with the smartposter.
2012 Sony introduces the "Smart Tags", which use NFC technology to change modes and profiles on a Sony smartphone at close range, included with the Sony Xperia P Smartphone released the same year.
2013 Samsung and Visa announce major partnership to develop mobile payments.
2013 IBM Scientists from Zurich, in an effort to curb fraud and security breaches have come up with a new mobile authentication security technology based on Near-Field Communication (NFC). IBM's new technology works on similar principles to that of a dual-factor authentication security measure.
2014 AT&T, Verizon and T-Mobile released Softcard (formally ISIS mobile wallet). It runs on NFC-enabled Android phones and iPhone4 and iPhone5 when an external NFC case is attached.
2014 Apple introduced Apple Pay for NFC-enabled mobile payment on iPhone 6, iPhone 6 Plus, and the Apple Watch, which was released on April 24, 2015.
NFC was approved as an ISO/IEC standard on December 8, 2003 and later as an ECMA standard.



NFC is a set of short-range wireless technologies, typically requiring a distance of 10 cm or less. NFC operates at 13.56 MHz on ISO/IEC 18000-3 air interface and at rates ranging from 106 kbit/s to 424 kbit/s. NFC always involves an initiator and a target; the initiator actively generates an RF field that can power a passive target. This enables NFC targets to take very simple form factors such as tags, stickers, key fobs, or cards that do not require batteries. NFC peer-to-peer communication is possible, provided both devices are powered.
NFC tags contain data and are typically read-only, but may be rewriteable. They can be custom-encoded by their manufacturers or use the specifications provided by the NFC Forum, an industry association charged with promoting the technology and setting key standards. The tags can securely store personal data such as debit and credit card information, loyalty program data, PINs and networking contacts, among other information. The NFC Forum defines four types of tags that provide different communication speeds and capabilities in terms of configurability, memory, security, data retention and write endurance. Tags currently offer between 96 and 4,096 bytes of memory.
As with proximity card technology, near-field communication uses magnetic induction between two loop antennas located within each other's near field, effectively forming an air-core transformer. It operates within the globally available and unlicensed radio frequency ISM band of 13.56 MHz. Most of the RF energy is concentrated in the allowed  7 kHz bandwidth range, but the full spectral envelope may be as wide as 1.8 MHz when using ASK modulation.
Theoretical working distance with compact standard antennas: up to 20 cm (practical working distance of about 4 cm)
Supported data rates: 106, 212 or 424 kbit/s (the bit rate 848 kbit/s is not compliant with the standard ISO/IEC 18092)
There are two modes:
Passive communication mode: The initiator device provides a carrier field and the target device answers by modulating the existing field. In this mode, the target device may draw its operating power from the initiator-provided electromagnetic field, thus making the target device a transponder.
Active communication mode: Both initiator and target device communicate by alternately generating their own fields. A device deactivates its RF field while it is waiting for data. In this mode, both devices typically have power supplies.

NFC employs two different codings to transfer data. If an active device transfers data at 106 kbit/s, a modified Miller coding with 100% modulation is used. In all other cases Manchester coding is used with a modulation ratio of 10%.
NFC devices are able to receive and transmit data at the same time. Thus, they can check for potential collisions if the received signal frequency does not match with the transmitted signal's frequency.
Although the communication range of NFC is limited to a few centimeters, NFC alone does not ensure secure communications. In 2006, Ernst Haselsteiner and Klemens Breitfu  described different possible types of attacks and detailed how to leverage NFC's resistance to man-in-the-middle attacks to establish a specific key. Unfortunately, as this technique is not part of the ISO standard, NFC offers no protection against eavesdropping and can be vulnerable to data modifications. Applications may use higher-layer cryptographic protocols (e.g., SSL) to establish a secure channel.
The RF signal for the wireless data transfer can be picked up with antennas. The distance from which an attacker is able to eavesdrop the RF signal depends on numerous parameters, but is typically a small number of meters. Also, eavesdropping is highly affected by the communication mode. A passive device that doesn't generate its own RF field is much harder to eavesdrop on than an active device. An attacker can typically eavesdrop within 10m and 1m for active devices and passive devices, respectively.
Because NFC devices usually include ISO/IEC 14443 protocols, the relay attacks described are also feasible on NFC. For this attack the adversary has to forward the request of the reader to the victim and relay back its answer to the reader in real time, in order to carry out a task pretending to be the owner of the victim's smart card. This is similar to a man-in-the-middle attack. For more information see a survey of practical relay attack concepts. One of libnfc code examples demonstrates a relay attack using only two stock commercial NFC devices. It has also been shown that this attack can be practically implemented using only two NFC-enabled mobile phones.




NFC standards cover communications protocols and data exchange formats, and are based on existing radio-frequency identification (RFID) standards including ISO/IEC 14443 and FeliCa. The standards include ISO/IEC 18092 and those defined by the NFC Forum, which was founded in 2004 by Nokia, Philips Semiconductors (became NXP Semiconductors since 2006) and Sony, and now has more than 160 members.The Forum also promotes NFC and certifies device compliance and whether it fits the criteria for being considered a personal area network. In addition to the NFC Forum, the GSMA has also worked to define a platform for the deployment of "GSMA NFC Standards".  within mobile handsets. GSMA's efforts include "Trusted Services Manager" (PDF). , Single Wire Protocol, testing and certification, "secure element" (PDF). . The GSMA standards surrounding the deployment of NFC protocols (governed by the NFC Forum) on mobile handsets are neither exclusive nor universally accepted. For example, Google's deployment of Host Card Emulation on Android KitKat provides for software control of a universal radio. In this "HCE Deployment". , the NFC protocol is leveraged without the GSMA standards.



NFC is standardized in ECMA-340 and ISO/IEC 18092. These standards specify the modulation schemes, coding, transfer speeds and frame format of the RF interface of NFC devices, as well as initialization schemes and conditions required for data collision-control during initialization for both passive and active NFC modes. Furthermore, they also define the transport protocol, including protocol activation and data-exchange methods. The air interface for NFC is standardized in:
ISO/IEC 18092 / ECMA-340
Near Field Communication Interface and Protocol-1 (NFCIP-1)
ISO/IEC 21481 / ECMA-352
Near Field Communication Interface and Protocol-2 (NFCIP-2)
NFC incorporates a variety of existing standards including ISO/IEC 14443 both Type A and Type B, and FeliCa. NFC-enabled phones work at a basic level with existing readers. In "card emulation mode" an NFC device should transmit, at a minimum, a unique ID number to an existing reader. In addition, the NFC Forum has defined a common data format called NFC Data Exchange Format (NDEF), which can store and transport various kinds of items, ranging from any MIME-typed object to ultra-short RTD-documents, such as URLs. The NFC Forum added the Simple NDEF Exchange Protocol (SNEP) to the spec that allows sending and receiving messages between two NFC-enabled devices.



The GSM Association (GSMA) is the global trade association representing nearly 800 mobile phone operators and more than 200 product and service companies across 219 countries. Many of its members have led NFC trials around the World and are now preparing services for commercial launch.
GSM is involved with several initiatives:
Standard setting: GSMA is developing certification and testing standards to ensure the global interoperability of NFC services.
The Pay-Buy-Mobile initiative' seeks to define a common global approach to using Near Field Communications (NFC) technology to link mobile devices with payment and contactless systems.
On November 17, 2010, after two years of discussions, AT&T, Verizon and T-Mobile launched a joint venture intended to develop a single platform on which technology based on the Near Field Communication (NFC) specifications can be used by their customers to make mobile payments. The new venture, then known as Isis Mobile Wallet and now as Softcard, is designed to usher in the broad deployment of NFC technology, allowing NFC-enabled cell phones to function similarly to credit cards for the 200 million customers using cell phone service provided by any of the three carriers throughout the United States.



StoLPaN ('Store Logistics and Payment with NFC') is a pan-European consortium supported by the European Commission's Information Society Technologies program. StoLPaN will examine the as yet untapped potential for the new kind of local wireless interface, NFC and mobile communication.



The NFC Forum is a non-profit industry association formed on March 18, 2004, by NXP Semiconductors, Sony and Nokia to advance the use of NFC short-range wireless interaction in consumer electronics, mobile devices and PCs. The NFC Forum promotes implementation and standardization of NFC technology to ensure interoperability between devices and services. As of June 2013, the NFC Forum had over 190 member companies.



Other standardization bodies that are involved in NFC include:
ETSI / SCP (Smart Card Platform) to specify the interface between the SIM card and the NFC chipset.
GlobalPlatform to specify a multi-application architecture of the secure element.
EMVCo for the impacts on the EMV payment applications




NFC builds upon RFID systems by allowing two-way communication between endpoints, where earlier systems such as contactless smart cards were one-way only. Since unpowered NFC "tags" can also be read by NFC devices, it is also capable of replacing earlier one-way applications.



NFC devices can be used in contactless payment systems, similar to those currently used in credit cards and electronic ticket smartcards, and allow mobile payment to replace or supplement these systems.
With the release of Android 4.4, Google introduced a new platform support for secure NFC-based transactions through Host Card Emulation (HCE), for payments, loyalty programs, card access, transit passes, and other custom services. With HCE, any app on an Android 4.4 device can emulate an NFC smart card, letting users tap to initiate transactions with an app of their choice. Apps can also use a new Reader Mode so as to act as readers for HCE cards and other NFC-based transactions.
On September 9, 2014, Apple also announced support for NFC-powered transactions as part of their Apple Pay program. Apple stated that their version of NFC payment is more secure than competitors because Apple Pay implements tokenization of its data in order to encrypt it and protect it from unauthorized use.



NFC offers a low-speed connection with extremely simple setup, and can be used to bootstrap more capable wireless connections. For example, the Android Beam software uses NFC to complete the steps of enabling, pairing and establishing a Bluetooth connection when doing a file transfer, disabling Bluetooth automatically on both devices once the desired task has completed. Nokia, Samsung, BlackBerry and Sony have used NFC technology to pair Bluetooth headsets, media players, and speakers with one tap in its NFC-enabled devices. The same principle can be applied to the configuration of Wi-Fi networks. Samsung Galaxy devices have a feature named S-Beam an extension of Android Beam that uses NFC (to share MAC Address and IP addresses) and then uses Wi-Fi Direct to share files and documents. The advantage of using Wi-Fi Direct over Bluetooth is that it permits much faster data transfers, having a speed of up to 300Mbit/s for sharing large files.



NFC can be used in social networking situations, such as sharing contacts, photos, videos or files, and entering multiplayer mobile games.



The NFC Forum promotes the potential for NFC-enabled devices to act as electronic identity documents and keycards. As NFC has a short range and supports encryption, it may be more suitable than earlier, less private RFID systems.



Smartphones equipped with NFC can be paired with NFC Tags or stickers which can be programmed by NFC apps to automate tasks. These programs can allow for a change of phone settings, a text to be created and sent, an app to be launched, or any number of commands to be executed, limited only by the NFC app and other apps on the smartphone.
These applications are perhaps the most practical current uses for NFC since it does not rely on a company or manufacturer but can be utilized immediately by anyone anywhere with an NFC-equipped smartphone and an NFC tag.
The NFC Forum published the Signature Record Type Definition (RTD) 2.0 in 2015 to add integrity and authenticity for NFC Tags. This open standard interoperable specification allows an NFC enabled device to verify the authenticity of NFC tag data and identify the author of the NFC tag.




NFC has been used in video games starting with Skylanders: Spyro's Adventure. With it you buy figurines which are customizable and contain personal data with each figure, so no two figures are exactly alike. The Wii U is the first system to include NFC technology out of the box via the GamePad. It has since been included in the New 3DS range. The amiibo range of accessories utilises NFC technology to unlock extra features in games.



NFC and Bluetooth are both short-range communication technologies that are integrated into mobile phones. NFC operates at slower speeds than Bluetooth, but consumes far less power and doesn't require pairing.
NFC sets up more quickly than standard Bluetooth, but has a lower transfer rate than Bluetooth low energy. With NFC, instead of performing manual configurations to identify devices, the connection between two NFC devices is automatically established in less than a tenth of a second. The maximum data transfer rate of NFC (424 kbit/s) is slower than that of Bluetooth V2.1 (2.1 Mbit/s).
With a maximum working distance of less than 20 cm, NFC has a shorter range, which reduces the likelihood of unwanted interception. That makes NFC particularly suitable for crowded areas where correlating a signal with its transmitting physical device (and by extension, its user) becomes difficult.
In contrast to Bluetooth, NFC is compatible with existing passive RFID (13.56 MHz ISO/IEC 18000-3) infrastructures. NFC requires comparatively low power, similar to the Bluetooth V4.0 low energy protocol. When NFC works with an unpowered device (e.g., on a phone that may be turned off, a contactless smart credit card, a smart poster), however, the NFC power consumption is greater than that of Bluetooth V4.0 Low Energy, since illuminating the passive tag needs extra power.




In 2011, handset vendors released more than 40 NFC-enabled handsets with the Android mobile operating system and support for the Google Wallet mobile payment service. Google Wallet is officially supported on most NFC equipped mobile devices running Android 4.4 Kit Kat which introduced Host Card Emulation for NFC payments. The iPhone 6 line is the first set of handsets from Apple to support NFC, and will use Apple Pay for payment services. BlackBerry devices have also supported NFC using BlackBerry Tag on a number of devices running BlackBerry OS 7.0 and greater. Mastercard has added further NFC support for PayPass for the Android and BlackBerry platforms, enabling PayPass users to make payments using their Android or BlackBerry smartphones in addition to a partnership between Samsung and Visa to include a 'payWave' application on the Galaxy S4 smartphone. Microsoft added native NFC functionality in their mobile OS with Windows Phone 8, as well as the Windows 8 operating system. Microsoft provides the "Wallet hub" in Windows Phone 8 for NFC payment, and can integrate multiple NFC payment services within a single application.




As of April 2011, several hundred NFC trials have been conducted. Some firms have moved to full-scale service deployments, spanning either a single country or multiple countries. Multi-country deployments include Orange's rollout of NFC technology to banks, retailers, transport, and service providers in multiple European countries, and Airtel Africa and Oberthur Technologies deploying to 15 countries throughout Africa.
China telecom (China's 3rd largest mobile operator) made its NFC rollout in November 2013. The company has signed up nearly 12 banks to make their payment apps available on its SIM Cards. China telecom stated that the wallet would also support coupons, membership cards, fuel cards and boarding passes. The company wishes to achieve targets of rolling out 40 NFC phone models and 30 Mn NFC SIMs by 2014.
Softcard (formerly Isis Mobile Wallet), a joint venture from Verizon Wireless, AT&T and T-Mobile, focuses on in-store payments making use of NFC technology. After doing pilots in some regions, they launched across the US recently.
Vodafone recently announced the launch of an NFC-based mobile payment service in Spain. The Vodafone SmartPass service has been developed in partnership with Visa. It enables consumers with an NFC-enabled mobile device to make contactless payments via their SmartPass credit balance at any POS.
OTI, an Israeli company that designs and develops contactless microprocessor based smart card technology, recently signed a major contract to supply NFC-readers to one of its channel partners in the U.S. According to the terms of the agreement, the partner is required to buy $10MM worth of OTI NFC readers over 3 years.
Rogers Communications announced on 7 November that it is launching a new virtual wallet Suretap that works on NFC technology to enable users to make payments with their phone. Rogers now struck a deal with MasterCard that allows users of Suretap to load up gift cards and pre-paid MasterCards from national retailers. The Suretap wallet officially launched in April 2014 and is the 1st of its kind offered in Canada.
According to the Ministry of Industry & Commerce, Sri Lanka's first workforce smartcard, uses NFC.
As of December 13, 2013 Tim Hortons TimmyME BlackBerry 10 Application allows users to link their existing prepaid Tim Card to the app, allowing payment by tapping the NFC-enabled device to a standard contactless terminal. An Android version of the application is expected in January 2014.
Google Wallet allows consumers to store credit card and store loyalty card information in a virtual wallet and then use an NFC-enabled device at terminals that also accept MasterCard PayPass transactions.
Germany, Austria, Finland, New Zealand, Italy, Iran, and Turkey have trialed NFC ticketing systems for public transport. The Lithuanian capital of Vilnius fully replaced paper tickets for public transportation with ISO/IEC 14443 Type A cards on July 1, 2013.
NFC stickers based payments in Australia's Bankmecu and card issuer Cuscal have completed an NFC payment sticker trial, enabling consumers to make contactless payments at Visa payWave terminals using a smart sticker stuck to their phone. Bankmecu now plans to further test the service before launching it to its wider cardholder base in the next few months.
India is implementing NFC based transactions in box offices for ticketing purposes.
A partnership of Google and Equity Bank in Kenya has introduced NFC payment systems for public transport in the Capital city Nairobi under the branding "Beba Pay"


