Internet Connection Sharing (ICS) is the use of a device with Internet access such as 3G cellular service, broadband via Ethernet, or other Internet gateway as an access point for other devices. It was implemented by Microsoft as a feature of its Windows operating system (as of Windows 98 Second Edition and later) for sharing a single Internet connection on one computer between other computers on the same local area network. It makes use of DHCP and network address translation (NAT).
ICS offers configuration for other standard services and some configuration of NAT.



ICS routes TCP/IP packets from a small LAN to the Internet. ICS maps individual IP addresses of local computers to unused port numbers in the TCP/IP stack. Due to the nature of the NAT, IP addresses on the local computer are not visible on the Internet. All packets leaving or entering the LAN are sent from or to the IP address of the external adapter on the ICS host computer.
On the host computer the shared connection is made available to other computers by enabling ICS in Network Connections, and other computers that will connect to and use the shared connection. Therefore, ICS requires at least two network connections. Normally ICS is used when there are several network interface cards installed on the host. In special cases, only one network interface card is required and other connections may be logical. For example, the host may connect to Internet using a modem/router configured in the bridge mode and share the PPPoE connection with ICS.
Starting with Windows XP, there are some improvements to ICS. Internet Connection Sharing is integrated with UPnP, allowing remote discovery and control of the ICS host. It also has a Quality of Service Packet Scheduler component. When an ICS client is on a relatively fast network and the ICS host is connected to the internet through a slow link, Windows may incorrectly calculate the optimal TCP receive window size based on the speed of the link between the client and the ICS host, potentially affecting traffic from the sender adversely. The ICS QoS component sets the TCP receive window size to the same as it would be if the receiver were directly connected to the slow link.
Internet Connection Sharing also includes a local DNS resolver in Windows XP to provide name resolution for all network clients on the home network, including non-Windows-based network devices. ICS is also location-aware, that is, when connected to a domain, the computer can have a Group Policy to restrict the use of ICS but when at home, ICS can be enabled.



While ICS makes use of DHCP, there is no way to review DHCP leases using ICS. The service is also not customizable in terms of which addresses are used for the internal subnet, and contains no provisions for bandwidth limiting or other features. ICS also was designed to connect only to Windows OS computers: computers on other operating systems will require different steps to be able to utilize ICS. The server will normally have the IP address 192.168.0.1 (changeable from the registry or via the IP settings) and will provide NAT services to the whole 192.168.0.x subnet, even if the address on the client was set manually, not by the DHCP server. Windows 7 uses the subnet 192.168.137.x by default.
Besides making sure that the firewall settings are correct, for Windows XP hosts with more than one Ethernet interface card and a wireless WAN connection, bridging the Ethernet interface cards may help eliminating some ICS problems.



Windows XP Service Pack 3 (SP3) introduces a problem that causes loss of ICS connectivity after certain activities (like disconnecting the network cable). Restart of the ICS Windows service is required to resolve the condition. (e.g. net stop SharedAccess & net start SharedAccess)
In Microsoft support article KB951446, the process of restarting the ICS service is explained, but there is no mention of a plan to fix this bug in the future.
In a later Microsoft support article KB951830, more details and a hotfix (Update for Windows XP (KB951830)) are given. This hotfix is not currently being distributed via Windows Update, and so it must be manually installed.



With the advent of home and personal networking in the mid-to-late 2000s, routers, Wi-Fi access points, and other serial communication options have replaced widespread use of Internet Connection Sharing. These alternatives to using Internet Connection Sharing can use either dial-up networking (most focus on the computer with a phone modem to act as a gateway server to the others sharing the connection) or other connection methods.


