A personal supercomputer (PSC) is a high-performance computer system with capabilities and costs between those of standard personal computers and supercomputers. They allow an individual or organization to have access to a significant amount of computing power and are often used for a single purpose. They are typically built by the user, but commercial models are available. Although considerably more expensive than a personal computer, PSCs are affordable to many people.



A common way of building a PSC is syncing several computers with fast networking (commonly dual gigabit Ethernet switching per processor) linked by a gigabit network switch. Some PSCs use clustered GPUs. For example, the TYANPSC uses 40 Xeon processors to achieve 256 gigaflops.



They can be used in medical applications for processing brain and body scans, resulting in faster diagnosis. Another application is persistent aerial surveillance where large amounts of video data needs to be processed and stored.



Cray CX1
Intel iPSC
Nvidia Tesla Personal Supercomputer
SGI Octane
TyanPSC
Chassis Plans M5U-2203 5U GPU Server


