Kissamos (Greek:  ) is a town and municipality in the west of the island of Crete, Greece. It is part of the Chania regional unit and of the former Kissamos Province which covers the northwest corner of the island. The city of Kissamos is also known as Kastelli Kissamou and often known simply as Kastelli after the Venetian castle that was there. It is now a port and fishing harbour, with a regular ferry from the Peloponnese via Kythira. A town museum is located in the old Venetian governor's palace and there have been important archaeological finds in the town, including fine mosaics, dating from the Roman city of Kisamos ( , Latinized as Cisamus). The head town of the municipality (Greek:    ) is Kastelli-Kissamos itself.




Strabo said that ancient Cisamus was dependent on Aptera and was its naval arsenal. The Peutinger Table distinguishes two port towns in Crete called Cisamus, Modern Kissamos (at 35 29 38 N 23 39 25 E) is much further west than where Aptera is now placed (at 35 27 46 N 24 8 31 E). It was excluded already by Pashley in 1837 as being, of the two ancient maritime Cretan cities named Kisamos, the one associated with Aptera. In the past, when the port of Aptera was thought to be present-day Kissamos, some supposed Aptera to be identical with Polyrrhenia, and Kissamos to be the port of Polyrrhenia. However, Strabo and other ancient sources say that Polyrrhenia's port was at Phalasarna on the west coast.
Ancient Cisamus became a Christian bishopric, a suffragan of the metropolitan see of Gortyna. Only two of its first-millennium bishops are named in extant contemporary documents: Theopemptus (according to 18th-century Lequien), Nicetas (according to 20th-century Janin) at the Trullan Council in 692, and Leo at the Second Council of Nicaea in 787. The bishopric is still a residential see of the Orthodox Church of Crete. After the Venetian conquest of Crete in 1212, Cisamus became a Latin Church diocese. The names of more than 20 residential Latin bishops from then until the second half of the 16th century are known. No longer a residential bishopric, Cisamus is today listed by the Catholic Church as a titular see.




The municipality Kissamos was formed at the 2011 local government reform by the merger of the following 3 former municipalities, that became municipal units:
Innachori
Kissamos
Mythimna
The municipal unit Kissamos includes the Gramvousa peninsula (Chernisos Gramvousas    ) in the northwest and the adjacent Gramvousa islets, as well as the islet of Pontikonisi, and the villages of Sfinari, Koukounaras, Polirinia, Platanos, Lousakia, Sirikari, Kallergiania and Kalathena. It forms the extreme western part of the region, and of Crete. It is bordered by Platanias to the East, and by Kantanos-Selino to the south.



The province of Kissamos (Greek:    ) was one of the provinces of the Chania Prefecture. Its territory corresponded with that of the current municipality Kissamos, and the municipal units Kolymvari and Voukolies (partly). It was abolished in 2006.



Manos Katrakis (1908 1984), actor



List of communities of Chania






See Chania Region for maps
Municipality description
Kissamos TV Official Website
GTP description
The district of Kissamos