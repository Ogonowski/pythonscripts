Intel vPro technology is an umbrella marketing term used by Intel for a large collection of computer hardware technologies, including Hyperthreading, Turbo Boost 2.0, VT-x, Trusted Execution Technology, and Intel Active Management Technology (AMT). When the vPro brand was launched (circa 2007), it was identified primarily with AMT, thus some journalists still consider AMT to be the essence of vPro.



Intel vPro is a brand name for a set of PC hardware features. PCs that support vPro have a vPro-enabled processor, a vPro-enabled chipset, and a vPro-enabled BIOS as their main elements.
A vPro PC includes:
Multi-core, multi-threaded Intel Core i5, Intel Core i7, or Xeon 
Intel Active Management Technology (Intel AMT), a set of hardware-based features targeted at businesses. / allow remote access to the PC for management and security tasks, when an OS is down or PC power is off. Note that AMT is not the same as Intel vPro; AMT is only one element of a vPro PC.
Remote configuration technology for AMT, with certificate-based security. Remote configuration can be performed on "bare-bones" systems, before the OS and/or software management agents are installed.
Wired and wireless (laptop) network connection.
Intel Trusted Execution Technology (Intel TXT), which verifies a launch environment and establishes the root of trust, which in turn allows software to build a chain of trust for virtualized environments. Intel TXT also protects secrets during power transitions for both orderly and disorderly shutdowns (a traditionally vulnerable period for security credentials).
Support for IEEE 802.1x, Cisco Self Defending Network (SDN), and Microsoft Network Access Protection (NAP) in laptops, and support for 802.1x and Cisco SDN in desktop PCs. Support for these security technologies allows Intel vPro to store the security posture of a PC so that the network can authenticate the system before the OS and applications load, and before the PC is allowed access to the network.
Intel Virtualization Technology, including Intel VT for memory, CPU, and Directed I/O, to support virtualized environments. Intel VT is hardware-based technology, not software-based virtualization. Intel VT lets you run multiple OSs (traditional virtualization) on the same PC or run a specialized or critical application in a separate space a virtual PC on the physical system in order to help protect the application or privacy of sensitive information.
Execute disable bit that, when supported by the OS, can help prevent some types of buffer overflow attacks.
Support for Microsoft Windows Vista, including Microsoft Windows Vista BitLocker with a Trusted Platform Module version 1.2 and Intel graphics support for Windows Vista Aero graphical user interface.



Intel AMT is the set of management and security features built into vPro PCs that makes it easier for a sys-admin to monitor, maintain, secure, and service PCs. Intel AMT (the management technology) is sometimes mistaken for being the same as Intel vPro (the PC "platform"), because AMT is one of the most visible technologies of an Intel vPro-based PC.
Intel AMT includes:
Encrypted remote power up/down/reset (via wake-on-LAN, or WOL)
Remote/redirected boot (via integrated device electronics redirect, or IDE-R)
Console redirection (via serial over LAN, or SOL)
Preboot access to BIOS settings
Programmable filtering for inbound and outbound network traffic
Agent presence checking
Out-of-band policy-based alerting
Access to system information, such as the PC's universally unique identifier (UUID), hardware asset information, persistent event logs, and other information that is stored in dedicated memory (not on the hard drive) where it is accessible even if the OS is down or the PC is powered off.
Hardware-based management has been available in the past, but it has been limited to auto-configuration (of computers that request it) using DHCP or BOOTP for dynamic IP address allocation and diskless workstations, as well as wake-on-LAN for remotely powering on systems.



Starting with vPro with AMT 6.0, PCs with i5 or i7 processors and embedded Intel graphics, now contains an Intel proprietary embedded VNC server. You can connect out-of-band using dedicated VNC-compatible viewer technology, and have full KVM (keyboard, video, mouse) capability throughout the power cycle   including uninterrupted control of the desktop when an operating system loads. Clients such as VNC Viewer Plus from RealVNC also provide additional functionality that might make it easier to perform (and watch) certain Intel AMT operations, such as powering the computer off and on, configuring the BIOS, and mounting a remote image (IDER).
Note: Not all i5 & i7 Processors with vPro may support KVM capability. This depends on the OEM's BIOS settings as well as if a discrete graphics card is present. Only Intel Integrated HD graphics support KVM ability.



Intel vPro supports encrypted wired and wireless LAN wireless communication for all remote management features for PCs inside the corporate firewall. Intel vPro supports encrypted communication for some remote management features for wired and wireless LAN PCs outside the corporate firewall.



Laptops with vPro include a gigabit network connection and support IEEE 802.11 a/g/n wireless protocols.



Intel vPro PCs support wireless communication to the AMT features.
For wireless laptops on battery power, communication with AMT features can occur when the system is awake and connected to the corporate network. This communication is available if the OS is down or management agents are missing.
AMT out-of-band communication and some AMT features are available for wireless or wired laptops connected to the corporate network over a host OS-based virtual private network (VPN) when laptops are awake and working properly.
A wireless connection operates at two levels: the wireless network interface (WLAN) and the interface driver executing on the platform host. The network interface manages the RF communications connection.
If the user turns off the wireless transmitter/receiver using either a hardware or software switch, Intel AMT cannot use the wireless interface under any conditions until the user turns on the wireless transmitter/receiver.
Intel AMT Release 2.5/2.6 can send and receive management traffic via the WLAN only when the platform is in the S0 power state. It does not receive wireless traffic when the host is asleep or off. If the power state permits it, Intel AMT Release 2.5/2.6 can continue to send and receive out-of-band traffic when the platform is in an Sx state, but only via a wired LAN connection, if one exists.
Release 4.0 and later releases support wireless out-of-band manageability in Sx states, depending on the power setting and other configuration parameters.
Release 7.0 supports wireless manageability on desktop platforms.
When a wireless connection is established on a host platform, it is based on a wireless profile that sets up names, passwords and other security elements used to authenticate the platform to the wireless Access Point. The user or the IT organization defines one or more profiles using a tool such as Intel PROSet/Wireless Software. In release 2.5/6, Intel AMT must have a corresponding wireless profile to receive out-of-band traffic over the same wireless link. The network interface API allows defining one or more wireless profiles using the same parameters as the Intel PROSet/Wireless Software. See Wireless Profile Parameters. On power-up of the host, Intel AMT communicates with the wireless LAN driver on the host. When the driver and Intel AMT find matching profiles, the driver routes traffic addressed to the Intel AMT device for manageability processing. With certain limitations, Intel AMT Release 4.0/1 can send and receive out-of-band traffic without an Intel AMT configured wireless profile, as long as the host driver is active and the platform is inside the enterprise.
In release 4.2, and on release 6.0 wireless platforms, the WLAN is enabled by default both before and after configuration. That means that it is possible to configure Intel AMT over the WLAN, as long as the host WLAN driver has an active connection. Intel AMT synchronizes to the active host profile. It assumes that a configuration server configures a wireless profile that Intel AMT uses in power states other than S0.
When there is a problem with the wireless driver and the host is still powered up (in an S0 power state only), Intel AMT can continue to receive out-of-band manageability traffic directly from the wireless network interface.
For Intel AMT to work with a wireless LAN, it must share IP addresses with the host. This requires the presence of a DHCP server to allocate IP addresses and Intel AMT must be configured to use DHCP.



Intel vPro PCs support encrypted communication while roaming.
vPro PCs version 4.0 or higher support security for mobile communications by establishing a secure tunnel for encrypted AMT communication with the managed service provider when roaming (operating on an open, wired LAN outside the corporate firewall). Secure communication with AMT can be established if the laptop is powered down or the OS is disabled. The AMT encrypted communication tunnel is designed to allow sys-admins to access a laptop or desktop PC at satellite offices where there is no on-site proxy server or management server appliance.
Secure communications outside the corporate firewall depend on adding a new element a management presence server (Intel calls this a "vPro-enabled gateway") to the network infrastructure. This requires integration with network switch manufacturers, firewall vendors, and vendors who design management consoles to create infrastructure that supports encrypted roaming communication. So although encrypted roaming communication is enabled as a feature in vPro PCs version 4.0 and higher, the feature will not be fully usable until the infrastructure is in place and functional.



vPro security technologies and methodologies are designed into the PC's chipset and other system hardware. Because the vPro security technologies are designed into system hardware instead of software, they are less vulnerable to hackers, computer viruses, computer worms, and other threats that typically affect an OS or software applications installed at the OS level (such as virus scan, antispyware, inventory, and other security or management applications).
For example, during deployment of vPro PCs, security credentials, keys, and other critical information are stored in protected memory (not on the hard disk drive), and erased when no longer needed.



According to Intel, it is possible to disable AMT through the BIOS settings, however, there is apparently no way for most users to detect outside access to their PC via the vPro hardware-based technology. Moreover, Sandy Bridge and most likely future chips will have, "...the ability to remotely kill and restore a lost or stolen PC via 3G."



Intel vPro supports industry-standard methodologies and protocols, as well as other vendors' security features:
Intel Trusted Execution Technology (Intel TXT).
Industry-standard Trusted Platform Module version 1.2 (TPM).
Support for IEEE 802.1x, Preboot Execution Environment (PXE), and Cisco Self Defending Network (SDN) in desktop PCs, and additionally Microsoft Network Access Protection (NAP) in laptops.
Execute Disable Bit.
Intel Virtualization Technology (Intel VT(Vt-x+Vt-d)).
Intel VMCS-Intel Virtual Machine Control Structure Shadowing
Intel Platform Trust Technology-PTT
Intel Data Protection Technology
Intel Identity Protection technology
Intel Secure key
Intel Anti-Theft Technology
Intel Boot Guard
Intel OS Guard
Intel Active Management Technology-Intel AMT
Intel Stable Image Platform Program-SIPP
Intel Small Business Advantage-Intel SBA



Intel Boot Guard is a processor feature that prevents the computer from running firmware images not released by the system manufacturer. When turned on, the processors verifies a signature contained in the firmware image before executing it, using the hash of the public half of the signing key, which is fused into the system's Platform Controller Hub (PCH) by the system manufacturer (not by Intel). Intel Boot Guard is an optional processor feature, meaning that it does not need to be activated during the system manufacturing. As a result, Intel Boot Guard, when activated, makes it impossible for end users to install replacement firmware such as Coreboot.



Intel vPro uses several industry-standard security technologies and methodologies to secure the remote vPro communication channel. These technologies and methodologies also improve security for accessing the PC's critical system data, BIOS settings, Intel AMT management features, and other sensitive features or data; and protect security credentials and other critical information during deployment (setup and configuration of Intel AMT) and vPro use.
Transport layer security protocol, including pre-shared key TLS (TLS-PSK) to secure communications over the out-of-band network interface. The TLS implementation uses AES 128-bit encryption and RSA keys with modulus lengths of 2048 bits.
HTTP digest authentication protocol as defined in RFC 2617. The management console authenticates IT administrators who manage PCs with Intel AMT
Single sign-on to Intel AMT with Microsoft Windows domain authentication, based on the Microsoft Active Directory and Kerberos protocols.
A pseudorandom number generator (PRNG) in the firmware of the AMT PC, which generates high-quality session keys for secure communication.
Only digitally signed firmware images (signed by Intel) are permitted to load and execute.
Tamper-resistant and access-controlled storage of critical management data, via a protected, persistent (nonvolatile) data store (a memory area not on the hard drive) in the Intel AMT hardware.
Access control lists for Intel AMT realms and other management functions.



The first release of Intel vPro was built with an Intel Core 2 Duo processor. The current versions of Intel vPro are built into systems with 22 nm Intel 4th Generation Core i5 & i7 processors.
PCs with Intel vPro require specific chipsets. Intel vPro releases are usually identified by their AMT version.



Laptops with Intel vPro require:
For Intel AMT release 9.0 (4th Generation Intel Core i5 and Core i7):
22 nm Intel 4th Generation Core i7 Mobile Processors.
22 nm Intel 4th Generation Core i5 Mobile Processors.
Mobile QM87 Chipsets 

For Intel AMT release 8.0 (3rd Generation Intel Core i5 and Core i7):
32 & 45 nm Intel 3rd Generation Core i7 Mobile Processors.
32 & 45 nm Intel 3rd Generation Core i5 Mobile Processors.
Mobile QM77 & Q77 Chipsets 

For Intel AMT release 4.1 (Intel Centrino 2 with vPro technology):45 nm Intel Core2 Duo processor T, P sequence 8400, 8600, 9400, 9500, 9600; small form factor P, L, U sequence 9300 and 9400, and Quad processor Q9100.
Mobile 45 nm Intel GS45, GM47, GM45 and PM45 Express Chipsets (Montevina with Intel Anti-Theft Technology) with 1066 FSB, 6 MB L2 cache, ICH10M-enhanced.

For Intel AMT release 4.0 (Intel Centrino 2 with vPro technology):45 nm Intel Core2 Duo processor T, P sequence 8400, 8600, 9400, 9500, 9600; small form factor P, L, U sequence 9300 and 9400, and Quad processor Q9100.
Mobile 45 nm Intel GS45, GM47, GM45 and PM45 Express Chipsets (Montevina) with 1066 FSB, 6 MB L2 cache, ICH9M-enhanced.

For Intel AMT release 2.5 and 2.6 (Intel Centrino with vPro technology):Intel Core2 Duo processor T, L, and U 7000 sequence3, 45 nm Intel Core2 Duo processor T8000 and T9000
Mobile Intel 965 (Broadwater-Q) Express Chipset with ICH8M-enhanced.

Note that AMT release 2.5 for wired/wireless laptops and AMT release 3.0 for desktop PCs are concurrent releases.



Desktop PCs with vPro (called "Intel Core 2 with vPro technology") require:
For AMT release 5.0:Intel Core2 Duo processor E8600, E8500, and E8400 ; 45 nm Intel Core2 Quad processor Q9650, Q9550, and Q9400.
Intel Q45 (Eaglelake-Q) Express Chipset with ICH10DO.

For AMT release 3.0, 3.1, and 3.2:Intel Core2 Duo processor E6550, E6750, and E6850; 45 nm Intel Core2 Duo processor E8500, E8400, E8300 and E8200; 45 nm Intel Core2 Quad processor Q9550, Q9450 and Q9300.
Intel Q35 (Bearlake-Q) Express Chipset with ICH9DO.

Note that AMT release 2.5 for wired/wireless laptops and AMT release 3.0 for desktop PCs are concurrent releases.
For AMT release 2.0, 2.1 and 2.2:Intel Core 2 Duo processor E6300, E6400, E6600, and E6700.
Intel Q965 (Averill) Express Chipset with ICH8DO.



There are numerous Intel brands. However, the key differences between vPro (an umbrella marketing term), AMT (a technology under the vPro brand), Intel Core i5 and Intel Core i7 (a branding of a package of technologies), and Core i5 and Core i7 (a processor) are as follows:
The Core i7, the first model of the i series was launched in 2008, and the less-powerful i5 and i3 models were introduced in 2009 and 2010, respectively. The microarchitecture of the Core i series was code-named Nehalem, and the second generation of the line was code-named Sandy Bridge.
Intel Centrino 2 was a branding of a package of technologies that included Wi-Fi and, originally, the Intel Core 2 Duo. The Intel Centrino 2 brand was applied to mobile PCs, such as laptops and other small devices. Core 2 and Centrino 2 have evolved to use Intel's latest 45-nm manufacturing processes, have multi-core processing, and are designed for multithreading.
Intel vPro is a brand name for a set of Intel technology features that can be built into the hardware of the laptop or desktop PC. The set of technologies are targeted at businesses, not consumers. A PC with the vPro brand often includes Intel AMT, Intel Virtualization Technology (Intel VT), Intel Trusted Execution Technology (Intel TXT), a gigabit network connection, and so on. There may be a PC with a Core 2 processor, without vPro features built in. However, vPro features require a PC with at least a Core 2 processor. The technologies of current versions of vPro are built into PCs with some versions of Core 2 Duo or Core 2 Quad processors (45 nm), and more recently with some versions of Core i5 and Core i7 processors.
Intel AMT is part of the Intel Management Engine that is built into PCs with the Intel vPro brand. Intel AMT is a set of remote management and security hardware features that let a sys-admin with AMT security privileges access system information and perform specific remote operations on the PC. These operations include remote power up/down (via wake on LAN), remote / redirected boot (via integrated device electronics redirect, or IDE-R), console redirection (via serial over LAN), and other remote management and security features.











