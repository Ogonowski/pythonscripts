Operation Mo ( MO , Mo Sakusen) or the Port Moresby Operation was the name of the Japanese plan to take control of the Australian Territory of New Guinea during World War II as well as other locations in the South Pacific with the goal of isolating Australia and New Zealand from their ally the United States. The plan was developed by the Imperial Japanese Navy and supported by Admiral Isoroku Yamamoto, the commander-in-chief of the Combined Fleet. The operation ultimately failed.



When the Japanese Navy was planning the New Guinea Campaign (air strikes against Lae and Salamaua, disembarkation in Huon Gulf, New Britain (Rabaul), New Ireland (Kavieng), Finch Harbor (also called Finschhafen), and the capture of Morobe and Buna), strategists envisioned those territories as support points to implement the capture of Port Moresby. The implementation of these operations was assigned to the Japanese Naval task force led by Admiral Ch ichi Nagumo, after completing the Java campaign. Another important step was the occupation of Christmas Island to the south of Java. The Japanese Navy General Staff had been considering Operation Mo since 1938, as a step in the consolidation of the Southern Seas areas in the Greater East Asia Co-Prosperity Sphere (  Dai-t -a Ky eiken).



The Directive of Operation Mo was conceived in 1938, but with no specific time for its execution, pending earlier successes in the southern area during the first and second phases of the conquest.
In April 1942, the operation was organized into four large actions and was approved by the Army and Navy General Staffs:
On 3 May, the Light Task Force occupied the port of Tulagi, near Guadalcanal in the Solomon Islands, to establish a seaplane base and a base for operations in the Coral Sea area. The same force was to take Nauru and Banaba Island (Ocean Island) for their valuable phosphate deposits.
The South Seas Detachment was to disembark in Port Moresby on 7 May, with another force occupying territory in the Louisiade Archipelago for another seaplane base.
Another objective of the South Sea Detachment was the assault on New Caledonia, Fiji, and Samoa. IGHQ assigned a new double objective: capture and secure Port Moresby, in cooperation with the Navy; and seize strategic points of opportunity in eastern New Guinea.
Another important Naval force, departing from Truk, was to pass the Eastern Solomons area to the south, finally advancing toward the west in order to intercept the enemy. Following this, strikes were planned on the coastal cities of Coen, Cooktown and Townsville in Queensland, which were terminal points in the supply line between the United States and Australia. The final object was Thursday Island to the north of Cape York.
The Japanese had one Air Naval land-based fleet detached in Rabaul, Lae, Salamaua and Buna. This Air fleet executed the air strikes against Port Moresby on 5 May and 6 May, in preparation for the Japanese landing on 7 May.
Japanese planners took into account an Allied response to the operation by detaching one task force to the west of parallel between of Rennel and Deboyne Islands and another to the east of same point. These measures would permit a Japanese invasion force to use the Jomard Passage directly to Port Moresby. Japanese naval intelligence also suspected the presence of the American aircraft carrier USS Yorktown in Coral Sea waters during this period.



The Tulagi assault force, led by Rear Admiral Kiyohide Shima, was composed of the following units:
Minelayer-cruiser Okinoshima
Seaplane tender Kiyokawa Maru
Destroyers Kikuzuki, Minazuki, Mochizuki and Y zuki.
two transports
smaller support vessels
The Port Moresby occupation force, led by Rear Admiral Sadamichi Kajioka, was composed of the following units:
Light cruiser Y bari
Destroyers Mutsuki, Yayoi, Uzuki, Asanagi, Oite and Y nagi
Minelayers and sea patrol vessels
Seaplane tender Kamikawa Maru
Minelayer Tsugaru
Supporting these operations and intercepting any Allied interference, Rear Admiral Aritomo Goto commanded:
Light carrier Sh h 
Heavy cruisers Aoba, Kinugasa, Kako and Furutaka
Light cruisers Tenry , Tatsuta
Destroyer Sazanami
During the course of operation, Yamamoto sent the following heavy support force from Truk, led by Rear Admiral Chuichi Hara:
Fleet carriers Sh kaku and Zuikaku
Heavy cruisers My k  and Haguro
Destroyers Asashio, Arashio, Arare, Kager , Shiranui and Kasumi
Auxiliary vessels
Supporting this force was the 25th Air Fleet, (Yokohama Air Corps) led by Rear Admiral Sadayoshi Yamada, based in Rabaul, Lae, Salamaua, Buna and Deboyne Island, composed of 60 Mitsubishi A6M "Zero" fighters, 48 Mitsubishi G3M "Nell" and 26 Aichi E13A "Jake" and Mitsubishi F1M "Pete" reconnaissance seaplanes. This unit bombed Port Moresby on 5 6 May, ahead of the Japanese Army-Navy landing on 7 May.



The Tulagi assault force began their landings on Tulagi on 3 May. On 4 May 1942, troopships bearing the South Seas Detachment set sail southward from Rabaul for Port Moresby. This same day US aircraft from Yorktown attacked the Tulagi assault force, inflicting heavy damage, but were unsuccessful in preventing the occupation of Tulagi, Gavutu, and Tanambogo islands.
Three days later, as a naval engagement appeared to be brewing in the Coral Sea, the Japanese Moresby transports immediately veered back to the north, in order to avoid combat. The resulting Battle of the Coral Sea inflicted significant aircraft losses on the Fourth Fleet, Sh h  was sunk, and Sh kaku was damaged. Air groups from the two carriers, including the relatively undamaged Zuikaku, suffered such sizable losses, it was necessary they return to Japan to re-equip and train.
The Japanese abandoned their plans to land the South Seas Detachment directly at Port Moresby from the sea. The Japanese Army was making new preparations for combat when, on 11 July, High Command ordered the suspension of the projected actions against New Caledonia, Fiji, and Samoa, after the remaining Japanese carrier strength was destroyed at Midway.
These battles prevented the Japanese landings against Port Moresby. Instead the Japanese army commenced an ultimately unsuccessful campaign to take Port Moresby with an overland approach across the Owen Stanley Range via the Kokoda Track.


