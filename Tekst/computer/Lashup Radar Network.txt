The Lashup Radar Network was a United States Cold War radar netting system for air defense surveillance which followed the post-World War II "five-station radar net" and preceded the "high Priority Permanent System". ROTOR was a similar expedient system in the United Kingdom.



United States electronic attack warning began with the 1929 Air Corps "experimenting with a rudimentary early-warning network at Aberdeen Proving Ground" MD, a 1939 networking demonstration at Twin Lights station NJ, and 2 SCR-270 radar stations during the August 1940 "Watertown maneuvers" (NY). When "Pearl Harbor was attacked, [there were 8 CONUS] early-warning stations" (ME, NJ, & 6 in CA), and Oahu's Opana Mobile Radar Station had 1 of 6 SCR-270s. CONUS "Army Radar Station" deployments for World War II were primarily for coastal anti-aircraft defense, e.g., L-1 at Oceanside CA, J-23 at Seaside OR (Tillamook Head), and B-30 at Lompoc CA; and "the AAF...inactivated the aircraft warning network in April 1944." In 1946 "Development of Radar Equipment for Detecting and Countering Missiles of the German A-4 type" was planned, and the Distant Early Warning Line was "first conceived and rejected in 1946". By 1948 there were only 5 AC&W stations, e.g., Twin Lights in June and Montauk's "Air Warning Station #3 on July 5 (cf. SAC radar stations, e.g., at Dallas & Denver Bomb Plots).




The Radar Fence was a planned U.S. Cold War air defense "warning and control system" for $600 million (including $388 million for radars and other equipment) proposed in a report by Maj. Gen. Francis L. "Ankenbrandt and his communications officers" and which was approved by the USAF Chief of Staff on November 21, 1947. The "Radar Fence Plan (code named Project SUPREMACY)" was to be complete by 1953 with 411 radar stations and 18 control centers in the continental United States. Air Defense Command (ADC) rejected Supremacy since "no provision was made in it for the Alaska to Greenland net with flanks guarded by aircraft and picket ships [required] for 3 to 6 hours of warning time", and "Congress failed to act on legislation required to support the proposed system." In the spring and summer of 1947, 3 ADC Aircraft Control and Warning (AC&W) plans had gone unfunded.



In November 1947 ADC "decided to go ahead with implementation [using] AC&W assets ADC possessed." The January 1, 1948, Finletter Commission report "while recognizing the need for a radar early-warning system, cautioned against the extraordinary expense of such a system, if constructed, to provide total coverage." The ADC commander "was ordered on 23 April 1948 to establish with his current resources [the initial networks with] AC&W systems in the Northwestern United States, the Northeastern United States, and the Albuquerque, New Mexico, areas, in that priority." The "first air defense division organization", the 25th Air Division, was established October 25, 1948, "at Silver Lake (Everett), Washington", the 26th Air Division was activated at Mitchell Field NY on November 16, and both were transferred to ADC on April 1, 1949.



"Lashup I" was a stopgap $561,000 program approved in October 1948 by the ADC commander to expand "the five-station radar net then in existence". Preliminary work began by the end of 1948, and L-1 at Dow Air Force Base was complete in June 1949. In the fall of 1949 a 2nd stage of "additional Lashup stations and heavy radar equipment [was] authorized", and after completed in April 1950 the "Lashup net went into operation" on June 1, 1950. After a mid-July direct telephone line was installed between CONAC headquarters and the 26th Air Division HQ ("the beginning of the Air Force air raid warning system"); in August "President Truman had a direct telephone line installed between the Air Force Pentagon post and the White House."



The 44 Lashup radar stations in April 1950 were 23 in the Northeast/Great Lakes areas, 10 in the Pacific Northwest, 5 in/near Southern California, 3 at Albuquerque, 2 at San Francisco, and 1 in Tennessee (Alaska radars were in a separate network.) Stations were geographically grouped by Air Divisions which each had a ground-controlled intercept (GCI) center (e.g., Roslyn Air Warning Station's Manual Control Center in New York).

Palermo Air Force StationPortland Air National Guard BaseFort Meade radar stationHighlands Air Force StationSelfridge AFB radar stationSnelling Air Force Station



Lashup used improved systems that included the Western Electric AN/TPS-1B Radar, which was first used in 1948 (a -1B was at Portland L-33 in March 1948 for warning the nuclear Hanford Site.) L-17 began using a 1949 Bendix AN/CPS-5 Radar, to which a height finder MIT AN/CPS-4 Radar was added by March 9, 1950. Also developed was the General Electric AN/CPS-6 Radar which was at L-12 in 1949. The Bendix AN/FPS-3 Radar used in the Lashup network was ready for installation in late 1950.



The Interim Program and its First Augmentation were planned to replace Lashup with a larger radar network "until the Supremacy plan network could be approved and constructed", and an $85,500,000 March 1949 Congressional bill funded both the Interim Program "for 61 basic radars and 10 control centers to be deployed in 26 months, with an additional ten radars and one control station for Alaska" and the augmentation's additional 15 radars ("essentially Phase II of Supremacy"). The USAF reallocated $50 million to instead implement the program as a "permanent Modified Plan" (modified from Supremacy) to "start construction on the high Priority Permanent System of radars in February 1950 with the first 24 radar sites to be constructed by the end of 1950"--operating in 1951 were P-1 in WA (opened June 1, 1950) and TM-187 in TX.
Early June 1950 exercises "in the 58th Air Division [tbd Lashup sites] indicated insufficient low-altitude coverage," and Maj Gen Morris R. Nelson identified on June 12 that ADC could employ "an American version of CDS", the British command and control system. Congress subsequently passed a "supplemental appropriation" in September 1950 of nearly $40 million for new radar stations and search/height-finder equipment." By November 1950, Ground Observation Corps filter centers (7 in the west, 19 in the east) were being installed, and by November 10 a separate Air Defense Command headquarters at Ent AFB was approved (the Federal Civil Defense Administration was created in December 1950.) On June 13, 1951, the government released $20 million for construction of permanent radar stations, and the "original construction program for the Permanent System" was completed in May 1952.



Phaseout of Lashup radar stations began in January 1952 at Larson AFB (L-29) & Richland (L-30) in Washington that were replaced by Othello AFS (P-40). On December 1, 1953, a few Lashup stations became part of the subsequent "75-station, permanent net", e.g., the Montauk USAF facility was named an Air Force Station when designated LP-45 (the Palermo AFS L-14 reportedly became permanent site LP-54 in 1951.) One station of the Lashup Radar Network remained in 1957 at the end of which ADC operated 182 radar stations (cf. 135 SAGE CDTS sites in 1963, 66 "long-range radars" in 1981, and 41 JSS stations in 1985).


