The United States Army Logistics University (ALU), a subordinate school of the United States Army Combined Arms Support Command, is located at Fort Lee, Virginia.
The Army Logistics University (ALU) officially opened its doors on 2 July 2009, continuing actions directed under the 2005 Base Realignment and Closure Law. The ALU received its genesis from the Army Logistics Management College (ALMC).

The ALU consolidates over 200 courses that were formerly offered by five Army logistics schools. The new organization comprises three distinct colleges. The Army Logistics Management College, the Logistics Leader College, and the Technical Logistics College, and a consolidated Logistics Noncommissioned Officer Academy. All of these organizations have a different focus, but share the same mission to provide military education and training to Civilians, Officers, Warrant Officers and NCOs.
While it is a self-titled "University", Army Logistics University is not a regionally accredited institute of higher education. The school, which despite its name is no different from any other branch-specific training school within the US Army, is not authorized to grant any degrees or graduate certificates in any programs.






Official web site