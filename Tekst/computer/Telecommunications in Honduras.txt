Telecommunication in Honduras started in 1876 when the first telegraph was introduced, continued development with the telephone in 1891, radio in 1928, television in 1959, the Internet in the early 1990s, and cellphones in 1996.




Radio stations: Radio Honduras is the lone government-owned radio network; roughly 300 privately owned radio stations (2007).
Radios: 2.45 million (1997)
The first radio station in Honduras was Tropical Radio, which started operations in 1928.




Televisions stations: Multiple privately owned terrestrial TV networks, supplemented by multiple cable TV networks (2007).
Television sets: 570,000 (1997).
The first TV station in Honduras was Canal 5, which started operations in 1959.
Television in Honduras includes both local and foreign channels, normally distributed by cable.
The Comisi n Nacional de Telecomunicaciones (CONATEL) adopted the ATSC standard for digital terrestrial television broadcasting in January 2007. The first digital high definition TV station, CampusTv, was founded by Universidad de San Pedro Sula.




Calling code: +504
International call prefix: 00
Main lines: 610,000 lines in use, 91st in the world (2012).
Mobile cellular: 7.4 million lines, 93rd in the world (2012).
Telephone system: fixed-line connections increasing but still limited; competition among multiple providers of mobile-cellular services is contributing to a sharp increase in subscribership; beginning in 2003, private sub-operators allowed to provide fixed-lines in order to expand telephone coverage contributing to a small increase in fixed-line teledensity; mobile-cellular subscribership is roughly 100 per 100 persons; connected to Central American Microwave System (2011), a trunk microwave radio relay system that links the countries of Central America and Mexico with each other.
Satellite earth stations: 2 Intelsat (Atlantic Ocean).
Communications cables: Landing point for both:the Americas Region Caribbean Optical-ring System (ARCOS-1) linking US, Mexico, Belize, Guatemala, Honduras, Nicaragua, Costa Rica, Panama, Colombia, Venezuela, Netherlands Antilles, Puerto Rico, Dominican Republic, Turks and Caicos Islands, and the Bahamas, and
the MAYA-1 linking the US, Mexico, Cayman Islands, Honduras, Costa Rica, Panama, and Colombia.

Hondutel, created in 1976, is the state owned telecommunications company in Honduras.
The first cellular company in Honduras, Celtel (now Tigo), started operations in 1996. Megatel (now Claro) started in 2001, Honducel in 2007, and Digicel (now Am rica M vil) in 2008.



Top-level domain: .hn
Internet users: 1.5 million users, 105th in the world; 18.1% of the population, 147th in the world (2012).
Fixed broadband: 64,216 subscriptions, 108th in the world; 0.8% of the population, 144th in the world (2012).
Wireless broadband: 347,217, 103rd in the world; 4.2% of the population, 115th in the world (2012).
Internet hosts: 30,955 hosts, 107th in the world (2012).
IPv4: 143,616 addresses allocated, less than 0.05% of the world total, 17.3 addresses per 1000 people (2012).
Internet Service Providers: 100+ ISPs (2005).
The Internet has been used in Honduras since 1990 and is common in all the major centers of population. Broadband Internet access is also common. All major media have an Internet presence.
Hondutel provides dial-up Internet access.



There are no government restrictions on access to the Internet or credible reports that the government monitors e-mail or Internet chat rooms without judicial oversight. The constitution and laws provide for freedom of speech and press, and the government generally respects these rights in practice. The constitution and law generally prohibit arbitrary interference with privacy, family, home, or correspondence.
Four journalists were killed during 2012, compared with five in 2011. Reports of harassment of journalists and social communicators (persons not employed as journalists, but who serve as bloggers or conduct public outreach for NGOs) continued to rise. There also were multiple reports of intimidation of members of the media and their families. Government officials at all levels denounced violence and threats of violence against members of the media and social communicators. During 2012 the efforts of the Special Victims Unit (SVU) created in January 2011 to address violent crimes against vulnerable communities, including journalists, led to seven arrests and one prosecution in cases involving killings of journalists and social communicators. Members of the media and NGOs stated that the press  self-censored  due to fear of reprisal from organized crime.



Media of Honduras




 This article incorporates public domain material from the CIA World Factbook document "2014 edition".
 This article incorporates public domain material from websites or documents of the United States Department of State.



NIC.hn, registry for the .hn domain.