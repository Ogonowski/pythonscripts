Next Unit of Computing (NUC) is a small form factor PC designed by Intel. Its first generation is based on the Sandy Bridge Celeron CPU. Its second generation CPU is based on the Ivy Bridge Core i3 and Core i5 processors. Its third generation is based on the Haswell architecture. Its motherboard measures 4   4 inches (10.16   10.16 cm).
The barebone kits consist of the board, in a plastic case with a fan, an external power supply and VESA mounting plate. Intel does offer for sale just the NUC motherboards, which have a built-in CPU, although (as of 2013) the price of a NUC motherboard is very close to the corresponding cased kit; third-party cases for the NUC boards are also available.



These tables show some of the key differences between the models of the Intel NUC.



This UCFF motherboard and system kit are codenamed Ski Lake (DCP847SK) and Deep Canyon (DCCP847DY) respectively.



The base UCFF motherboard and kit without Thunderbolt or USB 3 are codenamed Golden Lake (D33217GK) and Ice Canyon (DC3217IY) respectively. The Thunderbolt capable UCFF motherboard and kit are codenamed Campers Lake (D33217CK) and Box Canyon (DC3217BY) respectively. The USB 3 capable UCFF motherboard and kit are codenamed Rend Lake (D53427RK) and Horse Canyon (DC53427HY) respectively.
The stripped-down DC3217BY model has a signature red top cover and no Ethernet. This model, while stocks were still available, generally sold for a deep discount. The absence of Ethernet may be mitigated by using a USB 2.0 to 10/100 fast Ethernet dongle based upon the Kawasaki LSI one-chip adapter (KL5KUSB102, for example). The DC3217BY runs MacOS X (10.9, and any of its updates) flawlessly as the processor's HD4000 is fully supported by MacOS X. Of the two video ports, Thunderbolt/Display Port and HDMI, mDP is the most dependable for MacOS X. Sound is not provided by the basic chassis, but may be provided by a generic USB dongle, C-Media, for example. mDP to SVGA or HDMI to SVGA adapters with integral audio output jack can be made to work for audio output with the appropriate updates to the DSDT



This UCFF motherboard (DN2820FYB) and system kit (DN2820FYKH) model are based on Forest Canyon.
The DN2820FYKH product itself is mis-marked DN2820FYK, but the retail package, all retail documentation, and Intel's web site correctly identify this product as DN2820FYKH. The "H" indicates support for internal 2.5" media, SSD or HD. There is no "non-H" version of this product as it does not include an on-board mSATA connector, hence media which is external to the board is mandatory, and hence the "H" version.
This product introduces for the first time a new 12 volt, 3 ampere "wall wart", in place of the traditional 19 volt, 3.42 ampere power brick and its "Mickey Mouse" ac power cord. Four region-specific plug-on adapters, including North America and three overseas countries, are included in the retail package.
This model is shipped with the BIOS at revision 13, but revision 13 will not recognize most low voltage SO-DIMMs (these are seen as having zero GB capacity) and keyboard escapes to get into the various BIOS functions can be problematic. Also, legacy booting is not supported, and only UEFI booting is supported and even that is problematic.
BIOS revision 48 is available and this solves all current problems, certainly including proper recognition of most low voltage SO-DIMMs, keyboard escapes and legacy booting.
The most reliable way to update to BIOS revision 48 is to download FY0048.BIO from Intel's support site, write it to the root of a small USB flash drive and use the F7 keyboard escape to force BIOS FLASHING. After about five minutes, the BIOS will be updated and all noted problems will be resolved.
However, even with BIOS revision 48, the DSDT, which is found in /sys/firmware/acpi/tables/DSDT, is malformed, and will not compile using most compilers without some editing; so, at least for MacOS X Hackintoshes, the best choice is a DSDT-less installation, which, fortunately, is an option of several recent installation methods.
The Intel Wi-Fi card which is included in this model has proved to be quite good, as is the supplied internal antenna. The 2.5" media bay is somewhat different than earlier bays (perhaps a consequence of excessive cost reduction), but it is still quite functional and reliable, just not very robustly constructed.
Late news: N2820FYKH NUCs with a service tag ending in -103 are reported by Intel to have the new N2830 processor, replacing the older N2820 processor, an unannounced update of the -101 product which had been experiencing USB problems. Yet, the product remains externally identified as an N2820 NUC. The retention of the N2820 product name is believed to be related to a possible requirement for complete safety and emissions testing should the product be renamed N2830 (as in N2830FYKH). Yet, this creates possible issues for those who are expecting N2830 processors (-103) instead of the original N2820 processor (-101). Until all distributor stock of -101 is exhausted it is reasonable to expect that either -101 (N2820) or -103 (N2830) will be supplied, at the distributor's sole discretion, whenever a N2820FYKH product is ordered.



This UCFF motherboard (DE3815TYB) and system kit (DE3815TYK) models based on Thin Canyon.



UCFF motherboard (D34010WYB and D54250WYB) and system kit (D34010WYK/D34010WYKH and D54250WYK/D54250WYKH) models based on Wilson Canyon containing Haswell processors were revealed in June 2013.



In early 2015 a new generation of NUCs, powered by 5th generation Intel processors was released and in Q2 2015 the first NUC with Core i7 processor (NUC5i7RYH) will become available. The currently available collection of 5th generation of NUCs include adaptive/smart performance technology and Turbo Boost Technology 2.0.
UCFF motherboard (NUC5i3RYB, NUC5i5RYB and NUC5i7RYB) and system kit (NUC5i5RYK/NUC5i3RYH, NUC5i5RYK/NUC5i5RYH and NUC5i7RYH) models are based on the Rock Canyon architecture. UCFF motherboard (NUC5i3MYBE and NUC5i5MYBE) and system kit (NUC5i3MYHE and NUC5i5MYHE) models are based on the Maple Canyon architecture.
All models include:
Dual-channel DDR3L SO-DIMM, 1.35V, 1333/1600 MHz, 16 GB maximum
One Gigabit Ethernet port
Internal support for M.2 (either B-Keyed Maple Canyon or M-Keyed Rock Canyon) 22 80 SSD card supporting PCIe 2.0 and SATA 6 Gbit/s
Two USB 3.0 connectors on back panel
Two USB 3.0 connectors on front panel
Two internal USB 2.0 ports via header
Up to 7.1 surround audio via mini HDMI and mini DisplayPort
Headphone/microphone jack on the front panel



This UCFF system kit (NUC5PPYH and NUC5CPYH) models based on 5th generation Celeron and Pentium-branded Braswell 14 nm processor family formerly known as Pinnacle Canyon.
All models include:
One memory channel DDR3L SO-DIMM (204-pin), 1.35 V, 1333/1600 MHz, 8 GB maximum
One Gigabit Ethernet port
802.11ac Wi-Fi (Intel Wireless-AC 3165) and Bluetooth 4.0
Internal support for M.2 (E-Keyed) 22 30 wireless card supporting PCIe 2.0  1, and USB 2.0
Two USB 3.0 connectors on back panel
Two USB 3.0 connectors on front panel
Two internal USB 2.0 ports via header
Up to 7.1 surround audio via HDMI
Headphone/microphone jack on the front panel
Headphone/TOSLINK jack on the rear panel
SDXC slot with UHS-I support on the side
According to the Intel Technical Product Specification, these models have fans.



The NUC was seen by some reviewers as Intel's response to (or adoption of) the Apple Mac Mini format, although it is actually smaller, physically. Given its kit nature, other reviewers have seen it as a more powerful Raspberry Pi, particularly since the NUC boards could be bought without a case.
Most of the third generation NUCs come in two case sizes, one with room for a 2.5-inch drive, and one without. The smaller cases lacking room for a 2.5" drive still have an internal SATA connector (including SATA power). Some larger third-party cases have appeared that can fit such drives.
The Intel case is actively cooled with a fan. Silent PC Review notes that  The original Intel NUC had "the distinction of being the quietest fan-cooled mini-computer we've come across." The NUC D54250WYK [Haswell-based], with the same cooling system, sounds exactly the same. In normal use, you can't hear the fan until your ear is inches from the unit.  Nevertheless, passively cooled third-party cases have appeared on the market as well. Larger or metallic third-party cases provide lower operating temperatures as well.
A review by The Tech Report of the pre-production 2012-vintage NUC found that the NUC would seize up after a few gigabytes were transferred over wireless, and that the problem could be alleviated by better cooling of the NUC case. Intel later increased the default fan speed for production machines through a BIOS update (downloadable from Intel's web site for "early adopters").
Regarding power consumption, in their review of the D54250WYK with a Haswell i5-4250, Silent PC Review concluded that "An idle power level of just 6 W and typical use power barely into two digits is very impressive in one sense; in another sense, it's what you find in current Ultrabooks using similar components."
While Intel only sells barebone kits, some companies are emerging, such as Simply NUC, who have taken it on themselves to provide fully configured NUC systems to end customers.
Other companies have subsequently adopted a form factor similar, but not identical, to Intel's NUC. For example, Gigabyte Technology launched their BRIX series, which attempts to differentiate itself using more powerful components, up to the i7-4770R processor, which embeds Intel Iris Pro Graphics.



NUCs support Windows, as well as many distributions of Linux.
NUCs have also become very popular in the Hackintosh scene. The pre-Haswell Core i3 and Core i5 NUCs will run Mac OS X Mavericks flawlessly. The Haswell-based and later NUCs require a modified Mach kernel; modified versions are freely available from the OSx86 community. Mac OS X only has built-in support for Intel HD Graphics HD 4000 or better (including HD 4400 or HD 5000).
Versions of HD graphics prior to HD 4000, including HD 2500 and pre-HD 2500, can be supported in a very few cases, but never with full acceleration (which would be indicated by a transparent menu bar); there were no NUCs built with HD 3000 graphics, although HD 3000 graphics is fully supported by OS X.



^ Halfacree, G (March 2013). "Intel's Next Unit of Computing". Custom PC (Dennis Publishing) (116): 14 15. 
^ Intel NUC product specifications "Intel NUC", accessed 2013-06-10.
^ a b "Tranquil PC launches a fanless case for Intel s NUC | Chips". Geek.com. 2013-01-29. Retrieved 2014-01-12. 
^ a b "Akasa Rolls Out Tesla H NUC Case". techPowerUp. 2013-12-09. Retrieved 2014-01-12. 
^ Fast (10/100) Ethernet is possible using a Kawasaki LSI USB 2.0 dongle; the vendor provides drivers for all common OSes, including MacOS X.
^ Intel s Haswell  Wilson Canyon  NUC smiles for the cameras
^ Intel NUC Kit D54250WYK Review   The NUC Gets Haswell Power!
^ Tom's Hardware Guide, "3rd Generation of Intel NUC Boards Shown at Computex, 2013-06-07, accessed 2013-06-10.
^ a b c Internal power and data headers are provided for connection to external 2.5" media, with the bottom cover removed; female to female is required.
^ "Intel NUC Products". Retrieved 9 March 2015. 
^ "Intel Core i5 NUCs". Retrieved 9 March 2015. 
^ "Products (Formerly Rock Canyon)". 
^ "Products (Formerly Maple Canyon)". 
^ a b "Intel NUC Kit D54250WYK: Review" (in French). Digitalversus.com. 2013-12-06. Retrieved 2014-01-12. 
^ "Intel NUC review". PC Pro. Retrieved 2014-01-12. 
^ 1/09/14 4:00pm Thursday 4:00pm. "Intel NUC PCs Pack a Ton of Power into a Tiny Little Case". Lifehacker.com. Retrieved 2014-01-12. 
^ Ingredients (2013-07-31). "How to Build a NUC". Maximum PC. Retrieved 2014-01-12. 
^ "Blog - Intel NUC - a mini-PC revolution?". bit-tech.net. 2013-02-25. Retrieved 2014-01-12. 
^ Kirsch, Nathan (2013-09-18). "Intel NUC Kit D54250WYK Review - The NUC Gets Haswell Power! - Page 5 of 7 - Legit ReviewsGeneral NUC Performance". Legitreviews.com. Retrieved 2014-01-12. 
^ Niels Broekhuijsen. "3rd Generation of Intel NUC Boards Shown at Computex". Tomshardware.com. Retrieved 2014-01-12. 
^ "Haswell comes to NUC". silentpcreview.com. 2013-12-07. Retrieved 2014-01-12. 
^ "Haswell comes to NUC". silentpcreview.com. 2013-12-07. Retrieved 2014-01-12. 
^ Gharaei, Vahid (2013-11-22). "Tranquil PC D33217GKE NUC Case Review". techPowerUp. Retrieved 2014-01-12. 
^ "Intel tackles NUC heat issues with fan speed tweak, SSD fix". The Tech Report. 2013-01-29. Retrieved 2014-01-12. 
^ "Haswell comes to NUC". silentpcreview.com. 2013-12-07. Retrieved 2014-01-12. 
^ "GIGABYTE BRIX Pro: A First Look at the Intel i7-4770R with Iris Pro HD 5200". AnandTech. 2014-01-07. Retrieved 2014-01-12. 
^ "Intel NUC Supported Operating Systems". Intel. 2014-12-30. Retrieved 2015-01-14. 



Intel: "Intel NUC", accessed 2013-01-20.