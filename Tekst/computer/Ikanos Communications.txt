Ikanos Communications, Incorporated, is a provider of semiconductor and software products for use in homes. The company s digital subscriber line, communications processors and other products are used in customer premises equipment from network equipment manufacturers and telecommunications service providers.



On December 10, 2001, the founder and chief technology officer of Ikanos, Behrooz Rezvani, announced he was editor of the Institute of Electrical and Electronics Engineers (IEEE) draft standard for Ethernet in the first mile over copper.
On August 24, 2009 Ikanos acquired Conexant's Broadband Access product line for about $54 million in cash. It also announced an investment of $42 million from Tallwood Venture Capital. In June 2010, John Quigley replaced Michael Gulett as chief executive (who left in April). He was the fifth CEO in six years.
A U.S. appeals court on May 25, 2012 revived a shareholder lawsuit accusing Ikanos of failing to properly disclose known defects in its semiconductor chips at the time it was conducting a 2006 stock offering.






Ikanos Corporate Website