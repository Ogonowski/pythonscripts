A penetration test, or sometimes pentest, is a software attack on a computer system that looks for security weaknesses, potentially gaining access to the computer's features and data.
The process typically identifies the target systems and a particular goal then reviews available information and undertakes various means to attain the goal. A penetration test target may be a white box (which provides background and system information) or black box (which provides only basic or no information except the company name). A penetration test can help determine whether a system is vulnerable to attack, if the defenses were sufficient, and which defenses (if any) the test defeated.
Security issues that the penetration test uncovers should be reported to the system owner. Penetration test reports may also assess potential impacts to the organization and suggest countermeasures to reduce risk.
The goals of penetration tests are:
Determine feasibility of a particular set of attack vectors
Identify high-risk vulnerabilities from a combination of lower-risk vulnerabilities exploited in a particular sequence
Identify vulnerabilities that may be difficult or impossible to detect with automated network or application vulnerability scanning software
Assess the magnitude of potential business and operational impacts of successful attacks
Test the ability of network defenders to detect and respond to attacks
Provide evidence to support increased investments in security personnel and technology
Penetration tests are a component of a full security audit. For example, the Payment Card Industry Data Security Standard requires penetration testing on a regular schedule, and after system changes.



By the mid 1960s, growing popularity of time-sharing computer systems that made resources accessible over communications lines created new security concerns. As the scholars Deborah Russell and G. T. Gangemi, Sr. explain, "The 1960s marked the true beginning of the age of computer security." In June 1965, for example, several of the country's leading computer security experts held one of the first major conferences on system security hosted by the government contractor, the System Development Corporation (SDC). During the conference, someone noted that one SDC employee had been able to easily undermine various system safeguards added to SDC's AN/FSQ-32 time-sharing computer system. In hopes that further system security study would be useful, attendees requested "...studies to be conducted in such areas as breaking security protection in the time-shared system." In other words, the conference participants initiated one of the first formal requests to use computer penetration as a tool for studying system security.
At the Spring 1967 Joint Computer Conference, many leading computer specialists again met to discuss system security concerns. During this conference, the computer security experts Willis Ware, Harold Petersen, and Rein Tern, all of the RAND Corporation, and Bernard Peters of the National Security Agency (NSA), all used the phrase "penetration" to describe an attack against a computer system. In a paper, Ware referred to the military's remotely accessible time-sharing systems, warning that "Deliberate attempts to penetrate such computer systems must be anticipated." His colleagues Petersen and Turn shared the same concerns, observing that on-line communication systems "...are vulnerable to threats to privacy," including "deliberate penetration." Bernard Peters of the NSA made the same point, insisting that computer input and output "...could provide large amounts of information to a penetrating program." During the conference, computer penetration would become formally identified as a major threat to online computer systems.
The threat that computer penetration posed was next outlined in a major report organized by the United States Department of Defense (DoD) in late 1967. Essentially, DoD officials turned to Willis Ware to lead a task force of experts from NSA, CIA, DoD, academia, and industry to formally assess the security of time-sharing computer systems. By relying on many papers presented during the Spring 1967 Joint Computer Conference, the task force largely confirmed the threat to system security that computer penetration posed. Ware's report was initially classified, but many of the country's leading computer experts quickly identified the study as the definitive document on computer security. Jeffrey R. Yost of the Charles Babbage Institute has more recently described the Ware report as "...by far the most important and thorough study on technical and operational issues regarding secure computing systems of its time period." In effect, the Ware report reaffirmed the major threat posed by computer penetration to the new online time-sharing computer systems.
To better understand system weaknesses, the federal government and its contractors soon began organizing teams of penetrators, known as tiger teams, to use computer penetration to test system security. Deborah Russell and G. T. Gangemi, Sr. stated that during the 1970s "...'tiger teams' first emerged on the computer scene. Tiger teams were government and industry sponsored teams of crackers who attempted to break down the defenses of computer systems in an effort to uncover, and eventually patch, security holes."
A leading scholar on the history of computer security, Donald MacKenzie, similarly points out that, "RAND had done some penetration studies (experiments in circumventing computer security controls) of early time-sharing systems on behalf of the government." Jeffrey R. Yost of the Charles Babbage Institute, in his own work on the history of computer security, also acknowledges that both the RAND Corporation and the SDC had "engaged in some of the first so-called 'penetration studies' to try to infiltrate time-sharing systems in order to test their vulnerability." In virtually all these early studies, tiger teams successfully broke into all targeted computer systems, as the country's time-sharing systems had poor defenses.
Of early tiger team actions, efforts at the RAND Corporation demonstrated the usefulness of penetration as a tool for assessing system security. At the time, one RAND analyst noted that the tests had "...demonstrated the practicality of system-penetration as a tool for evaluating the effectiveness and adequacy of implemented data security safe-guards." In addition, a number of the RAND analysts insisted that the penetration test exercises all offered several benefits that justified its continued use. As they noted in one paper, "A penetrator seems to develop a diabolical frame of mind in his search for operating system weaknesses and incompleteness, which is difficult to emulate." For these reasons and others, many analysts at RAND recommended the continued study of penetration techniques for their usefulness in assessing system security.
Perhaps the leading computer penetration expert during these formative years was James P. Anderson, who had worked with the NSA, RAND, and other government agencies to study system security. In early 1971, the U.S. Air Force contracted Anderson's private company to study the security of its time-sharing system at the Pentagon. In his study, Anderson outlined a number of major factors involved in computer penetration. Anderson described a general attack sequence in steps:
Find an exploitable vulnerability.
Design an attack around it.
Test the attack.
Seize a line in use.
Enter the attack.
Exploit the entry for information recovery.
Over time, Anderson's description of general computer penetration steps helped guide many other security experts, who relied on this technique to assess time-sharing computer system security.
In the following years, computer penetration as a tool for security assessment became more refined and sophisticated. In the early 1980s, the journalist William Broad briefly summarized the ongoing efforts of tiger teams to assess system security. As Broad reported, the DoD-sponsored report by Willis Ware had "...showed how spies could actively penetrate computers, steal or copy electronic files and subvert the devices that normally guard top-secret information. The study touched off more than a decade of quiet activity by elite groups of computer scientists working for the Government who tried to break into sensitive computers. They succeeded in every attempt."
While these various studies may have suggested that computer security in the U.S. remained a major problem, the scholar Edward Hunt has more recently made a broader point about the extensive study of computer penetration as a security tool. Hunt suggests in a recent paper on the history of penetration testing that the defense establishment ultimately "...created many of the tools used in modern day cyberwarfare," as it carefully defined and researched the many ways that computer penetrators could hack into targeted systems.



The Information Assurance Certification Review Board (IACRB) manages a penetration testing certification known as the Certified Penetration Tester (CPT). The CPT requires that the exam candidate pass a traditional multiple choice exam, as well as pass a practical exam that requires the candidate to perform a penetration test against servers in a virtual machine environment.






Several operating system distributions are geared towards penetration testing. Such distributions typically contain a pre-packaged and pre-configured set of tools. The penetration tester does not have to hunt down each individual tool, which might increase the risk complications such as compile errors, dependencies issues, configuration errors. Also, acquiring additional tools may not be practical in the tester's context.
Popular penetration testing OS examples include:
Kali Linux (which replaced BackTrack in December 2012) based on Debian Linux
Pentoo based on Gentoo Linux
WHAX based on Slackware Linux
Many other specialized operating systems facilitate penetration testing each more or less dedicated to a specific field of penetration testing.
A number of Linux distributions include known OS and Application vulnerabilities, and can be deployed as targets. Such systems help new security professionals try the latest security tools in a lab environment. Examples include Damn Vulnerable Linux(DVL), the OWASP Web Testing Environment (WTW), and Metasploitable.



Metasploit
nmap
w3af



The process of penetration testing may be simplified as two parts:
Discover vulnerabilities combinations of legal operations that let the tester execute an illegal operation
Specify the illegal operation



Legal operations that let the tester execute an illegal operation include unescaped SQL commands, unchanged salts in source-visible projects, human relationships, and old hash or crypto functions. A single flaw may not be enough to enable a critically serious exploit. Leveraging multiple known flaws and shaping the payload in a way that appears as a valid operation is almost always required. Metasploit provides a ruby library for common tasks, and maintains a database of known exploits.
Under budget and time constraints, fuzzing is a common technique that discovers vulnerabilities. It aims to get an un-handled error through random input. The tester uses random input to access less often used code paths. Well-trodden code paths are usually free of errors. Errors are useful because they either expose more information, such as HTTP server crashes with full info trace-backs or are directly usable, such as buffer overflows.
Imagine a website has 100 text input boxes. A few are vulnerable to SQL injections on certain strings. Submitting random strings to those boxes for a while hopefully hits the bugged code path. The error shows itself as a broken HTML page half rendered because of an SQL error. In this case, only text boxes are treated as input streams. However, software systems have many possible input streams, such as cookie and session data, the uploaded file stream, RPC channels, or memory. Errors can happen in any of these input streams. The test goal is to first get an un-handled error, and then understand the flaw based on the failed test case. Testers write an automated tool to test their understanding of the flaw until it is correct. After that, it may become obvious how to package the payload so that the target system triggers its execution. If this is not viable, one can hope that another error produced by the fuzzer yields more fruit. The use of a fuzzer saves time by not checking adequate code paths where exploits are unlikely.



The illegal operation, or payload in Metasploit terminology, can involve a remote mouse controller, webcam peeker, ad popupper, botnet drone, or password hash stealer. Some companies maintain large databases of known exploits and provide products that automatically test target systems for vulnerability:
Nessus
OpenVAS




BackBox
BackTrack
IT risk
ITHC
Kali Linux
Pentoo
Tiger team
Flaw hypothesis methodology
Dell SecureWorks






Hunt, Edward (2012). "US Government Computer Penetration Programs and the Implications for Cyberwar", IEEE Annals of the History of Computing 34(3)
Long, Johnny (2007). Google Hacking for Penetration Testers, Elsevier
MacKenzie, Donald (2001). Mechanizing Proof: Computing, Risk, and Trust. The MIT Press
MacKenzie, Donald and Garrell Pottinger (1997). "Mathematics, Technology, and Trust: Formal Verification, Computer Security, and the U.S. Military", IEEE Annals of the History of Computing 19(3)
McClure, Stuart McClure (2009) Hacking Exposed: Network Security Secrets and Solutions, McGraw-Hill
Russell, Deborah and G. T. Gangemi, Sr. (1991). Computer Security Basics. O'Reilly Media
Yost, Jeffrey R. (2007)."A History of Computer Security Standards," in The History of Information Security: A Comprehensive Handbook, Elsevier



List of Network Penetration Testing software, Mosaic Security Research