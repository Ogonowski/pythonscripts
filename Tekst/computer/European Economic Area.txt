The European Economic Area (EEA) provides for the free movement of persons, goods, services and capital within the internal market of the European Union (EU) between its 28 member states, as well as three of the four member states of the European Free Trade Association (EFTA): Iceland, Liechtenstein and Norway. The Agreement is applied provisionally with respect to Croatia the remaining and most recent EU member state pending ratification of its accession by all EEA parties.
The EEA was established on 1 January 1994 upon entry into force of an agreement between the member states and the EU's predecessors, the European Economic Community and the European Coal and Steel Community. EFTA states which join the EEA participate in the EU's internal market without being EU members, adopting almost all the relevant EU legislation other than laws regarding agriculture and fisheries. The EEA's "decision-shaping" processes enable them to influence and contribute to new EEA policy and legislation from an early stage.
One EFTA member, Switzerland, has not joined the EEA, but has a series of bilateral agreements with the EU which allow it also to participate in the internal market.




In the late 1980s, the EFTA member states, led by Sweden, began looking at options to join the then European Communities. The reasons identified for this are manifold. Many authors cite the economic downturn in the beginning of the 1980s, and the subsequent adoption by the European Union of the Europe 1992 agenda as a primary reason. Arguing from a liberal intergovernmentalist perspective, these authors argue that large multinational corporations in EFTA countries, especially Sweden, pressed for EEC membership under threat of relocating their production abroad. Other authors point to the end of the Cold War, which made joining the EU less politically controversial for neutral countries.
Meanwhile, Jacques Delors, who was president of the European Commission at the time, did not like the idea of the EEC enlarging with more member states, as he feared that it would impede the ability of the Community to complete the internal market reform and establish the monetary union. Delors proposed a European Economic Space (EES) in January 1989, which was later renamed the European Economic Area, as it is known today.
By the time the EEA was established, however, several developments hampered its credibility. First of all, Switzerland rejected the EEA agreement in a national referendum on 6 December 1992 obstructing full EU-EFTA integration within the EEA. Furthermore, Austria had applied for full EEC membership in 1989, and was followed by Finland, Norway, Sweden, and Switzerland between 1991 and 1992 (Norway's EU accession was rejected in a referendum, Switzerland froze its EU application after the EEA agreement was rejected). The fall of the Iron Curtain made the EU less hesitant to accept these highly developed countries as member states, since that would relieve the pressure on the EU's budget when the former communist countries of Central Europe were to join.



The EEA Agreement was signed in Porto on 2 May 1992 by the then seven states of the European Free Trade Association (EFTA), the European Community (EC) and its then 12 member states. On 6 December 1992, Switzerland's voters rejected the ratification of the agreement in a constitutionally mandated referendum, effectively freezing the application for EC membership submitted earlier in the year. Switzerland is instead linked to the EU by a series of bilateral agreements. On 1 January 1995, three erstwhile members of EFTA Austria, Finland and Sweden acceded to the European Union, which had superseded the European Community upon the entry into force of the Maastricht Treaty on 1 November 1993. Liechtenstein's participation in the EEA was delayed until 1 May 1995.
As of 2014 the contracting parties to the EEA are 3 of the 4 EFTA member states and 27 of the 28 EU member states. The 28th and newest EU member, Croatia, finished negotiating their accession to the EEA in November 2013, and since 12 April 2014 is provisionally applying the agreement pending its ratification by all EEA member states.



Besides the 1992 Treaty, 1 amending treaty was signed, as well as 3 treaties to allow for accession of new members of the European Union













Croatia acceded to the EU in July 2013, which obliged them to apply for EEA membership. After Slovenia, Croatia has recovered best from the break-up of the former Yugoslavia and is the second former Yugoslav state to join the EU. According to Eurostat, Croatia has a stable market economy and its GDP per capita in 2010 was 61 per cent of the EU average, exceeding that of four other EU member states. EU accession negotiations were concluded on 30 June 2011, and the Treaty of Accession was signed on 9 December 2011 in Brussels. The Croatian government decided to submit their EEA membership application on 13 September 2012, and negotiations started 15 March 2013 in Brussels, with the aim of achieving simultaneous accession to both the EU and the EEA on 1 July 2013. However, this was not achieved. Accession negotiations were expected to be completed by the autumn of 2013, and on 20 November 2013 it was announced that an enlargement agreement was reached. The text was initialled on 20 December 2013, and following its signature in April 2014 the agreement is being provisionally applied pending ratification by Croatia, all EEA states, and the European Union.
By comparison, following the 2007 enlargement of the EU which saw Bulgaria and Romania acceding to the EU on 1 January 2007, an EEA Enlargement Agreement was not signed until 25 July 2007 and only provisionally entered into force on 1 August 2007. The agreement did not fully enter into force until 9 November 2011. On the other hand, the EEA Agreement was applied on a provisional basis to the 10 acceding countries in May 2004 as from the date of their accession to the EU.
There are five recognised candidates for EU membership that are not already EEA members: Albania (applied 2009), Macedonia (applied 2004), Montenegro (applied 2008, negotiating since June 2012), Serbia (applied 2009, negotiating since January 2014) and Turkey (applied 1987, negotiating since October 2005). Albania and Macedonia have not yet started negotiations to join, nor has the European union set any negotiations start date. Bosnia and Herzegovina and Kosovo are considered potential candidates for membership. Bosnia and Herzegovina has signed a Stabilisation and Association Agreement (SAA) with the EU and its member states, which generally precedes the lodging of a membership application, while Kosovo, whose independence is unrecognised by 5 EU member states, has finalized negotiations on a SAA.



EEA integration, either through EFTA membership or an association agreement directly with the EEA, has been discussed with regard to the microstates of Andorra, Monaco, and San Marino; Israel, Morocco, Turkey, and other ENP partners; and the territories of the Faroe Islands and the Isle of Man.
In mid-2005, representatives of the Faroe Islands hinted at the possibility of their territory joining the EFTA. However, the ability of the Faroes to join is uncertain because, according to Article 56 of the EFTA Convention, only states may become members of the Association. The Faroes, which form part of the Danish Realm, is not a sovereign state, and according to a report prepared for the Faroes Ministry of Foreign Affairs "under its constitutional status the Faroes cannot become an independent Contracting Party to the EEA Agreement due to the fact that the Faroes are not a state." However, the report went on to suggest that it is possible that the "Kingdom of Denmark in respect of the Faroes" could join the EFTA. The Danish Government has stated that the Faroes cannot become an independent member of the EEA as Denmark is already a party to the EEA Agreement. The Faroes already have an extensive bilateral free trade agreement with Iceland, known as the Hoyv k Agreement.




In November 2012, after the Council of the European Union had called for an evaluation of the EU's relations with the sovereign European microstates of Andorra, Monaco and San Marino, which they described as "fragmented", the European Commission published a report outlining options for their further integration into the EU. Unlike Liechtenstein, which is a member of the EEA via the EFTA and the Schengen Agreement, relations with these three states are based on a collection of agreements covering specific issues. The report examined four alternatives to the current situation: 1) a Sectoral Approach with separate agreements with each state covering an entire policy area, 2) a comprehensive, multilateral Framework Association Agreement (FAA) with the three states, 3) EEA membership, and 4) EU membership. The Commission argued that the sectoral approach did not address the major issues and was still needlessly complicated, while EU membership was dismissed in the near future because "the EU institutions are currently not adapted to the accession of such small-sized countries." The remaining options, EEA membership and a FAA with the states, were found to be viable and were recommended by the Commission.
As EEA membership is currently only open to EFTA or EU members, the consent of existing EFTA member states is required for the microstates to join the EEA without becoming members of the EU. In 2011, Jonas Gahr St re, the then Foreign Minister of Norway which is an EFTA member state, said that EFTA/EEA membership for the microstates was not the appropriate mechanism for their integration into the internal market due to their different requirements than large countries such as Norway, and suggested that a simplified association would be better suited for them. Espen Barth Eide, St re's successor, responded to the Commission's report in late 2012 by questioning whether the microstates have sufficient administrative capabilities to meet the obligations of EEA membership. However, he stated that Norway was open to the possibility of EFTA membership for the microstates if they decide to submit an application, and that the country had not made a final decision on the matter. Pascal Schafhauser, the Counsellor of the Liechtenstein Mission to the EU, said that Liechtenstein, another EFTA member state, was willing to discuss EEA membership for the microstates provided their joining did not impede the functioning of the organization. However, he suggested that the option direct membership in the EEA for the microstates, outside both the EFTA and the EU, should be given consideration.
On 18 November 2013 the EU Commission concluded that "the participation of the small-sized countries in the EEA is not judged to be a viable option at present due to the political and institutional reasons", and that Association Agreements were a more feasible mechanism to integrate the microstates into the internal market.



The EEA is based on the same "four freedoms" as the European Community: the free movement of goods, persons, services, and capital among the EEA countries. Thus, the EEA countries that are not part of the EU enjoy free trade with the European Union.
As a counterpart, these countries have to adopt part of the Law of the European Union. However they also contribute to and influence the formation of new EEA relevant policies and legislation at an early stage as part of a formal decision-shaping process.
Agriculture and fisheries are not covered by the EEA. Not being bound by the Common Fisheries Policy is perceived as very important by Norway and Iceland, and a major reason not to join the EU. The Common Fisheries Policy would mean giving away fishing quotas in their waters. They also want to be outside the Common Agriculture Policy, because it means that countries pay fairly much money based on GDP, which subsidise agriculture, which Norway and Iceland do not have so much of.
The EEA countries that are not part of the EU do not bear the financial burdens associated with EU membership, although they contribute financially to the European single market. After the EU/EEA enlargement of 2004, there was a tenfold increase in the financial contribution of the EEA States, in particular Norway, to social and economic cohesion in the Internal Market ( 1167 million over five years). Non-EU countries do not receive any funding from EU policies and development funds.




The non EU members of the EEA (Iceland, Liechtenstein and Norway) have agreed to enact legislation similar to that passed in the EU in the areas of social policy, consumer protection, environment, company law and statistics. These are some of the areas covered by the former European Community (the "first pillar" of the European Union).
The non-EU members of the EEA have no representation in Institutions of the European Union such as the European Parliament or European Commission. This situation has been described as a  fax democracy , with Norway waiting for their latest legislation to be faxed from the Commission.




A Joint Committee consisting of the EEA-EFTA States plus the European Commission (representing the EU) has the function of extending relevant EU law to the non EU members. An EEA Council meets twice yearly to govern the overall relationship between the EEA members.
Rather than setting up pan-EEA institutions, the activities of the EEA are regulated by the European Union institutions, as well as the EFTA Surveillance Authority and the EFTA Court. The EFTA Surveillance Authority and the EFTA Court regulate the activities of the EFTA members in respect of their obligations in the European Economic Area (EEA). The EFTA Surveillance Authority performs the European Commission's role as "guardian of the treaties" for the EFTA countries, while the EFTA Court performs the European Court of Justice's role for those countries.
The original plan for the EEA lacked the EFTA Court or the EFTA Surveillance Authority, as the "EEA court" (which would partly be composed of European Court of Justice members) and the European Commission were to exercise those roles. However, during the negotiations for the EEA agreement, the European Court of Justice informed the Council of the European Union (Opinion 1/91) that they considered that giving the EEA court with respect to EU law that would be part of the EEA law, would be a violation of the treaties, and therefore the current arrangement was developed instead. After having negotiated the Surveillance Authority, the ECJ confirmed its legality in Opinion 1/92.
The EFTA Secretariat is headquartered in Geneva, Switzerland. The EFTA Surveillance Authority has its headquarters in Brussels, Belgium (the same location as the headquarters of the European Commission), while the EFTA Court has its headquarters in Luxembourg (the same location as the headquarters of the European Court of Justice).




The EEA and Norway Grants are the financial contributions of Iceland, Liechtenstein and Norway to reduce social and economic disparities in Europe. In the period from 2004 to 2009,  1.3 billion of project funding is made available for project funding in the 15 beneficiary states in Central and Southern Europe.
Established in conjunction with the 2004 enlargement of the European Economic Area (EEA), which brings together the EU, Iceland, Liechtenstein and Norway in the Internal Market, the EEA and Norway Grants were administered by the Financial Mechanism Office, which is affiliated to the EFTA Secretariat in Brussels.



EudraVigilance
European integration
Free trade areas in Europe
Central European Free Trade Agreement
EFTA

EU enlargement to EFTA states
Accession of Iceland to the European Union
Liechtenstein European Union relations
Norway European Union relations
Switzerland European Union relations

National identity cards in the European Economic Area
Parallel importation
Passports of the European Economic Area
Schengen Agreement
Trade bloc
Eurasian Economic Union






Text of the EEA Agreement
Information about the EEA on the website of the Mission of Norway to the EU
EEA Grants and Norway Grants
The EU and the European Economic Area Europa (web portal): External Relations