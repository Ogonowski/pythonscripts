In electronics, a multi-level cell (MLC) is a memory element capable of storing more than a single bit of information.
MLC NAND flash is a flash memory technology using multiple levels per cell to allow more bits to be stored using the same number of transistors. In single-level cell (SLC) NAND flash technology, each cell can exist in one of two states, storing one bit of information per cell. Most MLC NAND flash memory has four possible states per cell, so it can store two bits of information per cell. This reduces the amount of margin separating the states and results in the possibility of more errors. Multi-level cells which are designed for low error rates are sometimes called enterprise MLC (eMLC).



The primary benefit of MLC flash memory is its lower cost per unit of storage due to the higher data density, and memory-reading software can compensate for a larger bit error rate. The higher error rate necessitates an error correcting code (ECC) that can correct multiple bit errors; for example, the SandForce SF-2500 Flash Controller can correct up to 55 bits per 512-byte sector with an unrecoverable read error rate of less than one sector per 1017 bits read. The most commonly used algorithm is Bose-Chaudhuri-Hocquenghem (BCH code). Other drawbacks of MLC NAND are lower write speeds, lower number of program-erase cycles and higher power consumption compared to SLC flash memory.
A few memory devices go the other direction, and use two cells per bit, to give even lower bit error rates. The Intel 8087 uses two-bits-per-cell technology, and in 1980 was one of the first devices on the market to use multi-level ROM cells. Some solid-state disks use part of a MLC NAND die as if it were single-bit SLC NAND, giving higher write speeds.
Samsung has also launched a type of NAND that stores three bits of information per cell, with eight total voltage states. This is commonly referred to as Triple Level Cell (TLC) and was first seen in the 840 EVO Series SSDs. Samsung refers to this technology as 3-bit MLC. SanDisk X4 flash memory cards are based on four bits per cell technology, which uses 16 states. The negative aspects of MLC are amplified with TLC, but TLC benefits from higher storage density and lower cost.



Flash memory stores data in individual memory cells, which are made of floating-gate transistors. Traditionally, each cell had two possible states, so one bit of data was stored in each cell in so-called single-level cells, or SLC flash memory. SLC memory has the advantage of faster write speeds, lower power consumption and higher cell endurance. However, because SLC memory stores less data per cell than MLC memory, it costs more per megabyte of storage to manufacture. Due to faster transfer speeds and longer life, SLC flash technology is used in high-performance memory cards.



Flash memory
Solid-state drive
StrataFlash






What is NAND Flash
NAND Flash Applications
Linux Memory Technology Devices - NAND
https://web.archive.org/web/20070927204538/http://www.st.com/stonline/products/literature/anp/12672.htm
Open NAND Flash Interface