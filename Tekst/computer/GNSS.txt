A satellite navigation or satnav system is a system of satellites that provide autonomous geo-spatial positioning with global coverage. It allows small electronic receivers to determine their location (longitude, latitude, and altitude/elevation) to high precision (within a few metres) using time signals transmitted along a line of sight by radio from satellites. The signals also allow the electronic receivers to calculate the current local time to high precision, which allows time synchronisation. A satellite navigation system with global coverage may be termed a global navigation satellite system (GNSS).
As of April 2013, only the United States NAVSTAR Global Positioning System (GPS) and the Russian GLONASS are global operational GNSSs. China is in the process of expanding its regional BeiDou Navigation Satellite System into the global Compass navigation system by 2020. The European Union's Galileo is a GNSS in initial deployment phase, scheduled to be fully operational by 2020 at the earliest., India has a regional satellite-based augmentation system, GAGAN, enhancing the accuracy of GPS and GLONASS positions. France and Japan are in the process of developing regional navigation systems.
Global coverage for each system is generally achieved by a satellite constellation of 20 30 medium Earth orbit (MEO) satellites spread between several orbital planes. The actual systems vary, but use orbital inclinations of >50  and orbital periods of roughly twelve hours (at an altitude of about 20,000 kilometres or 12,000 miles).



Satellite navigation systems that provide enhanced accuracy and integrity monitoring usable for civil navigation are classified as follows:
GNSS-1 is the first generation system and is the combination of existing satellite navigation systems (GPS and GLONASS), with Satellite Based Augmentation Systems (SBAS) or Ground Based Augmentation Systems (GBAS). In the United States, the satellite based component is the Wide Area Augmentation System (WAAS), in Europe it is the European Geostationary Navigation Overlay Service (EGNOS), and in Japan it is the Multi-Functional Satellite Augmentation System (MSAS). Ground based augmentation is provided by systems like the Local Area Augmentation System (LAAS).
GNSS-2 is the second generation of systems that independently provides a full civilian satellite navigation system, exemplified by the European Galileo positioning system. These systems will provide the accuracy and integrity monitoring necessary for civil navigation; including aircraft. This system consists of L1 and L2 frequencies for civil use and L5 for system integrity. Development is also in progress to provide GPS with civil use L2 and L5 frequencies, making it a GNSS-2 system. 
Core Satellite navigation systems, currently GPS (United States), GLONASS (Russian Federation), Galileo (European Union) and Compass (China).
Global Satellite Based Augmentation Systems (SBAS) such as Omnistar and StarFire.
Regional SBAS including WAAS (US), EGNOS (EU), MSAS (Japan) and GAGAN (India).
Regional Satellite Navigation Systems such as China's Beidou, India's yet-to-be-operational IRNSS, and Japan's proposed QZSS.
Continental scale Ground Based Augmentation Systems (GBAS) for example the Australian GRAS and the US Department of Transportation National Differential GPS (DGPS) service.
Regional scale GBAS such as CORS networks.
Local GBAS typified by a single GPS reference station operating Real Time Kinematic (RTK) corrections.




Early predecessors were the ground based DECCA, LORAN, GEE and Omega radio navigation systems, which used terrestrial longwave radio transmitters instead of satellites. These positioning systems broadcast a radio pulse from a known "master" location, followed by a pulse repeated from a number of "slave" stations. The delay between the reception of the master signal and the slave signals allowed the receiver to deduce the distance to each of the slaves, providing a fix.
The first satellite navigation system was Transit, a system deployed by the US military in the 1960s. Transit's operation was based on the Doppler effect: the satellites traveled on well-known paths and broadcast their signals on a well-known frequency. The received frequency will differ slightly from the broadcast frequency because of the movement of the satellite with respect to the receiver. By monitoring this frequency shift over a short time interval, the receiver can determine its location to one side or the other of the satellite, and several such measurements combined with a precise knowledge of the satellite's orbit can fix a particular position.
Part of an orbiting satellite's broadcast included its precise orbital data. In order to ensure accuracy, the US Naval Observatory (USNO) continuously observed the precise orbits of these satellites. As a satellite's orbit deviated, the USNO would send the updated information to the satellite. Subsequent broadcasts from an updated satellite would contain the most recent accurate information about its orbit.
Modern systems are more direct. The satellite broadcasts a signal that contains orbital data (from which the position of the satellite can be calculated) and the precise time the signal was transmitted. The orbital data is transmitted in a data message that is superimposed on a code that serves as a timing reference. The satellite uses an atomic clock to maintain synchronization of all the satellites in the constellation. The receiver compares the time of broadcast encoded in the transmission of three (at sea level) or four different satellites, thereby measuring the time-of-flight to each satellite. Several such measurements can be made at the same time to different satellites, allowing a continual fix to be generated in real time using an adapted version of trilateration: see GNSS positioning calculation for details.
Each distance measurement, regardless of the system being used, places the receiver on a spherical shell at the measured distance from the broadcaster. By taking several such measurements and then looking for a point where they meet, a fix is generated. However, in the case of fast-moving receivers, the position of the signal moves as signals are received from several satellites. In addition, the radio signals slow slightly as they pass through the ionosphere, and this slowing varies with the receiver's angle to the satellite, because that changes the distance through the ionosphere. The basic computation thus attempts to find the shortest directed line tangent to four oblate spherical shells centered on four satellites. Satellite navigation receivers reduce errors by using combinations of signals from multiple satellites and multiple correlators, and then using techniques such as Kalman filtering to combine the noisy, partial, and constantly changing data into a single estimate for position, time, and velocity.




The original motivation for satellite navigation was for military applications. Satellite navigation allows for hitherto impossible precision in the delivery of weapons to targets, greatly increasing their lethality whilst reducing inadvertent casualties from mis-directed weapons. (See Guided bomb). Satellite navigation also allows forces to be directed and to locate themselves more easily, reducing the fog of war.
The ability to supply satellite navigation signals is also the ability to deny their availability. The operator of a satellite navigation system potentially has the ability to degrade or eliminate satellite navigation services over any territory it desires.










The United States' Global Positioning System (GPS) consists of up to 32 medium Earth orbit satellites in six different orbital planes, with the exact number of satellites varying as older satellites are retired and replaced. Operational since 1978 and globally available since 1994, GPS is currently the world's most utilized satellite navigation system.




The formerly Soviet, and now Russian, Global'naya Navigatsionnaya Sputnikovaya Sistema (GLObal NAvigation Satellite System), or GLONASS, was a fully functional navigation constellation in 1995. After the collapse of the Soviet Union, it fell into disrepair, leading to gaps in coverage and only partial availability. It was recovered and fully restored in 2011.




Doppler Orbitography and Radio-positioning Integrated by Satellite (DORIS) is a French precision navigation system. However, unlike other GNSS systems, it is based on static emitting stations around the world, the receivers being on satellites, in order to precisely determine their orbital position (It may be used also for mobile receivers on land with more limited usage and coverage). Used with traditional GNNS systems, it pushes the accuracy of positions to centimetric precision (and to millimetric precision for altimetric application and also allows monitoring very tiny seasonal changes of Earth rotation and deformations), in order to build a much more precise geodesic reference system.







The European Union and European Space Agency agreed in March 2002 to introduce their own alternative to GPS, called the Galileo positioning system. At an estimated cost of EUR 3.0 billion, the system of 30 MEO satellites was originally scheduled to be operational in 2010. The original year to become operational was 2014. The first experimental satellite was launched on 28 December 2005. Galileo is expected to be compatible with the modernized GPS system. The receivers will be able to combine the signals from both Galileo and GPS satellites to greatly increase the accuracy. Galileo is now not expected to be in full service until 2020 at the earliest and at a substantially higher cost.




China has indicated they plan to complete the entire second generation Beidou Navigation Satellite System (BDS or BeiDou-2, formerly known as COMPASS), by expanding current regional (Asia-Pacific) service into global coverage by 2020. The BeiDou-2 system is proposed to consist of 30 MEO satellites and five geostationary satellites. A 16-satellite regional version (covering Asia and Pacific area) was completed by December 2012.










Chinese regional (Asia-Pacific, 16 satellites) network to be expanded into the whole global system which consists of all 35 satellites by 2020.




The Indian Regional Navigational Satellite System (IRNSS) is an autonomous regional satellite navigation system being developed by Indian Space Research Organisation (ISRO) which would be under the total control of Indian government. The government approved the project in May 2006, with the intention of the system to be completed and implemented by 2015. It will consist of a constellation of 7 navigational satellites. All the 7 satellites will be placed in the Geostationary orbit (GEO) to have a larger signal footprint and lower number of satellites to map the region. It is intended to provide an all-weather absolute position accuracy of better than 7.6 meters throughout India and within a region extending approximately 1,500 km around it. A goal of complete Indian control has been stated, with the space segment, ground segment and user receivers all being built in India. The first four satellites IRNSS-1A, IRNSS-1B, IRNSS-1C and IRNSS-1D of the proposed constellation were precisely launched on 1 July 2013, 4 April 2014, 16 October 2014 and 28 March 2015 respectively from Satish Dhawan Space Centre. The remaining three satellites IRNSS-1E, IRNSS-1F and IRNSS-1G are planned to be launched from May 2015 to May 2016.




The Quasi-Zenith Satellite System (QZSS), is a proposed three-satellite regional time transfer system and enhancement for GPS covering Japan. The first demonstration satellite was launched in September 2010.




Examples of augmentation systems include the Wide Area Augmentation System, the European Geostationary Navigation Overlay Service, the Multi-functional Satellite Augmentation System, Differential GPS, and Inertial Navigation Systems.



The two current operational low Earth orbit satellite phone networks are able to track transceiver units with accuracy of a few kilometers using doppler shift calculations from the satellite. The coordinates are sent back to the transceiver unit where they can be read using AT commands or a graphical user interface. This can also be used by the gateway to enforce restrictions on geographically bound calling plans.





