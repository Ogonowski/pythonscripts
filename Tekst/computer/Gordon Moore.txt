Gordon Earle Moore (born January 3, 1929) is an American businessman, co-founder and Chairman Emeritus of Intel Corporation, and the author of Moore's law. As of January 2015, his net worth is $6.7 billion.



Moore was born in San Francisco, California, and grew up in nearby Pescadero. He attended Sequoia High School in Redwood City. Initially he went to San Jose State University. After two years he transferred to the University of California, Berkeley, from which he received a Bachelor of Science degree in chemistry in 1950.
In September, 1950 Moore matriculated at the California Institute of Technology (Caltech). Moore received a PhD in chemistry and minor in physics from Caltech in 1954. Moore conducted postdoctoral research at the Applied Physics Laboratory at Johns Hopkins University from 1953 to 1956.



Moore met his future wife, Betty Irene Whitaker, while attending San Jose State University. Gordon and Betty were married September 9, 1950, and left the next day to move to the California Institute of Technology. The couple have two sons Kenneth and Steven.







Moore joined MIT and Caltech alumnus William Shockley at the Shockley Semiconductor Laboratory division of Beckman Instruments, but left with the "traitorous eight", when Sherman Fairchild agreed to back them and created the influential Fairchild Semiconductor corporation.




In 1965, Gordon E. Moore was working as the director of research and development (R&D) at Fairchild Semiconductor. He was asked by Electronics Magazine to predict what was going to happen in the semiconductor components industry over the next ten years. In an article published on April 19, 1965, Moore observed that the number of components (transistors, resistors, diodes or capacitors) in a dense integrated circuit had doubled approximately every year, and speculated that it would continue to do so for at least the next ten years. In 1975, he revised the forecast rate to approximately every two years. Carver Mead popularized the phrase "Moore's law." The prediction has become a target for miniaturization in the semiconductor industry, and has had widespread impact in many areas of technological change.




In July 1968, Robert Noyce and Moore founded NM Electronics which later became Intel Corporation. Moore served as Executive Vice President until 1975 when he became President. In April 1979, Moore became Chairman of the Board and Chief Executive Officer, holding that position until April 1987, when he became Chairman of the Board. He was named Chairman Emeritus of Intel Corporation in 1997. Under Noyce, Moore, and later Andrew Grove, Intel has pioneered new technologies in the areas of computer memory, integrated circuits and microprocessor design.



Moore has been a member of the Board of Directors of Gilead Sciences since 1996, after serving as a member of the company's Business Advisory Board from 1991 until 1996.



In 2000 Betty and Gordon Moore established the Gordon and Betty Moore Foundation, with a gift worth about $5 billion. Through the Foundation, they initially targeted environmental conservation, science, and the San Francisco Bay Area.
The foundation gives extensively in the area of environmental conservation, supporting major projects in the Andes-Amazon Basin and the San Francisco Bay area, among others. Moore was a director of Conservation International for some years. In 2002 he and Conservation International Senior Vice President Claude Gascon received the Order of the Golden Ark from His Royal Highness Prince Bernhard of Lippe-Biesterfeld for their outstanding contributions to nature conservation.
Moore has been a member of Caltech's board of trustees since 1983, chairing it from 1993 to 2000, and is now a life trustee. In 2001, Moore and his wife donated $600 million to Caltech, the largest gift ever to an institution of higher education. He said that he wants the gift to be used to keep Caltech at the forefront of research and technology.
On December 6, 2007, Gordon Moore and his wife donated $200 million to Caltech and the University of California for the construction of the Thirty Meter Telescope, the world's second largest optical telescope. The telescope will have a mirror 30 meters across and be built on Mauna Kea in Hawaii. This is nearly three times the size of the current record holder, the Large Binocular Telescope.
In addition, through the Foundation, Betty Moore has created the Betty Irene Moore Nursing Initiative, targeting nursing care in the San Francisco Bay Area and Greater Sacramento.
In 2009, the Moores received the Andrew Carnegie Medal of Philanthropy.



Gordon Moore has received many honors. He became a member of the National Academy of Engineering in 1976.
In 1990, Moore was presented with the National Medal of Technology and Innovation by President George H.W. Bush, "for his seminal leadership in bringing American industry the two major postwar innovations in microelectronics - large-scale integrated memory and the microprocessor - that have fueled the information revolution."
In 1998 he was inducted as a Fellow of the Computer History Museum "for his fundamental early work in the design and production of semiconductor devices as co-founder of Fairchild and Intel."
In 2001, Moore received the Othmer Gold Medal for outstanding contributions to progress in chemistry and science.
Moore is also the recipient of the Presidential Medal of Freedom, the United States' highest civilian honor, as of 2002. He received the award from President George W. Bush. In 2002, Moore also received the Bower Award for Business Leadership.
In 2003, he was elected a Fellow of the American Association for the Advancement of Science.
Moore was awarded the 2008 IEEE Medal of Honor for "pioneering technical roles in integrated-circuit processing, and leadership in the development of MOS memory, the microprocessor computer and the semiconductor industry." Moore was featured in the documentary film Something Ventured which premiered in 2011.
He was awarded the 2010 Future Dan David Prize for his work in the areas of Computers and Telecommunications.
The library at the Centre for Mathematical Sciences at the University of Cambridge is named after him and his wife Betty, as are the Moore Laboratories building (dedicated 1996) at Caltech and the Gordon and Betty Moore Materials Research Building at Stanford.
The Electrochemical Society presents an award in Moore s name, the Gordon E. Moore Medal for Outstanding Achievement in Solid State Science and Technology, every two years to celebrate scientists  contributions to the field of solid state science. The Society of Chemical Industry (SCI America) annually presents the Gordon E. Moore Medal in his honor to recognize early career success in innovation in the chemical industries.



Moore actively pursues and enjoys any type of fishing and has extensively traveled the world catching species from black marlin to rainbow trout. He has said his conservation efforts are partly inspired by his interest in fishing.
In 2011, Moore's genome was the first human genome sequenced on Ion Torrent's Personal Genome Machine platform, a massively parallel sequencing device. Ion Torrent's device obtains sequence information by directly sensing ions produced by DNA polymerase synthesis using ion-sensitive field effect transistor sensors.


