The Carnegie Mellon Software Engineering Institute (SEI) is a federally funded research and development center headquartered on the campus of Carnegie Mellon University in Pittsburgh, Pennsylvania, United States. SEI also has offices in Arlington, Virginia, and Frankfurt, Germany. The SEI operates with major funding from the U.S. Department of Defense. The SEI also works closely with industry and academia through research collaborations.
The SEI program of work is conducted in several principal areas: acquisition, process management, risk, security, software development, and system design.



The SEI defines specific initiatives aimed at improving organizations' software engineering capabilities.



Organizations need to effectively manage the acquisition, development, and evolution (ADE) of software-intensive systems. Success in software engineering management practices helps organizations predict and control quality, schedule, cost, cycle time, and productivity. The best-known example of SEI work in management practices is the SEI s Capability Maturity Model (CMM) for Software (now Capability Maturity Model Integration (CMMI)). The CMMI approach consists of models, appraisal methods, and training courses that have been proven to improve process performance. In 2006, Version 1.2 of the CMMI Product Suite included the release of CMMI for Development. CMMI for Development was the first of three constellations defined in Version 1.2: the others include CMMI for Acquisition and CMMI for Services. The CMMI for Services constellation was released in February 2009. Another management practice developed by CERT, which is part of the SEI, is the Resilience Management Model (CERT-RMM). The CERT-RMM is a capability model for operational resilience management. Version 1.0 of the Resilience Management Model was released in May 2010.



SEI work in engineering practices increases the ability of software engineers to analyze, predict, and control selected functional and non-functional properties of software systems. Key SEI tools and methods include the SEI Architecture Tradeoff Analysis Method (ATAM) method, the SEI Framework for Software Product Line Practice, and the SEI Service Migration and Reuse Technique (SMART).



The goal of SEI work is to improve organizations acquisition processes.



The SEI is also the home of the CERT/CC (CERT Coordination Center), a federally funded computer security organization. The SEI CERT Program's primary goals are to ensure that appropriate technology and systems-management practices are used to resist attacks on networked systems and to limit damage and ensure continuity of critical services in spite of successful attacks, accidents, or failures. The SEI CERT program is working with US-CERT to produce the Build Security In (BSI) website, which provides guidelines for building security into every phase of the software development lifecycle. The SEI has also conducted research on insider threats and computer forensics. Results of this research and other information now populate the CERT Virtual Training Environment.
Carnegie Mellon, Capability Maturity Model, CMM, CMMI, Architecture Tradeoff Analysis Method, ATAM, and CERT are registered in the U.S. Patent and Trademark Office by Carnegie Mellon University.



The CERT Coordination Center Program has developed a method to help organizations build security into the early stages of the production life cycle. The Security Quality Requirements Engineering (SQUARE) method consists of nine steps that generate a final deliverable of categorized and prioritized security requirements. Although the SQUARE method could likely be generalized to any large-scale design project, it was designed for use with information technology systems.
SQUARE is listed at DHS National Cyber Security Division Build Security In (BSI) initiative's site.
A web-based tool, released in December 2009, was created to assist teams using the SQUARE method. The tool was extended in 2011 to include support for privacy requirements using P-SQUARE as well as acquisition requirements using A-SQUARE.






The SEI Partner Network helps the SEI disseminate software engineering best practices. Organizations and individuals in the SEI Partner Network are selected, trained, and licensed by the SEI to deliver authentic SEI services, which include courses, consulting methods, and management processes. The network currently consists of nearly 250 partner organizations worldwide.



The SEI sponsors national and international conferences, workshops, and user-group meetings. Other events cover subjects including acquisition of software-intensive systems, commercial off-the-shelf (COTS)-based systems, network security and survivability, software process research, software product lines, CMMI, and the SEI Team Software Process.




SEI courses help bring state-of-the-art technologies and practices from research and development into widespread use. SEI courses are currently offered at the SEI s locations in the United States and Europe. In addition, using licensed course materials, SEI Partners train thousands of individuals annually.



The SEI Membership Program helps the software engineering community to network. SEI Members include small business owners, software and systems programmers, CEOs, directors, and managers from both Fortune 500 companies and prominent government organizations in 36 different countries.



Through the SEI Affiliate Program, organizations place technical experts with the SEI for periods ranging from 12 months to four years. Affiliates currently are working on projects with the SEI to identify, develop, and demonstrate improved software engineering practices.



In order to recognize outstanding achievement in improving an organization's ability to create and evolve software-dependent systems, the SEI and IEEE Computer Society created the Software Process Achievement Award program. In addition to rewarding excellence, the purpose of this award is to foster continuous advancement in the practice of software engineering and to disseminate insights, experiences, and proven practices throughout the relevant research and practitioner communities.



The SEI publishes reports that offer new technical information about software engineering topics, whether theoretical or applied. The SEI also publishes books on software engineering for industry, government and military applications and practices.
In addition, the SEI offers public courses, workshops, and conferences in process improvement, software architecture and product lines, and security.



SEI has been an occasional site of anti-war movement and peace movement protests, many of which have been organized by Pittsburgh's Thomas Merton Center.









Fenton, Edwin (2000). Carnegie Mellon 1900 2000: A Centennial History. Pittsburgh: Carnegie Mellon University Press. ISBN 0-88748-323-2. 



Official website