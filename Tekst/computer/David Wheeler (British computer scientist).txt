David John Wheeler FRS (9 February 1927   13 December 2004) was a computer scientist at the University of Cambridge.



Wheeler was born in Birmingham and gained a scholarship at Trinity College, Cambridge to read the Cambridge Mathematical Tripos, graduating in 1948. He completed the world's first PhD in computer science in 1951.



Wheeler's contributions to the field included work on the EDSAC and the Burrows Wheeler transform. Along with Maurice Wilkes and Stanley Gill he is credited with the invention of the subroutine (which they referred to as the closed subroutine), and gave the first explanation of how to design software libraries; as a result, the jump to subroutine instruction is often called Wheeler Jump. He was responsible for the implementation of the CAP computer, the first to be based on security capabilities. In cryptography, he was the designer of WAKE and the co-designer of the TEA and XTEA encryption algorithms together with Roger Needham.
Wheeler married Joyce Blackler in August 1957, who herself used EDSAC for her own mathematical investigations as a research student from 1955. He became a Fellow of Darwin College, Cambridge in 1964 and formally retired in 1994, although he continued to be an active member of the University of Cambridge Computer Laboratory until his death. In 1994 he was inducted as a Fellow of the Association for Computing Machinery. In 2003, he was named a Computer History Museum Fellow Award recipient "for his invention of the closed subroutine, and for his architectural contributions to ILLIAC, the Cambridge Ring, and computer testing." The Computer Laboratory at the University of Cambridge annually holds the "Wheeler Lecture", a series of distinguished lectures named after him.
Wheeler is often quoted as saying "All problems in computer science can be solved by another level of indirection, except of course for the problem of too many indirections." Another quotation attributed to him is "Compatibility means deliberately repeating other people's mistakes."






Oral history interview with David Wheeler, 14 May 1987. Charles Babbage Institute, University of Minnesota. Wheeler was a research student at the University Mathematical Laboratory at Cambridge from 1948 51, and a pioneer programmer on the EDSAC project. Wheeler discusses projects that were run on EDSAC, user-oriented programming methods, and the influence of EDSAC on the ILLIAC, the ORDVAC, and the IBM 701. Wheeler also notes visits by Douglas Hartree, Nelson Blackman (of ONR), Peter Naur, Aad van Wijngarden, Arthur van der Poel, Friedrich Bauer, and Louis Couffignal.
Oral history interview with Gene H. Golub. Charles Babbage Institute, University of Minnesota. Golub discusses the construction of the ILLIAC computer, the work of Ralph Meager and David Wheeler on the ILLIAC design, British computer science, programming, and the early users of the ILLIAC at the University of Illinois.
In 2003, he was made a Fellow of the Computer History Museum "for his invention of the closed subroutine, his architectural contributions to the ILLIAC, the Cambridge Ring, and computer testing."