Single density, often shortened SD, is a capacity designation on magnetic storage, usually floppy disks. It describes the use of an encoding (or modulation) of information using Frequency modulation, also known as biphase mark code.
Early floppy disk drives used this method, but they are now obsolete. It was quickly replaced by double density floppy drives using modified frequency modulation (MFM) or group code recording (GCR).



Floppy disk format