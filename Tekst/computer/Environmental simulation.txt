Interactive Scenario Builder (Builder) is a modeling and simulation, three-dimensional application developed by the Advanced Tactical Environmental Simulation Team (ATEST) at the Naval Research Laboratory (NRL) that aids in understanding the radio frequency (RF) environment.



RF Tactical Decision Aid
Creation/Generation of Complex Electronic Warfare (EW) Synthetic Environments (Scenarios)
Simulation of Both Hardware and/or Modeling of Existing and Future EW Systems
Visualization of the RF Capabilities of Platforms
Modeling the Communication of Radar Systems by Calculating One-way and Two-way RF Propagation Loss
Pre-Mission Planning
Near-Realtime, Geospatial and Temporal Situational Awareness
After-Action Debriefing
Acquisition
Support to Operations (Ops)
Surface EW Test and Evaluation (T&E)
Training
Options Development
Targeting Support



ENEWS used Builder 2 to support the design, specification, and evaluation of EA-6B and AN/SLY-2 (AIEWS) EW systems from the conceptual through the design stages.
The Fleet Information Warfare Center (FIWC) used Builder 2 to assist in EW asset scheduling and allocation during Operation Desert Fox and the Kosovo campaign.
The U.S. Army s 160th Special Operations Aviation Regiment (Airborne) used Builder 2 for mission planning and mission rehearsal.



Builder is developed by the:
Advanced Tactical Environmental Simulation Team (ATEST) (Code 5774)
Electronic Warfare Modeling & Simulation (EW M&S) Branch (Code 5770)
Tactical Electronic Warfare Division (TEWD) (Code 5700)
Systems Directorate (Code 5000)
Naval Research Laboratory (NRL)
Office of Naval Research (ONR)
A listing in the Department of Defense (DoD) Modeling and Simulation Resource Registry (MSRR) states that "The primary objective of the Electronic Warfare Modeling and Simulation Branch is to develop and utilize tools for effectiveness evaluations of present, proposed, and future electronic warfare (EW) concepts, systems, and configurations for U.S. Naval Units." The EW M&S Branch used to be known as the Effectiveness of Navy Electronic Warfare Systems (ENEWS) Group (Code 5707) cerca 2005. At that time, the Builder Team was under Code 5707.4. In an NRL "Solicitation, Offer and Award" document, the "Statement of Work" section states that "Code 5707 has historically developed simulations of naval EW systems, anti-ship threats, and military communication systems to support the development, fielding and testing of electronic and weapons systems."



Office of Naval Research (ONR), a sponsor of Builder development
Naval Research Laboratory (NRL)
SIMDIS, another application developed by the EW M&S Branch



Interactive Scenario Builder Website
Advanced Tactical and Environmental Simulation Team Website
2008 NRL Review Article: "CREW Modeling of Effectiveness and Compatibility for Operational Test and Evaluation"




 This article incorporates public domain material from the United States Government document "https://builder.nrl.navy.mil".