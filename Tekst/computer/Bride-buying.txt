Bride-purchasing or bride-buying is the industry or trade of  purchasing a bride  to become property and at times as property that can be resold or repurchased for reselling. Bride-purchasing or bride-selling is practiced by bride-sellers and bride-buyers in parts of countries such as Syria, Iraq, Iran, India and China, among others. The practice is described as a form of  marriage of convenience  but is illegal in many countries in the world.



Bride-buying is an old practice in many regions in India. Bride-purchasing is common in the states of India such as Haryana, Jharkhand, and Punjab. According to CNN-IBN, women are  bought, sold, trafficked, raped and married off without consent  across some parts of India. Bride-purchases are usually outsourced from Bihar, Assam, and West Bengal. The price of the bride (locally known as paros in Jharkhand), if bought from the sellers, may cost between 4,000 to 30,000 Indian rupees, the equivalent of US$88 to US$660. The brides' parents are normally paid an average of 500 to 1,000 Indian rupees (around US$11 to US$22). The need to buy a bride is because of the low ratio of females to males. Such low ratio in turn was caused by the preference by most Indian parents to have sons instead of daughters, and female foeticide. In 2006, according to BBC News, there were around 861 women for every 1,000 men in Haryana; and the national ratio in India as a whole was 927 women for every 1,000 men. The women are not only purchased as brides or wives but also to work as farm workers or househelp. Most women become  sex slaves  or forced laborers who are later resold to human traffickers to defray the cost.
According to the Punjabi writer, Kirpal Kazak, bride-selling began in Jharkhand after the arrival of the Rajputs. The tribe decorate the women for sale with ornaments. The practice of the sale of women as brides declined after the Green Revolution in India, the  spread of literacy , and the improvement of the male-female ratio since 1911. The ratio, however, declined in 2001. The practice of bride-purchasing became confined to the poor sections of society such as farmers, Scheduled Castes, and tribes. In poverty-stricken families, only one son gets married due to poverty and to  avoid the division of landed property .



Bride buying is also an old tradition in China. The practice was largely stamped out by the Chinese Communists. However, the modern practice is "not unusual in rural villages"; it is also known as mercenary marriage. According to Ding Lu of the non-governmental organization All-China Women's Federation, the practice had a resurgence due to China s surging economy. From 1991 to 1996, Chinese police rescued upwards of 88,000 women and children who had been sold into marriage and slavery, and the Chinese government claimed that 143,000 traffickers involved were caught and prosecuted. Some human rights groups state that the figures are not correct and that the real number of abducted women is higher. Bay Fang and Mark Leong reported in U.S. News & World Report that "the government sees the commerce in wives as a shameful problem, it has only in recent years begun to provide any statistics, and it tries to put the focus on the women who have been saved rather than on the continuing trade." Causes include poverty and bride shortage in the rural areas (rural women go to the cities to work). As women leave rural areas to find work in cities, they are considered more vulnerable to being "tricked or forced into becoming chattel for men desperate for wives." The shortage of brides in turn is due to amplification of the traditional preference of Chinese couples for sons by the 1979 one-child policy in China. The Chinese Academy of Social Sciences estimated that in 1998 there were 120 men for every 100 women, with imbalances in rural areas being about 130 males for every 100 females. The increase in the cost of dowries is also a contributing factor leading men to buy women for wives. Human Rights in China states that it is more affordable for a man to buy a wife from a trafficker for 2,000 to 4,000 yuan than to pay a traditional dowry, which often runs upwards of 10,000 yuan. For the average urban worker, wife selling is an affordable option when in 1998 China urban workers make approximately $60 a month. Brides for sale are outsourced from countries such as Burma, Laos, Vietnam and North Korea. The bride-traders sell women as brides or as prostitutes depending on their physical appearance. A common trick employed by bride-brokers in acquiring brides for sale is the offer of a job such as in factories and instead kidnapping them. Bride-traders can sell a young woman for the price of US$250 to US$800. US$50 to US$100 of the original price goes to the primary kidnappers while the rest of the income goes to the traffickers who bring the bride to the main client.
Chinese women who are bought as wives who bear children are more prone to staying within the marriage. Fang Yuzhu of the China Women's Federation credits it with a "strong sense of duty" that Chinese women have, and the idea that it is shameful to leave their husband. Yuzhu also credits that some women might consider their forced marriage a better option to the life of poverty and hard labor they would be subject to upon returning home or the idea that some women may not feel they can find another husband, since they "have already been with one".



Literature that tackles selling of women as brides include titles such as Niami by Arab writer Mir Dad, Mul di Tiveen (meaning  A Purchased Woman  in the Punjabi language), Kudesan (meaning  a woman from other land  in Punjabi), Eh Hamara Jeewna by Punjabi novelist Dalip Kaur Tiwana, and the play Ik Hor Ramayan by playwright Ajmer Aulakh.



Bazaar (Market), a 1982 Indian film directed by Sagar Sarhadi is based around the theme of bride buying expatriate Indians in the Gulf countries from Hyderabad, India. 



Mail-order bride
Picture bride
Bride kidnapping
Bride price
Arranged marriage
Arranged marriage in India
Human trafficking in India
Human trafficking in Vietnam
Human trafficking in the People's Republic of China
Wife selling
Wife selling (English custom)
The Bartered Bride






Gates, Hill. Buying brides in China - again
Gregg, William. "BUY A BRIDE"