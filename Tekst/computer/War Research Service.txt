The War Research Service (WRS) was a civilian agency of the United States government established during World War II to pursue research relating to biological warfare. Established in May 1942 by Secretary of War Henry L. Stimson, the WRS was embedded in the Federal Security Agency, the federal agency that administered Social Security and other New Deal programs in the administration of President Franklin D. Roosevelt. Headed by George W. Merck, president of the Merck & Co. pharmaceutical firm, the WRS was headquartered at Fort Detrick, Maryland.
Being a civilian agency, the WRS was initially tasked to supervise the military Chemical Warfare Service's biological program. However, the WRS was disbanded in 1944, and the weapons research was continued under the exclusive oversight of the CWS.



National Academies: Committees on Biological Warfare, 1941-1948
Cutting Edge: A History of Fort Detrick (Chapter 4)