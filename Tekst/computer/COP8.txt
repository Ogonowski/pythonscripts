The COP8 microcontroller from National Semiconductor is an 8 bit CISC core microcontroller, whose main features are:
Large amount of I/O pins.
Plenty of Flash memory/ROM for code and data (up to 32 KB).
Very low EMI. No known bugs.
Many integrated peripherals (meant as single chip design).
In-System Programming
Free assembler toolchain. Commercial C compilers available.
Free Multitasking OS and TCP/IP stack.
It has a machine cycle of up to 2M cycles per second, but most versions seem to be overclockable to up to 2.8M cycles per second (28 MHz clock).



The COP8 uses separate instruction and data spaces (Harvard architecture). Instruction address space is 15 bits (32 KiB maximum), while data addresses are 8 bits (256 bytes maximum, extended via bank-switching).
To allow software bugs to be caught, all invalid instruction addresses read as zero, which is a trap instruction. Invalid RAM above the stack reads as all-ones, which is an invalid address.
The CPU has an 8-bit accumulator and 15-bit PC. 16 additional 8-bit registers (R0 R15) and an 8-bit program status word are memory mapped. There are special instructions to access them, but general RAM access instructions may also be used.
The memory map is as follows:
If RAM is not banked, then R15 (S) is just another general-purpose register. If RAM is banked, then the low half of the data address space (addresses 0x00 7F) is directed to a RAM bank selected by S. The special purpose registers in the high half of the data address space are always visible. The data registers at 0xFx can be used to copy data between banks.
RAM banks other than bank 0 have all 128 bytes available. The stack (addressed via the stack pointer) is always on bank 0, no matter how the S register is set.



In addition to 3-byte JMP and JSR instructions which can address the entire address space, 2-byte versions of these instructions can jump within a 4K page. The instruction specifies the low 12 bits, and the high 3 bits of the PC are preserved.
There are also jump indirect and load accumulator indirect instructions which use the accumulator contents as the low 8 bits of an address; the high 7 bits of the current PC are preserved.
For short-distance branches, there are 63 1-byte instructions which perform PC-relative branches from PC 32 to PC+31. This is a 15-bit addition, and no page boundary requirements apply.
Conditional branches are done using a number of conditional skip instructions. For example, IFEQ compares its two operands, and skips the following instruction if they are unequal. Any instruction may be skipped; it is not limited to branches.