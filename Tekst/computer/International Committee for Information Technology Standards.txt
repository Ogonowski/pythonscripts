The InterNational Committee for Information Technology Standards, or INCITS (pronounced "insights"), is an ANSI-accredited Standards Development Organization composed of IT developers. It was formerly known as the X3 and NCITS.
INCITS is the central U.S. forum dedicated to creating technology standards for the next generation of innovation. INCITS members combine their expertise to create the building blocks for globally transformative technologies. From cloud computing to communications, from transportation to health care technologies, INCITS is the place where innovation begins. INCITS is accredited by the American National Standards Institute (ANSI) and is affiliated with the Information Technology Industry Council, a global policy advocacy organization that represents U.S. and global innovation companies.
INCITS coordinates technical standards activity between ANSI in the USA and joint ISO/IEC committees worldwide. This provides a mechanism to create standards that will be implemented in many nations. As such, INCITS' Executive Board also serves as ANSI's Technical Advisory Group for ISO/IEC Joint Technical Committee 1. JTC 1 is responsible for International standardization in the field of Information Technology.
INCITS operates through consensus. Our members work together in technical committees and working groups to create standards that result in globally accepted, highly interoperable products. Many times, the U.S. standard that INCITS members develop then will become the baseline standard for the international community. The INCITS process protects the intellectual property rights of members and ensures that every voice counts, with a quick pace to match the rapidly evolving market. Our procedures allow everyone with an interest in the subject covered by a standard to participate, either as a member of the consensus body or through public comment, and to have their comments considered.



INCITS is guided by the leadership of its Executive Board. Members include AIM Global, Adobe, Apple, Dept. of Commerce-NIST, DMTF, EMC, Farance Inc., Futurewei Technologies, GSI GO, Hewlett-Packard, IBM, IEEE, Intel, Microsoft, Oracle, Purdue University, TIA, US Dept. of Defense, US Dept. of Homeland Security
The INCITS Executive Board has established more than 50 Technical Committees, Task Groups and Study Groups that are constantly developing standards for new technologies and updating standards for older products that are evolving through innovation.
Our technical committees grow regularly to match the ever-evolving ICT marketplace. New products and new approaches come online every day. INCITS members work with the developers and users before a product goes to market so it is broadly accepted and highly interoperable. INCITS' technical committees span a wide range of information technology areas.



Ad Hoc on Smart Cities
ATA Storage Interface
BigData
Biometrics

Technical Interfaces
Data Interchange Formats
Performance Testing and Reporting
Cross Jurisdictional and Societal Aspects

Character Sets and Internationalization
Cloud Computing and Distributed Platforms
Coding of Audio, Picture, Multimedia, and Hypermedia Information

MPEG
JPEG

Computer Graphics & Image Processing
Cyber Security
Data Management

SQL
Metadata

Fibre Channel Interfaces

Physical Variants
Interconnection Schemes

Geographic Information Systems (GIS)
Governance of IT
Identification Cards and Related Devices Physical Characteristics and Test Methods

Integrated Circuit Cards with Contacts
Contactless IC Cards
Driver s License/ID Cards
Physical Characteristics and Test Methods

Internet of Things (IoT)
IT Sustainability
Programming Languages - Fortran, Cobol, C Language, C++ Language
Office Equipment
Open Systems
Optical Digital Data Disks SCSI Storage Interfaces
SCSI Storage Interfaces
Text Processing: Office and Publishing Systems Interface



American National Standards are essential tools used in everyday life. Today, there are more than 750 such standards that have been created and approved through the INCITS process, with another 800 in development. American National Standards are voluntary and serve U.S. interests well because all materially affected stakeholders have the opportunity to work together to create them. INCITS-approved standards only become mandatory when, and if, they are adopted or referenced by the government or when market forces make them imperative.
Given the responsibilities and the expenditures associated with U.S. participation in international standards activities, INCITS considers participation as a "P" member of ISO/IEC JTC 1, as a declaration of support for the international committee s technical work. INCITS policy is to adopt as "Identical" American National Standards all ISO/IEC or ISO standards that fall within its program of work, with exceptions as outlined in our procedures. Accordingly, INCITS will adopt as "Identical" American National Standards all ISO/IEC or ISO standards that fall within its program of work. Similarly, INCITS will withdraw any such adopted American National Standard that has been withdrawn as an ISO/IEC or ISO International Standards.



The INCITS catalog of work includes nationally developed standards, technical reports and international standards that have been adopted by the US.



Becoming an INCITS member starts with filling out a membership application form. Once submitted, it goes through a quick review process.



INCITS was established in 1961 as the 'Accredited Standards Committee X3, Information Technology' and is sponsored by Information Technology Industry Council (ITI), a trade association representing providers of information technology products and services then known as the Business Equipment Manufacturers' Association (BEMA) and later renamed the Computer and Business Equipment Manufacturers' Association (CBEMA). The first organizational meeting was in February 1961 with ITI (CBEMA then) taking Secretariat responsibility. X3 was established under American National Standards Institute (ANSI) procedures. The forum was renamed Accredited Standards Committee NCITS, National Committee for Information Technology Standards (NCITS) in 1997, and the current name was approved in 2001.






Homepage of INCITS, includes a list of INCITS standards
Contact INCITS
ANSI Accredited Standards Developers (ANSI Accredited SDO )
JTC 1 Homepage
Charles A. Phillips Papers, 1959-1985 (Historical reference to BEMA)