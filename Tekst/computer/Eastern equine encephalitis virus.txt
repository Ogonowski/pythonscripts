Eastern equine encephalitis virus (EEE), commonly called Triple E or, sleeping sickness (not to be confused with Trypanosomiasis) is a zoonotic alphavirus and arbovirus present in North, Central and South America and the Caribbean. EEE was first recognized in Massachusetts, USA in 1831 when 75 horses died mysteriously of viral encephalitis.
Epizootics in horses have continued to occur regularly in the United States. EEE is found today in the eastern part of the country and is often associated with coastal plains.
EEEV is closely related to Venezuelan equine encephalitis virus and Western equine encephalitis virus.



The causative agent, later identified as a togavirus was first isolated from infected horse brains in 1933. In 1938, the first confirmed human cases were identified when thirty children died of encephalitis in the northeastern United States. These cases coincided with outbreaks in horses in the same regions. The fatality rate in humans is 33% and there is currently no cure for human infections. This virus has two distinct antigenic variants, the more pathogenic North American (NA) and the less pathogenic South American (SA).
These two clades may actually be distinct viruses. The NA strains appear to be monotypic with a mutation rate of 2.7   10 4 substitutions/site/year (s/s/y). It appears to have diverged from the SA strains 922 to 4,856 years ago. The SA strains are divided into two main clades and a third smaller one. The two main clades diverged between 577 and 2,927 years ago. The mutation rate in the genome has been estimated to be 1.2   10 4 s/s/y.



EEE is capable of infecting a wide range of animals including mammals, birds, reptiles and amphibians. The virus is maintained in nature through a bird - mosquito cycle. There are two mosquito species primarily involved in this portion of the cycle, they are Culiseta melanura and Cs. morsitans. These mosquitoes feed on the blood of birds. The amount of virus found in nature increases throughout the summer as more birds and more mosquitoes become infected.
Transmission of EEEV to mammals (including humans) occurs via other mosquitoes, species that feed upon the blood of both birds and mammals. These other mosquitoes are called bridge vectors because they carry the virus from the avian hosts to other types of hosts, particularly mammals. The bridge vectors include Aedes vexans, Coquillettidia perturbans, Ochlerotatus canadensis and Ochlerotatus sollicitans. Ochlerotatus canadensis also frequently bites turtles.
Humans, horses and most other infected mammals do not circulate enough virus in their blood to infect additional mosquitoes. There have been some cases where EEEV has been contracted through lab exposures or from exposure of the eyes, lungs or skin wounds to brain or spinal cord matter from infected animals.






In humans, symptoms include high fever, muscle pain, altered mental status, headache, meningeal irritation, photophobia, and seizures, which occur three to 10 days after the bite of an infected mosquito.



After inoculation by the vector, the virus travels via lymphatics to lymph nodes, and replicates in macrophages and neutrophils, resulting in lymphopenia, leukopenia and fever. Subsequent replication occurs in other organs leading to viremia. Symptoms in horses occur one to three weeks after infection, and begins with a fever that may reach as high as 106  F (41  C). The fever usually lasts for 24 48 hours.
Nervous signs appear during the fever that include sensitivity to sound, periods of excitement, and restlessness. Brain lesions appear, causing drowsiness, drooping ears, circling, aimless wandering, head pressing, inability to swallow, and abnormal gait. Paralysis follows, causing the horse to have difficulty raising its head. The horse usually suffers complete paralysis and death two to four days after symptoms appear. Mortality rates among horses with the eastern strain range from 70 to 90%.



The disease can be prevented in horses with the use of vaccinations. These vaccinations are usually given together with vaccinations for other diseases, most commonly WEE, VEE, and tetanus. Most vaccinations for EEE consist of the killed virus.




There is no cure for EEE. Treatment consists of corticosteroids, anticonvulsants, and supportive measures (treating symptoms) such as intravenous fluids, tracheal intubation, and antipyretics. About four percent of humans known to be infected develop symptoms, with a total of about six cases per year in the US. A third of these cases die, and many survivors suffer permanent brain damage.




Several states in the northeast US have seen increased virus activity since 2004. Between 2004 and 2006, there were at least 10 human cases of EEE reported in Massachusetts. In 2006, approximately 500,000 acres (2,000 km2) in southeastern Massachusetts were treated with mosquito adulticides to reduce the risk of humans contracting EEE. There have been several human cases reported in New Hampshire as well.
In October 2007, a citizen of Livingston, West Lothian, Scotland became the first European victim of this disease. The man had visited New Hampshire during the summer of 2007 on a fishing vacation, and was diagnosed as having EEEV on 13 September 2007. He fell ill with the disease on 31 August 2007, just one day after flying home.
On July 19, 2012 the virus was identified in a mosquito of the species Coquillettidia perturbens in Nickerson State Park on Cape Cod, Massachusetts. On July 28, 2012 the virus was found in mosquitos in Pittsfield, MA.



EEEV was one of more than a dozen agents that the United States researched as potential biological weapons before the nation suspended its biological weapons program.


