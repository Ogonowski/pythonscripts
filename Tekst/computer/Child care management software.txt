Child care management software also referred to as child care administrative software or daycare accounting software  is business software designed specifically for use by child care centers, preschools, and similar child-oriented facilities. They can be either run from local computers or via mobile/hand-held access to other systems running elsewhere. The software is intended to increase staff productivity by recording unique child and family information.
This category of software incorporates aspects of other types of business software, notably accounting software, but typically addresses business management issues that are unique to child care and beyond the scope of generic business software. Immunization tracking and record keeping for the Child and Adult Care Food Program (CACFP) are examples of the specific nature of such software that appeals to a niche market.
Child care management software is typically developed by an independent software vendor. Capabilities vary by vendor  but may include tracking one or more of the following:
Sign In-Out of Children and Staff
Child scheduling and attendance
Authorized pick up persons
Child/teacher ratios
Staff scheduling
Classroom rosters and reporting
Child immunizations
Emergency contacts
Photographs of children or pick up persons
Waiting list management
Summer camps, activities and after school programs
Student meal counts
Menu planning
Automated billing for private pay and subsidized child care
Family tax statements and receipts
Electronic payment processing
Payroll and time cards
General accounting and financial reporting



^ lovetoknow.com
^ Open Directory Project: Daycare Software Vendors