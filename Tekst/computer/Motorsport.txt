Motorsport or motorsports is the group of competitive events which primarily involve the use of motorized vehicles, whether for racing or non-racing competition. MotoSport refers to motorcycle racing specifically and includes off-road racing such as motocross.
Motorsport is governed by the F d ration Internationale de l'Automobile, and F d ration Internationale de Motocyclisme.



In 1894, a French newspaper organized a race from Paris to Rouen and back, starting city to city racing. In 1900, the Gordon Bennett Cup was established. Closed circuit racing arose as open road racing was banned. Brooklands was the first dedicated track in the United Kingdom.
After World War I, European countries organized Grand Prix races over closed courses. In the United States, dirt track racing was popular.
After World War II, the Grand Prix circuit was organized. In the United States, stock car racing and drag racing was organized.
Motorsports divided by types of motor vehicles into racing events, and organizations.




Open wheel racing is a set of classes of vehicles, with the wheels outside of the chassis.




Formula One is a class of single seat grand prix closed course racing, organized by the F d ration Internationale de l'Automobile. The formula is a set of rules governing vehicle power, weight and size.




In the United States, Indy Car is a class of single seat paved track racing, organized by INDYCAR. The premier race is the Indianapolis 500.




Enclosed wheel racing is a set of classes of vehicles, where the wheels are inside the chassis of the vehicle, similar to a "stock car."




Sports car racing is a set of classes of vehicles, over a closed course track, including sports cars, and specialized racing types. The premiere race is the 24 Hours of Le Mans.




Stock car racing is a set of vehicles, that race over a speedway track, organized by NASCAR. While once stock cars, the vehicles are now purpose built, but resemble production cars. NASCAR was organized in 1947, to combine flat track oval racing of production cars. Daytona Beach and Road Course was founded where land speed records were set on the beach, and including part of A1A.




Touring car racing is a set of vehicles, modified street cars, that race over closed track courses.




Motor sports which involve competitors racing against each other include:
Auto (car) racing
Motor rallying
Motorcycle racing
Air racing
Kart racing
Boat racing
Hovercraft racing
Lawn mower racing
Snowmobile racing
Truck racing



Forms of motorsport which do not involve racing include drifting, regularity rally, motorcycle trials, gymkhana, Freestyle Motocross and tractor pulling.



Motorsport was a demonstration event at the 1900 Summer Olympics.


