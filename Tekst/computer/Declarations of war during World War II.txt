This is a timeline of formal Declarations of War during World War II.
A Declaration of War is a formal act by which one nation goes to war against another. The declaration is usually an act of delivering a performative speech (not to be confused with a mere speech) or the presentation of a signed document by an authorized party of a national government in order to create a state of war between two or more sovereign states. The official international protocol for declaring war was defined in The Hague Peace Conference of 1907 (or Hague II). For the diplomatic maneuvering behind these events which led to hostilities between nations during World War II, see the article entitled Diplomatic history of World War II.



Below is a table showing the Outbreaks of Wars between Nations which occurred during World War II. Indicated are the dates (during the immediate build-up to, or during the course of, World War II), from which a de facto state of war existed between nations. The table shows both the "Initiator Nation(s)" and the nation at which the aggression was aimed, or "Targeted Nation(s)". Events listed include those in which there were simple diplomatic breaking of relations that did not involve any physical attack, as well as those involving overt declarations or acts of aggression.

Outbreaks of Wars between Nations during World War II
Table Legend: Concerning "Declaration of War: Type." A = Attack without prior, formal declaration of war; U = State of war arrived at through use of ultimatum;W = Formal declaration of war made.



Harman, Nicholas (1990). Dunkirk: the Necessary Myth. Jove. ISBN 978-0340517857. 
German White Book. All World Wars. 
Hitler, Adolph (2012). The Great Tragedy: Germany's Declaration of War against the United States of America. ISBN 978-1300127703. 
Torrie, Julia S. (2010). "For Their Own Good": Civilian Evacuations in Germany and France, 1939-1945. Berghahn Books. ISBN 978-1845457259. 


