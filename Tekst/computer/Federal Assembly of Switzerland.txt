The Federal Assembly (German: Bundesversammlung, French: Assembl e f d rale, Italian: Assemblea federale, Romansh: Assamblea federala), is Switzerland's federal legislature. It meets in Bern in the Federal Palace.
The Federal Assembly is bicameral, being composed of the 200-seat National Council and the 46-seat Council of States. The houses have identical powers. Members of both houses represent the cantons, but, whereas seats in the National Council are distributed in proportion to population, each canton has two seats in the Council of States, except the six 'half-cantons' which have one seat each. Both are elected in full once every four years, with the last election being held in 2011.
The Federal Assembly possesses the federal government's legislative power, along with the separate constitutional right of citizen's initiative. For a law to pass, it must be passed by both houses. The Federal Assembly may come together as a United Federal Assembly in certain circumstances, including to elect the Federal Council, the Federal Chancellor, a General (Swiss generals are only selected in times of great national danger), or federal judges.



The Federal Assembly is made up of two chambers:
the National Council, with 200 seats
the Council of States, with 46 councillors.
Seats in the National Council are allocated to the cantons proportionally, based on population. In the Council of States, every canton has two seats (except for the former "half-cantons", which have one seat each).



On occasions the two houses sit jointly as the "United Federal Assembly" (German: Vereinigte Bundesversammlung, French: Assembl e f d rale, Chambres r unies, Italian: Assemblea federale plenaria, Romansh: Assamblea federala plenara). This is done to:
elect members of the Federal Council, the Federal Chancellor, the General and federal judges;
arbitrate in the event of conflicts between federal authorities;
issue pardons; or
listen to special announcements
The United Federal Assembly is presided by the National Council's Presidency.



Parties can cooperate in groups, allowing smaller parties access to rights as part of a caucus. These groups must have at least five members and must be maintained across both chambers. Being a member of a formal group gives members the right to sit on committees, and those that aren't members can't speak in most debates. Each group receives a fixed allowance of  112,000, whilst each member of a group also receives an additional  20,800 a year each.
Since March 2009, there have been six groups in the Federal Assembly. The latest group to form was the Conservative Democratic Party which split off the Swiss People's Party in 2008. The Christian Democrats/EPP/glp Group (CEg) was formed after the 2007 elections, out of the former Christian Democratic (C) and EPP (E) groups. The current FTP/Liberal group (RL) was formed in 2003 out of the former FDP (R) and Liberal (L) groups; since the 2009 fusion of the Free Democrati and Liberal Parties, RL is once again a single-party group. In 2011, the CEg was disbanded, the Green Liberals formed their own faction (GL) and the three Christian parties formed the Christian-Evangelical Group (CE).
Currently (as of 2012), the seven factions are composed as follows:



Swiss federal election, 2011
Swiss federal election, 2007
Hotel Bellevue Palace
Federal Diet of Switzerland






The Swiss Confederation   a brief guide 2010. Swiss Confederation. 2010. p. 19. 



Official site