The Halaf culture is a prehistoric period which lasted between about 6100 and 5100 BCE. The period is a continuous development out of the earlier Pottery Neolithic and is located primarily in south-eastern Turkey, Syria, and northern Iraq, although Halaf-influenced material is found throughout Greater Mesopotamia.
While the period is named after the site of Tell Halaf in north Syria, excavated by Max von Oppenheim between 1911 and 1927, the earliest Halaf period material was excavated by John Garstang in 1908 at the site of Sakce G z , then in Syria but now part of Turkey. Small amounts of Halaf material were also excavated in 1913 by Leonard Woolley at Carchemish, on the Turkish/Syrian border. However, the most important site for the Halaf tradition was the site of Tell Arpachiyah, now located in the suburbs of Mosul, Iraq.
The Halaf period was succeeded by the Halaf-Ubaid Transitional period which comprised the late Halaf (c. 5400-5000 BC), and then by the Ubaid period.



Previously, the Syrian plains were not considered as the homeland of Halaf culture, and the Halafians were seen either as hill people who descended from the nearby mountains of southeastern Anatolia, or herdsmen from northern Iraq. However, those views changed with the recent archaeology conducted since 1986 by Peter Akkermans, which have produced new insights and perspectives about the rise of Halaf culture. A formerly unknown transitional culture between the pre-Half Neolithic's era and Halaf's era was uncovered in the Balikh valley, at Tell Sabi Abyad (the Mound of the White Boy).
Currently, eleven occupational layers have been unearthed in Sabi Abyad, levels from 11 to 7 are considered pre-Halaf, from 6 to 4 transitional, and from 3 to 1 early Halaf. No hiatus in occupation is observed except between levels 11 and 10. The new archaeology demonstrated that Halaf culture was not sudden and was not the result of foreign people, but rather a continuous process of indigenous cultural changes in northern Syria, that spread to the other regions.







Although no Halaf settlement has been extensively excavated some buildings have been excavated: the tholoi of Tell Arpachiyah, circular domed structures approached through long rectangular anterooms. Only a few of these structures were ever excavated. They were constructed of mud-brick sometimes on stone foundations and may have been for ritual use (one contained a large number of female figurines). Other circular buildings were probably just houses.




The best known, most characteristic pottery of Tell Halaf, called Halaf ware, produced by specialist potters, can be painted, sometimes using more than two colors (called polychrome) with geometric and animal motifs. Other types of Halaf pottery are known, including unpainted, cooking ware and ware with burnished surfaces. There are many theories about why the distinctive pottery style developed.
The theory is that the pottery came about due to regional copying and that it was exchanged as a prestige item between local elites is now disputed. The polychrome painted Halaf pottery has been proposed to be a "trade pottery" pottery produced for export however, the predominance of locally produced painted pottery in all areas of Halaf sites including potters settlement questions that theory.
Halaf pottery has been found in other parts of northern Mesopotamia, such as at Nineveh and Tepe Gawra, Chagar Bazar and at many sites in Anatolia (Turkey) suggesting that it was widely used in the region. In addition, the Halaf communities made female figurines of partially baked clay and stone and stamp seals of stone, (see also Impression seal). The seals are thought to mark the development of concepts of personal property, as similar seals were used for this purpose in later times. The Halaf people used tools made of stone and clay. Copper was also known, but was not used for tools.



Dryland farming was practiced by the population. This type of farming was based on exploiting natural rainfall without the help of irrigation, in a similar practice to that still practiced today by the Hopi people of Arizona. Emmer wheat, two-rowed barley and flax were grown. They kept cattle, sheep and goats.



Halaf culture ended by 5000 BC after entering the so called Halaf-Ubaid Transitional period. Many Halafians settlements were abandoned, and the remaining ones showed Ubaidian characters. The new period is named Northern Ubaid to distinguish it from the proper Ubaid in southern Mesopotamia, and two explanations were presented for the transformation. The first maintain an invasion and a replacement of the Halafians by the Ubaidians, however, there is no hiatus between the Halaf and northern Ubaid which exclude the invasion theory. The most plausible theory is a Halafian adoption of the Ubaid culture, which is supported by most scholars including Oates, Breniquet and Akkermans.


