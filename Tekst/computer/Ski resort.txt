A ski resort is a resort developed for skiing, snowboarding, and other winter sports. In Europe a ski resort is a town or village in a ski area   a mountainous area, where there are ski trails and other supporting services such as hotels, restaurants, equipment rentals, and a ski lift system. In North America it is more common for ski areas to exist well away from towns, and the term ski resort is used for a destination resort, often purpose-built and self-contained, where skiing is the main activity.




The ski industry has identified advancing generations of ski resorts:
First generation
Developed around a well-established summer resort or village (e.g. Davos, St. Moritz, Kitzb hel, Chamonix Haute-Savoie, Meg ve, Val Gardena).
Second generation
Created from a non-tourist village or pasture (e.g. St. Anton, Lech, Courchevel Savoie, L'Alpe d'Huez, Aspen, Breckenridge).
Third generation or integrated
Designed from scratch on virgin territory to be a purpose-built ski resort, all the amenities and services nearby (e.g. Sestri re, Flaine, La Plagne, Isola 2000).
Fourth generation or village resorts
Created from virgin territory or around an existing village, but more concerned with traditional uses (e.g. resorts built since 1975 like Shahdag Mountain Resort, Azerbaijan).
The term ski station is also used, particularly in Europe, for a skiing facility which is not located in or near a town or village. A ski resort which is also open for summer activities is often referred to as a mountain resort.



Ski areas have marked paths for skiing known as runs, trails or pistes. Ski areas typically have one or more chairlifts for moving skiers rapidly to the top of hills, and to interconnect the various trails. Rope tows can also be used on short slopes (usually beginner hills or bunny slopes). Larger ski areas may use gondolas or aerial trams for transportation across longer distances within the ski area.
Some ski resorts offer lodging options on the slopes themselves, with ski-in and ski-out access allowing guests to ski right up to the door. Ski resorts often have other activities, such as snowmobiling, sledding, horse-drawn sleds, dog-sledding, ice-skating, indoor or outdoor swimming, and hottubbing, game rooms, and local forms of entertainment, such as clubs, cinema, theatre and cabarets.
Ski areas usually have at least a basic first aid facility, and some kind of ski patrol service to ensure that injured skiers are rescued. The ski patrol is usually responsible for rule enforcement, marking hazards, closing individual runs (if a sufficient level of hazard exists), and removing (dismissing) dangerous participants from the area.






List of ski areas and resorts
Private ski area (North America)
Environmental impact of ski resorts






 Media related to Ski resorts at Wikimedia Commons