The QC record format is one of the two standard formats used for the interchange of financial transactions in the New Zealand banking system. The other standard format is BACHO.
QC-format transactions are primarily used in batch processing systems running on MVS mainframe computers.
A QC record consists of a fixed 23-byte header (containing record type codes, destination account details, and the transaction amount) followed by zero or more optional fields, each of which is of variable size.



Record-oriented filesystem