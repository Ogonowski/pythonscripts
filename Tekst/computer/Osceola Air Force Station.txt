Osceola Air Force Station (ADC ID: P-35, NORAD ID: Z-35) is a closed United States Air Force General Surveillance Radar station. It is located 5.6 miles (9.0 km) south-southeast of Osceola, Wisconsin. It was closed in 1975.



In late 1950 Air Defense Command selected the East Farmington, Wisconsin site as one of twenty-eight radar stations built as part of the second segment of the permanent radar surveillance network. Prompted by the start of the Korean War, on July 11, 1950, the Secretary of the Air Force asked the Secretary of Defense for approval to expedite construction of the second segment of the permanent network. Receiving the Defense Secretary s approval on July 21, the Air Force directed the Corps of Engineers to proceed with construction.
Construction of the station began in 1950 and was opened in June 1951. The 674th Aircraft Control and Warning Squadron was assigned and began operating a pair of AN/CPS-6B radars from the site. 261 enlisted and 33 officers were assigned, and initially the station functioned as a Ground-Control Intercept (GCI) and warning station. As a GCI station, the squadron's role was to guide interceptor aircraft toward unidentified intruders picked up on the unit's radar scopes. East Farmington was re-designated as Osceola Air Force Station on 1 December 1953. The radars were retired at the end of the decade as an AN/FPS-7 search radar and two AN/FPS-6A height-finder radars were installed during 1959.
During 1959 Osceola AFS joined the Semi Automatic Ground Environment (SAGE) system, initially feeding data to DC-10 at Duluth AFS, Minnesota. After joining, the squadron was re-designated as the 674th Radar Squadron (SAGE) on 15 July 1959, its direction and control duties taken over by DC-10. The radar squadron provided information 24/7 to Duluth where it was analyzed to determine range, direction altitude speed and whether or not aircraft were friendly or hostile. On 31 July 1963, Osceola was re-designated as NORAD ID Z-35. In addition, the two height-finder radars were modified to AN/FPS-90 sets in 1963. One of the AN/FPS-90 was retired a few years later.
In addition to the main facility, Osceola operated the following AN/FPS-18 Gap Filler sites:
Northfield, MN (P-35B) 44 25 14 N 093 11 05 W
Jim Falls, WI (P-35C) 45 02 12 N 091 20 54 W
La Crescent, MN (P-35F) 43 51 35 N 091 18 59 W
Over the years, the equipment at the station was upgraded or modified to improve the efficiency and accuracy of the information gathered by the radars.
In November 1974 the Air Force announced that Osceola would be closing due to what was called "redundancies with more strategically located radars". The 674th Radar Squadron (SAGE) was inactivated in March 1975 as part of a draw-down of ADC, and was closed on 30 April.
Today, Osceola Air Force Station is used as an Association Retreat Center. Most of the buildings are in use and well-maintained. The former radar towers are still standing.






674th Aircraft Control and Warning Squadron
Activated on 8 October 1950
Redesignated 674th Radar Squadron (SAGE), 15 July 1959
Redesignated 674th Radar Squadron, 1 February 1974
Inactivated 31 March 1975
Assignments:
543d Aircraft Control and Warning Group, 18 October 1950
37th Air Division, 1 January 1959
30th Air Division, 1 April 1959
Duluth Air Defense Sector, 1 July 1959
29th Air Division, 1 April 1966
34th Air Division, 15 September 1969
29th Air Division, 14 November 1969
23d Air Division, 19 November 1969   31 March 1975




List of USAF Aerospace Defense Command General Surveillance Radar Stations



 This article incorporates public domain material from websites or documents of the Air Force Historical Research Agency.

Cornett, Lloyd H. and Johnson, Mildred W., A Handbook of Aerospace Defense Organization 1946 - 1980, Office of History, Aerospace Defense Center, Peterson AFB, CO (1980).
Winkler, David F. & Webster, Julie L., Searching the Skies, The Legacy of the United States Cold War Defense Radar Program, US Army Construction Engineering Research Laboratories, Champaign, IL (1997).
Information for Osceola AFS, WI


