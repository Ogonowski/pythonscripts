The Canton of Basel-Landschaft (German:  Basel-Landschaft  "Basel-Country"; informally: Baselland/Baselbiet, in French B le-Campagne), is one of the 26 cantons of Switzerland. The capital is Liestal. It shares borders with the cantons of Basel-Stadt, Solothurn, Jura and Aargau, and with the French r gion of Alsace and the German state of Baden-W rttemberg.




Basel-Landschaft, together with Basel-Stadt, formed the historic Canton of Basel until they separated following the uprising of 1833 (Battle of the H lftenschanz near Frenkendorf).

In Roman times, the area of Basel was a centre of Roman activity. There are well-preserved remains at the site of Augusta Raurica in the canton of Basel-Landschaft. Around AD 200 there were about 20,000 people living in this city, now part of the much smaller Augst. The remains are on display in an open-air museum. The museum attracts over 140,000 visitors per year. Many of these visitors are schoolchildren from other parts of Switzerland. The site of Augusta Raurica includes the best-preserved amphitheatre north of the Alps, and a reconstructed Roman villa.
The lands of the canton Basel-Landschaft are part of the lands acquired by the city of Basel. Until the end of the 16th century, most of the canton's land belonged to the city of Basel. After Napoleon s visit in 1798, the country achieved equality with the city. The country was economically dependent on the city, most probably because of the low level of education in the agricultural areas at the time. The city of Basel remained the cultural and economic centre of both Basel half cantons until then. Castles and residences of Basel merchants dominated much of the landscape in Basel-Landschaft.

After 1830 there were political quarrels and armed conflict in the canton of Basel. Some of these were concerned with the rights of the population in the agricultural areas. They ultimately led to the separation of the canton Basel-Landschaft from the city of Basel on 26 August 1833. Since then, there has been a movement for reunification. This movement gained momentum after 1900 when many parts of Basel-Landschaft became industrialized. The two half cantons agreed in principle to merge, but in 1969 the people of Basel-Landschaft voted down a referendum on this proposal in favour of retaining their independence. It is thought that the closing economic gap between the two cantons was the main reason why the population changed their attitude.
That vote was not the end of a close relationship between the two Basels. The two half cantons have since signed a number of agreements to co-operate. The contribution of Basel-Landschaft to the University of Basel since 1976 is just one example.



The canton of Basel-Landschaft lies in the north of Switzerland. The Jura mountain chain traverses the canton. The rivers Ergolz and Birs drain the lands of the canton. The thirty rivers of Baselland are:
Ergolz
Eibach
Homburger Bach
Diegter Bach
Walibach
Vodere Frenke
Fluebach
Hintere Frenke
Frenke
Orisbach
R serenbach
Arisdorfer Bach
Violenbach
Wintersinger Bach
Buuser Bach
Magdener Bach
Hemmiker Bach
Rickenb chlein
Rhein
Birsig
Binnbach
Marchbach
Birs
Seebach
Chastelbach
Ibach
D rrbach
L ssel
Wahlenbach and
L tzel




Since the decision to remain independent from Basel-Stadt in 1969 there have been a number of requests in the parliament for Basel-Landschaft to become a full canton. In 1988 the canton of Basel-Landschaft had this aim written into its constitution. The aim remains to change the Swiss constitution to recognize the two cantons of Basel as full members.



The nine municipalities of the Arlesheim district used to belong to the diocese of Basel. In 1792 French troops occupied the district and in 1793 the lands were annexed by France, which explains the linguistic switch of the Family of the House of Basel's name from "von Basel" to "de B le", since Arlesheim holds their manor. In 1815 at the Congress of Vienna the district joined Basel.
The district of Laufental has the same history as that of Arlesheim. The important difference is that in 1815 Laufental joined Bern rather than Basel. When the canton of Jura was created in 1979, the district of Laufental became an enclave of the canton of Bern. It was allowed self- determination and in 1980 the people decided to join the canton of Basel-Landschaft. This led to Laufental joining the canton of Basel-Landschaft on 1 January 1994 after a lengthy administrative process.




There are 5 areas (Bezirke) in Basel-Landschaft (as of 2015):
Arlesheim (Pop: 153,586 ) capital: Arlesheim (Pop: 9,183)
Laufen (Pop: 19,591) capital: Laufen (Pop: 5,529)
Liestal (Pop: 59,170) capital: Liestal (Pop: 14,042)
Sissach (Pop: 35,100) capital: Sissach (Pop: 6,564)
Waldenburg (Pop: 15,974) capital: Waldenburg (Pop: 1,182)




There are 86 municipalities in the canton (As of 2009).



The population is predominantly German-speaking. Protestantism is the main religion in the canton (43% as of 2000), while about one-third of the population (32%) is Roman Catholic. The Swiss Reformed Church and Christian Catholic Church of Switzerland are recognized as state churches. Between 1959 and 1970 the canton was Switzerland's fastest-growing, as the population almost doubled from 108,000 to 205,000. Today, the canton has a population (as of 30 June 2015) of 283,421. As of 2007, the population included 48,719 foreigners who made up 18.1% of the population.




Agriculture in the canton includes fruit growing, dairy farming and cattle breeding. Important industries include textiles, metals and chemicals.
The canton of Basel-Landschaft is part of the economic region around Basel that includes parts of France and Germany as well as both half cantons of Basel. Since the 1960s there are agreements in force to strengthen contacts within the so-called Regio Basiliensis. This economic co-operation is often considered as the most intensive in Europe.
From the 17th century until the beginning of the 20th century silk weaving was important in Basel-Landschaft. Factories were established as early as 1850, following the finding of salt in underground deposits, founding industries such as the chemical industry in Schweizerhalle. The chemical industry means that Basel is one of the richer parts of Switzerland.
The chemical industry is not the sole employer in the canton. Small and middle-sized businesses make up about a quarter of the cantonal economy. Many of these, however, are connected to the larger employers.






Augusta Raurica
List of castles and fortresses in Switzerland
Basellandschaftliche Zeitung, daily newspaper published in Liestal.



Official website (German)
Official statistics
Basel-Landschaft in German, French and Italian in the online Historical Dictionary of Switzerland.