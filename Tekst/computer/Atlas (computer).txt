The Atlas Computer was a joint development between the University of Manchester, Ferranti, and Plessey. The first Atlas, installed at Manchester University and officially commissioned in 1962, was one of the world's first supercomputers, considered to be the most powerful computer in the world at that time. It was said that whenever Atlas went offline half of the United Kingdom's computer capacity was lost. It was a second-generation machine, using discrete germanium transistors. Two other Atlas machines were built: one for British Petroleum and the University of London, and one for the Atlas Computer Laboratory at Chilton near Oxford.
A derivative system was built by Ferranti for Cambridge University. Called the Titan, or Atlas 2, it had a different memory organisation and ran a time-sharing operating system developed by Cambridge University Computer Laboratory. Two further Atlas 2s were delivered: one to the CAD Centre in Cambridge (later called CADCentre, then AVEVA), and the other to the Atomic Weapons Research Establishment (AWRE), Aldermaston.
The University of Manchester's Atlas was decommissioned in 1971, but the last was in service until 1974. Parts of the Chilton Atlas are preserved by National Museums Scotland in Edinburgh; the main console itself was rediscovered in July 2014 and is at Rutherford Appleton Laboratory in Chilton, near Oxford. CADCentre's Atlas 2 was decommissioned in late 1976.



Development of MUSE a name derived from microsecond engine began at Manchester University in 1956. The aim was to build a computer that could operate at processing speeds approaching one microsecond per instruction, about one million instructions per second. Mu (or  ) is a prefix in the SI and other systems of units denoting a factor of 10 6 (one millionth).
At the end of 1958 Ferranti agreed to collaborate with Manchester University on the project, and the computer was shortly afterwards renamed Atlas, with the joint venture under the control of Tom Kilburn. The first Atlas was officially commissioned on 7 December 1962, and was considered at that time to be equivalent to four IBM 7094s and nearly as fast as the IBM 7030 Stretch, then the world's fastest supercomputer.






The machine had many innovative features but the key operating parameters were as follows (the store size relates to the Manchester installation; the others were larger):
48-bit word size. A word could hold one floating-point number, one instruction, two 24-bit addresses or signed integers, or eight 6-bit characters.
24-bit (2 million words, 16 million characters) address space that embraced supervisor ('sacred') store, V-store, fixed store and the user store
16K words of core store (equivalent to 96 KB), featuring interleaving of odd/even addresses
96K words of drum store (eqv. to 576 KB), split across four drums but integrated with the core store using virtual memory (at that time referred to as 'one-level store') and paging techniques
A large number (more than 100) of high-speed index registers (B-lines) that could be used for address modification in the mostly double-modified instructions. The register address space also included special registers such as the extracode operand address, the exponent of the floating-point accumulator and three control (program counter) registers: supervisor control, extracode control and user control.
Capability for the addition of (for the time) sophisticated new peripherals such as magnetic tape
Peripheral control through V-store addresses, interrupts and extracode routines
An associative memory (content-addressable memory) to determine whether the desired virtual memory location was in core store
Instruction pipelining
Atlas did not use a synchronous clocking mechanism it was an asynchronous Processor so performance measurements were not easy but as an example:
Fixed-point register add   1.59 microseconds
Floating-point add, no modification   1.61 microseconds
Floating-point add, double modify   2.61 microseconds
Floating-point multiply, double modify   4.97 microseconds



One feature of the Atlas was "Extracode", a technique that allowed complex instructions to be implemented in software.
The uppermost ten bits of a 48-bit Atlas machine instruction denoted which operation should be performed. If the most significant bit was set to zero, this was an ordinary machine instruction executed directly by the hardware. If the uppermost bit was set to one, this was an Extracode and was implemented as a special kind of subroutine jump to a location in the fixed store (ROM), its address being determined by the other nine bits. About 250 extracodes were implemented, of the 512 possible.
Extracode mode had its own program address counter, and easy access to the instruction operands.
Extracodes were what would be called software interrupt or trap today. They were used to call mathematical procedures which would have been too inefficient to implement in hardware, for example sine, logarithm, and square root. But about half of the codes were designated as Supervisor functions, which invoked operating system procedures. Typical examples would be  Print the specified character on the specified stream  or  Read a block of 512 words from logical tape N . Extracodes were the only means by which a program could communicate with the Supervisor. Other UK machines of the era, such as the Ferranti Orion, had similar mechanisms for calling on the services of their operating systems.



Atlas pioneered many software concepts still in common use today, including the Atlas Supervisor, "considered by many to be the first recognisable modern operating system".
One of the first high level languages available on Atlas was named Atlas Autocode, which was contemporary to Algol60 and created specifically to address what Tony Brooker perceived to be some defects in that language. The Atlas did however support Algol 60, as well as Fortran and COBOL. Being a university machine it was patronised by a large number of the student population, who had access to a protected machine code development environment.



Manchester computers



Notes

Bibliography



The Central Control Unit of the "Atlas" Computer, F. H. Sumner, G. Haley, E. C. Y. Chen, Information Processing 1962, Proc. IFIP Congress '62
One-Level Storage System, T. Kilburn, D. B. G. Edwards, M. J. Lanigan, F. H. Sumner, IRE Trans. Electronic Computers April 1962 Accessed 2011-10-13
The Manchester University Atlas Operating System, Part I: Internal Organization, T. Kilburn, D. J. Howarth, R. B. Payne, F. H. Sumner, Comp. J. October 1961
The Manchester University Atlas Operating System, Part II: Users' Description, D. J. Howarth, R. B. Payne, F. H. Sumner, Comp. J. October 1961
The Atlas Supervisor, T. Kilburn, R .B. Payne, D .J. Howarth, reprinted from Computers Key to Total Systems Control, Macmillan 1962
The Atlas Scheduling System, D. J. Howarth, P. D. Jones, M. T. Wyld, Comp. J. October 1962
The First Computers: History and Architectures, edited by Ra l Rojas and Ulf Hashagen, 2000, MIT Press, ISBN 0-262-18197-5
A History of Computing Technology, M. R. Williams, IEEE Computer Society Press, 1997, ISBN 0-8186-7739-2



The Atlas Autocode Reference Manual
The Atlas Supervisor paper (T Kilburn, R B Payne, D J Howarth, 1962)