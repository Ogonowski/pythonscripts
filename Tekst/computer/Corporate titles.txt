Corporate titles or business titles are given to company and organization officials to show what duties and responsibilities they have in the organization. Such titles are used in publicly and privately held for-profit corporations. In addition, many non-profit organizations, educational institutions, partnerships, and sole proprietorships also confer corporate titles.
The highest-level executives in senior management usually have titles beginning with "chief" and are therefore usually called "C-level" or part of the "C-suite". The traditional three such officers are chief executive officer (CEO), chief operations officer (COO), and chief financial officer (CFO). Depending on the management structure, C-titles may exist instead of or are blended/overlapped with other traditional executive titles, such as president, various designations of vice presidents (e.g. VP of marketing), and general managers or directors of various divisions (such as director of marketing); the latter may or may not imply membership of the board of directors.
Certain other prominent C-level positions have emerged, some of which are sector-specific. For example, CEO and chief risk officer (CRO) positions are often found in many types of financial services companies. Technology companies of all sorts now tend to have a chief technology officer (CTO) to manage technology development. A chief information officer (CIO) oversees IT (information technology) matters, either in companies that specialize in IT or in any kind of company that relies on it for supporting infrastructure.
Many companies now also have a chief marketing officer (CMO), particularly mature companies in competitive sectors, where brand management is a high priority. In creative/design industries, there is sometimes a chief creative officer (CCO), responsible for keeping the overall look and feel of different products consistent across a brand. A chief administrative officer may be found in many large complex organizations that have various departments or divisions. Additionally, many companies now call their top diversity leadership position the chief diversity officer (CDO). However, this and many other nontraditional and/or lower-ranking C-level titles (see below) are not universally recognized as corporate officers, and they tend to be specific to particular organizational cultures or the preferences of employees.



There are considerable variations in the composition and responsibilities of corporate titles.
Within the corporate office or corporate center of a company, some companies have a chairman and CEO as the top-ranking executive, while the number two is the president and COO; other companies have a president and CEO but no official deputy. Typically, C-level managers are "higher" than vice presidents, although many times a C-level officer may also hold a vice president title, such as executive vice president and CFO. The board of directors is technically not part of management itself, although its chairman may be considered part of the corporate office if he or she is an executive chairman.
A corporation often consists of different businesses, whose senior executives report directly to the CEO or COO. If organized as a division then the top manager is often known as an executive vice president (for example, Todd Bradley, who used to head the Personal Systems Group in Hewlett-Packard). If that business is a subsidiary which has considerably more independence, then the title might be chairman and CEO (for example, Philip I. Kent of Turner Broadcasting System in Time Warner).
In many countries, particularly in Europe and Asia, there is a separate executive board for day-to-day business and supervisory board (elected by shareholders) for control purposes. In these countries, the CEO presides over the executive board and the chairman presides over the supervisory board, and these two roles will always be held by different people. This ensures a distinction between management by the executive board and governance by the supervisory board. This seemingly allows for clear lines of authority. There is a strong parallel here with the structure of government, which tends to separate the political cabinet from the management civil service.
In the United States and other countries that follow a single-board corporate structure, the board of directors (elected by the shareholders) is often equivalent to the European/Asian supervisory board, while the functions of the executive board may be vested either in the board of directors or in a separate committee, which may be called an operating committee (J.P. Morgan Chase), management committee (Goldman Sachs), executive committee (Lehman Brothers), or executive council (Hewlett-Packard), composed of the division/subsidiary heads and C-level officers that report directly to the CEO.



State laws in the United States traditionally required certain positions to be created within every corporation, such as president, secretary and treasurer. Today, the approach under the Model Business Corporation Act, which is employed in many states, is to grant companies discretion in determining which titles to have, with the only mandated organ being the board of directors.
Some states that do not employ the MBCA continue to require that certain offices be established. Under the law of Delaware, where most large US corporations are established, stock certificates must be signed by two officers with titles specified by law (e.g. a president and secretary or a president and treasurer). Every corporation incorporated in California must have a chairman of the board or a president (or both), as well as a secretary and a chief financial officer.
LLC-structured companies are generally run directly by their members (shareholders), but the members can agree to appoint officers such as a CEO, or to appoint "managers" to operate the company.
American companies are generally led by a chief executive officer (CEO). In some companies, the CEO also has the title of president. In other companies, the president is responsible for internal management of the company while the CEO is responsible for external relations. Many companies also have a chief financial officer (CFO), chief operating officer (COO) and other "C-level" positions that report to the president and CEO. The next level of middle management may be called vice president, director or manager, depending on the company.



In British English, the title of managing director is generally synonymous with that of chief executive officer. Managing directors do not have any particular authority under the Companies Act in the UK, but do have implied authority based on the general understanding of what their position entails, as well as any authority expressly delegated by the board of directors.



In Japan, corporate titles are roughly standardized across companies and organizations; although there is variation from company to company, corporate titles within a company are always consistent, and the large companies in Japan generally follow the same outline. These titles are the formal titles that are used on business cards. Korean corporate titles are similar to those of Japan, as the South Korean corporate structure had been influenced by the Japanese model.
Legally, Japanese and Korean companies are only required to have a board of directors with at least one representative director. In Japanese, a company director is called a torishimariyaku ( ) and the representative director is called a daihyo torishimariyaku ( ). The equivalent Korean titles are isa ( ,  ) and daepyo-isa ( ,  ). These titles are often combined with lower titles, e.g. senmu torishimariyaku or jomu torishimariyaku for Japanese executives who are also board members. Most Japanese companies also have statutory auditors, who operate alongside the board of directors in a supervisory role.
The typical structure of executive titles in large companies includes the following: 
The top management group, comprising jomu/sangmu and above, is often referred to collectively as "senior management" (  or  ; kambu or juyaku in Japanese; ganbu or jungy k in Korean).
Some Japanese and Korean companies have also adopted American-style C-level titles, but these are not yet widespread and their usage varies. For example, although there is a Korean translation for chief operating officer ( , choego uny ng chaegimja), not many companies have yet adopted it with an exception of a few multi-national companies such as Samsung and CJ, while the chief financial officer title is often used alongside other titles such as bu-sajang (SEVP) or J nmu (EVP).
Since the late 1990s, many Japanese companies have introduced the title of shikko yakuin ( ) or "officer," seeking to emulate the separation of directors and officers found in American companies. In 2002, the statutory title of shikko yaku ( ) was introduced for use in companies that introduced a three-committee structure in their board of directors. The titles are frequently given to bucho and higher-level personnel. Although the two titles are very similar in intent and usage, there are several legal distinctions: shikko yaku make their own decisions in the course of performing work delegated to them by the board of directors, and are considered managers of the company rather than employees, with a legal status similar to that of directors. Shikko yakuin are considered employees of the company that follow the decisions of the board of directors, although in some cases directors may have the shikko yakuin title as well.






Chief academic officer or CAO; in some academic institutions, in charge of all academic aspects of the learning institution
Chief accounting officer
Chief administrative officer or CAO
Chief agility officer, "tasked with creating and nurturing an Agile culture that pervades the whole organization" as recommended by Jim Highsmith, one of the original writers of the Agile Manifesto
Chief analytics officer or CAO   high-level corporate manager with overall responsibility for the analysis and interpretation of data relevant to a company's activities; generally reports to the CEO, or COO.
Chief architect the executive responsible for designing systems for High Availability and Scalability, specifically in technology companies.
Chief audit executive or CAE   high-level 'independent' corporate executive with overall responsibility for the Internal audit
Chief business officer or CBO
Chief business development officer or CBDO
Chief brand officer or CBO   a relatively new executive-level position at a corporation, company, organization, or agency, typically reporting directly to the CEO or board of directors. The CBO is responsible for a brand's image, experience, and promise, and propagating it throughout all aspects of the company. The brand officer oversees marketing, advertising, design, public relations and customer service departments. The brand equity of a company is seen as becoming increasingly dependent on the role of a CBO.
Chief commercial officer or CCO   the executive responsible for commercial strategy and development
Chief communications officer or CCO.
Chief compliance officer   in charge of regulatory compliance, especially Sarbanes Oxley
Chief content officer or CCO   the executive responsible for developing and commissioning content (media) for broadcasting channels and multimedia exploitation. This is commonly used by media companies such as Netflix, AOL, Time, and Slam Content.
Chief creative officer
Chief credit officer or CCO
Chief customer officer   responsible in customer-centric companies for the total relationship with an organization s customers.
Chief data officer or CDO
Chief debriefing officer or CDO   responsible for getting the status, and maintaining forward motion in production.
Chief design officer or CDO
Chief digital officer or CDO
Chief diversity officer or CDO
Chief electrification officer   responsible for electrical generating and distribution systems. The title was used mainly in developed countries from the 1880s to 1940s during the electrification of industry, but is still used in some developing countries.
Chief engineering officer   similar to the more common CTO; responsible for technology/product R & D and/or manufacturing issues in a technology company. This position is generally separate from any internal IT functions (the realm of the CIO). This title occurs more in those technology companies that make products other than software, but increasingly CTO is used instead now in both software and non-software industries alike to refer to overseeing the development of technology being commercialized.
Chief executive officer or CEO/(United States), chief executive or managing director (United Kingdom, Commonwealth and some other English-speaking countries)   The CEO of a corporation is the highest-ranking management officer of a corporation and has final decisions over human, financial, environmental and technical operations of the corporation. The CEO is also a visionary, often leaving day-to-day operations to the president, COO or division heads. Other corporate officers such as the COO, CFO, CIO, and division heads report to the CEO. The CEO is also often the chairman of the board, especially in closely held corporations and also often in public corporations. Recently, though, many public companies have been separating the roles of chairman and CEO to improve corporate governance. President and CEO is a popular combination if there is a separate chairman.
Chief executive manager or CEM (United States). The CEM of a Limited liability company is the highest ranking management person of an LLC and has final decisions over the day-to-day operations and of an LLC. The CEM often reports directly to the membership of the LLC. The CEM role and leadership within a limited liability company is comparable to the role of Chief executive officer within corporate governance.
Chief experience officer or CXO, not to be confused with CxO, a term commonly used when referring to any one of various chief officers
Chief financial officer or CFO   high-level corporate officer with oversight of corporate finances; reports to the CEO. May concurrently hold the title of treasurer or oversee such a position; finance deals with accounting and audits, while treasurer deals with company funds.
Chief human resources officer or CHRO
Chief information officer or CIO   high-level corporate manager with overall responsibility for the company's information resources and processing environment; generally reports to the CEO or COO. Particularly important in IT companies or companies that rely heavily on an IT infrastructure for their operations.
Chief information security officer or CISO
Chief innovation officer
Chief insurance officer
Chief intellectual property officer or CIPO   responsible for the management of the IP assets and potential IP-related liabilities of the enterprise
Chief international officer or CIO   responsible for development and implementation of overseas markets
Chief investment officer or CIO   high-level corporate officer responsible for the assets of an investment vehicle or investment management company and/or responsible for the asset-liability management (ALM) of typical large financial institutions such as insurers, banks and/or pension funds; generally reports to the CEO or CFO.
Chief knowledge officer or CKO   the CKO is responsible for managing intellectual capital and the custodian of knowledge management practices, usually in a legal organization
Chief legal officer or CLO   the CLO is traditionally referred to as the General Counsel, or GC
Chief lending officer or CLO   mid to high-level corporate manager with overall responsibility for the company's lending portfolio; generally reports to the CEO or COO. Particularly important in financial institutions or corporations where funds are lent out.
Chief learning officer or CLO   the CLO is commonly responsible for all Learning/Training Operations
Chief marketing officer or CMO
Chief media officer
Chief medical officer or CMO   especially in a pharmaceutical company, the person responsible for scientific and medical excellence of the company's research, development and products, or the highest ranking physician at a hospital. The title is used in many countries for the senior government official who advises on matters of public health importance.
Chief networking officer or CNO   responsible for the social capital within the company and between the company and its partners
Chief operating officer or COO/ director of operations for the nonprofit sector   high-level corporate officer with responsibility for the daily operation of the company; reports to the CEO. The COO often also carries the title of president, especially if the number one is the chairman and CEO. Unlike other C-suite positions, which tend to be defined according to commonly designated responsibilities across most companies, the COO job tends to be defined in relation to the specific CEO with whom he/she works, given the close working relationship of these two individuals. In many ways, the selection of a COO is similar to the selection of a vice president of the United States: the role (including the power and responsibilities therein) can vary dramatically, depending on the style and needs of the president. Similarly, the COO role is highly contingent and situational, as the role changes from company to company and even from CEO to successor CEO within the same company. Many modern companies operate without a COO. For example, in 2006 more than 60 percent of Fortune 500 companies did not have a COO, and in 2007 almost 58 percent of Fortune 500 companies did not have a COO. In these instances the CEO either takes on more roles and responsibilities, or the roles traditionally assigned to the COO are carried out by sub C-suite executives (as discussed above).
Chief people officer, similar in nature to chief human resources officer but often with a more substantive emphasis on the broader management and development of people within an organization, beyond traditional human resources functional areas:
Chief performance officer
Chief privacy officer
Chief process officer or CPO
Chief procurement officer or CPO
Chief product officer or CPO   responsible for all product-related matters. Usually includes product conception and development, production in general, innovation, project and product management. In many IT/telecommunications companies, this position is organically higher than the chief technical officer and includes release management and production. In small and mid-sized companies it can also play the role of the COO.
Chief program officer or CPO   high-level corporate officer with responsibility for the daily operations of an organization's programs. Often found in government and non-profit organizations. Similar to the COO found in for profit organizations. Reports to the CEO.
Chief promotions officer or CPO
Chief quality officer or CQO; responsible for setting up quality goals and assuring that those goals are kept
Chief relationship officer or CRO   Companies have used this title to mean several different things: (1) officer responsible for key external relationships including Investor Relations, Government Relations and sometimes Public Relations or Communications; (2) alternate term for chief human resources officer; (3) alternate term for chief networking officer
Chief research officer or CRO   responsible for research within the organization
Chief revenue officer or CRO   responsible for all revenue-related activities within the organization
Chief risk officer (chief risk management officer) or CRO   common in financial institutions
Chief sales officer or CSO   responsible for all sales/revenue within the organization
Chief science officer   responsible for research, development and new technologies. In other companies, this is known as the chief scientist.
Chief search officer   responsible for research, development and planning of brand search marketing
Chief security officer or CSO
Chief software manufacturing officer or CSMO   responsible for all aspects of software development and operation in a Services Company
Chief specialist officer or CSO   VP-level corporate officer responsible for a specific function or area at corporate level
Chief strategy officer (chief strategic planning officer) or CSO (CSPO)
Chief supply chain officer or CSCO   high-level corporate officer responsible for the supply chain management of the company
Chief sustainability officer or CSO
Chief tax officer or CTO   high-level corporate officer responsible for the tax function (compliance, accounting and planning) within a company. The CTO may report to the CEO, CFO, general counsel or the internal audit function.
Chief technology officer or CTO (sometimes chief technical officer)   high-level corporate officer responsible for the company's technology/R&D direction. Now common in both IT/software and other technological fields as well, the focus on this position is typically overseeing the development of technology to be commercialized. (For an IT company, the subject matter would be similar to the CIO's, however the CTO's focus is technology for the firm to sell versus technology used for facilitating the firm's own operations.)
Chief visionary officer
Chief web officer



Chairman of the board   presiding officer of the corporate board of directors. The Chairman influences the board of directors, which in turn elects and removes the officers of a corporation and oversees the human, financial, environmental and technical operations of a corporation.
The CEO may also hold the title of chairman, resulting in an executive chairman. In this case, the board frequently names an independent member of the board as a lead director.
Executive chairman   the chairman's post may also exist as an office separate from that of CEO, and it is considered an executive chairman if that titleholder wields influence over company operations, such as Steve Case of AOL Time Warner and Douglas Flint of HSBC. In particular, the group chairmanship of HSBC is considered the top position of that institution, outranking the chief executive, and is responsible for leading the board and representing the company in meetings with government figures. Prior to the creation of the group management board in 2006, HSBC's chairman essentially held the duties of a chief executive at an equivalent institution, while HSBC's chief executive served as the deputy. After the 2006 reorganization, the management cadre ran the business, while the chairman oversaw the controls of the business through compliance and audit and the direction of the business.
Non-executive chairman   also a separate post from the CEO, unlike an executive chairman, a non-executive chairman does not interfere in day-to-day company matters. Across the world, many companies have separated the roles of chairman and CEO, often resulting in a non-executive chairman, saying that this move improves corporate governance.

Chief of Staff is a corporate director level manager who has overall responsibility for the staff activity within the company who often would have responsibility of hiring and firing of the highest level managers and sometimes directors. They can work with and report directly to managing directors and the chief executive officer.
Commissioner
Financial Control Officer, FCO or FC, also Comptroller or Controller   supervises accounting and financial reporting within an organization
Director or member of the board of directors   high-level official with a fiduciary responsibility of overseeing the operation of a corporation and elects or removes officers of a corporation; nominally, directors, other than the chairman are usually not considered to be employees of the company per se, although they may receive compensation, often including benefits; in publicly held companies. A board of directors is normally made up of members (directors) who are a mixture of corporate officials who are also management employees of the company (inside directors) and persons who are not employed by the company in any capacity (outside directors or non-executive directors). In privately held companies, the board of directors often only consists of the statutory corporate officials, and in sole proprietorship and partnerships, the board is entirely optional, and if it does exist, only operates in an advisory capacity to the owner or partners. Non-profit corporations are governed by a board of trustees instead of a board of directors
Director   a manager of managers within an organization who is often responsible for a major business function and who sometimes reports to a vice president (note that in some financial services companies the title vice president has a different meaning). Often used with name of a functional area; finance director, director of finance, marketing director, and so on. Not to be confused with a member of the board of directors, who is also referred to as a director. Usually denotes the lowest executive level within a company, except in the banking industry. Alternatively, a manager of managers is often referred to as a "senior manager' or as an "associate vice president", depending upon levels of management, and industry type.
Fellow   In a dual career ladder organization a fellow is often a very senior technical position and is equal to a director or VP
President   legally recognized highest "titled" corporate officer, and usually a member of the board of directors. There is much variation; often the CEO also holds the title of president, while in other organizations if there is a separate CEO, the president is then second highest-ranking position. In such a case the president is often the COO and is considered to be more focused upon daily operations compared to the CEO, who is supposed to be the visionary. If the corporate president is not the COO (such as Richard Parsons of Time Warner from 1995 2001), then many division heads report directly to the CEO themselves, with the president taking on special assignments from the CEO.
Secretary or company secretary   legally recognized "titled" corporate officer who reports to the board of directors and is responsible for keeping the records of the board and the company. This title is often concurrently held by the treasurer in a dual position called secretary-treasurer; both positions may be concurrently held by the CFO. Note, however, that the Secretary has a reporting line to the board of directors, regardless of any other reporting lines conferred by concurrent titles.
Secretary-treasurer   in many cases, the offices of Secretary and Treasurer are held by the same person. In this case, the position is commonly referred to by the combined title Secretary-Treasurer
Treasurer   legally recognized corporate officer entrusted with the fiduciary responsibility of caring for company funds. Often this title is held concurrently with that of secretary in a dual role called secretary-treasurer. It can also be held concurrently with the title of CFO or fall under the jurisdiction of one, though the CFO tends to oversee the finance department instead, which deals with accounting and audits, while the treasurer deals directly with company funds. Note, however, that the treasurer has a reporting line to the board of directors, regardless of any other reporting lines conferred by concurrent titles.
Statutory agent
Superintendent
Owner (sometimes proprietor or sole proprietor, for sole proprietorships)
Partner   Used in many different ways. This may indicate a co-owner as in a legal partnership or may be used in a general way to refer to a broad class of employees or temporary/contract workers who are often assigned field or customer service work. Associate is often used in a similar way.
Principal   may refer to an owner of the business or a high-level technical worker such as Principal Engineer or Principal Scientist. The Principal title is often used in dual career ladder organizations and may be equivalent to manager or director.
Vice Chair or Vice Chairman   officer of the board of directors who may stand in for the chairman in his/her absence. However, this type of vice chairman title on its own usually has only an advisory role and not an operational one (such as Ted Turner at Time Warner). An unrelated definition of vice chair describes an executive who is higher ranking or has more seniority than executive vice president. Sometimes, EVPs report to the vice chair, who in turn reports directly to the CEO (so vice chairs in effect constitute an additional layer of management), other vice chairs have more responsibilities but are otherwise on an equal tier with EVPs. Executive vice chairman are usually not necessarily on the board of directors. Royal Bank of Canada previously used vice chair in their inner management circle until 2004 but have since renamed them as group head.



Associate   Used in many different ways in US business. Often used to indicate a customer service position or temporary/part-time employee. Some US businesses use the term for all or most exempt employees. In legal firms an associate attorney indicates a lawyer who is not a partner of the law firm. Partner is often used in a similar way.
Supervisor
Foreman
General manager or GM
Manager
Of Counsel   A lawyer working on a part-time or temporary basis for a company or law firm.
Vice president   Middle or upper manager in a corporation. They often appear in various hierarchical layers such as executive vice president, senior vice president, associate vice president, or assistant vice president, with EVP usually considered the highest and usually reporting to the CEO or president. Many times, corporate officers such as the CFO, COO, CIO, CTO, secretary, or treasurer will concurrently hold vice president titles, commonly EVP or SVP. Vice presidents in small companies are also referred to as chiefs of a certain division, such as vice president for finance, or vice president for administration. Note that in some financial contexts, the title of vice president is actually subordinate to a director.



Other corporate employee classifications, in US organizations, include:
Exempt   exempt from the Fair Labor Standards Act (FLSA). In a corporation, this generally applies to salaried professional staff, and executives, earning in excess of $23,660 annually.
Non-exempt   Generally an employee paid by the hour who is entitled to a minimum wage, overtime pay at the rate of time and one-half the regular rate for all hours worked in excess of 40 hours per week or according to state labor laws, as well as other protections under child labor and equal pay laws.



Most modern corporations also have non-employee workers. These are usually 'temps' (temporary workers) or consultants who, depending on the project and their experience, might be brought on to lead a task for which the skill-set did not exist within the company, or in the case of a temp, in the vernacular sense, to perform busy-work or an otherwise low-skilled repetitive task for which an employee is deemed too valuable to perform.
Non-employees generally are employed by outside agencies or firms, but perform their duties within a corporation or similar entity. They do not have the same benefits as employees of that company, such as pay-grades, health insurance, or sick days.
Some high-skilled consultants, however, may garner some benefits such as a bonus, sick leave, or food and travel expenses, since they usually charge a high flat-fee for their services, or otherwise garner high hourly wages. An example of high-skilled consultants include lawyers, lobbyists, and accountants who may not be employed by a corporation, but have their own firms or practices. Most temps, however, are compensated strictly for the hours they work, and are generally non-exempt.




Corporate governance
Corporate liability
Identification with corporation






Taking Stock - Corporate Execs Get Scammed, FBI