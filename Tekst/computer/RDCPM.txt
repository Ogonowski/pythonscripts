86-DOS was an operating system developed and marketed by Seattle Computer Products (SCP) for its Intel 8086-based computer kit. Initially known as QDOS (Quick and Dirty Operating System) the name was changed to 86-DOS once SCP started licensing the operating system in 1980.
86-DOS had a command structure and application programming interface that imitated that of Digital Research's CP/M operating system, which made it easy to port programs from the latter. The system was purchased by Microsoft and developed further as MS-DOS and PC DOS.



86-DOS was created because sales of the Seattle Computer Products 8086 computer kit, demonstrated in June 1979 and shipped in November, were languishing due to the absence of an operating system. The only software which SCP could sell with the board was Microsoft's Standalone Disk BASIC-86, which Microsoft had developed on a prototype of SCP's hardware. SCP wanted to offer the 8086-version of CP/M that Digital Research had announced, but its release date was uncertain. This was not the first time Digital Research had lagged behind hardware developments; two years earlier it had been slow to adapt CP/M for new floppy disk formats and hard disks. In April 1980 SCP assigned 24-year-old Tim Paterson to develop a substitute for CP/M-86.
Using a CP/M-80 manual as reference Paterson modeled 86-DOS after its architecture and interfaces, but adapted to meet the requirements of Intel's 8086 16-bit processor, for easy (and partially automated) source-level translatability of the many existing 8-bit CP/M programs; porting them to either DOS or CP/M-86 was about equally difficult. At the same time he made a number of changes and enhancements to address what he saw as CP/M's shortcomings. CP/M cached file system information in memory for speed, but this required a user to force an update to a disk before removing it; if the user forgot, the disk would be corrupt. Paterson took the safer but slower approach of updating the disk with each operation. CP/M's PIP command, which copied files, supported several special file names that referred to hardware devices such as printers and communication ports. Paterson built these names into the operating system as device files so that any program could use them. He gave his copying program the more intuitive name COPY. Rather than implementing CP/M's file system, he drew on Microsoft Standalone Disk BASIC-86's FAT filesystem.
By mid-1980 SCP advertised 86-DOS, priced at $95 for owners of its $1290 8086 board and $195 for others. It touted the software's ability to read Z80 source code from a CP/M disk and translate it to 8086 source code, and promised that only "minor hand correction and optimization" was needed to produce 8086 binaries.



In October 1980, IBM was developing what would become the original IBM Personal Computer. CP/M was by far the most popular operating system in use at the time, and IBM felt it needed CP/M in order to compete. IBM's representatives visited Digital Research and discussed licensing with Digital Research's licensing representative, Dorothy Kildall (n e McEwen), who hesitated to sign IBM's non-disclosure agreement. Although the NDA was later accepted, Digital Research would not accept IBM's proposal of $250,000 in exchange for as many copies as IBM could sell, insisting on the usual royalty-based plan. In later discussions between IBM and Bill Gates, Gates mentioned the existence of 86-DOS and IBM representative Jack Sams told him to get a license for it.



Microsoft purchased a non-exclusive license for 86-DOS from Seattle Computer Products in December 1980 for $25,000. In May 1981, it hired Tim Paterson to port the system to the IBM PC, which used the slower and less expensive Intel 8088 processor and had its own specific family of peripherals. IBM watched the developments daily, submitted over 300 change requests before it accepted the product and wrote the user manual for it.
In July 1981, a month before the PC's release, Microsoft purchased all rights to 86-DOS from SCP for $50,000. It met IBM's main criteria: it looked like CP/M, and it was easy to adapt existing 8-bit CP/M programs to run under it, notably thanks to the TRANS command which would translate source files from 8080 to 8086 machine instructions. Microsoft licensed 86-DOS to IBM, and it became PC DOS 1.0. This license also permitted Microsoft to sell DOS to other companies, which it did. The deal was spectacularly successful, and SCP later claimed in court that Microsoft had concealed its relationship with IBM in order to purchase the operating system cheaply. SCP ultimately received a 1 million dollar settlement payment.



When Digital Research founder Gary Kildall examined PC DOS and found that it duplicated CP/M's programming interface, he wanted to sue IBM, which at the time claimed that PC DOS was its own product. However, Digital Research's attorney did not believe that the relevant law was clear enough to sue. Nonetheless, Kildall confronted IBM and persuaded them to offer CP/M-86 with the PC in exchange for a release of liability.
Controversy has continued to surround the similarity between the two systems. Perhaps the most sensational claim comes from Jerry Pournelle, who claims that Kildall personally demonstrated to him that DOS contained CP/M code by entering a command in DOS that displayed Kildall's name; as of 2006 Pournelle has not revealed the command and nobody has come forward to corroborate his story. A 2004 book about Kildall says that he used such an encrypted message to demonstrate that other manufacturers had copied CP/M, but does not say that he found the message in DOS; instead Kildall's memoir (a source for the book) pointed to the well-known interface similarity. Paterson insists that the 86-DOS software was his original work, and has denied referring to or otherwise using CP/M code while writing it. After the 2004 book appeared, he sued the authors and publishers for defamation. The court ruled in summary judgement that no defamation had occurred, as the book's claims were opinions based on research or were not provably false.



By 1982, when IBM asked Microsoft to release a version of DOS that was compatible with a hard disk, PC DOS 2.0 was an almost complete rewrite of DOS, so by March 1983, very little of 86-DOS remained. The most enduring element of 86-DOS was its primitive line editor, EDLIN, which remained the only editor supplied with Microsoft versions of DOS until the June 1991 release of MS-DOS 5.0, which included a TUI-based editor, MS-DOS Editor based on QBasic. EDLIN can still be used on contemporary machines, since there is an emulated DOS environment up to Windows 8 (32 bit).






Seattle Computer Products' 86-DOS supported the FAT12 filesystem on a range of 8.0" and 5.25" floppy disk drives on S-100 floppy disk controller hardware manufactured by Cromemco, Tarbell Electronics and North Star Computers. The Western Digital FD1771-based Cromemco and Tarbell boards supported one-sided single-density soft-sectored drives. A Tarbell double-density board utilizing the FD1791 was supported as well. At a later stage, SCP offered advanced floppy disk controllers like the Disk Master series themselves.
86-DOS itself did not take advantage of a FAT ID byte or BPB (as later DOS issues do) to distinguish between different media formats; instead different drive letters were hard-coded (at time of compilation) to be associated with different physical floppy drives, sides and densities, that is, depending on its type, a disk had to be addressed under a certain drive letter to be recognized correctly (a concept later emulated with more flexibility by DRIVER.SYS under DOS 3.x and higher).
Two logical format variants of the 86-DOS 12-bit FAT format existed, the original format with 16-byte directory entries and the later format (since 86-DOS 0.42) with 32-byte directory entries. Only the second one is logically compatible with the FAT12 format known since the release of MS-DOS and PC DOS. MS-DOS still cannot mount such volumes as in absence of a BPB it falls back to retrieve the FAT ID in the FAT entry for cluster 0 to choose among hard-coded disk geometry profiles. In all formats of a volume formatted under MS-DOS which would otherwise be supported by both systems (and typically also in all other formats), this ID is located in the first byte of logical sector 1 (that is, the volume's second sector with physical CHS address 0/0/2 or LBA address 1), since MS-DOS assumes a single reserved sector (for the boot sector). Under 86-DOS, the reserved sectors area is significantly larger (whole tracks), and therefore the prototypical FAT ID 0xFE (and 0xFF) is located elsewhere on disk making it impossible for MS-DOS to retrieve it, and even if it would, the hard-wired disk profile associated with it would not take this larger reserved sectors region under 86-DOS into account.
CP/M 2 floppy media were readable through RDCPM.
86-DOS did not offer any specific support for fixed disks by itself, but third-party solutions in form of hard disk controllers and corresponding I/O system extensions for 86-DOS were available from companies like Tallgrass, making hard disks accessible similar to superfloppies within the size limits of the FAT12 file system.
Various OEM versions of MS-DOS 1.2x and 2.x supported a number of similar 8.0" FAT12 floppy disk formats as well, although not identical to those supported by 86-DOS.
Disk formats supported by one of the last versions developed by Tim Paterson at Microsoft, MS-DOS 1.25  (March 1982) for the SCP Gazelle computer with SCP controller or Cromemco 16FDC controller (by default, this version only supported the MS-DOS-compatible variants of the 8.0" formats with a single reserved sector but it could be built to provide two extra drive letters to read and write floppies in the previous SCP 86-DOS 8.0" disk formats since 0.42 as well):
In 1984 Seattle Computer Products released an OEM version of MS-DOS 2.0 for the SCP S-100 computer with SCP-500 Disk Master Floppy controller. It added support for 5.25" DD/1S (180 KB) and DD/2S (360 KB) FAT12 formats and supported the older formats as well, although possibly with some of the parameters modified compared to MS-DOS 1.25.



MIDAS
MSX-DOS



^ Somewhat ironically, Tim Paterson claims to have hidden an easter egg, displaying his own name, in the FAT code of MSX-DOS 1, when he developed this Z80-based CP/M clone resembling 86-DOS/MS-DOS 1.25 in 1983: Paterson, Tim (2014-02-17). "The History of MSX-DOS". Jorito, Maggoo, John Hassink, MSX Resource Center. Retrieved 2014-05-31. 
^ a b c d e 8.0" 250.25 KB images formatted under 86-DOS 1.00 sport a FAT ID of FEh, however, in contrast to MS-DOS/PC DOS, 86-DOS does not seem to use this to detect the disk format, as this information is hard-coded into disk profiles associated to certain drive letters at compile-time. MS-DOS would not be able to mount such volumes as (in absence of a BPB) it expects the FAT ID in logical sector 1, assuming only one reserved sector of 512 bytes (the boot sector in logical sector 0) instead of the 52 reserved sectors   128 bytes used by 86-DOS here. This works for MS-DOS, because the system files are not part of the reserved area under MS-DOS, while under 86-DOS there are no system files and the ca. 6 KB large DOS kernel is located in the reserved area.
^ a b c d Executing the CLEAR command under 86-DOS 1.00 COMMAND.COM seems to initialize a volume's FAT ID byte to FEh regardless of disk drive and format used.
^ DOS 1.x does not support a BPB, but this entry for the number of physical sectors per track corresponds with BPB offset 0x0D under DOS 3.0 and higher.
^ DOS 1.x does not support a BPB, but this entry for the number of heads corresponds with BPB offset 0x0F under DOS 3.0 and higher.
^ DOS 1.x does not support a BPB, but this entry for the bytes per logical sector corresponds with BPB offset 0x00 under DOS 2.0 and higher.
^ DOS 1.x does not support a BPB, but this entry for the logical sectors per cluster (allocation units) corresponds with BPB offset 0x02 under DOS 2.0 and higher.
^ DOS 1.x does not support a BPB, but this entry for the number of reserved logical sectors corresponds with BPB offset 0x03 under DOS 2.0 and higher.
^ DOS 1.x does not support a BPB, but this entry for the number of FATs corresponds with BPB offset 0x05 under DOS 2.0 and higher.
^ DOS 1.x does not support a BPB, but this entry for the number of root directory entries (  32 bytes) corresponds with BPB offset 0x06 under DOS 2.0 and higher.
^ DOS 1.x does not support a BPB, but this entry for the total number of logical sectors corresponds with BPB offset 0x08 under DOS 2.0 and higher.
^ DOS 1.x does not support a BPB, but this entry for the number of logical sectors per FAT corresponds with BPB offset 0x0B under DOS 2.0 and higher.
^ DOS 1.x does not support a BPB, but this entry for the number of hidden sectors corresponds with BPB offset 0x11 under DOS 3.0 and higher.
^ For unknown reasons, some Microsoft documents give a value of 3 for this entry, where 0 seems correct technically. SCP MS-DOS 1.25 implicitly assumes 0 as well.
^ DOS 1.x does not support a BPB, but this entry for the FAT ID corresponds with the media descriptor byte at BPB offset 0x0A under DOS 2.0 and higher.



^ a b c d e f g h i j k Hunter, David (March 1983). "The Roots of DOS: Tim Paterson". Softalk. Retrieved 2013-08-18. 
^ a b Paterson, Tim (June 1983). "An Inside Look at MS-DOS - The design decisions behind the popular operating system". Byte. 6 (Byte Publications Inc.) 8: 230. Retrieved 2013-10-19.  (NB. The article often uses "MS-DOS" to refer to both 86-DOS and MS-DOS, but mentions QDOS and 86-DOS in a sidebar article, "A Short History of MS-DOS".)
^ Cringely, Robert X. (June 1996). "Part II". Triumph of the Nerds: The Rise of Accidental Empires. Season 1. PBS. 
^ Edlin, Jim (1982-06-07). "CP/M Arrives - IBM releases a tailed-for-the-PC version of CP/M-86 that profits from the learning curve". PC Magazine: 43. Retrieved 2013-10-21. 
^ a b Seattle Computer Products (August 1980). "86-DOS - 8086 OPERATING SYSTEM - $95". Byte. 8 5. p. 174. Retrieved 2013-08-18.  (NB. The SCP advertisement already calls the product 86-DOS, but does not mention a specific version number. Version 0.3 is known to be called 86-DOS already, so the name change must have taken place either for version 0.2 or immediately afterwards in August 1980.)
^ Freiberger, Paul; Swaine, Michael (2000) [1984]. Fire in the Valley: The Making of the Personal Computer (2nd ed.). New York: McGraw-Hill. pp. 332 333. ISBN 0-07-135892-7. 
^ a b c 86-DOS version 0.3 (1980-11-15) License Agreement between Seattle Computer Products and Microsoft, signed 1981-01-06, published as part of the Comes v. Microsoft case as exhibit #1, retrieved 2013-04-01.
^ a b c d 86-DOS Sales Agreement between Seattle Computer Products and Microsoft, dated 1981-07-27, published as part of the Comes v. Microsoft case as exhibit #2, retrieved 2013-04-01. (NB. The document also carries a typed date stamp as of 1981-07-22.)
^ Duncan, Ray (1988) [1988]. The MS-DOS Encyclopedia. Microsoft Press. p. 20. ISBN 1-55615-049-0. 
^ "this WEEK in TECH". The TWiT Netcast Network (Podcast). 2006-10-16. Retrieved 2006-11-28. 
^ Evans, Harold; Buckland, Gail; Lefer, David (2004). They Made America. Little, Brown and Co. ISBN 0-316-27766-5. 
^ Paterson, Tim (1994-10-03). "The Origins of DOS: DOS Creator Gives His View of Relationship Between CP/M, MS-DOS" (PDF). Microprocessor Report (MicroDesign Resources) 8 (13). ISSN 0899-9341. Archived from the original (PDF) on 2012-05-31. 
^ "Programmer sues author over role in Microsoft history". USA Today. Associated Press. 2005-02-03. Retrieved 2006-11-28. 
^ Order (2007-07-25). Paterson v. Little, Brown, and Co., et al. W. D. Wash.. Retrieved on 2007-08-03.
^ Paterson, Tim (2007-09-30). "Design of DOS". DosMan Drivel. Archived from the original on 2013-01-20. Retrieved 2011-07-04. 
^ Schulman, Andrew; Brown, Ralf; Maxey, David; Michels, Raymond J.; Kyle, Jim (1994). Undocumented DOS - A programmer's guide to reserved MS-DOS functions and data structures - expanded to include MS-DOS 6, Novell DOS and Windows 3.1 (2 ed.). Addison Wesley. ISBN 0-201-63287-X. ISBN 978-0-201-63287-3. 
^ a b c d e f Paterson, Tim (June 1983). "A Short History of MS-DOS". Byte (6 ed.) 8. ISSN 0360-5280. Retrieved 2013-08-18.  (NB. This source mentions the 1981-07-27 as the date of purchase.)
^ a b c d e f g h i j k l m n o p q r s t u v w Paterson, Tim (2013-12-19) [1983]. "Microsoft DOS V1.1 and V2.0: /msdos/v11source/MSDOS.ASM". Computer History Museum, Microsoft. Retrieved 2014-03-25.  (NB. While the publishers claim this would be MS-DOS 1.1 and 2.0, it actually is SCP MS-DOS 1.25 and a mixture of Altos MS-DOS 2.11 and TeleVideo PC DOS 2.11.)
^ Seattle Computer Products (1981). "SCP 86-DOS 1.0 Addendum" (PDF). Retrieved 2013-04-02. 
^ a b c d e f g http://www.86dos.org/downloads/86DOS_FILES.ZIP, A ZIP file containing most of the files from 86-DOS 0.75 (1981-04-17/1981-04-18) to 1.00/1.10 (1981-07-21)
^ 86-DOS 1.00 disk images for SIMH simulator
^ MS-DOS 1.25 disk images for SIMH simulator
^ Shustek, Len (2014-03-24). "Microsoft MS-DOS early source code". Software Gems: The Computer History Museum Historical Source Code Series. Retrieved 2014-03-29.  (NB. While the author claims this would be MS-DOS 1.1 and 2.0, it actually is SCP MS-DOS 1.25 and a mixture of Altos MS-DOS 2.11 and TeleVideo PC DOS 2.11.)
^ Levin, Roy (2014-03-25). "Microsoft makes source code for MS-DOS and Word for Windows available to public". Official Microsoft Blog. Retrieved 2014-03-29.  (NB. While the author claims this would be MS-DOS 1.1 and 2.0, it actually is SCP MS-DOS 1.25 and a mixture of Altos MS-DOS 2.11 and TeleVideo PC DOS 2.11.)
^ "Seattle Computer Products 8086 S-100 Bus Microcomputer - Picture of 8.0" DD/1S distribution floppy disks for SCP MS-DOS 2.0". 2009-11-22. Archived from the original on 2013-09-04. Retrieved 2014-05-06. 



86-DOS documentation from Paterson Technology
Run SCP 86-DOS 1.0 in the Altair 8800 SIMH simulator by Howard M. Harte