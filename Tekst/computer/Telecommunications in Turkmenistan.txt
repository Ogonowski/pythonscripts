Turkmenistan has a state-controlled press and monitored communication systems. Turkmenistan s telecommunications services are considered to be the least developed of all the Commonwealth of Independent States (CIS) countries. Overall, the telecom market in this predominantly rural country is relatively small but has been trying boldly to expand in recent years. The state-owned Turkmen Telecom has been the primary provider of public telephone, email and internet services, and through a subsidiary has been operating a GSM mobile network in competition with a private mobile operator, BCTI (BCTI became MTS Turkmenistan in 2005).



The launch of the first Turkmen communication satellite TurkmenSat 1 is scheduled for launch in March 2015, the satellite has an anticipated service life of 15 years. The satellite will be launched aboard SpaceX Falcon 9. The satellite is built by French Thales Alenia Space and is from the Spacebus 4000 family. The satellite will cover Europe and significant part of Asian countries and Africa and will have transmission for TV, radio broadcasting and the internet. The satellite's operations will be controlled by the state-run Turkmenistan National Space Agency (TNSA).



BCTI was the only GSM operator in the country with a 10-year exclusive license granted in 1994. Due to lack of competition, BCTI continued to operate without investing much to cover rural areas. The expensive cost of the service has limited the number of subscribers to a very small percentage of the general population.
The Mobile phone sector started to improve rapidly after the expiration of company's exclusive license in 2004. The Russian mobile phone operator MTS acquired BCTI  and state owned communication company TurkmenTelekom opened a new subsidiary, Altyn Asyr. The number of mobile phone subscribers has now reached over 4,440,000 (MTS - 1,440,000 and Altyn Asyr GSM 3,000,000 ). With a population of 5 million, this translates into 88.8% mobile penetration rate.




Turkmenistan gained access to the Internet in 1997 through a contract with MCI Communications (later became MCI WorldCom). A small number of independent Internet Service Providers were forced out of business in 2001 when TurkmenTelecom was granted a monopoly over data services. Dependence on expensive satellite channels limited the availability of Internet to only two thousand subscribers. To upgrade the Internet backbone, Ministry of Communication signed a contract with TATA Communications for routing traffic through Transit-Asia-Europe fiber optic channel. As a result of this development, TurkmenTelecom started offering an access to the higher speed Internet with ADSL to the consumers in Ashgabat.
In 2008, MTS started offering Internet service to mobile subscribers via GPRS. Altyn Asyr was first to launch 3G and 2 mpbs mobile internet service in March 2010. Move surprised mobile customers as the provider was known for inferior but cheaper service. In 2013 Altyn Asyr launched a 4G network based on LTE. In 2013, unlimited use of the internet became available, reducing the total cost of services from Turkmentelecom.
Country code (Top level domain): TM



An Analog TV signal feed of 5 national channels is receivable over-the-air in all living areas across the country. Foreign TV channels are watched with digital satellite receiver. In some places of Ashgabat, cable service is available where satellite dishes are not allowed to be installed.
List of broadcast stations:
Altyn Asyr (Golden Age)
 a lyk (Youth)
Miras (Inheritance)
TV4 Turkmenistan - News channel broadcast in 7 languages.
T rkmen Owazy - Turkmen Music channel, launched in 2009.
A gabat
T rkmen sport - sport channel, launched in 2012.
All 7 of the national channels are aired on Yamal satellite for an international audience.



Telephones - main lines in use: 500,000 (2006)domestic: 500 automatic telephone stations and 500,000 telephone numbers.international: linked by cable and microwave radio relay to other Commonwealth of Independent States republics and to other countries by leased connections to the Moscow international gateway switch; a new telephone link from Ashgabat to Iran has been established; a new exchange in Ashgabat switches international traffic through Turkey via Intelsat; satellite earth stations - 1 Orbita and 1 Intelsat



List of newspapers in Turkmenistan:
Turkmenskaya Iskra (Soviet-era)
Balkan newspaper, Turkmenistan (Balkanabat)
"Neutral Turkmenistan"



Starting from 2007 the Ministry of Communication has organised the international exhibition "TurkmenTel" each year. Leading companies from all around the world are invited to Ashgabat to exhibit their technologies.



^ "Turkmenistan - Telecoms, Mobile and Internet". Paul Budde Communication Pty Ltd. Retrieved 28 November 2012. 
^ SpaceX
^ "SpaceX Will Launch Turkmenistan Satellite for Thales Alenia Space". 21 June 2013. Retrieved 23 September 2014. 
^ "China to Launch the first Turkmen Communication Satellite". The Gazette of Central Asia (Satrapia). 25 November 2012. 
^ US Says SpaceX to Launch Turkmenistan's Maiden Satellite
^ Turkmenistan to launch first space satellite in 2015
^ http://www.turkmenistan.ru/?page_id=3&lang_id=en&elem_id=6803&type=event&sort=date_desc
^ http://www.telecompaper.com/news/mts-turkmenistan-revenues-up-to-tmt-27-mln-in-q4--9321434/
^ http://www.turkmenistan.ru/en/articles/16762.html/
^ http://www.turkmenistan.ru/?page_id=3&lang_id=en&elem_id=12682&type=event&sort=date_desc
^ TMCELL starts to connect the subscribers to the LTE network
^      
^ Telecommunications At The World Level



Ministry of Communication of Turkmenistan
MTS Turkmenistan
TurkmenTel 2008 Official website
Software & Telecommunications services Ashgabat
TurkmenTel 2007