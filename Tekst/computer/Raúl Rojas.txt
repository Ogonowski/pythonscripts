Ra l Rojas Gonz lez (born 1955, in Mexico City) is a professor of Computer Science and Mathematics at the Free University of Berlin and a renowned specialist in artificial neural networks. The FU-Fighters, football-playing robots he helped build, were world champions in 2004 and 2005. He is now leading an autonomous car project called Spirit of Berlin.
He and his team were awarded the Wolfgang von Kempelen Prize for his work on Konrad Zuse and the history of computers. Although most of his current research and teaching revolves around artificial intelligence and its applications, he holds academic degrees in mathematics and economics.
In 2009 the Mexican government created the Ra l Rojas Gonz lez Prize for scientific achievement by Mexican citizens. The first recipient of the prize was Luis Rafael Herrera Estrella, for contributions to plant biotechnology.
He ran for president at the Free University of Berlin in 2010. 



Ra l Rojas was born on June 25th, 1955 in Mexico City to an engineer and a teacher. He attended university at the National Polytechnic Institute in Mexico City, where he majored in Mathematics and Physics. He moved to Germany in 1982 as a doctoral student in economics under the guidance of the political economist Elmar Altvater. The resulting dissertation was published under the title "Die Armut der Nationen - Handbuch zur Schuldenkrise von Argentinien bis Zaire" (The poverty of nations - Handbook of debt crisis from Argentina to Zaire). He became a full professor at University of Halle-Wittenberg in 1994, and later moved to the Free University of Berlin, where he remains today in the Informatics department. His wife, Margarita Esponda Arg ero, is a professor in the same department.



2001: Gr nderpreis Multimedia of the German Ministry of Finance and Technology
2002: European Academic Software Award
2004 and 2005: World champions in football robots with the FU-Fighters
2005: Wolfgang von Kempelen Prize for the History of Informatics
2009: Received the Heberto Castillo gold medal for contributions to science from the Mexico City government



Neural Networks - A Systematic Introduction Springer, Berlin, 1996. Available as a free e-book
Die Rechenmaschinen von Konrad Zuse, Springer, 1998.
The First Computers, MIT Press, Cambridge, 2000.
Encyclopedia of Computers and Computer History, Fitzroy-Dearborn, Chicago, 2001.
RoboCup 2002: Robot Soccer World Cup VI (Lecture Notes in Computer Science), 2002.






Homepage of Ra l Rojas at the Free University of Berlin
Curriculum vitae of Ra l Rojas
FU-Fighters football robots
Autonomous car project