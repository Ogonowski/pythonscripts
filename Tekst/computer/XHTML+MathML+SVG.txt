XHTML+MathML+SVG is a W3C standard that describes an integration of MathML and SVG semantics with XHTML and CSS. It is currently categorized as "obsolete" on the W3C's HTML Current Status page.



W3C Working Draft