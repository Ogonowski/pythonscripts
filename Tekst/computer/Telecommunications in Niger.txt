Telecommunications in Niger include radio, television, fixed and mobile telephones, and the Internet.



Radio stations:
state-run TV station; 3 private TV stations provide a mix of local and foreign programming (2007);
5 AM, 6 FM, and 4 shortwave stations (2001).
Radios:
680,000 (1997);
500,000 (1992).
Television stations: state-run TV station; 3 private TV stations provide a mix of local and foreign programming (2007).
Television sets:
125,000 (1997);
  37,000 (1992).
Because literacy levels in the country are low, radio is a key source for news and information.
Radio France Internationale (RFI) is available in the capital, Niamey, and in the Maradi and Zinder regions. The BBC World Service broadcasts in the capital (100.4 FM).



The state controls much of the nation's broadcasting, though private radio stations have proliferated. The media regulatory body, the National Observatory on Communication, and the Independent Nigerien Media Observatory for Ethics, a voluntary media watchdog organization, help to maintain the media environment in Niger. The government maintains a 200 million CFA (~$400,000 USD) press support fund, established by law and available to all media, to encourage support for education, information, entertainment, and promoting democracy.
Press freedom "improved considerably" after Mamadou Tandja was ousted as president in 2010. Media offences were decriminalised shortly afterwards. With the passage of the 2010 law protecting journalists from prosecution related to their work and President Issoufou s November 2011 endorsement of the Declaration of Table Mountain statement on press freedom in Africa (the first head of state to sign the statement), the country continues its efforts to improve press freedom. The Declaration of Table Mountain calls for the repeal of criminal defamation and "insult" laws and for moving press freedom higher on the African agenda.




Calling code: +227
International call prefix: 00
Main lines:
100,500 lines in use, 145th in the world (2012);
  24,000 lines in use, 186th in the world (2005).
Mobile cellular:
5.4 million lines, 107th in the world (2012);
900,000 lines, 139th in the world (2007).
Telephone system: inadequate; small system of wire, radio telephone communications, and microwave radio relay links concentrated in the southwestern area of Niger; domestic satellite system with 3 earth stations and 1 planned; combined fixed-line and mobile-cellular teledensity remains only about 30 per 100 persons despite a rapidly increasing cellular subscribership base (2010); United Nations estimates placed telephone subscribers at 0.2 per hundred in 2000, rising to 2.5 per hundred in 2006.
Satellite earth stations: 2 Intelsat (1 Atlantic Ocean and 1 Indian Ocean) (2010).
Communications cables: Africa Coast to Europe (ACE) via land links between Niger and the Atlantic coast.



Top-level domain: .ne, controlled by the parastatal telecom company, SONITEL.
Internet users:
230,084 users, 150th in the world; 1.4% of the population, 205th in the world (2012).
115,900 users, 155th in the world (2009);
  40,000 users, 173rd in the world (2006).
Fixed broadband: 3,596 subscriptions, 166th in the world; less than 0.05% of the population, 185th in the world (2012).
Wireless broadband: Unknown (2012).
Internet hosts:
454 hosts, 185th in the world (2012);
216 hosts, 176th in the world (2008).
IPv4: 20,480 addresses allocated, less than 0.05% of the world total, 1.2 addresses per 1000 people (2012).
The United Nations estimated that there were only 0.3 Internet users per 100 Nigeriens in 2006, up from less than 0.1 per 100 in 2000. As a point of reference, the Millennium Development Goal for least developed countries by 2015 is 8.2 Internet users per 100 population.



There are no government restrictions on access to the Internet or reports that the government monitors e-mail or Internet chat rooms. Although individuals and groups can engage in the peaceful expression of views via the Internet, few residents have access to it.
The constitution and law provide for freedom of speech and press, and the government generally respects these rights in practice. The constitution and law generally prohibit arbitrary interference with privacy, family, home, or correspondence, and the government generally respects these prohibitions.



Soci t  Nig rienne des T l communications (SONITEL), state owned telecommunications company.
Office of Radio and Television of Niger, state broadcast authority.
Media of Niger
Economy of Niger
List of terrestrial fibre optic cable projects in Africa



 This article incorporates public domain material from websites or documents of the CIA World Factbook.
 This article incorporates public domain material from websites or documents of the United States Department of State.



Transports et t l coms (Statistical series) (French), Institut National de la Statistique, Niger.
Does Digital Divide or Provide? The Impact of Cell Phones on Grain Markets in Niger, Jenny C. Aker, Center for Global Development, Tufts University, 1 October 2008.
Le paysage m diatique nig rien (The Nigerian media landscape) (French), Djilali Benamrane, Afrik.com, 6 March 2002. English translation.
Niger Information and Communications Technology Assessment, Yaovi Atohoun, Eileen Reynolds, Karl Stanzick, United States Education for Development and Democracy Initiative, 3 May 2001.