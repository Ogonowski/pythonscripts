The Harvard Mark II was an electromechanical computer built at under the direction of Howard Aiken and was finished in 1947. It was financed by the United States Navy.
The Mark II was constructed with high-speed electromagnetic relays instead of electro-mechanical counters used in the Mark I, making it much faster than its predecessor. Its addition time was 0.125 seconds (8 Hz) and the multiplication time was 0.750 seconds. This was a factor of 2.6 faster for addition and a factor 8 faster for multiplication compared to the Mark I. It was the second machine (after the Bell Labs Relay Calculator) to have floating-point hardware. A unique feature of the Mark II is that it had built-in hardware for several functions such as the reciprocal, square root, logarithm, exponential, and some trigonometric functions. These took between five and twelve seconds to execute.
The Mark II was not a stored-program computer   it read an instruction of the program one at a time from a tape and executed it (like the Mark I). This separation of data and instructions is known as the Harvard architecture. The Mark II had a peculiar programming method that was devised to ensure that the contents of a register were available when needed. The tape containing the program could encode only eight instructions, so what a particular instruction code meant depended on when it was executed. Each second was divided up into several periods, and a coded instruction could mean different things in different periods. An addition could be started in any of eight periods in the second, a multiplication could be started in any of four periods of the second, and a transfer of data could be started in any of twelve periods of the second. Although this system worked, it made the programming complicated, and it reduced the efficiency of the machine somewhat.
The Mark II ran some realistic test programs in July 1947. It was delivered to the US Navy Proving Ground at Dahlgren, Virginia in 1947 or 1948.



Harvard Mark I
Harvard Mark III
Harvard Mark IV
Howard Aiken



^ Williams, 1985, pages 248-251
Michael R. Williams (1997). A History of Computing Technology. IEEE Computer Society Press. ISBN 0-8186-7739-2. 
Staff of the Computation Laboratory (1949). Description of a Relay Calculator. Annals of the Computation Laboratory of Harvard University, Vol. XXIV. Harvard University Press, Cambridge, Mass.