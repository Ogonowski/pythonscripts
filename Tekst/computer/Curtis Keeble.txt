Sir Herbert Ben Curtis Keeble GCMG (18 September 1922   6 December 2008) was a British diplomat and Ambassador to the Soviet Union between 1978 and 1982.



He was born in Chingford in 1922 and attended Clacton County High School and Queen Mary, University of London, where he studied Modern Languages.



His studies were interrupted by World War II, and he volunteered in 1941, and was commissioned as a second lieutenant in the Royal Irish Fusiliers on 20 February 1943. He worked as an interpreter on various convoys, including one marked H9 carrying 2000 Russian refugees to Odessa despite the fact that, at the time, he did not speak fluent Russian. It was later revealed that he should have been sent on a course to learn the language beforehand but never was due to a bureaucratic error, and as a result the men were "quite unmanageable" until the convoy reached Naples and a Red Army officer boarded.



Experiences with convoys during the war inspired Keeble to apply to join Her Majesty's Diplomatic Service, and he was accepted in 1947, and posted to Jakarta as acting vice-consul despite the normal requirement for applicants to have a university degree. His main task while serving there was to decipher coded messages arriving from the British ambassador at The Hague, something the staff did with only pencils and cipher books. In 1949 he returned to London, and on 2 March 1949 he formally relinquished his army commission, receiving the honorary rank of captain. In 1951 he was sent to the Berlin British Sector as deputy political advisor. In 1954 he was transferred yet again, this time to Washington to serve in the commercial department and again in 1958 back to London. In 1961 he was appointed head of the department negotiating the entry of Britain into the European Economic Community, something that failed due to clashes with the French. He was appointed Companion of the Order of St Michael and St George (CMG) in the 1970 New Year Honours, at the time he was serving at the British High Commission in Australia.
In 1971 he returned to London again as assistant under-secretary and handled negotiations with Iceland during the second Cod War. On 16 January 1974 he was posted again to East Germany as Ambassador, where despite requesting a subdued colour for his official car he was given one that was bright red. According to him this later proved key, however; when Leonid Brezhnev was introduced to the British diplomatic staff he expressed amazement at Keeble's height (5 ft 9in) telling him it did not do justice to his country, but "the situation was rapidly restored by the red Daimler".
He was posted again in 1976 back to London, where he served as Chief Clerk at the Foreign and Commonwealth Office. On 28 March 1978 he was posted to Moscow as the British Ambassador to Russia, where he would form close ties with his opposite number, Andrei Gromyko, and he was promoted to Knight Commander of the Order of St Michael and St George (KCMG) in the Queen's Birthday Honours. He retired in 1982, at which point he was given a tour of a private part of the Kremlin; a mark of the regard the Russians held him in. He was also promoted Knight Grand Cross of the Order of St Michael and St George (GCMG) in the Birthday Honours.



After retirement he continued to be involved in foreign affairs, serving as an adviser to the House of Commons Foreign Affairs Committee and from 1985 as chairman of the Great Britain-USSR Association. In 1985 he also became a governor of the BBC, and from 1992 he served as chairman of the Foundation for Accountancy and Financial Management. He was a director of one of Gerald Carroll's Carroll Group companies.



While serving in World War II, he met his future wife Margaret Fraser, with whom he had three daughters: Suzanne, a doctor; Sally Keeble, a Member of Parliament for Northampton North constituency from 1997-2010; and Jane Keeble, who was killed on 12 July 1998 in a car accident. Lady Keeble died in 2014 aged 92.


