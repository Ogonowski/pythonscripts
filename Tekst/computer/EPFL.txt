The  cole polytechnique f d rale de Lausanne (EPFL, English: Swiss Federal Institute of Technology in Lausanne) is a research university in Lausanne, Switzerland, that specialises in physical sciences and engineering.
EPFL is regarded as one of the world leading universities, ranking 14th overall and 17th in engineering in the 2015 QS World University Rankings; 31th overall and 12th in engineering in the 2015 Times Higher Education World University Rankings.
EPFL is located in the French-speaking part of Switzerland; the sister institution in the German-speaking part of Switzerland is the Swiss Federal Institute of Technology in Zurich (ETH Zurich). Associated with several specialised research institutes, the two universities form the Swiss Federal Institutes of Technology Domain (ETH Domain), which is directly dependent on the Federal Department of Economic Affairs, Education and Research. In connection with research and teaching activities, EPFL operates a nuclear reactor CROCUS, a Tokamak Fusion reactor, a Blue Gene/Q Supercomputer and P3 bio-hazard facilities.



The roots of modern-day EPFL can be traced back to the foundation of a private school under the name  cole sp ciale de Lausanne in 1853 at the initiative of Lois Rivier, a graduate of  cole Centrale Paris and John Gay, the then professor and rector of the Acad mie de Lausanne. At its inception it had only eleven students and the offices was located at Rue du Valentin in Lausanne. In 1869, it became the technical department of the public Acad mie de Lausanne. When the Acad mie was reorganised and acquired the status of a university in 1890, the technical faculty changed its name to  cole d'ing nieurs de l'Universit  de Lausanne. In 1946, it was renamed the  cole polytechnique de l'Universit  de Lausanne (EPUL). In 1969, the EPUL was separated from the rest of the University of Lausanne and became a federal institute under its current name. EPFL, like ETH Zurich, is thus directly controlled by the Swiss federal government. In contrast, all other universities in Switzerland are controlled by their respective cantonal governments. Following the nomination of Patrick Aebischer as president in 2000, EPFL has started to develop into the field of life sciences. It absorbed the Swiss Institute for Experimental Cancer Research (ISREC) in 2008.
In 1946, there were 360 students. In 1969, EPFL had 1,400 students and 55 professors. In the past two decades the university has grown rapidly and as of 2012 roughly 14,000 people study or work on campus, about 9,300 of these beings Bachelor, Master or PhD students. As EPFL first became a federal institute under its current name in 1969, with a student body of then less than 1500, the university is included in the Times Higher Education list of top 100 universities under 50 years old. The environment at modern day EPFL is highly international with the school now attracting top students and researchers from all over the world. More than 125 countries are represented on the campus and the university has two official languages, French and English.



Like every public university in Switzerland, EPFL is obliged to grant admission to every Swiss resident who took the maturit  high-school certificate recognized by the Swiss Federation. As such, for Swiss students, EPFL is not selective in its undergraduate admission procedures.
The real selection process happens during the first year of study. This period is called the propaedeutic cycle and the students must pass a block examination of all the courses taken during the first year at the end of the cycle. If the weighted average is insufficient, a student is required to retake the entire first year of coursework if they wish to continue their studies at the school. Roughly 50% of students fail the first year of study, and many of them choose to drop out rather than repeat the first year. The failure rate for the propaedeutic cycle differs between fields of study, it is highest for Mathematics, Physics and Electrical Engineering majors where only 30-40% of students pass the first year.
For foreign students, the selection procedure towards the undergraduate program is rather strict, and since most undergraduate courses are taught in French, foreign students must provide documentation of having acquired a level B2 proficiency as measured on the CEF scale, though C1 proficiency is recommended.
As at all universities in Switzerland, the academic year is divided into two semesters. Regular time to reach graduation is six semesters for the Bachelor of Science degree and four additional semesters for the Master of Science degree. Though only 58% of the student's who manage to graduate are able to graduate within this time-period. The possibility to study abroad for one or two semesters is offered during the 3rd year of study under certain conditions as EPFL maintains several long-standing student exchange programs, such as the junior year engineering and science program with Carnegie Mellon University in the United States, as well as a graduate Aeronautics and Aerospace program with the ISAE in France. The final semester is dedicated to writing a thesis.
Entrepreneurship is actively encouraged to foster a start-up culture among the student body as evident by the EPFL-Innovationpark being an integral part of campus. Since 1997, 12 start-ups have been created per year on average by EPFL students and faculty. In the year 2013, a total of 105 million CHF was raised by EPFL start-ups.



The three most widely observed international university rankings, QS World University Rankings, Academic Ranking of World Universities and Times Higher Education World University Rankings ranks EPFL No. 2, No. 1 and No. 2 respectively in the field of Engineering and Technology on continental Europe in their 2015 2016 rankings. In the rankings EPFL competes with Cambridge, Oxford, Imperial College London and its sister institution, ETH Zurich, for the European top five spots in Engineering and Technology.
QS World University Ranking 2015 ranks EPFL world No. 14, reaching world No. 17 in Engineering and world No. 13 in the Natural Sciences subcategories. Academic Ranking of World Universities 2014 ranks EPFL world No. 14 and Europe No. 3 in the Engineering, Technology and Computer Sciences category, behind Cambridge and notably ahead of ETH Zurich and American universities such as Caltech and Princeton. THE World University Rankings 2014 2015 ranks EPFL world No. 31 and world No. 12 in the Engineering and Technology subcategory.
EPFL typically scores high on faculty to student ratio, international outlook and scientific impact. The specialised CWTS Leiden Ranking that "aims to provide highly accurate measurements of the scientific impact of universities" ranks EPFL world No. 13 and No. 1 in Europe in the 2013 rankings for all the sciences. Similarly, in the THE World University Rankings 2014-2015, EPFL ranks 15th in the world and 2nd in Europe on the citation index score.
The reputation of EPFL as a strong research institution has been further strengthened by a number of high-profile projects, the most notable of these being the Blue Brain Project that in 2013 secured a 0.5 Billion Euro Flagship Grant from the European Commission.
Although EPFL generally rank well on academic measures such as citation index and scientific impact, due to the young age of the school, it tends to rank comparatively low in name-brand surveys, a recent example being the Times 2015 reputation ranking where EPFL was ranked No. 48 in the world. In recent years, multiple EPFL faculty members has been selected as Young Global Leader or as Young Scientist by the World Economic Forum, increasing the visibility of EPFL outside tech-circles.




The  cole d'ing nieurs de l'Universit  de Lausanne, from which EPFL in its modern-day form originates, was located in the center of Lausanne. In 1974, 5 years after EPFL was separated from University of Lausanne and became a federal institute under its current name, the construction of a new campus at Dorigny in  cublens, a suburb south-west of Lausanne on the shores of Lake Geneva, began. The inauguration of the first EPFL buildings of the new campus took place in 1978.
The EPFL campus has been evolving ever since. The first stage of the development plans, with a total budget of 462 million Swiss francs, was completed in 1984 and in 1990 the second stage was completed. The construction of the northern parts of campus began in 1995 with the construction of the Microtechnology building, completed in 1998, and the architecture building, completed in 2000. In 2002, the department of architecture also moved to the campus in  cublens, uniting all departments of EPFL on the same site. The latest addition to the EPFL campus is the Rolex Learning Center completed February 2010. The Rolex Learning Center includes areas for work, leisure and services and is located at the center of the campus. The EPFL campus is now being expanded with the construction of the Swiss Tech Convention Center to be completed in 2014.
Together with the University of Lausanne, also located in  cublens, the EPFL forms a vast campus, welcoming about 20,000 students, at the shores of Lake Geneva. The campus is served by the Lausanne Metro Line 1 (M1) and is equipped with an electric bicycle sharing system. Since 2012, only electricity from certified hydroelectric generation is being bought by EPFL to power it's campus. EPFL was the first campus to receive the International Sustainable Campus Excellence Award by the International Sustainable Campus Network.
Of the 14,000 people that work and study at the Ecole Polytechnique F d rale de Lausanne campus, roughly 9.300 are students in either Bachelor, Master or Doctoral programs, the remaining 4,700 being administrative staff, scientists, technical staff, professors and the entrepreneurs located in the Science Park EPFL7. More than 125 nationalities are present on campus with 48% of the student population being foreign nationals.
Almost all of the structures of the EPFL are located on its main campus. However, it also has branches in Neuch tel ("Microcity"), in Sion ("P le EPFL Valais"), in Geneva (Campus Biotech, including the Wyss Center for Bio- and Neuro-engineering) and in Fribourg ("Smart Living Lab").
The EPFL also has a project of research centre in Ras al-Khaimah (United Arab Emirates), EPFL Middle East.



The campus consists of about 65 buildings on 136 acres (55 ha). Built according to the growth of the school, the campus includes different types of architectures:
Late 1970s 1980s: modularised building, used today by the Schools of Basic Sciences and Architecture, Civil and Environmental Engineering, Mechanical and Electrical Engineering
1990s: buildings with institutes from the Schools of Engineering Sciences and Techniques, Computer and Communication Sciences, and the Scientific Park (PSE)
Modern: new buildings (2002 2004) with Microengineering, Communications and Architecture institutes, the School of Life Sciences and the College of Management.
The Learning Center, a new library (2010)
2014: The Swiss Tech Convention Center and the "Quartier Nord" (convention center, student accommodation, shops...)
The EPFL and the University of Lausanne also share an active sports centre five minutes away from EPFL, on the shores of Lake Geneva.



Facilities are available on the campus for the students and staff:
Libraries: the Learning Center
Restaurants: Le Copernic and La Table de Vallotton
Cafeterias: La Coupole, Le Corbusier, Le Parmentier, Le Vinci, BMX (B timent des mat riaux), BC (B timent des communications), L'Arcadie, Le Hodler, Le Klee, L'Ornithorynque
Bar: Satellite
Travel agencies: Swiss Federal Railways and STA Travel
Banks: Credit Suisse and PostFinance
Radio: Fr quence Banane (student radio)
Conference centre: the Swiss Tech Convention Center
Museums: Mus e Bolo and Archizoom
Student housing: Quartier Nord (Atrium) and Les Estudiantines.




Microcity (in Neuch tel): "A temple dedicated to innovation" ( Patrick Aebischer)
EPFL Fribourg: "The EPFL Fribourg outpost develops its activities in the fields of technology, construction and sustainable architecture. Besides the activities of advanced research, it designs, with the School of Engineering and Architecture HEIA-FR and the University of Fribourg, the Smart Living Lab, a new interuniversity pole of competence. Its location at the heart of the blueFACTORY innovation park will allow it to exploit the development potential of public-private partnerships offered by this opportunity and will contribute to the future National Innovation Park." (EPFL website)
EPFL also has satellite facilities in Sion, Geneva, and Basel.






The number of students attending studying at EPFL has been rising heavily since EPFL was formed in 1969 under its current name. In 1969 EPFL had roughly 1400 students, that number had grown to 2367 by 1982, 4302 by 1997 and now 9921 students in 2014. Within the student body, 112 different nationalities are represented. In the period from 1982 to 2014 the female proportion of the student body has increased from 12% to 27%. The proportion of female students is lowest at the School of Computer Science and Communication, 15%, and highest at the School of Life Sciences, 49%.




The school encourages the formation of associations and sports activities on campus. As of 2012 more than 79 associations exist on campus for recreational and social purposes. In addition, the school has its own monthly newspaper, Flash. Included in the 79 associations are
AGEPoly is the Student's Association of the EPFL. The purpose of AGEPoly is to represent the EPFL's students, defend the general interests of the students and inform and consult its members on decisions of the EPFL Direction that concern them.
The Forum is an EPFL student association responsible for organisation of the Forum EPFL. The Forum was founded in 1982 as a platform for exchange and meeting between the academic and professional communities. Today, the Forum EPFL is one of the largest recrouting events in Europe.
UNIPOLY is the EPFL Association for Sustainable Development, the Association works to create awareness of sustainable developmenent on campus and in western Switzerland. UNIPOLY is part of the World Student Community for Sustainable Development, an international network of student organisations for sustainable development consisting of EPFL, ETH Zurich, Massachusetts Institute of Technology, University of Tokyo, University of Fort Hare, University of Nairobi, Chalmers, and University of Yaounde.



Several music festivals are held yearly at EPFL. The most important one is the Bal lec Festival, organised in May. The festival wellcomes 15,000 visitors to around 30 concerts. Other smaller festivals include Sysmic organised in April by the students of the Department of Microengineering, hosting two stages for local and national bands, and Artiphys, organised by the students of the Physics Department.



The EPFL was the birthplace of the Archimedean Oath, proposed by students in 1990. The Archimedean Oath has since spread to a number of European engineering schools.
The Archimedean Oath is an ethical code of practice for engineers and technicians, similar to the Hippocratic Oath used in the medical world:
"I commit to keeping completely, to the full extent of my capacities and judgment, the following promises:
I shall use my knowledge for the benefit of mankind. I shall not put my skill to the service of people who do not respect Human Rights I shall not permit consideration regarding religion, nationality, race, sex, wealth and politics to harm people affected by my actions. I shall bear the entire responsibility for my actions and shall in no way discharge them on another. I shall practice in respect for the environment. I shall not use my knowledge for destructive purposes. I shall practice my profession in complete intellectual honesty, with conscience and dignity. I solemnly take this oath, freely and on my honour."




EPFL is the official scientific advisor of Alinghi, twice winners of the America's cup 2003 and 2007.
Solar Impulse is a Swiss long-range solar powered aircraft project developed at EPFL, the project is currently achieving the first circumnavigation of the world using only solar power.
The Hydropt re, is an experimental sailing hydrofoil that in 2009 broke the world speed sailing record, sustaining a speed of 52.86 knots (97.90 km/h; 60.83 mph) for 500m in 30 knots of wind
EPFL contributed to the construction of SwissCube-1. It is the first satellite entirely built in Switzerland. It was put into orbit on 23 September 2009 by the Indian launcher PSLV.
To better understand the relationship between nutrition and the brain, EPFL and the Nestl  research center has signed a five-year agreement providing 5 million CHF each year for the creation of two new chairs at the EPFL Brain Mind Institute.
Logitech and EPFL has announced the creation of the EPFL Logitech Incubator that will provide financial, educational and operational support in entrepreneurship to researchers and students.
Breitling Orbiter became the first balloon to circumnavigate the earth non-stop in March 1999. The balloon was piloted by Bertrand Piccard and Brian Jones.
The Human Brain Project is the successor of the EPFL Blue Brain Project. The project is directed by EPFL and involves 86 institutions across Europe. The total cost is estimated at 1.190 billion euros.




EPFL is organised into seven schools, themselves formed of institutes that group research units (laboratories or chairs) around common themes. The seven schools at EPFL:
School of Basic Sciences (SB, Thomas Rizzo)
Mathematics Institute of Computational Science and Engineering (MATHICSE, Alfio Quarteroni)
Mathematics institute of Analysis and Applications (MATHAA, Anthony Davison)
Mathematics Institute of Geometry and Applications, (MATHGEOM, Eva Bayer-Fluckiger)
Institute of Chemical Sciences and Engineering (ISIC, Paul Dyson)
Institute of Physics of Energy and Particles (IPEP, Minh Quang Tran)
Institute of Condensed Matter Physics (IPMC, Wolf-Dieter Schneider)
Institute of Physics of Biological Systems (IPSB, Giovanni Dietler)
Institute of Physical Sciences (SPH-GE, Jean-Philippe Ansermet)
Institute of Quantum Electronics and Photonics (IPEQ, Beno t Deveaud-Pl dran)
Institute of Theoretical Physics (ITP, Alfonso Baldereschi)
Institute of Computational Condensed Matter Physics (IRRMA, Alfonso Baldereschi)
Interdisciplinary Center for Electron Microscopy (CIME, C cile H bert)
Center for Research in Plasma Physics (CRPP, Minh Quang Tran)
PRN Quantum Photonics (PRN-QP, Beno t Deveaud-Pl dran)
Bernoulli Center (CIB, Nicolas Monod)

School of Engineering (STI, Demetri Psaltis)
Institute of Electrical Engineering (IEL, Giovanni De Micheli)
Institute of Mechanical Engineering (IGM, Daniel Favrat)
Institute of Materials (IMX, Andreas Mortensen)
Institute of Microengineering(IMT, Nico de Rooij)
Institute of Bioengineering (IBI, Jeffrey Hubbell)

School of Architecture, Civil and Environmental Engineering (ENAC, Marilyne Andersen)
Institute of Architecture (IA, Luca Ortelli)
Civil Engineering Institute (IIC, Eugen Br hwiler)
Institute of Urban and Regional Sciences (INTER, Philippe Thalmann)
Environmental Engineering Institute (IIE, Andrea Rinaldo)

School of Computer and Communication Sciences (IC, James Larus)
Algorithms & Theoretical Computer Science
Artificial Intelligence & Machine Learning
Computational Biology
Computer Architecture & Integrated Systems
Data Management & Information Retrieval
Graphics & Vision
Human-Computer Interaction
Information & Communication Theory
Networking
Programming Languages & Formal Methods
Security & Cryptography
Signal & Image Processing
Systems

School of Life Sciences (SV, Gisou van der Goot)
Bachelor-Master Teaching Section in Life Sciences and Technologies (SSV)
Brain Mind Institute (BMI, Carmen Sandi)
Institute of Bioengineering (IBI, Melody Swartz)
Swiss Institute for Experimental Cancer Research (ISREC, Douglas Hanahan)
Global Health Institute (GHI, Stewart T. Cole)
Ten Technology Platforms & Core Facilities (PTECH)
Center for Phenogenomics (CPG)
NCCR Synaptic Bases of Mental Diseases (NCCR-SYNAPSY)

College of Management of Technology (CDM, Philippe Gillet)
Swiss Finance Institute at EPFL (CDM-SFI, Damir Filipovic)
Program of Management of Technology and Entrepreneurship (CDM-PMTE, Christopher Tucci)
Chair of Entrepreneurship and Technology Commercialization (CDM-ENTC, Marc Gruber)
Institute of Logistics, Economy and Management of Technology (ILEMT, Dominique Foray)
Management of Technology EPFL   UNIL (CMT, Francis-Luc Perret)

College of Humanities (CDH, Francesco Panese)
Human and social sciences teaching program (CDH-SHS, Eric Junod)

In addition to the seven schools there are seven closely related institutions
Swiss Cancer Centre
Center for Biomedical Imaging (CIBM)
Centre for Advanced Modelling Science (CADMOS)
 cole cantonale d'art de Lausanne (ECAL)
Campus Biotech
Wyss Center for Bio- and Neuro-engineering
Swiss National Supercomputing Centre







Guy Berruyer (chief executive of Sage Group)
Aart de Geus (Chairman, founder and CEO of Synopsys)
George de Mestral (Electrical engineer, inventor of Velcro)
Andr  Gorz (Austrian-French philosopher and economist)
Martin Vetterli (Scientist in the field of Digital Signal Processing, president of the Swiss National Science Foundation)
Daniel Borel (Co-founder of Logitech)
Franck Riboud (CEO of Danone)
Andr  Kudelski (CEO of Kudelski)
Jean-Daniel Nicoud (Swiss computer scientist)
Othman Benjelloun (Moroccan businessman)
Daniel Br laz (Swiss Mathematician, Politician and Environmentalist)
Stefan Kudelski (Industrialist, inventor of the Nagra)
Luc Recordon (Swiss politician)
Andr  Borschberg (Businessman and pilot, founder of the Solar Impulse project)



The EPFL has awarded a doctorate honoris causa to several people:















The EPFL has 179 partner universities around the globe.















Science and technology in Switzerland
Swiss Electromagnetics Research and Engineering Centre
Top Industrial Managers for Europe
Swiss Tech Convention Center
Green building on college campuses
Technologist, magazine published by Eurotech Universities



Official website
Official portrait
EPFL news's channel on YouTube, EPFL students's channel on YouTube
Associations at EPFL
EPFL virtual tour
EPFL innovation park
Bal lec Festival