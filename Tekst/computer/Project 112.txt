Project 112 was a biological and chemical weapon experimentation project conducted by the United States Department of Defense from 1962 to 1973. The project started under John F. Kennedy's administration, and was authorized by his Secretary of Defense Robert McNamara, as part of a total review of the US military. The name "Project 112" refers to this project's number in the 150 project review process authorized by McNamara. Funding and staff were contributed by every branch of the U.S. armed services and intelligence agencies a euphemism for the Office of Technical Services of the Central Intelligence Agency's Directorate of Science & Technology. Canada and the United Kingdom also participated in some Project 112 activities.
Project 112 primarily concerned the use of aerosols to disseminate biological and chemical agents that could produce "controlled temporary incapacitation" (CTI). The test program would be conducted on a large scale at "extracontinental test sites" in the Central and South Pacific and Alaska in conjunction with Britain, Canada and Australia. At least 50 trials were conducted; of these at least 18 tests involved simulants of biological agents (such as BG), and at least 14 involved chemical agents including sarin and VX, but also tear gas and other simulants. Test sites included Porton Down (UK), Ralston (Canada) and at least 13 US warships; the shipborne trials were collectively known as Shipboard Hazard and Defense SHAD. The project was coordinated from Deseret Test Center, Utah.
As of 2005 publicly available information on Project 112 remains incomplete.




In January 1961, McNamara sent a directive about chemical and biological weapons to the Joint Chiefs of Staff, urging them to: "consider all possible applications, including use as an alternative to nuclear weapons. Prepare a plan for the development of an adequate biological and chemical deterrent capability, to include cost estimates, and appraisal of domestic and international political consequences." The Joint Chiefs established a Joint Task Force that recommended and a five-year plan to be conducted in three phases.
On April 17, 1963, President Kennedy signed National Security Action Memorandum 235 (NSAM 235) which approved:

Policy guides governing the conduct of large-scale scientific or technological experiments that might have significant or protracted effects on the physical or biological environment. Experiments which by their nature could result in domestic or foreign allegations that they might have such effects will be included in this category even though the sponsoring agency feels confident that such allegations would in fact prove to be unfounded.

Project 112 was a highly classified military testing program which was aimed at both offensive and defensive human, animal, and plant reaction to biological and chemical warfare in various combinations of climate and terrain. The U.S. Army Chemical Corps sponsored the United States portion of an agreement between the U.S., Britain, Canada, and Australia to negotiate, host, conduct, or participate in mutual interest research and development activity and field testing.




The command structure for the Deseret Test Center, which was organized to oversee Project 112, somewhat bypassed standard Defense Department channels and reported directly to the Joint Chiefs of Staff and US Cabinet consisting of Secretary of Defense, Secretary of State, and to a much smaller extent, the Secretary of Agriculture . Experiments were planned and conducted by the Deseret Test Center and Deseret Chemical Depot at Fort Douglas, Utah. The tests were designed to test the effects of biological weapons and chemical weapons on personnel, plants, animals, insects, toxins, vehicles, ships and equipment. Project 112 and Project SHAD experiments involved unknowing test subjects who did not give informed consent, and took place on land and at sea in various climates and terrains. Experiments involved humans, plants, animals, insects, aircraft, ships, submarines and amphibious vehicles.




There was a large variety of goals for the proposed tests, for example:  selected protective devices in preventing penetration of a naval ship by a biological aerosol,  the impact of  meteorological conditions on weapon system performance over the open sea,  the penetrability of jungle vegetation by biological agents,  the penetration of an arctic inversion by a biological aerosol cloud,   the feasibility of an offshore release of Aedes aegypti mosquito as a vector for infectious diseases,   the feasibility of a biological attack against an island complex,  and the study of the decay rates of biowarfare agents under various conditions.
Project 112 tests used the following agents and simulants: Francisella tularensis, Serratia marcescens, Escherichia coli, Bacillus globii, staphylococcal enterotoxin Type B, Puccinia graminis var. tritici (stem rust of wheat). Agents and simulants were usually dispensed as aerosols using spraying devices or bomblets.
In May 1965, vulnerability tests in the U.S. using the anthrax simulant Bacillus globigii were performed in the Washington D.C. area by SOD covert agents. One test was conducted at the Greyhound bus terminal and the other at the north terminal of the National Airport. In these tests the bacteria were released from spray generators hidden in specially built briefcases. SOD also conducted a series of tests in the New York City Subway system between 7 and 10 June 1966 by dropping light bulbs filled with Bacillus subtilis var. niger. In the latter tests, results indicated that a city-level epidemic would have occurred. Local police and transit authorities were not informed of these tests.




Project SHAD, an acronym for Shipboard Hazard and Defense (or sometimes Decontamination), was part of a larger effort called Project 112, which was conducted during the 1960s. Project SHAD encompassed tests designed to identify U.S. warships' vulnerabilities to attacks with chemical or biological warfare agents and to develop procedures to respond to such attacks while maintaining a war-fighting capability. The Department of Defense (DoD) states that Project 112 was initiated out of concern for the ability of the United States to protect and defend against potential CB threats. Project 112 consisted of both land-based and sea-based tests. The sea-based tests, called Project SHAD were primarily launched from other ships such as the USS Granville S. Hall (YAG-40) and USS George Eastman (YAG-39), Army tugboats, submarines, or fighter aircraft and was designed to identify U.S. warships  vulnerabilities to attacks with chemical or biological warfare agents and to develop decontamination and other methods to counter such attacks while maintaining a war-fighting capability. The classified information related to SHAD was not completely cataloged or located in one facility. Furthermore, The Deseret Test Center was closed in the 1970s and the search for 40-year-old documents and records kept by different military services in different locations was a challenge to researchers. A fact sheet was developed for each test that was conducted and when a test cancellation was not documented, a cancellation analysis was developed outlining the logic used to presume that the test had been cancelled.



The existence of Project 112 (along with the related Project SHAD) was categorically denied by the military until May 2000, when a CBS Evening News investigative report produced dramatic revelations about the tests. This report caused the Department of Defense and the Department of Veterans Affairs to launch an extensive investigation of the experiments, and reveal to the affected personnel their exposure to toxins.
Revelations concerning Project SHAD were first exposed by independent producer and investigative journalist Eric Longabardi. Longabardi's 6-year investigation into the still secret program began in early 1994. It ultimately resulted in a series of investigative reports produced by him, which were broadcast on the CBS Evening News in May 2000. After the broadcast of these exclusive reports, the Pentagon and Veteran's Administration opened their own ongoing investigations into the long classified program. In 2002, Congressional hearings on Project SHAD, in both the Senate and House, further shed media attention on the program. In 2002, a class action federal lawsuit was filed on behalf of the US sailors exposed in the testing. Additional actions, including a multi-year medical study was conducted by National Academy of Sciences/Institute of Medicine to assess the potential medical harm caused to the thousands of unwitting US Navy sailors, civilians, and others who were exposed in the secret testing. The results of that study were finally released in May 2007.
Because most of the participants that were involved with Project 112 and SHAD were unaware of any tests being done, no effort was made to ensure the informed consent of the military personnel. The US Department of Defense (DoD) conducted testing of agents in other countries that were considered too unethical to perform within the continental United States. Until 1998, the Department of Defense stated officially that Project SHAD did not exist. Because the DoD refused to acknowledge the program, surviving test subjects have been unable to obtain disability payments for health issues related to the project. US Representative Mike Thompson said of the program and the DoD's effort to conceal it, "They told me   they said, but don t worry about it, we only used simulants. And my first thought was, well, you ve lied to these guys for 40 years, you ve lied to me for a couple of years. It would be a real leap of faith for me to believe that now you re telling me the truth."
The Department of Veterans Affairs commenced a three-year study comparing known SHAD-affected veterans to veterans of similar ages who were not involved in any way with SHAD or Project 112. The study cost approximately 3 million US Dollars, and results are being compiled for future release. DoD has committed to providing the VA with the relevant information it needs to settle benefits claims as quickly and efficiently as possible and to evaluate and treat veterans who were involved in those tests. This required analyzing historical documents recording the planning and execution of Project 112/SHAD tests.
The released historical information about Project 112 from DoD consists of summary fact sheets rather than original documents or maintained federal information. As of 2003, 28 fact sheets have been released, focusing on the Deseret Test Center in Dugway, Utah, which was built entirely for Project 112/SHAD and was closed after the project was finished in 1973.
Original records are missing or incomplete. For example, a 91-meter aerosol test tower was sprayed by an F-4E with "aerosols" on Ursula Island in the Philippines and appears in released original Project SHAD documentation but without a fact sheet or further explanation or disclosure as to the nature of the test that was conducted or even what the test was called.




Corroborating suspicions of Project 112 activities on Okinawa include "An Organizational History of the 267th Chemical company", which was made available by the U.S. Army Heritage and Education Center to Yellow Medicine County, Minnesota, Veteran's Service Officer Michelle Gatz in 2012. According to the document, the 267th Chemical Company was activated on Okinawa on December 1, 1962 as the 267th Chemical Platoon (SVC) was billeted at Chibana Depot. During this deployment, "Unit personnel were actively engaged in preparing RED HAT area, site 2 for the receipt and storage of first increment items, [shipment] "YBA", DOD Project 112." The company received further shipments, code named YBB and YBF, which according to declassified documents also included sarin, VX, and mustard gas.
The late author Sheldon H. Harris in his book "Factories of Death: Japanese Biological Warfare, 1932 1945, and the American cover up" wrote about Project 112:

The test program, which began in fall 1962 and which was funded at least through fiscal year 1963, was considered by the Chemical Corps to be  an ambitious one.  The tests were designed to cover  not only trials at sea, but Arctic and tropical environmental tests as well.  The tests, presumably, were conducted at what research officers designated, but did not name,  satellite sites.  These sites were located both in the continental United States and in foreign countries. The tests conducted there were aimed at both human, animal and plant reaction to BW. It is known that tests were undertaken in Cairo, Egypt, Liberia, in South Korea, and in Japan s satellite province of Okinawa in 1961, or earlier. This was at least one year prior to the creation of Project 112. The Okinawa anti-crop research project may lend some insight to the larger projects 112 sponsored. BW experts in Okinawa and  at several sites in the Midwest and south: conducted in 1961  field tests  for wheat rust and rice blast disease. These tests met with  partial success  in the gathering of data, and led, therefore, to a significant increase in research dollars in fiscal year 1962 to conduct additional research in these areas. The money was devoted largely to developing  technical advice on the conduct of defoliation and anti-crop activities in Southeast Asia.  By the end of fiscal year 1962, the Chemical Corps had let or were negotiating contracts for over one thousand chemical defoliants. The Okinawa tests evidently were fruitful.

The U.S. government has previously disclosed information on chemical and biological warfare tests it held at sea and on land yet new-found documents show that the U.S. Army tested biological weapons in Okinawa in the early 1960s, when the prefecture was still under U.S. rule. During these tests, conducted at least a dozen times between 1961 and 1962, rice blast fungus was released by the Army using  a midget duster to release inoculum alongside fields in Okinawa and Taiwan,  in order to measure effective dosages requirements at different distances and the negative effects on crop production. Rice blast or Pyricularia oryzae produces a mycotoxin called Tenuazonic acid which has been implicated in human and animal disease.



A number of studies, reports and briefings have been done on chemical and biological warfare exposures. A list of the major documents is provided below.



Government Accountability Office (GAO) Report: GAO-04-410, DOD Needs to Continue to Collect and Provide Information on Tests and on Potentially Exposed Personnel, May 2004
Government Accountability Office (GAO) Report: GAO-08-366, DOD and VA Need to Improve Efforts to Identify and Notify Individuals Potentially Exposed during Chemical and Biological Tests, February 2008



Release from Secrecy Oaths Under Chem-Bio Research Programs, January 11, 2011



Institute of Medicine Study: SHAD II, study in progress, 2012
Institute of Medicine Study: Long-Term Health Effects of Participation in Project SHAD, 2007
Supplement to Institute of Medicine Study: Long-Term Health Effects of Participation in Project SHAD,  Health Effects of Perceived Exposure to Biochemical Warfare Agents, 2004 
Three-Part National Research Council Series Reports on Possible Long-Term Health Effects of Short-Term Exposure to Chemical Agents (1982-1985)



Senate Committee on Veterans  Affairs Hearing: Military Exposures: The Continuing Challenges of Care and Compensation, July 10, 2002
House Committee on Veterans  Affairs, Subcommittee on Health, Hearing: Military Operations Aspects of SHAD and Project 112, October 9, 2002
Senate Armed Services Committee, Subcommittee on Personnel, Prepared Statement of Dr. William Winkenwerder, Jr., Assistant Secretary of Defense for Health Affairs, on Shipboard Hazard and Defense, October 10, 2002



Military Service Organizations/Veterans Service Organizations Briefing: Chemical/Biological Exposure Databases, September 17, 2009
Military Service Organizations/Veterans Service Organizations Briefing: Chemical/Biological Exposure Databases, February 21, 2008
Extracts from 2003 Report to Congress Disclosure of Information on Project 112 to the Department of Veterans Affairs as Directed by PL 107-314, August 1, 2003   Executive Summary and Disclosure of Information



The following is a list of Department of Defense-issued press releases for Project 112 and Project SHAD:
June 30, 2003   SHAD   Project 112   Deseret Test Center Investigation Draws To A Close The Department of Defense completed today its nearly three-year investigation of operational tests conducted in the 1960s.
December 31, 2002   DoD corrects data on SHAD test "High Low" Since the Department of Defense began investigating the operational shipboard hazard and defense tests in September 2000, it has released fact sheets on 42 of the 46 shipboard and land-based tests.
October 31, 2002   DoD Releases Five Project 112 SHAD Fact Sheets The Department of Defense today released five new detailed fact sheets on Cold War-era chemical and biological warfare tests conducted in support of Project 112.
October 9, 2002   DoD Releases Deseret Test Center/Project 112/Project SHAD Fact Sheets The Department of Defense today released another 28 detailed fact sheets on 27 Cold War-era chemical and biological warfare tests identified as Project 112.
July 9, 2002   DoD expands SHAD investigationThe Department of Defense announced today an expansion of the Shipboard Hazard and Defense investigation. A team of investigators will travel to Dugway Proving Ground in mid-August to review Deseret Test Center records.
May 23, 2002   DoD releases Project SHAD fact sheets The Department of Defense today released detailed fact sheets on six Cold War-era chemical and biological warfare tests.
May 23, 2002   DoD releases six new Project SHAD fact sheets The Department of Defense released detailed fact sheets on six Cold War-era chemical and biological warfare tests.
January 4, 2002   DoD Releases Information on 1960 tests In the 1960s, the Department of Defense conducted a series of chemical and biological warfare vulnerability tests on naval ships known collectively as Project Shipboard Hazard and Defense.
January 4, 2002   No Small Feat The ongoing investigation into the Project Shipboard Hazard and Defense, or SHAD, tests is a detective story worthy of Sherlock Holmes.



Human experimentation in the United States
United States biological weapons program
Edgewood Arsenal human experiments
Operation Whitecoat
Operation LAC (Large Area Coverage)
Dorset Biological Warfare Experiments
HMS Icewhale
CFB Suffield#Chemical warfare training and Suffield Experimental Station
Porton Down




 This article incorporates public domain material from websites or documents of the United States Government.



Project SHAD at the United States Department of Veterans Affairs, includes pocket guides and Q&A
Force Protection and Readiness information page for SHAD (Project 112)
GAO