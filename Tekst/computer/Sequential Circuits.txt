Sequential Circuits Inc. (SCI) was a California-based synthesizer company that was founded in the early 1970s by Dave Smith, and sold to Yamaha Corporation in 1987. Throughout its lifespan, Sequential pioneered technologies and design principles that have served as a foundation for the development of modern music technology. Sequential was also pivotal in the planning, design, and support of 1982's groundbreaking innovation in electronic music, MIDI.
Following the purchase of Sequential Circuits by Yamaha, Dave Smith continued to develop musical instruments through Dave Smith Instruments. In January 2015, he reacquired the rights to use the Sequential brand name from Yamaha president Takuya Nakata.



Sequential's first products were sequencers and programmer devices for third-party synthesizers.

Sequential's first synthesizer, the brainchild of Dave Smith and John Bowen, was the very successful Prophet-5, released in 1978. This was the first affordable, fully programmable polyphonic analog synthesizer, which enjoyed considerable popularity in the early 1980s. By combining full microprocessor control with low-cost synthesizer module chips (initially by Solid State Music and later Curtis Electromusic), Sequential was able to produce a relatively low-cost synthesizer with five voices of polyphony. The then-revolutionary principle of combining five analog voices with easy editing and programming within a compact format established a new standard for polyphonic instruments. With the addition of patch storage (an innovation by Dave Smith with assistance from E-mu's Dave Rossum)  the user was able to cycle through up to one-hundred sounds at the push of a button. These were viewed as remarkable developments in the synthesizer industry, especially from a previously unknown company that operated out of a California garage. Dave Smith designed, programmed, and built the Prophet 5 in less than eight months. The Prophet-5 was in production from 1978 1984, and sold approximately 8,000 units.
Sequential followed up this successful debut with a ten-voice version of the Prophet 5, the Prophet-10, in 1980. The Prophet 10 was packaged in a massive dual-manual format, with a digital sequencer and the ability to play two different timbres simultaneously. When equipped with the optional sequencer, it sold for $9,000 USD. Fewer than 1,000 were produced.
In 1981, Sequential released a low-cost monophonic version of the Prophet 5, the Pro-One. The Pro-One became particularly popular, with sales of over 10,000 units.

Sequential was also instrumental in developing MIDI, and introduced the first MIDI synthesizer: the Prophet 600 in 1982. At the January, 1983 NAMM convention, this instrument successfully communicated with a Roland Jupiter-6 synthesizer in the first public demonstration of the MIDI protocol. At the Goodman Music Exposition in 1983, Hybrid Arts and Robert Moore demonstrated Atari's first personal computer, the Atari 800 (8 bit with 48k RAM), communicating with a MIDI equipped Prophet 600. This was made possible through the MidiMate hardware interface and MidiTrack program, developed by Moore and his partner, Paul Rother. The MidiMate holds the distinction as the first MIDI to personal computer interface, and physically connected the Prophet 600 to the Atari 800 computer. The virtual interface was achieved through the MidiTrack program, which included a 16 track MIDI recorder/sequencer, full graphic user interface (GUI), and a programmer/editor/librarian. Moore designed the graphics, and programming was accomplished using a light pen. The software application displayed the controls of the Prophet 600 on the Atari's CRT, and made it possible to store over 1,000 sound patches from the Prophet 600 onto a single floppy disk.

1983 also saw the release of the Prophet-T8, which featured a 76-note wooden keyboard (uneven A to C), and implemented a basic MIDI interface. It had an eight-voice architecture that was similar to that of the Prophet 5. The weighted, velocity and aftertouch-sensitive keyboard was so well received that New England Digital adopted it for use in their Synclavier workstations. Dave Smith himself keeps a T8 unit as the main controller keyboard in his home studio.

The Six-Trak was released in 1984. It was one of the first multitimbral synthesizers, and featured MIDI and an on-board six-track digital sequencer. It was designed as an inexpensive and easily portable 'scratch-pad' for writing arrangements. Its larger six voice brother, the MultiTrak would be released that following year.
Sequential released the six voice, six-way multitimbral Max in 1984, which was heavily dependent upon the accompaniment of an external computer for editing duties. Thus began the ill-fated leap into the new realm of computer-based editing and sequencing, which would prove to be a relatively unsuccessful experiment that set the stage for further financial troubles.
Sequential also released two drum machines during the mid-1980s, Drumtraks (1984) and TOM (1985).

In late 1985, the Prophet 2000 sampler was released, along with Sequential's last analogue synthesizer, the commercially unsuccessful Split-8.
The Prophet VS vector synthesizer, Sequential's only digital synthesizer, was released in early 1986. Boasting a scheme described as vector synthesis, it combined a digital waveform generator and vector joystick to the proven analog Curtis filters, which resulted in a unique instrument with a very distinct sound. The VS is still revered today despite its reliance on rare custom components with high failure rate.
Sequential Circuits' final release was the Sequential Circuits Studio 440. This expensive ($5000 USD) unit was constructed like a drum machine, and combined a sampler with a sequencer to make a music composition workstation. This preceded the release of the popular Akai MPC sequencers by several years, which were designed by Roger Linn, a good friend and frequent collaborator of Dave Smith.
In 1987, Sequential was finalizing development of a 16-bit sampler called the Prophet 3000, but went out of business and was acquired by Yamaha. Yamaha liquidated the few completed Prophet 3000 samplers at a substantial discount. Like most of the Sequential line, this sampler contained features that were far ahead of its time, such as automatic pitch detection, key-mapping, a remote control interface, and facilities for easily looping and trimming sampled sounds. Many of these technologies were later included in Yamaha's A-series samplers.



Support for Sequential Circuits instruments is now provided by Wine Country Productions, which is directed by ex-Sequential employee Dave Sesnak. After a short stint at Yamaha to develop a new synthesizer (the F8), several members of the Sequential team became part of the Korg R&D department. Dave Smith consulted with Korg during that time, and the powerful and memorable Korg Wavestation synthesizer (with the UI based on the F8 design) was born. This synthesizer borrowed design elements from the Prophet VS, but broke new ground in making full use of rapidly evolving digital technology. (Contrary to popular rumor, the Sequential team members never worked on Yamaha's SY22/TG33 products.)

Sequential's legacy products are popular targets for simulation or emulation in software synthesizers, with developers such as Native Instruments and SonicCore offering virtual instruments inspired by Sequential designs. Various analog modeling synthesizers also include presets to emulate Sequential's signature sounds. For example, the Clavia Nord Lead includes a preset patch bank which contains faithful recreations of the Prophet 5's original factory sounds.
Today, Dave Smith operates Dave Smith Instruments, which manufactures, among other things, the Prophet '08 (an update of Sequential Circuits' Prophet line), and the Tempest analog drum machine.
On January 20, 2015, Smith announced the release of the Prophet-6, the first instrument to bear the Sequential name in decades. The instrument is described as a spiritual successor to the original Prophet-5, and sports a 100% analog architecture.



The Sequential Circuits logo makes use of the Stop font designed by Aldo Novarese, in addition to a faux-Celtic font used for the model name.




Model 600 (1974)     analog sequencer
Model 800 (1975)     digital sequencer
Model 700 (1977)     programmer
Prophet-5 (1978)     5-voice analog synthesizer with patch memory
Fugue (1979)     ensemble keyboard, designed by SCI and built by Siel

Prophet-10 (1980)     essentially two Prophet-5s in one large enclosure
Pro-One (1980)     basically a monophonic version of the Prophet 5
Pro-FX (1982)     the first programmable modular effects units
Prelude (1982)     4 section orchestral synthesizer, manufactured by Siel; a silk screen variation of Orchestra 2 and OR400
Prophet 600 (1982)     the world's first MIDI synthesizer
Prophet Remote (1982)     remote keyboard for Prophet-5; the interface was Serial Control Buss (SCB).
Prophet T8 (1983)    The first "piano action" MIDI synthesizer
Model 64 Sequencer (1983)    software MIDI sequencer with MIDIinterface, in the form of Commodore 64 cartridge
Six-trak (1984)    the first multi-timbral synthesizer
Drumtraks (1984)     programmable drum machine with MIDI

MAX (1984)     6 voice synthesizer controlled by external PC
MultiTrak (1985)     Sequential's second multi-timbral synthesizer
Split-8 / Pro-8 (1985)     8-voice synthesizer
TOM (1985)     advanced programmable drum machine
Prophet 2000 (1985)     12bit sampler
Prophet VS (1986)     the first vector synthesizer
Studio 440 (1986)     music production machine with sequencing/sampling drum
Prophet 3000 (1987)     16bit sampler, the final Sequential Circuits product
Prophet-6 (2015)     fully analogue synthesizer, loosely based on the Prophet 5


