The House of Kyburg was family of Grafen or counts from Z rich in Switzerland. The family was one of the three most powerful noble families in the Swiss plateau beside the Habsburg and the House of Savoy during the 11th and 12th Centuries. With the extinction of the male line in 1263, Rudolph of Habsburg laid claim to the Kyburg lands and annexed them to the Habsburg holdings, which marked the beginning of the Habsburg rise to power.







The first line of counts of Kyburg were influential in local politics during the 1020s but the male line died out in 1078. Kyburg castle, southeast of Winterthur (in the modern canton of Z rich), passed on to the Swabian counts of Dillingen. Through the marriage of Hartmann von Dillingen (  1121) with a certain Adelheid the House of Dillingen acquired the old Kyburg possessions as well as territorial claims in the Thurgau. The exact origin of Adelheid is unclear. She is either the granddaughter of the Count of Gr ningen-Winterthur or from a cadet branch of the Winterthur family, the Counts of Nellenburg. She might also be the daughter of Adalbert II von Winterthur, the last knight from Winterthur, who died in 1053 at the Battle of Civitate against the Normans.



The Kyburg land continued to be part of the possessions of the House of Dillingen until the grandson of Hartmann von Dillingen, Hartmann III (  1180), split the Dillingen lands. Adalbert received the Swabian territories, while Hartmann III von Dillingen got the Swiss lands and became Hartmann I of Kyburg. The House of Kyburg were vassals of the Duke of Swabia, who was of the House of Hohenstaufen and would become the Kings of Germany from 1138-1254. When the House of Lenzburg died out in 1172/73, the Kyburgs together with the Hohenstaufen and Z hringen split the Lenzburg possessions between them. The Kyburg family acquired the allodial title to the Vogtei of Windegg or Gaster (today 7 municipalities in the See-Gaster Wahlkreis of St. Gallen) and land around Baden. Later additional Lenzburg territories, the Sch nis Abbey and Berom nster, were also acquired by the House of Kyburg.
In 1180 the family began to consolidate their power. They founded the cities of Diessenhofen and Winterthur to help spread their power. They also appointed many of the Lenzburg, and later Z hringen, vassals to be unfree knights or Ministerialis for the Kyburg family.
When the Z hringen family died out in 1218, the Kyburgs grabbed another chance to expand. Anna von Z hringen, the sister of the last Duke of Z hringen, Berthold V, was the wife of Ulrich III von Kyburg ( 1227). From the Z hringen line the Kyburgs acquired land west of the Rhine and in Burgundy including the cities of Fribourg, Thun and Burgdorf as well as estates in the canton of Zurich. However, the House of Hohenstaufen, the family of the Holy Roman Emperors, refused to support the Kyburg claims on the city of Zurich and in 1226 on the Abbey of St. Gall. As a result, they turned increasingly away from the Hohenstaufens and in 1243 and were one of the mainstays of the pro-Pope and anti-Holy Roman Emperor Party.
Around 1220 they started to make claims on property and rights that had unclear ownership and was near property that they already owned. In 1225 they founded a burial site for the Chorherrenstift Heiligberg in the center of the property of the former Freiherr von Weisslingen at Winterthur, and in 1233 founded T ss Abbey west of Winterthur. Both sites were endowed with property that they had taken from the Weisslingen and Rossberg families. These two properties served to define the borders between the Kyburg and Rapperswil families.
At the same time the Kyburg family attempted to strengthen themselves through marriage. Hartmann V, a grandson of Ulrich III was engaged to Anna of Rapperswil in 1227. His uncle, Hartmann IV, called the Elder, married Margaret of Savoy while his sister Heilwig, the future mother of King Rudolf I von Habsburg, married Albert IV of Habsburg
Even though the family continued to found cities and expand, they were declining in power. In 1230 they founded Zug and Baden, then Frauenfeld, Aarau, Lenzburg, and Mellingen. In 1250 they founded Sursee, Weesen and the fortified towns of Kyburg and Laupen. The last two were Richensee and Huttwil which they lost shortly thereafter.




In 1250/51 the childless Hartmann IV gave the western part of the property with the center of Burgdorf to his nephew Hartmann V. As a result, Hartmann V, who was supported by the Habsburgs, came into conflict repeatedly with the growing city-state of Bern. His uncle had to step in often to keep the peace. When Hartmann V died in 1263, Count Rudolf von Habsburg became the guardian of Hartmann's daughter Anna, and also took over the administration of the western section. One year later, after the death of Hartman IV, Rudolf stepped in to control the eastern half as well. Though this brought him into conflict with the claims by the widow Margaret of Savoy and her family.
Anna, daughter of Hartmann V, married Eberhard I of Habsburg-Laufenburg. This marriage was intended to secure Habsburg interests in Aargau (Argovia) against Savoy. The son of Eberhard and Anna, Hartmann I (1275 1301) again called himself "of Kyburg". His line came to be known as that of Neu-Kyburg or Kyburg-Burgdorf, persisting until 1417.
In 1322, the brothers Eberhard II and Hartmann II started fighting with each other over who would inherit the undivided lands. The fighting led to the "fratricide at Thun Castle" where Eberhard killed his brother Hartmann. To avoid punishment by his Habsburg overlords, Eberhard fled to Bern. In the following year, he sold the town of Thun, its castle and the land surrounding Thun to Bern. Bern granted the land back to Eberhard as a fief.
The decline of Neu-Kyburg began with a failed raid by Rudolf II on Solothurn, on 11 November 1382. The ensuing conflict with the Old Swiss Confederacy is known as the Burgdorferkrieg (also Kyburgerkrieg). Bern took the opportunity to assert its interests in Aargau against the Habsburgs, and after the Bernese laid siege to Burgdorf, Neu-Kyburg was forced to concede an unfavourable peace. Bern bought Thun and Burgdorf, the most important cities of Neu-Kyburg, and their remaining towns passed to Bern and Solothurn by 1408. The last of the Neu-Kyburgs, Berchtold, died destitute in Bern in 1417.



sources:



sources:


