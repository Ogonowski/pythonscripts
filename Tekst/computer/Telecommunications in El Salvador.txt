Telecommunications in El Salvador include radio, television, fixed and mobile telephones, and the Internet, centered primarily around the capital, San Salvador.




Radio stations: Hundreds of commercial radio broadcast stations and 1 government-owned radio broadcast station (2007).
Radios: 5.75 million (1997).
Televisions stations: Multiple privately owned national terrestrial TV networks, supplemented by cable TV networks that carry international channels (2007).
Television sets: 5,900,881 (2005).
The Telecorporaci n Salvadore a (TCS) consists of four television stations, channels 2, 4, 6, 35. Other small chains of television networks operate in the west and east sides of the country.
The law permits the executive branch to use the emergency broadcasting service to take over all broadcast and cable networks temporarily to televise political programming. The president occasionally uses this law to highlight his accomplishments.




Calling code: +503
International call prefix: 00
Fixed lines: 1.1 million lines in use, 74th in the world (2012).
Mobile cellular: 8.7 million lines, 88th in the world (2012); in 2007 the number of mobile phones exceeded the country's population giving the country a 1.15 per capita cellphone penetration rate.
Teledensity: Mobile cellular exceeds 135 per 100 persons (2011).
Telephone system: multiple mobile-cellular providers are expanding services rapidly; growth in fixed-line services has slowed in the face of mobile-cellular competition (2011).
Satellite earth stations: 1 Intelsat (Atlantic Ocean) (2011).
Connected to the Central American Microwave System (2011), a trunk microwave radio relay system that links the countries of Central America and Mexico with each other.



Top-level domain: .sv
Internet users: 1.6 million users, 104th in the world; 25.5% of the population, 136th in the world (2012).
Fixed broadband: 235,403 subscriptions, 81st in the world; 3.9% of the population, 111th in the world (2012).
Wireless broadband: 335,716, 104th in the world; 5.5% of the population, 104th in the world (2012).
Internet hosts: 24,070 hosts (2012).
IPv4: 575,744 addresses allocated, less than 0.05% of the world total, 94.5 addresses per 1000 people (2012).
Internet Service Providers: 11 ISPs (early 2005).



There are no government restrictions on access to the Internet or credible reports that the government monitors e-mail or Internet chat rooms without judicial oversight. Individuals and groups engage in the expression of views via the Internet, including by e-mail. Internet access is available in public places throughout the country.
The constitution provides for freedom of speech and press, and the government generally respects these rights. Individuals criticize the government publicly or privately without reprisal, and in most cases the government does not interfere with such criticism. In March 2012, Carlos Dada, the owner of online newspaper El Faro, received death threats from gang members. The gangs were unhappy with El Faro s reporting on the gang truce. On April 13, the International Press Institute criticized the government for not taking any actions to guarantee the safety of El Faro journalists. According to the Salvadoran Association of Journalists (APES), the media practices self-censorship, especially in their reporting on gangs and narcotics trafficking. APES stated that many members of the media were afraid to report in detail on these subjects due to fear of retaliation from gangs and narcotics trafficking groups.
The constitution prohibits arbitrary interference with privacy, family, home, or correspondence, and the government generally respects these prohibitions.



Economy of El Salvador



 This article incorporates public domain material from the CIA World Factbook document "2014 edition".
 This article incorporates public domain material from websites or documents of the United States Department of State.



Superintendency of Electricity and Telecommunications (SIGET) (Spanish).
SVNet, registrar for the .sv domain (Spanish).
Instructions on how to send SMS to El Salvador (Spanish).
GSM Cell Phone Networks in El Salvador