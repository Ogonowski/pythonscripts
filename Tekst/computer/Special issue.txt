W.I.T.C.H. is an Italian Fantasy, magical girl comic series written by Elisabetta Gnone, Alessandro Barbucci, and Barbara Canepa. The series was first published in Italy in April 2001 before the series was released in other countries. As of January 2005, W.I.T.C.H. has been released in over 65 countries. The final issue of W.I.T.C.H. was released on October 2012, concluding the series' 139 issue run.
The series tells the story of five teenage girls who are chosen to be the new Guardians of Kandrakar, protectors of the center of the universe from people and creatures who wish to cause harm to it. For this purpose, powers over the five elements have been given to them. The new guardians are Will, Irma, Taranee, Cornelia, and Hay Lin, whose initials form the title acronym "W.I.T.C.H.".



It is about a group of girls who find out they are the new guardians of the element earth; fire; water; air and energy.



The first saga introduced a world called Metamoor, ruled by a tyrannical ruler named Prince Phobos. Years ago, Prince Phobos caused the disappearances of his parents and took over the capital city, Meridian. To prevent his spreading tyranny, the Oracle lowered a veil over the planet, separating Metamoor from the rest of the worlds under Kandrakar s protection. However, twelve portals opened between Meridian and Earth, creating a series of passageways for the desperate refugees and monsters of Meridian to infiltrate Earth. The portals also weaken the veil and in order to prevent this, generations of Guardians were sent to protect it from collapsing. In the present day, five girls are chosen to become the new Guardians: Will Vandom, the Guardian of Energy and the Keeper of the Heart, making her the leader; Irma Lair, the new Guardian of Water; Taranee Cook, the Guardian of Fire; Cornelia Hale, the Guardian of Earth; and Hay Lin, the Guardian of Air. Together, they must prevent the collapse of the veil and promote justice throughout the universe under the guidance of the Oracle of Kandrakar.
The first saga also introduced Elyon Brown, a childhood friend of Cornelia. The Guardians  first mission was to close the twelve portals, but as they found out for themselves, Elyon had betrayed them. It is revealed that Elyon is Prince Phobos s younger sister who had disappeared from Meridian more than a decade ago. After Prince Phobos s right-hand man, Lord Cedric, revealed Elyon of her true alien heritage, he manipulated her into thinking that the Guardians were her enemies. The Guardians had to find a way to defeat Prince Phobos and bring Elyon back home. In Meridian, however, there is a rebellion taking place led by Caleb, whom Cornelia had seen in her dreams for years. They sought to overthrow Prince Phobos and put Elyon on the throne, as she is the legitimate ruler. The Guardians later worked together with the rebels and eventually, Elyon realized her mistake for trusting her cruel brother. Prince Phobos had planned to absorb his sister s powers for himself during her coronation, but with the Guardians  help Elyon escaped and later battled against him for the crown. The final battle was successful, but not without some losses. Prince Phobos transformed Caleb back into a crystal flower, his most primitive form as a Whisperer. This devastated Cornelia greatly to the point where it would haunt her in the next series. As Elyon is crowned Queen and the Light of Meridian, the world of Metamoor is finally liberated. The Oracle lifted the veil off of Metamoor and light shines on the world once again. Both Prince Phobos and Cedric are imprisoned in the Tower of Mist.



The Guardians must face an old enemy of Candracar   Nerissa, the corrupted ex-Guardian and the previous Keeper of the Heart. When the Oracle realized how the immense power of the Heart was corrupting Nerissa, he took it away from her and gave it to Cassidy, the former Guardian of Water. Obsessed with her lost power and blinded by jealousy, Nerissa tricked Cassidy and killed her. As punishment, Nerissa was stripped of her magic and sentenced to sleep,alone, in Mount Thanos for an eternity. When Cornelia accidentally absorbed all five elements into herself, the seal on Nerissa s tomb broke, setting her free. When Caleb is kidnapped, the Guardians must face Nerissa along with her four Knights of Vengeance. While successful in their rescue mission, they lost Luba who sacrificed herself to save them. Hoping to steal the Heart, Nerissa later attacked the girls through their dreams in order to weaken them (but failed). She was later successful in stealing the Heart back by tricking Will into giving it to her. With the Heart now corrupted by Nerissa s hatred, the former Guardian and her Knights of Vengeance attacked Candracar. Will later acquired the Star of Cassidy, a copy of the Heart from Cassidy s ghost. With it Will and her friends faced Nerissa in Candracar. In the final battle, Nerissa was killed by the Guardians. Despite their victory, Cornelia is heartbroken when Caleb left her due to their complicated romantic relationship.



Crisis after crisis threatens to break up the girls once and for all. Will's father, Thomas Vandom, returns into her life after years of absence, and his intentions are anything but loving and trustful. Later, Taranee went on strike because she felt that the Oracle is using them to carry out his missions and sentences. A new character, Orube, joins the remaining Guardians on their new mission to defeat a new enemy; Ari, the powerful lord of Arkhanta, and they must prevent him from rising up and destroying the Oracle. Ari's son, Maqi, suffered from a strange disorder since birth and the Oracle had refused to help Ari cure his son. Angered and blinded by rage, Ari captured a banshee, named Yua, and tried to use her wish-granting power to oppose the Oracle and Kandrakar (but it could not be done). The longer Yua was held prisoner and serving as Ari's slave, the more powerful Ari becomes, and soon, he would have enough power to overthrow the Oracle. Taranee later rejoined the group and together, they were able to defeat Ari and set Yua free. Liberated, Yua kidnapped Maqi because she wanted Ari to suffer for holding her imprisoned for many years against her will. Ari and the Guardians followed her into the swamp where she and her banshee sisters live and fought them to bring Maqi back. However, Maqi was nearly killed during the confusion of the battle. He was later saved and cured when the Guardians and Orube offered their gifts from the nymph Xin Jing to him.
Although their mission in Arkhanta was successful, the girls have a whole bunch of new problems on Earth that needs their attention. Interpol discovers the secret of the Guardians and wants to capture them and use their powers for their own mysterious intentions. As if that was not enough, the girls' Astral Drops decide to rebel after months of being taken advantage of by the Guardians. The Astral Drops started to cause numerous problems between the Guardians' families, teachers, and boyfriends and eventually they ran away to another city. The Guardians later recaptured the Astral Drops and decided to liberate them from slavery.



The members of the Congregation are questioning the Oracle's recent actions, and he shocked everyone by putting himself on trial. The Guardians are called in to recall his recent unsatisfactory deeds and he was later found guilty by Endarno and was banished back into his home world, Basil ade. Back home in Heatherfield, Will confronted Orube and Matt, while Principal Knickerbocker goes to drastic measures to raise the girls' grades at Sheffield. In Kandrakar, the Council has elected Endarno the new Oracle. After Endarno has been chosen, he makes the life of the Guardians difficult by trying to prove to the Congregation that they are too young and immature. He also convinced the Congregation that Elyon, the queen and the Light of Meridian, is dangerous and unfit to rule over Metamoor. Many times he tried to get the Guardians to arrest their old friend and bring him the Crown of Light. But W.I.T.C.H. managed to hide Elyon from Endarno by smuggling her back to Earth and had Orube protect her. Moreover, Endarno is someone else than he appears to be...an old enemy coming back for revenge and a bigger thirst for power. Elyon and Yan Lin discovered that Endarno is actually Prince Phobos who s been using Endarno s body in his new quest for power and revenge. The Guardians were able locate the old Oracle, now called Himerish upon the discovery of his old identity before leaving for Kandrakar, with the help of Orube. Together, they tried to free Elyon from her prison and put a stop to Cedric and Phobos before Endarno (Phobos) is formally instated as the Oracle. In the end, Himerish was able to locate the real Endarno and he, in turn, revealed to the Council that the Endarno that they knew was Phobos in his body. Elyon returned to power on her world, Cedric was back in the Tower of Mist, and Phobos, rather than being locked up in prison again, threw himself into the endless void of Kandrakar.



The Five Elements themselves have something in store for the girls: their powers are growing stronger and stronger. Each Guardian has discovered new and much, much stronger magical abilities, yet their elemental abilities are going out of control. Even the Triumvirate of Kandrakar does not have full understanding about it. At the same time, Cedric is disposed of his magical strength and power and gets a second chance from the Oracle. He returns to "Ye Olde Book Shop" to live a decent life, under supervision from the Guardians. Nevertheless, he searches for a useful resource to regain his powers in hopes of getting revenge on the Guardians. He finds his luck in the Book of Elements, written by Jonathan Ludmoore (which the Guardians later found out). About a century before, Jonathan Ludmoore was an alchemist who came from Metamoor and it was Prince Phobos who had sent him to Heatherfield in order to open the Twelve Portals. After completing his mission, Ludmoore discovered that the city is a place where all five elements-Water, Fire, Earth, Air, Quintessence- met and stayed. He experimented on the elements but it trapped Ludmoore in his own book and released all five elemental stones throughout the city.
After Matt was swallowed by the Book of Elements (thanks to Cedric), the Guardians must cooperate with Cedric in order to save Matt. The book gives them riddles as clues and the Guardians must used these riddles to look for the scattered elemental stones. Endarno warned the Guardians that in order to capture these stones, they must face the Elemental Guards that stand and protect them. One by one, the girls were able to find the stones, and back in the bookstore, Cedric and Orube slowly fell deeply in love. However, the Guardians, Cedric, and Orube were sucked into the world of the book when Will refuses to surrender the Heart of Kandrakar. Cedric was later killed by Ludmoore and dissolved into ink when he tried to protect Orube from Ludmoore's deadly gaze. The Guardians later found Matt and managed to defeat Ludmoore once and for all (with Matt's help). While Orube mourns for Cedric's death Will's mother, Susan Vandom, married her history teacher, Dean Collins. Orube left and a magical portal in the shape of the Heart of Kandrakar later appeared in the basement of the "Ye Olde Book Shop".



When Hay Lin stayed over at the Lyndons' home, she meets Karl and Tecla Ibsen, an elderly couple who appeared to have a connection with Eric's new school. Karl tells Hay Lin about the Ragorlang, a monster that absorbs the thoughts, the voices, and the sounds of its victims. Coincidentally, a few students at Eric's school are getting inactive as if they had lost their youthful strength. The same evening, Hay Lin gets attacked by a Ragorlang and was saved just in time by the other Guardians. Soon they discover who's behind the new threat: Tecla Ibsen. She wanted to be young and beautiful again and used her magical ability to conjure up and control the Ragorlangs, ordering them to steal the youthful strength of the teenagers by absorbing their thoughts and voices. The Guardians managed to defeat her and her Ragorlang. Unfortunately, Tecla returns, but as a frail, older woman, who can't summon a fully grown Ragorlang. Tecla brainwashed a magical girl named Erin, making her believe that the Guardians are the Ragorlangs and that it was their fault that her brother, Kader, went missing. At first, Erin uses her magic to make the girls hate each other while posing as an exchange student.The Guardians eventually helped her find her brother despite their hate for her.
This saga also introduced a strange optometrist, named Folkner, who stationed himself in Sheffield to monitor the students' health. The Guardians found out that he's a Ragorlang hunter who wishes to hunt down Tecla Isben. She later regain full power due to leaked magics from Kandrakar and attacked the Guardians with a new pact of Ragorlangs. Despite being able to use the Shadow Heart against the Guardians, she was defeated when the last of her Ragorlangs was absorbed into a box created by Folkner. Obsessed with the dark power, Folkner unleashed the powers and became a supreme Ragorlang himself. He was later killed by the Guardians and was sucked into his own Ragorlang box at the end of the saga, and Karl and Tecla were both accepted into the Congregation of Kandrakar after admitting their mistakes.
Note: This was the last saga that was translated into English.



The Guardians must face an ancient enemy   Dark Mother. She was once Meter the Queen of Spring and made flowers bloom beneath her feet. But when she became corrupted, she transform into Dark Mother and attacked Kandrakar. She was defeated by the Elemental Queens of fire, Water, and Air and was cast into the dark void where she continued to wait patiently in chained. In silence, Dark Mother sent a seed and planted it in Kandrakar where it will served as her infiltrator against the Fortress of Light. The Guardians received their New Powers, making them stronger and giving them new abilities. Matt now served as their new trainer and mentor after being recruited by the Oracle to prepare them for their upcoming battle against Dark Mother. One by one, the Guardians found the root of their elements and obtained full control over their new and much stronger abilities. Meanwhile, Dark Mother escaped (thanks to Will) and set off to take over Kandrakar. Thanks to her tree, which she had planted beforehand, Dark Mother enslaved the minds of Kandrakar, except for Yan Lin. The Guardians later faced her in one last battle and Dark Mother was sealed in a stone tomb. Oracle Himerish relinquished his place to Yan Lin who foresaw the coming threat and left without saying goodbye.



The Guardians must find and teach the magical people of Heatherfield how to fully control their new abilities. Oracle Yan Lin assisted them by giving them new weapons to help greatly strengthen and magnify their powers and abilities as well as the key to the W.I.T.C.H. Van, a special on-the-road-school which houses five magical laboratories connected to each Guardians  elements, personalities, and skills. Kandor, a member of the Congregation is sent to be their new bus driver and care-taker. Meanwhile, a new enemy is watching their every move: Foreman Takeda the CEO and scientist of Takeshita Inc. Takeda vowed to destroy all magic because he believed that it is responsible for his daughter s illness. Mariko, his eldest daughter, discovered the Fast World and her mind now trapped there. Takeda enlisted the help of Liam, a mysterious boy that Mariko met in the Fast World and they fell in love, to spy on the Guardians and ordered him to kidnap Will s little brother. Using Liam and William as bait, Takeda lured the Guardians into his lair and used his cold technologies to trap the Guardians into the 'Fast World forever'. However, his plan was thwarted by his youngest daughter, Shinobu, who saw her father s cruelty and had him frozen by his own robotic minions.
While looking for Liam and William in the Fast World, the Guardians encounter the malicious ruler Arkaam, the White Queen, who wished to rule all of the Fast World under her tyranny and possessed a deep desire to kill the Black Queen. It is revealed that the identity of the Black Queen is Mariko s conscious and Shinobu s sister. In the confusing of their escape, Liam was killed by the White Queen after she stabbed him with her sword. Although successful in bringing William back, Will and her friends are horrified that the White Queen and her army had followed them to their world. In their last battle, invading White Queen and her army were dissipated by the Guardians  powerful sound frequency. After hearing Mariko s accusation and Shinobu s forgiveness, Takeda forgot his vendetta and rejoined his family.
Beginning from issue #97 onward, most W.I.T.C.H. comic issue is its own story.



Starting from issue #97 of the previous saga, each issue has its own story. The Guardians now face their daily problems from families, friends, and school. The stories ranged from facing their first day of returning to Sheffield Institute to finding out more about both of their parents  pasts and abilities. At the same time, they must face several new enemies who wanted to destroy them. Many of these villains included the Runics, a group of five evil wizards who control the dark powers of the elements; and Nihila, the Queen of Darkness who desire to weave and control everyone s destinies with her magical Loom. The series also introduced new missions where the Guardians must assist the beings of the natural worlds; from listening to the voice of the Earth to helping Mareeve, the Saarineen, avoid marrying the King of the Sharks, Orristurr. Later on, the stories ranged from Dean's parents returning to Hay Lin and her family celebrating Chinese New Year.



From issue number 118 the concept of arcs is dropped. All stories are one of themselves, sometimes connected to previous issues.



From issue 130 through issue 139, the saga has been named the "Magical Sovereigns".



Aside from the original series, the Walt Disney Company Italia had published several special issues depicting the lives of the Guardians and their friends taking places on the side lines from the original canon issues. The following is a list of special issues that have been published thus far:
The Year Before
Elyon: Return of the Queen
Cornelia and Caleb: A Love Not Meant to Be
Core of Kandrakar
Planet Boys
Orube Special
Two Hearts for a Ball: World Cup Special
Christmas Special 2004
Christmas Special 2005
Christmas Special 2006
W.I.T.C.H. Look Book
Caleb and Elyon - Two Destinies
W.I.T.C.H. on Stage
Halloween Special 2007
Olympic Games 2008 Special
2012 Special









Each Guardian's powers are fueled by Kandrakar, transmitted to the Heart via the Aurameres (the physical representations of the Guardians' powers). The Aurameres grant the same powers to each of their guardians; for instance, powers held by Irma would also have been possessed by Cassidy.
When the Guardians transform, they change form and grow wings. Contrary to the TV series, the wings don't allow them to fly, except after they get the New Powers.
Teletransportation can be performed from one spot to another on earth as well as teletransporting from planet to planet.
Irma and Cassidy control and manipulate the element of Water
Taranee and Halinor control and manipulate the element of Fire
Cornelia and Kadma control and manipulate the element of Earth
Hay Lin and Yan Lin control and manipulate the element of Air
Will and Nerissa control and manipulate the element of Quintessence.
Will and Nerissa, and briefly Cassidy, are the keepers of the Heart of Kandrakar, and as it, leaders of their teams.









Kandrakar is an ethereal dimension in the very center of the entire Universe, where time and space do not exist. In the fortress the wisest of thousands of other planets and worlds are gathered to discuss the state of the worlds which are of their concern. They simply observe and do not interfere.



Metamoor is a world ruled by the Escanor, who originates from Earth. For a long time Metamoor, and its capital Meridian, was isolated from the rest of the universe because it was ruled by the evil prince Phobos. With help of the Guardians and the Meridian people the rightful heiress to the throne, Elyon, could reign again. Elyon is the last of the Escanor.



Arkhanta is an Earth-like world ruled by Ari. Ari accused the Oracle for not wanting to cure his son. Even the banshee Yua was unable to help him. With trickery, Ari could keep Yua in his scarlet citadel forever. Despite all this, Ari brought prosperity to his people. Knowing they had received everything thanks to the powers of Yua, the people are both happy and disturbed. Fortunately, after the Guardians released Yua and healed Ari's son Maqui, any threat of Yua's anger was over. On his turn Ari ended his hostility against Kandrakar.



Basiliade is the homeworld of the Oracle (Himerish), Endarno, Orube and We. It makes part of a binary solar system.



After the success of the 2001 release of the W.I.T.C.H. comic magazines, Disney Publishing Worldwide expanded the W.I.T.C.H. franchise to magazines, video and mobile games and a website.



The W.I.T.C.H. book series consists of 26 books, adapted from the comic series by either Elizabeth Lenhard or Alice Alfonsi. Each book contains comic inserts from the original W.I.T.C.H. comics. The book series was designed for UK and USA release and preceded the graphic novel adaptation in the United States.
The Power of Five (Elizabeth Lenhard)
The Disappearance (Elizabeth Lenhard)
Finding Meridian (Elizabeth Lenhard)
The Fire of Friendship (Elizabeth Lenhard)
The Last Tear (Elizabeth Lenhard)
Illusions and Lies (Elizabeth Lenhard)
The Light of Meridian (Julie Komorn)
Out of the Dark (Julie Komorn)
The Four Dragons (Elizabeth Lenhard)
A Bridge Between Worlds (Alice Alfonsi)
The Crown of Light (Elizabeth Lenhard)
The Return of a Queen (Elizabeth Lenhard)
A Different Path (Elizabeth Lenhard)
Worlds Apart (Alice Alfonsi)
The Courage to Choose (Alice Alfonsi)
Path of Revenge (Alice Alfonsi)
The Darkest Dream (Alice Alfonsi)
Keeping Hope (Alice Alfonsi)
The Other Truth (Alice Alfonsi)
Whispers of Doubt (Alice Alfonsi)
A Weakened Heart (Alice Alfonsi)
A Choice Is Made (Alice Alfonsi)
Farewell to Love (Alice Alfonsi)
Trust Your Heart (Alice Alfonsi)
Enchanted Waters (Alice Alfonsi)
Friends Forever (Alice Alfonsi)



Eight graphic novels based on the W.I.T.C.H. series were published by Hyperion Books in the United States. The novels were first released in 2005, each graphic novel containing two issues of the original comic series. Included in every graphic novel was a miniature poster relating to the W.I.T.C.H. franchise. The titles of the graphic novels are as followed:
The Power of Friendship (Halloween and The Twelve Portals)
Meridian Magic (The Dark Dimension and The Power of Fire)
The Revealing (So Be It Forever and Illusions and Lies)
Between Light and Dark (One Day You'll Meet Him and The Black Roses of Meridian)
Legends Revealed (The Four Dragons and A Bridge Between Worlds)
Forces of Change (The Crown of Light and The Challenge of Phobos)
Under Pressure (I Know Who You Are and The End of a Dream)
An Unexpected Return (The Courage to Choose and Nerissa's Seal)



The comic was never published in Japan, and instead a manga based on the comic, with art by Haruko Iida ( , Iida Haruko), was published by Kadokawa Publishing ( , Kadokawa Shoten) in their monthly magazine Asuka Monthly ( , Gekkan Asuka) and later collected in two Tankoban volumes, upon which the comic was canceled. The paperback volumes contained covers by Daisuke Ehara ( , Ehara Daisuke). The two volumes were also published in Italy and Eastern Europe by the Italian Disney Manga.






An animated Movie was planned by the producers of the TV series, SIP Animation, Jetix Europe, The Walt Disney Company, France 3 and Super RTL but was never produced. This was mostly due to criticism the TV series received from the second season of the show, that had also resulted in the cancelling of the show's planned third season. The Movie's plot was planned to take place between the Second and planned Third Season.






Welcome is a side magazine of W.I.T.C.H. In total, there are eleven issues of the magazine. It is only published in parts of Europe, and is not published in the UK or Australia. Its mascot is We, a creature from Basil ade. Welcome was included with the Italian W.I.T.C.H. magazine, starting with issue #64. The Swedish W.I.T.C.H. magazine started publishing Welcome stories much earlier, instead of publishing a separate magazine.



Danish author Lene Kaaberb l has written nine novels set in the W.I.T.C.H. universe, which have been published in 2002 and 2003 in Denmark. The first five of them each have one of the five girls as the main character in the order of the first letters in their names (Will, Irma, Taranee, Cornelia, and Hay Lin).
When Lightning Strikes (The Heart of the Salamander)
Enchanted Music (The Music of the Silencer)
Heartbreak Island (The Fire of the Ocean)
Stolen Spring (Green Magic)
The Cruel Empress (The Gruesome Empress)
Her next series of W.I.T.C.H. pockets is called Crystal Birds. It contains the following titles:
The Stone Falcon
The Talons of the Eagle
The Shadow of the Owl
The Golden Phoenix



Books that have been released in Norway, Sweden, Finland, Scotland,Denmark and Russia.
Ruben Eliassen
Ibenholt Pyramiden (The Ebony Pyramid)
Josefine Ottesen
Isblomsten (The Iceflower) 2004
Den Gyldne Kilde (The Golden Spring) 2005
Maud Mangold
Queen of the Night
The Flame of Clarity
Lene M ller J rgensen
The Pristine Rose
A Touch of a Star
Cecilie Eken
The Wells of Fog
The Storms of Windmor





