M.E. Sharpe, Inc., an academic publisher, was founded by Myron Sharpe in 1958 with the original purpose of publishing translations from Russian in the social sciences and humanities. These translations were published in a series of journals, the first of which was Problems of Economics, now called Problems of Economic Transition. In the 1960s the translation project was expanded to include other European languages, and then Chinese and later, Japanese. Other academic journals launched by M.E. Sharpe during these years featured articles originating in English. At present the firm publishes over 35 periodicals including Challenge: The Magazine of Economic Affairs, Journal of Management Information Systems, International Journal of Electronic Commerce, Journal of Post-Keynesian Economics, and Problems of Post-Communism. Shortly after it was established, M.E. Sharpe, Inc. also began to publish scholarly books in the social sciences and humanities, with a special emphasis on international studies. In the 1980s the book division was expanded and it currently publishes approximately 60 new titles a year, including works in economics, business, management, public administration, political science, history, and literature. Many of M.E. Sharpe s textbooks are available in digital editions through the Sharpe E-Text Center.
Several Nobel Prize winners, including Kenzaburo  e and Wassily Leontief, are among M.E. Sharpe authors, as is the acclaimed American novelist Howard Fast, author of Spartacus. The East Gate Books imprint is widely recognized as representing the best in Asian Studies.
In 1995 Sharpe Reference was founded to provide essential reference material for the high school, undergraduate, and general reader again, building on Sharpe s areas of strength in American Studies and Global Studies. The full, updated content of many of these reference sets is also available in electronic editions published by Sharpe Online Reference.
M.E. Sharpe, Inc. was first located in New York City and was originally called International Arts and Sciences Press. After 12 years in New York, the firm moved to White Plains, NY. Its offices have been based in Armonk, NY, since 1980.
M.E. Sharpe was sold to Routledge in 2014.



M. E. Sharpe specializes in social sciences, humanities, business management, and public administration.



Some journals published by M.E. Sharpe are:
Challenge: The Magazine of Economic Affairs
European Education
Problems of Post-Communism
Journal of Economic Issues
Journal of Management Information Systems
Journal of Post Keynesian Economics
International Journal of Political Economy


