







All models support: MMX






Based on P6 microarchitecture
All models support: MMX, SSE
All models support quad-processor configurations
Die size: 123 mm 
Steppings: B0, C0



All models support: MMX, SSE
Only Xeon 700 and 900 are capable of quad processor configurations









Based on NetBurst microarchitecture
All models support: MMX, SSE, SSE2



Based on NetBurst microarchitecture
All models support: MMX, SSE, SSE2, Hyper-Threading



Based on NetBurst microarchitecture
All models support: MMX, SSE, SSE2, Hyper-Threading



Based on NetBurst microarchitecture
All models support: MMX, SSE, SSE2, SSE3, Hyper-Threading, Intel 64
Steppings: D0, E0, G1



Based on NetBurst microarchitecture
All models support: MMX, SSE, SSE2, SSE3, EIST, XD bit (an NX bit implementation) and Intel 64.
E processors do not support Hyper-Threading.
Steppings: N0, R0






Based on NetBurst microarchitecture
All models support: MMX, SSE, SSE2, SSE3, Hyper-Threading, Intel 64, XD bit (an NX bit implementation)
Steppings: A0



Based on NetBurst microarchitecture
All models support: MMX, SSE, SSE2, SSE3, Hyper-Threading, Intel 64, XD bit (an NX bit implementation), Intel VT-x
All models support dual-processor configurations
Die size: 2   81 mm 
Demand Based Switching (Intel's Server EIST): Supported by: All except 5060, 5063.
Steppings: C1






Based on NetBurst microarchitecture
All models support: MMX, SSE, SSE2, Hyper-Threading



Based on NetBurst microarchitecture
All models support: MMX, SSE, SSE2, Hyper-Threading



Based on NetBurst microarchitecture
All models support: MMX, SSE, SSE2, SSE3, Hyper-Threading, Intel 64, XD bit (an NX bit implementation)
Steppings: A0, B0



Based on NetBurst microarchitecture
All models support: MMX, SSE, SSE2, SSE3, Hyper-Threading, Intel 64, XD bit (an NX bit implementation)
Steppings: C0






Based on NetBurst microarchitecture
All models support: MMX, SSE, SSE2, SSE3, Hyper-Threading, Intel 64, XD bit (an NX bit implementation), Intel VT-x
Demand Based Switching (Intel's Server EIST): All except 7030.
Steppings: A0



Based on NetBurst microarchitecture
All models support: MMX, SSE, SSE2, SSE3, Hyper-Threading, Intel 64, XD bit (an NX bit implementation), Intel VT-x
All models support quad-processor and octo-processor configurations
Demand Based Switching (Intel's Server EIST): Supported by: All except 7110M/N & 7120M/N.
Die size: 435 mm 
Steppings: B0









Based on Enhanced Pentium M microarchitecture
All models support: MMX, SSE, SSE2, SSE3, Demand Based Switching (Intel's Server EIST) , XD bit (an NX bit implementation), Intel VT-x
All models support dual-processor configurations
Die size: 90.3 mm 
Steppings: C0, D0









Based on Core microarchitecture
All models support: MMX, SSE, SSE2, SSE3, SSSE3, Intel 64, XD bit (an NX bit implementation), Intel VT-x
All models support uni-processor configurations
Die size: 111 mm 
Steppings: L2



Based on Core microarchitecture
All models support: MMX, SSE, SSE2, SSE3, SSSE3, Intel 64, XD bit (an NX bit implementation), Intel VT-x
All models support uni-processor configurations
Die size: 143 mm 
Steppings: B2, G0



Based on Core microarchitecture
All models support: MMX, SSE, SSE2, SSE3, SSSE3, Intel 64, EIST, XD bit (an NX bit implementation), Intel VT-x
All models support dual-processor configurations
Die size: 143 mm 
Steppings: B2, G0
For processors with G0 stepping Vmin = 0.85 V



Based on Penryn microarchitecture
All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x
Models support only uni-processor configurations
Die size: 107 mm 
Steppings: C0, E0



Based on Penryn microarchitecture
All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, Intel 64, XD bit (an NX bit implementation), Intel VT-x
All model support EIST
All models support only single-processor configurations
Die size: 107 mm 
Steppings: C0, E0



Based on Penryn microarchitecture
All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, Intel 64, XD bit (an NX bit implementation), Intel VT-x
All model support EIST except L5238, L5240.
E5205, L5238, L5240, X5260, X5270, X5272 support Demand-Based Switching.
All models support dual-processor configurations
Die size: 107 mm 
Steppings: C0, E0






Based on Core microarchitecture
All models support: MMX, SSE, SSE2, SSE3, SSSE3, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x
All models support uni-processor configurations
Die size: 2   143 mm 
Steppings: B3, G0



Based on Penryn microarchitecture
All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, Enhanced Intel SpeedStep Technology (EIST), Enhanced Halt State (C1E), Intel 64, XD bit (an NX bit implementation), Intel VT-x
All models support uni-processor configurations
Die size: M1: 2   107 mm , R0: 2   81 mm 
Steppings: M1, R0



Based on Penryn microarchitecture
All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, Enhanced Intel SpeedStep Technology (EIST), Enhanced Halt State (C1E), Intel 64, XD bit (an NX bit implementation), Intel VT-x
All models support uni-processor configurations
Die size: 2   107 mm 
Steppings: C1, E0



Based on Penryn microarchitecture
All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, Enhanced Intel SpeedStep Technology (EIST), Enhanced Halt State (C1E), Intel 64, XD bit (an NX bit implementation), Intel VT-x
All models support uni-processor configurations
Die size: 2   107 mm 
Steppings: C1, E0



Based on Core microarchitecture
All models support: MMX, SSE, SSE2, SSE3, SSSE3, Intel 64, XD bit (an NX bit implementation), Intel VT-x
EIST support all except E5310, E5335.
Intel Demand-Based Switching support E5320, E5345, L5318, X5355, X5365.
All models support dual-processor configurations
Steppings: B3, G0
Die size: 2   143 mm 
E5330, E5340 and E5350 is not listed on [2] but it is mentioned on [3]. In August 2007, E5330 is widely available. In June 2007, E5340 Engineering Samples were available on eBay.



Based on Penryn microarchitecture
All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, Intel 64, XD bit (an NX bit implementation), Intel VT-x, Demand-Based Switching except E5405, L5408; EIST except E5405
All models support dual-processor configurations
Die size: 2   107 mm 
Steppings: C0, E0






Based on Core microarchitecture
All models support: MMX, SSE, SSE2, SSE3, SSSE3, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x
All models support quad-processor configurations
Die size: 2   143 mm 
Steppings: G0



Based on Penryn microarchitecture
All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, Demand-Based Switching, Intel 64, XD bit (an NX bit implementation), Intel VT-x
E7440, E7450, X7460 support EIST.
All models support quad-processor configurations
Transistors: 1.9 billion
Die size: 503 mm 
Steppings: A1









Based on Westmere microarchitecture
Uni-processor only
L3406 supports Hyper-Threading, Turbo Boost
All models support: MMX, XD bit, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, x64, SpeedStep, Smart Cache, VT-x, EPT, VT-d, TXT, ECC
Contains 45 nm "Ironlake" GPU.
Die size: 81 mm  (CPU Component)
Steppings: C2



Based on Nehalem microarchitecture
Uni-processor only
All models except X3430 support Hyper-Threading
All models support: MMX, XD bit, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, x64, SpeedStep, Turbo Boost, Smart Cache, VT-x, EPT, VT-d, TXT, ECC
Die size: 296 mm 
Steppings: B1



Based on Nehalem microarchitecture
Uni-processor only
Quad Core models support: Hyper-Threading, Turbo Boost
All models support: MMX, XD bit, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, x64, SpeedStep, Smart Cache, VT-x, EPT, ECC
Die size: 263 mm 
Steppings: D0



Based on Nehalem microarchitecture
Uni-processor only
LC3528 supports: Hyper-Threading, Turbo Boost
All models support: MMX, XD bit, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, x64, SpeedStep, Smart Cache, VT-x, EPT, VT-d, ECC
Die size: 263 mm 
Steppings: B0



Based on Westmere microarchitecture
Uniprocessor-only systems
All models support: Hyper-Threading
All models support: MMX, XD bit, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, x64, AES-NI, SpeedStep, Turbo Boost, Smart Cache, VT-x, EPT, VT-d, TXT, ECC
Die size: 240 mm 
Steppings: B1






Based on Nehalem microarchitecture
All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, Demand-Based Switching (Intel's Server EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x, Intel EPT, Intel VT-d, Intel VT-c, Intel x8 SDDC
All models support: Hyper-Threading, Turbo Boost except E5502, E5503, E5504, E5506, L5506, E5507
All models support dual-processor configurations
Die size: 263 mm 
Steppings: D0



Based on Nehalem microarchitecture
All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, Demand-Based Switching (Intel's Server EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x, Intel EPT, Intel VT-d, Intel VT-c, Intel x8 SDDC
EC5549, LC5528, and LC5518 support: Hyper-Threading, Turbo Boost
Die size: 263 mm 
Steppings: B0



Based on Westmere microarchitecture
All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), TXT, Intel VT-x, Intel EPT, Intel VT-d, Intel VT-c, Intel x8 SDDC, AES-NI, Smart Cache, Hyper-Threading, Turbo Boost except E5603, E5606, E5607, L5609
Dual-socket configurations supported
Die size: 240 mm 
Steppings: B1






Based on Nehalem microarchitecture
All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), TXT, Intel VT-x, Intel EPT, Intel VT-d, Intel VT-c, Intel x8 SDDC, Turbo Boost, Smart Cache, Hyper-Threading except X7542
65xx models support single- and dual-processor configurations, while 75xx models support up to 8-processor configurations
Transistors: 2.3 billion
Die size: 684 mm 
Steppings: D0






Based on Westmere microarchitecture.
All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), TXT, Intel VT-x, Intel EPT, Intel VT-d, Intel VT-c, Intel DDDC, Hyper-threading (except E7-8837), Turbo Boost, AES-NI, Smart Cache.
28xx models support single- and dual-processor configurations, 48xx models support up to four-processor configurations, 88xx models support up to eight-processor configurations.
Transistors: 2.6 billion
Die size: 513 mm 
Steppings: A2






All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), TXT, Intel VT-x, Intel EPT, Intel VT-d, Hyper-threading, AES-NI.
All models support uni-processor configurations only.
Die size:216 mm 
Steppings: D2






All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), TXT, Intel VT-x, Intel EPT, Intel VT-d, Hyper-threading (except E3-1220 and E3-1225), Turbo Boost, AES-NI, Smart Cache.
All models support uni-processor configurations only.
Intel HD Graphics P3000 uses drivers that are optimized and certified for professional applications, similar to Nvidia Quadro and AMD FirePro products.
Die size: D2: 216 mm , Q0: 131 mm 
Steppings: D2, Q0






Based on Sandy Bridge-E CPU.
All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), TXT, Intel VT-x, Intel EPT, Intel VT-d, Intel VT-c, Intel x8 SDDC, Hyper-threading (except E5-2403 and E5-2407), Turbo Boost (except E5-1428L, E5-2403 and E5-2407), AES-NI, Smart Cache.









Based on Sandy Bridge microarchitecture.
All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), TXT, Intel VT-x, Intel EPT, Intel VT-d, Intel VT-c, Intel x8 SDDC, Hyper-threading(except E5-1603, E5-1607, E5-2603, E5-2609 and E5-4617), Turbo Boost (except E5-1603, E5-1607, E5-2603, E5-2609, E5-4603 and E5-4607), AES-NI, Smart Cache.















All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), TXT, Intel VT-x, Intel EPT, Intel VT-d, Hyper-threading, AES-NI.
All models support uni-processor configurations only.
Die size:160 mm 
Steppings: E1






All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, F16C, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), TXT, Intel VT-x, Intel EPT, Intel VT-d, Hyper-threading (except E3-1220 v2 and E3-1225 v2), Turbo Boost, AES-NI, Smart Cache, ECC
Transistors: E1: 1.4 billion
Die size: E1: 160 mm 
All models support uni-processor configurations only.
Intel HD Graphics P4000 uses drivers that are optimized and certified for professional applications, similar to nVidia Quadro and AMD FirePro products.






All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, F16C, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), TXT, Intel VT-x, Intel EPT, Intel VT-d, Intel VT-c, Intel x8 SDDC, Hyper-threading (except E5-2403 v2 and E5-2407 v2), Turbo Boost (except E5-2403 v2, E5-2407 v2 and E5-2418L v2), AES-NI, Smart Cache.
Support for up to six DIMMs of DDR3 memory per CPU socket.









All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, F16C, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), TXT, Intel VT-x, Intel EPT, Intel VT-d, Intel VT-c, Intel x8 SDDC, Hyper-threading (except E5-1607 v2, E5-2603 v2, E5-2609 v2 and E5-4627 v2), Turbo Boost (except E5-1607 v2, E5-2603 v2, E5-2609 v2, E5-2618L v2, E5-4603 v2 and E5-4607 v2), AES-NI, Smart Cache.
Support for up to 12 DIMMs of DDR3 memory per CPU socket.












All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, F16C, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), TXT, Intel VT-x, Intel EPT, Intel VT-d, Intel VT-c, Intel x8 SDDC, Hyper-threading (except E7-8857 v2), Turbo Boost (except E7-4809 v2), AES-NI, Smart Cache.
Support for up to 24 DIMMs of DDR3 memory per CPU socket.















All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, AVX2, FMA3, F16C, BMI1 (Bit Manipulation Instructions1), BMI2, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), TXT, Intel vPro, Intel VT-x, Intel VT-d, Hyper-threading (except E3-1220 v3, E3-1225 v3 and E3-1226 v3), Turbo Boost 2.0, AES-NI, Smart Cache, TSX, ECC, Intel x8 SDDC






All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, F16C, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), TXT, Intel VT-x, Intel EPT, Intel VT-d, Intel VT-c, Intel x8 SDDC, Hyper-threading, Turbo Boost (except E5-2408 v3 and E5-2418L v3), AES-NI, Smart Cache.
Support for up to six DIMMs of DDR3 memory per CPU socket.









All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, AVX2, FMA3, F16C, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x, Intel VT-d, Hyper-threading (except E5-1603 v3, E5-1607 v3, E5-2603 v3, E5-2609 v3, E5-2628 v3, E5-2663 v3, E5-2685 v3 and E5-4627 v3), Turbo Boost 2.0 (except E5-1603 v3, E5-1607 v3, E5-2603 v3, E5-2608L v3, E5-2609 v3 and E5-4610 v3), AES-NI, Smart Cache.
Transistors: Up to 8 cores: 2.60 billion, Up to 12 cores: 3.84 billion, Up to 18 cores: 5.69 billion
Die size: Up to 8 cores: 354 mm , Up to 12 cores: 492 mm , Up to 18 cores: 662 mm 
Support for up to 12 DIMMs of DDR4 memory per CPU socket (E5-2629 v3, 2649 v3 and 2669 v3 also support DDR3 memory).












All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, AVX2, FMA3, F16C, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), Intel VT-x, Intel VT-d, Hyper-threading, Turbo Boost 2.0 (except E7-4809 v3 and 4820 v3), AES-NI, Smart Cache.
Transistors: Up to 18 cores: 5.69 billion
Die size: Up to 18 cores: 662 mm 
Support for up to 24 DIMMs of DDR3 or DDR4 memory per CPU socket.












All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, F16C, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), TXT, Intel VT-x, Intel EPT, Intel VT-d, Hyper-threading, Turbo Boost, AES-NI, Smart Cache, ECC memory.
SoC peripherals include 8   USB (4   2.0, 4   3.0), 6   SATA, 2   Integrated 10 GbE LAN, UART, GPIO, and 32 lanes of PCI Express (8   2.0, 24   3.0), in  16,  8 and  4 configurations.
Support for up to four DIMMs of DDR4 or DDR3L memory per CPU socket.






All models support: MMX, SSE, SSE2, SSE3, SSSE3, SSE4.1, SSE4.2, AVX, F16C, Enhanced Intel SpeedStep Technology (EIST), Intel 64, XD bit (an NX bit implementation), TXT, Intel VT-x, Intel EPT, Intel VT-d, Hyper-threading, Turbo Boost, AES-NI, Smart Cache, ECC memory.
Support for up to four DIMMs of DDR3L memory per CPU socket.























