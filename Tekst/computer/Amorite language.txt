Amorite is an early Northwest Semitic language, spoken by the Amorite tribes prominent in ancient Near Eastern history. It is known exclusively from non-Akkadian proper names recorded by Akkadian scribes during periods of Amorite rule in Babylonia (end of the 3rd and beginning of the 2nd millennium), notably from Mari, and to a lesser extent Alalakh, Tell Harmal, and Khafajah. Occasionally such names are also found in early Egyptian texts; and one place-name   "S n r" ( ) for Mount Hermon   is known from the Bible (Deut. 3:9). Notable characteristics include:
The usual Northwest Semitic imperfective-perfective distinction is found   e.g. Yantin-Dagan, 'Dagon gives' (ntn); Ra a-Dagan, 'Dagon was pleased' (r y). It included a 3rd-person suffix -a (unlike Akkadian or Hebrew), and an imperfect vowel -a-, as in Arabic rather than the Hebrew and Aramaic -i-.
There was a verb form with a geminate second consonant   e.g. Yabanni-Il, 'God creates' (root bny).
In several cases where Akkadian has  , Amorite, like Hebrew and Arabic, has h, thus hu 'his', -haa 'her', causative h- or  - (I. Gelb 1958).
The 1st-person perfect is in -ti (singular), -nu (plural), as in the Canaanite languages.



D. Cohen, Les langues chamito-semitiques, CNRS: Paris 1985.
I. Gelb, "La lingua degli amoriti", Academia Nazionale dei Lincei. Rendiconti 1958, no. 8, 13, pp. 143 163.
H. B. Huffmon. Amorite Personal Names in the Mari Texts. A Structural and Lexical Study, Baltimore 1965.
Remo Mugnaioni. "Notes pour servir d approche   l amorrite" Travaux 16   La s mitologie aujourd hui, Cercle de Linguistique d Aix-en-Provence, Centre des sciences du language, Aix-en-Provence 2000, p. 57-65.
M. P. Streck, Das amurritische Onomastikon der altbabylonischen Zeit. Band 1: Die Amurriter, Die onomastische Forschung, Orthographie und Phonologie, Nominalmorphologie. Alter Orient und Altes Testament Band 271/1, M nster 2000.
^ Nordhoff, Sebastian; Hammarstr m, Harald; Forkel, Robert; Haspelmath, Martin, eds. (2013). "Amorite". Glottolog. Leipzig: Max Planck Institute for Evolutionary Anthropology.