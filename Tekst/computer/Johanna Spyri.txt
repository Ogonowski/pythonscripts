Johanna Louise Spyri (n e Heusser) (German: [jo hana  pi ri]; 12 June 1827   7 July 1901) was a Swiss-born author of novels and best known for her children's stories, and is best known for her book Heidi. Born in the rural area of Hirzel, Switzerland, as a child she spent several summers in the area around Chur in Graub nden, the setting she later would use in her novels.



In 1852, Johanna Heusser married Bernhard Spyri, a lawyer. While living in the city of Z rich she began to write about life in the country. Her first story, A Note on Vrony's Grave, which deals with a woman's life of domestic violence, was published in 1880; the following year further stories for both adults and children appeared, among them the novel Heidi, which she wrote in four weeks. Heidi is the story of an orphan girl who lives with her grandfather in the Swiss Alps, and is famous for its vivid portrayal of that landscape.
Her husband and her only child, named Bernard, both died in 1884. Alone, she devoted herself to charitable causes and wrote over fifty more stories before her death in 1901. She was interred in the family plot at the Sihlfeld-A Cemetery in Z rich. An icon in Switzerland, Spyri's portrait was placed on a postage stamp in 1951 and on a 20 CHF commemorative coin in 2009.

In April 2010 a professor, searching for children's illustrations, found a book written in 1830 by a German history teacher, Hermann Adam von Kamp, that Spyri may have used as a basis for Heidi. The 1830 story is titled Adelaide - das M dchen vom Alpengebirge translated, "Adelaide, the girl from the Alps". The two stories share many similarities in plot line and imagery. Spyri biographer Regine Schindler said it was entirely possible that Spyri may have been familiar with the story as she grew up in a literate household with many books.


