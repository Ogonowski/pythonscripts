Greenliant Systems is a manufacturer of NAND flash memory-based solid state storage and controller semiconductors for embedded systems and datacenter products. Greenliant is headquartered in Santa Clara, California, with offices in North America, Europe and Asia.
Affiliations include JEDEC, NVM Express, PCI-SIG, SATA-IO, and the Storage Networking Industry Association.



Silicon Storage Technology (SST) developed NANDrive technology which was recognized by trade publications in 2008 and 2009. In April 2010, Microchip Technology acquired SST, while SST's founder and CEO Bing Yeh founded Greenliant along with other former SST executives. In May 2010, Greenliant acquired NANDrive technology and other assets from Microchip for an estimated $23.6 million.
The Greenliant logo symbolizes a multi-chip module with an energy-efficient core and the name represents green and reliable.
In November 2010, Greenliant began sampling its Serial ATA interface NANDrive GLS85LS products, which had up to 64GB capacity in a 14mm x 24mm x 1.95mm, 145 BGA (ball grid array), 1mm ball pitch package.
In June 2012, Greenliant began sampling its embedded MultiMediaCard interface NANDrive GLS85VM products, which operate at industrial temperatures between -40 and +85 degrees, and are offered in a 14mm x 18mm x 1.40mm, 100-ball, 1mm ball pitch package.



NAND flash memory controller products manage the inherent deficiencies of NAND flash, providing a simpler interface to a computer system.



NANDrive embedded solid-state drives (SSDs) consist of an integrated controller and NAND flash die in a small multi-chip package. GLS85 devices have the same size across all capacities in each family, are backwards compatible and designed for commercial and industrial grade temperatures.
NANDrive devices have content protection zones and users can select areas of the storage to protect with fast erase.



ArmourDrive mSATA solid-state drives (SSDs), are based on SATA NANDrive, using Greenliant s internally developed NAND flash memory controller. Dedicated power failure detection and backup power circuitry are built-in to prevent data integrity issues due to sudden power interruptions.



Specialty flash memory includes the Concurrent SuperFlash (CSF), Many-Time Programmable and Small-Sector Flash (SSF) families. Available in commercial, extended and industrial grade temperatures, these products are marketed for space and power-constrained code storage applications.


