The Pick operating system (often called just "the Pick system" or simply "Pick") is a demand-paged, multiuser, virtual memory, time-sharing computer operating system based around a unique MultiValue database. Pick is used primarily for business data processing.
The term "Pick system" has also come to be used as the general name of all operating environments which employ this multivalued database and have some implementation of Pick/BASIC and ENGLISH/Access queries. Although Pick started on a variety of minicomputers, the system and its various implementations eventually spread to a large assortment of microcomputers, personal computers and mainframe computers and is still in use as of 2015.



The Pick operating system consists of a database, dictionary, query language, procedural language (PROC), peripheral management, multi-user management and a compiled BASIC Programming language.
The database is a 'hash-file' data management system. A hash-file system is a collection of dynamic associative arrays which are organized altogether and linked and controlled using associative files as a database management system. Being hash-file oriented, Pick provides efficiency in data access time. Originally, all data structures in Pick were hash-files (at the lowest level) meaning records are stored as associated couplets of a primary key to a set of values. Today a Pick system can also natively access host files in Windows or Unix in any format.
A Pick database is divided into one or more accounts, master dictionaries, dictionaries, files and sub-files, each of which is a hash-table oriented file. These files contain records made up of fields, sub-fields and sub-sub-fields. In Pick, records are called items, fields are called attributes, and sub-fields are called values or sub-values (hence the present-day label "multivalued database"). All elements are variable-length, with field and values marked off by special delimiters, so that any file, record, or field may contain any number of entries of the lower level of entity. As a result, a Pick item (record) can be one complete entity (one entire invoice, purchase order, sales order, etc.), or is like a file on most conventional systems. Entities that are stored as 'files' in other common-place systems (e.g. source programs and text documents) must be stored as records within files on Pick.
The file hierarchy is roughly equivalent to the common Unix-like hierarchy of directories, sub-directories, and files. The master dictionary is similar to a directory in that it stores pointers to other dictionaries, files and executable programs. The master dictionary also contains the command-line language.
All files (accounts, dictionaries, files, sub-files) are organized identically, as are all records. This uniformity is exploited throughout the system, both by system functions, and by the system administration commands. For example, the 'find' command will find and report the occurrence of a word or phrase in a file, and can operate on any account, dictionary, file or sub-file.
Each record must have a unique, primary key which determines where in a file that record is stored. To retrieve a record, its key is hashed and the resultant value specifies which of a set of discrete "buckets" (called "groups") to look in for the record. (Within a bucket, records are scanned sequentially.) Therefore, most records (e.g. a complete document) can be read using one single disk-read operation. This same method is used to write the record back to its correct "bucket".
In its initial implementation, Pick records were limited to 32K bytes in total (when a 10MB hard disk cost US$5000), although this limit was removed in the 1980s. Files can contain an unlimited number of records, but retrieval efficiency is determined by the number of records relative to the number of buckets allocated to the file. Each file may be initially allocated as many buckets as required, although changing this extent later may (for some file types) require the file to be quiescent. All modern multi-value databases have a special file-type which changes extent dynamically as the file is used. These use a technique called linear hashing, whose cost is proportional to the change in file size, not (as in typical hashed files) the file size itself. All files start as a contiguous group of disk pages, and grow by linking additional "overflow" pages from unused disk space.
Initial Pick implementations had no index structures as they were not deemed necessary. Around 1990, a B-tree indexing feature was added. This feature makes secondary key look-ups operate much like keyed inquiries of any other database system: requiring at least two disk reads (a key read then a data-record read).
Pick data files are usually two levels. The first level is known as the "dictionary" level and is mandatory. It contains:
Dictionary items   the optional items that serve as definitions for the names and structure of the items in the data fork, used in reporting
The data-level identifier   a pointer to the second or "data" level of the file
Files created with only one level are, by default, dictionary files. Some versions of the Pick system allow multiple data levels to be linked to one dictionary level file, in which case there would be multiple data-level identifiers in the dictionary file.
A Pick database has no data typing since all data is stored as characters, including numbers (which are stored as character decimal digits). Data integrity, rather than being controlled by the system, is controlled by the applications and the discipline of the programmers. Because a logical document in Pick is not fragmented (as it would be in SQL), intra-record integrity is automatic.
In contrast to many SQL database systems, Pick allows for multiple, pre-computed field aliases. For example, a date field may have an alias definition for the format "12 Oct 1999", and another alias formatting that same date field as "10/12/99". File cross-connects or joins are handled as a synonym definition of the foreign key. A customer's data, such as name and address, are "joined" from the customer file into the invoice file via a synonym definition of "customer number" in the "invoice" dictionary.
Pick record structure favors a non-first-normal-form composition, where all of the data for an entity is stored in a single record, obviating the need to perform joins. Managing large, sparse data sets in this way can result in efficient use of storage space. This is why these databases are sometimes called NF2 or NF-squared databases.



Pick was originally implemented as the Generalized Information Retrieval Language System (GIRLS) on an IBM System/360 in 1965 by Don Nelson and Richard (Dick) Pick at TRW. It was supposed to be used by the U.S. Army to control the inventory of Cheyenne helicopter parts. Pick was subsequently commercially released in 1973 by Microdata Corporation (and its British distributor CMC) as the Reality Operating System now supplied by Northgate Information Solutions.
Originally on the Microdata implementation, and subsequently implemented on all Pick systems, a BASIC language called Data/BASIC with numerous syntax extensions for smart terminal interface and database operations was the primary programming language for applications. A PROC procedure language was provided for executing scripts. A SQL-style language called ENGLISH allowed database retrieval and reporting, but not updates (although later, the English command "REFORMAT" allowed updates on a batch basis). ENGLISH did not fully allow manipulating the 3-dimensional multivalued structure of data records. Nor did it directly provide common relational capabilities such as joins. This was because powerful data dictionary redefinitions for a field allowed joins via the execution of a calculated lookup in another file. The system included a spooler. A simple text editor for file-system records was provided, but the editor was only suitable for system maintenance, and could not lock records, so most applications were written with the other tools such as Batch, RPL, or the BASIC language so as to ensure data validation and allow record locking.
By the early 1980s observers saw the Pick operating system as a strong competitor to Unix. Dick Pick founded Pick & Associates, later renamed Pick Systems, then Raining Data and as of 2011 TigerLogic. He licensed "Pick" to a large variety of manufacturers and vendors who have produced different "flavors" of Pick. The database flavors sold by TigerLogic were D3, mvBase, and mvEnterprise. Those previously sold by IBM under the "U2" umbrella are known as UniData and UniVerse. Rocket Software purchased IBM's U2 family of products in 2010 and TigerLogic's D3 and mvBase family of products in 2014.
Dick Pick died of stroke complications in October 1994.
Pick Systems often became tangled in licensing litigation, and devoted relatively little effort to marketing and improving its software. Subsequent ports of Pick to other platforms generally offered the same tools and capabilities for many years, usually with relatively minor improvements and simply renamed (for example, Data/BASIC became Pick/BASIC and ENGLISH became ACCESS). Licensees often developed proprietary variations and enhancements (for example, Microdata created their own input processor called ScreenPro).



What most characterizes Pick is the design and features of the database and the associated retrieval language. The Pick database was licensed to roughly three dozen licensees between 1978 and 1984, some of which are included in this list. Application-compatible implementations evolved into derivatives and also inspired similar systems.
These implementations are split up into three basic divisions:
Hardware: The operating system and database were based in firmware. On the assembler level, all references were to addresses on the disk, rather than RAM.
Software: Intermediate software translated the already existing programming into the native machine language, usually on microprocessors.
Guest operating environments: The system and database were implemented in an application that simulated most of the Pick functions, like the Basic and retrieval languages. In general, they depended on their host operating systems for everything else, from disk mapping to security.
Reality 
The first implementation of the Pick database was on a Microdata platform using firmware and called Reality. The first commercial release was in 1973. Microdata acquired CMC Ltd. in the early 80's and were based in Hemel Hempstead, England. The Microdata implementations ran in firmware, so each upgrade had to be accompanied by a new configuration chip. Microdata itself was eventually bought by McDonnell-Douglas Information Systems. Pick and Microdata sued each other for the right to market the database, the final judgment being that they both had the right. In addition to the RealitySequoia and Pegasus] series of computers, Microdata and CMC Ltd. sold the Sequel(Sequoia) series which was a much larger class able to handle over 1000 simultaneous users. The earlier Reality minicomputers were known to handle well over 200 simultaneous users, although things got pretty slow and it was above the official limit. Pegasus systems superseded Sequoia and could handle even more simultaneous users than its predecessors. The modern version of this original Pick implementation is owned and distributed by Northgate Information Solutions Reality.
Ultimate 
The second implementation of the Pick database was developed in about 1978 by a New Jersey company called The Ultimate Corp, run by Ted Sabarese. Like the earlier Microdata port, this was a firmware implementation, with the Pick instruction set in firmware and the monitor in assembly code on a Honeywell Level 6 machine. The system had dual personalities in that the monitor/kernel functions (mostly hardware I/O and scheduling) were executed by the native Honeywell Level 6 instruction set. When the monitor "select next user" for activation control was passed to the Honeywell WCS (writable control store) to execute Pick assembler code (implemented in microcode) for the selected process. When the users time slice expired control was passed back to the kernel running the native Level 6 instruction set.
Ultimate took this concept further with the DEC LSI/11 family of products by implementing a co-processor in hardware (bit-slice, firmware driven). Instead of a single processor with a WCS microcode enhanced instruction set this configuration used two independent but cooperating CPU's. The LSI11 CPU executed the monitor functions and the co-processor executed the Pick assembler instruction set. The efficiencies of this approach resulted in a 2X performance improvement.
The co-processor concept was used again to create a 5X, 7x and dual-7x versions for Honeywell Level 6 systems. Dual ported memory with private busses to the co-processors were used to increase performance of the LSI11 and Level 6 systems.
Another version used a DEC LSI-11 for the IOP and a 7X board. Ultimate enjoyed moderate success during the 1980s, and even included an implementation running as a layer on top of DEC VAX systems, the 750, 780, 785 and later the MicroVAX. Ultimate also had versions of the Ultimate Operating System running on IBM 370 series systems (under VM and native) and also the 9370 series computers. Ultimate was renamed Allerion, Inc., before liquidation of its assets. Most assets were acquired by Groupe Bull, and consisted of mostly maintaining extant hardware. Bull had its own problems and in approximately 1994 the US maintenance operation was sold to Wang.
Prime INFORMATION 
Devcom, a Microdata reseller, wrote a Pick-style database system called INFORMATION in FORTRAN and assembler in 1979 to run on Prime Computer 50-series systems. It was then sold to Prime Computer and renamed Prime INFORMATION. It was subsequently sold to Vmark Software. This was the first of the guest operating environment implementations. INFO/BASIC, a variant of Dartmouth BASIC, was used for database applications.
UniVerse 
Another implementation of the system called UniVerse was by VMark Software, which operated under Unix and Windows. This was the first one to incorporate the ability to emulate other implementations of the system, such as Microdata's Reality Operating System, and Prime INFORMATION. Originally running on Unix, it was later also made available for Windows. It now is owned by Rocket Software. (The systems developed by Prime Computer and VMark are now owned by Rocket Software and referred to as "U2".)
UniData 
Very similar to UniVerse but UniData had facilities to interact with other Windows applications. It is also owned and distributed by Rocket Software.
PI/open 
Prime Computer rewrote Prime INFORMATION in C for the Unix-based systems it was selling, calling it PI+. It was then ported to other Unix systems offered by other hardware vendors and renamed PI/open.
ADDS 
(Applied Digital Data Systems) This was the first implementation to be done in software only, so upgrades were accomplished by a tape load, rather than a new chip. The "Mentor" line was initially based on the Zilog Z-8000 chipset and this port set off a flurry of other "software implementations" across a wide array of processors with a large emphasis on the Motorola 68000.
Fujitsu Microsystems of America
Another software implementation, existing in the late 1980s. Fujitsu Microsystems of America was acquired by Alpha Microsystems on October 28, 1989.
Pyramid 
Another software implementation, existing in the 1980s
General Automation "Zebra" 
Another software implementation, existing in the 1980s
Altos 
A software implementation on an 8086 chipset platform launched around 1983.
WICAT/Pick
Another software implementation, existing in the 1980s
Sequoia 
Another software implementation, existing from 1984. Sequoia was most well known for its fault-tolerant multi-processor model, which could be dialed into with the users permission and his switching terminal zero to remote with the key on the system consol. He could watch what was done by the support person who had dialed on his terminal 0, a printer with a keyboard. Pegasus came out in 1987. The Enterprise Systems business unit (which was the unit that sold Pick), was sold to General Automation in 1996/1997.
Revelation 
In 1984, Cosmos released a Pick-style database called Revelation, later Advanced Revelation, for DOS on the IBM PC. Advanced Revelation is now owned by Revelation Technologies, which publishes a GUI-enabled version called OpenInsight.
jBASE 
jBASE was released in 1991 by a small company of the same name located in Hemel Hempstead. Written by former Microdata engineers, jBASE emulates all implementations of the system to some degree. jBASE is unique in that it compiles applications to native machine code form, rather than to an intermediate byte code. In 2015, cloud solutions provider Zumasys in Irvine, California, acquired the jBASE distribution rights as well as the intellectual property from Temenos Group.
UniVision 
UniVision was a Pick-style database, designed as a replacement for the Mentor version but with extended features, released in 1992 by EDP located in Sheffield.
OpenQM 
OpenQM is the only MultiValue database product available both as a fully supported non-open source commercial product and in open source form under the General Public Licence. OpenQM is available from its exclusive worldwide distributor, Zumasys.
Cach  
In 2005 InterSystems, the maker of Cach  database, announced support for a broad set of MultiValue extensions in Cach . Cach  for MultiValue.
ONware 
ONware ends isolation of MultiValue applications. ONware equips these applications with the ability to use the common databases, such as Oracle and SQL Server. Using ONware, MultiValue applications can be integrated with Relational, Object and Object-Relational applications.
D3 
Pick Systems ported the Pick operating system to run as a database product utilizing host operating systems such as Unix, Linux or Windows servers, with the data stored within the file system of the host operating system. Previous Unix or Windows versions had to run in a separate partition, which made interfacing with other applications difficult. The D3 releases opened the possibility of integrating internet access to the database or interfacing to popular word processing and spreadsheet applications, which has been successfully demonstrated by a number of users. The D3 family of databases and related tools is owned and distributed by Rocket Software.
Through the implementations above, and others, Pick-like systems became available as database/programming/emulation environments running under many variants of Unix and Microsoft Windows.
Over the years, many important and widely used applications have been written using Pick or one of the derivative implementations. In general, the end users of these applications are unaware of the underlying Pick implementation.






Although SQL-Based Joins and arrays can emulate the Pick data structure to some extent, they are at best clumsy. Relational databases attempt to jam the messy real-world into a fixed columnar world. Pick and other so-called 'post-relational' databases do a much more elegant job of making the database conform to the real world.
In contrast to relational databases which have a mathematical underpinning, Pick was originally developed by, essentially, non-computer scientists. The inability to rigorously explain the rationale of the Pick model has been a source of criticism.



Native Pick did not need another underlying operating system to run. All of the early versions of Pick ran without another operating system. However today's implementations of the Pick operating system needs a host OS (Windows, Linux, Unix, etc.) to exist. The host OS provides access to hardware resources (processor, memory, storage, etc.), but Pick also has its own process and memory management internal to itself. (Purportedly object-oriented solutions like Cach  may address these problems.)
Modern networking in mvBase (one of the more popular implementations) is not possible without a side-car application running in the host OS that translates network connections (TCP ports) to Pick's native implementation of networking, serial connections.
Individual user accounts must be created within the Pick OS, and cannot be tied to an external source (such as local accounts on the host OS, or LDAP.)



Companies looking to hire developers and support personnel for Pick-based systems must train new employees due to the low market penetration of these languages and systems.



The Pick OS invites comparison with MUMPS, which evolved into Cach . Similarities include:
Both systems are built on the efficient implementation of large, sparse, string-indexed arrays;
Both historically commingled the language and the OS;
Both have a similar domain of applicability.


