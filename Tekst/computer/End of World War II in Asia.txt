The end of World War II in Asia occurred on 14 and 15 August 1945, when armed forces of Japan surrendered to the forces of the Allied Powers. The surrender came just over three months after the surrender of the Axis forces in Europe.




November 28, 1943   Tehran Conference   Soviet Union agrees to invade Japan "after the defeat of Germany"   commence stockpiling resources in the Far East.
February 4, 1945   Yalta Conference   Soviet Union agrees to invade Japan within 3 months of German surrender.
April 5, 1945   Soviet Union denounces the Soviet Japanese Neutrality Pact that had been signed on April 13, 1941.
April 29, 1945 - Italian Fascist Republican troops, under Rodolfo Graziani's command, surrender in the "Rendition of Cazerta".
May 8, 1945   Germany surrenders
July 16, 1945   Potsdam Conference.
July 26, 1945   Potsdam Declaration to Japan demanding unconditional surrender.




August 3, 1945   Soviet General Vasilevskii reported to Stalin that Soviet forces ready for invasion from August 7.
August 6, 1945   An atomic bomb, Little Boy, dropped on Hiroshima from a B-29 Superfortress named Enola Gay, flown by Col. Paul Tibbets. It is the first use of atomic weapons in combat.
August 8, 1945   The Soviet Union declares war on Japan.
August 9, 1945   Second, and more powerful plutonium implosion atomic bomb, Fat Man, is dropped on Nagasaki from a B-29 named Bockscar, flown by Maj. Charles Sweeney
August 9, 1945   Soviet Armies launch the Soviet invasion of Manchuria.
August 10, 1945   The 38th Parallel is set as the delineation between the Soviet and US occupation zones in Korea.
August 14, 1945   Emperor Hirohito's decree to surrender announced over radio.
August 14, 1945   General Douglas MacArthur is appointed to head the occupation forces in Japan.
August 16, 1945   Gen. Jonathan Wainwright, a POW since May 6, 1942 following the surrender of U.S. forces in the Philippines under his command, is released from a POW camp in Manchuria.
August 17, 1945   Japanese IGHQ issues formal cease-fire in Manchuria.
August 17, 1945   General MacArthur issues General Order No. 1.
August 18, 1945   Soviet Army invades Karafuto on southern Sakhalin Island.
August 18, 1945   Soviet amphibious landings in northern Korea.
August 18, 1945   Japanese pilots attack two Consolidated B-32 Dominators of the 386th Bomb Squadron, 312th Bomb Group, on a photo reconnaissance mission over Japan. Sgt. Anthony Marchione, 19, a photographer's assistant on the B-32 Hobo Queen II, was fatally wounded in the attack. Marchione would be the last American killed in air combat in the Second World War.
August 18, 1945   Soviet invasion of the Kuril Islands begins with amphibious landings on Shumshu.
August 19, 1945   Kwantung Army HQ transmit capitulation order to Japanese troops in Manchuria.
August 23, 1945   Last Japanese troops on Shumshu surrender to Soviet forces.
August 25, 1945   Japanese surrender in Karafuto (south Sakhalin Island).
August 27, 1945   B-29s drop supplies to Allied POWs in China.
August 29, 1945   The Soviets shoot down a B-29 Superfortress dropping supplies to POWs in Korea
August 29, 1945   U.S. troops land near Tokyo to begin the occupation of Japan.
August 30, 1945   The United Kingdom reoccupies Hong Kong.
September 2, 1945   Formal Japanese surrender ceremony aboard USS Missouri in Tokyo Bay; U.S. President Harry S. Truman declares VJ Day.




September 3, 1945   The Japanese commander in the Philippines, Gen. Yamashita, surrenders to Gen. Wainwright at Baguio.
September 4, 1945   Japanese troops on Wake Island surrender.
September 5, 1945   The British land in Singapore.
September 5, 1945   The Soviets complete their occupation of the Kuril Islands.
September 8, 1945   MacArthur enters Tokyo.
September 8, 1945   U.S. forces land at Incheon to occupy Korea south of the 38th Parallel
September 9, 1945   Japanese in Korea surrender.
September 13, 1945   Japanese in Burma surrender.
October 25, 1945   Japanese in Taiwan surrender to Generalissimo Chiang Kai-shek as part of General Order No. 1, which later led to the ambiguous and unresolved political status of Taiwan.



After Japan's defeat in 1945, with the help of Seri Thai, Thailand was treated as a defeated country by the British and French, although American support mitigated the Allied terms. Thailand was not occupied by the Allies, but it was forced to return the territory it had regained to the British and the French. In the postwar period Thailand had relations with the United States, which it saw as a protector from the communist revolutions in neighbouring countries.



At the end of World War II, Japan was occupied by the Allied Powers, led by the United States with contributions also from Australia, India, New Zealand and the United Kingdom. This foreign presence marked the first time in its history that the island nation had been occupied by a foreign power. The San Francisco Peace Treaty, signed on September 8, 1951, marked the end of the Allied occupation, and after it came into force on April 28, 1952, Japan was once again an independent country.



During the occupation leading Japanese war criminal were tried at the International Military Tribunal for the Far East (Tokyo War Crimes Tribunal). The tribunal was convened on April 29, 1946, to try the leaders of the Empire of Japan for three types of crimes: "Class A" crimes were reserved for those who participated in a joint conspiracy to start and wage war, and were brought against those in the highest decision-making bodies; "Class B" crimes were reserved for those who committed "conventional" atrocities or crimes against humanity; "Class C" crimes were reserved for those in "the planning, ordering, authorization, or failure to prevent such transgressions at higher levels in the command structure."
Twenty-eight Japanese military and political leaders were charged with Class A crimes, and more than 5,700 Japanese nationals were charged with Class B and C crimes, mostly entailing prisoner abuse. China held 13 tribunals of its own, resulting in 504 convictions and 149 executions.
The Japanese Emperor Hirohito and all members of the imperial family such as Prince Asaka, were not prosecuted for involvement in any the three categories of crimes. Herbert Bix explains that "the Truman administration and General MacArthur both believed the occupation reforms would be implemented smoothly if they used Hirohito to legitimise their changes." As many as 50 suspects, such as Nobusuke Kishi, who later became Prime Minister, and Yoshisuke Aikawa, head of the zaibatsu Nissan, and future leader of the Chuseiren, were charged but released without ever being brought to trial in 1947 and 1948. Shiro Ishii received immunity in exchange for data gathered from his experiments on live prisoners. The lone dissenting judge to exonerate all indictees was Indian jurist Radhabinod Pal.
The tribunal was adjourned on 12 November 1948.



Japanese holdout


