The MIT Lincoln Laboratory, located in Lexington, Massachusetts, is a United States Department of Defense research and development center chartered to apply advanced technology to problems of national security. Research and development activities focus on long-term technology development as well as rapid system prototyping and demonstration. These efforts are aligned within key mission areas. The laboratory works with industry to transition new concepts and technology for system development and deployment.






Created in 1951 as a federally funded research and development center of the Massachusetts Institute of Technology, Lincoln Laboratory was focused on improving the nation's air defense system through advanced electronics. The laboratory's inception was prompted by the Air Defense Systems Engineering Committee's 1950 report that concluded the United States was unprepared for the threat of an air attack. Because of MIT's management of the Radiation Laboratory during World War II, the experience of some of its staff on the Air Defense Systems Engineering Committee, and its proven competence in electronics, the U.S. Air Force suggested that MIT could provide the research needed to develop an air defense that could detect, identify, and ultimately intercept air threats.
James R. Killian, then president of MIT, was not eager for MIT to become involved in air defense. He asked the Air Force if MIT could first conduct a study to evaluate the need for a new laboratory and to determine its scope. Killian's proposal was approved, and a study named Project Charles (for the Charles River that flows past MIT) was carried out between February and August 1951. The final Project Charles report stated that the United States needed an improved air defense system and unequivocally supported the formation of a laboratory at MIT dedicated to air defense problems.
This new undertaking was initially called Project Lincoln and the site chosen for the new laboratory was on the Laurence G. Hanscom Field (now Hanscom Air Force Base), where the Massachusetts towns of Bedford, Lexington and Lincoln meet. A Project Bedford (on antisubmarine warfare) and a Project Lexington (on nuclear propulsion of aircraft) were already in use, so Major General Putt, who was in charge of drafting the charter for the new laboratory, decided to name the project for the town of Lincoln.



The Semi-Automatic Ground Environment (SAGE) Air Defense System is the beginning of MIT Lincoln Laboratory's history of developing innovative technology. The system was conceived to meet the challenge of providing air defense to the continental United States. SAGE was designed to collect, analyze, and finally relay data from a multitude of radars, all quickly enough that defense responses could be initiated if needed.
The key to this system was a computer that could perform reliably in real time. MIT's Whirlwind computer, built in the 1940s, looked to be a possible candidate for the system. However, the Whirlwind was not reliable or fast enough for the processing needed for analyzing data coming in from dozens of, perhaps even 100, radars. Jay Forrester, an MIT professor instrumental in the development of the Whirlwind, found the breakthrough to enable the computer to achieve outstanding reliability and doubled speed the magnetic core memory. The magnetic core memory revolutionized computing. Computers became machines that were not just large and fast calculators; their uses for varying applications grew. Industry followed this development closely, adopting the magnetic core memory that expanded the capabilities of computers.
Lincoln Laboratory quickly established a reputation for pioneering advanced electronics in air defense systems. Many of the technical developments that later evolved into improved systems for the airborne detection and tracking of aircraft and ground vehicles have formed the basis for current research.




Since MIT Lincoln Laboratory's establishment, the scope of the problems has broadened from the initial emphasis on air defense to include programs in space surveillance, missile defense, surface surveillance and object identification, communications, homeland protection, high-performance computing, air traffic control, and intelligence, surveillance, and reconnaissance (ISR). The core competencies of the laboratory are in sensors, information extraction (signal processing and embedded computing), communications, integrated sensing, and decision support, all supported by a strong advanced electronic technology activity.
Lincoln Laboratory conducts research and development pertinent to national security on behalf of the military services, the Office of the Secretary of Defense, and other government agencies. Projects focus on the development and prototyping of new technologies and capabilities. Program activities extend from fundamental investigations, through simulation and analysis, to design and field testing of prototype systems. Emphasis is placed on transitioning technology to industry.
The work of Lincoln Laboratory revolves around several mission areas:
Space Control
Air and Missile Defense Technology
Communication Systems 
Cyber Security and Information Sciences 
Intelligence, Surveillance, and Reconnaissance Systems and Technology
Advanced Technology
Tactical Systems
Homeland Protection
Air Traffic Control
Engineering
Lincoln Laboratory also undertakes work for non-DoD agencies such as programs in environmental monitoring for NASA and the National Oceanic and Atmospheric Administration.
The dissemination of information to the government, academia, and industry is a principal focus of Lincoln Laboratory's technical mission. Wide dissemination of technical information is achieved through annual technical workshops, seminars, and courses hosted at the laboratory. Toward the goal of knowledge sharing, the laboratory publishes the Lincoln Laboratory Journal, which contains comprehensive articles on current major research and journalistic pieces highlighting novel projects. Other publications include Tech Notes, brief descriptions of Laboratory capabilities and technical achievements; the Annual Report, which highlights technical accomplishments and ongoing corporate and community outreach initiatives; and an overview brochure, MIT Lincoln Laboratory: Technology in Support of National Security. Current news about Laboratory technical milestones is featured on the laboratory's website.
MIT Lincoln Laboratory maintains a strong relationship with the MIT campus. Ongoing research collaborations, student internship programs, reciprocal seminar series, and cooperative community and educational outreach projects are just a few of the ways the laboratory and the campus share the talents, facilities, and resources of each other.



Approximately 1700 technical staff members work on research, prototype building, and field demonstrations. The technical staff come from a broad range of scientific and engineering fields, with electrical engineering, physics, computer science and mathematics being among the most prevalent. Two-thirds of the professional staff hold advanced degrees, and 60% of those degrees are at the doctoral level.
The technical work is organized into eight divisions: Air, Missile, & Maritime Defense Technology, Homeland Protection and Air Traffic Control, Cyber Security and Information Sciences, Communication Systems, Engineering, Advanced Technology, Space Systems and Technology, and ISR and Tactical Systems.
Lincoln Laboratory supports its research and development work with a strong infrastructure of services from six departments: Contracting Services, Facility Services, Financial Services, Information Services, Security Services, and Human Resources. Approximately 1300 people working in the service departments or as technical specialists support the research and development mission of the laboratory.
Lincoln Laboratory demonstrates a firm commitment to community outreach. Programs that promote education in science, technology, engineering, and mathematics for students in grades kindergarten to high school are well received by the local community and well supported by volunteers from across the laboratory. The Lincoln Laboratory community service program raises awareness of both local and national needs by organizing fund-raising and outreach events that support selected charitable organizations, medical research, and U.S. troops abroad.
MIT Lincoln Laboratory is also committed to diversity and inclusion in the workforce as core values. All employees contribute to an environment founded upon technical excellence and outstanding innovation. The laboratory recognizes that its continuing success is achieved through the appreciation and support of the diverse talents, ideas, cultures, and experiences of its employees.






Since 1995, the Lincoln Space Surveillance Complex in Westford, Massachusetts, has played a key role in space situational awareness and the laboratory's overall space surveillance mission. The site comprises three major radars   Millstone Deep-Space Tracking Radar (an L-band radar), Haystack Long-Range Imaging Radar (X-band), and the Haystack Auxiliary Radar (Ku-band). Lincoln Laboratory is also engaged in field work at sites in the continental U.S. and the Pacific region.



Lincoln Laboratory serves as the scientific advisor to the Reagan Test Site at the U.S. Army Kwajalein Atoll installation located about 2500 miles WSW of Hawaii. The laboratory also supports upgrades to the command-and-control infrastructure of the range to include applications of real-time discrimination and decision aids developed as a result of research at the laboratory.



The Experimental Test Site is an electro-optical test facility located on the grounds of the White Sands Missile Range in Socorro, New Mexico. The ETS is operated by the laboratory for the Air Force; its principal mission is the development, evaluation, and transfer of advanced electro-optical space surveillance technologies. The ETS has been a contributing sensor to the U.S. Air Force Space Command. A spin-off program for NASA, Lincoln Near-Earth Asteroid Research, uses the ground-based electro-optical deep-space surveillance telescopes at White Sands to discover near-Earth asteroids. More than 50% of the known asteroids in our solar system have been discovered through this program.



The Pacific Missile Range Facility (PMRF) on the Hawaiian island of Kauai is one of the ranges supporting experimental and developmental testing of the Ballistic Missile Defense System. Lincoln Laboratory personnel provide technical advice, consultation, and analysis support as requested by the government leadership at the range.



F. Wheeler Loomis, July 26, 1951   July 9, 1952
Albert G. Hill, July 9, 1952   May 5, 1955
Marshall G. Holloway, May 5, 1955   February 1, 1957
Carl F.J. Overhage, February 1, 1957   February 1, 1964
William H. Radford, February 1, 1964   May 9, 1966
C. Robert Weiser, acting director, May 10, 1966   January 1, 1967
Milton U. Clauser, January 1, 1967   June 1, 1970
Gerald P. Dinneen, June 1, 1970   April 1, 1977
Walter E. Morrow, Jr., April 1, 1977   June 30, 1998
David L. Briggs, July 1, 1998   June 30, 2006
Eric D. Evans, July 1, 2006   present



List of United States college laboratories conducting basic defense research






Official website
Lincoln Laboratory Journal,
Information Systems Technology Speech Corpora for researchers
Publications at MIT Lincoln Laboratory