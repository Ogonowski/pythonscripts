Both "14nm" and "16nm" fabrication nodes are discussed here
The 14 nanometer (14 nm) semiconductor device fabrication node is the technology node following the 22 nm/(20 nm) node. The naming of this technology node as "14 nm" came from the International Technology Roadmap for Semiconductors (ITRS).
The first 14 nm scale devices were shipped to consumers by Intel in 2014.






14 nm resolution is difficult to achieve in a polymeric resist, even with electron beam lithography. In addition, the chemical effects of ionizing radiation also limit reliable resolution to about 30 nm, which is also achievable using current state-of-the-art immersion lithography. Hardmask materials and multiple patterning are required.
A more significant limitation comes from plasma damage to low-k materials. The extent of damage is typically 20 nm thick, but can also go up to about 100 nm. The damage sensitivity is expected to get worse as the low-k materials become more porous.
For comparison, the atomic radius of unstrained silicon is 111 pm (0.111 nm). Thus about 90 Si atoms would span the channel length, leading to substantial leakage.
Tela Innovations and Sequoia Design Systems developed a methodology allowing double exposure for the 14 nm node. c.2010.
Samsung and Synopsys have also begun implementing double patterning in 22 nm and 16 nm design flows.
Mentor Graphics reported taping out 16 nm test chips in 2010.
On January 17, 2011, IBM announced that they were teaming up with ARM to develop 14 nm chip processing technology.
On February 18, 2011, Intel announced that it would construct a new $5 billion semiconductor fabrication plant in Arizona, designed to manufacture chips using the 14 nm manufacturing processes and leading-edge 300 mm wafers. The new fabrication plant was to be named Fab 42, and construction was meant to start in the middle of 2011. Intel billed the new facility as "the most advanced, high-volume manufacturing facility in the world," and said it would come on line in 2013. Intel has since decided to postpone opening this facility and instead upgrade its existing facilities to support 14-nm chips. On May 17, 2011, Intel announced a roadmap for 2014 that included 14 nm transistors for their Xeon, Core, and Atom product lines.



In 2005, Toshiba demonstrated 15 nm gate length and 10 nm fin width using a sidewall spacer process. It has been suggested that for the 16 nm node, a logic transistor would have a gate length of about 5 nm. In December 2007, Toshiba demonstrated a prototype memory unit that used 15 nanometer thin lines.
In December 2009, National Nano Device Laboratories, owned by the Taiwanese government, produced a 16 nm SRAM chip.
In September 2011, Hynix announced the development of 15 nm NAND cells.
In December 2012, Samsung Electronics taped out a 14 nm chip.
In September 2013, Intel demonstrated an Ultrabook laptop that used a 14 nm Broadwell CPU and Intel CEO Brian Krzanich said "[CPU] will be shipping by the end of this year." However, shipment had been delayed further until Q4 2014.
In August 2014, Intel announced details of the 14 nm microarchitecture for its upcoming Core M processors, the first product to be manufactured on Intel's 14 nm manufacturing process. The first systems based on the Core M processor were to become available in Q4 2014 - according to the press release. "Intel's 14 nanometer technology uses second-generation Tri-gate transistors to deliver industry-leading performance, power, density and cost per transistor," said Mark Bohr, Intel senior fellow, Technology and Manufacturing Group, and director, Process Architecture and Integration.



On 5 September 2014, Intel launched the first three Broadwell-based processors that belonged to the low-TDP Core M family, Core M 5Y10, Core M 5Y10a and Core M 5Y70.
In February 2015, Samsung announced its flagship smartphones Galaxy S6 and Galaxy S6 Edge would feature 14 nm Exynos systems-on-a-chip.
On March 9, 2015, Apple Inc. released the "Early 2015" MacBook and MacBook Pro, which utilized 14 nm Intel processors. Of note is the i7-5557U, which has Intel Iris 6100 graphics and two cores running at 3.1Ghz, using only 28 watts.


