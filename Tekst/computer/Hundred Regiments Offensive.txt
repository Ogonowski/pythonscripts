The Hundred Regiments Offensive (Chinese:  ) (20 August   5 December 1940) was a major campaign of the Communist Party of China's National Revolutionary Army divisions commanded by Peng Dehuai against the Imperial Japanese Army in Central China. The battle had long been the focus of propaganda in history of Chinese Communist Party but had become Peng Dehuai's "crime" in Cultural Revolution. Certain issues regarding its launching and consequences still have controversy.



In 1939 1940, the Japanese occupiers launched more than 109 small campaigns involving around 1,000 combatants each and 10 large campaigns of 10,000 men each to wipe out Communist guerrillas in the Hebei and Shandong plains. In addition, Wang Jingwei s anti-Communist puppet government had its offensive against the CCP guerrillas.
There was also a general sentiment among the anti-Japanese resistance forces particularly in the Kuomintang that the CCP was not contributing enough to the war effort, and that they were only interested in expanding their power base. It was out of these circumstances that the CCP planned to stage a great offensive to prove that they were helping the war effort and to mend KMT-CCP relations.




The Japanese North China Area Army estimated the strength of communist regulars to be about 88,000 in December 1939. Two years later, they revised the estimate to 140,000. On the eve of the battle, the Communist forces grew to 400,000 men strong, in 115 regiments. The extraordinary success and expansion of the 8th Route Army against the Japanese had Zhu De and the rest of the military leadership hoping that they could engage the Japanese army and win.
By 1940, growth was so impressive that Zhu De ordered a coordinated offensive by most of the communist regulars (46 regiments from the 115th Division, 47 from the 129th, and 22 from the 120th) against the Japanese-held cities and the railway lines linking them. According to CCP's official statement the battle started on 20 August. From 20 August to 10 September, communist forces attacked the railway line that separated the communist base areas, chiefly those from Dezhou to Shijiazhuang in Hebei, Shijiazhuang to Taiyuan in central Shanxi, and Taiyuan to Datong in northern Shanxi. Originally Peng's order of battle consisted of 20 regiments and on 22 August he found more that 80 regiments took part in, mostly without telling him.
They succeeded in blowing up bridges and tunnels and ripping up track, and went on for the rest of September to attack Japanese garrisons frontally. About 600 mi (970 km) of railways were destroyed, and the Jingxing coal mine which was important to the Japanese war industry was rendered inoperative for six months. It was the greatest victory the CCP fought and won during the war.
However, from October to December, the Japanese responded in force, reasserting control of railway lines and conducting aggressive "mopping up operations" in the rural areas around them. On 22 December, Mao Zedong told Peng Dehuai "Don't declare the end of the offensive yet. Chiang Kai-shek is launching anti-communist climax and we need the influence of Hundred Regiment Battle to win propaganda."



The Eighth Army had left two reports that are both based on statistics before December 5, one claiming killing/injuring 12,645 Japanese and 5,153 puppet troops; capturing 281 Japanese and 1,407 puppet troops; 7 Japanese and 1,845 puppet troops defected; 293 strong-points taken. The other one claimed killing/injuring 20,645 Japanese and 5,155 puppet troops; capturing 281 Japanese and 18,407 puppet troops; 47 Japanese and 1,845 puppet troops defected; 2,993 strongpoints taken. These two records were both based on the same figure but separated to two different records for unknown reason. This amounted to 21,338 and 46,000 combat successes respectively. In 2010, a Chinese article by Pan Zeqin emerged to say the combat success result should be more than 50,000. No figure about total casualties in Japanese military record but it was recorded 276 KIAs from 4th Independent Mixed Brigade and  133 KIA and 31 MIA from 2nd Independent Mixed Brigade. A western source recorded 20,900 Japanese casualties and about 20,000 collaborator casualties
Chinese also recorded 474 km of railway and 1502 km of road sabotaged, 213 bridges and 11 tunnels blown up, and 37 stations destroyed. But Japanese record gives 73 bridges, 3 tunnels, and 5 water towers blown up; 20 stations burned, and 117 railway sabotages (amounting to 44 km). The damage regarding communication systems are 1,333 cut down and 1,107 capsized cable posts, up to 146 km long cable cut. One mining site of Jingxing Coal Mine also stopped operating for half a year.



When General Yasuji Okamura took command of the North China Area Army in the summer 1941, the new strategy was "Three All", meaning "kill all, burn all, and destroy all" in those areas containing Anti-Japanese forces.



Peng and Mao had disagreed over how directly to confront the Japanese since at least the Luochuan Conference in August 1937, with Mao concerned about Communist losses to the well equipped Japanese. After the establishment of the People's Republic Mao is alleged to have said to Lin Biao that "allowing Japan to occupy more territory is the only way to love your country. Otherwise, it would have become a country that loved Chiang Kai-shek." Thus, the Hundred Regiments Offensive became the last of the two major Communist frontal engagements against the Japanese during the war. There had been controversy that Peng had no authorization, even no knowledge of Central Military Committee and Mao Zedong. As early as 1945 the accusation of launching battle without telling Mao had appeared in the North China Conference. During the Great Leap Forward, Peng's bad temper led to his downfall and then the launching of the battle became yet again a criminal action in Cultural Revolution. In 1967, the Red Guard group of Tsinghua University, with the support of Central Cultural Revolution Committee issued a leaflet saying "The mug Peng, along with Zhu De, launched the offensive to defend Chongqin ... He rejected Chairman Mao's instruction and mobilized 105 regiments in an adventuristic pulse ... Chairman Mao said 'How can Peng Dehuai make such a big move without consulting me? Our forces are completely revealed. The result shall be terrible.'"
Peng admitted in his memoir  he ordered the launch in late July, without awaiting a green light of the Central Military Committee and he regretted it. But Pan Zeqin said that it was Peng's incorrect memory, the correct start date should have officially been on August 20, so Peng actually had the green light. Nie Rongzhen defended Peng, stating "there is a legend that the offensive did not have the knowledge of Central Military Committee. After investigation we found out that Eight Army HQ sent to the top a report. The report mentioned we would strike at and sabotage Zhentai Railway. Sabotaging one railway or another is very common in guerilla warfare so it's our routine work. This is not some strategic issue and the Committee won't say no". He mentioned no exact date of launch. The consensus in China after Cultural Revolution is generally in support of the battle. But modern Chinese article described that "Liu Bocheng had some opinion on the arbitrary launching of the battle of Peng."
While a successful campaign, Mao later attributed it as the main provocation for the devastating Japanese Three Alls Policy later and used it to criticize Peng at the Lushan Conference.






The Battle of One Hundred Regiments, from Kataoka, Tetsuya; Resistance and Revolution in China: The Communists and the Second United Front. Berkeley: University of California Press, [1974]. [1]
     (Morimatsu Toshio:Chinese Front:The defeat and victory of Hundred Regiment Offensive)    137     1982 



Map of the 100 Regiments Offensive
Map of the 100 Regiments Offensive and Japanese counter attacks with units ID