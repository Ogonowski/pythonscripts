A third-generation programming language (3GL) is a generational way to categorize high-level computer programming languages. Where assembly languages, categorized as second generation programming languages, are machine-dependent, 3GLs are much more machine independent and more programmer-friendly. This includes features like improved support for aggregate data types, and expressing concepts in a way that favors the programmer, not the computer. A third generation language improves over a second generation language by having the computer take care of non-essential details. 3GLs feature more abstraction than previous generations of languages, and thus can be considered higher level languages than their first and second generation counterparts.
First introduced in the late 1950s, Fortran, ALGOL, and COBOL are early examples of this sort of language.
Most popular general-purpose languages today, such as C, C++, C#, Java, BASIC and Pascal, are also third-generation languages, although each of these languages can be further subdivided into other categories based on other contemporary traits.
Most 3GLs support structured programming.
A programming language such as C, FORTRAN, or Pascal enables a programmer to write programs that are more or less independent from a particular type of computer. Such languages are considered high-level because they are closer to human languages and further from machine languages. In contrast, assembly languages are considered low-level because they are very close to machine languages.
The main advantage of high-level languages over low-level languages is that they are easier to read, write, and maintain. Ultimately, programs written in a high-level language must be translated into machine language by a compiler or interpreter.
The first high-level programming languages were designed in the 1950s. Examples of early high level languages are ALGOL, COBOL, and FORTRAN. These programs could run on different machines so they were machine-independent. As new, more abstract languages have been developed, however, the concept of high and low level languages have become rather relative. Many of the early "high level" languages are now considered relatively low level in comparison to languages such as Python, Ruby, and CommonLisp.



Domain-specific programming language
Second generation programming language
Fourth-generation programming language


