Alain Berset (born 9 April 1972) is a Swiss politician of the Social Democratic Party. Since 1 January 2012, he is a member of the Swiss Federal Council, the seven member Swiss government, and head of the Federal Department of Home Affairs (the Swiss interior minister). Before being elected to the Federal Council in December 2011, he was a member of the Swiss Council of States for the Canton of Fribourg since 2003, serving as the chamber's president during the 2008/2009 term.



Born in Fribourg on 9 April 1972, the son of a teacher and a bookseller, Berset studied political science and economics at the University of Neuch tel, where he received a masters degree in political science in 1996 and a Ph.D in economics in 2005 with a dissertation about the role of international migration upon local working conditions.
Berset is married to Muriel Zeender Berset and father of three children. The family lives in Belfaux, a village near Fribourg.



Berset worked as assistant lecturer and researcher at the Institute for Regional Economics of the University of Neuch tel from 1996 till 2000, when he moved to the Hamburg Institute for Economic Research for a year. In 2000 he became a member of the Constituent Assembly of the canton of Fribourg and president of its social democrat parliamentary group till 2004. He also served on the Belfaux communal parliament from 2001 to 2003. In 2002, he became strategic consultant to the Department of Economic Affairs of the canton of Neuch tel. In 2003 he was elected to the Swiss Council of States for the canton of Fribourg as a member of the Social Democratic Party becoming the youngest member of the Council of States, and the party's parliamentary group's vicepresident in December 2005. He was also a member of the parliamentary assembly of the Organization for Security and Co-operation in Europe. After his reelection in 2007, he was elected the State Council's vicepresident and served as its president during 2008/2009.
On 14 December 2011, he was elected with 126 votes out of 245 to the Swiss Federal Council, the seven member Swiss government, as one of the Social Democratic Party's two official candidates to succeed head of the Department of Foreign Affairs Micheline Calmy-Rey, who had resigned from the Federal Council. Berset became head of the Federal Department of Home Affairs as its former head Didier Burkhalter changed to the Department of Foreign Affairs.



Berset is the author of several books and some 30 articles on economic development, migration and regional development.






Official biography in English Federal Department of Home Affairs FDHA
Alain Berset, Speaker of the Council of States 2008/2009 The Federal Assembly - The Swiss Parliament
Biography of Alain Berset on the website of the Swiss Parliament. (French)
Main d'oeuvre  trang re et diversit  des comp tences, Editions Harmattan, Paris 2000
Transformation des syst mes locaux d'emploi et comp titivit  des r gions: le r le des migrations internationales, thesis for doctorate in economics, Editions Universitaires, Neuch tel 2005
Circulation of Competencies and Dynamics of Regional Production Systems , International Journal of Multicultural Societies, vol.8, no.1, 2006, pp. 61 83, with O. Crevoisier
Changer d' re, pour un nouveau contrat gouvernemental,  ditions Favre, Lausanne 2007, with C. Levrat
 Ciel, le Parlement a d mantel  mon projet de loi; les al as de la phase parlementaire , in Fluckiger A., Guy-Ecabert C, Guider les parlements et les gouvernements pour mieux l gif rer, Edition Schulthess, Z rich 2007 pp. 137 145