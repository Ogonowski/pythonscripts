A puppet state is a metaphor of a state that is supposedly independent but is in fact dependent upon an outside power, it is nominally sovereign but effectively controlled by a foreign or otherwise alien power, for reasons such as financial interests, in fact anything but the common good.
A puppet state preserves the external paraphernalia of independence like a name, flag, anthem, constitution, law codes and motto but in reality is an organ of another state which created or sponsored the government. Puppet states are not recognised as legitimate under international law.



In the Middle Ages vassal states existed which were based on delegation of rule of a country from a King to noble men of lower rank. Since the Peace of Westphalia of 1648 the concept of a nation came into existence where sovereignty was connected more to the people who inhabited the land than to the nobility who owned the land. The term is a metaphor which compares a state or government to a puppet controlled by an outside puppeteer using strings. The first recorded use of the term "puppet government" is from 1884, in reference to Egypt.



The Batavian Republic was established in the Netherlands under French revolutionary protection.
In Italy, republics were created in the late 18th and early 19th centuries with the assistance and encouragement of Napoleonic France. See also French client republics.
In 1836 U.S. citizens allowed to live in the Mexican state of Texas revolted against the Mexican government to establish a U.S.-backed Republic of Texas, a country that existed less than 10 years (from May 14, 1836 to December 29, 1845) before it was annexed to the United States of America. However, since August 1837, Memucan Hunt, Jr., the Texan minister to the United States, submitted the first official annexation proposal to the Van Buren administration.
In 1895, Japan detached Korea from its tributary relationship with China, giving it formal independence which was a prelude to Japanese annexation.
In 1896 Britain established a state in Zanzibar.



Kingdom of Poland (1916 1918)   The Central Powers' forces occupied Russian Congress Poland in 1915 and in 1916 the German Empire and Austria-Hungary created a Polish Monarchy in order to exploit the occupied territories in an easier way and mobilize the Poles against the Russians (see Polish Legions). In 1918 the state became independent and formed the backbone of the new internationally recognized Second Polish Republic.
Kingdom of Lithuania (1918)   after Russia's defeat, the Germans established a Lithuanian kingdom. However it became an independent republic with Germany's defeat.
Duchy of Courland and Semigallia (1918)   in 1915 the Imperial German forces occupied the Russian Courland Governorate and the Treaty of Brest-Litovsk ended the war in the east, so the local ethnic Baltic Germans established a Duchy under the German crown from that part of Ober Ost, with a common return of civil administration in favor of military. This state was very swiftly merged with the Baltic State Duchy, and German-occupied territories of Russian Empire in Livonia and Estonia, into a multi-ethnic United Baltic Duchy.
Kingdom of Finland (1918)   A short-lived monarchy in Finland after the end of czarist rule
Belarusian People's Republic - Short lived republic founded in German occupied Belarus, generally operating as a German dependent collaborationist government from 1918 to 1919.
Provisional National Government of the Southwestern Caucasus and Provisional Government of Western Thrace were the provisional republics that were established by the Turkish minorities in Thrace and Caucasia, after the Ottoman Empire lost its lands in these regions. Both were the products of the Ottoman Intelligence agency, Te kilat-  Mahsusa, in terms of organisational structure and organisers, and they had remarkably common features. 



in Ukraine
Odessa Soviet Republic (1917 1918)
Soviet Republic of Tavrida (1917 1918)
Donetsk-Krivoy Rog Soviet Republic (1917 1918)
Ukrainian People's Republic of Soviets (1917 1918)
Crimean Socialist Soviet Republic (1919)
Galician Soviet Socialist Republic (1920)
Ukrainian Soviet Socialist Republic (1919 1941)

in Belarus
Socialist Soviet Republic of Byelorussia (1919)
Lithuanian Belorussian Soviet Socialist Republic (1919)
Byelorussian Soviet Socialist Republic (1920 1941)

in Central Europe
Slovak Soviet Republic

in Lithuania
Lithuanian Soviet Socialist Republic (1918 19) (1918 1919)
Lithuanian Belorussian Soviet Socialist Republic (1919)

in Latvia
Executive Committee of Latvia (1917)
Latvian Socialist Soviet Republic (1918 1920)

in Estonia
Commune of the Working People of Estonia (1918 1919)

in Finland
Finnish Socialist Workers' Republic (1918)

in Southern Russia
Black Sea Soviet Republic (1918)
Kuban Soviet Republic (1918)
Kuban-Black Sea Soviet Republic (1918)
Stavropol Soviet Republic (1918)
Terek Soviet Republic (1918)
North Caucasian Soviet Republic (1918)

in Caucasus and Caspiy
Mughan Soviet Republic (1919)
Azerbaijan Soviet Socialist Republic (1920 1991)
Persian Socialist Soviet Republic (1920 1921)
Transcaucasian Socialist Federative Soviet Republic (1922 1936)
Armenian Soviet Socialist Republic (1920 1991)
Georgian Soviet Socialist Republic (1921 1991)
Socialist Soviet Republic of Abkhazia (1921 1931)

in Turkestan
Bukharan People's Soviet Republic (1920 1925)
Khorezm People's Soviet Republic (1920 1925)

in Russian Far East
Russia Eastern Outskirts (1920)
Far Eastern Republic (1920 1922)
Tuvan People's Republic (1921 1944)



in Moldova
Bessarabian Soviet Socialist Republic in exile (1919)

in Poland
Polrevkom (1920)




During Japan's imperial period, and particularly during the Pacific War (parts of which are considered the Pacific theatre of World War II), Japan established a number of dependent states.



Manchukuo (1932 1945), set up in Manchuria under the leadership of the last Chinese Emperor, Puyi.
Mengjiang, set up in Inner Mongolia on May 12, 1936, as the Mongol Military Government ( ) was renamed in October 1937 as the Mongol United Autonomous Government ( ). On September 1, 1939, the predominantly Han Chinese governments of South Chahar Autonomous Government and North Shanxi Autonomous Government were merged with the Mongol Autonomous Government, creating the new Mengjiang United Autonomous Government ( ). All of these were headed by De Wang.
East Hebei Autonomous Council   a state in northeast China between 1935 and 1938.
Dadao government (Shanghai 1937-1940)   A short lived regime based in Shanghai.
Reformed Government of the Republic of China   First regime established in Nanjing after the Battle of Nanjing. Later fused into the Provisional Government of China.
Provisional Government of China December 14, 1937   March 30, 1940   Incorporated into the Nanjing Nationalist Government on March 30, 1940.
Nanjing Nationalist Government ( March 30, 1940   1945)   Established in Nanjing under the leadership of Wang Jingwei.
State of Burma (Burma, 1942 1945)   Head of state Ba Maw.
Second Philippine Republic (1943 1945)   government headed by Jos  P. Laurel as President.
The Provisional Government of Free India (1943 1945), set up in Singapore in October 1943 by Subhas Chandra Bose was in charge of Indian expatriates and military personnel in Japanese Southeast Asia. The government was established with prospective control of Indian territory to fall to the offensive to India. Of the territory of post-independence India, the government took charge of Kohima (after it fell to Japanese-INA offensive), parts of Manipur that fell to both the Japanese 15th Army as well as to the INA, and the Andaman and Nicobar Islands.
Empire of Vietnam (H n t :  ) (March August 1945)   Emperor B o  i's regime with Tran Trong Kim as prime minister after proclaiming independence from France.
Kingdom of Cambodia (Cambodia, March August 1945)   King Norodom Sihanouk's regime with Son Ngoc Thanh as Prime Minister after proclaiming independence from France.
Kingdom of Laos   King Sisavang Vong's r gime after proclaiming independence from France.



Japan had made drafts for other dependent states.
The Provisional Priamurye Government has never got beyond the planning stages. In addition to the Japanese, the Germans supported the formation of this state.
In 1945, as the Second World War drew to a close, Japan planned to grant independence to the Netherlands East Indies (Indonesia). These plans ended when the Japanese surrendered on 15 August 1945.




Several European governments under the domination of Germany and Italy during World War II have been described as "puppet r gimes". The formal means of control in occupied Europe varied greatly. These states fall into several categories.



Romania (1940 1944)   The "National Legionary State" government of General (later Marshal) Ion Antonescu and Horia Sima's Iron Guard was an ally of Germany from 1940 1943. After Italy surrendered in September 1943 Romania became a puppet of Germany. The Iron Guard was an ultra-nationalist anti-Semitic Fascist movement.
Hungary's Government of National Unity (1944 1945)   The pro-Nazi r gime of Prime Minister Ferenc Sz lasi supported by the Arrow Cross Party was a German puppet r gime. Arrow Cross was a pro-German, anti-Semitic Fascist party. Sz lasi was installed by the Germans after Hitler launched Operation Panzerfaust and had the Hungarian Regent, Admiral Mikl s Horthy, removed and placed under house arrest. Horthy was forced to abdicate in favor of Sz lasi. Sz lasi fought on even after Budapest fell and Hungary was completely overrun.



Albania under Italy (1940 1943) and Albania under Nazi Germany (1943 1944)   The Kingdom of Albania was an Italian protectorate and puppet r gime. Italy invaded Albania in 1939 and ended the rule of King Zog I. Zog was exiled and King Victor Emmanuel III of Italy added King of Albania to his titles. King Victor Emmanuel and Shefqet Bej Verlaci, Albanian Prime Minister and Head of State, controlled the Italian protectorate. Shefqet Bej Verlaci was replaced as Prime Minister and Head of State by Mustafa Merlika Kruja on 3 December 1941. The Germans occupied Albania when Italy quit the war in 1943 and Ibrahim Bej Bi aku, Mehdi Bej Frash ri, and Rexhep Bej Mitrovica became successive Prime Minister under the Nazis.
Vichy France (1942 1944)   The Vichy French r gime of Philippe P tain had limited autonomy from 1940 to 1942, being heavily dependent on Germany. The Vichy government controlled many of France's colonies and the unoccupied part of France and enjoyed international recognition. In 1942, the Germans occupied the portion of France administered by the Vichy government and installed a new leadership, which ended much of the international legitimacy the government had.
Monaco (1943 1945)   In 1943, the Italian army invaded and occupied Monaco, setting up a fascist administration. Shortly thereafter, following Mussolini's collapse in Italy, the German army occupied Monaco and began the deportation of the Jewish population. Among them was Ren  Blum, founder of the Ballet de l'Opera, who died in a Nazi extermination camp.



Slovak Republic under the Slovak People's Party (1939 1945)   The Slovak Republic was a German client state. The Slovak People's Party was a clerofascist nationalist movement associated with the Roman Catholic Church. Monsignor Jozef Tiso became the president in a nominally independent Slovakia.
Independent State of Croatia (1941 1945)   The Independent State of Croatia (Nezavisna Dr ava Hrvatska or NDH) was a German and Italian puppet r gime. On paper, the NDH was a kingdom under King Tomislav II (Aimone, Duke of Spoleto) of the House of Savoy, but Tomislav II was only a figurehead in Croatia who never exercised any real power, with Ante Paveli  being a somewhat independent leader ("poglavnik"), though staying obedient to Rome and Berlin.



Greece (1941 1944)   The Hellenic State r gime of Georgios Tsolakoglou, Konstantinos Logothetopoulos and Ioannis Rallis was a "collaborationist" puppet government during the Axis occupation of Greece. Germany, Italy and Bulgaria occupied different portions of Greece at different times during these r gimes.
the Government of National Salvation (1941 1944)   The government of General Milan Nedi  and sometimes known as Nedi 's Serbia was a German puppet r gime operating in the Territory of the Military Commander in Serbia.
Lokot Republic, Russia (1941 1943)   The Lokot Republic under Konstantin Voskoboinik and Bronislaw Kaminski was a semi-autonomous region in Nazi-occupied Russia under an all-Russian administration. The republic covered the area of several raions of Oryol and Kursk oblasts. It was directly associated with the Kaminski Brigade and the Russian Liberation Army (Russkaya Osvoboditelnaya Narodnaya Armiya or RONA).
Belarusian Central Rada (1943 1944)   The Belarusian Central Council (Bie aruskaja Centralnaja Rada) was nominally the government of Belarus from 1943 1944. It was a collaborationist government established by Nazi Germany (see Reichskommissariat Ostland).
Quisling's Norwegian National government (1942 1945)   The occupation of Norway by Nazi Germany started with all authority held by German Reich Commissioner (Reichskommissar) Josef Terboven, who exercised this through the Reichskommissariat Norwegen. The Norwegian pro-German fascist Vidkun Quisling had attempted a coup d' tat against the Norwegian government during the German invasion on 9 April 1940, but he was not appointed by the Germans to head another native government until 1 February 1942.
Independent State of Croatia (1941 1945)   Formed after the invasion of Yugoslavia, the Independent State of Croatia was led by the Croatian fascist leader Ante Paveli . It controlled all of most of Croatia, Bosnia and Herzegovina, parts of Serbia, and parts of Slovenia. The government relied on German support for much of its existence.



The Italian Social Republic (1943 1945, known also as the Republic of Sal )   General Pietro Badoglio and King Victor Emmanuel III withdrew Italy from the Axis Powers and moved the government in southern Italy, already conquered by the Allies. In response, the Germans occupied northern Italy and founded the Italian Social Republic (Repubblica Sociale Italiana or RSI) with Italian dictator Benito Mussolini as its "Head of State" and "Minister of Foreign Affairs". While the RSI government had some trappings of an independent state, it was completely dependent both economically and politically on Germany.






Tuvinian People's Republic, also Tannu Tuva (1921 1944) Achieved independence from China by means of local nationalist revolutions only to come under the domination of the Soviet Union in the 1920s. In 1944, Tannu Tuva was absorbed into the Soviet Union.
Mahabad Republic (January 22, 1946   December 15, 1946) officially known as the Republic of Kurdistan and established on several provinces of northwestern Iran, or what is known as Iranian Kurdistan, was a short-lived republic that sought Kurdish autonomy within the limits of the Iranian state. Iran re-took control in December and the leaders of the state were executed in March 1947 in Mahabad.
Finnish Democratic Republic (1939 1940)   The Finnish Democratic Republic (Suomen Kansanvaltainen Tasavalta) was a short-lived republic in those parts of Finland that were occupied by the Soviet Union during the Winter War. The Finnish Democratic Republic was also known as the "Terijoki Government" (Terijoen hallitus) because Terijoki was the first town captured by the Soviets.
Karelo-Finnish Soviet Socialist Republic (1940)
Estonian Soviet Socialist Republic (1940)   In June 1940 the Republic of Estonia was occupied by the USSR and in July a government proclaimed Soviet power. In August 1940, Estonia was illegally annexed by the USSR.
Latvian Soviet Socialist Republic (1940)   In June 1940 the Republic of Latvia was occupied by the USSR and in July a government proclaimed Soviet power, In August 1940, Latvia was illegally annexed by the USSR.
Lithuanian Soviet Socialist Republic (1940)   In June 1940 the Republic of Lithuania was occupied by the USSR and in July a government proclaimed Soviet power, In August 1940, Lithuania was illegally annexed by the USSR.
Second East Turkestan Republic (1944 1949)   The Second East Turkestan Republic, usually known as the East Turkistan Republic (ETR), was a short-lived Soviet-backed separatist republic which existed in the 1940s in what is now the Xinjiang Uyghur Autonomous Region of the People's Republic of China.
Azerbaijan People's Government (1945 1946)- A short-lived state in Iranian Azerbaijan after WWII.
As Soviet forces prevailed over the German Army on the Eastern Front during the Second World War, the Soviet Union supported the creation of communist governments in Eastern Europe. Specifically, the People's Republics in Romania, Bulgaria, Hungary, and Poland were dominated by the Soviet Union. While all of these People's Republics did not "officially" take power until after World War II ended, they all have roots in pro-Communist war-time governments.
Poland (1944 1947)   The war-time governments under the Polish Committee of National Liberation, the Provisional Government of the Republic of Poland, and the Provisional Government of National Unity.
Bulgaria (1944 1946)   The war-time pro-Communist Fatherland Front government headed by Kimon Georgiev (Zveno).
Hungary (1944 1945)   The war-time government of Prime Minister B la Mikl s.
Romania (1945 1946)   The war-time National Front (FND) government under Premier Petru Groza. The FND was led by the Romanian Communist Party (PCR).



The Axis demand for oil and the concern of the Allies that Germany would look to the oil-rich Middle East for a solution, caused the invasion of Iraq by the United Kingdom and the invasion of Iran by the United Kingdom and the Soviet Union. Pro-Axis governments in both Iraq and Iran were removed and replaced with Allied-dominated governments.
Kingdom of Iraq (1941 1943)   Iraq was important to the United Kingdom because of its position on the route to India. Iraq also could provide strategic oil reserves. But, due to the UK's weakness early in the war, Iraq backed away from the pre-war Anglo-Iraqi Alliance. On 1 April 1941, the Hashemite monarchy in Iraq was over-thrown and there was a pro-German coup d' tat under Rashid Ali. The Rashid Ali regime began negotiations with the Axis powers and military aid was quickly sent to Mosul via Vichy French-controlled Syria. The Germans provided a squadron of twin engine fighters and a squadron of medium bombers. The Italians provided a squadron of biplane fighters. In mid-April 1941, a brigade of the 10th Indian Infantry Division landed at Basra (Operation Sabine). On 30 April, British forces at RAF Habbaniya were besieged by a numerically superior Iraqi force. On 2 May, the British launched pre-emptive airstrikes against the Iraqis and the Anglo-Iraqi War began. By the end of May, the siege of RAF Habbaniya was lifted, Falluja was taken, Baghdad was surrounded by British forces, and the pro-German government of Rashid Ali collapsed. Rashid Ali and his supporters fled the country. The Hashemite monarchy (King Faisal II and Prime Minister Nuri al-Said) was restored. The UK then forced Iraq to declare war on the Axis in 1942. Commonwealth forces remained in Iraq until 26 October 1947.
Imperial State of Iran (1941 1943)   German workers in Iran caused the United Kingdom and the Soviet Union to question Iran's neutrality. In addition, Iran's geographical position was important to the Allies. So, in August 1941, the Anglo-Soviet invasion of Iran (Operation Countenance) was launched. In September 1941, Reza Shah Pahlavi was forced to abdicate his throne and went into exile. He was replaced by his son Mohammad Reza Pahlavi. Mohammad Reza Pahlavi was willing to declare war on the Axis powers. By January 1942, the UK and the Soviet Union agreed to end their occupation of Iran six months after the end of the war.



In some cases, the process of decolonization has been managed by the decolonizing power to create a neo-colony, that is a nominally independent state whose economy and politics permits continued foreign domination. Neo-colonies are not normally considered puppet states.



Following Belgian Congo's independence as the Republic of the Congo (L opoldville) in 1960, Belgian interests supported the short-lived breakaway state of Katanga (1960 1963).



During the 1950 1953 Korean War, South Korea and the United States alleged that North Korea was a Soviet puppet state. At the same time, South Korea was accused of being an American puppet state by North Korea and its allies. Additionally, in 1951 Dean Rusk, the Assistant Secretary of State for Far Eastern Affairs, branded the People's Republic of China a "Slavic Manchukuo", implying that it was a puppet state of the Soviet Union just as Manchukuo had been a puppet state of the Empire of Japan. This position was commonly taken by American propaganda of the 1950s, despite the fact that the Chinese communist movement had developed largely independently of the Soviet Union.
Following the victory of the Viet Minh in the First Indochina War, the 1954 Geneva Accords stipulated that Vietnam would be divided for two years only, until national elections could be held. However, the Americans along with Ngo Dinh Diem feared that Ho Chi Minh and the Communists would win the election. The State of Vietnam and the United States didn't sign the Geneva Accords, citing that it was impossible to hold free and fair nationwide democratic elections in the communist North, and this was later expressed by UN observers monitoring the partition of Vietnam. As a result, South Vietnam and the U.S. were not bound by its terms. In 1955 the Vietnamese president Ngo Dinh Diem, supported by the United States, declared the independence of the Republic of Vietnam in the southern half of Vietnam. Over time, Diem grew increasingly uncomfortable with the role of the U.S. in his country, complaining that they were increasing the conflict with North Vietnam. Diem's complaints became more vocal as American soldiers, called "advisors", continued to pour into the country, and some began calling Diem an uncooperative client and a puppet pulling his own strings. After he became seen more as a liability than an asset to America, Diem was assassinated in 1963 with the complicity of the CIA and John F. Kennedy.
During the Vietnam War, South Vietnam was allied with the U.S. and other anti-communist states in Asia and the West, whereas North Vietnam was allied with China, and particularly the Soviet Union, and with other socialist and communist nations. The Paris Peace Accords were preceded by months of intensive negotiations over whether the National Front for the Liberation of Vietnam (Viet Cong) should be treated as an independent party or as a puppet of North Vietnam.



During the 1970s and 1980s, four ethnic bantustans, some of which were extremely fragmented, were carved out of South Africa and given nominal sovereignty. Two (Ciskei and Transkei) were for the Xhosa people; and one each for the Tswana people (Bophuthatswana) and for the Venda people (Venda Republic).
The principal purpose of these states was to remove the Xhosa, Tswana and Venda peoples from South African citizenship (and so to provide grounds for denying them democratic rights). All four were reincorporated into South Africa in 1994.






The Republic of Kuwait was a short-lived pro-Iraq state in the Persian Gulf that only existed three weeks before it was annexed by Iraq.



The Coalition Provisional Authority was a US-led administration in Iraq between 2003 and 2004.



The Republic of Crimea, a short-lived pro-Russian state before it was annexed by Russia in 2014.






Turkish Republic of Northern Cyprus' isolation and heavy dependence on Turkish support, Turkey has a high level of control over the country's decision-making processes. This has led to some experts stating that it runs as an effective puppet state of Turkey. Other experts, however, have pointed out to the independent nature of elections and appointments in Northern Cyprus and disputes between the Turkish Cypriot and Turkish governments, concluding that "puppet state" is not an accurate description for Northern Cyprus.



Republic of Abkhazia is sometimes considered a puppet state that depends on Russia. The economy of Abkhazia is heavily integrated with Russia and uses the Russian ruble as its currency. About half of Abkhazia's state budget is financed with aid money from Russia. Most Abkhazians have Russian passports. Russia maintains a 3,500-strong force in Abkhazia with its headquarters in Gudauta, a former Soviet military base on the Black Sea coast. The borders of the Republic of Abkhazia are being protected by the Russian border guards.
South Ossetia has declared independence but its ability to maintain independence is solely based on Russian troops deployed on its territory. As South Ossetia is landlocked between Russia and Georgia, from which it seceded, it has to rely on Russia for economic and logistical support, as its entire exports and imports and air and road traffic is only between Russia. Former President of South Ossetia Eduard Kokoity claimed he would like South Ossetia eventually to become a part of the Russian Federation through reunification with North Ossetia.
Transnistria   Created by Igor Smirnov and supported by Vladimir Antyufeyev and Alexander Lebed
Donetsk People's Republic   Created by Alexander Borodai and Igor Girkin
Lugansk People's Republic   Created by Valery Bolotov and Mikhail Kozitsyn



Nagorno-Karabakh Republic









James Crawford. The creation of states in international law (1979)