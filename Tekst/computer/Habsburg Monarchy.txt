The Habsburg Monarchy or Empire (occasionally also styled as the Austrian Monarchy and Danubian Monarchy) is an unofficial appellation among historians for the countries and provinces that were ruled by the junior Austrian branch of the House of Habsburg until 1780 and then by the successor branch of Habsburg-Lorraine until 1918. The Monarchy was a composite state composed of territories within and outside the Holy Roman Empire, united only in the person of the monarch. The dynastic capital was Vienna, except from 1583 to 1611, when it was moved to Prague. From 1804 to 1867 the Habsburg Monarchy was formally unified as the Austrian Empire, and from 1867 to 1918 as the Austro-Hungarian Empire.
The head of the House of Habsburg was often elected Holy Roman Emperor until the Empire's dissolution in 1806. The two entities were never coterminous, as the Habsburg Monarchy covered many lands beyond the Holy Roman Empire, and most of the Empire was ruled by other dynasties. The Habsburg Monarchy did not usually include all the territories ruled by the Habsburgs. The senior branch ruled Spain until 1700, but it is not usually included in the definition of "Habsburg Monarchy" after the reign of Charles V, who divided the dynasty between its Austrian and Spanish branches upon his abdication in 1556.



The Habsburg family originated with the Habsburg Castle in modern Switzerland, and after 1279 came to rule in Austria ("the Habsburg Hereditary Lands"). The Habsburg family grew to European prominence with the marriage and adoption treaty by Emperor Maximilian I at the First Congress of Vienna in 1515, and the subsequent death of adopted Louis II of Hungary and Bohemia in 1526.
Archduke Ferdinand of Austria, the younger brother of the Holy Roman Emperor Charles V, was elected the next King of Bohemia and Hungary following the death of Louis II of Hungary and Bohemia in the Battle of Moh cs against the Turks.



Names of the territory that (with some exceptions) finally became Austria-Hungary:
Habsburg monarchy or Austrian monarchy (1526 1867): This was an unofficial, but very frequent name   even at that time. The entity had no official name.
Austrian Empire (1804 1867): This was the official name. Note that the German version is Kaisertum  sterreich, i.e. the English translation empire refers to a territory ruled by an emperor, not just to a "widespreading domain".
Austria-Hungary (1867 1918): This was the official name. An unofficial popular name was the Danubian Monarchy (German: Donaumonarchie) also often used was the term Doppel-Monarchie ("Double Monarchy") meaning two states under one crowned ruler.
Crownlands or crown lands (Kronl nder) (1849 1918): This is the name of all the individual parts of the Austrian Empire (1849-1867), and then of Austria-Hungary from 1867 on. The Kingdom of Hungary (more exactly the Lands of the Hungarian Crown) was not considered a "crownland" after the establishment of Austria-Hungary 1867, so that the "crownlands" became identical with what was called the Kingdoms and Lands represented in the Imperial Council (Die im Reichsrate vertretenen K nigreiche und L nder).
The Hungarian parts of the Empire were called "Lands of the Holy Hungarian Crown of Saint Stephen" or "Lands of Holy (St.) Stephen's Crown" (L nder der Heiligen Stephans Krone). The Bohemian (Czech) Lands were called "Lands of the St. Wenceslaus' Crown" (L nder der Wenzels-Krone).
Names of some smaller territories:
Austrian lands ( sterreichische L nder) or "Archduchies of Austria" (Erzherzogt mer von  sterreich) - Lands up and below the Enns (ober und unter der Enns) (996 1918): This is the historical name of the parts of the Archduchy of Austria that became the present-day Republic of Austria (Republik  sterreich) on 12 November 1918 (after Emperor Charles I had abdicated the throne). Modern day Austria is a semi-federal republic of nine states (Bundesl nder) that are: Lower Austria, Upper Austria, Tyrol, Styria, Salzburg, Carinthia, Vorarlberg and Burgenland and the Capital of Vienna that is a state of its own. Burgenland came to Austria in 1921 from Hungary. Salzburg finally became Austrian in 1816 after Napoleonic wars (before it was ruled by prince-archbishops of Salzburg as a sovereign territory).
Vienna, Austria's capital became a state January 1, 1922, after being residence and capital of the Austrian Empire (Reichshaupt und Residenzstadt Wien) for the Habsburg monarchs for centuries. Upper and Lower Austria, historically, were split into "Austria above the Enns" and "Austria below the Enns" (the Enns river is the state-border between Upper- and Lower Austria). Upper Austria was enlarged after the Treaty of Teschen (1779) following the "War of the Bavarian Succession" by the so-called Innviertel ("Inn Quarter"), formerly part of Bavaria.
Hereditary Lands (Erblande or Erbl nder; mostly used  sterreichische Erblande) or German Hereditary Lands (in the Austrian monarchy) or Austrian Hereditary Lands (Middle Ages   1849/1918): In a narrower sense these were the "original" Habsburg Austrian territories, i.e. basically the Austrian lands and Carniola (not Galicia, Italian territories or the Austrian Netherlands).
In a wider sense the Lands of the Bohemian Crown were also included in (from 1526; definitely from 1620/27) the Hereditary lands. The term was replaced by the term "Crownlands" (see above) in the 1849 March Constitution, but it was also used afterwards.
The Erblande also included lots of small and smallest territories that were principalities, duchies or counties etc. some of them can namely be found in the reigning titles of the Habsburg monarchs like Graf (Earl/Count of) von Tyrol etc.




The territories ruled by the branch changed over the centuries, but the core always consisted of four blocs:
The Hereditary Lands, which covered most of the modern states of Austria and Slovenia, as well as territories in northeastern Italy and (before 1797) southwestern Germany. To these were added in 1779 the Inn Quarter of Bavaria; and in 1803 the Bishoprics of Trent and Brixen. The Napoleonic Wars caused disruptions where many parts of the Hereditary lands were lost, but all these, along with the former Archbishopric of Salzburg, which had previously been temporarily annexed between 1805 and 1809, were recovered at the peace in 1815, with the exception of the Vorlande. The Hereditary provinces included:
Archduchy of Austria (Upper Austria);
Archduchy of Austria (Lower Austria);
Duchy of Styria;
Duchy of Carinthia;
Duchy of Carniola;
The Adriatic port of Trieste;
Istria (although much of Istria was Venetian territory until 1797);
Gorizia and Gradisca;
These lands (3 8) were often grouped together as Inner Austria.

The County of Tyrol (although the Bishoprics of Trent and Brixen dominated what would become the South Tyrol before 1803);
The Vorarlberg (actually a collection of provinces, only united in the 19th century);
The Vorlande, a group of territories in Breisgau and elsewhere in southwestern Germany lost in 1801 (although the Alsatian territories (Sundgau) which had formed a part of it had been lost as early as 1648);
Vorarlberg and the Vorlande were often grouped together as Further Austria and mostly ruled jointly with Tyrol.

Grand Duchy of Salzburg (only after 1805);

The Lands of the Bohemian Crown   initially consisting of the five lands: Kingdom of Bohemia, March of Moravia, Silesia, and Upper and Lower Lusatia. Bohemian Diet (Czech: zemsk  sn m) elected Ferdinand I, Holy Roman Emperor as king in 1526.
Lusatia was ceded to Saxony in 1635.
Most of Silesia was conquered by Prussia in 1740 1742 and the remnants which stayed under Habsburg sovereignty were ruled as Duchy of Upper and Lower Silesia (Austrian Silesia).

The Kingdom of Hungary   two thirds of the former territory that was administered by the medieval Kingdom of Hungary was conquered by the Ottoman Empire and the Princes of vassal Ottoman Transylvania, while the Habsburg administration was restricted to the western and northern territories of the former kingdom, which remained to be officially referred as the Kingdom of Hungary. In 1699, at the end of the Ottoman-Habsburg wars, one part of the territories that were administered by the former medieval Kingdom of Hungary came under Habsburg administration, with some other areas being picked up in 1718 (some of the territories that were part of medieval kingdom, notably those in the south of the Sava and Danube rivers, remained under Ottoman administration).

Over the course of its history, other lands were, at times, under Austrian Habsburg rule (some of these territories were secundogenitures, i.e. ruled by other lines of Habsburg dynasty):
The Kingdom of Croatia (1527 1868);
The Kingdom of Slavonia (1699 1868);
The Grand Principality of Transylvania, between 1699 (Treaty of Karlowitz) and 1867 (Ausgleich)
The Austrian Netherlands, consisting of most of modern Belgium and Luxembourg (1713 1792);
The Duchy of Milan (1713 1797);
The Kingdom of Naples (1713 1735);
The Kingdom of Sardinia (1713 1720);
The Kingdom of Serbia (1718 1739);
The Banat of Temeswar (1718 1778);
Oltenia (1718 1739, de facto, 1737), as Grand-Voivodate (sometimes designated as Valachia Caesarea);
The Kingdom of Sicily (1720 1735);
The Duchy of Parma (1735 1748);
The Kingdom of Galicia and Lodomeria, in modern Poland and Ukraine (1772 1918)
Duchy of Bukovina (1774 1918);
New Galicia, the Polish lands, including Krak w, taken in the Third Partition (1795 1809);
Venetia (1797 1805);
Kingdom of Dalmatia (1797 1805, 1814 1918);
Kingdom of Lombardy-Venetia (1814 1859);
Krak w, which was incorporated into Galicia (1846 1918);
The Serbian Vojvodina (1848 1849); de facto entity, officially unrecognized
The Voivodeship of Serbia and Banat of Temeschwar (1849 1860);
The Kingdom of Croatia-Slavonia (1868 1918);
Sanjak of Novi Pazar occupation (1878 1913);
Bosnia and Herzegovina (1878 1918).
The boundaries of some of these territories varied over the period indicated, and others were ruled by a subordinate (secundogeniture) Habsburg line. The Habsburgs also held the title of Holy Roman Emperor between 1438 and 1740, and again from 1745 to 1806.



The various Habsburg possessions never really formed a single country each province was governed according to its own particular customs. Until the mid 17th century, all of the provinces were not even necessarily ruled by the same person junior members of the family often ruled portions of the Hereditary Lands as private apanages. Serious attempts at centralization began under Maria Theresa and especially her son Joseph II in the mid to late 18th century, but many of these were abandoned following large scale resistance to Joseph's more radical reform attempts, although a more cautious policy of centralization continued during the revolutionary period and the long Metternichian period which followed.
An even greater attempt at centralization began in 1849 following the suppression of the various revolutions of 1848. For the first time, ministers tried to transform the monarchy into a centralized bureaucratic state ruled from Vienna. The Kingdom of Hungary, in particular, ceased to exist as a separate entity, being divided into a series of districts. Following the Habsburg defeats in the Wars of 1859 and 1866, this policy was abandoned, and after several years of experimentation in the early 1860s, the famous Austro-Hungarian Compromise of 1867 was arrived at, by which the so-called Dual Monarchy of Austria-Hungary was set up. In this system, the Kingdom of Hungary was given sovereignty and a parliament, with only a personal union and a joint foreign and military policy connecting it to the other Habsburg lands. Although the non-Hungarian Habsburg lands, often, but erroneously, referred to as "Austria," received their own central parliament (the Reichsrat, or Imperial Council) and ministries, as their official name   the "Kingdoms and Lands Represented in the Imperial Council"   shows that they remained something less than a genuine unitary state. When Bosnia and Herzegovina were annexed (after a long period of occupation and administration), they were not incorporated into either half of the monarchy. Instead, they were governed by the joint ministry of finance.
Austria-Hungary collapsed under the weight of the various unsolved ethnic problems that came to a head with its defeat in World War I. In the peace settlement that followed, significant territories were ceded to Romania and Italy, new republics of Austria (the German-Austrian territories of the Hereditary lands) and Hungary (the Magyar core of the old kingdom) were created, and the remainder of the monarchy's territory was shared out among the new states of Poland, Kingdom of Serbs, Croats and Slovenes (later Yugoslavia), and Czechoslovakia.




The Habsburg monarchy should not be confused with various other territories ruled at different times by members of the Habsburg dynasty. The senior Spanish line of the Habsburgs ruled over Habsburg Spain and various other territories from 1516 until it became extinct in 1700. A junior line ruled over Tuscany between 1765 and 1801, and again from 1814 to 1859. While exiled from Tuscany, this line ruled at Salzburg from 1803 to 1805, and in W rzburg from 1805 to 1814. Another line ruled the Duchy of Modena from 1814 to 1859, while Empress Marie Louise, Napoleon's second wife and the daughter of Austrian Emperor Francis, ruled over the Duchy of Parma between 1814 and 1847. Also, the Second Mexican Empire, from 1863 to 1867, was headed by Maximilian I of Mexico, the brother of Emperor Franz Josef of Austria.



For a historical account, see:
History of Austria in the Habsburg Monarchy
History of Hungary under the Habsburg Monarchy
Kingdom of Bohemia: 1526 1648, 1648 1867, 1867 1918.
Kingdom of Croatia (Habsburg); Kingdom of Croatia-Slavonia; Kingdom of Dalmatia
Kingdom of Hungary (1538-1867)
Kingdom of Serbia: 1718 1739, 1788-1791
History of Slovakia within the Habsburg Monarchy
Economy of the Habsburg Monarchy
History of the Balkans







Charles V 1519-1520
Ferdinand I 1520 1564
Maximilian II 1564 1576
Rudolf II 1576 1612
Matthias 1612 1619
Ferdinand II 1619 1637
Ferdinand III 1637 1657
Leopold I 1657 1705
Joseph I 1705 1711
Charles VI 1711 1740 "Karl VI."
Maria Theresa 1740 1780 correctly written "Maria Theresia"



Joseph II 1780 1790 known as "the great Reformer"
Leopold II 1790 1792 from 1765 to 1790 "Grandduke of Tuscany"
Francis II 1792 1835 correctly written "Franz" (became Emperor Francis I of Austria in 1804, at which point numbering starts anew)
Ferdinand I 1835 1848 known as "Ferdinand the Good" German: "Ferdinand der G tige"
Francis Joseph I 1848 1916 Brother of Emperor Maximilian of Mexico (ruled 1864 1867)
Charles I 1916 1918 last reigning Monarch of Austria-Hungary
Otto von Habsburg-Lothringen or sometimes called Otto von  sterreich Crown Prince of Austria to be found as Otto von Habsburg



Habsburg family tree



The most famous memoir on the decline of the Habsburg Empire is Stefan Zweig's The World of Yesterday.


