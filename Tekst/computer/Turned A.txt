Turned A (capital:   or  , lowercase:   or  , math symbol  ) is a symbol based upon the letter A.
Lowercase   (in two story form) is used in the International Phonetic Alphabet to identify the near-open central vowel. A variant, turned alpha,  , is also used in the IPA as the open back rounded vowel.
It was used in the 18th century by Edward Lhuyd and William Pryce as phonetic character for the Cornish language. In their books, both   and   have been used.  It was used in the 19th century by Charles Sanders Peirce as a logical symbol for 'un-American' ("unamerican").
The symbol   has the same shape as a capital turned A, sans-serif. It is used to represent universal quantification in predicate logic. When it appears in a formula together with a predicate variable, they are referred to as a universal quantifier. In traffic engineering it is used to represent flow, the number of units (vehicles) passing a point in a unit of time.






List of logic symbols
List of mathematical symbols
Transformation of text
Universal quantification


