A permissive free software licence is a class of free software licence with minimal requirements about how the software can be redistributed. Such licenses therefore make no guarantee that future generations of the software will remain free. This is in contrast to licences which have reciprocity / share-alike requirements. Both sets of free software licences offer the same freedoms in terms of how the software can be used, studied, and privately modified. A major difference is that when the software is being redistributed (either modified or unmodified), permissive licences permit the redistributor to restrict access to the modified source code, while copyleft licenses do not allow this restriction. The term "permissive" as applied to software licensing is sometimes debatable in terms of specific terms and requirements, with occasional references to very weak copyleft being described as "permissive". A more narrowly constrained term related to permissive licensing is copyfree, which implies distinct licence term requirements analogous to, but different from, those of free software.
Well-known examples of permissive free software licences include the MIT License and the BSD licenses. A well known copyleft licence is the GNU General Public License.



Computer Associates Int'l v. Altai used the term "public domain" to refer to works that have become widely shared and distributed under permission, rather than work that was deliberately put into the public domain. However, permissive licences are not actually equivalent to releasing a work into the public domain.
Permissive licences often do stipulate some limited requirements, such as that the original authors must be credited (attribution). If a work is truly in the public domain, this is usually not legally required, but a United States copyright registration requires disclosing material that has been previously published, and attribution may still be considered an ethical requirement in academia.




Copyleft is "a general method for making a program or other work free, and requiring all modified and extended versions of the program to be free as well." By comparison with permissive licences, copyleft licensing places more requirement in terms of distribution and combination with software under other licences.



Copycenter is a term originally used to explain the modified BSD license, a permissive free software licence. The term was presented by Kirk McKusick, a computer scientist famous for his work on BSD, during one of his speeches at BSDCon 1999. It is a word play on copyright, copyleft and copy center.

The way it was characterized politically, you had copyright, which is what the big companies use to lock everything up; you had copyleft, which is free software's way of making sure they can't lock it up; and then Berkeley had what we called  copycenter , which is  take it down to the copy center and make as many copies as you want. 

The liberty to 'make as many copies as you want' is in fact also provided by all copyleft licences. However, unlike both copyleft licences and copyright law, permissive free software licences do not control the licence terms that a derivative work falls under.



Some permissive free software licences contain clauses that make them incompatible with copyleft licences. One example is clauses requiring advertising materials to credit the copyright holder. Licences with this type of advertising clause include the 4-clause BSD license, the PHP License, and the OpenSSL Licence.
Examples of permissive free software licences without advertising clauses are the MIT License, the 3-clause BSD license, and the Zlib License.
Some licences do not allow derived works to add a restriction that says a redistributor cannot add more restrictions. Examples include the CDDL and MsPL. However such restrictions also make the licence incompatible with permissive free software licences.




Free software license
Comparison of free and open-source software licenses


