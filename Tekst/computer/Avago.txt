Avago Technologies is a designer, developer and supplier of analog, digital, mixed signal and optoelectronics components and subsystems. Hock Tan is the company's president and CEO. Avago Technologies is incorporated in Singapore, and jointly headquartered in San Jose, California and Singapore.



The company was founded in 1961 as a semiconductor products division of HP. The division separated from Hewlett-Packard as part of Agilent Technologies in 1999. KKR and Silver Lake Partners acquired the division of Agilent Technologies in 2005 for $2.6 billion and formed Avago Technologies. Avago Technologies agreed to sell its I/O solutions unit to PMC-Sierra for $425 million in October 2005. In August 2008, the company filed an initial public offering of $400 million. In October 2008, Avago Technologies acquired Infineon Technologies' based bulk acoustic wave business for  21.5 million. In 2009, Avago Technologies went public on NASDAQ with the ticker symbol AVGO. Avago Technologies announced its agreement to acquire CyOptics, an optical chip and component supplier, for $400 million in April 2013. The acquisition aimed to expand Avago Technologies' fiber optics product portfolio. In October 2013, Avago Technologies invested $5 million in Amantys, power electronics technology provider, as part of a strategic investment agreement between the two companies. Avago Technologies announced its agreement to acquire LSI Corporation in December 2013 for $6.6 billion. The acquisition helped move Avago Technologies away from specialized products and towards a more mainstream industry, which included chips, especially storage for data centers.
The company sold its SSD controller business to Seagate in May 2014. In August 2014, the company was the ninth largest semiconductor company. Avago Technologies agreed to sell LSI's Axxia Networking business to Intel for $650 million. The company also agreed to buy PLX Technology, an integrated circuits developer, for $309 million. In Feb 2015, it was announced that Avago Technologies Limited had reached an agreement to acquire Emulex Corporation for $8 per share in cash.
On 28 May 2015, Avago announced that it would buy Broadcom for $37 Billion ($17 billion cash and $20 billion in shares). The combined company, which will be named Broadcom Ltd., will have annual revenue of $15 billion and a market value of $77 billion. Broadcom Corp. will strengthen Avago Technologies' patent position significantly in sectors such as mobile, the data center and the Internet of Things and would make the company the ninth largest holder of patents among the top semiconductor vendors, according to an analysis by technology consulting firm LexInnova.



The company's technologies include fiber optic, LED, LSI, motion control encoders, optical sensors, radio frequency and microwave. Avago Technologies also manufactures enterprise storage chips and PCIe switching and bridge chips.



Agilent
Verigy
HP
Broadcom


