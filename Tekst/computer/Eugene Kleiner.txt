Eugene Kleiner (12 May 1923   20 November 2003) was an Austrian-born American engineer and venture capitalist. He was one of the original founders of Kleiner Perkins, the Silicon Valley venture capital firm which later became Kleiner Perkins Caufield & Byers.
The company was an early investor in more than 300 information technology and biotech firms, including Amazon.com, AOL, Brio Technology, Electronic Arts, Flextronics, Genentech, Google, Hybritech, Intuit, Lotus Development, LSI Logic, Macromedia, Netscape, Quantum, Segway, Sun Microsystems and Tandem Computers.



He was born on 12 May 1923 in Vienna, Austria.
In 1938, he fled Nazi persecution of Jews with his family from Vienna, Austria, arriving in New York two years later. He served in the U.S. Army, then earned a Bachelor's degree in mechanical engineering from the Polytechnic University of New York in 1948 and a Master's degree in industrial engineering from New York University. After briefly teaching engineering, he joined Western Electric, the manufacturing arm of AT&T Corporation.
In 1947, he married the former Rose Wassertheil (died 2001), a Polish Jewish  migr e. They had two children, Robert Kleiner and Lisa.
In 1956, he was among the first to accept an offer from William Shockley to come to California to help form what became Shockley Semiconductor Laboratory. In 1957, he and seven colleagues (the "Fairchild eight", whom Shockley dubbed the "traitorous eight") left to found Fairchild Semiconductor, which most historians mark as the first major spin-off of what later was called Silicon Valley. According to fellow venture capitalist Arthur Rock, Kleiner led the eight, obtaining a $1.5 million investment from Sherman Fairchild and taking over the new firm's administrative duties.
Kleiner later invested his own money in Intel, a semiconductor firm founded in 1968 by fellow Fairchild founders Robert Noyce and Gordon Moore.
In 1972 he joined Hewlett-Packard veteran Tom Perkins to found Kleiner Perkins, the venture capital firm now headquartered on Sand Hill Road. In 1977, the company added Brook Byers and Frank J. Caufield as named partners. He retired from day-to-day responsibilities in the early 1980s.
He died on 20 November 2003 in Los Altos Hills, California.



There is a time when panic is the appropriate response.



Make sure the dog wants to eat the dog food. No matter how ground-breaking a new technology, how large a potential market, make certain customers actually want it.
Build one business at a time. Most business plans are overly ambitious. Concentrate on being successful in one endeavor first.
Risk up front, out early.
The time to take the tarts is when they're being passed.
The problem with most companies is they don't know what business they're in.
Even turkeys can fly in a high wind. In times of strong economies, even bad companies can look good.
It's easier to get a piece of an existing market than to create a new one.
It's difficult to see the picture when you're inside the frame.
After learning some of the tricks of the trade, some people think they know the trade. (This reflected some of Eugene's own humility; he recognized that many venture capitalists thought they were experts when they had just a bit of knowledge.)
Venture capitalists will stop at nothing to copy success.
Invest in people, not just products. (Eugene always respected founding entrepreneurs. He wanted to build companies with them not just with their ideas.)
When the money is available, take it.
There is a time when panic is the appropriate response.
The more difficult the decision, the less it matters what you choose.





