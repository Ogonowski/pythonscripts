An interactive kiosk is a computer terminal featuring specialized hardware and software designed within a public exhibit that provides access to information and applications for communication, commerce, entertainment, and education.
Early interactive kiosks sometimes resembled telephone booths, but can also be used while sitting on a bench or chair. Interactive kiosks are typically placed in high foot traffic settings such as hotel lobbies or airports.
Integration of technology allows kiosks to perform a wide range of functions, evolving into self-service kiosks. For example, kiosks may enable users to enter a public utility bill account number in order to perform an online transaction, or collect cash in exchange for merchandise. Customised components such as coin hoppers, bill acceptors, card readers and thermal printers enable kiosks to meet the owner's specialised needs.



The first self-service, interactive kiosk was developed in 1977 at the University of Illinois at Urbana-Champaign by a pre-med student, Murray Lappe. The content was created on the PLATO computer system and accessible by plasma touch screen interface. The plasma display panel was invented at the University of Illinois at Urbana-Champaign by Donald L. Bitzer. Lappe's kiosk, called The Plato Hotline allowed students and visitors to find movies, maps, directories, bus schedules, extracurricular activities and courses. When it debuted in the U of Illinois Student Union in April 1977, more than 30,000 students, teachers and visitors stood in line during its first 6 weeks, to try their hand at a "personal computer" for the first time.
The first successful network of interactive kiosks used for commercial purposes was a project developed by the shoe retailer Florsheim Shoe Co., led by their visionary executive VP, Harry Bock, installed circa 1985. The interactive kiosk was created, manufactured and customized by ByVideo Inc. of Sunnyvale, CA. The network of over 600 kiosks provided images and video promotion for customers who wished to purchase shoes that were not available in the retail location. Style, size and color could be selected, and the product paid for on the kiosk itself. The transaction was sent to the Florsheim mainframe in St, Louis, MO, via dialup lines, for next-day home or store delivery via Federal Express. The hardware (including microcomputer, display system, touchscreen) were designed and built by ByVideo, while other components (like the CRT, floppy disk, printer, keyboard and physical housing) were sourced from other vendors. The videodisc material was created quarterly by ByVideo at Florsheim's direction, in ByVideo's state-of-the-art video production facility in CA. This kiosk network operated for over 6 years in Florsheim retail locations.
In 1991, the first commercial kiosk with internet connection was displayed at Comdex. The application was for locating missing children. The first true documentation of a kiosk was the 1995 report by Los Alamos National Laboratory which detailed what the interactive kiosk consisted of. This was first announced on comp.infosystems.kiosks by Arthur the original usenet moderator.
In 1997, KioskCom was launched to provide a tradeshow for organizations looking to deploy interactive self-service kiosks, and continues to provide these services to this day. These tradeshows occur twice a year, and offer companies education and demonstrations for successful self-service deployments.
The first company to launch a statewide interactive kiosk program was Imperial Multimedia in 2007. Imperial Multimedia installed interactive kiosks in 31 of Virginia's State Parks and these electronic kiosks included park overviews, printable maps, waypoints, points of interest, video tours of trails, and emergency information.
Today's kiosks brings together the classic vending machine with high-tech communications and complex robotic and mechanical internals. Such interactive kiosks can include self-checkout lanes, e-ticketing, information and wayfinding, and vending. Electronic kiosks have become a larger part of the retail landscape. One such example of a strong retail kiosk business is Redbox, a movie rental kiosk company. Redbox is a subsidiary of Outerwall, Inc., another well known company popular for Coinstar kiosks.



The aesthetic and functional design of interactive kiosks is a key element that drives user adoption, overall up-time and affordability. There are many factors to consider when designing an interactive kiosk including:
Aesthetic design: The design of the enclosure is often the driving factor in user adoption and brand recognition.
Manufacturing volume: This will determine which manufacturing processes are appropriate to use (i.e. sheet-metal, thermoformed plastic, etc.).
Kiosk software: The interactive function of the kiosk hardware is largely determined by the software program and kiosk software configuration.
Graphic messaging: Plays a key role in communicating with potential users.
Maintenance and thermal design: Critical in order to maximize up-time (the time between failures or crashes).
Component specification: Typical components include Touch-screen, P.C., pointing device, keyboard, bill acceptor, mag-stripe and/ or bar-code scanner, surge protector, UPS, etc.
Ergonomic: Is important to ensure comfortable and easy user accessibility.
Regulatory compliance: In the US it is important to design to ADA. Electrical standards include UL in the U.S. and CE in Europe. In the retail space you have PCI certification in the U.S. which is descendant of VISA PED (relative of Chip and PIN in Europe).
Interface design: Designing for interactive kiosks typically requires larger buttons and simpler decision trees than designing for a web or computer based interactive. Catchy attractive animations and short dwell times are important.
Durability: The intended location of the kiosk will largely influence the construction as materials and electronic requirements are significantly different for indoor vs. outdoor kiosks.






Several countries have already implemented nationwide installation of kiosks for various purposes. One example of such large scale installations can be found in the United Kingdom, where thousands of special-purpose kiosks are now available to aid job-seekers in finding employment.
The United States Department of Homeland Security has created immigration kiosks where visitors register when they enter the United States. There are also Exit kiosks where visitors register when they leave the U.S.
Internally the U.S. government has institutions such as the Postal Service which use HR kiosks for their disconnected employees to update their training as well as monitor and maintain their benefits.
In India, digital kiosks are used for various purposes, such as payment of bills.



It is estimated that over 1.200.000 kiosk terminals exist in the U.S. and Canada alone.
Groups who use kiosks in their business environment include: Delta Airlines, United Airlines, JetBlue Airways, GTAA, Future Shop, The Home Depot, Target Corporation, and Wal-Mart.






The telekiosk can be considered the technical successor to the telephone booth, a publicly accessible set of devices that are used for communication. These can include email, fax, SMS, as well as standard telephone service. Telekiosk is rarely seen or heard anymore.
Telekiosks gradually appeared around the United Kingdom in the first years of the 21st century. Some are situated in shopping centres and transport terminals, with the intention of providing detailed local information. Others are in public places, including motorway service areas and airports.
The International Telecommunications Union is promoting the use of the telekiosk in Africa and parts of Asia where local people do not have access to communications technology. In part this work addresses the "digital divide" between rich and poor nations. There are, however, great practical benefits. The scheme in Bhutan aims to provide an E-Post system, whereby messages are relayed by telephone, then delivered by hand to rural areas, easing the problems of transporting letters across the countryside. Health, agricultural and educational information is also available.



The financial services kiosk can provide the ability for customers to perform transactions that may normally require a bank teller and may be more complex and longer to perform than desired at an ATM. These are sometimes to referred to as "bank-in-a-box" and the first prime example would be the Vcom units deployed in 7-11 in U.S.
These units are generally referred to 'multi-function financial service kiosks' and the first iteration was back in late 1990s with the VCOM product deployed in Southland (7-Eleven) convenience stores. Check-cashing, bill-payment and even dispensing cashcards. New multi-function machines have been deployed in "c-store" markets supported by Speedway and others.
By 2010 the largest bill pay kiosk network is AT&T for the phone customers which allows them customers to pay their phone bill. Verizon and Sprint have similar units for their customers



An interactive kiosk which allows users to print pictures from their digital images. The marquee example began with Kodak who had at one point had over 100,000 units up and running in the U.S. Many of these units were customized PC's with an LCD which would then print to central printer in Customer service. Two major classes of photo kiosks exist:
Digital Order Stations -- This type of photo kiosk exists within retail locations and allows users to place orders for prints and photographic products. Products typically get produced in-store by a digital minilab, or at another location to be shipped directly to the consumer, or back to the store to be picked up at a later time. Digital Order Stations may or may not support instant printing, and typically do not handle payments.
Instant Print Stations - This type of photo kiosk uses internal printers to instantly create photographic prints for a self serve paying customer. Often located in public locations (hotels, schools, airports), Instant Print Stations handle payments. Often such systems will only print 4x6 inch prints, although popular dye sublimation photo printers as of 2008 allow for 4x6, 5x7, 8x10, 8x12. It's more a matter of resupply labor economics and chassis size.



An Internet kiosk is a terminal that provides public Internet access. Internet kiosks sometimes resemble telephone booths, and are typically placed in settings such as hotel lobbies, long-term care facilities, medical waiting rooms, apartment complex offices, or airports for fast access to e-mail or web pages. Internet kiosks sometimes have a bill acceptor or a credit card swipe, and nearly always have a computer keyboard, a mouse (or a fixed trackball which is more robust), and a monitor.
Some Internet kiosks are based on a payment model similar to vending machines or Internet caf , while others are free. A common arrangement with pay-for-use kiosks has the owner of the Internet kiosk enter into a partnership with the owner of its location, paying either a flat rate for rental of the floor space or a percentage of the monthly revenue generated by the machine.
Internet kiosks have been the subject of hacker activity. Hackers will download spyware and catch user activity via keystroke logging. Other hackers have installed hardware keystroke logging devices that capture user activity.
Businesses that provide Internet kiosks are encouraged to use special Internet kiosk software and management procedures to reduce exposure to liability.




Many amusement parks such as Disney have unattended outdoor ticketing kiosks. Amtrak has automated self-service ticketing kiosks. Busch Gardens uses kiosks for amusement parks. Cruise ships use ticketing kiosks for passengers. Check-in Kiosks for auto rental companies such as Alamo and National have had national deployments. The ticket halls of train stations and metro stations have ticketing kiosks that sell transit passes, train tickets, transit tickets, and train passes.



Many movie theater chains have specialized ticket machines that allow their customers to purchase tickets and/or pick up tickets that were purchased online. Radiant and Fujitsu have been involved in this segment.



A new way to order in-cafe from tablet kiosks. Kiosks are available in addition to cashier stations so that wait time is reduced for all guests. The kiosk is highly visual and includes a product builder to assist with order accuracy and customization. 



An example of a vending kiosk is that of the DVD rental kiosks manufactured by several manufacturers, where a user can rent a DVD, secured by credit card for $1 per day.
Beginning in 2002, new vending kiosks have started to be deployed which dispense a variety of items including electronic device and cosmetics.



A visitor management and security kiosk can facilitate the visitor check in process at businesses, schools, and other controlled access environments. These systems can check against blacklists, run criminal background checks, and print access badges for visitors. School security concerns in the United States have led to an increase in these types of kiosks to screen and track visitors.



Many shopping malls, hospitals, airports and other large public buildings use interactive kiosks to allow visitors to navigate in buildings. Harris County Hospital District, Baptist Hospital of Miami, the Children's Hospital of Philadelphia and the Cayuga Medical Center are but a few medical centers utilizing interactive touch screen kiosks with a building directory and wayfinding solution.



Hospitals and medical clinics are looking to kiosks to allow patients to perform routine activities. Kiosks that allow patients to check in for their scheduled appointments and update their personal demographics reduce the need to line up and interact with a registration clerk. In areas where patients must make a co-pay, kiosks will also collect payment. As the requirements for documentation, waivers and consent increase, kiosks with integrated signature capture devices are able to present the documentation to the patient and collect their signature. A business case for registration and check-in kiosks is built around:
workload reduction,
data quality improvements,
consistency of registration process, and
patient experience improvement.
A large community hospital has been able to reduce their registration staff by 30%, improve data quality, and shorten lineups.



Museums, historical sites, national parks and other tourist/visitor attractions often engage kiosks as a method for conveying information about a particular exhibit or site. Kiosks allows guests to read about - or view video of - particular artifacts or areas at their own pace and in an interactive manner, learning more about those areas that interest them most. The Rockwell Museum in New York uses touchscreen tablets to provide visitors with accessible and relevant labels for a particular exhibit. The Penn State All sports museum employs interactive kiosks to display up to date information about past and current Penn State athletes and sports teams. The Ellis Island National Museum of Immigration now boasts a citizen test available for visitors to take online via an informational kiosk. Additional kiosk displays include a  Threads of Migration  interactive exhibition featuring three touch-screen kiosks as part of  The Journey: New Eras of Immigration" section, which covers immigration since 1954.



Reliability is an important consideration, and as a result many specialised kiosk software applications have been developed for the industry. These applications interface with the bill acceptor and credit card swipe, meter time, prevent users from changing the configuration of software or downloading computer viruses and allow the kiosk owner to see revenue. Threats to reliability come from vulnerabilities to hacking, allowing access to the OS, and the need for a session or hardware restart.



The kiosk industry is divided into three segments: kiosk hardware, kiosk software, and kiosk application. Kiosk software locks down your operating system (be it Apple, Windows, Android, or Linux) to restrict access and/or functionality of a kiosk hardware device. This allows for users to interact with an application that serves a self-service purpose such as those mentioned above.



Historically electronic kiosks though are standalone enclosures which accept user input, integrate many devices, include a software GUI application and remote monitoring and are deployed widely across all industry verticals. This is considered "Kiosk Hardware" within the kiosk industry.
POS-related "kiosks" are "lane busting" check-outs such as seen at large retailers like Home Depot and Kroger.
Simple touchscreen terminals or panel-pcs are another segment and enjoy most of their footprint in POS retail applications and typically facing the employee. Terminals include NCR Advantage (740x terminal) and the IBM Anyplace computer terminal. These units are considered "kiosks" only in functionality delivered and typically only incorporate touchscreen, bar code scanner and/or magnetic stripe reader.
Market segments for kiosk and self-service terminal manufacturers include photo kiosks, government, airlines, internet, music, retail loyalty, HR and financial services, just to name some.



This segment includes healthcare patient check-in and "take a number" type custom flow. Devices range from simple ticket dispense to biometrics (fingerprint readers) for patient check-in.









This article is based on material taken from the Free On-line Dictionary of Computing prior to 1 November 2008 and incorporated under the "relicensing" terms of the GFDL, version 1.3 or later.