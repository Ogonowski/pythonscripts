The National Council (German: Nationalrat, French: Conseil National, Italian: Consiglio Nazionale, Romansh: Cussegl Naziunal) is the lower house of the Federal Assembly of Switzerland. With 200 seats, it is the larger of the two houses.
Members are elected by adult citizens, along with the Swiss Council of States. 4.6 million citizens were eligible to vote in 2003. The members, called 'National Councillors', serve four-year terms.



Each of Switzerland's 26 cantons is a constituency. The number of deputies for each constituency depends on the canton's population, but each canton has at least one deputy.
Each canton then uses a unique system of proportional representation, sometimes called a "free list". Each citizen may cast a vote for any candidate for every seat available to their constituency, with up to one candidate being voted for twice. For every vote received by a candidate, that candidate's party also receives a vote. Voters also list a party vote, in which all blank candidate votes contribute towards the parties total.
The seats are then apportioned using the Hagenbach-Bischoff System. This system is unique in that it allows voters to split their vote across different parties, depending on which candidate the voter prefers.



To determine a party's strength, the notion of "fictional voter" was introduced and is defined by the Swiss Federal Statistical Institute as: number of votes obtained by party A * (number of valid ballots / number of valid votes). Individual voters can choose to make fewer than the permissible number of votes. The number of valid votes / number of valid ballots closely match the number of deputies a canton needs to elect. More exactly, this number represents the average number of valid votes per voter. The formula can then be summed up by: number of votes obtained by party A / average of valid votes per voters.
The result is the number of fictional voters for a given party in a given canton. A total number of fictional voters can then be established and the party strength can be deduced.
The number of deputies in each party is determined at the cantonal level using proportional representation with the Hagenbach-Bischoff system (except in single-member cantons.) The election's turnout is computed as: number of valid ballots cast / number of registered voters.




The National Council election in 2011 resulted in the strengthening of the political center and the reversing of the trend towards polarisation in Swiss politics that took place during the 1990s and 2000s. During this election, centrist parties gained about 7% of the popular vote, with the right pole losing 3.6% and the left pole losing 3.5%. Voter turnout was 48.5%, compared to 48.3% in 2007.



Foreign Affairs Committee (FAC)
Committee for Science, Education and Culture (CSEC)
Committee for Social Security and Health (CSSH)
Committee for the Environment, Spatial Planning and Energy (CESPE)
Defence Committee (DefC)
Committee for Transportation and Telecommunications (CTT)
Committee for Economic Affairs and Taxation (CEAT)
Political Institutions Committees (PIC)
Committee for Legal Affairs (CLA)
Committee for Public Buildings (CPB)



Finance Committee (FC)
Control Committees (CC)
Parliamentary investigation committees (PIC)



Committee on Pardons
Rehabilitation Committee
Drafting Committee
Judicial Committee





