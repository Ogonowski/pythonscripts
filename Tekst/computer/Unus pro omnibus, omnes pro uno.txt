Unus pro omnibus, omnes pro uno is a Latin phrase that means "One for all, all for one" in English.



Switzerland has no official motto defined in its constitution or legislative documents. The phrase, in its German (Einer f r alle, alle f r einen), French (un pour tous, tous pour un), Italian (Uno per tutti, tutti per uno) and Romansh (In per tuts, tuts per in) versions, came into widespread use in the 19th century. After autumn storms had caused widespread floods in the Alps in late September and early October 1868, officials launched an aid campaign under that slogan, deliberately using it to evoke a sense of duty and solidarity and national unity in the population of the young nation Switzerland had become a federal state only 20 years earlier, and the last civil war among the cantons, the Sonderbundskrieg, had been in 1847. Newspaper ads that used the motto to call for donations were run in all parts of the country. The phrase was increasingly associated with the founding myths of Switzerland, which often also have solidarity as a central theme, to such a degree that "Unus pro omnibus, omnes pro uno" was even written in the cupola of the Federal Palace of Switzerland in 1902. It has since been considered the motto of the country. Politicians of all parties and regions acknowledge it as the motto of Switzerland.




One for all, and all for one (Un pour tous, tous pour un; also inverted to All for one, and one for all) is a motto traditionally associated with the titular heroes of the novel The Three Musketeers written by Alexandre Dumas p re, first published in 1844. In the novel, it was the motto of a group of French musketeers named Athos, Porthos, Aramis and d'Artagnan who stayed loyal to each other through thick and thin.
On November 30, 2002, in an elaborate but solemn procession, six Republican Guards carried the coffin of Dumas from its original interment site in the Cimeti re de Villers-Cotter ts in Aisne to the Panth on. The coffin was draped in a blue-velvet cloth inscribed with the motto.



In a meeting in 1618 between leaders of the Bohemian Catholic and Protestant communities, which resulted in the defenestrations of Prague, a representative of the Protestants read a letter affirming that, "As they also absolutely intended to proceed with the execution against us, we came to a unanimous agreement among ourselves that, regardless of any loss of life and limb, honour and property, we would stand firm, with all for one and one for all... nor would we be subservient, but rather we would loyally help and protect each other to the utmost, against all difficulties."


