T-Mobile International AG is a German holding company for Deutsche Telekom AG's various mobile communications subsidiaries outside Germany. Based in Bonn, Germany, its subsidiaries operates GSM, UMTS and LTE-based cellular networks in Europe, the United States, Puerto Rico, and the U.S. Virgin Islands. The company has financial stakes in mobile operators in both Central and Eastern Europe.
The T-Mobile brand is present in 12 European countries   Austria, Croatia (as Hrvatski telekom), Czech Republic, Germany (as Telekom), Hungary, Macedonia, Montenegro, the Netherlands, Poland, Romania, Slovakia, and the United Kingdom, as well as the United States, Puerto Rico, and the U.S. Virgin Islands.
Globally, T-Mobile International subsidiaries have a combined total of approximately 230 million subscribers. T-Mobile International is the world's fifteenth-largest mobile-phone service provider by subscribers and the fourth-largest multinational after the UK's Vodafone, India's Airtel, and Spain's Telef nica.



Germany's first mobile-communications services were radiotelephone systems that were owned and operated by the state postal monopoly, Deutsche Bundespost. It launched the analog first-generation C-Netz ("C Network", marketed as C-Tel), Germany's first true mobile phone network in 1985.
On July 1, 1989, West Germany reorganized Deutsche Bundespost and consolidated telecommunications into a new unit, Deutsche Bundespost Telekom. On July 1, 1992, it began to operate Germany's first GSM network, along with the C-Netz, as its DeTeMobil subsidiary. The GSM 900 MHz frequency band was referred to as the "D-Netz", and Telekom named its service D1; the private consortium awarded the second license (now Vodafone Germany) chose the name D2.
Deutsche Bundespost Telekom was renamed Deutsche Telekom in 1995, and began to be privatized in 1996. That same year, DT began to brand its subsidiaries with the T- prefix, renaming the DeTeMobil subsidiary T-Mobil.
In 2002, as DT consolidated its international operations, it anglicized the T-Mobil name to T-Mobile.
On April 1, 2010, the T-Home and T-Mobile German operations merged to form a new wholly owned DT subsidiary, Telekom Deutschland GmbH. The T-Mobile brand was discontinued in Germany and replaced with the Telekom brand. The T-Mobile brand is still used in markets outside Germany. Non-German mobile-network assets are organized into various country-specific subsidiaries under the T-Mobile International AG subsidiary of DT.
In 2010, T-Mobile UK became part of a joint venture with France T l com's UK mobile-network provider, Orange (UK). Combined, the two companies make the UK's largest mobile-network operator, called EE. Despite the joint venture, the T-Mobile and Orange brands continue to co-exist in the UK market.
In July 2014, Telekom group had bought the Romanian companies Romtelecom and Cosmote, acquiring almost 40 percent of the country's shares.









Until 2000, T-Mobile was a shareholder of the former max.mobil. network. In April 2001, it acquired one hundred percent and subsequently introduced the T-Mobile brand in Austria by rebranding max.mobil. in April 2002 as T-Mobile Austria.
In 2005, it acquired former competitor tele.ring from Western Wireless International. It is now used as a discount brand. tele.ring is an Austrian mobile network operator. Since it was bought by T-Mobile in 2006, it is no longer a legally independent company. Tele.ring is administratively independent and now acts primarily as a discount-offer, similar to Yesss and BoB of A1. In the past, tele.ring was known for their aggressive price-politics.



T-Mobile entered the Croatian market in October 1999 when DT initially acquired a thirty-five percent interest in Hrvatski telekom, including its cell phone service provider Cronet. Two years later, DT signed an agreement with the Croatian government to acquire the additional 16 percent needed for a majority holding. In January 2003, Hrvatski Telekom assembled all of its mobile activities under a single brand HTmobile. Finally, in October 2004, HTmobile became T-Mobile Hrvatska, or T-Mobile Croatia, thus joining the global T-Mobile family also by name. Since January 1, 2010, Hrvatski Telekom and T-Mobile Croatia merged into one company on the Croatian market under the name Hrvatski Telekom (in English: Croatian Telecom); the T-Mobile brand remained active in the mobile-business area and T-Com in the fixed-business area until 2013 when they were replaced by unified brand "Hrvatski Telekom".



T-Mobile was previously known as Paegas in the Czech Republic. T-Mobile Czech Republic a.s. has been operating in the Czech market since 1996.  As of May 30, 2008, 5.4 million customers were using T-Mobile services.
T-Mobile Czech Republic a.s. operates a public mobile communications network on the GSM standard in the 900 and 1800 MHz bands and is also authorized to operate a UMTS network. On October 19, 2005, T-Mobile was the first operator in the Czech Republic to launch this third-generation technology under the name Internet 4G.




Germany's initial mobile communications services were radiotelephone systems that were owned and operated by the state postal monopoly, Deutsche Bundespost. These early mobile communications networks were referred to as the "A" and "B" networks.
Deutsche Bundespost Telekom built Germany's first cellular mobile network, an analog, first-generation system referred to as the "C" network or C-Netz. The network became operational in 1985 and services were marketed under the C-Tel brand. Following German reunification in 1990, the "C" network was extended to the former East Germany.
On July 1, 1992, Deutsche Bundespost Telekom's DeTeMobil subsidiary began operating Germany's first GSM 900 MHz frequency cellular network, which the organization referred to as D-Netz. Digital GSM services were marketed under the "D1" brand and DeTeMobil continued to sell analog cellular services concurrently under the existing C-Tel brand. In 1994, DeTeMobil introduced short message service (SMS) services.
In 1996, DT began to brand its subsidiaries with the T- prefix, renaming the DeTeMobil subsidiary T-Mobil and rebranding the GSM cellular network T-D1. C-Netz was renamed to T-C-Tel. The T-C-Tel / C-Netz services were fully discontinued in 2000.
D1 introduced prepaid service called Xtra in 1997.
Despite the numerous changes in subsidiary names and brands, Germans sometimes continue to use the T-D1 name within Germany and refer to T-Mobile as D1. On April 1, 2010, after the T-Home and T-Mobile German operations merged to form Telekom Deutschland GmbH, a wholly owned DT subsidiary; the T-Mobile brand was discontinued in Germany and replaced with the Telekom brand.
In T-Mobile's home market of Germany, it is the largest mobile-phone operator with almost 38 million subscribers. (As of December 2013) and provides up to LTE-Cat4 with 300 Mbit/s.



On May 1, 2004, the same day as Hungary joined the European Union, the former company, named Westel (which was owned entirely by the former Mat v) changed its name, and the entire marketing. Westel was the most popular cellphone network in Hungary at the time. The company was called T-Mobile Hungary, but after some financial decisions, as with the other T- companies, it formed to Magyar Telekom Nyrt. Mobil Szolg ltat sok  zlet g (Hungarian Telekom, Mobile Services Business Unit), but they still say T-Mobile. T-Mobile also provides high-speed services, like EDGE, 3G, and HSDPA in Hungary's major cities. Since January 2012 the company provides LTE on the 1.8 GHz frequency.




In Republic of Macedonia, T-Mobile was previously known as Mobimak. The company has been operating in the Macedonian market since 1996. On September 7, 2006, Mobimak accepted the international T-Mobile branding. By June 2007, T-Mobile reached one million subscribers, out of which 85 percent were active and using their services. T-Mobile MK covers 98 percent of the population. It has a GSM 900 license, offers GPRS, MMS and mobile internet services using T-Mobile HotSpots and has implemented the EDGE fast mobile internet specification. T-Mobile Macedonia applied for a UMTS license on August 1, 2007. From July 1 T-Mobile ceased to exist as a legal entity and was replaced by the Telekom brand. The carrier name is now Telekom.mk. The codes are 070/071/072.



The T-Mobile brand entered the Montenegrin market in 2006 through the acquisition of MoNet GSM mobile provider. T-Mobile Montenegro (T-Mobile Crna Gora) is fully owned by T-Crnogorski Telekom, which is itself owned by Magyar Telekom, a DT subsidiary. Although the acquisition by Magyar Telekom was done in 2005, it was not until September 26, 2006, that the MoNet GSM operator was re-branded as T-Mobile Montenegro.
MoNet GSM launched on July 1, 2000, as part of Telecom Montenegro. It became an independent incorporated limited-liability company a month later, on August 1, 2000. The company currently holds around 34 percent of the Montenegrin market and uses GSM 900, GPRS, and EDGE technologies. Since June 21, 2007, 3G/UMTS services have been available in larger cities as well as on the coast.



T-Mobile (Deutsche Telekom) entered the Dutch market by the acquisition of Ben on September 20, 2002. In 2007, T-Mobile Netherlands, a wholly owned subsidiary of T-Mobile International, acquired Orange Netherlands from France T l com for EUR 1.33 billion. This makes it the third largest mobile telephone operator in the country behind KPN and Vodafone.



T-Mobile announced in May 2010 that it was dealing with major capacity problems on its 3G network. T-Mobile admitted the problems after much pressure from customers and the Dutch media. T-Mobile could not keep up with the growing data demand from smartphones, caused by the number of new customers who wanted an iPhone: T-Mobile in the Netherlands failed to keep up with the demand, and capacity problems on the network were the result. T-Mobile denied the problems at first by telling complaining customers that their mobile phone or SIM-card was causing the problem.
The capacity problems occurred mostly in cities and densely populated areas. When affected, people could experience problems with calling or receiving calls, text messaging (SMS), or data services. A substantial number of customers were not able to use any of these services in cities or urban areas when the network capacity was overloaded, for instance the cities of Amsterdam and Utrecht were heavily impacted.
After being put under pressure by several consumer interest groups and the Dutch media, T-Mobile started a cash-back settlement for all consumers who had complained about failing communication services.
T-Mobile invested tens of millions of euros to upgrade its network. The upgrade was to have been completed by the end of first quarter of 2011.




T-Mobile (Poland) serves over thirteen million customers, and owns licenses for 900, 1800, 2100 MHz bands which are used for GSM, WCDMA and LTE. Formerly Era, rebranding took place  on June 5, 2011. T-Mobile Poland with Orange Polska have consolidated their infrastructure and used this opportunity to roll out 3G coverage using 900 MHz band.



The T-Mobile brand entered the Slovak market in May 2005, after rebranding the EuroTel network from Eurotel Bratislava to T-Mobile Slovensko nowadays Telekom. The company Eurotel Bratislava was partially owned by Slovak Telekom, an incumbent fixed-line operator, which later acquired a one hundred percent stake in Eurotel Bratislava. T-Mobile International and DT never owned T-Mobile Slovensko directly; DT is partially owner of Slovak Telekom and thus T-Mobile International has procurement managing function within T-Mobile Slovensko. On July 1, 2010, Slovak Telekom and T-Mobile Slovensko merged into one company on the Slovak market under the name Telekom; T-Mobile brand no more remains active in the mobile-business area, as well as the T-Com in the fixed-business area.
The Telekom network provides services on three networks GSM (900/1800 MHz), UMTS (2100 MHz), Flash OFDM (450 MHz). Mobile data services are provided on 4G, 3G, GSM network with EDGE extension and on UMTS with DC-HSPA+ 42 Mbit/s and HSUPA 5,8 Mbit/s. Flash OFDM is one of two commercially successfully launched solely data networks in the world. It supports upload speed up to 5.8 Mbit/s.




T-Mobile UK started life as Mercury One2One, the world's first GSM 1800 mobile network. It was originally operated by the now-defunct Mercury Communications. Later known simply as One 2 One, it was purchased by DT in 1999 and rebranded as T-Mobile in 2002.
T-Mobile offers both pay-as-you-go and pay-monthly contract phones. The pay-monthly contracts consists of set amounts of minutes and "flexible boosters", which allow the customer to change them month to month depending on their needs. T-Mobile launched their 3G UMTS services in the Autumn of 2003.
In late 2007, it was confirmed that a merger of the high-speed 3G and HSDPA networks operated by T-Mobile UK and 3 (UK) was to take place starting January 2008. This left T-Mobile and 3 with the largest HSDPA mobile phone network in the country.
In 2009, France T l com's Orange and DT, T-Mobile's parent, announced they were in advanced talks to merge their UK operations to create the largest mobile operator. In March 2010, the European Commission approved this merger on the condition that the combined company sell 25% of the spectrum it owns on the 1800 MHz radio band and amend a network sharing agreement with smaller rival 3. The merger was completed the following month, the new company's name later being announced as EE. Orange and T-Mobile will continue as separate brands in the market for at least eighteen months, both run by the new parent company.
T-Mobile UK's network is also used as the backbone network behind the Virgin Mobile virtual network.



1 Deutsche Telekom controls 40 percent plus one of the shares of parent company OTE.
2 Deutsche Telekom controls 51 percent of the shares of parent company T-Hrvatski Telekom which holds 39.1 percent of HT.




T-Mobile US, Inc. is the Bellevue, Washington, United States-based subsidiary of T-Mobile International AG. It provides wireless voice, messaging, and data services in the United States, Puerto Rico and the U.S. Virgin Islands under the T-Mobile, MetroPCS, and GoSmart Mobile brands. The company operates the third-largest wireless network in the U.S. market with 58.9 million customers and annual revenues of $21.35 billion. Its nationwide network reaches 96 percent of Americans. As of 2011, J. D. Power and Associates, a global marketing-information-services firm, ranked the company highest among major wireless carriers for retail-store satisfaction four years consecutively and highest for wireless customer care two years consecutively.
The company owns licenses to operate a 1900 MHz GSM PCS digital cellular network and a 1700 MHz UMTS AWS digital cellular network that cover areas of the continental U.S., Alaska, Hawaii, Puerto Rico and the U.S. Virgin Islands. It provides coverage in areas where it does not own radio frequency spectrum licenses via roaming agreements with other operators of compatible networks. In addition to its cellular mobile network, T-Mobile US operates a nationwide Wi-Fi Internet-access network under the T-Mobile HotSpots brand. The T-Mobile HotSpot service offers access to a nationwide network of approximately 8,350 access points, installed in venues such as Starbucks coffeehouses, FedEx Office Office and Print Centers, Hyatt hotels and resorts, Red Roof Inns, Sofitel hotels, Novotel hotels, the airline clubs of American Airlines, Delta Air Lines, United Airlines and US Airways, and airports.
T-Mobile US, Inc. traces its roots to the 1994 establishment of VoiceStream Wireless PCS as a subsidiary of Western Wireless Corporation. Spun off from parent Western Wireless on May 3, 1999, VoiceStream Wireless Corporation was purchased by DT on May 31, 2001, for $35 billion and renamed T-Mobile USA, Inc. in July 2002. This legacy is reflected in some mismatch between US and German T-Mobile service, notably the frequency mismatch making phones inoperative in the other country, and picture messaging issues (non-delivery of pictures in text messages) between those networks.
After a failed attempt by AT&T in 2011 to purchase the company in an $39 billion stock and cash offer (which was withdrawn after being faced with significant regulatory and legal hurdles, along with heavy resistance from the U.S. government), T-Mobile USA announced its intent to merge with MetroPCS Communications, Inc., the sixth largest carrier in the U.S., to improve its competitiveness with other national carriers; the deal was approved by the Department of Justice and Federal Communications Commission in March 2013. The merger agreement gave Deutsche Telekom the option to sell its 72% stake in the merged company, valued at around $14.2 billion, to a third-party before the end of the 18-month lock-up period. On May 1, 2013, the combined company, now known as T-Mobile US, began trading on the New York Stock Exchange as a public company.



In 2013, T-Mobile US began a process of re-branding their corporate image into the unCarrier. The beginning of this process was the eradication of two-year contracts. Instead, the company will charge their users an additional amount of money per month, in addition to the down-payment of their chosen device. For example, the company proclaimed they offered the iPhone for less money down than any other carrier at the time, and in exchange for being locked into a contract, users would pay an extra $20 per month, for as long as it takes to fulfill the full cost of the phone. 
Started primarily to increase T-Mobile profitability in the US, where AT&T and Verizon were strong, through the unCarrier initiative T-Mobile has begun fostering competition, drawing more customers to their service and away from the telecom giants at the time.  Only a month after T-Mobile introduced the Data Stash, their service which allows users to roll-over unused data, AT&T announced a very similar service, Data Rollover.  T-Mobile US is showing no signs of slowing down their competitive efforts, as in May 2015, their strategy focused around directly challenging Verizon by offering Verizon customers a side-by-side trial of services, and an offer to pay a customer's fees if they switch.




All T-Mobile networks (including affiliates and minority owned): 148.4 million (as of March 2009)




Beginning in 2009, T-Mobile embarked upon a progressive marketing campaign with hip commercials featuring, Charles Barkley, Dwyane Wade, Dwight Howard and Kym Whitley. Actress Catherine Zeta-Jones has also appeared in television commercials for the company. The inventor of the song is unknown, but is known to have been a member of the PR team for T-Mobile.
The five-note T-Mobile audio logo was composed by Lance Massey in 1999, and was originally part of the song ("Hello Ola" by Clan Chi) used to promote DT's Tour de France bicycle team.
In 2002, an advertisement for UK television for when T-Mobile introduced picture messaging; was aired, which shows the face of a baby being spread around a built-up area as things like billboards, posters, shirts, and on newspapers. The advertisement featuring the R yksopp song "So Easy", and the song was consequently a #21 hit single in the UK, along with its double a-side partner "Remind Me". The advertisement is believed to have inspired music videos like Where Is the Love? (by The Black Eyed Peas) and Have a Nice Day (by Bon Jovi).
In 2013 in Poland, T-Mobile released a television ad promoting the late Communist leader Lenin of the war against Poland 1919-1920. The campaign resulted with the debate on business ethics and the image of the company has been perceived as unethical. Some suggestions of future marketing utilization of Hitler's image were made. Later in 2013, local Attorneys General complained about T-Mobile ads that featured customers eager to have their phones stolen away from them.




T-Mobile's parent company, Deutsche Telekom, currently serves as the kit sponsor for German Bundesliga club FC Bayern Munich. T-Mobile was also the official sponsor of English Football League Championship side West Bromwich Albion and previously sponsored Rotherham United as well as Scottish Premier League clubs Rangers and Celtic.
T-Mobile co-sponsored Everton with the One2One brand and they also sponsored the 2002 FA Youth Cup Final. T-Mobile was also a kit sponsor for English club Birmingham City. The phone company is also involved in sponsoring leagues such as the Austrian Football Bundesliga, which is named the T-Mobile Bundesliga. It was also the official global mobile phone carrier for the 2006 FIFA World Cup football tournament in Germany and sponsored its own cycling team, the T-Mobile Team (later Team HTC-High Road).
T-Mobile also has banner ads at some matches of Mexico's top association football league, Liga MX, despite not having a presence in that country. Presumably, this is an attempt to market to U.S. soccer fans, as well as the country's Hispanic and more specifically Mexican community. Many top-flight Mexican matches are televised in the United States on both English- and Spanish-language networks.


