Optical or photonic computing uses photons produced by lasers or diodes for computation. For decades, photons have promised to allow a higher bandwidth than the electrons used in conventional computers.
Most research projects focus on replacing current computer components with optical equivalents, resulting in an optical digital computer system processing binary data. This approach appears to offer the best short-term prospects for commercial optical computing, since optical components could be integrated into traditional computers to produce an optical-electronic hybrid. However, optoelectronic devices lose 30% of their energy converting electrons into photons and back. This also slows transmission of messages. All-optical computers eliminate the need for optical-electrical-optical (OEO) conversions.
Application-specific devices such as optical correlators have been designed that use principles of optical computing. Such devices can be used for detecting and tracking objects, for example  and classification of serial time-domain optical data, for example. 



The fundamental building block of modern electronic computers is the transistor. To replace electronic components with optical ones, an equivalent optical transistor is required. This is achieved using materials with a non-linear refractive index. In particular, materials exist where the intensity of incoming light affects the intensity of the light transmitted through the material in a similar manner to the voltage response of an electronic transistor. Such an "optical transistor" can be used to create optical logic gates, which in turn are assembled into the higher level components of the computer's CPU. These will be non linear crystals used to manipulate light beams into controlling others.



There are disagreements among researchers about the future capabilities of optical computers: will they be able to compete with semiconductor-based electronic computers on speed, power consumption, cost, and size. Opponents of the idea that optical computers can be competitive note that real-world logic systems require "logic-level restoration, cascadability, fan-out and input output isolation", all of which are currently provided by electronic transistors at low cost, low power, and high speed. For optical logic to be competitive beyond a few niche applications, major breakthroughs in non-linear optical device technology would be required, or perhaps a change in the nature of computing itself.



A claimed advantage of optics is that it can reduce power consumption, but an optical communication system typically uses more power over short distances than an electronic one. This is because the shot noise of an optical communication channel is greater than the thermal noise of an electrical channel which, from information theory, means that more signal power is required to achieve the same data capacity. However, over longer distances and at greater data rates, the loss in electrical lines is larger than that of optical communications. As communication data rates rise, this distance becomes shorter and so the prospect of using optics in computing systems becomes more practical.
A significant challenge to optical computing is that computation is a nonlinear process in which multiple signals must interact. Light, which is an electromagnetic wave, can only interact with another electromagnetic wave in the presence of electrons in a material, and the strength of this interaction is much weaker for electromagnetic waves such as light than for the electronic signals in a conventional computer. This may result in the processing elements for an optical computer requiring more power and larger dimensions than those for a conventional electronic computer using transistors.




Photonic logic is the use of photons (light) in logic gates (NOT, AND, OR, NAND, NOR, XOR, XNOR). Switching is obtained using nonlinear optical effects when two or more signals are combined.
Resonators are especially useful in photonic logic, since they allow a build-up of energy from constructive interference, thus enhancing optical nonlinear effects.
Other approaches currently being investigated include photonic logic at a molecular level, using photoluminescent chemicals. In a recent demonstration, Witlicki et al. performed logical operations using molecules and SERS.


