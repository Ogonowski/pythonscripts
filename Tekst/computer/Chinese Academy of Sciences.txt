The Chinese Academy of Sciences (CAS; Chinese:  ), with roots in the Academia Sinica of the Republic of China, is the national academy for the natural sciences of the People's Republic of China. Collectively known as the "Two Academies ( )" along with the Chinese Academy of Engineering, it is an institution of the State Council of China, functioning as the national scientific thinktank and academic governing body, providing advisory and appraisal services on issues stemming from the national economy, social development, and science and technology progress. It is headquartered in Beijing, with branch institutes all over mainland China. It has also created hundreds of commercial enterprises, with Lenovo being one of the most famous.




The Chinese Academy has its roots from Academia Sinica, founded in 1928 by the Guomindang Nationalist Government. After the Communist Party took control of mainland China, Academia Sinica was renamed Chinese Academy of Sciences. The Chinese Academy of Sciences has six academic divisions:
Mathematics and Physics ( )
Chemistry ( )
Life Sciences and Medical Sciences ( )
Earth Sciences ( )
Information Technological Sciences ( )
Technological Sciences ( )
The CAS has thirteen regional branches in Beijing, Shenyang, Changchun, Shanghai, Nanjing, Wuhan, Guangzhou, Chengdu, Kunming, Xi'an, Lanzhou, Hefei and Xinjiang. It has over one hundred institutes and two universities (the University of Science and Technology of China at Hefei, Anhui and the University of the Chinese Academy of Sciences in Beijing). Backed by the institutes of CAS, UCAS is headquartered in Beijing, with graduate education bases in Shanghai, Chengdu, Wuhan, Guangzhou and Lanzhou, four Science Libraries of Chinese Academy of Sciences, three technology support centers and two news and publishing units. These CAS branches and offices are located in 20 provinces and municipalities throughout China. CAS has invested in or created over 430 science- and technology-based enterprises in eleven industries including eight companies listed on stock exchanges.
Being granted a Fellowship of the Academy represents the highest level of national honor for Chinese scientists. The CAS membership system includes Academicians ( ), Emeritus Academician ( ) and Foreign Academicians ( ).
Current President: Bai Chunli



Based on the number of papers published in Nature and/or Nature monthly research journals, the Chinese Academy of Science ranks 6th in the world by Nature Publishing Index (2013), by Nature Publishing Group.



Guo Moruo ( ): 1949 1978
Fang Yi ( ): 1979 1981
Lu Jiaxi ( ): 1981 1987
Zhou Guangzhao ( ): 1987 1997
Lu Yongxiang ( ): 1997 2011
Bai Chunli ( ): 2011 incumbent



Beijing Branch
Institute of Physics
Institute of Theoretical Physics
Institute of High Energy Physics
Institute of Biophysics
Institute of Electronics
National Astronomical Observatories
Institute of Computing Technology
Institute of Software
Institute of Automation
Beijing Institute of Genomics
Institute of Vertebrate Paleontology and Paleoanthropology
National Center for Nanoscience and Technology
Institute of Policy and Management
Institute of Psychology

Changchun Branch
Changchun Institute of Optics and Fine Mechanics and Physics
Changchun Institute of Applied Chemistry
Northeast Institute of Geography and Agroecology
Changchun Observatory

Guangzhou Branch
South China Botanical Garden

Hefei Branch
Hefei Institutes of Physical Science
University of Science and Technology of China

Kunming Branch
Kunming Institute of Botany
Kunming Institute of Zoology
Xishuangbanna Tropical Botanical Garden
Institute of Geochemistry
Yunnan Astronomical Observatory

Shanghai Branch
Shanghai Institute of Microsystem & Information Technology
Shanghai Institute of Technical Physics
Shanghai Institute of Optics and fine Mechanics
Shanghai Institute of Ceramics
Shanghai Institute of Organic Chemistry
Shanghai Institute of Applied Physics
Shanghai Institutes for Biological Sciences
Shanghai Institute of Materia Medica
Institut Pasteur of Shanghai

Shenyang Branch
Institute of Metal Research
Shenyang Institute of Automation
Shenyang Institute of Applied Ecology, formerly the Institute of Forestry and Pedology
Shenyang Institute of Computing Technology
Dalian Institute of Chemical Physics
Qingdao Institute of Oceanology
Qingdao Institute of Bioenergy and Bioprocess Technology
Yantai Institute of Coastal Zone Research

Wuhan Branch
Wuhan Institute of Rock and Soil Mechanics
Wuhan Institute of Physics and Mathematics
Wuhan Institute of Virology
Institute of Geodesy and Geophysics
Institute of Hydrobiology
Wuhan Botanical Garden



On 26 February 2007, the CAS published a Declaration of Scientific Ideology and set up a commission for scientific integrity to promote transparency, autonomy and accountability of scientific research in the country. The Ministry of Science and Technology had at the same time also initiated measures to address misconduct in state-funded programs.



Together with the National Natural Science Foundation of China, the academy publishes the peer-reviewed academic journal, Science China (also known as Science in China). Science China comprises seven series:
A: Mathematics
B: Chemistry
C: Life Sciences
D: Earth Sciences
E: Technological Sciences
F: Information Sciences
G: Physics, Mechanics & Astronomy



Since 1999 the CAS has issued the annual State Preeminent Science and Technology Award, presented by the President of China to the recipient.


