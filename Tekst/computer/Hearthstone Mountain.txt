The Bear Pond Mountains are a sub-range in the Appalachian Mountains, that straddle Pennsylvania and Maryland in the United States. These mountains are a part of the Ridge and Valley Appalachians and reach their highest point at Cross Mountain (Pennsylvania) 2,062 feet (628 m). A unique geologic feature known as the "Punchbowl" occurs in this range. This feature was created by the weathered shales of the Ordovician age in the center of a south-plunging anticline, having been eroded to expose a large amphitheater like feature (punchbowl). Cross and Hearthstone Mountain are made of hard resistant quartzite of the Tuscarora Formation of the Silurian age, which form the walls of the bowl.
Whitetail Ski Resort is also located in this range on Two Top Mountain.
The chief summits of the Bear Pond Mountains are the following:
Cross Mountain (Pennsylvania) 2,062 feet (628 m)
Hearthstone Mountain 2,021 feet (616 m)
Two Top Mountain 1,780 feet (543 m)
Kasies Knob 1,760 feet (536 m)
Fairview Mountain 1,690 feet (515 m)
Gillians Knob 1,575 feet (480 m)
Bullskin Mountain 1,551 feet (473 m)
Rickard Mountain 1,550 feet (472 m)
Powell Mountain 1,548 feet (472 m)
Sword Mountain 1,500 feet (457 m)
Abe Mills Mountain 1,376 feet (419 m)
Johnson Mountain 1,140 feet (347 m)



Alan R. Geyer (1979) "Outstanding Geologic Features of Pennsylvania", Geological Survey of Pennsylvania
"Latitude and Longitude from TopoQuest". TopoQuest.com.