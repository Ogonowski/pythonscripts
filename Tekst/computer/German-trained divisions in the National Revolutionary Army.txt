The German trained divisions were the elite of the infantry divisions in the Chiang Kai-Shek's National Revolutionary Army (NRA) trained under Sino-German cooperation until 1941. These divisions were active in the Chinese Civil War and Second Sino-Japanese War.
In 1927, after the dissolution of the First United Front between the Nationalists and the Communists, the ruling Kuomintang (KMT, or the Chinese Nationalist Party) purged its leftist members and largely eliminated Soviet influence from its ranks. Chiang Kai-shek then turned to Germany, historically a great military power, for the reorganisation and modernisation of the NRA.
The Weimar Republic sent advisors to China, but because of the restrictions imposed by the Treaty of Versailles, they could not serve in military capacities. Chiang initially requested famous generals such as Ludendorff and von Mackensen as advisors - the Weimar Republic turned him down, fearing that they were too famous, would invite the ire of the Allies, and would result in the loss of national prestige for such renowned figures to work, essentially, as mercenaries.
When Adolf Hitler became Chancellor in 1933 and disregarded the Treaty, the anti-communist NSDAP (the Nazi Party) and the anti-communist KMT were soon engaged in cooperation with Germany training Chinese troops and expanding Chinese infrastructure, while China opened its markets and natural resources to Germany. Max Bauer was the first advisor to China.
In 1934 General Hans von Seeckt, acting as advisor to Chiang, proposed a "80 Division Plan" for reforming the entire Chinese army into 80 divisions of highly trained, well-equipped troops organised along German lines. The plan was never fully realised, as the vying warlords could not agree upon the reorganisationed division organization and the disbanding of the others. By July 1937 only 8 infantry divisions had completed reorganisation and training. These were the 3rd, 6th, 9th, 14th, 36th, 87th, 88th, and the Training Division.
Another 12 divisions equipped with Chinese arms on the reorganized model with German advisors had some training by the time the Second Sino-Japanese War started in July 1937. These Divisions were the 2nd, 4th, 10th, 11th, 25th, 27th, 57th, 67th, 80th, 83rd, 89th Infantry Divisions.
Reorganized Division organization
Division headquarters troops:
1 Artillery battalion:
3 Artillery companies (12 x 75 millimeter mountain artillery guns in batteries of 4)

1 Anti Tank artillery company (4 x 37mm AT gun)
1 Anti-aircraft gun company (4 x 20mm anti-aircraft guns)
1 Military Engineer Battalion
1 Signal battalion
2 Wire communications companies
1 Wireless correspondence platoon

1 Heavy Transport Battalion
1 Special duty Battalion (Special Operations Battalion)
1 medical team (usually is a Division hospital)

2 Infantry Brigades of:
each with 2 infantry regiments, Each Regiment consisted of:
Regimental headquarters troops:
1 Mortar company (6 x 81 millimeter mortars)
1 Small artillery company (6 x 20 millimeter autocannon)
1 Communications company
1 Regimental recon company
3 infantry battalions each of:
Infantry battalion headquarters troops:
1 heavy Machinegun company (6 heavy machine guns, 2 x 82 millimeter mortars)
3 Infantry companies of:
3 rifle platoons of
3 squads (each of 1 light machine gun, 10 rifles)

Each Division was to have two Home Regiments to provide trained Infantry replacements. The strength of this Division was to be approximately 14,000 men.


