Curt Herzstark (July 26, 1902 - October 27, 1988) was an Austrian engineer. During World War II, Curt Herzstark's designed plans for a mechanical pocket calculator (the Curta).



Herzstark was born in Vienna, the son of Marie and Samuel Jakob Herzstark. His father was Jewish and his mother, born a Catholic, converted to Lutheranism and raised Herzstark Lutheran. In 1938, while he was technical manager of his father's company Rechenmaschinenwerk AUSTRIA Herzstark & Co., Herzstark had already completed the design, but could not manufacture it due to the Nazi German annexation of Austria. Instead, the company was ordered to make measuring devices for the German Army. In 1943, perhaps influenced by the fact that his father was a liberal Jew, the Nazis arrested him for "helping Jews and subversive elements" and "indecent contacts with Aryan women" and sent him to the Buchenwald concentration camp. However, the reports of the army about the precision-production of the firm AUSTRIA and especially about the technical expertise of Herzstark led the Nazis to treat him as an "intelligence-slave".
His imprisonment at Buchenwald seriously threatened his health, but his condition improved when he was called to work in the factory linked to the camp, which was named after Wilhelm Gustloff. There he was ordered to make a drawing of the construction of his calculator, so that the Nazis could ultimately give the machine to the F hrer as a gift after the successful end of the war. The preferential treatment this allowed ensured that he survived his stay at Buchenwald until the camp's liberation in 1945, by which time he had redrawn the complete construction from memory.
Herzstark died in Nendeln, Liechtenstein.
The Curta is referenced in chapter four of William Gibson's Pattern Recognition. The chapter is entitled 'Math Grenades', referring to protagonist Cayce Pollard mistaking them for hand grenades at first glance.



Cliff Stoll, Scientific American, 290(1):92 99, January 2004.
Frank Thadeusz, Der Spiegel, [1], July 2013.






Biographical information about Curt Herzstark and the Curta calculator.
Interview of Curt Herzstark at the Charles Babbage Institute, University of Minnesota. Herzstark interview in German.