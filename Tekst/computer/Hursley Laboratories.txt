Hursley House is an 18th-century Queen Anne style mansion in Hursley, near Winchester in the English county of Hampshire. The building is Grade II* listed.



The Hursley estate was bought by William Heathcote, MP from the daughters of Richard Cromwell. Cromwell had acquired the estate by marriage to the daughter of Richard Major, MP. Heathcote commissioned the present house to be built between 1721 and 1724, during the reign of George I, and was created a baronet in 1733. The estate descended in the Heathcote family to the 5th Baronet, whose widow sold it after his death in 1881 to Joseph Baxendale, the owner of the Pickfords logistics and removal company. He sold it in 1902 to Sir George Cooper whose wife, Mary Emma Smith, an American railways heiress from Chicago, commissioned architect Alexander Marshall Mackenzie to carry out extensive development work in 1902 to create the house that can be seen today. Sir George was created a baronet in 1905.
During the First World War the second floor of the house was made available as a nursing hospital for officers. It was intended to turn it over again as a military hospital during the Second World War but Sir George died in 1940 and it was requisitioned instead by the Ministry of Aircraft Production (MAP) to rehouse the Design and Production departments of Vickers Supermarine, which had been bombed out of its original premises in Woolston, Hampshire. During its time in the House, Supermarine worked on the development of many aircraft, of which the most famous is the Spitfire but also includes the early Jet fighters like the Attacker, Swift and Scimitar.




In 1958 IBM started using the House and its grounds as development laboratories. In 1963 IBM purchased the 100 acres (405,000 m ) of surrounding land and have since erected a large modern office complex employing over 1500 people. The original house is still used by IBM as an Executive Briefing Centre; the original oak-panelled library - restocked with technical books - retains its former use. The lower ground floor of the house is home to the IBM Hursley Museum, a computing museum that covers the history of IBM Hursley Park, IBM United Kingdom and IBM Corporation.






IBM Hursley Site
Map of Hursley Park in 1588

IBM Hursley Labs Flickr Galleries
IBM Hursley Labs Pinterest Pins
IBM Hursley Labs YouTube Channel
IBM Hursley Labs Vimeo Channel
IBM Hursley Museum