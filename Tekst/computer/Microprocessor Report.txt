Microprocessor Report, is a publication for engineers and other industry professionals on microprocessors. The publication is accessible only to paying subscribers. To avoid bias, it does not take advertisements. The publication provides extensive analysis of new high-performance microprocessor chips. In addition, it covers microprocessor design issues, microprocessor-based systems, new memory and system logic chips, embedded processors, GPUs, DSP technology, and intellectual property (IP) cores.
Microprocessor Report was first published in 1987 by Michael Slater, originally monthly in print. Since 2000, it is published weekly online and monthly in print. The annual April 1 issue sometimes contained humorous spoofs such as "The Butt Gate" and an article on write-only memory. More often, the articles describe the internal design and feature set of microprocessors from vendors such as Intel, Broadcom, and Qualcomm. The articles usually compare the leading products and discuss their strengths and weaknesses. The annual "year in review" articles provide a broader look at the processor landscape. The publication gives annual awards to the best microprocessor products. Free summaries of these articles are available online.
Slater's company MicroDesign Resources (MDR), based in Sebastopol, California, originally published Microprocessor Report. MDR also hosted an annual conference, the Microprocessor Forum, and related events. In 1992, MDR was acquired by Ziff-Davis. During the 1990s, Microprocessor Report was recognized four times as Best Newsletter by the Computer Press Association, which noted its "comprehensive and in-depth coverage." In 1999, MDR was sold to Cahners (later Reed Business Information), where it became part of the analyst group In-Stat. During the 2000s, the staff of the newsletter declined. On May 6, 2010, Microprocessor Report was acquired by The Linley Group. Since this acquisition, the staff has increased to a total of seven editors.
In addition to Slater, frequent contributors have included Linley Gwennap, Keith Diefendorff, Peter Glaskowsky, Kevin Krewell, Tom Halfhill, and Max Baron. The current staff, led by editor-in-chief Gwennap, includes Halfhill.
Slater wrote an account of the early years for the work's tenth anniversary.






About the magazine
The Linley Group website