Swiss cuisine bears witness to many regional influences, including from French, German and Italian cuisines and also features many dishes specific to Switzerland. Switzerland was historically a country of farmers, so traditional Swiss dishes tend to be plain and made from simple ingredients, such as potatoes and cheese.




There are many regional dishes in Switzerland. One example is Z rcher Geschnetzeltes, thin strips of veal with mushrooms in a cream sauce served with r sti. Italian cuisine is popular in contemporary Switzerland, particularly pasta and pizza. Foods often associated with Switzerland include cheese and chocolate. Swiss cheeses, in particular Emmental cheese, Gruy re, Vacherin, and Appenzeller, are famous Swiss products. The most popular cheese dishes are fondue and Raclette. Both these dishes were originally regional dishes, but were popularized by the Swiss Cheese Union to boost sales of cheese.
R sti is a popular potato dish that is eaten all over Switzerland. It was originally a breakfast food, but this has been replaced by the muesli, which is commonly eaten for breakfast and in Switzerland goes by the name of "Bircherm esli" ("Birchermiesli" in some regions). For breakfast and dinner many Swiss enjoy sliced bread with butter and jam. There is a wide variety of bread rolls available in Switzerland. Bread and cheese is a popular dish for dinner.
Tarts and quiches are also traditional Swiss dishes. Tarts in particular are made with all sorts of toppings, from sweet apple to onion.
In the Italian-speaking part of Switzerland, the Ticino area, one will find a type of restaurant unique to the region. The Grotto is a rustic eatery, offering traditional food ranging from pasta to homemade meat specialties. Popular dishes are Luganighe and Luganighetta, a type of artisan sausages. Authentic grottoes are old wine caves re-functioned into restaurants. Due to their nature they are mostly found in or around forests and built against a rocky background. Typically, the facade is built from granite blocks and the outside tables and benches are made of the same stone as well. Grottoes are popular with locals and tourists alike, especially during the hot summer months.
Cervelat or cervelas is considered the national sausage, and is popular all over Switzerland.



Papet vaudois: the Canton of Vaud is home to this filling dish of leeks and potatoes (hence the name vaudois). It is usually served with Saucisse au chou (cabbage sausage).
Carac: A Swiss shortcrust pastry with chocolate.
Fondue: This is probably the most famous Swiss menu. Fondue is made out of melted cheese. It is eaten by dipping small pieces of bread or potatoes in the melted cheese.
Meringue: Swiss Meringue with double cream from Gruy re.
Raclette: Hot cheese dribbled over potatoes, served with small gherkins, pickled onions etc.



 lplermagronen: (Alpine herdsman's macaroni) is a frugal all-in-one dish making use of the ingredients the herdsmen had at hand in their alpine cottages: macaroni, potatoes, onions, small pieces of bacon, and melted cheese. Traditionally  lplermagronen is served with applesauce instead of vegetables or salad.
Cut meat, Zurich style (Z rcher Geschnetzeltes): This dish is often served with R sti.
Emmental Apple R sti: This used to be a very popular meal, since the ingredients were usually at hand and the preparation is very simple. The recipe comes from the Emmental ("Emmen valley") in Canton Bern, the home of the famous Emmentaler cheese.
Fotzel slices: Nobody really knows how this dish got its name. Literally, "fotzel" means a torn-off scrap of paper, but in Basel dialect it means a suspicious individual. Stale bread can be used to make fotzel slices, which made it an ideal recipe for homemakers accustomed to never throwing bread away.
Kalberwurst: A sausage with a distinctive, creamy flavor that originated in the Canton of Glarus, kalberwurst is made with veal, milk, ground crackers, and mild spices. It has a smooth texture and mild taste, and although most sausages are smoked, kalberwurst is not. It is often cooked with onions and gravy.
Landj ger: A semi-dried sausage traditionally made in Switzerland, but also in Southern Germany, Austria, and Alsace. It is popular as a snack food during activities such as hiking. It also has a history as soldier's food because it keeps without refrigeration and comes in single-meal portions. Landj ger tastes similar to dried salami.
Birchermuesli: "Bircherm esli" was invented by Dr Maximilian Oskar Bircher-Benner (1867-1939), a pioneer of organic medicine and wholefoods.
Riz Casimir is a preparation of rice with curry sauce and minced pork blended with tropical fruits: pineapple, banana and cherries, sometimes with currant grape. It was first served in 1952 by the international chain of hotel and resorts M venpick.
R sti: This simple dish, similar to hash browns, is traditionally regarded as a Swiss German favorite. It has given its name to the "R sti ditch", the imaginary line of cultural demarcation between the German and French regions of Switzerland. However, it is also eaten by the French-speaking Swiss.
Tirggel are traditional Christmas biscuits from Zurich. Made from flour and honey, they are thin, hard, and sweet.
Zopf (bread): There are dozens of types of bread in Switzerland. However, Zopf is a typical Swiss speciality for Sundays.



Pizzoccheri: Short tagliatelle made of buckwheat flour cooked along with greens and cubed potatoes.
Polenta: For centuries polenta was regarded as a meal for the poor. Corn was introduced to the south of what is now Canton Ticino as long ago as the beginning of the 17th century, which led to a change in the monotonous cuisine. But it took another 200 years before polenta - at first made of mixed flour, only later of pure cornmeal - became the staple dish of the area.
Saffron Risotto is a common dish from Ticino, the southernmost canton of Switzerland.



B ndner Nusstorte: There are several different recipes for nut cake, but the most famous is probably the one from the Engadine, a valley in Canton Graub nden.
Chur Meat Pie: A popular dish from Graub nden in south eastern Switzerland
Graub nden Barley Soup: The most famous soup from Graub nden
Pizokel with cabbage: Pizokel were eaten in a wide variety of ways. In some places when eaten by themselves they are known in Romansh as "bizochels bluts", or  bald pizokel . If someone leaves a small amount of any kind of food on the serving dish for politeness's sake, in the Engadine this is called "far sco quel dal bizoccal", meaning more or less  leaving the last pizokel .



In the 2005 Michelin Guide, Switzerland ranked 2nd worldwide in terms of stars awarded per capita.




Rivella, a carbonated Swiss drink based on lactose, is one of the most popular drinks in Switzerland. Apple juice, both still and sparkling, is popular in many areas of Switzerland, and is also produced in the form of apple cider. The chocolate drink Ovomaltine (known in the USA as "Ovaltine") originates in Switzerland and enjoys ongoing popularity, particularly with young people. Aside from being a beverage, the powder is also eaten sprinkled on top of a slice of buttered bread.

Wine is produced in many regions of Switzerland, particularly the Valais, the Vaud, the Ticino and the canton of Zurich. Riesling X Sylvaner is a common white wine produced in German-speaking parts of the country, while Chasselas is the most common white wine in the French-speaking parts of the country. Pinot noir is the most popular red grape in both the French-speaking and the German-speaking part, while this position is held by Merlot in the Italian-speaking part.
Absinthe is being distilled officially again in its Val-de-Travers birthplace, in the Jura region of Switzerland, where it originated. Long banned by a specific anti-Absinthe article in the Swiss Federal Constitution, it was legalized again in 2005, with the adoption of the new constitution. Now Swiss absinthe is also exported to many countries, with K bler and La Clandestine Absinthe amongst the first new brands to emerge. Wine and beer can legally be purchased by youths of 16 or more years of age. Spirits and beverages containing distilled alcohol (including wine coolers like Bacardi Breezer) can be bought at 18.
Damassine is a liqueur produced by distillation of the Damassine prune from the Damassinier tree and is produced in the Canton of Jura.
Bon Pere William is a famous regional Swiss brandy produced from pears, alcohol 43% by volume. It is usually paired with fondue or raclette dishes or taken after dinner, sometimes poured in coffee with dessert. Some bottles are available with the full size pear inside the bottle, grown with the bud placed in the bottle. There are many other types of regional brandies made from local fruit, the most popular being cherries (kirschwasser).


