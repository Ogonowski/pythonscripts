German occupied Europe refers to the sovereign countries of Europe which were occupied by the military forces of Nazi Germany at various times between 1939 and 1945 and administered by the Nazi regime.



Several German occupied countries entered World War II as Allies of the United Kingdom or the Soviet Union. Some were forced to surrender such as Czechoslovakia; others like Poland (invaded on 1 September 1939) were conquered in battle and then occupied. In some cases, the legitimate governments went into exile, in other cases the governments-in-exile were formed by their citizens in other Allied countries. Selected countries occupied by Nazi Germany were officially neutral. Yet, others were former members of the Axis powers, and have been occupied by German forces at a later stage of the war.



The countries occupied included all, or most of the following:


