This page lists and links to information on the various versions of Microsoft Windows, a major computer operating system developed by Microsoft.



In this section, a client version of Windows is a version that can be purchased and installed on personal computers (desktop computers, laptops and workstations) or purchased with these computers.












Windows 10
Windows Phone
Windows Phone 8.1
Windows Phone 8
Windows Phone 7.8
Windows Phone 7.5
Windows Phone 7

Windows Mobile
Windows Mobile 6.5 for smartphones
Windows Mobile 6.1 for smartphones and PDAs
Windows Mobile 6 for smartphones and PDA
Windows Mobile 6 Standard for smartphones
Windows Mobile 6 Classic for PDA without GSM
Windows Mobile 6 for PDA with GSM



Windows Embedded
Windows Embedded Compact 2013
Windows Embedded 8
Windows Embedded Automotive
Windows Embedded Industry
Windows Embedded Compact 7
Windows XP Embedded
Windows NT 4.0 Embedded   Abbreviated NTe, it is an edition of Windows NT 4.0 that was aimed at computer-powered major appliances, vending machines, ATMs and other devices that cannot be considered computers per se. It is the same system as the standard Windows NT 4.0, but it comes packaged in a database of components and dependencies, from which a developer can choose individual components to build customized setup CDs and hard disk boot images. Windows NT 4.0 Embedded includes Service Pack 5.

Windows CE
Windows CE 6.0 (2006)
Windows CE 5.0 (2005), with version for smart phones and PDAs sold as Windows Mobile 5.0
Windows CE 4.2 (2004), with version for smart phones and PDAs sold as Windows Mobile 2003 SE
Windows CE 4.1 (2003), with version for smart phones and PDAs sold as Pocket PC 2003
Windows CE 4.0 (2002), with version for smart phones and PDAs sold as Pocket PC 2002
Windows CE 3.0 (2000 July), with version for smart phones and PDAs sold as Pocket PC 2000
Windows CE 2.12 (1999 August)
Windows CE 2.11 (1998 October)
Windows CE 2.1 (1998 July)
Windows CE 2.0 (1998 November)
Windows CE 1.0 (1995 November)



Windows Odyssey (13 January 2000)   a version intended to be an update to the Microsoft Windows NT 5.x codebase. The teams working on Neptune and Odyssey combined to work on Windows XP.
Windows Mobile 7 or Photon - originally a successor of Windows Mobile, it had been scrapped for Windows Phone 7 with Metro UI.
Windows Neptune (27 December 1999)   the first planned version of Microsoft Windows NT to have a consumer edition variant, based on the Windows 2000 codebase. A version was sent out to testers but was never released.
Windows Nashville (2 May 1996)   also known as Windows 96
Cairo (29 February 1996)   a "true object-oriented OS", planned after Windows NT 4.0 and had the same primary and secondary version



List of Microsoft software codenames
Comparison of Microsoft Windows versions


