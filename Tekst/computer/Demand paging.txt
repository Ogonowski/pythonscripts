In computer operating systems, demand paging (as opposed to anticipatory paging) is a method of virtual memory management. In a system that uses demand paging, the operating system copies a disk page into physical memory only if an attempt is made to access it and that page is not already in memory (i.e., if a page fault occurs). It follows that a process begins execution with none of its pages in physical memory, and many page faults will occur until most of a process's working set of pages is located in physical memory. This is an example of a lazy loading technique.



Demand paging follows that pages should only be brought into memory if the executing process demands them. This is often referred to as lazy evaluation as only those pages demanded by the process are swapped from secondary storage to main memory. Contrast this to pure swapping, where all memory for a process is swapped from secondary storage to main memory during the process startup.
Commonly, to achieve this process a page table implementation is used. The page table maps logical memory to physical memory. The page table uses a bitwise operator to mark if a page is valid or invalid. A valid page is one that currently resides in main memory. An invalid page is one that currently resides in secondary memory. When a process tries to access a page, the following steps are generally followed:
Attempt to access page.
If page is valid (in memory) then continue processing instruction as normal.
If page is invalid then a page-fault trap occurs.
Check if the memory reference is a valid reference to a location on secondary memory. If not, the process is terminated (illegal memory access). Otherwise, we have to page in the required page.
Schedule disk operation to read the desired page into main memory.
Restart the instruction that was interrupted by the operating system trap.



Demand paging, as opposed to loading all pages immediately:
Only loads pages that are demanded by the executing process.
As there is more space in main memory, more processes can be loaded reducing context switching time which utilizes large amounts of resources.
Less loading latency occurs at program startup, as less information is accessed from secondary storage and less information is brought into main memory.
As main memory is expensive compared to secondary memory, this technique helps significantly reduce the bill of material (BOM) cost in smart phones for example. Symbian OS had this feature.



Individual programs face extra latency when they access a page for the first time.
Programs running on low-cost, low-power embedded systems may not have a memory management unit that supports page replacement.
Memory management with page replacement algorithms becomes slightly more complex.
Possible security risks, including vulnerability to timing attacks; see Percival 2005 Cache Missing for Fun and Profit (specifically the virtual memory attack in section 2).
Thrashing which may occur due to repeated page faults.


