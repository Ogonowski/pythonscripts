Stewart Johonnot Oliver Alsop (May 17, 1914   May 26, 1974) was an American newspaper columnist and political analyst.



Born and raised in Avon, Connecticut from an old Yankee family, Alsop attended Groton School and Yale University. After graduating from Yale in 1936, Alsop moved to New York City, where he worked as an editor for the publishing house of Doubleday, Doran.



After the United States entered World War II, Alsop joined the British Army, because his high blood pressure precluded his joining the United States Army. While training in England, Alsop met Patricia Barnard "Tish" Hankey, an Englishwoman, whom he would marry on June 20, 1944.
A month after the wedding, Alsop was allowed to transfer to the U.S. Army, and was immediately sent on a mission planned by the Office of Strategic Services. For the mission, Alsop was parachuted into the P rigord region of France to aid the French Resistance. Alsop was later awarded the Croix de Guerre with Palm for his work on that and other wartime missions. Alsop worked with and for the OSS for the rest of the war.



From 1945 to 1958, Stewart Alsop was co-writer, with his elder brother Joseph Alsop, of the thrice-weekly "Matter of Fact" column for the New York Herald Tribune. Stewart Alsop usually stayed in Washington and covered domestic politics, while Joseph Alsop traveled the world to cover foreign affairs. In 1958, the Alsops described themselves as "Republicans by inheritance and registration, and [...] conservatives by political conviction."
After the Alsop brothers ended their partnership, Stewart Alsop went on to write articles and a regular column for the Saturday Evening Post until 1968, then a weekly column for Newsweek from 1968 to 1974.
He published several books, including a "sort of memoir" of his battle with an unusual form of leukemia, Stay of Execution. At the end of his battle with cancer, he requested that he be given something other than morphine to numb the pain because he was tired of morphine's sedative effect. His doctor suggested heroin.
In Avon, Connecticut, Stewart has a 53-acre (210,000 m2) public park named after him called Alsop Meadows.



Alsop was a grand-nephew of Theodore Roosevelt, and the son of Joseph Wright Alsop IV (1876 1953) and his wife Corinne Douglas Robinson (1886 1971).
Stewart and Patricia "Tish" Alsop (Died November 3, 2012) had six children: Joseph Wright Alsop VI, Ian Alsop, Elizabeth Winthrop Alsop children's book author, Stewart Alsop II investor and pundit, Richard Nicholas Alsop missionary with FamilyLife, and Andrew Alsop.



Sub Rosa : The O.S.S. and American Espionage (1946, with Thomas Braden)
We Accuse! The Story of the Miscarriage of American Justice in the Case of J. Robert Oppenheimer (1954, with Joseph Alsop)
The Reporter's Trade (1958, with Joseph Alsop)
Nixon & Rockefeller : A Double Portrait (1960)
The Center : People and Power in Political Washington (1968)
Stay of Execution : A Sort of Memoir (1973)



Operation Mockingbird
Corinne Roosevelt Robinson, grandmother
Eleanor Roosevelt, first cousin once-removed
Theodore Roosevelt, granduncle






Herken, Gregg. The Georgetown Set: Friends and Rivals in Cold War Washington (2014), covers both brothers [1]
Yoder, Jr., Edwin M. Joe Alsop's Cold War: A Study of Journalistic Influence and Intrigue (Chapel Hill, NC: University of North Carolina Press, 1995)



Joseph W. Alsop, with Adam Platt, "I've Seen the Best of It": Memoirs (NY: W.W. Norton, 1992)



Oral History Interview with Stewart Alsop, from the Lyndon Baines Johnson Library
Booknotes interview with Robert Merry on Taking on the World: Joseph and Stewart Alsop - Guardians of the American Century, March 24, 1996.