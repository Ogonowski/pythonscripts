NHS Connecting for Health (CFH) Agency was part of the UK Department of Health and was formed on 1 April 2005, having replaced the former NHS Information Authority. It was part of the Department of Health Informatics Directorate, with the role to maintain and develop the NHS national IT infrastructure. It adopted the responsibility of delivering the NHS National Programme for IT (NPfIT), an initiative by the Department of Health in England to move the National Health Service (NHS) in England towards a single, centrally-mandated electronic care record for patients and to connect 30,000 general practitioners to 300 hospitals, providing secure and audited access to these records by authorised health professionals.
NHS Connecting for Health ceased to exist on 31 March 2013, and some projects and responsibilities were taken over by Health and Social Care Information Centre.



Contracts for the NPfIT Spine and five Clusters were awarded in December 2003 and January 2004.
It was planned that patients would also have access to their records online through a service called HealthSpace. NPfIT is said by NHS CFH to be "the world's biggest civil information technology programme".
The cost of the programme, together with its ongoing problems of management and the withdrawal or sacking of two of the four IT providers, have placed it at the centre of ongoing controversy, and the Commons Public Accounts Committee has repeatedly expressed serious concerns over its scope, planning, budgeting, and practical value to patients. As of January 2009, while some systems were being deployed across the NHS, other key components of the system were estimated to be four years behind schedule, and others had yet to be deployed outside individual Primary Care Trusts (PCTs).
While the Daily Mail announced on 22 September 2011 that " 12bn NHS computer system is scrapped...", The Guardian noted that the announcement from the Department of Health on 9 September, had been "part of a process towards localising NHS IT that has been under way for several years". Whilst remaining aspects of the National Programme for IT were cancelled, most of the spending would proceed with the Department of Health seeking for local software solutions rather than a single nationally imposed system.



The programme was established in October 2002 following several Department of Health reports on IT Strategies for the NHS, and on 1 April 2005 a new agency called NHS Connecting for Health (CfH) was formed to deliver the programme. CfH absorbed both staff and workstreams from the abolished NHS Information Authority, the organisation it replaced. CfH was based in Leeds, West Yorkshire. By 2009, it was still managed nationally by CfH, with responsibility for delivery shared with the chief executives of the ten NHS strategic health authorities.



The refusal of the Department of Health to make "concrete, objective information about NPfIT's progress [...] available to external observers", nor even to MPs, attracted significant criticism, and was one of the issues which in April 2006 prompted 23 academics in computer-related fields to raise concerns about the programme in an open letter to the Health Select Committee. 2006-10-06 the same signatories wrote a second open letter
A report by the King's Fund in 2007 also criticised the government's "apparent reluctance to audit and evaluate the programme", questioning their failure to develop an ICT strategy whose benefits are likely to outweigh costs and the poor evidence base for key technologies.
A report by the Public Accounts Committee in 2009 called the risks to the successful deployment of the system "as serious as ever", adding that key deliverables at the heart of the project were "way off the pace", noting that "even the revised completion date of 2014 2015 for these systems now looks doubtful in the light of the termination last year of Fujitsu's contract covering the South", and concluding "essential systems are late, or, when deployed, do not meet expectations of clinical staff".
The initial reports into the feasibility of the scheme, known to have been conducted by McKinsey, and subsequent reports by IT industry analyst Ovum among others have never been published nor made available to MPs.



Originally expected to cost  2.3 billion (bn) over three years, in June 2006 the total cost was estimated by the National Audit Office to be  12.4bn over 10 years, and the NAO also noted that "...it was not demonstrated that the financial value of the benefits exceeds the cost of the Programme". Similarly, the British Computer Society (2006) concluded that "...the central costs incurred by NHS are such that, so far, the value for money from services deployed is poor". Officials involved in the programme have been quoted in the media estimating the final cost to be as high as  20bn, indicating a cost overrun of 440% to 770%.
In April 2007, the Public Accounts Committee of the House of Commons issued a damning 175-page report on the programme. The Committee chairman, Edward Leigh, claimed "This is the biggest IT project in the world and it is turning into the biggest disaster." The report concluded that, despite a probable expenditure of 20 billion pounds "at the present rate of progress it is unlikely that significant clinical benefits will be delivered by the end of the contract period."
In September 2013, the Public Accounts Committee said that although the National Programme for IT had been effectively disbanded in 2011, some large regional contracts and other costs remained outstanding and were still costing the public dearly. It described the former National Programme for IT as one of the "worst and most expensive contracting fiascos" ever.
The costs of the venture should have been lessened by the contracts signed by the IT providers making them liable for huge sums of money if they withdrew from the project; however, when Accenture withdrew in September 2006, then Director-General for NPfIT Richard Granger charged them not  1bn, as the contract permitted, but just  63m. Granger's first job was with Andersen Consulting, which later became Accenture.



The programme was divided into a number of key deliverables.
*NHSmail was renamed to Contact in late 2004, before being reverted to NHSmail in April 2006.



The Spine is a set of national services used by the NHS Care Record Service. These include:
The Personal Demographics Service (PDS), which stores demographic information about each patient and their NHS Number. Patients cannot opt-out from this component of the spine, although they can mark their record as 'sensitive' to prevent their contact details being viewed by 831,000 staff.
The Summary Care Record (SCR). The Summary Care Record is a summary of patient's clinical information, such as allergies and adverse reactions to medicine.
The Secondary Uses Service (SUS), which uses data from patient records to provide anonymised and pseudonymised business reports and statistics for research, planning and public health delivery.
The Spine also provides a set of security services, to ensure access to information stored on the Spine is appropriately controlled. These security measures were queried during the early stages of Spine development, with leaked internal memos seen by the Sunday Times mentioning "fundamental" design flaws. In addition, government spokeswoman Caroline Flint failed to dispel concerns regarding access to patients' data by persons not involved in their care when she commented in March 2007 that "in general only those staff who are working as part of a team that is providing a patient with care, that is, those having a legitimate relationship with the patient, will be able to see a patient's health record."
The Spine was migrated to a new Open Source system in August 2014.



The NHS in Wales is also running a national programme for service improvement and development via the use of Information Technology   this project is called Informing Healthcare. A challenge facing both NHS CFH and Informing Healthcare is that the use of national systems previously developed by the NHS Information Authority are shared by both of these organisations and the Isle of Man. Separate provision needs to be made for devolution, while maintaining links for patients travelling across national borders.
NPfIT is currently focussed on delivering the NHS Care Record Service to GPs, Acute and Primary Hospitals, medical clinics and local hospitals and surgeries. Whilst there are no immediate plans to include opticians or dentists in the electronic care record, services are delivered to these areas of the NHS.



The programme originally divided England into five areas known as "clusters": Southern, London, East & East Midlands, North West & West Midlands, and North East. For each cluster, a different Local Service Provider (LSP) was contracted to be responsible for delivering services at a local level. This structure was intended to avoid the risk of committing to one supplier which might not then deliver; by having a number of different suppliers implementing similar systems in parallel, a degree of competition would be present which would not be if a single national contract had been tendered. Four clusters were awarded in two tranches on 8 and 23 December 2003, with the fifth on 26 January 2004. However, in July 2007 Accenture withdrew from their 2 clusters, and in May 2008 Fujitsu had their contract terminated, meaning that half the original contractors had dropped out of the project. As of May 2008, two IT providers were LSPs for the main body of the programme:
Computer Sciences Corporation (CSC)   North, Midlands & Eastern (NME) cluster
BT Health London (formerly BT Capital Care Alliance)   London cluster
Accenture had full responsibility for the North East and East/East Midlands clusters until January 2007, when it handed over the bulk of its responsibilities to the CSC, retaining responsibility for Picture archiving and communication system (PACS) rollout only.
Fujitsu   had responsibility for the Southern cluster until May 2008 when their contract was terminated. Most of their responsibilities were subsequently transferred to BT Health except for PACS which was transferred to the CSC Alliance.



In the first half of 2007, David Nicholson announced the "National Programme, Local Ownership programme" (known as "NLOP") which dissolved the 5 clusters and devolved responsibility for the delivery of the programme to the ten English NHS strategic health authorities (SHAs). Connecting for Health retains responsibility for the contracts with the LSPs.
Under NLOP, staff employed by CfH in the Clusters had their employment transferred to the SHAs, with some being recruited to revised national CfH posts.



In addition to these LSPs the programme has appointed National Application Service Providers (NASPs) who are responsible for services that are common to all users e.g. Choose and Book and the national elements of the NHS Care Records Service that support the summary patient record and ensure patient confidentiality and information security. As of October 2005, the NASPs are:
BT   NHS Care Records Service and N3
Atos Origin and Cerner   Choose & Book
Cable and Wireless   NHSmail



In March 2004, EDS had their 10-year contract to supply the NHSMail service terminated. On 1 July 2004, Cable and Wireless were contracted to provide this service, which was initially renamed Contact.
IDX Systems Corporation was removed from the Southern Cluster Fujitsu Alliance in August 2005 following repeated failure to meet deadlines. They were replaced in September 2005 by Cerner Corporation.
In early 2006, ComMedica's contract for supply of PACS to the North-West/West-Midlands cluster was terminated, and they were replaced by GE Healthcare.
In July 2006, the London region started the contractual replacement of IDX (which had been bought out by GE Healthcare in January 2006) as its supplier. Systems for secondary care, primary care and community and mental health services are proposed by BT to be provided by Cerner, INPS (formerly in Practice Systems) and CSE Healthcare Systems, part of the CSE-Global group of companies, respectively. This is subject to contractual negotiation known as 'CCN2'.
In September 2006, the CSC Alliance, Accenture and Connecting for Health signed a tripartite agreement that as of January 2007, the CSC Alliance would take over the responsibility for the majority of care systems the North East and Eastern clusters from Accenture, with the exception of PACS. As part of the handover process, around 300 Accenture personnel transferred under a TUPE process to CSC, and CSC took over the leases for some of Accenture's premises in Leeds. Accenture now retains only a small presence in the city for the delivery of its PACS responsibilities.
In May 2008 it was announced that following the failure to conclude renegotiation of the contract for the Southern Cluster, CfH terminated the contract with Fujitsu. The majority of the Southern Cluster care systems were subsequently transferred to BT Health except for PACS which was transferred to the CSC Alliance, aligning with the technology deployed by each company.






The 2009 Public Accounts Committee report noted that the NPfIT had provided "little clinical functionality...to-date"
The latest PAC report here 18 July 2011
The National Programme for IT in the NHS: an update on the delivery of detailed care records systems



NPfIT has been criticised for inadequate attention to security and patient privacy, with the Public Accounts Committee noting "patients and doctors have understandable concerns about data security", and that the Department of Health did not have a full picture of data security across the NHS. In 2000, the NHS Executive won the "Most Heinous Government Organisation" Big Brother Award from Privacy International for its plans to implement what would become the NPfIT. In 2004 the NPfIT won the "Most Appalling Project" Big Brother Award because of its plans to computerise patient records without putting in place adequate privacy safeguards.
The balance between the right to privacy and the right to the best quality care is a sensitive one. Also there are sanctions against those who access data inappropriately, specifically instant dismissal and loss of professional registration.
More worryingly, a January 2005 survey among doctors indicates that support for the initiative as an 'important NHS priority' has dropped to 41%, from 70% the previous year. There have been concerns raised by clinicians that clinician engagement has not been addressed as much as might be expected for such a large project.
Concerns over confidentiality, and the security of medical data uploaded to the Spine have also led to opposition from civil liberties campaigners such as NO2ID the anti-database state pressure group and The Big Opt Out who provide patients with a letter to send to their doctor so that their records are withheld from the database.



As of 5 August 2005, research carried out across the NHS in England suggested that clinical staff felt that the programme was failing to engage the clinicians fully, and was at risk of becoming a white elephant. The Public Accounts Committee observed in 2009 that "the current levels of support reflect the fact that for many staff the benefits of the Programme are still theoretical".
Surveys in 2008 suggested that two-thirds of doctors would refuse to have their own medical records on the system.



According to the Daily Telegraph, the head of NPfIT, Richard Granger, 'shifted a vast amount of the risk associated with the project to service providers, which have to demonstrate that their systems work before being paid.' The contracts meant that withdrawing from the project would leave the providers liable for 50% of the value of the contract; however, as previously mentioned, when Accenture withdrew in September 2006, Granger chose not to use these clauses, saving Accenture more than  930m.
The programme's largest software provider iSOFT has been seriously affected by this process and is under investigation by the UK Financial Services Authority for irregular accounting. On 28 September 2006, the consultancy Accenture announced its intention to withdraw from  2bn of 10-year contracts with NPfIT, which were taken over in January 2007 by the CSC Alliance   both Accenture and CSC laid blame with iSOFT, although CSC has said it will be retaining iSOFT as its software provider for all its clusters. Earlier in the year Accenture had written off $450m from its accounts because of 'significant delays' in the programme. iSOFT announced in March 2011 that trading in its shares would be suspended pending a corporate announcement. Subsequently in April 2011, the company announced that it was recommending a cash offer from CSC. CSC acquired iSOFT in August 2011



The first trusts in the London & Southern Clusters to implement the new Cerner system found it problematic, with NHS hospital trust board minutes revealing a catalogue of errors. Difficulties with the system meant that:
2007: Enfield PCT were unable to obtain vital data on patients awaiting operations and were obliged to delay 63 patients of the Barnet and Chase Farm Hospitals. Further, 20 patients were not readmitted for treatment within 28 days towards the end of the year because the surveillance system for tracking them "was not operational in the new ... system". Buckinghamshire Hospitals NHS Trust found that problems with the system had meant potentially infectious patients with MRSA were not isolated for up to 17 days, requiring six weeks work by staff to update them manually.
April 2008: Enfield PCT found that the system had failed to flag up possible child-abuse victims entering hospital to key staff, "leaving the responsibility to the receptionist"
May 2008: Enfield PCT found that 272 elective operations were cancelled at the last minute for "non-clinical reasons"
May 2008: Barts and The London NHS Trust blamed their failure over the preceding six months to meet targets for treating emergency patients within four hours on staff not being familiar with the new computer system. The same report cited "breaches of the two-week urgent cancer access guarantee" and delays in assessing 11 patients with possible cancer as being due to the computer system.
July 2008: the Royal Free Hampstead NHS Trust said 12,000 patient records had to be manually amended over a three-week period due to the system, and noted that "The outpatient appointment centre has experienced a significant increase in the time taken to process individual patient appointment bookings. This has had a consequent and negative effect on call-answer performance."



The NHS appointed a management team, responsible for the delivery of the system:
Richard Granger the former Director General of IT for the NHS, took up his post in October 2002, before which he was a partner at Deloitte Consulting, responsible for procurement and delivery of a number of large scale IT programmes, including the Congestion Charging Scheme for London. In October 2006, he was suggested by The Sunday Times to be the highest paid civil servant, on a basic of  280,000 per year,  100,000 per year more than then-Prime Minister Tony Blair. Granger announced on 16 June 2007 that he would leave the agency "during the latter part" of 2007. Granger finally left the programme in February 2008. Granger's credentials were questioned by his own mother, a campaigner for the preservation of local health services in her area, who expressed her amazement at his appointment, criticising the whole scheme as "a gross waste of money".
Gordon Hextall   Chief operating officer for NHS Connecting for Health. A career civil servant. On Richard Granger's departure, Hextall assumed overall responsibility for the programme.
Richard Jeavons   Senior responsible owner for service implementation. Previous posts included being CEO of the West Yorkshire NHS strategic health authority.
Harry Cayton   Chair of the Care Record Development Board.
In 2009, overall leadership of CfH was described by the Public Accounts Committee as having been "uncertain" since the announcement that Richard Granger would be leaving the project.



Megaproject
National Identity Register
Citizen Information Project
Cost overrun
Universal Child Database
Medical privacy
Health Informatics
N3 (NHS)
NHS Care Records Service
Choose and Book
OPCS-4
Picture archiving and communication system (PACS)
NHS Electronic Prescription Service
Quality Management and Analysis System
NHSmail






NHS Connecting for Health website
NHS number
Personal Demographics Service (PDS)
"A guide to the National Programme for Information Technology" (PDF), NHS Connecting for Health, April 2005
PC Coaching appointed to provide Trainers & MTUs for NPfIT, NHS Connecting for Health award contract for NPfIT Trainers and Mobile Training Units
Programmes and Systems Delivery, NHS Connecting for Health, retrieved 30 October 2005
NHS Connecting for Health NPfIT site
NHS Connecting for Health History Page
BT Health, Delivering three major contracts to the NHS as part of the NPfIT
June 2006: NHS risks  20bn white elephant, say auditors
Times Online: Patient records go on database
Computer loophole hits hi-tech NHS trial
NHS IT upgrade success 'at risk'
The Big Opt Out, Advice to patients   How to opt out
Opting out of the NHS Database Detailed information from Dr Neil Bhatia, a GP in Hampshire
NHS Information Standards Board for Health and Social Care