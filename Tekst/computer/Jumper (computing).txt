In electronics and particularly computing, a jumper is a short length of conductor used to close a break in or open, or bypass part of, an electrical circuit. Jumpers are typically used to set up or configure printed circuit boards, such as the motherboards of computers.



Jumper pins (points to be connected by the jumper) are arranged in groups called jumper blocks, each group having at least one pair of contact points. An appropriately sized conductive sleeve called a jumper, or more technically, a jumper shunt, is slipped over the pins to complete the circuit.
Jumpers must be electrically conducting; they are usually encased in a non-conductive block of plastic for convenience. This also avoids the risk that an unshielded jumper will accidentally short out something critical (particularly if it is dropped on a live circuit).



When a jumper is placed over two or more jumper pins, an electrical connection is made between them, and the equipment is thus instructed to activate certain settings accordingly. For example, with older PC systems, CPU speed and voltage settings were often made by setting jumpers. Informally, technicians often call setting jumpers "strapping". To adjust the SCSI ID jumpers on a hard drive, for example, is to "strap it up".
Some documentation may refer to setting the jumpers to on, off, closed, or open. When a jumper is on or covering at least two pins it is a closed jumper, when a jumper is off, is covering only one pin, or the pins have no jumper it is an open jumper.
Jumperless designs have the advantage that they are usually fast and easy to set up, often require little technical knowledge, and can be adjusted without having physical access to the circuit board. With PCs, the most common use of jumpers is in setting the operating mode for ATA drives (master, slave, or cable select), though this use is declining with the rise of SATA drives. Jumpers have been used since the beginning of printed circuit boards.



DIP switch
Pin header



Jumper Settings Archive at the Wayback Machine (archived October 23, 2007)