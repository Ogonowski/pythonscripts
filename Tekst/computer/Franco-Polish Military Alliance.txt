The Franco-Polish alliance was the military alliance between Poland and France that was active between 1921 and 1940. During the interwar period the alliance with Poland was one of the cornerstones of French foreign policy. Near the end of that period, along with the Franco-British Alliance, it was the basis for the creation of the Allies of World War II.




Already during the France-Habsburg rivalry that started in the 16th century, France had tried to find allies to the east of Austria, hoping to ally with Poland. Poland's King Jan III Sobieski also had the intention to ally with France against the threat of Austria, but the greater threat posed by the Muslim-led Ottoman Empire made him fight for the Christian cause in the Battle of Vienna. In the 18th century, Poland was partitioned by Russia, Prussia and Austria, but Napoleon recreated the Polish state in the Grand Duchy of Warsaw. With the rise of a united German Empire in the 19th century, both France and Poland found a new common enemy.



During the Polish-Soviet War of 1920, France was one of the most active supporters of Poland, and sent the French Military Mission to Poland to aid the Polish army. In early February in Paris, three pacts were discussed by Polish Chief of State J zef Pi sudski and French President Alexandre Millerand: political, military, and economic.
The political alliance was signed there on February 19, 1921 by Polish Minister of Foreign Affairs Count Eustachy Sapieha and his French counterpart Aristide Briand, in the background of the negotiations that ended the Polish-Soviet War (Treaty of Riga). The agreement assumed common foreign policies, promotion of bilateral economical contacts, consultation of new pacts concerning Central and Eastern Europe as well as help in case one of the signatories became a victim of an "unprovoked" attack. As such it was a defensive alliance. The secret military pact was signed two days later, on February 21, 1921, and clarified that the agreement was aimed at possible threats from both Germany and the Soviet Union. In case of aggression on Poland, the French would keep the communication lines free, and keep Germany in check, but was not required to send their troops or to declare war.[1] Both political and military pacts were legally not in force until the economic pact was ratified,[2] which concluded on August 2, 1923.
The alliance was further extended by the Franco-Polish Warrant Agreement signed October 16, 1925 in Locarno, as part of the Locarno Treaties. The new treaty subscribed all previously-signed Polish-French agreements to the system of mutual pacts of the League of Nations.
This alliance was closely tied with the Franco-Czech Alliance. The alliances of France with Poland and Czechoslovakia were aimed at deterring Germany from the use of force to achieve a revision of the postwar settlement or ensuring that German forces would be confronted with significant combined strength of its neighbours. Although Czechoslovakia had a significant economy and industry, and Poland a strong army, the French-Polish-Czechoslovakian triangle never reached its full potential. The Czechoslovakian foreign policy under Edvard Bene  shied from signing a formal alliance with Poland that would force Czechoslovakia to take sides in the Polish-German territorial disputes. Czechoslovakia's influence was weakened by the doubts of its allies as to the trustworthiness of its army, Poland's influence was in turn undermined by the infighting between supporters and opponents of J zef Pi sudski. French reluctance to invest in its allies (especially Polish) industry, strengthening trade relations (buying their agricultural products) and sharing military expertise further weakened the alliance.
In the 1930s the Franco-Polish alliance remained mostly inactive and its only effect was the French Military Mission to Poland, which continued to work with the Polish General Staff ever since the Polish-Soviet War of 1919-1920. However, with the Nazi threat becoming increasingly visible, in the later part of the decade both countries started to seek a new pact that would not only guarantee the independence of all contracting parties but would also ensure military cooperation in case of a war with Germany.



Finally, a new alliance started to be formed in 1939. The Kasprzycki-Gamelin Convention was signed May 19, 1939 in Paris. It was named after the Polish Minister of War Affairs General Tadeusz Kasprzycki and the commander of the French Army Maurice Gamelin. It was military (army-to-army, not state-to-state) convention and was not in force legally, as it was dependent on signing and ratification of political convention.[3] It obliged both armies to provide help to each other in case of a war with Nazi Germany. In May Gamelin promised a "bold relief offensive" within three weeks of German's attack.[4] Finally the treaty became ratified by France on September 4, 1939, on the fourth day of German offensive on Poland.
Despite all the obligations, France provided only token help to Poland during the Polish Defensive War of 1939, in the form of the Saar Offensive. This is often considered an example of Western betrayal. However, the political convention was a basis of the recreation of the Polish Army in France
Piotr Zychowicz quotes memoirs of French ambassador to Poland, Leon Noel, who as early as October 1938 wrote: "It is of utmost importance that we remove from our obligations everything that would deprive French government the freedom of decision on the day when Poland finds itself in war with Germany". Georges Bonnet reassured Noel, writing that "our agreement with Poland is full of gaps, needed to keep our country away from war". (pages 279 - 280)



British-Polish Military Alliance
Cordon sanitaire
Foreign alliances of France
Phoney War
Polish Army in France (1940)
Western betrayal
Why Die for Danzig?




Andrzej Ajnenkiel, Polsko-francuski sojusz wojskowy. Akademia Obrony Narodowej, Warsaw, 2000.
Piotr Stefan Wandycz, The twilight of French eastern alliances. 1926-1936. French-Czechoslovak-Polish relations from Locarno to the remilitarization of the Rheinland., Princeton University Press, 1988 (republished in 2001). ISBN 1-59740-055-6
Piotr Zychowicz, Pakt Ribbentrop - Beck. Dom Wydawniczy Rebis, Pozna  2012. ISBN 978-83-7510-921-4