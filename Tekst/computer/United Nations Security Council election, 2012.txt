The 2012 United Nations Security Council election was held on 18 October 2012 during the 67th session of the United Nations General Assembly, held at United Nations Headquarters in New York City. The elections were for five non-permanent seats on the UN Security Council for two-year mandates commencing on 1 January 2013 to replace the five countries whose terms expired. The countries elected were Argentina, Australia, Luxembourg, the Republic of Korea, and Rwanda.



In accordance with the rotation rules whereby the ten non-permanent UNSC seats rotate among the various regional blocs into which UN member states traditionally divide themselves for voting and representation purposes, the five available seats were allocated as follows:
One for Africa (previously held by South Africa)
One for the Asia-Pacific Group (previously held by India)
One for Latin America (previously held by Colombia)
Two for the Western European and Others Group (previously held by Germany and Portugal)
The election is for the term from 1 January 2013 to 31 December 2014.






 Australia Finland Luxembourg



 Bhutan Cambodia Republic of Korea



 Argentina



 Rwanda



Prior to the actual vote, the representative of the Democratic Republic of the Congo rose to speak and said that Rwanda "was an oasis for criminals" operating in the eastern part of the Democratic Republic of the Congo, and on those grounds, she objected to Rwanda as a non-permanent member of the UN Security Council. A confidential U.N. report was also leaked before the election, which stated that Rwanda is involved in the current conflict in the eastern DRC and that "Rwandan officials exercise overall command and strategic planning for M23 ... Rwanda continues to violate the arms embargo through direct military support to M23 rebels, facilitation of recruitment, encouragement and facilitation of FARDC (Congolese army) desertions as well as the provision of arms and ammunition, intelligence, and political advice." Rwandan U.N. representative Olivier Nduhungirehe responded by saying, "The members of the General Assembly know exactly what our record is and they cannot be deterred or swayed by a baseless report, which has no credibility."



Argentina, Australia, and Rwanda were elected in the first round of voting, while Luxembourg and the Republic of Korea were elected in the second. In both rounds, 193 voting papers were distributed.












List of members of the United Nations Security Council
Australia and the United Nations
Korea and the United Nations
European Union and the United Nations






"UN General Assembly to elect five non-permanent members to Security Council". UN News Centre. Retrieved 2012-10-18.