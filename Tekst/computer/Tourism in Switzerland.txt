Tourists are drawn to Switzerland's Alpine climate and landscapes, in particular for skiing and mountaineering.
As of 2011, tourism accounted for an estimated 2.9% of Switzerland's gross domestic product.




Tourism begins with British mountaineers climbing the main peaks of the Bernese Alps in the early 19th century (Jungfrau 1811, Finsteraarhorn 1812). The Alpine Club in London is founded in 1857. Reconvalescence in the Alpine climate, in particular from Tuberculosis, is another important branch of tourism in the 19th and early 20th centuries for example in Davos, Graub nden. Because of the prominence of the Bernese Alps in British mountaineering, the Bernese Oberland was long especially known as a tourist destination. Meiringen's Reichenbach Falls achieved literary fame as the site of the fictional death of Sir Arthur Conan Doyle's Sherlock Holmes (1893). The first organised tourist holidays to Switzerland were offered during the 19th century by the Thomas Cook and Lunn Travel companies.



Official statistics of tourism were planned since 1852, but were only realized from 1934, and continued until 2003. Since 2004, the Federal Statistical Office had discontinued its own statistics, but collaborates with Switzerland Tourism in the publication of yearly "Swiss Tourism Figures". In the year 2011 as a total number of 4,967 registered hotels or hostels, offering a total of 240,000 beds in 128,000 rooms. This capacity was saturated to 41.7% (compared to 39.7% in 2005), amounting to a total of 38.8 million lodging nights. 14% of hotels were in Grisons, 12% each in the Valais and Eastern Switzerland, 11% in Central Switzerland and 9% in the Bernese Oberland. The ratio of lodging nights in relation to resident population ("tourism intensity", a measure for the relative importance of tourism to local economy) was largest in Grisons (8.3) and Bernese Oberland (5.3), compared to a Swiss average of 1.3. 56.4% of lodging nights were by visitors from abroad (broken down by nationality: 16.5% Germany, 6.3% UK, 4.8% USA, 3.6% France, 3.0% Italy) 
The total financial volume associated with tourism, including transportation, is estimated to CHF 35.5 billion (as of 2010) although some of this comes from fuel tax and sales of motorway vignettes. The total gross value added from tourism is 14.9 billion. Tourism provides a total of 144,838 full time equivalent jobs in the entire country. The total financial volume of tourist lodging is 5.19 billion CHF and eating at the lodging provides an additional 5.19 billion. The total gross value added of 14.9 billion is about 2.9% of Switzerland's 2010 nominal GDP of 550.57 billion CHF.
The major airport of Switzerland is at Zurich, main railway connections are to Geneva, Zurich and Basel. The main connection across the Alps is via the Gotthard tunnels (road and railway).
The most visited Swiss tourist attractions are first, the Rhine Falls, second, the Berne Bear exhibit (both for free), and third, with over 1.8 million paid entries: Zoo Basel.




Notable tourist destinations in Switzerland:
Alpine
Grisons
Engadin
Davos

Eastern Switzerland
Wildhaus

Valais
Aletsch
Anz re
Saas-Fee
Zermatt

Bernese Oberland
Grindelwald
Gstaad
Interlaken

Bernese Alps
Jungfrau

Cities
Z rich
Bern
Lucerne
Basel
Geneva
Lausanne
Canton Ticino
Locarno
Lugano
Ascona



Swiss Federal Railways
Geography of Switzerland
Swiss Alps
List of ski areas and resorts in Switzerland
List of mountain railways in Switzerland
Swiss National Park
List of lakes in Switzerland
Economy of Switzerland






Swiss Federal Office of Statistics
Switzerland Tourism, a national tourism organisation
Tourism in Switzerland , a tourism Website
List of Swiss municipalities, all about Swiss municipalities
Switzerland travel and tourism at DMOZ