The Army Mountain Warfare School (AMWS) is an United States Army school located at the Camp Ethan Allen Training Site, Jericho, Vermont to train soldiers in mountain warfare, the specialized skills required for operating in mountainous terrain. The school is located in Vermont's Green Mountains.



The school teaches a number of courses to train soldiers of the army (including the Army Reserve, Army National Guard, and ROTC Cadets) in military operations in mountainous areas. Graduates of these course receive the special qualification identifier (SQI) of "E," Military Mountaineer.
The school runs 'summer' sessions focusing on rock climbing and high-angle tactical combat, and 'winter' courses that also include winter travel, camping, and survival skills. The basic and advanced courses of instruction trains individual soldiers, not units. These soldiers then return to their units to provide at least some training and experience in mountain warfare throughout the U.S. Army. Unit training, both at the AMWS and at the requesting unit's location, can also be provided.



The school was established in April 1983 to train members of the 3rd Battalion, 172 Infantry Regiment(Mountain) then consisting of company-sized units from several New England states. Enrollment was opened over the years to include members of all branches of the armed services, Federal law enforcement and foreign armies. The curriculum design broadens the soldier's knowledge and unit s capabilities. The curriculum is designed to enable the soldier to operate in mountains and cold and to enable him to assist in planning operations. The school uses a small group participatory learning process.
The school conducts three primary courses of instruction (Basic Military Mountaineer, Advanced Military Mountaineer (Summer) and Advanced Military Mountaineer (Winter). Each level is to prepare soldiers to operate in mountains year-round and in cold, snowy, and/or icy environments. The Army Mountain Warfare School is the only institution that awards the Skill Qualification Identifier (SQI)  E,  Military Mountaineer.
The school is the executive agent for military mountaineering for the United States Army Infantry School. The school is responsible for the content of Field Manual 3 97.61, Military Mountaineering. The school is the only non-European permanent member of the International Association of Military Mountain Schools (IAMMS).
The curriculum is broken down into a two-week course, with three general type of mountain-specific skills taught in each: individual, small unit, and medical. The Basic Military Mountaineer Course is a prerequisite to attend the Advanced Military Mountaineer Course. The training focus is on providing the force structure with soldiers capable of assisting their units in mountainous environments. Courses for the Basic and Advanced Mountaineering Courses run monthly from May through October and January through March.
This school is not to be confused with the Mountain phase of Ranger School.



It is very common for Army ROTC cadets to attend AMWS as a specialty school during ROTC. Cadets take the course during the summer as they are in school, and training during the school season. Other ROTC summer schools include: Airborne School, Air Assault School, Combat Diver Qualification Course, Drill Cadet Leadership Training, and Northern Warfare.


