Alain Menu (born 9 August 1963 in Geneva) is a Swiss racing driver who is currently working for Team BMR as a driving coach. He was one of the most successful touring car drivers of the 1990s, winning the prestigious British Touring Car Championship twice (the only driver during the series' 1991 2000 Super Touring era to do so). He drove for Chevrolet in the World Touring Car Championship between 2005 and 2012 with a best finish of second in 2012.









He is the son of a farmer. Like many drivers who eventually build a career in touring cars, Menu began his career in single-seater racing, reaching the International Formula 3000 championship in 1991 after two years in the British Formula Three Championship and one year in the British Formula 3000 Championship, in which he finished runner-up in 1990. However, for the next year he returned to Great Britain to race a BMW 3 Series in the British Touring Car Championship (BTCC), showing promise before being injured mid-season in a quadbike accident at Knockhill; despite only participating in half of the season, he still managed to finish 9th in the final championship standings. As a result of this accident, he was unable to jog for exercise again   he instead took up cycling as his main exercise.



In 1993 Menu began a six-year association with Renault in the BTCC, who had just entered the series with the GB Motorsport run Renault 19. He finished second in a very wet round two at Donington Park behind team mate Tim Harvey. At the next round at Oulton Park he crashed out of fifth place late on in the race. Menu collided with Nissan Primera of Tiff Needell on the final lap of round eight at Silverstone, dropping Menu for seventh to eighth and forcing Needell into the pits. The Renault team missed the double header at Knockhill and returned to the grid for round ten at Oulton Park. The car had been revised and Menu was sixth after the first few corners having started eighth; he went on to finish fourth behind John Cleland. The first year of their partnership was not particularly successful, with the 19 little better than a midfield runner in the hands of Menu and reigning champion Tim Harvey. However, Menu did manage to win one race at a rain-soaked Donington Park late in the season.

For 1994 the 19 was replaced by the Renault Laguna, which proved to be a much better package all-round. Alfa Romeo had dominated the first four races and Menu didn't finish on the podium until Silverstone. At Oulton Park, Alfa Romeo were told to run their cars without their disputed aerodynamic aids and they left the circuit in protest. This allowed Menu to win the race from pole position and set fastest lap. He collided with Julian Bailey at the first corner of the British Grand Prix support race at Silverstone, triggering a pileup that red flagged the race. He took pole position for round fifteen at Oulton Park by nearly a second over BMW driver Joachim Winkelhock but made a poor start and finished third. Menu scored four further podiums in the last four races to finish second in the championship behind Gabriele Tarquini.
The running of the Renault team was taken over by WilliamsF1 for the 1995 season and Menu was joined by former Toyota driver Will Hoy. Despite relatively little testing, Menu qualified third for the season opener at Donington Park and went on to finish second in the race. He took pole position for the first race at Thruxton and held on to take a narrow victory over Cleland. Menu was running third in round seven at Silverstone when he was forced to retire with mechanical issues but he was able to take part in the second race of the day and he finished fourth. Third place in round nine at Oulton Park was the start of five successive midseason podiums that kept him second in the championship behind Rickard Rydell and then Cleland. Menu was third in the championship after round 21 at Snetterton but three consecutive wins finished the year and elevated him back up to second in the standings.

Menu was partnered with Hoy once again in 1996. The main rival for the Renault team was Audi Sport UK and their 4-wheel drive car, driven by Frank Biela. The season opener at Donington Park saw Menu finish third behind team mate Hoy and Biela. Menu had to start the second race of the day from the back of the grid having failed the ride height check after race one. He recovered to seventh before having to retire with mechanical problems. Menu took pole position for the next event at Brands Hatch but was demoted to second at the start by Biela and those positions stuck until the finish line. The next event at Thruxton saw Menu spin at Church corner in race one and ditch his car into the nearby woodland, he was unhurt but the damage left him on the sidelines for race two. Menu worked his way up into the lead of race one at Snetterton but almost immediately slowed down with car problems and retired. He took the first win of the year for Renault in the next meeting at Brands Hatch, winning both races. After this Menu won twice more on his way to second in the championship, 92 points shy of champion Frank Biela.
He continued with Renault in 1997 and was joined by Renault Spider champion Jason Plato. The car was much more competitive for that year and Menu won the first four races of the season. Rounds five and six at Thruxton took place in wet conditions and the Renaults front row lock-out translated into two third placed finishes for Menu. He was nudged into a spin in round eight at Brands Hatch by Anthony Reid's Nissan but was able to rejoin the race and finish fourth while Reid finished fourteenth. Menu started on pole for a wet round eleven at Donington Park although the two Audis got a better start and led into the first corner. Menu was able to cleanly retake second from John Bintcliffe despite a misted up windscreen. Contact with James Thompson at Knockhill meant Menu retired for the first and only time that season. Just before round 19 at Thruxton, Menu pulled into the pits to fix a broken rear wheel which meant he missed the start and he finished in a non points classified finish for the first time that year. After the second race at Thruxton, Menu had secured the championship crown and so on the final lap of round 21 at Brands Hatch, Menu let team mate Plato through into second place to allow him to close in on Biela in the championship. Menu had taken twelve wins in a dominant car in 1997.
1998 would see the Renault team have a large amount of sponsorship from Nescafe and Menu was once again partnered by Plato. The Renault Laguna was not as dominant as the previous season and Menu won only three races, the first which came at Thruxton in the second race of the year.



Menu switched to Ford in 1999 with 1998 championship runner-up Anthony Reid leaving Nissan. The Ford Mondeo was not successful in 1999 and Menu finished 11th in the championship with 1 win at Knockhill. Menu would win his second title in 2000 against team-mates Anthony Reid and Rickard Rydell. 2000 was to be the last year of the BTCC in that form; the championship had been gradually in decline and losing entrants since 1998, with 2000 only featuring 3 works teams and 12 championship contenders, as opposed to the 9 teams and 20+ championship contenders in 1995. The championship was reformed for 2001 with new regulations, but only two manufacturers committed to the series, and Menu, like most of the star drivers, chose to leave the series.



On 4 October 2007, it was announced that Menu would make a one-off return to the BTCC with which he is most closely associated. He drove a Vauxhall Vectra for VX Racing at the final round of the 2007 BTCC season at Thruxton to assist Fabrizio Giovanardi in his successful title bid.



On 27 January 2014, it was announced that Menu would return to the BTCC, driving a Volkswagen CC for Team BMR. Menu struggled to find the ideal set up in the new breed NGTC Touring Car. He finished 10th in the standings with a pair of podium finishes, 2nd at Rockingham and 3rd at Silverstone.
As of 2015, Menu is no longer a driver for Team BMR, however he remains on the team's roster as a driver coach, helping to train those involved in the British Touring Car Championship, Renault Clio Cup and Ginetta Junior Championship refusing to rule out driving for the team again later in the year.



From 2001 to 2003, Menu raced for Opel in the Deutsche Tourenwagen Masters, a high-tech touring car series based in Germany. However, he achieved little success (Menu would describe his DTM years as "not very good" in a 2004 interview). While competing in the DTM, he ventured into sports car racing with Ferrari, competing in both the Le Mans 24 Hours and the Sebring 12 Hours as well as winning one race each in both the FIA GT Championship and the American Le Mans Series.






In 2004, Menu announced his return to production-based touring car racing, with the fledgling Chevrolet team, RML in the 2005 World Touring Car Championship season. Menu, and his equally highly regarded team-mate Nicola Larini, did not expect to win races in their first year but the team's performance was still seen as a big disappointment, Menu only achieving three points finishes, his best result being a 6th at Spa-Francorchamps.

The team had expected to make progress up the grid in 2006, and their faith was fulfilled at a very wet Brands Hatch on 21 May when Menu won the second race of the day, scoring Chevrolet's first-ever outright win in a FIA-accredited world championship event.
Chevrolet introduced the new Cruze model to the championship for 2009. Menu crashed out of the first race of the season at the Race of Brazil after the collision with BMW pair Andy Priaulx and J rg M ller. At the Race of Morocco, he had originally qualified second but was demoted to tenth by the stewards after his car did not restart following a mandatory visit to the weighing bay after the second phase of qualifying. He was caught up in a race one incident with Jorg M ller and Rickard Rydell which put Rydell out of the race with damage. The Race of France saw Menu take his first win in the Chevrolet Cruze having started on the reversed grid pole for race two and winning the race from there. Menu collided with Augusto Farfus in race one of the Race of Portugal with Menu being spun into the wall. Menu's team mate Larini tagged Tiago Monteiro while the pair tried to avoid the wreckage but Mehdi Bennani collided with the stationary Chevrolet, the track was then blocked and the race red flagged. Menu took pole position at the following event, the Race of UK at Brands Hatch. Having swapped places throughout the race with team mate Huff, Menu won the race to take his second win of the year. During the final qualifying session of the year for the Race of Macau, Yvan Muller lost control on cement dust that had been laid on the track following an earlier accident for James Thompson. Tarquini hit Muller's car, and the SEAT Sport pair were then hit by Menu. He finished the season tenth in the drivers' championship behind fifth placed Huff.

He stayed with Chevrolet for the 2010 season, joined by Huff and new team mate Yvan Muller. Menu brought out the red flags during qualifying for the Race of Morocco when he crashed into the wall during Q1. He came into contact with the wall again during race two when he tangled with BMW Team RBM driver Augusto Farfus. Having been passed by eBay Motors driver Colin Turkington near the end of the race one of the Race of UK, Menu dropped down the order to benefit from the reversed grid for race two. He started on the front row but damaged his steering trying to pass Farfus which put an end to his race. He won only one race in 2010, the first race of the Race of Germany and he finished the year sixth in the standings, three places and 103 points down on team mate Huff while Muller was champion.
Menu continued with Chevrolet for 2012. Muller had won the first three races of the season but Menu broke this streak by winning the second race of the Race of Spain despite going through a gravel trap after encountering oil on the circuit. He took his first pole position of the season at the Race of Morocco and won the first race of the day. Menu was running third in the second race of the Race of Slovakia until he dropped down the order and retired with a broken rim. The second race of the Race of Hungary saw Menu pull away from his team mates to chase down race leader and eventual winner Norbert Michelisz to close in on his team mates in the championship. While running third in race two of the Race of Austria, he suffered a puncture and a high speed collision with the barriers ensued in the final sector of the lap. A similar specification Chevrolet Cruze of bamboo-engineering's Alex MacDowall also had a left front puncture and came to rest in the gravel trap in close proximity to Menu's stranded car. He took up the role of comic book character Michel Vaillant for the Race of Portugal. His Chevrolet Cruze raced in special Vaillante colours and he wore a special race suit and dyed his hair to look like the French comic book racer.
Menu began a run of three successive pole positions at the Race of United States but he retired from the first race with power steering issues. He led a Chevrolet 1 2 3 from pole in the opening race of the Race of Japan and led from start to finish in race one of the Race of China. Menu was pushed into a half spin by team mate Muller in the second race while leading, allowing Huff to go through and win. Muller finished second and Menu finished third but a post race thirty second penalty saw Muller drop to thirteenth in the classification and Menu was elevated to second in the championship. Menu won the final race of the year at Macau and secured his best overall result in the WTCC with second in the drivers' championship, twelve points shy of Huff.
Prior to the Race of Brazil, Chevrolet announced they were ending their WTCC programme, leaving their drivers without seats for 2013.



Having been unable to find a seat in the WTCC, Menu moved to the Porsche Supercup for the 2013 season, driving for team FACH Auto Tech. Some sources stated that FACH Auto Tech is Menu's own team, but he declared that's not true. With the main sponsor of the team not supplying funds, Menu only completed one race of the season.



In 2006 he won the 200 km de Buenos Aires, a round of Argentina's TC2000 championship, with Matias Rossi driving a Chevrolet Astra.



2012: 2nd in World Touring Car Championship, 6 wins (Chevrolet Cruze)
2011: 3rd in World Touring Car Championship, 5 wins (Chevrolet Cruze)
2010: 6th in World Touring Car Championship, 1 win (Chevrolet Cruze)
2009: 10th in World Touring Car Championship, 2 wins (Chevrolet Cruze)
2008: 9th in World Touring Car Championship, 3 wins (Chevrolet Lacetti), 3rd "200 km de Buenos Aires" (TC2000)
2007: 6th in World Touring Car Championship, 5 wins (Chevrolet Lacetti)
2006: 15th in World Touring Car Championship, 1 win (Chevrolet Lacetti), won the "200 km de Buenos Aires" (TC2000)
2005: 16th in World Touring Car Championship (Chevrolet Lacetti)
2004: 4th in class, 11th overall at Le Mans 24 Hours (Ferrari 550 Maranello), 2nd, BTCC Masters Race, Donington Park
2003: 9th in Deutsche Tourenwagen Masters (Opel Astra), 1st in class at Petit Le Mans (Ferrari 550 Maranello)
2002: 9th in Deutsche Tourenwagen Masters (Opel Astra), DNF (engine) at Le Mans 24 Hours (Ferrari 550 Maranello)
2001: 21st in Deutsche Tourenwagen Masters (Opel Astra), 1 win in FIA GT Championship (Ferrari 550 Maranello)
2000: 1st in British Touring Car Championship, 6 wins (Ford Mondeo)
1999: 11th in British Touring Car Championship, 1 win (Ford Mondeo)
1998: 4th in British Touring Car Championship, 3 wins (Renault Laguna)
1997: 1st in British Touring Car Championship, 12 wins (Renault Laguna)
1996: 2nd in British Touring Car Championship, 4 wins (Renault Laguna)
1995: 2nd in British Touring Car Championship, 7 wins (Renault Laguna)
1994: 2nd in British Touring Car Championship, 2 wins (Renault Laguna)
1993: 10th in British Touring Car Championship, 1 win (Renault 19)
1992: 9th in British Touring Car Championship (BMW 3 Series) (only completed half-season)
1991: International Formula 3000 Championship
1990: 2nd in British Formula 3000 Championship
1989: British Formula 3 Championship
1988: British Formula 3 Championship
1987: 2nd in British Formula Ford 1600 Championship, 2nd in FF1600 Festival.
1985: French FF1600 Championship
1984: Enrolled at the Elf Winfield racing school in France.






(key) (Races in bold indicate pole position) (Races in italics indicate fastest lap)



(key) Races in bold indicate pole position (1 point awarded   1996 2000 all races, 2007 just for first race) Races in italics indicate fastest lap (1 point awarded   2007 only) * signifies that driver lead race for at least one lap (1 point awarded   1998 2000 just in feature race, 2007 all races)






(key) (Races in bold indicate pole position) (Races in italics indicate fastest lap)
    Retired, but was classified as he completed 90% of the winner's race distance.






(key) (Races in bold indicate pole position) (Races in italics indicate fastest lap)
    Drivers did not finish the race, but were classified as they completed over 90% of the race distance.



(key) (Races in bold indicate pole position) (Races in italics indicate fastest lap)


