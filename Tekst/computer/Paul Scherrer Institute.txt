The Paul Scherrer Institute (PSI) is a multi-disciplinary research institute which belongs to the Swiss Federal Institutes of Technology Domain covering also the ETH Zurich and the EPFL. It was established in 1988 by merging in 1960 established EIR (Eidgen ssisches Institut f r Reaktorforschung, Federal Institute for Reactor Research) and in 1968 established SIN (Schweizerisches Institut f r Nuklearphysik, Swiss Institute for Nuclear Physics). Currently, it is based in Villigen and W renlingen.
The PSI is a multi-disciplinary research centre for natural sciences and technology. In national and international collaboration with universities, other research institutes and industry, PSI is active in solid state physics, materials sciences, elementary particle physics, life sciences, nuclear and non-nuclear energy research, and energy-related ecology.
It is the largest Swiss national research institute with about 1,900 (year 2014) members of staff, and is the only one of its kind in Switzerland.
PSI is a User Laboratory and runs several particle accelerators. The 590MeV cyclotron, with its 72MeV companion pre-accelerator, is one of them. As of 2011, it delivers up to 2.2mA proton beam, which is the world record for such proton cyclotrons. It drives the spallation neutron source complex. The Swiss Light Source (SLS), built in 2001, is a synchrotron light source with a 2.4GeV electron storage ring. It is one of the world's best with respect to electron beam brilliance and stability. An X-ray free-electron laser called SwissFEL is currently under construction and is slated to begin operation in 2016.
The proton accelerators are also used for the proton therapy program.



Solid-state physics and materials sciences
Elementary particle physics
Life sciences and medicine
Nuclear energy and nuclear safety
Non-nuclear energy
Energy-related ecology






Injector 1 is a variable energy cyclotron built by the Dutch company Philips Gloeilampen-Fabrieken. Its one-piece magnet has an azimuthally varying magnetic field for vertical focusing even at relativistic energies. The beam energy goes up to 72 MeV for protons and 120 MeV Z2/A for ions with charge Z and mass number A. Equipped with several ion sources, Injector 1 offers a wide variety of beams ranging from protons and deuterons to light and heavy ions. Polarized beams of protons and deuterons are also available. In 1994 an ECR ion source was installed to extend its ability to accelerate heavy ions. This machine has been decommissioned on 1 December 2010.



The Injector 2 cyclotron has been built to replace the multiparticle variable energy Injector 1. It provides high intensity, high quality beams of 72 MeV protons to be injected into the 590 MeV Ring cyclotron.
The Injector 2 is itself a ring cyclotron, but with 4 sectormagnets and with an extremely low injection energy of 870 keV. The 870 keV proton beam is achieved by extracting protons from an ion source with 60 kV and additional acceleration of the particles by means of a Cockcroft-Walton type accelerator operated at 810 kV.
It was put in operation in 1984.



The Ring Cyclotron is a separated sector cyclotron with a fixed beam energy of 590 MeV, built by PSI and commissioned in 1974. The 72 MeV beam from either one of two injector cyclotron enters from the back of the cyclotron, is injected into an orbit in the center of the Ring, accelerated over about 186 revolutions and extracted at the full energy.
The design is based on criteria that allow operation at very high beam intensities: an open structure of four large and powerful RF-cavities providing a high acceleration voltage, and a flat-top cavity operating at the third harmonic of the accelerating RF-voltage. The resulting strong, phase-independent energy gain per revolution gives good turn separation and hence beam extraction with low beam losses. This is a mandatory condition for high current operation in a cyclotron. Presently, a continuous beam current of 2.2 mA at 590 MeV can be extracted from the ring cyclotron. This corresponds to a beam power of approximately 1.3 MW turning the PSI-proton facility into the world's most powerful accelerator complex at the moment.



Neutron scattering is one of the most effective ways to obtain information on both the structure and the dynamics of condensed matter. A wide scope of problems, ranging from fundamental to solid state physics and chemistry, and from materials science to biology, medicine and environmental science, can be investigated with neutrons. Aside from the scattering techniques, non-diffractive methods like imaging techniques can also be applied with increasing relevance for industrial applications.
The spallation neutron source SINQ is a continuous source - the first of its kind in the world - with a flux of about 1014 n/cm2/s. Beside thermal neutrons, a cold moderator of liquid deuterium (cold source) slows neutrons down and shifts their spectrum to lower energies. These neutrons have proved to be particularly valuable in materials research and in the investigation of biological substances. SINQ is a user facility. Interested groups can apply for beamtime on the various instruments by using the SINQ proposal system.



Neutrons from the spallation target are brought to thermal energies by means of a heavy water. Some of the neutrons are further cooled down to kinetic energies below 300 Nano-Electronvolts by means of a solid deuterium moderator.
These ultra cold neutrons are used by the nEDM experiment to probe the Neutron electric dipole moment at scales below the current upper limit (|dn| < 6974290000000000000 2.9 10 26e cm)






Thanks to the high proton current (2200  A), the Swiss Muon Source is the world's most intense continuous beam muon source today. One of the main applications of these intense muon beams is Muon spin spectroscopy.



Since 1984 PSI operates the OPTIS facility for treatment of eye tumours. It was the first such installation in Western Europe, developed by PSI physicists. In close cooperation with the 'H pital Opthalmique' of the University of Lausanne by March 2008 nearly 5000 patients have been treated at PSI with this unique method, in which a proton beam is directed accurately onto the eye tumour.
Since 1996 PSI operates also the only compact scanning-Gantry worldwide for proton radiation therapy of deep-seated tumours. The spot-scanning technique developed at PSI enables malignant tumours to be targeted with high precision deep inside in the body, and their growth successfully stopped, without damaging healthy tissue around the target area. By March 2008 320 patients have been treated at the Gantry 1, suffering from brain, head and neck, skull-base, spinal cord or abdominal tumours.
The excellent and promising results of patient treatment have led to the (Project PROSCAN) with the objectives to install a dedicated and compact superconducting proton accelerator and to develop a new Gantry (Gantry 2) with advanced 2-dimensional and fast parallel scanning features. The new compact superconducting cyclotron is in operation since February 2007. The Gantry 2 is under construction and first beam to the isocenter is planned for May 2008. Patient treatment at Gantry 2 will start first half of 2009.
The goals of these developments are to increase radiation precision of even moving tumours and to transfer the knowledge and proton therapy technology into hospital-based projects.
With the expansion of the facility, PSI will be able to strengthen the clinical research program and to treat more than 500 patients per year, including those with eye tumours.



Swiss National Supercomputing Centre



Science and technology in Switzerland
Proton therapy






PSI Homepage
Proton therapy program
High-Intensity-Proton-Accelerators at PSI