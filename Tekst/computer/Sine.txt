Sine, in mathematics, is a trigonometric function of an angle. The sine of an angle is defined in the context of a right triangle: for the specified angle, it is the ratio of the length of the side that is opposite that angle to the length of the longest side of the triangle (i.e., the hypotenuse).
Trigonometric functions are commonly defined as ratios of two sides of a right triangle containing the angle, and can equivalently be defined as the lengths of various line segments from a unit circle. More modern definitions express them as infinite series or as solutions of certain differential equations, allowing their extension to arbitrary positive and negative values and even to complex numbers.
The sine function is commonly used to model periodic phenomena such as sound and light waves, the position and velocity of harmonic oscillators, sunlight intensity and day length, and average temperature variations throughout the year.
The function sine can be traced to the jy  and ko i-jy  functions used in Gupta period Indian astronomy (Aryabhatiya, Surya Siddhanta), via translation from Sanskrit to Arabic and then from Arabic to Latin. The word "sine" comes from a Latin mistranslation of the Arabic jiba, which is a transliteration of the Sanskrit word for half the chord, jya-ardha.



For any similar triangle the ratio of the length of the sides remains the same. For example, if the hypotenuse is twice as long, so are the other sides. Therefore respective trigonometric functions, depending only on the size of the angle, express those ratios: between the hypotenuse and the "opposite" side to an angle A in question (see illustration) in the case of sine function; or between the hypotenuse and the "adjacent" side (cosine) or between the "opposite" and the "adjacent" side (tangent), etc.
To define the trigonometric functions for an acute angle A, start with any right triangle that contains the angle A. The three sides of the triangle are named as follows:
The adjacent side is the side that is in contact with (adjacent to) both the angle we are interested in (angle A) and the right angle, in this case side b.
The hypotenuse is the side opposite the right angle, in this case side h. The hypotenuse is always the longest side of a right-angled triangle.
The opposite side is the side opposite to the angle we are interested in (angle A), in this case side a.
In ordinary Euclidean geometry, according to the triangle postulate the inside angles of every triangle total 180  (  radians). Therefore, in a right-angled triangle, the two non-right angles total 90  ( /2 radians), so each of these angles must be greater than 0  and less than 90 . The following definition applies to such angles.
The angle A (having measure  ) is the angle between the hypotenuse and the adjacent side.
The sine of an angle is the ratio of the length of the opposite side to the length of the hypotenuse. In our case, it does not depend on the size of the particular right triangle chosen, as long as it contains the angle A, since all such triangles are similar.




The trigonometric functions can be defined in terms of the rise, run, and slope of a line segment relative to some horizontal line.
When the length of the line segment is 1, sine takes an angle and tells the rise
Sine takes an angle and tells the rise per unit length of the line segment.
Rise is equal to sin   multiplied by the length of the line segment
In contrast, cosine is used for the telling the run from the angle; and tangent is used for telling the slope from the angle. Arctan is used for telling the angle from the slope.
The line segment is the equivalent of the hypotenuse in the right-triangle, and when it has a length of 1 it is also equivalent to the radius of the unit circle.




In trigonometry, a unit circle is the circle of radius one centered at the origin (0, 0) in the Cartesian coordinate system.
Let a line through the origin, making an angle of   with the positive half of the x-axis, intersect the unit circle. The x- and y-coordinates of this point of intersection are equal to cos   and sin  , respectively. The point's distance from the origin is always 1.
Unlike the definitions with the right triangle or slope, the angle can be extended to the full set of real arguments by using the unit circle. This can also be achieved by requiring certain symmetries and that sine be a periodic function.




Exact identities (using radians):
These apply for all values of .



The reciprocal of sine is cosecant, i.e., the reciprocal of sin(A) is csc(A), or cosec(A). Cosecant gives the ratio of the length of the hypotenuse to the length of the opposite side:




The inverse function of sine is arcsine (arcsin or asin) or inverse sine (sin 1). As sine is non-injective, it is not an exact inverse function but a partial inverse function. For example, sin(0) = 0, but also sin( ) = 0, sin(2 ) = 0 etc. It follows that the arcsine function is multivalued: arcsin(0) = 0, but also arcsin(0) =  , arcsin(0) = 2 , etc. When only one value is desired, the function may be restricted to its principal branch. With this restriction, for each x in the domain the expression arcsin(x) will evaluate only to a single value, called its principal value.

k is some integer:

Or in one equation:

Arcsin satisfies:

and




For the sine function:

The derivative is:

The antiderivative is:

C denotes the constant of integration.




It is possible to express any trigonometric function in terms of any other (up to a plus or minus sign, or using the sign function).
Sine in terms of the other common trigonometric functions:
Note that for all equations which use plus/minus ( ), the result is positive for angles in the first quadrant.
The basic relationship between the sine and the cosine can also be expressed as the Pythagorean trigonometric identity:

where sin2x means (sin(x))2.



Over the four quadrants of the sine function is as follows.
Points between the quadrants. k is an integer.

For arguments outside those in the table, get the value using the fact the sine function has a period of 360  (or 2  rad): , or use . Or use  and . For complement of sine, we have .




Using only geometry and properties of limits, it can be shown that the derivative of sine is cosine, and that the derivative of cosine is the negative of sine.
Using the reflection from the calculated geometric derivation of the sine is with the 4n + k-th derivative at the point 0:

This gives the following Taylor series expansion at x = 0. One can then use the theory of Taylor series to show that the following identities hold for all real numbers x (where x is the angle in radians) :

If x were expressed in degrees then the series would contain messy factors involving powers of  /180: if x is the number of degrees, the number of radians is y =  x /180, so

The series formulas for the sine and cosine are uniquely determined, up to the choice of unit for angles, by the requirements that

The radian is the unit that leads to the expansion with leading coefficient 1 for the sine and is determined by the additional requirement that

The coefficients for both the sine and cosine series may therefore be derived by substituting their expansions into the pythagorean and double angle identities, taking the leading coefficient for the sine to be 1, and matching the remaining coefficients.
In general, mathematically important relationships between the sine and cosine functions and the exponential function (see, for example, Euler's formula) are substantially simplified when angles are expressed in radians, rather than in degrees, grads or other units. Therefore, in most branches of mathematics beyond practical geometry, angles are generally assumed to be expressed in radians.
A similar series is Gregory's series for arctan, which is obtained by omitting the factorials in the denominator.



The sine function can also be represented as a generalized continued fraction:

The continued fraction representation expresses the real number values, both rational and irrational, of the sine function.




Zero is the only real fixed point of the sine function; in other words the only intersection of the sine function and the identity function is sin(0) = 0.



The arc length of the sine curve between  and  is  This integral is an elliptic integral of the second kind.
The arc length for a full period is  where  is the Gamma function.
The arc length of the sine curve from 0 to x is the above number divided by  times x, plus a correction that varies periodically in x with period . The Fourier series for this correction can be written in closed form using special functions, but it is perhaps more instructive to write the decimal approximations of the Fourier coefficients. The sine curve arc length from 0 to x is




The law of sines states that for an arbitrary triangle with sides a, b, and c and angles opposite those sides A, B and C:

This is equivalent to the equality of the first three expressions below:

where R is the triangle's circumradius.
It can be proven by dividing the triangle into two right ones and using the above definition of sine. The law of sines is useful for computing the lengths of the unknown sides in a triangle if two angles and one side are known. This is a common situation occurring in triangulation, a technique to determine unknown distances by measuring two angles and an accessible enclosed distance.




For certain integral numbers x of degrees, the value of sin(x) is particularly simple. A table of some of these values is given below.
90 degree increments:
Other values not listed above:
  A019812
  A019815
  A019818
  A019821
  A019827
  A019830

  A019833
  A019836
  A019842
  A019845
  A019848
  A019851




Sine is used to determine the imaginary part of a complex number given in polar coordinates (r, ):

the imaginary part is:

r and   represent the magnitude and angle of the complex number respectively. i is the imaginary unit. z is a complex number.
Although dealing with complex numbers, sine's parameter in this usage is still a real number. Sine can also take a complex number as an argument.




The definition of the sine function for complex arguments z:

where i 2 =  1, and sinh is hyperbolic sine. This is an entire function. Also, for purely real x,

For purely imaginary numbers:

It is also sometimes useful to express the complex sine function in terms of the real and imaginary parts of its argument:



Using the partial fraction expansion technique in Complex Analysis, one can find that the infinite series

both converge and are equal to .
Similarly we can find

Using product expansion technique, one can derive



sin z is found in the functional equation for the Gamma function,

which in turn is found in the functional equation for the Riemann zeta-function,

As a holomorphic function, sin z is a 2D solution of Laplace's equation:

It is also related with level curves of pendulum.







While the early study of trigonometry can be traced to antiquity, the trigonometric functions as they are in use today were developed in the medieval period. The chord function was discovered by Hipparchus of Nicaea (180 125 BC) and Ptolemy of Roman Egypt (90 165 AD).
The function sine (and cosine) can be traced to the jy  and ko i-jy  functions used in Gupta period (320 to 550 CE) Indian astronomy (Aryabhatiya, Surya Siddhanta), via translation from Sanskrit to Arabic and then from Arabic to Latin.
The first published use of the abbreviations 'sin', 'cos', and 'tan' is by the 16th century French mathematician Albert Girard; these were further promulgated by Euler (see below). The Opus palatinum de triangulis of Georg Joachim Rheticus, a student of Copernicus, was probably the first in Europe to define trigonometric functions directly in terms of right triangles instead of circles, with tables for all six trigonometric functions; this work was finished by Rheticus' student Valentin Otho in 1596.
In a paper published in 1682, Leibniz proved that sin x is not an algebraic function of x. Roger Cotes computed the derivative of sine in his Harmonia Mensurarum (1722). Leonhard Euler's Introductio in analysin infinitorum (1748) was mostly responsible for establishing the analytic treatment of trigonometric functions in Europe, also defining them as infinite series and presenting "Euler's formula", as well as the near-modern abbreviations sin., cos., tang., cot., sec., and cosec.



Etymologically, the word sine derives from the Sanskrit word for chord, jiva*(jya being its more popular synonym). This was transliterated in Arabic as jiba  , abbreviated jb   . Since Arabic is written without short vowels, "jb" was interpreted as the word jaib  , which means "bosom", when the Arabic text was translated in the 12th century into Latin by Gerard of Cremona. The translator used the Latin equivalent for "bosom", sinus (which means "bosom" or "bay" or "fold"). The English form sine was introduced in the 1590s.




The sine function, along with other trigonometric functions, is widely available across programming languages and platforms. In computing, it is typically abbreviated to sin.
Some CPU architectures have a built-in instruction for sine, including the Intel x87 FPUs since the 80387.
In programming languages, sin is typically either a built-in function or found within the language's standard math library.
For example, the C standard library defines sine functions within math.h: sin(double), sinf(float), and sinl(long double). The parameter of each is a floating point value, specifying the angle in radians. Each function returns the same data type as it accepts. Many other trigonometric functions are also defined in math.h, such as for cosine, arc sine, and hyperbolic sine (sinh).
Similarly, Python, defines math.sin(x) within the built-in math module. Complex sine functions are also available within the cmath module, e.g. cmath.sin(z). CPython's math functions call the C math library, and use a double-precision floating-point format.
There is no standard algorithm for calculating sine. IEEE 754-2008, the most widely used standard for floating-point computation, does not address calculating trigonometric functions such as sine. Algorithms for calculating sine may be balanced for such constraints as speed, accuracy, portability, or range of input values accepted. This can lead to different results for different algorithms, especially for special circumstances such as very large inputs, e.g. sin(1022).
A once common programming optimization, used especially in 3D graphics, was to pre-calculate a table of sine values, for example one value per degree. This allowed results to be looked up from a table rather than being calculated in real time. With modern CPU architectures this method may offer no advantage.



 ryabha a's sine table
Bhaskara I's sine approximation formula
Discrete sine transform
Euler's formula
Generalized trigonometry
Hyperbolic function
Law of sines
List of periodic functions
List of trigonometric identities
Madhava series
Madhava's sine table
Optical sine theorem
Polar sine   a generalization to vertex angles
Proofs of trigonometric identities
Sine and cosine transforms
Sine quadrant
Sine wave
Sine Gordon equation
Sinusoidal model
Trigonometric functions
Trigonometry in Galois fields






 Media related to Sine function at Wikimedia Commons