Glencore plc (an acronym for Global Energy Commoditity Resources) is an Anglo Swiss multinational commodity trading and mining company headquartered in Baar, Switzerland, with its registered office in Saint Helier, Jersey. The company was created through a merger of Glencore with Xstrata on 2 May 2013. As of 2014, it ranked tenth in the Fortune Global 500 list of the world's largest companies. It is the world's third-largest family business.
As Glencore International, the company was already one of the world's leading integrated producers and marketers of commodities. It was the largest company in Switzerland and the world's largest commodities trading company, with a 2010 global market share of 60 percent in the internationally tradeable zinc market, 50 percent in the internationally tradeable copper market, 9 percent in the internationally tradeable grain market and 3 percent in the internationally tradeable oil market.
Glencore had a number of production facilities all around the world and supplied metals, minerals, crude oil, oil products, coal, natural gas and agricultural products to international customers in the automotive, power generation, steel production and food processing industries. The company was formed in 1994 by a management buyout of Marc Rich + Co AG (itself founded in 1974). It was listed on the London Stock Exchange in May 2011 and was a constituent of the FTSE 100 Index. It has a secondary listing on the Hong Kong Stock Exchange. Glencore's shares started trading on the Johannesburg Stock Exchange in November 2013.






According to an Australian Public Radio report, "Glencore's history reads like a spy novel". The company was founded as Marc Rich & Co. AG in 1974 by billionaire commodity trader Marc Rich, who was charged with tax evasion and illegal business dealings with Iran in the US, but pardoned by President Bill Clinton in 2001. He was never brought before US courts before his pardoning, therefore there was never a verdict on these charges.
In 1993, commodity trading and marketing company Trafigura was "split off from" Marc Rich's group of companies. As physical commodities traders, along with Trafigura, Glencore's main rivals in 2011 were identified as Vitol and Cargill, amongst a number of others.
In 1993 and 1994, after failing to control the zinc market, losing $172 million, its founder Marc Rich was forced to sell his 51 percent stake majority share in his own company Marc Rich & Company AG to Glencore International, the commodities trading and industrial company. Glencore International had a 21-year relationship with its founder Marc Rich.



Due to weak global prices for the assets Glencore owned, particularly coal and copper producers, and the commodities Glencore traded in, the company showed a net loss of $676 million for its operations during the first half of 2015. As of September 2015 the value of its stock had fallen significantly. Concerns cited by financial analysts included the weak global commodity market and Glencore's high debt. The company was reducing its $30 billion debt by selling stock and assets.



In 2005, proceeds from an oil sale to Glencore were seized as fraudulent, in an investigation into corruption in the Democratic Republic of Congo (Allen-Mills 17 June 2008).
In a 2011 article by Reuters journalists described Glencore as the biggest company you never heard of. The article described how Glencore and Dan Gertler partnered in Nikanor from 2007 until its final merger with Katanga Mining. According to Reuters, the Nikanor PLc acquisition was an example of Glencore's opportunistic, contrarian, well-funded investment approach focusing on equity participation, controlling interest, and working upstream from trading relationships.

The acquisition was the culmination of 18 months of deal-making in Congo... [including fighting off a counterbid by] former England cricketer Phil Edmonds.... [Starting i]n June 2007, Glencore and partner Dan Gertler, an Israeli mining magnate, paid  300 million for a quarter-stake in mining company Nikanor, which was seeking to revive derelict copper mines next to Katanga Mining's properties. That deal gave Glencore exclusive rights to sell all Nikanor's output   an "offtake" agreement.... [Then, o]n Christmas Eve 2008, ... [having] lost 97 percent of its market value over the previous six months ... in the depths of the global financial crisis and ... running out of cash, Katanga accepted a lifeline it could not refuse. [Glencore] wanted control. For about US$500 million in a convertible loan and rights issue, Katanga agreed to issue more than a billion new shares and hand what would become a stake of 74 percent to Glencore. ... [By early 2011], with copper prices regularly setting records above US$10,000 a ton, Katanga's stock market value [had reached] nearly US$3.2 billion.... [Since the Glencore acquisition], Katanga ... is reaping the benefit of the surging markets and its wealthy, powerful owner. After losing US$108 million in 2009, it posted an annual profit of US$265 million in 2010.

In the course of the Congo events, Nikanor was merged into Katanga in late 2007 in a transaction valued at US$3.3 billion.
In May 2009, Glencore announced it would manage Brazilian bankrupted agricultural products company Agrenco.
In early 2011, the Reuters report included speculation that, after an Initial Public Offering (IPO), Glencore could develop an interest in London/Kazakh Eurasian Natural Resources Corporation. Glencore said that, contrary to recent reports, it was not interested in bidding for the under-fire group.
In May 2011 the company launched an IPO valuing the business at US$61 billion and creating five new billionaires. Trading was limited to institutional investors for the first week and private investors were only allowed to buy the shares from 24 May 2011.



Along with several other major coal producers, Glencore is also a large shareholder in globalCOAL, the online physical coal trading platform. During the shareholder formation of globalCOAL, Glencore suggested selling all their coal through the platform if the other producers did the same. This notion was rejected and the board of globalCOAL also contains a number of power utility shareholders. Relationships also exist with Century Aluminum Co. (CENX; 44% economic ownership interest)) in the US; Glencore partial subsidiary Minara Resources Ltd (AU:MRE), a 70.5% stake in one of Australia s top three nickel producers); and 8.8% in United Company Rusal (HK:486), the Russian aluminium giant that went public in 2010.
In mid-2011, Century was called "one of the most harrowing stocks of the past few years" but identified as a risky but potentially profitable investment for the future.



When the commodities group, Glencore International made its Initial Public Offering (IPO) in May 2011 in dual listing, London and Hong Kong, valued at about $US60 billion, it was obliged by IPO regulations to provide a prospectus. The 1,637-page revealed invaluable information about this private company that has remained discreet for thirty-seven years. With the IPO, Glasenberg shares would fall from 18.1% before the IPO percent to 15.8% after the offering. Daniel Mate and Telis Mistakidis, zinc, copper and lead co-directors would fall from 6.9 percent to 6 percent. Glencore, the mining-to-trading giant went public to raise gross proceeds of around $10 billion. According to Reuters, Glencore is known for its "opportunistic but lucrative acquisition strategy."
In May 2011, United Arab Emirates state-owned Aabar Investments confirmed an investment of $850 million in Glencore International plc as a cornerstone investor with an intention to invest an additional $150 million in the Global Offer. The investment made Aabar the largest cornerstone investor in the initial public offering (IPO) and the largest new shareholder of Glencore post its IPO giving the investment company a 1.4% stake. The two firms intend to explore areas of co-operation between them.  
In November 2012 Abu Dhabi's Aabar Investments, a unit of Abu Dhabi's state-owned United Arab Emirates International Petroleum Investment, wrote off more than $392-million of its $1-billion investment in Glencore, less than two years after investing $1-billion in Glencore's record Initial Public Offering listing. Aabar Investments was the largest new shareholder in Glencore.



Prior to its merger with Xstrata, Glencore is reported to have served as a marketing partner for the company. As of 2006, Glencore leaders Willy Strothotte and Ivan Glasenberg were on the board of Xstrata, which Strothotte chaired. According to The Sunday Times, Glencore controlled 40% of Xstrata stock and appointed the Xstrata CEO, Mick Davis.
In July 2012, Xstrata PLC announced that the Court Meeting originally scheduled for 12 July 2012, to approve the details of the merger between Xstrata and Glencore had been adjourned to 7 September 2012.  After the merger with Glencore, the Xstrata CFO Trevor Reid announced that he would not continue to work as employee but as consultant. After 11 years of involvement, this marked a massive shift in the company's strategy and the group was entering a post-Reid era.
In February 2012, Glencore International Plc, agreed to buy Xstrata Plc for  39.1 billion (US$62 billion) in shares. Glencore offered 2.8 new shares for each Xstrata share in agreed all-share "merger of equal". It is the biggest mining takeover and after approval for the plan would create an entity with 2012 sales of US$209 billion. In June 2012, Glencore and Xstrata began to reconsider the proposed retention package for their merger, following shareholder opposition to a huge payout for executives. In total, 73 key executives stood to receive over GBP 170 million under the initial retention package.  In July 2012, Xstrata PLC announced that the Court Meeting originally scheduled for 12 July 2012, to approve the details of the merger between Xstrata and Glencore had been adjourned to 7 September 2012. Glencore raised the offer to US$82 billion. 
In October 2012, BBC News reported that Glencore had more ships than the British Royal Navy. Glencore's operations in 40 countries handled 3% of the world's oil consumption. Xstrata's operations in more than 20 countries employed 70,000 people. According to mining analyst John Meyer, if the two companies merged into Glencore Xstrata, they would be the 4th largest commodities trader in the world.
Just before completing its forced April 2013 takeover of mining rival Xstrata as it awaited Chinese regulatory approval for its long-planned merger, the world's largest diversified commodities trader, Glencore's annual income fell 25 percent, as its trading division offset the impact of weak commodity prices. Including the impact of an impairment related to a reclassification of its holding in Russian aluminium producer RUSAL, net income fell 75 per cent.
On 2 May 2013, it completed its long-awaited merger with Xstrata. On 20 May 2014, Glencore Xstrata changed its name to Glencore plc following the 2014 AGM.



In 2011 five non-government organisations filed a complaint to the OECD against a subsidiary of Glencore over allegations that a mine it owns in Zambia may not be paying enough tax on its profits. The cause for the complaint lies in the financial and accounting manipulations performed by the two companies' subsidiary, Mopani Copper Mines Plc (MCM), to evade taxation in Zambia. A draft Grant Thornton report alleged that tax avoidance by Glencore in Zambia cost the Zambian Government hundreds of millions of dollars in lost revenue. The avoidance was alleged to have been facilitated through mechanisms such as transfer pricing and inflating costs at Glencore's Mopani Copper Mine. The Mopani mines are controlled through the British Virgin Islands, a recognised tax haven. Glencore and its auditor Deloitte rejected these allegations.



ABC Radio reported in 2005 that Glencore "has been accused of illegal dealings with rogue states: apartheid South Africa, USSR, Iran, and Iraq under Saddam Hussein", and has a "history of busting UN embargoes to profit from corrupt or despotic regimes". Specifically, Glencore was reported to have been named by the CIA to have paid $3,222,780 in illegal kickbacks to obtain oil in the course of the UN oil-for-food programme for Iraq. The company denied these charges, according to the CIA report quoted by ABC.



Swiss public television (TSR) reported in 2006 that allegations of corruption and severe human rights violations were being raised against Glencore on account of the alleged conduct of its Colombian Cerrej n mining subsidiary. Local union president Francisco Ramirez was reported to have accused Cerrej n of forced expropriations and evacuations of entire villages to enable mine expansion, in complicity with Colombian authorities. According to TSR, a representative of the local Wayuu Indians also accused Colombian paramilitary and military units, including those charged with Cerrej n mining security, of forcibly driving the Wayuu off their land, in what she described as a "massacre". Glencore refuted the court's ruling.
Glencore/Xtrata's "huge coal operation in Colombia, Prodeco, was fined a total of nearly $700,000 in 2009 for several environmental violations [running in earlier years], including waste disposal without a permit and producing coal without an environmental management plan."
A BBC investigation in 2012 uncovered sale documents showing the company had paid the associates of paramilitary killers in Colombia. In 2011, a Colombian court had been told by former paramilitaries that they had stolen the land so they could sell it on to Glencore subsidiary Prodeco, to start an open-cast coal mine; the court accepted their evidence and concluded that coal was the motive for the massacre. Glencore refuted the allegations.



"In Ecuador, the current government has tried to reduce the role played by middle men such as Glencore with state oil company Petroecuador" due to questions about transparency and follow-through, according to Fernando Villavicencio, a Quito-based oil sector analyst.



According to a Reuters article in 2011 "[O]fficials in Zambia believe pollution from Glencore's Mopani mines is causing acid rain and health problems in an area where 5 million people live." The upgrade of the Mopani Mines asset plant was completed in March 2014 eliminating the emissions of 97 per cent of sulphur dioxide emissions in line with the recommended international standards by the World Health Organisation (WHO).
An audit report from 2009 revealed, an unexplained increase in operating costs in 2007 (+ $380 million), stunningly low reported volumes of extracted cobalt when compared to similar mining companies operating in the region, and manipulations of copper selling prices in favor of Glencore which constitute a violation of OECD s  arm s length  principle. The result of those various processes was to lower MCM s net income for the 2003-2008 period by several hundreds of millions of dollars, hereby substantially lightening the company s tax burden.



The company's Luilu copper refinery uses acid to extract the copper. For three years after taking over the mine it continued to allow the waste acid to flow into a river. The chief executive, Ivan Glasenberg, was interviewed for Panorama by John Sweeney and said 'It was impossible to remedy any way faster' Glencore said the pollution started long before the company took over the refinery and that the pollution has now ended.
They have also come under scrutiny for acquiring illicit "conflict minerals" In a detailed letter sent to Global Witness, the company denied any wrongdoing.
Glencore acquired stakes in the Kansuki mine in Congo's southern Katanga Province in 2012. According to Global Witness, Congo's government transferred a 75 per cent stake in Kansuki mine in secret and at vastly undervalued prices in July 2010 to a company in which Dan Gertler, who is a close friend of President Joseph Kabila, has an interest. Just a month later in August 2010, Glencore took half the shares of the company that acquired that 75 per cent stake, becoming the operator of the mine. Glencore is financing the entire development of the Kansuki mine, thereby carrying the costs for its other partner companies which are associated with Mr Gertler. Glencore said at the time "During the period when these transactions took place, Glencore had decided in general not to increase its shareholdings in DRC projects."
Glencore acquired a 50 per cent share in SAMREF Congo SPRL in 2007, a Congolese registered company holding 80% of the Mutanda mine. According to Global Witness, SMREF Congo SPRL recommended on 1 March 2011 that Congo's state-run company Gecamines, holding the other 20% share in the Mutanda Mine, sell this share to Rowny Assets Limited, an entity associated again with Dan Gertler. The state-owned share was sold in secret and undervaluated. Glencore has been designated operator of the Mutanda Mine. Glencore has responded a number of times to Global Witness regarding these allegations.



In 2011, Glencore was listed on the London Stock Exchange. In the same year, chairman Simon Murray opposed quotas for women on boards, claiming that women are not so ambitious in business as men because they've got better things to do. Quite often they like bringing up their children and all sorts of other things, remarks for which he later apologised.
By 2014, Glencore was the only blue chip company with no female member of the board, giving it what the Financial Times called pariah status.
Following criticism from Government Minister Vince Cable and objections by major shareholders, including Aviva Investors and the Local Authority Pension Fund Forum (LAPFF), Glencore promised to appoint a woman director.
In June 2014, Glencore appointed Patrice Merrin as its first female director.



In 2013 and 2014, a subsidiary of Glencore Xstrata was awarded two licences off occupied Western Sahara. The licences are awarded in clear violation of international law, as described by the UN in 2002.



In May 2014 the company announced it would close its Newlands underground coal mine in Queensland, Australia in late 2015. The mine which began its life in 1983 produced 2.8 million tonnes of thermal coal in 2013. The company had earlier suspended operations at its Ravensworth underground mine following falling coal prices, escalating production costs, and a higher Australian dollar.



As of October 2014:
Tony Hayward (Non-executive Chairman)
Ivan Glasenberg (CEO)
Peter Grauer (Non-executive Director)
Peter Coates (Non-executive Director)
Leonhard Fischer (Non-executive Director)
William Macaulay (Non-executive Director)
John Mack (Non-executive Director)
Patrice Merrin (Non-executive Director)






Ammann, Daniel (2009). The King of Oil: The Secret Lives of Marc Rich. New York: St. Martin's Press. ISBN 0-312-57074-0. 




Official website
Glencore companies grouped at OpenCorporates
A visual Relationship Map of Glencore executive board and stakeholders with their connections.
Glencore CEO says IPO demand, commodity outlook strong
Special report: The biggest company you never heard of, Reuters, 25 February 2011
The rise of Glencore, the biggest company you've never heard of, The Guardian, 19 May 2011
A Giant Among Giants, Foreign Policy, May/June 2012