Tractor Supply Company (TSCO) is a large retail chain of stores that offers products for home improvement, agriculture, lawn and garden maintenance, and livestock, equine and pet care. It is a leading U.S. retailer in its market.




Founded as a mail order tractor parts business in 1938, the first Tractor Supply Co. retail store was founded in 1939 in Minot, North Dakota. TSC is headquartered in Brentwood, Tennessee, a suburb of Nashville.
In 2004, when the company reported revenues of more than US$1.7 billion, Fortune magazine named Tractor Supply to its list of the 100 fastest growing businesses. As of March 28, 2015, Tractor Supply Company operated 1,422 stores in 49 states, and operated an online store at TractorSupply.com.
On October 10, 2013, the company celebrated its 75th anniversary by ringing the opening bell at the Nasdaq stock exchange in New York.



Tractor Supply lists five major product categories. The livestock and pet products category accounted for 44 percent of the company's sales in 2014. The category with the second highest sales was hardware, tools, truck and towing products, with 22 percent of sales. Seasonal products such as lawn and garden equipment, gifts and toys were 20 percent of sales, followed by clothing and footwear with 9 percent and agricultural products with 5 percent of sales.



The company has conducted advertising campaigns featuring the slogan "The Stuff You Need Out Here." Their mission statement is: "To work hard, have fun and make money by providing legendary service and great products at everyday low prices."



In 2002, TSC was part of a group (also including four liquidation firms) that purchased some of the leases of the bankrupt Quality Stores, Inc., a Michigan-based company which at one time had operated nearly 700 stores in 30 states under the names CT Farm & Country; Country General; Quality Farm & Fleet; County Post; Central Farm and Fleet, and FISCO.
The company also owns Del's Feed and Farm Supply, a farm retail chain in the Pacific Northwest and Hawaii.



In November 1966, TSC opened its first Ontario location in London. In 1987, when there were ten Ontario locations, the American parent company sold these locations to a management team led by Murray Cummings. As of July 2015, there were 51 corporate-owned TSC stores in Ontario, mostly in the southwest, and two stores in Manitoba (Winkler and Brandon). In Canada, TSC brands itself as "The Incredible Country Hardware Store."
From 2007 to 2009, TSC Stores was title sponsor of The Dominion Tankard curling championship in Southern Ontario.






Eddy, Nelson (2004). Work hard, have fun, make money: the Tractor Supply story. Brentwood, Tenn.: Tractor Supply Co. ISBN 0976106604. 



Tractor Supply Company