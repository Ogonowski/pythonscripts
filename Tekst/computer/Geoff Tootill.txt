Geoff Tootill (born March 4, 1922) is a noted computer scientist who worked at Manchester University with Frederic Calland Williams and Tom Kilburn developing "the world's first wholly electronic stored program computer".



Hollingdale, S. H., & Tootill, G. C. (1967). Electronic computers Harmondsworth, Mddx.: Penguin Books.


