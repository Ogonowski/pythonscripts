In Switzerland, most of the people have a regular sport activity and one in four is an active member of a sports club. The most important all-embracing organisations for sports in Switzerland are the Federal Office of Sport, and the Swiss Olympic Committee (Swiss Olympic).
Because of its varied landscape and climate, Switzerland offers a large variety of sports to its inhabitants and visitors. While winter sports are enjoyed throughout the country, football and ice hockey remain the most popular sports.
Major sport events in Switzerland include the Olympic Games, which were held two times in St. Moritz in Winter 1928 and Winter 1948, and, the 1954 FIFA World Cup, the UEFA Euro 2008 in Switzerland and Austria.



Skiing and mountaineering are much practiced by Swiss people and foreigners, the highest summits attract mountaineers from around the world.
Curling has been a very popular winter sport for more than 30 years. The Swiss teams have won 3 World Men's Curling Championships and 2 Women's titles. The Swiss men's team skipped by Dominic Andres won a gold medal at 1998 Nagano Winter Olympics.
St phane Lambiel, two-time winner of the World Figure Skating Championships amongst numerous other domestic and international competitions, is one of the world's top figure skaters.



Like many other Europeans, most Swiss are fans of association football and the national team or 'Nati' is widely supported. The national team has previously participated at seven different FIFA World Cups (last in 2014) and two different UEFA European Championships (last in 2008 as a co-host with Austria).
At club level Grasshopper Club Z rich holds the records for winning the most national championship titles (27) and the most Swiss Cup trophies (19). More recently FC Basel enjoyed great success on a national (winning 7 championship titles in the last 10 years) and international level (qualifying 5 times for the UEFA Champions League Group stage).



Most Swiss people also follow ice hockey and support one of the 12 clubs in the National League A, which, as of 2015, is the most-attended European ice hockey league.
In April-May 2009, Switzerland hosted the Ice Hockey World Championships for the 10th time. The Swiss national ice hockey team's latest achievement is the silver medal at the 2013 World Ice Hockey Championships in Sweden.




Switzerland (Mies) is home to the headquarters of FIBA, the world's governing agency for international events. Unsurprisingly, the country is one of FIBA's founding members and therefore has one of the world's longest basketball traditions.
Once a major team at the international scene, its national team does not have major international significance anymore, despite occasional strong showings at qualification games.
The country has brought forth two NBA Players, Thabo Sefolosha and Clint Capela.




Swiss rugby dates back over a century.
More recently, 2006-07 Heineken Cup clash between the French side Bourgoin and Irish rugby's Munster was moved from Bourgoin's home ground, to the Stade de Gen ve (Geneva Stadium). The stadium's capacity is 30,000, and attendance on the day was 16,255.




Over the last few years several Swiss tennis players, like Roger Federer, Stanislas Wawrinka and Martina Hingis, became Grand Slam singles champions. Federer has won 17 Grand Slam Titles and holds the record for the longest consecutive stay as the world number 1.
Switzerland is also the home of the sailing team Alinghi which won the America's Cup in 2003 and defended the title in 2007. Golf is becoming increasingly popular, with already more than 35 courses available and more in planning. Andr  Bossert is a successful Swiss professional golfer.
The Switzerland national beach football team won the Euro Beach football Cup in 2005 and were runners-up twice, in 2008 Euro Beach football Cup and 2009 Euro Beach football Cup. More recently, they were also runners-up in the 2009 FIFA Beach football World Cup that took place in November.
Other sports where the Swiss have been successful include athletics, (Werner G nth r and Markus Ryffel), fencing, (Marcel Fischer), cycling, (Fabian Cancellara), kickboxing (Andy Hug), whitewater slalom (Ronnie D rrenmatt canoe, Mathias R thenmund kayak), beach volleyball (Sascha Heyer, Markus Egger, Paul and Martin Laciga), professional wrestling (Claudio Castagnoli), and triathlon.
Motorsport racecourses and events were banned in Switzerland following the 1955 Le Mans disaster with exception to events such as Hillclimbing. On June 6, 2007 an amendment to lift the ban was passed by the lower house of the Swiss parliament. The proposed law failed to pass the upper house, and was withdrawn in 2009 after being rejected twice.
The country has produced successful racing drivers such as Clay Regazzoni, Jo Siffert and successful World Touring Car Championship driver Alain Menu. Switzerland also won the A1GP World Cup of Motorsport in 2007-08 with driver Neel Jani. Swiss motorcycle racer Thomas L thi won the 2005 MotoGP World Championship in the 125cc category and Marcel F ssler in the World Endurance Championship.
High profile drivers from Formula One and World Rally Championship such as Michael Schumacher, Nick Heidfeld, Kimi R ikk nen, Fernando Alonso, Lewis Hamilton, S bastien Loeb and Sebastian Vettel all have a residence in Switzerland, sometimes for tax purposes.
Cycling: Fabian Cancellara nicknamed 'Spartacus' is one of the best road racer of modern times. He has achieved great success in the classics; he has won Paris Roubaix three times, the Milan   San Remo once, and the Tour of Flanders three times. Cancellara has won the opening stage of the Tour de France five times and has led the race for 28 days total, which is the most of any rider who has not won the Tour. His success has not been limited to just time trials and classics, as he has won general classification of the Tirreno Adriatico, Tour de Suisse, and the Tour of Oman. In 2008, he won gold in the individual time trial and silver in the men's road race at the Summer Olympics. In addition, Cancellara has been the time trial world champion four times in his career.




Traditional sports include Swiss wrestling or "Schwingen". It is an old tradition from the rural central cantons and considered the national sport by some. Hornussen is another indigenous Swiss sport, which is like a cross between baseball and golf. Steinstossen is the Swiss variant of stone put, a competition in throwing a heavy stone. Practiced only among the alpine population since prehistoric times, it is recorded to have taken place in Basel in the 13th century. It is also central to the Unspunnenfest, first held in 1805, with its symbol the 83.5 kg stone named Unspunnenstein.



See: Federal Department of Defence, Civil Protection and Sports



Lauberhorn
Patrouille des Glaciers
Weltklasse Z rich
Athletissima
Davidoff Swiss Indoors
Allianz Suisse Open Gstaad
Swiss Cup
Spengler Cup



Swiss Sports Personality of the Year
Switzerland at the Olympics
Football in Switzerland
Switzerland men's national ice hockey team
Roger Federer
Martina Hingis
Thabo Sefolosha, the first Swiss NBA player
Antonio Cesaro






 Media related to Sports in Switzerland at Wikimedia Commons
Federal Office of Sport
Swiss Olympic