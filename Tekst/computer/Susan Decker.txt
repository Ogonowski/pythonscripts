Susan (Sue) Lynne Decker (born 1962) is an American businesswoman. She was president of Yahoo! Inc in 2007 and 2008, leading the operations of the company while Jerry Yang's was chief executive officer.






Decker received her Bachelor of Science degree in computer science and economics from Tufts University, and a Masters of Business Administration from Harvard Business School. She is also a Chartered Financial Analyst.
Prior to joining Yahoo, she worked at the U.S. investment bank Donaldson, Lufkin & Jenrette for 14 years. She spent 12 years as an equity research analyst, providing coverage to institutional investors on more than 30 media, publishing, and advertising stocks. In this capacity, she was listed by Institutional Investor magazine as a top rated analyst for ten consecutive years. She subsequently became the global director of equity research, a $300 million operation.



Decker was chief financial officer and executive vice-president of finance and administration of Yahoo from 2000 to 2007. From 2006 to 2007 she was executive vice president of the Advertiser and Publisher Group in addition to her CFO responsibilities, where she led a consortium with the newspaper industry, and launched a display ad platform.
Decker was under consideration as CEO during Jerry Yang's CEO leadership, when the company was receiving a takeover offer from Microsoft. Decker drove a proposed search deal with Google as an alternative to a transaction with Microsoft, but this was rejected by the U.S. Justice Department as a breach of United States antitrust law. The company asked Yang to resign, and for the board to select an outside candidate for the CEO position. Decker and Yang were unable to resolve shareholder concerns as Yahoo's share price fell in 2007 and 2008 during Google's ascendency. Decker announced her intention to resign from the company on January 13, 2009, following the appointment of Carol Bartz as CEO.



Decker is on the boards of directors of Berkshire Hathaway, Intel Corporation, Costco, and LegalZoom. She was on the board of directors of Pixar Animation Studios from June 2004 to May 2006, until its sale to The Walt Disney Company, and on the board of the Stanford Institute for Economic Policy Research from March 2005 to May 2007. She was named a Trustee of Save The Children in March 2010.
In the fall of 2009, Decker became an Entrepreneur In Residence at the Harvard Business School, working with students in their own ventures and helping to develop and deliver the entrepreneurship-focused curriculum of the school's Immersion Experience Program.



On September 30 2010, Decker received Harvard Business School's Alumni Achievement Award.



Decker has three children and lives in Marin County.


