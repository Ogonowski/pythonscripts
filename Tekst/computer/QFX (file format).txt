QFX is Intuit's proprietary version of the standard OFX financial interchange file format. QFX is used in Intuit's "Web Connect" and "Direct Connect" features to transmit and receive financial information over the internet.
A QFX file is a standard OFX file with additional fields to support a licensing fee paid by institutions to Intuit. In contrast, the standard OFX format is a free and open standard. Intuit's Quicken software will only import QFX files where the providing institution has paid the fee and in some cases passed quality tests, otherwise giving the error message "Quicken is currently unable to verify the financial institution information for this download". The same error message is given for Quicken users attempting to import previously downloaded QFX files with non-supported versions of Quicken (i.e. Quicken 2011 and earlier).



Open Financial Exchange


