In logic, false or untrue is a truth value or a nullary logical connective. In a truth-functional system of propositional logic it is one of two postulated truth values, along with its negation, truth. Usual notations of the false are 0 (especially in Boolean logic and computer science), O (in prefix notation, Opq), and the up tack symbol  .
Another approach is used for several formal theories (for example, intuitionistic propositional calculus) where the false is a propositional constant (i.e. a nullary connective)  , the truth value of this constant being always false in the sense above.



Boolean logic defines the false in both senses mentioned above: "0" is a propositional constant, whose value by definition is 0. In a classical propositional calculus, depending on the chosen set of fundamental connectives, the false may or may not have a dedicated symbol. Such formulas as p    p and  (p   p) may be used instead.
In both systems the negation of the truth gives false. The negation of false is equivalent to the truth not only in classical logic and Boolean logic, but also in most other logical systems, as explained below.



In most logical systems, negation, material conditional and false are related as:
 p   (p    )
This is the definition of negation in some systems, such as intuitionistic logic, and can be proven in propositional calculi where negation is a fundamental connective. Because p   p is usually a theorem or axiom, a consequence is that the negation of false (   ) is true.
The contradiction is a statement which entails the false, i.e.      . Using the equivalence above, the fact that   is a contradiction may be derived, for example, from    . Contradiction and the false are sometimes not distinguished, especially due to Latin term falsum denoting both. Contradiction means a statement is proven to be false, but the false itself is a proposition which is defined to be opposite to the truth.
Logical systems may or may not contain the principle of explosion (in Latin, ex falso quodlibet),      .




A formal theory using " " connective is defined to be consistent if and only if the false is not its theorem. In the absence of propositional constants, some substitutes such as mentioned above may be used instead to define consistency.



Contradiction
Logical truth
Tautology (logic) (for symbolism of logical truth)


