The Wikimedia Foundation (WMF) is an American non-profit and charitable organization headquartered in San Francisco, California, that operates many wikis. The foundation is mostly known for hosting Wikipedia, the Internet encyclopedia, as well as Wiktionary, Wikiquote, Wikibooks, Wikisource, Wikimedia Commons, Wikispecies, Wikinews, Wikiversity, WikiData, Wikivoyage, Wikimedia Incubator, and Meta-Wiki. It also owned the now-defunct Nupedia.
The organization was founded in 2003 by Jimmy Wales, co-founder of Wikipedia, as a way to fund Wikipedia and its sister projects through non-profit means.
As of 2015, the foundation employs around 250 people, with annual revenues in excess of US$50 million. Lila Tretikov leads the foundation as its executive director, and Patricio Lorente is chairman of the board.



The Wikimedia Foundation falls under section 501(c)(3) of the US Internal Revenue Code as a public charity. Its National Taxonomy of Exempt Entities (NTEE) code is B60 (Adult, Continuing education). The foundation's by-laws declare a statement of purpose of collecting and developing educational content and to disseminate it effectively and globally.
The Wikimedia Foundation's stated goal is to develop and maintain open content, wiki-based projects and to provide the full contents of those projects to the public free of charge.



In 2001, Jimmy Wales, an Internet entrepreneur, and Larry Sanger, a software developer, founded Wikipedia, an Internet encyclopedia. The project was originally funded by Bomis, Wales' for-profit business. As Wikipedia's popularity skyrocketed, revenues to fund the project stalled. Since Wikipedia was depleting Bomis' resources, Wales and Sanger thought of a charity model to fund the project. The Wikimedia Foundation was then created from Wikipedia and Nupedia on June 20, 2003. It applied to the United States Patent and Trademark Office to trademark Wikipedia on September 17, 2004. The mark was granted registration status on January 10, 2006. Trademark protection was accorded by Japan on December 16, 2004, and, in the European Union, on January 20, 2005. There were plans to license the use of the Wikipedia trademark for some products, such as books or DVDs.
In April 2005, the US Internal Revenue Service approved the foundation as an educational foundation in the category "Adult, Continuing education", meaning all contributions to the foundation are tax-deductible for US federal income tax purposes.
On December 11, 2006, the Foundation's board noted that the corporation could not become the membership organization initially planned but never implemented due to an inability to meet the registration requirements of Florida statutory law. Accordingly, the by-laws were amended to remove all reference to membership rights and activities. The decision to change the bylaws was passed by the board unanimously.
On September 25, 2007, the foundation's board gave notice that the operations would be moving to the San Francisco Bay Area. Major considerations cited for choosing San Francisco were proximity to like-minded organizations and potential partners, a better talent pool, as well as cheaper and more convenient international travel than is available from St. Petersburg, Florida.







In addition to Wikipedia, the foundation operates other wikis that follow the free content model with their main goal being the dissemination of knowledge. These include:




Wikimedia chapters are national (or in some cases sub-national) not-for-profit organisations created to support and promote the Wikimedia projects locally. The chapters are independent of the Wikimedia Foundation, with no legal control of nor responsibility for the Wikimedia projects. The organizations are recognized and overseen by a Chapters Committee; following approval they enter into a "Chapters Agreement" with the foundation. As of April 2012, there were 39 recognized Wikimedia chapters.




Each year, an international conference called Wikimania brings the people together who are involved in the Wikimedia organizations and projects. The first Wikimania was held in Frankfurt, Germany, in 2005. Nowadays, Wikimania is organized by a committee supported usually by the national chapter, in collaboration with the Wikimedia Foundation. Wikimania has been held in cities such as Buenos Aires, Cambridge, Haifa, Hong Kong, and London. In 2015, Wikimania took place in Mexico City. In 2016, Wikimania will be held in Esino Lario, Italy.




In response to the growing size and popularity of Wikipedia, the Wikimedia Foundation announced a Strategic Plan to improve and sustain the Wikimedia movement. The plan was announced in July 2009, followed by a process of interviews and surveys with people from across the Wikimedia movement, including board of trustees, members of staff and volunteer editors. The ongoing plan was intended to be the basis of a five-year plan to further outreach, improve content quality and quality control, and optimising operational areas such as finance and infrastructure.



In December 2008, the Wikimedia Foundation announced a restricted donation grant of $890,000 from the Stanton Foundation, to improve Wikipedia's accessibility. Later named the Wikipedia Usability Initiative, the grant was used by the Wikimedia Foundation to appoint project-specific staff to the technology department.
A series of surveys were conducted throughout 2009. This began with a qualitative environment survey on MediaWiki extensions, followed by a Qualitative Statistical Survey focusing on volume of edits, number of new users, and related statistics. In March 2009, a usability and experience study was carried out on new and non-editors of the English Wikipedia. The aim was to discover what obstacles participants encountered while editing Wikipedia, ranging from small changes to more complicated syntax such as templates. The study recruited 2500 people for in-person laboratory testing via the Wikipedia website, which was filtered down to ten participants. The results were collated and used by the technology team to improve Wikipedia's usability. The Usability and Experience Study was followed up by the Usability, Experience and Progress Study in September 2009. This study recruited different new and non-editors for in-person trials on a new Wikipedia skin.
The initiative ultimately culminated in a new Wikipedia skin named Vector, constructed based on the results of the usability studies. This was introduced by default in stages, beginning in May 2010.



In May 2010, the Wikimedia Foundation announced the Public Policy Initiative, following a $1.2 million donation by the Stanton Foundation. The Initiative was set up to improve articles relating to public policy related issues. As part of the initiative, Wikipedia collaborated with ten universities to help students and professors create and maintain articles relating to public policy. Volunteer editors of Wikipedia, known as "ambassadors", provided assistance to students and professors. This was either done on campus sites or online.



The foundation employs technology including hardware and software to run its projects.




Wikimedia currently runs on dedicated clusters of Linux servers (mainly Ubuntu). As of December 2009, there were 300 in Florida and 44 in Amsterdam. Wikipedia employed a single server until 2004, when the server setup was expanded into a distributed multitier architecture. In January 2005, the project ran on 39 dedicated servers in Florida. This configuration included a single master database server running MySQL, multiple slave database servers, 21 web servers running the Apache HTTP Server, and seven Squid cache servers.
Wikipedia receives between 25,000 and 60,000-page requests per second, depending on the time of day. Page requests are first passed to a front-end layer of Squid-caching servers. Further statistics are available based on a publicly available 3-months Wikipedia access trace. Requests that cannot be served from the Squid cache are sent to load-balancing servers running the Linux Virtual Server software, which in turn pass the request to one of the Apache web servers for page rendering from the database. The web servers deliver pages as requested, performing page rendering for all the language editions of Wikipedia. To increase speed further, rendered pages are cached in a distributed memory cache until invalidated, allowing page rendering to be skipped entirely for most common page accesses.




The operation of Wikimedia depends on MediaWiki, a custom-made, free and open-source wiki software platform written in PHP and built upon the MySQL database. The software incorporates programming features such as a macro language, variables, a transclusion system for templates, and URL redirection. MediaWiki is licensed under the GNU General Public License and it is used by all Wikimedia projects, as well as many other wiki projects. Originally, Wikipedia ran on UseModWiki written in Perl by Clifford Adams (Phase I), which initially required CamelCase for article hyperlinks; the present double bracket style was incorporated later. Starting in January 2002 (Phase II), Wikipedia began running on a PHP wiki engine with a MySQL database; this software was custom-made for Wikipedia by Magnus Manske. The Phase II software was repeatedly modified to accommodate the exponentially increasing demand. In July 2002 (Phase III), Wikipedia shifted to the third-generation software, MediaWiki, originally written by Lee Daniel Crocker. Several MediaWiki extensions are installed to extend the functionality of MediaWiki software. In April 2005, a Lucene extension was added to MediaWiki's built-in search and Wikipedia switched from MySQL to Lucene for searching. Currently Lucene Search 2.1, which is written in Java and based on Lucene library 2.3, is used. Wikimedia Foundation also uses CiviCRM and WordPress.
The Foundation published official Wikipedia mobile apps for Android and iOS devices and in March 2015, the apps were updated to include mobile user friendly features.







The Wikimedia Foundation relies on public contributions and grants to fund its mission. It is exempt from federal income tax and from state income tax. It is not a private foundation, and contributions to it qualify as tax-deductible charitable contributions.
The continued technical and economic growth of each of the Wikimedia projects is dependent mostly on donations but the Wikimedia Foundation also increases its revenue by alternative means of funding such as grants, sponsorship, services and brand merchandising. The Wikimedia OAI-PMH update feed service, targeted primarily at search engines and similar bulk analysis and republishing, has been a source of revenue for several years, but is no longer open to new customers. DBpedia was given access to this feed free of charge. In July 2014, the Foundation announced it would be accepting Bitcoin donations.
Since the end of fiscal year ended 2004, the Foundation's net assets have grown from $57K to $53.5M at the end of fiscal year ended June 30, 2014. Under the leadership of Sue Gardner, who joined the Wikimedia Foundation in 2007, the Foundation's staff levels, number of donors and revenue have seen very significant growth.

In 2007, Charity Navigator gave Wikimedia an overall rating of three out of four possible stars (one out of four in efficiency, which has been criticised). Charity Navigator gave three out of four possible stars in overall rating for fiscal years 2008 and 2009 which improved to four-stars in 2010. The current overall rating is four stars   three stars for Financial, four stars for Accountability and Transparency.
There are both supporting and opposing arguments regarding whether Wikimedia should switch to an advertising-based revenue model.




In March 2008, the Foundation announced a large donation, at the time its largest donation yet: a three-year, $3 million grant from the Alfred P. Sloan Foundation.
In 2009, the Foundation received four grants   the first grant was a $890,000 Stanton Foundation grant which was aimed to help study and simplify user interface for first-time authors of Wikipedia. The second was a $300,000 Ford Foundation Grant, given in July 2009, for Wikimedia Commons that aimed to improve the interfaces and workflows for multimedia uploading on Wikimedia websites. In August 2009, the Foundation received a $500,000 grant from The William and Flora Hewlett Foundation. Lastly, in August 2009, the Omidyar Network issued a potential $2 million in "grant" funding to Wikimedia.
In 2010, the Google corporation donated $2 million to the Foundation. Also in 2010, the William and Flora Hewlett Foundation pledged a $800,000 grant and all was funded during 2011.
In March 2011, the Alfred P. Sloan Foundation authorized another $3 million grant to continue to develop and maintain the Foundation's mission. The grant was to be funded over three years with the first $1 million funded in July 2011 and the remaining $2 million was scheduled to be funded in August 2012 and 2013. In August 2011, the Stanton Foundation pledged to fund a $3.6 million grant of which $1.8 million was funded and the remaining was due to be funded in September 2012. As of 2011, this was the largest grant received by the Wikimedia Foundation to-date. In November 2011, the Foundation received a $500,000 donation from Google co-founder Sergey Brin and his wife.
In 2012, the Foundation was awarded a grant of $1.25 million from the historians Lisbet Rausing and Peter Baldwin through Charities Aid Foundation, scheduled to be funded in five equal installments. The first installment of $250,000 was received in April 2012 and the remaining were to be funded in December 2012 through 2015.










The board of trustees has ultimate authority of all the businesses and affairs of the Foundation. It is composed of ten members:
four who are appointed by the Board itself;
three who are selected by the community encompassed by all the different Wikimedia projects;
two who are selected by the local chapters and thematic organizations;
and one emeritus for the foundation's founder, Jimmy Wales.
Three permanent entities support the board on its mission and responsibilities: an executive director, namely Lila Tretikov, which leads and oversees the operational arm of the foundation; an advisory board composed of individuals selected by the board itself that advise the board on different matters; and standing committees to which the board delegates certain matters while retaining ultimate authority. The board has also at times created other orthodox entities to support itself, such as executive secretaries and ad-hoc committees established for specific tasks.
The current board comprises Patricio Lorente as Chairman and Alice Wiegand as Vice-Chairman, together with Frieda Brioschi, James Heilman, Dariusz Jemielniak, Guy Kawasaki, Denny Vrande i , Jan-Bart de Vreede, Jimmy Wales and Stu West as members at-large.



The Advisory Board, according to the Wikimedia Foundation, is an international network of experts who have agreed to give the foundation meaningful help on a regular basis in many different areas, including law, organizational development, technology, policy, and outreach.







In 2004, the foundation appointed Tim Starling as developer liaison to help improve the MediaWiki software, Daniel Mayer as chief financial officer (finance, budgeting, and coordination of fund drives), and Erik M ller as content partnership coordinator. In May 2005, the foundation announced seven more official appointments.
In January 2006, the foundation created several committees, including the Communication Committee, in an attempt to further organize activities essentially handled by volunteers at that time. Starling resigned that month to spend more time on his PhD program.




The foundation's functions were, for the first few years, executed almost entirely by volunteers. In 2005, it had only two employees, Danny Wool, a coordinator, and Brion Vibber, a software manager.
As of October 4, 2006, the foundation had five paid employees: two programmers, an administrative assistant, a coordinator handling fundraising and grants, and an interim executive director, Brad Patrick, previously the foundation's general counsel. Patrick ceased his activity as interim director in January 2007, and then resigned from his position as legal counsel, effective April 1, 2007. He was replaced by Mike Godwin, who served as general counsel and legal coordinator from July 2007 until 2010.
In January 2007, Carolyn Doran was named chief operating officer and Sandy Ordonez joined as head of communications. Doran began working as a part-time bookkeeper in 2006 after being sent by a temporary agency. Doran later left the foundation in July 2007, and Sue Gardner was hired as consultant and special advisor (later CEO). Her departure from the organization was cited by Florence Devouard as one of the reasons the foundation took about seven months to release its fiscal 2007 financial audit.

Danny Wool, officially the grant coordinator but also largely involved in fundraising and business development, resigned in March 2007. Wales was accused by former Wikimedia Foundation employee Danny Wool of misusing the foundation's funds for recreational purposes. Wool also stated that Wales had his Wikimedia credit card taken away in part because of his spending habits, a claim Wales denied. In February 2007, the foundation added a new position, chapters coordinator, and hired Delphine M nard, who had been occupying the position as a volunteer since August 2005. Cary Bass was hired in March 2007 in the position of volunteer coordinator. Oleta McHenry was brought in as accountant in May 2007, through a temporary placement agency and made the official full-time accountant in August 2007. In January 2008, the foundation appointed Veronique Kessler as the new chief financial and operating officer, Kul Wadhwa as head of business development, and Jay Walsh as head of communications.
By early 2015, the foundation had well over 200 employees.
According to Business Insider, "In September of 2012, there was a quite a bit of media attention surrounding two Wikipedia employees who were running a PR business on the side and editing Wikipedia on behalf of their clients."




Many disputes have resulted in litigation while others have not. Attorney Matt Zimmerman stated, "Without strong liability protection, it would be difficult for Wikipedia to continue to provide a platform for user-created encyclopedia content."
In December 2011, the Foundation hired Washington, DC lobbyist Dow Lohnes Government Strategies LLC to lobby the United States Congress with regard to "Civil Rights/Civil Liberties" and "Copyright/Patent/Trademark." At the time of the hire the Foundation was concerned specifically about a bill known as the Stop Online Piracy Act.
In October 2013, a German Court ruled that the Wikimedia Foundation can be held liable for content added to Wikipedia.
In June 2014, a copyright infringement lawsuit was filed by Bildkonst Upphovsr tt i Sverige against Wikimedia Sweden.
On June 20, 2014, a defamation lawsuit (Law Division civil case No. L-1400-14) involving Wikipedia editors was filed with the Mercer County Superior Court in New Jersey seeking, inter alia, compensatory and punitive damages.
In a March 10, 2015, op-ed for The New York Times, Wales and Tretikov announced the Foundation was filing a lawsuit against the National Security Agency, calling into question its practice of mass surveillance, which they argued infringed the constitutional rights of the Foundation's readers, editors and staff.


