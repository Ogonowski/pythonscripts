In computing, rebooting is the process by which a running computer system is restarted, either intentionally or unintentionally. Reboots can be either cold (alternatively known as hard) where the power to the system is physically turned off and back on again, causing an initial boot of the machine, or warm (alternatively known as soft) where the system restarts without the need to interrupt the power. The term restart is used to refer to a reboot when the operating system closes all programs and finalizes all pending input and output operations before initiating a soft reboot.






Technical sources describe two contrasting forms of reboot known as cold reboot (also cold boot, hard reboot or hard boot) and warm reboot (also soft reboot) although the definition of these forms slightly vary between sources.
According to Jones, Landes, Tittel (2002), Cooper (2002), Tulloch (2002) and Soper (2004), on IBM PC compatible platform, a cold boot is a boot process in which the computer starts from a powerless state. All except Tulloch (2002) also mention that in cold boot, system performs a power-on self-test (POST). In addition to the power switch, Cooper (2002) and Soper (2004) also state that reset button may commence a cold reboot. Jones, Landes, Tittel (2002) contradicts this assertion and states that a reset button may commence either a cold or warm reboot, depending on the system. Microsoft Support article 102228 also confers that although the reset button is designed to perform a cold reboot, it may not disconnect the power to the motherboard   a state that does not correspond to the cold boot definition given above. According to Jones, Landes, Tittel (2002), both the operating system and third-party software can initiate a cold boot; the restart command in Windows 9x initiates a cold reboot, unless Shift key is held.
Finding a definition for warm boot, however, is more of a challenge. All aforementioned sources indicate that a warm boot is initiated by pressing Ctrl + Alt + Delete key combination; all except Tulloch (2002) mention that a warm reboot does not perform POST. Jones, Landes, Tittel (2002) specifies that for a warm reboot to occur, BIOS must be the recipient of the key combination. Microsoft Support article 102228 takes a more technical approach and defines a warm boot a result of invoking INT 19h, a BIOS interrupt call; the Ctrl + Alt + Delete key combination is only one of the ways. According to Grimes (2001), malware may prevent or subvert a warm boot by intercepting Ctrl + Alt + Delete key combination and prevent it from reaching BIOS. Windows NT family of operating systems also does the same and reserves the key combination for its own use. Soper (2004) asserts that Windows restart command initiates a warm boot, thus contradicting Jones, Landes, Tittel (2002) that believes the same action performs a cold boot.
The Linux family of operating systems support an alternative to warm boot - the Linux kernel has optional support for kexec, a system call which transfers execution to a new kernel and skips hardware or firmware reset. The entire process occurs independently of the system firmware. The kernel being executed does not have to be a Linux kernel.
Outside the domain of IBM compatible PCs, the types of boot may not apply. According to Sue Loh of Windows CE Base Team, Windows CE devices support three types of boots: Warm, cold and clean. A warm boot discards "program memory" area. A cold boot also discards the contents of "storage memory" area (also known as "object store"). A clean boot erases all forms of storage memories for the device. However, since these areas do not exist on all Windows CE devices, users are only concerned with two forms of reboot: One that resets the volatile memory and one that wipes the device clean and restores factory settings. For example, for a Windows Mobile 5.0 device, the former is a cold boot and the latter is a clean boot.



A hard reboot means that the system is not shut down in an orderly manner, skipping file system synchronisation and other activities that would occur on an orderly shutdown. This can be achieved by either applying a reset, by cycling power, by issuing the halt -q command in UNIX/Linux or by triggering a Kernel panic.



In Berlin, resetting a computer by cycling power, disconnecting and reconnecting the power connector, or blackout is colloquial known as a "BEWAG reset" (name of the local power company) within the local computing scene.



The term "restart" is used by Microsoft Windows and Linux family of operating system (including but not limited to Ubuntu and Linux Mint) to denote an operating system-assisted reboot. In a restart, the operating system ensures that all pending I/O operations are gracefully ended before commencing a reboot.






Users may deliberately initiate a reboot. Rationale for such action may include:
Troubleshooting: Rebooting may be used by users, support staff or system administrators as a technique to work around bugs in software, for example memory leaks or processes that hog resources to the detriment of the overall system, or to terminate malware. While this approach does not address the root cause of the issue, resetting a system back to a good, known state may allow it to be used again for some period until the issue next occurs.
Switching operating systems: On a multi-boot system without a hypervisor, a reboot is required to switch between installed operating systems.
Offensive: As stated earlier, components lose power during a cold reboot; therefore, components such as RAM that require power lose the data they hold. However, in a cold boot attack, special configurations may allow for part of the system state, like a RAM disk, to be preserved through the reboot.
The means performing a deliberate reboot also varies and may include:
Manual, hardware-based: A power switch or reset button can cause the system to reboot. Doing so, however, may cause the loss of all unsaved data.
Manual, software-based: Computer software and operating system can trigger a reboot as well; more specifically, Microsoft Windows operating systems are outfitted with a restart command that closes open programs and eliminate data loss due to reboot.
Automated: Software can be scheduled to run at a certain time and date; therefore, it is possible to schedule a reboot.



Unexpected loss of power for any reason (including power outage, power supply failure or depletion of battery on a mobile device) forces the system user to perform a cold boot once the power is restored. Some BIOSes have an option to automatically boot the system after a power failure. An uninterruptible power supply (UPS), backup battery or redundant power supply can prevent such circumstances.




"Random reboot" is a non-technical term referring to an unintended (and often undesired) reboot following a system crash, whose root cause may not immediately be evident to the user. Such crashes may occur due to a multitude of software and hardware problems, such as triple faults. They are generally symptomatic of an error in ring 0 that is not trapped by an error handler in an operating system or a hardware-triggered non-maskable interrupt.
Systems may be configured to reboot automatically after a power failure, or a fatal system error or kernel panic. The method by which this is done varies depending whether the reboot can be handled in software, or must be handled at the firmware or hardware level. Operating systems in the Windows NT family (from Windows NT 3.1 through Windows 7) have an option to modify the behavior of the error handler so that a computer immediately restarts rather than displaying a Blue Screen of Death (BSOD) error message.




The introduction of advanced power management allowed operating systems greater control of hardware power management features. With Advanced Configuration and Power Interface (ACPI), newer operating systems are able to manage different power states and thereby sleep and/or hibernate. While hibernation also involves turning a system off then subsequently back on again, the operating system does not start from scratch, therefore differentiating this process from rebooting.



A reboot may be simulated by software running on an operating system. For example, the Sysinternals BlueScreen utility, which is used for pranking. Malware may also simulate a reboot, and thereby deceive a computer user for some nefarious purpose.
Microsoft App-V sequencing tool captures all the file system operations of an installer in order to create a virtualized software package for users. As part of the sequencing process, it will detect when an installer requires a reboot, interrupt the triggered reboot, and instead simulate the required reboot by restarting services and loading/unloading libraries.


