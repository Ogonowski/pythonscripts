Pen computing refers to a computer user-interface using a pen (or stylus) and tablet, rather than devices such as a keyboard, joysticks or a mouse.
Pen computing is also used to refer to the usage of mobile devices such as wireless tablet PCs, PDAs and GPS receivers. The term has been used to refer to the usage of any product allowing for mobile communication. An indication of such a device is a stylus or digital pen, generally used to press upon a graphics tablet or touchscreen, as opposed to using a more traditional interface such as a keyboard, keypad, mouse or touchpad.
Historically, pen computing (defined as a computer system employing a user-interface using a pointing device plus handwriting recognition as the primary means for interactive user input) predates the use of a mouse and graphical display by at least two decades, starting with the Stylator  and RAND Tablet systems of the 1950s and early 1960s.



User interfaces for pen computing can be implemented in several ways. Actual systems generally employ a combination of these techniques.



The tablet and stylus are used as pointing devices, such as to replace a mouse. While a mouse is a relative pointing device (one uses the mouse to "push the cursor around" on a screen), a tablet is an absolute pointing device (one places the stylus where the cursor is to appear).
There are a number of human factors to be considered when actually substituting a stylus and tablet for a mouse. For example, it is much harder to target or tap the same exact position twice with a stylus, so "double-tap" operations with a stylus are harder to perform if the system is expecting "double-click" input from a mouse.
A finger can be used as the stylus on a touch-sensitive tablet surface, such as with a touchscreen.



The tablet and stylus can be used to replace a keyboard, or both a mouse and a keyboard, by using the tablet and stylus in two modes:
Pointing mode: The stylus is used as a pointing device as above.
On-line Handwriting recognition mode: The strokes made with the stylus are analyzed as a "electronic ink", by software which recognizes the shapes of the strokes or marks as handwritten characters. The characters are then input as text, as if from a keyboard.
Different systems switch between the modes (pointing vs. handwriting recognition) by different means, e.g.
by writing in separate areas of the tablet for pointing mode and for handwriting-recognition mode.
by pressing a special button on the side of the stylus to change modes.
by context, such as treating any marks not recognized as text as pointing input.
by recognizing a special gesture mark.
The term "on-line handwriting recognition" is used to distinguish recognition of handwriting using a real-time digitizing tablet for input, as contrasted to "off-line handwriting recognition", which is optical character recognition of static handwritten symbols from paper.




The stylus is used to touch, press, and drag on simulated objects directly. The Wang Freestyle system  is one example. Freestyle worked entirely by direct manipulation, with the addition of electronic "ink" for adding handwritten notes.



This is the technique of recognizing certain special shapes not as handwriting input, but as an indicator of a special command.
For example, a "pig-tail" shape (used often as a proofreader's mark) would indicate a "delete" operation. Depending on the implementation, what is deleted might be the object or text where the mark was made, or the stylus can be used as a pointing device to select what it is that should be deleted. With Apple's Newton OS, text could be deleted by scratching in a zig-zag pattern over the text which the user desired to remove.
Recent systems have used digitizers which can recognize more than one "stylus" (usually a finger) at a time, and make use of Multi-touch gestures.
The PenPoint OS was a special operating system which incorporated gesture recognition and handwriting input at all levels of the operating system. Prior systems which employed gesture recognition only did so within special applications, such as CAD/CAM applications  or text processing.




Pen computing has very deep historical roots. For example, the first patent for an electronic device used for handwriting, the telautograph, was granted in 1888. What is probably the first patent for a system that recognized handwritten characters by analyzing the handwriting motion was granted in 1915. The first publicly demonstrated system using a tablet and handwriting text recognition instead of a keyboard for working with a modern digital computer dates to 1956.
In addition to many academic and research systems, there were several companies with commercial products in the 1980s: Pencept, Communications Intelligence Corporation, and Linus were among the best known of a crowded field. Later, GO Corp. brought out the PenPoint OS operating system for a tablet PC product: one of the patents from GO corporation was the subject of recent infringement lawsuit concerning the Tablet PC operating system.
The following timeline list gives some of the highlights of this history:
Before 1950
1888: U.S. Patent granted to Elisha Gray on electrical stylus device for capturing handwriting.
1915: U.S. Patent on handwriting recognition user interface with a stylus.
1942: U.S. Patent on touchscreen for handwriting input.
1945: Vannevar Bush proposes the Memex, a data archiving device including handwriting input, in an essay As We May Think.

1950s
Tom Dimond demonstrates the Styalator electronic tablet with pen for computer input and handwriting recognition.

Early 1960s
RAND Tablet invented.

Late 1960s
Alan Kay of Xerox PARC proposed a notebook using pen input called Dynabook: however device is never constructed.

1982
Pencept of Waltham, Massachusetts markets a general-purpose computer terminal using a tablet and handwriting recognition instead of a keyboard and mouse.
Cadre System markets the Inforite point-of-sale terminal using handwriting recognition and a small electronic tablet and pen.

1985:
Pencept and CIC both offer PC computers for the consumer market using a tablet and handwriting recognition instead of a keyboard and mouse. Operating system is MS-DOS.

1989
The first commercially available tablet-type portable computer was the GRiDPad from GRiD Systems, released in September. Its operating system was based on MS-DOS.
Wang Laboratories introduces Freestyle. Freestyle was an application that would do a screen capture from an MS-DOS application, and let the user add voice and handwriting annotations. It was a sophisticated predecessor to later note-taking applications for systems like the Tablet PC. The operating system was MS-DOS

1991
The Momenta Pentop was released.
GO Corp announced a dedicated operating system, called PenPoint OS, featuring control of the operating system desktop via handwritten gesture shapes. Gestures included "flick" gestures in different directions, check-marks, cross-outs, pig-tails, and circular shapes, among others.
Portia Isaacsen of Future Computing estimates the total annual market for pen computers such as those running the PenPoint OS to be on the order of $500 Million.
NCR released model 3125 pen computer running MS-DOS, Penpoint or Pen Windows.
The Apple Newton entered development; although it ultimately became a PDA, its original concept (which called for a larger screen and greater sketching capabilities) resembled that of a tablet PC.
Sam Tramiel of Atari Corp. presented the "ST-Pad" (codenamed "STylus") at the CeBIT '91 in Hanover, Germany. The computer never went into production.

1992
GO Corp shipped PenPoint and IBM announced IBM 2125 pen computer (the first IBM model named "ThinkPad") in April.
Microsoft releases Windows for Pen Computing as a response to the PenPoint OS.

1993
The IBM releases the ThinkPad, IBM's first commercialized portable tablet computer product available to the consumer market, as the IBM ThinkPad 750P and 360P
Apple Computer announces the Newton PDA, also known as the Apple MessagePad, which includes handwriting recognition with a stylus.
Amstrad release the "Pen Pad" or PDA6000, a similar pen-based device. It did not achieve commercial success.
AT&T introduced the EO Personal Communicator combining PenPoint with wireless communications.
BellSouth released the IBM Simon Personal Communicator, an analog cellphone using a touch-screen and display. It did not include handwriting recognition, but did permit users to write messages and send them as faxes on the analog cellphone network, and included PDA and Email features.

1999
The "QBE" pen computer created by Aqcess Technologies wins Comdex Best of Show.

2000
The "QBE Vivo" pen computer created by Aqcess Technologies ties for Comdex Best of Show.

2001
Bill Gates of Microsoft demonstrates first public prototype of a Tablet PC (defined by Microsoft as a pen-enabled computer conforming to hardware specifications devised by Microsoft and running a licensed copy of Windows XP Tablet PC Edition) at Comdex.

2003
FingerWorks  develops the touch technology and touch gestures later used in the Apple iPhone.

2006
Windows Vista released for general availability. Vista included the functionality of the special Tablet PC edition of Windows XP.

2008
In April 2008, as part of a larger federal court case, the gesture features of the Windows/Tablet PC operating system and hardware were found to infringe on a patent by GO Corp. concerning user interfaces for pen computer operating systems. Microsoft's acquisition of the technology is the subject of a separate lawsuit.
HP releases the second MultiTouch capable tablet: the HP TouchSmart tx2z.



Gesture recognition
Handwriting movement analysis
Handwriting recognition
Interactive whiteboard
Laser pointer (e.g. highlighting)
Graffiti Lighting

Light pen
Sketch recognition






The Unknown History of Pen Computing contains a history of pen computing, including touch and gesture technology, from approximately 1917 to 1992.
Annotated bibliography of references to handwriting recognition and pen computing
A number of links to pen computing resources.
Digital Ink, Breakthrough Technology in Tablet PC, Brings the Power of the Pen to the Desktop
Notes on the History of Pen-based Computing (Youtube)