The M nch (German: "monk") is a mountain in the Bernese Alps, in Switzerland. Together with the Eiger and the Jungfrau it forms a highly recognisable group of mountains visible from far away.
The M nch lies on the border between the cantons of Valais and Bern, and forms part of a mountain ridge between the Jungfrau and Jungfraujoch to the west, and the Eiger to the east. The mountain is located west of M nchsjoch (a 3,650 m high pass) and M nchsjoch Hut and north of the Jungfraufirn and Ewigschneef ld, two affluents of the Great Aletsch Glacier. The north side of the M nch forms a step wall above the Lauterbrunnen valley.
The Jungfrau railway tunnel runs right under the summit at a height of approximately 3,300 metres.
The peak was first climbed on August 15, 1857 by Christian Almer, Christian Kaufmann, Ulrich Kaufmann and Sigismund Porges.





