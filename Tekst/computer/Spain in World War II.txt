The Spanish State under General Franco was officially non-belligerent during World War II. This status was not recognised by international law but in practice amounted to neutrality. In fact, Franco's regime did supply material and military support to the Axis Powers in recognition of the heavy assistance it had received in the Spanish Civil War. However, despite an ideological sympathy, Franco refused to bring Spain into the war as a belligerent and stationed field armies in the Pyrenees to dissuade a German occupation of the Iberian Peninsula. The Spanish policy frustrated German proposals that encouraged Franco to take British-controlled Gibraltar. The apparent contradictions in Franco's foreign policy can be explained by his pragmatism.



Following the Spanish Civil War, many supporters of the former Republican government decided to start a movement to overthrow Franco; these members were called the Spanish Maquis. Several guerrilla raids occurred during the timeline of World War II, with most of them happening in 1944. One major confrontation happened in the Val d'Aran valley where a large group of rebels attacked and briefly occupied the north-western border with France. The battle ended in ten days. Some people refer to this as The Spanish Revolution or The second part of the civil war. Evidence shows that this was Spain's main battlefront during World War II.



During World War II Spain was governed by a military dictatorship, but despite Franco's own pro-Axis leanings and debt of gratitude to Benito Mussolini and Adolf Hitler, the government was divided between Germanophiles and Anglophiles. When the war started, Juan Beigbeder Atienza, an Anglophile, was the Minister of Foreign Affairs. The rapid German advance in Europe convinced Franco to replace him with Ram n Serrano S er, Franco's brother-in-law and a strong Germanophile (October 18, 1940). After the 1942 Allied victories in Eastern Europe and north Africa, Franco changed tack again, appointing Francisco G mez-Jordana Sousa, sympathetic to the British, as minister. Another influential anglophile was the Duke of Alba, Spain's ambassador in London.




The main part of Spain's involvement in the war was through volunteers. They fought for both sides, largely reflecting the allegiances of the civil war.



Although the Spanish State remained neutral throughout World War II, it was ideologically aligned with Nazi Germany and Fascist Italy. There was also a "debt" for the help that these regimes had given to the military uprising. When Germany invaded the Soviet Union on June 22, 1941, Franco, pressured by the Germans, offered Spanish manpower to help in civilian warwork and military volunteers to fight against the allies.
This was accepted by Hitler and, within two weeks, there were more than enough volunteers to form a division   the Blue Division or Divisi n Azul under Agust n Mu oz Grandes   including an air force squadron   the Blue Squadron. The Blue Division was trained in Germany and served, with distinction, in the Siege of Leningrad, and notably at the Battle of Krasny Bor, where General Infantes' 6,000 Spanish troops threw back some 30,000 Soviet troops. In October 1943, under severe Allied diplomatic pressure, the Blue Division was ordered home leaving a token force until March 1944. In all, about 45,000 Spanish served on the Eastern Front, mostly committed volunteers, and around 4,500 died. Joseph Stalin's desire for revenge against Franco was frustrated at the Potsdam Conference in July 1945, when his attempt to make an Allied invasion of Spain the conference's first order of business was rejected by Harry S. Truman and Winston Churchill. War weary and unwilling to continue the conflict, Truman and Churchill persuaded Stalin to instead settle for a full trade embargo against Spain.




After their defeat in the Spanish Civil War, numbers of Republican veterans and civilians went into exile in France; the French Republic interned them in refugee camps, such as Camp Gurs in southern France. To improve their conditions, many joined the French Foreign Legion at the start of World War II, making up a sizeable proportion of it. Around sixty thousand joined the French Resistance, mostly as guerrillas, with some also continuing the fight against Francisco Franco. Several thousand more joined the Free French Forces and fought against the Axis Powers. Some sources have claimed that as many as 2,000 served in General Leclerc's Second French Division, many of them from the former Durruti Column. The 9th Armoured Company comprised almost entirely battle-hardened Spanish veterans; it became the first Allied military unit to enter Paris upon its liberation in August, 1944, where it met up with a large number of Spanish Maquis fighting alongside French resistance fighters. Furthermore, 1,000 Spanish Republicans served in the 13th Half-brigade of the French Foreign Legion.
In Eastern Europe, the Soviet Union received former pro-Republican Spaniards, leftist leaders and child evacuees from Republican families. When Germany invaded the Soviet Union in 1941, many, such as General Enrique L ster, joined the Red Army. According to Beevor, 700 Spanish Republicans served in the Red Army and another 700 operated as partisans behind the German lines.
Individual Spaniards, such as the double-agent Juan Pujol (alias Garbo), also worked for the Allied cause.



Initially, Spain favoured the then-victorious Axis Powers. Apart from ideology, Spain had a debt to Germany of $212 million for supplies of mat riel during the Civil War. Indeed, in June 1940, after the Fall of France, the Spanish Ambassador to Berlin had presented a memorandum in which Franco declared he was "ready under certain conditions to enter the war on the side of Germany and Italy".

At first, the German leader, Adolf Hitler, did not encourage Franco's offer, as the German leadership was convinced of eventual victory. Later on, in September, when the Royal Air Force had demonstrated its resilience in defeating the Luftwaffe in the Battle of Britain, Hitler promised Franco help in return for its active intervention. This had become part of a strategy to forestall Allied intervention in north-west Africa. Hitler promised that "Germany would do everything in its power to help Spain" and would recognise Spanish claims to French territory in Morocco, in exchange for a share of Moroccan raw materials. Franco responded warmly, but without any firm commitment.
Falangist media agitated for irredentism, claiming for Spain the regions of Catalonia and the Basque Country which were under French administration.
Hitler and Franco met at Hendaye, France on 23 October 1940 to fix the details of an alliance. By this time, the advantages had become less clear for either side. Franco asked for too much from Hitler. In exchange for entering the war alongside the alliance of Germany and Italy, Franco, among many things, demanded heavy fortification of the Canary Islands as well as large quantities of grain, fuel, armed vehicles, military aircraft and other armaments. In response to Franco's nearly impossible demands, Hitler threatened Franco with a possible annexation of Spanish territory by Vichy France. At the end of the day, no agreement was reached. A few days later in Germany, Hitler would famously tell Mussolini, "I prefer to have three or four of my own teeth pulled out than to speak to that man again!" It is subject to historical debate whether Franco overplayed his hand by demanding too much from Hitler for Spanish entry into the war, or if he deliberately stymied the German dictator by setting the price for his alliance unrealistically high, knowing that Hitler would refuse his demands and thus save Spain from entering another devastating war. (German resistance leader and Abwehr chief Wilhelm Canaris, had secretly briefed Franco about which demands would be found excessive.)
Spain relied upon oil supplies from the United States, and the US had agreed to listen to British recommendations on this. As a result, the Spanish were told that supplies would be restricted, albeit with a ten-week reserve. Lacking a strong navy, any Spanish intervention would rely, inevitably, upon German ability to supply oil. Some of Germany's own activity relied upon captured French oil reserves, so additional needs from Spain were unhelpful.
From the German point of view, Vichy's active reaction to British and Free French attacks (Destruction of the French Fleet at Mers-el-Kebir and Dakar) had been encouraging, so perhaps Spanish intervention was less vital. Also, in order to keep Vichy "on-side", the proposed territorial changes in Morocco became a potential embarrassment and were diluted. As a consequence of this, neither side would make sufficient compromises and after nine hours, the talks failed.
In December 1940, Hitler contacted Franco again via a letter sent by the German ambassador to Spain and returned to the issue of Gibraltar. Hitler attempted to force Franco's hand with a blunt request for the passage of several divisions of German troops through Spain to attack Gibraltar. Franco refused, citing the danger that the United Kingdom still presented to Spain and the Spanish colonies. In his return letter, Franco told Hitler that he wanted to wait until Britain "was on the point of collapse". In a second diplomatic letter, Hitler got tougher and offered grain and military supplies to Spain as an inducement. By this time, however, the Luftwaffe had been defeated in the Battle of Britain, Italian troops were being routed by the British in Cyrenaica and East Africa, the Royal Navy displayed its freedom of action in Italian waters and neutralised the Vichy French fleet at Mers-el-K bir in French Algeria. The UK was clearly not finished. Franco responded "that the fact has left the circumstances of October far behind" and "the Protocol then agreed must now be considered outmoded".
According to Franco's own autobiography, he also met privately with Italian leader Benito Mussolini in Bordighera, Italy on 12 February 1941 at Hitler's request. Hitler hoped that Mussolini could persuade Franco to enter the war. However, Mussolini was not interested in Franco's help due to the recent string of defeats his forces had suffered in North Africa and the Balkans.
Despite being non-belligerent throughout the war, Franco's regime of open support to the Axis Powers led to a period of postwar isolation for Spain as trade with most countries ceased. U.S. President Franklin Roosevelt, who had assured Franco that Spain would not suffer consequences from the United Nations (a wartime term for those nations allied against Germany), died in April 1945. Roosevelt's successor, Harry S. Truman, as well as new Allied governments, were less friendly to Franco. A number of nations withdrew their ambassadors, and Spain was not admitted to the United Nations until 1955.



Although it sought to avoid entering the war, Spain did make plans for defence of the country. Initially, the mass of the Spanish army was stationed in southern Spain in case of an Allied attack from Gibraltar during 1940 and 1941. However, Franco ordered the divisions to gradually redeploy in the mountains along the French border in case of a possible German invasion of Spain as Axis interest in Gibraltar grew. By the time it became clear that the Allies were gaining the upper hand in the conflict, Franco had amassed all his troops on the French border and received personal assurances from the leaders of Allied countries that they did not wish to invade Spain.




Before Hendaye, there had been Spanish-German planning for an attack, from Spain, upon the British territory of Gibraltar which was, and is, a British dependency and military base. At the time, Gibraltar was important for control of the western exit from the Mediterranean and the sea routes to the Suez Canal and Middle East, as well as Atlantic patrols.
The Germans also appreciated the strategic importance of north-west Africa for bases and as a route for any future American involvement. Therefore, the plans included the occupation of the region by substantial German forces, to forestall any future Allied invasion attempt.
The plan, Operation Felix, was in detailed form before the negotiations failed at Hendaye. By March 1941, military resources were being ear-marked for Barbarossa and the Soviet Union. Operation Felix-Heinrich was an amended form of Felix that would be invoked once certain objectives in Russia had been achieved. In the event, these conditions were not fulfilled and Franco still held back from entering the war.
After the war, Field Marshal Wilhelm Keitel said: "Instead of attacking Russia, we should have strangled the British Empire by closing the Mediterranean. The first step in the operation would have been the conquest of Gibraltar. That was another great opportunity we missed." If that had succeeded, Hermann G ring proposed that Germany would "... offer Britain the right to resume peaceful traffic through the Mediterranean if she came to terms with Germany and joined us in a war against Russia".
As the war progressed and the tide turned against the Axis, the Germans planned for the event of an Allied attack through Spain. There were three successive plans, progressively less aggressive as German capability waned:




This was planned in April 1941 as a reaction to a proposed British landing on the Iberian peninsula near Gibraltar. German troops would advance into Spain to support Franco and expel the British wherever they landed.



Ilona was a scaled down version of Isabella, subsequently renamed Gisella. Devised in May 1942, to be invoked whether or not Spain stayed neutral. Ten German divisions would advance to Barcelona and, if necessary, towards Salamanca to support the Spanish army in fighting another proposed Allied landing either from the Mediterranean or Atlantic coasts.



Devised in June 1943, Nurnberg was purely a defensive operation in the Pyrenees along both sides of the Spanish-French border in the event of Allied landings in the Iberian peninsula, which were to repel an Allied advance from Spain into France.



According to a 2008 book, Winston Churchill authorised millions of dollars in bribes to Spanish generals in an effort to influence General Franco against entering the war on the side of Germany.



Despite lacking cash, oil and other supplies, Francoist Spain was able to supply some essential materials to Germany. There were a series of secret war-time trade agreements between the two countries.
The principal resource was wolfram (or tungsten) ore from German-owned mines in Spain. Wolfram was essential to Germany for its advanced precision engineering and therefore for armament production. Despite Allied attempts to buy all available supplies, which rocketed in price, and diplomatic efforts to influence Spain, supplies to Germany continued until August 1944. Payment for wolfram was effectively set against the Spanish debt to Germany. Other minerals included iron ore, zinc, lead and mercury.
Spain also acted as a conduit for goods from South America, for example, industrial diamonds and platinum.
After the war, evidence was found of significant gold transactions between Germany and Spain, ceasing only in May 1945. It was believed that these were derived from Nazi looting of occupied lands, but attempts by the Allies to obtain control of the gold and return it were largely frustrated.



As long as Spain permitted it, the Abwehr   the German intelligence organisation   was able to operate in Spain and Spanish Morocco, often with cooperation of the Nationalist government.
Gibraltar's installations were a prime target for sabotage, using sympathetic anti-British Spanish workers. One such attack occurred in June 1943, when a bomb caused a fire and explosions in the dockyard. The British were generally more successful after this and managed to use turned agents and sympathetic anti-Fascist Spaniards to uncover subsequent attacks. A total of 43 sabotage attempts were prevented in this way. In January 1944, two Spanish workers, convicted of attempted sabotage, were executed.
The Abwehr also maintained observation posts along both sides of the Straits of Gibraltar, reporting on shipping movements.
A German agent in C diz was the target of a successful Allied disinformation operation, Operation Mincemeat, prior to the invasion of Sicily in 1943.
In early 1944, the situation changed. The Allies were clearly gaining the advantage over Germany and one double agent had provided enough information for Britain to make a detailed protest to the Spanish government. As a result, the Spanish government declared its "strict neutrality". The Abwehr operation in southern Spain was consequently closed down.

The rail station of Canfranc was the conduit for the smuggling of people and information from Vichy France to the British consulate in San Sebasti n. The nearer border station of Ir n could not be used as it bordered occupied France.



In the first years of the war, "Laws regulating their admittance were written and mostly ignored." They were mainly from Western Europe, fleeing deportation to concentration camps from occupied France, but also Jews from Eastern Europe, especially in Hungary. Trudy Alexy refers to the "absurdity" and "paradox of refugees fleeing the Nazis' Final Solution to seek asylum in a country where no Jews had been allowed to live openly as Jews for over four centuries." 
Throughout World War II, Spanish diplomats of the Franco government extended their protection to Eastern European Jews, especially in Hungary. Jews claiming Spanish ancestry were provided with Spanish documentation without being required to prove their case and either left for Spain or survived the war with the help of their new legal status in occupied countries.
Once the tide of war began to turn, and Count Francisco G mez-Jordana Sousa succeeded Franco's brother-in-law Serrano S er as Spain's foreign minister, Spanish diplomacy became "more sympathetic to Jews", although Franco himself "never said anything" about this. Around that same time, a contingent of Spanish doctors travelling in Poland were fully informed of the Nazi extermination plans by Governor-General Hans Frank, who was under the misimpression that they would share his views about the matter; when they came home, they passed the story to Admiral Lu s Carrero Blanco, who told Franco.
Diplomats discussed the possibility of Spain as a route to a containment camp for Jewish refugees near Casablanca but it came to naught due to lack of Free French and British support. Nonetheless, control of the Spanish border with France relaxed somewhat at this time, and thousands of Jews managed to cross into Spain (many by smugglers' routes). Almost all of them survived the war. The American Jewish Joint Distribution Committee operated openly in Barcelona.
Shortly afterwards, Spain began giving citizenship to Sephardic Jews in Greece, Hungary, Bulgaria, and Romania; many Ashkenazic Jews also managed to be included, as did some non-Jews. The Spanish head of mission in Budapest,  ngel Sanz Briz, saved thousands of Ashkenazim in Hungary by granting them Spanish citizenship, placing them in safe houses and teaching them minimal Spanish so they could pretend to be Sephardim, at least to someone who did not know Spanish. The Spanish diplomatic corps was performing a balancing act: Alexy conjectures that the number of Jews they took in was limited by how much German hostility they were willing to engender.
Toward the war's end, Sanz Briz had to flee Budapest, leaving these Jews open to arrest and deportation. An Italian diplomat, Giorgio Perlasca, who was himself living under Spanish protection, used forged documents to persuade the Hungarian authorities that he was the new Spanish Ambassador. As such, he continued Spanish protection of Hungarian Jews until the Red Army arrived.
Although Spain effectively undertook more to help Jews escape deportation to the concentration camps than most neutral countries did, there has been debate about Spain's wartime attitude towards refugees. Franco's regime, despite its aversion to Zionism and "Judeo"-Freemasonry, does not appear to have shared the rabid anti-Semitic ideology promoted by the Nazis. About 25,000 to 35,000 refugees, mainly Jews, were allowed to transit through Spain to Portugal and beyond.
Some historians argue that these facts demonstrate a humane attitude by Franco's regime, while others point out that the regime only permitted Jewish transit through Spain. After the war, Franco's regime was quite hospitable to those who had been responsible for the deportation of the Jews, notably Louis Darquier de Pellepoix, Commissioner for Jewish Affairs (May 1942   February 1944) under the Vichy R gime in France.
Jos  Mar a Finat y Escriv  de Roman , Franco's chief of security, issued an official order dated May 13, 1941 to all provincial governors requesting a list of all Jews, both local and foreign, present in their districts. After the list of six thousand names was compiled, Romani was appointed Spain's ambassador to Germany, enabling him to deliver it personally to Himmler. Following the defeat of Germany in 1945, the Spanish government attempted to destroy all evidence of cooperation with the Nazis, but this official order survived.



Moscow Gold
Spanish Maquis
Laurel Incident
Neutral powers during World War II






Payne, Stanley G (2008). Franco and Hitler. New Haven: Yale University Press. ISBN 978-0-300-12282-4. 
Shulman, Milton (1995) [1947]. Defeat in the West. Chailey, East Sussex. ISBN 1-872947-03-4. 
Wilmot, Chester (1997). The Struggle for Europe. Ware, Hertfordshire: Wordsworth Editions. ISBN 1-85326-677-9. 



Bowen, Wayne H. (2000). Spaniards and Nazi Germany: Collaboration in the New Order. Columbia, MO: University of Missouri Press. p. 250. ISBN 978-0826213006. OCLC 44502380. 
Bowen, Wayne H. (2005). Spain During World War II. Columbia, MO: University of Missouri Press. p. 279. ISBN 978-0826216588. OCLC 64486498. 



1939 1945: The Spanish Resistance in France
Nueve Company (French Second Armoured Division)
The Blue Division
Spanish Involvement in World War II
Operation Felix: Assault on Gibraltar
Excerpt from Christian Leitz, "Spain and Holocaust"
Libro Memorial. Espa oles deportados a los campos nazis (1940-1945), Benito Bermejo and Sandra Checa, Ministerio de Cultura de Espa a, 2006. Re-published in Portable Document Format.
Los vascos y la II Guerra Mundial, Mikel Rodr guez, Euskonews & Media 301.
Jimmy Burns, Papa Spy: Love, Faith & Betrayal in Wartime Spain. London, Bloomsbury, 2009. [1]