The MCS-48 microcontroller ( C) series, Intel's first microcontroller, was originally released in 1976. Its first members were 8048, 8035 and 8748. Initially this family was produced using NMOS-technology, in the early 1980s it became available in CMOS-technology. It was still manufactured into the 1990s to support older designs that still used it.
The MCS-48 series has a Modified Harvard architecture, with internal or external program ROM and 64 256 bytes of internal (on-chip) RAM. The I/O is mapped into its own address space, separate from programs and data. The 8048 is probably the most prominent member of Intel's MCS-48 family of microcontrollers.
Though the MCS-48 series was eventually replaced by the very popular MCS-51 series, even at around year 2000 it remained quite popular, due to its low cost, wide availability, memory efficient one-byte instruction set, and mature development tools. Because of this it is much used in high-volume consumer electronics devices such as TV sets, TV remotes, toys, and other gadgets where cost-cutting is essential.



The 8049 has 2 KB of masked ROM (the 8748 and 8749 had EPROM) that can be replaced with a 4 KB external ROM, as well as 128 bytes of RAM and 27 I/O ports. The  C's oscillator block divides the incoming clock into 15 internal phases, thus with its 11 MHz max. crystal one gets 0.73 MIPS (of one-clock instructions). Some 70% of instructions are single byte/cycle ones, but 30% need two cycles and/or two bytes, so the raw performance would be closer to 0.5 MIPS.
Philips Semiconductors (now NXP) owned a license to produce this series and developed their MAB8400-family based on this architecture. These were the first microcontrollers with an integrated I C-interface and were used in the first Philips (Magnavox in the US) Compact Disc players (e.g. the CD-100).
Another variant, the ROM-less 8035, was used in Nintendo's arcade game Donkey Kong. Although not being a typical application for a microcontroller, its purpose was to generate the background music of the game.
The Intel 8748 has on-chip clock oscillator, 2  8-bit timers, 27  I/O ports, 64 bytes of RAM and 1 KB of EPROM. A version with 2 KB EPROM and 128 bytes RAM was also available under the 8749 number.



The 8048 was used in the Magnavox Odyssey  video game console, the Korg Trident series, the Korg Poly-61, Roland Jupiter-4 and Roland ProMars analog synthesizers.
The original IBM PC keyboard used an 8048 as its internal microcontroller. The PC AT replaced the PC's Intel 8255 peripheral interface chip at I/O port addresses 0x60-63 with an 8042 accessible through port addresses 0x60 and 0x64. As well as managing the keyboard interface the 8042 controlled the A20 line of the AT's Intel 80286 CPU, and could be commanded by software to reset the 80286 (unlike the 80386 and later processors, the 80286 had no way of switching from protected mode back to real mode except by being reset). Later PC compatibles integrate the 8042's functions into their super I/O devices.



MCS-48
MCS-48 Single Component Microcomputer, Applications Seminar Notebook, 1978, Intel Corporation.
MCS-48 MICROCOMPUTER USER'S MANUAL, 1978, Intel Corporation.
Lionel Smith, Cecil Moore: Serial I/O and Math Utilities for the 8049 Microcomputer, Application Note AP-49, January 1979, Intel Corporation.
A High-Speed Emulator for Intel MCS-48 Microcomputers, Application Note AP-55A, August 1979, Intel Corporation.
Phil Dahm, Stuart Rosenberg: Intel MCS-48 and UPI-41A Microcontrollers, Reliability Report RR-25, December 1979, Intel Corporation.
Microcontroller Handbook, Intel 1984, Order number 210918-002.
8-Bit Embedded Controllers, Intel 1991, Order number 270645-003.
UPI-41
UPI-41A User's Manual, Intel 1980, Order number 9800504-02 Rev. B.
Microprocessor Peripherals UPI-41A/41AH/42/42AH User's Manual, October 1993, Order number 231318-006, Intel Corporation.
Johan Beaston, Jim Kahn: An 8741A/8041A Digital Cassette Controller, Application Note AP-90, May 1980, Intel Corporation.


