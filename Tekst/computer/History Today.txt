History Today is an illustrated history magazine. Published monthly in London since January 1951, it aims to present serious and authoritative history to as wide a public as possible. It covers all periods and geographical regions and publishes articles of traditional narrative history alongside new research and historiography. A sister publication History Review, produced tri-annually until April 2012, provided information for sixth-form history students.



Founded by Brendan Bracken, Minister of Information after the Second World War, chairman of the Financial Times and lieutenant to Sir Winston Churchill, the magazine has been independently owned since 1981. The founding co-editors were Peter Quennell (1951 79) and Alan Hodge (1951 78); subsequent editors were Michael Crowder (1979 81); Michael Trend (1981 82); Juliet Gardiner (1981 85); Gordon Marsden (1985 97) and Peter Furtado (1997 2008). The current editor is Paul Lay.
The website contains all the magazine's published content since 1951 and launched a digital edition in 2012.
History Review was a tri-annual sister publication of History Today magazine publishing material for sixth-form level history students. The final issue of History Review was published in April 2012 but the archive of published material is available for research in the History Today archive.
In 1995 it compiled The History today companion to British history (London: Collins & Brown, 1995), with 4500 entries covering the entire field in 840 pages edited by Neil Wenborn.



History Today generally commissions its articles directly from academic authors and historians, though it does accept unsolicited essays from freelance historians and others if the article is deemed to be serious history, of wide interest or of academic worth.



In conjunction with the Longman/History Today Charitable Trust, History Today holds an annual awards ceremony at which presentations are made to historians, researchers and others whose worked has helped promote history to the wider public.
In 2003, the magazine instituted a prize for the best undergraduate dissertation.






Official website