Unconventional computing is computing by a wide range of new or unusual methods. It is also known as alternative computing. The different methods of unconventional computing include optical computing, quantum computing, chemical computing, natural computing, biologically-inspired computing, wetware computing, DNA computing, molecular computing, amorphous computing, nanocomputing, reversible computing, ternary computing, fluidics, analogue computing, human and domino computation.
Historically, mechanical computers were used in industry before the advent of the transistor. Mechanical computers retain some interest today both in research and as analogue computers. Some mechanical computers have a theoretical or didactic relevance, such as billiard-ball computers or hydraulic ones,. While some are actually simulated, others are not; no attempt is made to build a functioning computer through the mechanical collisions of billiard balls. The domino computer is another theoretically interesting mechanical computing scheme.
Unconventional computing is, according to a recent conference description, "an interdisciplinary research area with the main goal to enrich or go beyond the standard models, such as the Von Neumann computer architecture and the Turing machine, which have dominated computer science for more than half a century". These methods model their computational operations based on non-standard paradigms, and are currently mostly in the research and development stage. This computing behavior can be "simulated" using the classical silicon-based micro-transistors or solid state computing technologies, but aim to achieve a new kind of computing engineering inspired in nature.



Billiard balls (billiard ball computer)
This is an unintuitive and pedagogical example that a computer can be made out of almost anything.
Light (optical computing)
Computers can manipulate information as light (rather than electricity or billiard balls).
Molecules (DNA computing, chemical computing)
Gears, levels, dials, etc. (mechanical computer)
Continuous-valued electronics or fluidics, etc. (e.g. analog computer although these may also be implemented mechanically)
Neurons (wetware computer)
Fluid (fluidics)
Software agents acting under a special set of rules (e.g. cellular automata)
Quantum mechanics (quantum computing)
Human computer
Chaos computing
Stochastic computing


