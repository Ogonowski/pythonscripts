The Federal Criminal Police Office of Germany (in German:  Bundeskriminalamt , abbreviated  BKA ) is the federal investigative police agency of Germany, directly subordinated to the Federal Ministry of the Interior. It is headquartered in Wiesbaden, Hesse, and maintains major branch offices in Berlin and Meckenheim near Bonn. It is headed by Holger M nch since Dec 2014.
Primary jurisdiction of the agency includes coordinating cooperation between the federation and state police forces; investigating cases of international organized crime, terrorism and other cases related to national security; counterterrorism; the protection of members of the constitutional institutions, and of federal witnesses. When requested by the respective state authorities or the federal minister of the interior, it also assumes responsibility for investigations in certain large-scale cases. Furthermore, the Attorney General of Germany can direct it to investigate cases of special public interest.



The Federal Criminal Police Office was founded in Germany in 1951.



The BKA's missions include:
Coordinating cooperation between the federation and state police forces (especially state criminal investigation authorities) and with foreign investigative authorities.
Collecting and analyzing criminal intelligence, managing the INPOL database of all important crimes and criminals.
Investigating cases of terrorism or other areas of political motivated crime, as well as narcotics, weapons and financial/economical crime.
Protection of federal witnesses.
Acting as a clearing house for identifying and cataloging images and information on victims of child sexual exploitation, similar to the National Center for Missing & Exploited Children in the United States.

The BKA provides assistance to the states in forensic matters, research and organized crime investigations. It is Germany's national central bureau for the European Police Office (Europol), International Criminal Police Organization (Interpol), Schengen Information System, and the German criminal Automated Fingerprint Identification System (AFIS).
The DVI-Team (in German: Identifizierungskommission or more common IDKO) is an event driven organisation of mainly forensic specialists dedicated to identification of disaster victims. The DVI's past missions include several airplane crashes, the Eschede train disaster and the 2004 Indian Ocean earthquake.
The Close Protection Group protects the members of Germany's constitutional bodies and their foreign guests of state and is often the most visible part of the BKA. Specially selected and trained officers with special equipment and vehicles provide round-the-clock personal security to those they protect. The Protection Group is now headquartered in Berlin.
Approximately 5,200 BKA personnel operate nationwide and (e.g. as liaison officers) in 60 countries around the globe.




Dr. Max Hagemann (1951 1952)
Dr. Hanns Jess (1952 1955)
Reinhard Dullien (1955 1964)
Paul Dickopf (1965 1971)
Horst Herold (1971   March 1981)
Heinrich Boge (March 1981   1990)
Hans-Ludwig Zachert (1990   April 1996)
Klaus Ulrich Kersten (April 1996   February 26, 2004)
J rg Ziercke (February 26, 2004 - December 2014)
Holger M nch (since 1st December 2014)



BKA buildings


