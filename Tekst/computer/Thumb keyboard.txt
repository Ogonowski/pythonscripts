A thumb keyboard, or a thumb board, is a type of keyboard commonly found on PDAs, mobile phones, and PDA phones which has a familiar layout to an ordinary keyboard, such as QWERTY. The inputting surface is usually relatively small, and is intended for typing using the available thumbs, while holding the device.



BlackBerry
Some Nokia phones including Nokia 6800 series
HTC TyTN, HTC TyTN II, HTC Touch Pro, HTC Touch Pro2
Some Sony CLIE PDAs
Some Hewlett Packard PDAs (HP iPaq 4350/4355/63XX series)
Microsoft Xbox 360 Controller "Chatpad"
Motorola MPx220
Palm Treo smartphones, with the exception of the Treo 180g
Palm Tungsten C
Samsung products including Samsung Blackjack (SGH-i607)
Motorola Q
Wibrain: B1, B1L, B1H, I1


