Professor Sir Timothy John Berners-Lee, OM, KBE, FRS, FREng, FRSA, DFBCS (born 8 June 1955), also known as TimBL, is an English computer scientist, best known as the inventor of the World Wide Web. He made a proposal for an information management system in March 1989, and he implemented the first successful communication between a Hypertext Transfer Protocol (HTTP) client and server via the Internet sometime around mid-November of that same year.
Berners-Lee is the director of the World Wide Web Consortium (W3C), which oversees the Web's continued development. He is also the founder of the World Wide Web Foundation, and is a senior researcher and holder of the Founders Chair at the MIT Computer Science and Artificial Intelligence Laboratory (CSAIL). He is a director of the Web Science Research Initiative (WSRI), and a member of the advisory board of the MIT Center for Collective Intelligence. In 2011 he was named as a member the Board of Trustees of the Ford Foundation.
In 2004, Berners-Lee was knighted by Queen Elizabeth II for his pioneering work. In April 2009, he was elected a foreign associate of the United States National Academy of Sciences. He was honoured as the "Inventor of the World Wide Web" during the 2012 Summer Olympics opening ceremony, in which he appeared in person, working with a vintage NeXT Computer at the London Olympic Stadium. He tweeted "This is for everyone", which instantly was spelled out in LCD lights attached to the chairs of the 80,000 people in the audience.



Berners-Lee was born in London, England, one of four children born to Mary Lee Woods and Conway Berners-Lee. His parents worked on the first commercially-built computer, the Ferranti Mark 1. He attended Sheen Mount Primary School, and then went on to attend south west London's independent Emanuel School from 1969 to 1973. A keen trainspotter as a child, he learnt about electronics from tinkering with a model railway. He studied at The Queen's College, Oxford from 1973 to 1976, where he received a first-class degree Bachelor of Arts degree in physics.




After graduation, Berners-Lee worked as an engineer at the telecommunications company Plessey in Poole, Dorset. In 1978, he joined D. G. Nash in Ferndown, Dorset, where he helped create type-setting software for printers.
Berners-Lee worked as an independent contractor at CERN from June to December 1980. While there, he proposed a project based on the concept of hypertext, to facilitate sharing and updating information among researchers. To demonstrate, he built a prototype system named ENQUIRE.
After leaving CERN in late 1980, he went to work at John Poole's Image Computer Systems, Ltd, in Bournemouth, Dorset. He ran the company's technical side for three years. The project he worked on was a "real-time remote procedure call" which gave him experience in computer networking. In 1984, he returned to CERN as a fellow.
In 1989, CERN was the largest Internet node in Europe, and Berners-Lee saw an opportunity to join hypertext with the Internet:

Berners-Lee wrote his proposal in March 1989 and, in 1990, redistributed it. It was then accepted by his manager, Mike Sendall. He used similar ideas to those underlying the ENQUIRE system to create the World Wide Web, for which he designed and built the first Web browser. His software also functioned as an editor (called WorldWideWeb, running on the NeXTSTEP operating system), and the first Web server, CERN HTTPd (short for Hypertext Transfer Protocol daemon).

The first website built was at CERN within the border of France, and was first put online on 6 August 1991:

It provided an explanation of what the World Wide Web was, and how one could use a browser and set up a web server.
In 1994, Berners-Lee founded the W3C at MIT, which is located in Massachusetts, United States (US). It comprised various companies that were willing to create standards and recommendations to improve the quality of the Web. Berners-Lee made his idea available freely, with no patent and no royalties due. The World Wide Web Consortium decided that its standards should be based on royalty-free technology, so that they could easily be adopted by anyone.
In 2001, Berners-Lee became a patron of the East Dorset Heritage Trust, having previously lived in Colehill in Wimborne, East Dorset.
In December 2004, he accepted a chair in Computer Science at the School of Electronics and Computer Science, University of Southampton, Hampshire, to work on the Semantic Web.
In a Times article in October 2009, Berners-Lee admitted that the initial pair of slashes ("//") in a web address were actually "unnecessary". He told the newspaper that he could easily have designed web addresses not to have the slashes. "There you go, it seemed like a good idea at the time," he said in his lighthearted apology.




In June 2009 then British Prime Minister Gordon Brown announced Berners-Lee would work with the UK Government to help make data more open and accessible on the Web, building on the work of the Power of Information Task Force. Berners-Lee and Professor Nigel Shadbolt are the two key figures behind data.gov.uk, a UK Government project to open up almost all data acquired for official purposes for free re-use. Commenting on the opening up of Ordnance Survey data in April 2010 Berners-Lee said that: "The changes signal a wider cultural change in Government based on an assumption that information should be in the public domain unless there is a good reason not to not the other way around." He went on to say "Greater openness, accountability and transparency in Government will give people greater choice and make it easier for individuals to get more directly involved in issues that matter to them."
In November 2009, Berners-Lee launched the World Wide Web Foundation in order to "Advance the Web to empower humanity by launching transformative programs that build local capacity to leverage the Web as a medium for positive change."
Berners-Lee is one of the pioneer voices in favour of Net Neutrality, and has expressed the view that ISPs should supply "connectivity with no strings attached," and should neither control nor monitor customers' browsing activities without their expressed consent. He advocates the idea that net neutrality is a kind of human network right: "Threats to the Internet, such as companies or governments that interfere with or snoop on Internet traffic, compromise basic human network rights."
Berners-Lee joined the board of advisors of start-up State.com, based in London.
As of May 2012, Berners-Lee is President of the Open Data Institute.

The Alliance for Affordable Internet (A4AI) was launched in October 2013 and Berners-Lee is leading the coalition of public and private organisations that includes Google, Facebook, Intel and Microsoft. The A4AI seeks to make Internet access more affordable so that access is broadened in the developing world, where only 31% of people are online. Berners-Lee will help to decrease internet access prices so that they fall below the UN Broadband Commission's worldwide target of 5% of monthly income.




Berners-Lee has received many awards and honours.
Berners-lee was knighted in 2004 when he was promoted to Knight Commander of the Order of the British Empire (KBE) in the New Year Honours "for services to the global development of the Internet", and was formally invested on 16 July 2004. On 13 June 2007, he received the Order of Merit, becoming one of only 24 living members entitled to hold the honour, and to use the post-nominals 'O.M.' after their name. (The Order of Merit is within the personal bestowal of The Queen, and does not require recommendation by ministers or the Prime Minister). He was elected a Fellow of the Royal Society (FRS) in 2001.|



In 2014 Berners-Lee and Rosemary Leith were married at St James's Palace in London. Berners-Lee was previously married to Nancy Carlson in 1990 and the marriage ended in divorce. Rosemary Leith is Director of the World Wide Web Foundation and Fellow at Harvard University's Berkman Center. She was previously World Economic Forum Global Agenda Council Chair of the Future of Internet Security  and is on the board of YouGov.



Berners-Lee was raised as an Anglican, but in his youth, he turned away from religion, and in his adulthood he became a Unitarian Universalist (UU). He has stated: "Like many people, I had a religious upbringing which I rejected as a teenager... Like many people, I came back to religion when we had children". He and his wife wanted to teach spirituality to his children, and after hearing a Unitarian minister and visiting the UU Church, they opted for it. He is an active member of that Church, to which he adheres because he perceives it as a tolerant and liberal belief. He has also recognized the value of other faiths, stating: "I believe that much of the philosophy of life associated with many religions is much more sound than the dogma which comes along with it. So I do respect them."


