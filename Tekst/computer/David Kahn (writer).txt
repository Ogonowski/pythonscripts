David Kahn (b. February 7, 1930) is a US historian, journalist and writer. He has written extensively on the history of cryptography and military intelligence.
Kahn's first published book, The Codebreakers - The Story of Secret Writing (1967), has been widely considered to be a definitive account of the history of cryptography.



David Kahn was born in New York City to Florence Abraham Kahn, a glass manufacturer, and Jesse Kahn, a lawyer.
Kahn has said he traces his interest in cryptography to reading Fletcher Pratt's Secret and Urgent as a boy. Kahn is a founding editor of the Cryptologia journal. In 1969, Kahn married Susanne Fiedler; they are now divorced. They have two sons, Oliver and Michael.
He attended Bucknell University. After graduation, he worked as a reporter at Newsday for several years. He also served as an editor at the International Herald Tribune in Paris for two years in the 1960s.
It was during this period that he wrote an article for the New York Times Magazine about two defectors from the National Security Agency. This article was the origin of his monumental book, The Codebreakers.




The Codebreakers comprehensively chronicles the history of cryptography from ancient Egypt to the time of its writing. It is widely regarded as the best account of the history of cryptography up to its publication. Most of the editing, German translating, and insider contributions were from the American World War II cryptographer, Bradford Hardie III. William Crowell, the former deputy director of the National Security Agency, was quoted in Newsday as saying "Before he (Kahn) came along, the best you could do was buy an explanatory book that usually was too technical and terribly dull."
Kahn, then a newspaper journalist, was contracted to write a book on cryptology in 1961. He began writing it part-time, at one point quitting his regular job to work on it full-time. The book was to include information on the National Security Agency (NSA), and according to the author James Bamford writing in 1982, the agency attempted to stop its publication, and considered various options, including publishing a negative review of Kahn's work in the press to discredit him. A committee of the United States Intelligence Board concluded that the book was "a possibly valuable support to foreign COMSEC [communications security] authorities" and recommended "further low-key actions as possible, but short of legal action, to discourage Mr. Kahn or his prospective publishers". Kahn's publisher, the Macmillan company, handed over the manuscript to the federal government for review without Kahn's permission on March 4, 1966. Kahn and Macmillan eventually agreed to remove some material from the manuscript, particularly concerning the relationship between the NSA and its British counterpart, the GCHQ.
The Codebreakers did not cover most of the history concerning the breaking of the German Enigma machine (which became public knowledge only in the 1970s). Nor did it cover the advent of strong cryptography in the public domain, beginning with the invention of public key cryptography and the specification of the Data Encryption Standard in the mid-1970s. The book was republished in 1996, and the new edition included an additional chapter briefly covering the events since the original publication.
The Codebreakers was a finalist for the non-fiction Pulitzer Prize in 1968.




Kahn was awarded a doctorate (D.Phil) from Oxford University in 1974, in modern German history under the supervision of the then Regius professor of modern history, Hugh Trevor-Roper.
Kahn continued his work as a reporter and op-ed editor for Newsday until 1998, and also served as a journalism professor at New York University.
Despite past differences between Kahn and the National Security Agency over the information in The Codebreakers, Kahn was selected in 1995 to become NSA's scholar-in-residence. On October 26, 2010, Kahn attended a ceremony at NSA's National Cryptologic Museum (NCM) to commemorate his donation of his lifetime collection of cryptologic books, memorabilia, and artifacts to the museum and its library. The collection is housed at the NCM library and is non-circulating (that is, items cannot be checked out or loaned out), but photocopying and photography of items in the collection are allowed.
Kahn lives (as of 2012) in New York City. He has lived in Washington, D.C.; Paris, France; Freiburg, Germany; Oxford, England; and Great Neck, New York.



Plaintext in the new unabridged: An examination of the definitions on cryptology in Webster's Third New International Dictionary (Crypto Press 1963)
The Codebreakers   The Story of Secret Writing (ISBN 978-0-684-83130-5) (1967)
Hitler's Spies: German Military Intelligence in World War II (Macmillan 1978) (ISBN 0-02-560610-7)
The Codebreakers   The Story of Secret Writing Revised edition (ISBN 0-684-83130-9) (1996)
Cryptology goes Public (Council on Foreign Relations 1979)
Notes & correspondence on the origin of polyalphabetic substitution (1980)
Codebreaking in World Wars I and II: The major successes and failures, their causes and their effects (Cambridge University Press 1980)
Kahn on Codes: Secrets of the New Cryptology (Macmillan 1984) (ISBN 978-0-02-560640-1)
Cryptology: Machines, History and Methods by Cipher Deavours and David Kahn (Artech House 1989) (ISBN 978-0-89006-399-6)
Seizing the Enigma: The Race to Break the German U-Boats Codes, 1939 1943 (Houghton Mifflin 1991) (ISBN 978-0-395-42739-2)
The Reader of Gentlemen's Mail: Herbert O. Yardley and the Birth of American Codebreaking (Yale University Press 2004) (ISBN 978-0-300-09846-4)
How I Discovered World War II's Greatest Spy and Other Stories of Intelligence and Code, Boca Raton : CRC Press, Taylor & Francis Group, 2014. ISBN 978-1466561991


