Single Connector Attachment, or SCA, is a type of connection for the internal cabling of SCSI systems. There are two versions of this connector: the SCA-1, which is deprecated, and SCA-2, which is the most recent standard. In addition there are Single-Ended (SE) and Low Voltage Differential (LVD) types of the SCA.
SCA is no longer in widespread use, having been superseded by Serial attached SCSI.
Since hard disk drives are among the components of a server computer that are the most likely to fail, there has always been demand for the ability to replace a faulty drive without having to shut down the whole system. This technique is called hot-swapping and is one of the main motivations behind the development of SCA. In connection with RAID, for example, this allows for seamless replacement of failed drives.
Normally, hard disk drives make use of two cables: one for data and one for power, and they also have their specific parameters (SCSI ID etc.) to be set using jumpers on each drive. Drives employing SCA have only one plug which carries both data and power and also allows them to receive their configuration parameters from the SCSI backplane. The SCA connector for parallel SCSI drives has 80 pins, as opposed to the 68 pin interface found on most modern parallel SCSI drives.
Some of the pins in SCA connectors are longer than others, so they are connected first and disconnected last. This ensures the electrical integrity of the whole system. Otherwise, the angle at which the plug is inserted into the drive could be the reason for damage because, for instance, the pin carrying the voltage could get connected before its corresponding ground reference pin. The additional length also provides what is known as a pre-charge which provides a means whereby the device is alerted to a pending power surge. That allows a slower transition to full power and thereby makes the device more stable.

To make better use of their hot-plugging capability, SCA drives usually are installed into drive bays into which they slide with ease. At the far end of these bays is the backplane of the SCSI subsystem located with a connector that plugs into the drive automatically when it is inserted.
Full hot-swappable functionality still requires the support of other software and hardware components of the system. In particular the operating system and RAID layers will need hot-swap support to enable hard drive hot-swapping to be carried out without shutting down the system.



SCSI connector, for a description of other SCSI connectors
Fibre Channel electrical interface, for details of the SCA-40 connector
SAF-TE - active backplanes for sensoring and swap assistance



SCSI devices and connectors are specified in SPI-2, SPI-4 and SPI-5 as part of the SCSI-3 Standards Architecture: http://www.t10.org/scsi-3.htm
Organization responsible for drafts of the specification documents is the Technical Committee T10: http://www.t10.org/