The 2013 IIHF World Championship was the 77th event hosted by the International Ice Hockey Federation (IIHF), held in Stockholm, Sweden, and Helsinki, Finland, between 3 19 May 2013. TV4 and MTV3 served as host broadcasters of the event.
The host team Sweden won the team's ninth title in history by defeating Switzerland in the final 5 1, and became the first host team to win the tournament since the Soviet Union team won the 1986 World Championship in Moscow, Soviet Union. The Swedish team started the tournament with an unconvincing performance but managed to get a collective boost when the Sedin brothers joined the team after the Vancouver Canucks had been eliminated in the Stanley Cup playoffs. Switzerland sent a clear message about their recently improved hockey program by going undefeated through the tournament before the final; finishing first in their group (ahead of Canada and Sweden); and earning their second silver medal in history, as well as the team's first medal since 1953.



At the semi-annual congress in Vancouver on 21 September 2007, Sweden was voted the host of the 2013 tournament, defeating the runner-up Belarus by 55 votes. Other countries in the running were Hungary, Czech Republic and Latvia (which withdrew from the race and endorsed the Swedish bid). At the congress in Bern in 2009, it was announced that Finland (the host for the 2012 World Championship) and Sweden would co-host both the 2012 and 2013 tournaments.






The host arenas were the Ericsson Globe in Stockholm (12,500 permitted seats) and Hartwall Areena in Helsinki (13,506 permitted seats). Capacity has been limited to these numbers because of modern health and safety rules. Malm  Arena was originally planned to co-host according to the Swedish bid, but the Swedish Hockey Federation decided to drop Malm  as a host city when they decided to collaborate with Finland before the Eurovision Song Contest 2013, which took place in that arena. Tampere was also a candidate to be the Finnish venue, but due to a delay in construction of the new Tampereen Keskusareena, Helsinki was named as co-host. Tele2 Arena, a new retractable-roof multi-purpose stadium seating 30,000 spectators, was planned to host at least one game, but due to construction delays it would not be finished until July 2013, two months after the World Championship.



The format of the tournament was the same as in 2012, which was also co-hosted by Helsinki and Stockholm. Sixteen teams were divided into two groups of eight, who played a seven-game round-robin within their groups. The top four teams in each group advanced to a three-round single-knockout playoff.
The only difference from 2012 was that the semifinals and medal games were played in Stockholm instead of Helsinki.







Each team's roster consisted of at least 15 skaters (forwards and defencemen) and two goaltenders, and at most 22 skaters and three goaltenders. All sixteen participating nations, through the confirmation of their respective national associations, had to submit a roster by the first IIHF directorate meeting.



The IIHF selected 16 referees and 16 linesmen to work the 2013 IIHF World Championship. They were the following:




The seeding in the preliminary round is based on the 2012 IIHF World Ranking, which ended at the conclusion of the 2012 IIHF World Championship. The teams were grouped according to seeding (in parenthesis is the corresponding world ranking). However, Russia and the Czech Republic swapped their slots between their groups to optimize the seeding for the Finnish-Swedish organizers.






All times are local (UTC+3).



All times are local (UTC+2).






The games in Stockholm are UTC+2, while the games in Helsinki are UTC+3.



All times are local (UTC+2).



Time is local (UTC+2).




Time is local (UTC+2).






The official IIHF final ranking of the tournament:



List shows the top skaters sorted by points, then goals.
GP = Games played; G = Goals; A = Assists; Pts = Points; +/  = Plus/Minus; PIM = Penalties in Minutes; POS = Position
Source: IIHF.com



Only the top ten goaltenders, based on save percentage, who have played at least 40% of their team's minutes, are included in this list.
TOI = Time on Ice (minutes:seconds); SA = Shots Against; GA = Goals Against; GAA = Goals Against Average; Sv% = Save Percentage; SO = Shutouts
Source: IIHF.com






Official website