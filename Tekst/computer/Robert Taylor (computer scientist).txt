Robert William Taylor (born 1932), known as Bob Taylor, is an Internet pioneer, who led teams that made major contributions to the personal computer, and other related technologies. He was director of ARPA's Information Processing Techniques Office from 1965 through 1969, founder and later manager of Xerox PARC's Computer Science Laboratory from 1970 through 1983, and founder and manager of Digital Equipment Corporation's Systems Research Center until 1996.
His awards include the National Medal of Technology and Innovation and the Draper Prize. Taylor is known for his high-level vision: "The Internet is not about technology; it's about communication. The Internet connects people who have shared interests, ideas and needs, regardless of geography."



Robert W. Taylor was born in Dallas, Texas in 1932. His adoptive father was a Methodist minister and the family spent an itinerant childhood, moving from parish to parish. He started at Southern Methodist University at 16, served a stint in the Navy during the Korean War, and went back to school at the University of Texas under the GI Bill. At UT he was a "professional student," he says, taking courses for pleasure. He finally put them together for a degree in experimental psychology, with minors in math, philosophy, English and religion. While Taylor was trained as an experimental psychologist and mathematician his earliest career was devoted to brain research and the auditory nervous system.
Taylor taught math and coached basketball at a co-ed prep school in Florida. "I had a wonderful time but was very poor, with a second child -- who turned out to be twins -- on the way," he says.
Taylor took engineering jobs with aircraft companies at better salaries. After working for defense contractor Martin Marietta, he was invited to join NASA in 1961 after submitting a research proposal for a flight-control simulation display.



Taylor worked for NASA in Washington, DC while the Kennedy administration was backing scientific projects such as the Apollo program for a manned moon landing. In late 1962 Taylor met J.C.R. Licklider, who was heading the new Information Processing Techniques Office of the Advanced Research Project Agency (ARPA) of the United States Department of Defense. Licklider had done his graduate work in psychoacoustics as Taylor, and wrote an article in 1960 envisioning new ways to use computers.
He met another visionary, Douglas Engelbart, at the Stanford Research Institute in Menlo Park, California. Taylor directed funding to Engelbart's studies of computer-display technology at SRI that led to the computer mouse. The public demonstration of a mouse-based user interface was later called "the Mother of All Demos." At the 1968 Fall Joint Computer Conference in San Francisco, Engelbart, Bill English, Jeff Rulifson and the rest of the Human Augmentation Research Center team at SRI showed on a big screen how he could manipulate a computer remotely located in Menlo Park, while sitting on a San Francisco stage, using his mouse.



In 1965 Taylor moved from N.A.S.A to ARPA, first as a deputy to Ivan Sutherland to fund a few large programs in advanced research in computing at major universities and corporate research centers throughout the US. Among the computer projects that ARPA supported was time-sharing, in which many users could work at terminals to share a single large computer. Users could work interactively instead of using punched cards or punched tape in a batch processing style. Taylor's office in the Pentagon had a terminal connected to time-sharing at MIT, a terminal connected to the Berkeley Timesharing System at the University of California at Berkeley, and a third terminal to the System Development Corporation in Santa Monica, California. He noticed each system developed a community of users, but was isolated from the other communities.
Taylor hoped to build a computer network to connect the ARPA-sponsored projects together, if nothing else to let him communicate to all of them through one terminal. Sutherland returned to a teaching position, and by June 1966 Taylor was officially director of Information Processing Techniques Office (IPTO) where he directed the ARPANET project until 1969. Taylor had convinced ARPA's Director Charles M. Herzfeld to fund a network project earlier in February 1966, and hired Lawrence G. Roberts from MIT's Lincoln Laboratory to be its first program manager. Roberts first resisted moving to Washington DC, until Herzfeld reminded the director of Lincoln Laboratory that ARPA dominated its funding. Licklider continued to provide guidance, and Wesley A. Clark suggested the use of a dedicated computer, called the Interface Message Processor at each node of the network instead of centralized control. ARPA issued a request for quotation (RFQ) to build the system, which was awarded to Bolt, Beranek and Newman (BBN). ATT Bell Labs and IBM Research were invited to join, but were not interested. At a pivotal meeting in 1967 most participants resisted testing the new network; they thought it would slow down their research.
A second paper, "The Computer as a Communication Device" published in 1968 by Licklider and Taylor, lays out the future of what the Internet would eventually become. Their paper starts out: "In a few years, men will be able to communicate more effectively through a machine than face to face." The vision would take more than "a few years".
At some point Taylor was sent by ARPA to investigate inconsistent reports coming from the Vietnam War. Only about 35 years old, he was given the military rank equivalent to his civilian position: brigadier general, and made several trips to the area. He helped set up a computer center at the Military Assistance Command, Vietnam base in Saigon. In his words: "After that the White House got a single report rather than several. That pleased them; whether the data was any more correct or not, I don't know, but at least it was more consistent." The Vietnam project took him away from directing research, and "by 1969 I knew ARPAnet would work. So I wanted to leave." For about a year Taylor joined Sutherland and David C. Evans at the University of Utah, where he had funded a center for research on computer graphics while at ARPA.
In 1970 Taylor moved to Palo Alto, California for his next historic job.



Jerome I. Elkind from BBN was hired by George Pake to co-manage the Computer Systems Laboratory (CSL) at the new Palo Alto Research Center of Xerox Corporation. Taylor assumed he would run day-to-day operations, while Elkind assumed Taylor would be associate director.
Technologies developed at PARC between 1970 and 1983 focused on reaching beyond ARPAnet to develop what has become the Internet, and the systems that support today's personal computers. They included:
Powerful personal computers (the Xerox Alto) with windowed displays and graphical user interfaces that were the basis of the Macintosh.
Ethernet, which networks local computers within a building or campus; and the first Internet, a network that connected the Ethernet to the ARPAnet utilizing PUP (PARC Universal Protocol), forerunner to TCP/IP.
The electronics and software that led to the laser printer and the graphical programs that allowed John Warnock and Chuck Geschke to take off and found Adobe Systems.
"What-you-see-is-what-you-get (WYSIWYG) word-processing programs, such as Bravo that Charles Simonyi took to Microsoft to serve as the basis for Microsoft Word.
Elkind was involved in a number of corporate and government projects. After one of Elkind's extended absences, Taylor became the official manager of the laboratory in early 1978. In 1983, integrated circuit specialist William J. Spencer became director of PARC. Spencer blamed Taylor for the failure of Xerox's own commercialization efforts. Taylor and most of the researchers at CSL left Xerox.



Taylor was hired by Ken Olsen of Digital Equipment Corporation, and formed the Systems Research Center in Palo Alto. Many of the former CSL researchers came to work at SRC. Among the projects at SRC were the Modula-3 programming language; the snoopy cache, used in the Firefly multiprocessor workstation; the first multi-threaded Unix system; the first User Interface editor; and a networked Window System.



Taylor retired in 1996 and lives in Woodside, California. In 2000 he voiced two concerns about the future of the Internet: control and access. In his words:

There are many worse ways of endangering a larger number of people on the Internet than on the highway. It's possible for people to generate networks that reproduce themselves and are very difficult or impossible to kill off. I want everyone to have the right to use it, but there's got to be some way to insure responsibility.

Will it be freely available to everyone? If not, it will be a big disappointment.



In 1984, Taylor, Butler Lampson, and Charles P. Thacker received the ACM Software Systems Award "For conceiving and guiding the development of the Xerox Alto System demonstrating that a distributed personal computer system can provide a desirable and practical alternative to time-sharing." In 1994, all three were named ACM Fellows in recognition of the same work. In 1999, Taylor received a National Medal of Technology and Innovation. The citation read "For visionary leadership in the development of modern computing technology, including computer networks, the personal computer and the graphical user interface."
In 2004, the National Academy of Engineering awarded him along with Lampson, Thacker and Alan Kay their highest award, the Draper Prize. The citation reads: "for the vision, conception, and development of the first practical networked personal computers."
In 2013, the Computer History Museum named him a Museum Fellow, "for his leadership in the development of computer networking, online information and communications systems, and modern personal computing."






M. Mitchell Waldrop (2001). The Dream Machine: J. C. R. Licklider and the Revolution That Made Computing Personal. New York: Viking Penguin. ISBN 0-670-89976-3. 
Michael A. Hiltzik (April 4, 2000). The Dealers of Lightning: Xerox PARC and the Dawn of the Computer Age. HarperCollins. ISBN 0-88730-989-5. 
"In Memoriam: J. C. R. Licklider 1915 1990" (PDF). Palo Alto, California: Digital Equipment Corporation Systems Research Center. August 7, 1990.  Reprints of early papers with preface by Taylor



The New Old Boys From the ARPAnet Extract from 'Tools for Thought' by Howard Rheingold
1984 ACM Software Systems Award citation
1994 ACM Fellow citation
2004 Draper Prize citation
Paul McJones (October 11, 2008). "Oral History of Robert (Bob) W. Taylor" (PDF). Computer History Museum. Retrieved March 30, 2011.