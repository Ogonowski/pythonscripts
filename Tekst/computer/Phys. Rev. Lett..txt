Physical Review Letters (PRL), established in 1958, is a peer-reviewed, scientific journal that is published 52 times per year by the American Physical Society. As also confirmed by various measurement standards, which include the Journal Citation Reports impact factor and the journal h-index proposed by Google Scholar, many physicists and other scientists consider Physical Review Letters one of the most prestigious journals in the field of physics.
PRL is published as a print journal, and is in electronic format, online and CD-ROM. Its focus is rapid dissemination of significant, or notable, results of fundamental research on all topics related to all fields of physics. This is accomplished by rapid publication of short reports, called "Letters". Papers are published and available electronically one article at a time. When published in such a manner, the paper is available to be cited by other work. The new Leading Editor is Pierre Meystre. The Managing Editor is Reinhardt B. Schuhmann.



Physical Review Letters is an internationally read physics journal, describing a diverse readership. Advances in physics, as well as cross disciplinary developments, are disseminated weekly, via this publication. Topics covered by this journal are also the explicit titles for each section of the journal. Sections are delineated (in the table of contents) as follows:
General Physics: Statistical and Quantum mechanics, Quantum information, etc.
Gravitation and Astrophysics
Elementary Particles and Fields
Nuclear physics
Atomic, Molecular, and Optical physics
Nonlinear dynamics, Fluid dynamics, Classical optics, etc.
Plasma and Beam physics
Condensed matter: Structure, etc.
Condensed Matter: Semiconductor-Electronic properties, etc.
Soft matter, Biological, and Interdisciplinary physics
Worthy of note is a section which consists of emphasized articles. This section is designed to be articles suggested by the editors of the journal, which have been covered by Viewpoint in Physics. Further criteria for these selections are these may be features in Physical Review Focus, or these selections are worthy of note for other reasons.



On May 20, 1899, 36 physicists gathered to establish the American Physical Society at Columbia University, in the City of New York. These 36 decided that the mission of the APS would be "to advance and diffuse the knowledge of physics". In the beginning the dissemination of physics knowledge took place only through quarterly scientific meetings. In 1913, the APS took over the operation of Physical Review, already in existence since 1893. Hence, journal publication also became an important goal, second only to its original mission. Physical Review was followed by Reviews of Modern Physics in 1929, and by Physical Review Letters in 1958. Volume 1, Issue 1 was published on July 1, 1958 (See archives link). As the years passed the fields of physics have multiplied, and the number of submissions has grown. Consequently, Physical Review has been subdivided into five separate sections, which are distinct from Physical Review Letters.



Physical Review Letters is rated an impact factor of 7.728 for 2013, and it is indexed in the following bibliographic databases:


