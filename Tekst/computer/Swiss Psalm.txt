The Swiss Psalm (German: Schweizerpsalm [ va ts r psalm], French: Cantique suisse [k tik s is], Italian: Salmo svizzero [sal mo zvi tsero], Romansh: Psalm Svizzer [ psalm  vi tser]) is the national anthem of Switzerland.
It was composed in 1841, by Alberich Zwyssig (1808 1854). Since then, it has been frequently sung at patriotic events. The Federal Council declined however on numerous occasions to accept the psalm as the official anthem. This was because the council wanted the people to express their say on what they wanted as a national anthem. From 1961 to 1981 it provisionally replaced Rufst du, mein Vaterland ("When You Call, My Country", French   monts ind pendants; Italian Ci chiami o patria, Romansh E clomas, tger paeis), the anthem by Johann Rudolf Wyss (1743 1818) which was set to the melody of God Save the Queen. On 1 April 1981, the Swiss Psalm was declared the official Swiss national anthem.
In 2014-2015, the Soci t  suisse d utilit  publique is organising a public competition and unofficial vote to change the national anthem.






Until the end of the 19th century, there was no Swiss national anthem. The German-language patriotic song Rufst du, mein Vaterland (French   monts ind pendants, Italian Ci chiami o patria, Romansh E clomas, tger paeis), composed in 1811 by Johann Rudolf Wyss (1743 1818), was the first national anthem, used until 1961. The setting of the hymn to the British tune of God Save the Queen led to confusing situations when both countries' anthems were played. Therefore it was replaced with another tune in 1961.



The Swiss Psalm was composed in 1841 by Alberich Zwyssig (1808 1854), with lyrics by Leonhard Widmer (1809 1867). Since then it had been frequently suggested it be adopted as the official anthem, but the Swiss Federal Government had refused several times, wishing to let the people decide what they want to sing on political and military occasions.
The Swiss Psalm temporarily became the national anthem in 1961. After a trial period of three years the Swiss tune was adopted indefinitely in 1965. The statute could not be challenged until ten years later but did not totally exclude the possibility of an ultimate change. A competition was set up in 1979 to search for a successor to the anthem. Despite many submissions, none of the others seemed to express the Swiss sentiment. The Swiss anthem finally got its definitive statutory status in April 1981, the Federal Council maintaining that it was purely a Swiss song suitably dignified and solemn. The popularity of the song has not been established. At least, it has been shown with several vox pops taken that many people do not know it at all, and only a small percentage can recite it all.



Proposed replacements for the psalm include:
In 1986, Roulez tambours (Roll the drums) by Romand Henri-Fr d ric Amiel was proposed by the Swiss National Alliance.
At the end of the 1990s, The Fondation Pro CH 98 equally tried to promote a new anthem composed by the Argovian Christian Daniel Jakob.



In 2014, the Soci t  suisse d utilit  publique started a public competition for a new national anthem. The instruction was to take inspiration from the preamble of the Federal Constitution of Switzerland. The jury received 208 proposals; it selected six of them and translated them in the four national languages of Switzerland. In March 2015, the six selected proposals were released on-line (with videos in four languages) and opened to public vote (until May 2015). The best three will be selected for a second on-line ballot between June and August. In September, a televised final will select one song. Finally, the Soci t  suisse d utilit  publique will propose the winning anthem to the federal authorities.



Owing to the fact that Switzerland has four national languages, the lyrics of the original German song were translated into the other three national languages: French, Italian and Romansh.









Landeshymne in German, French and Italian in the online Historical Dictionary of Switzerland.
Official page with text and sound in the four national languages