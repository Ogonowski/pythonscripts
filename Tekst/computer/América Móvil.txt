Am rica M vil is a Mexican telecommunications corporation headquartered in Mexico City, Mexico. It is the fourth largest mobile network operator in terms of equity subscribers and one of the largest corporations in the world. Am rica M vil is a Forbes Global 2000 company. A venture of Carlos Slim, Am rica M vil provides services to 289.4 million wireless subscribers, 34.3 million landlines, 22.6 million broadband accesses and 21.5 million PayTV units as of the end of 2014.



The company's world headquarters are located in Mexico City, Mexico. Its Mexican subsidiary Telcel is the largest mobile operator in that country, commanding a market share in excess of 70%. The company operates under its Claro subsidiaries in many countries in Latin America and the Caribbean, these include Jamaica, the Dominican Republic, El Salvador, Guatemala, Honduras, Nicaragua, Peru, Argentina, Uruguay, Chile, Paraguay, Puerto Rico, Colombia and Ecuador. In Brazil it also operates Claro and other subsidiary Embratel. It owns 30% of KPN Telecom in the Netherlands and has done a bid on 100% of the shares. The group has also fully consolidated the Telekom Austria Group into its financial reporting, owning 59.7% of its shares and using the Austrian operator to expand Am rica M vil's European network.
Am rica M vil acquired 100% of Jamaican mobile operator Oceanic Digital, under the brand name MiPhone in August 2007. On November 15, 2005, the company signed an international pact with Bridge Alliance to jointly deliver various international services.
In the United States, it operates through its subsidiary TracFone Wireless, Inc. under the brands TracFone, NET10 Wireless, Straight Talk, SIMPLE Mobile, Telcel Am rica, Page Plus Cellular and Total Wireless. It is one of the leading national pre-paid wireless service provider in the U.S.
As of December 2010, the company was one of the top four telecommunications companies in the world and boasted 290,000 kilometres of fiber optic cable, making it the largest in infrastructure.
With annual sales of over $47 billion (As of April 2012), the company is currently the largest company in Mexico by revenue, more than the next five largest companies combined.
With annual profits of over $5 billion (As of April 2012), the company is currently the most profitable company in Mexico, more than the next three most profitable companies combined.
With assets of over $67 billion (As of April 2012), the company is currently the largest company in Mexico by assets with Banorte very closely behind them with assets of over $59 billion (As of April 2012) It is highly likely that the company will buy a group of companies with at least $29 billion in assets in 2013 in the pension, insurance, payroll, currency exchange and mutual funds industries to secure their position as the most asset rich company in Mexico.
With a market value of over $93 billion (As of April 2012), the company is currently the most valuable company in Mexico, more than the next three most valuable companies combined.



Lack of competition in telecommunications in Mexico is estimated to cost the economy of Mexico $25 billion a year, although this finding has been disputed by economists and academics such as Hausman & Ros, where the authors conclude the opposite: there was in fact a consumer surplus of $4   $5 billion USD with respect to comparable countries.



In January 2010, it made an offer to buy Carso Telecom and Telmex International in order to better compete against Spain's Telefonica and Malaysia's Telekom Malaysia. The acquisition was approved by the CFC (Comisi n Federal de Competencia) Antitrust Office in Mexico on February 11, 2010.
Am rica M vil had once been Telmex' mobile division, but since its spinoff in 2001 it had grown far larger than its former parent.



As of Dec 2014:
North America
 United States - TracFone Wireless (TracFone, NET10 Wireless, Straight Talk, SafeLink Wireless, SIMPLE Mobile, Total Wireless and Telcel Am rica) 26.006 million
 Mexico - Telcel 71.463 million
Central America and The Caribbean
 Costa Rica  Dominican Republic  El Salvador  Guatemala  Honduras  Nicaragua  Panama  Puerto Rico - Claro 19.065 million
South America
 Colombia - Claro 29.776 million
 Ecuador - Claro 11.772 million
 Brazil - Claro 71.107 million
 Chile - Claro 5.754 million
 Per  - Claro 12.498 million
 Argentina  Paraguay  Uruguay - Claro 22.000 million
Europe
 Austria - A1  Bulgaria - MTel  Belarus - Velcom  Croatia - Vipnet  Slovenia - Si.mobil  Serbia - Vip mobile  Macedonia - Vip operator  Liechtenstein - Telecom Liechtenstein 20.008 million
Global wireless customers 289.449 million









 CDMA (800/1900MHZ), GSM/GPRS/EDGE (850/1900MHZ), UMTS/HSDPA (850/1900MHZ) first UMTS live by Am rica M vil
 TDMA (800MHZ), GSM/GPRS/EDGE (900/1800MHZ), UMTS/HSDPA (850/2100MHZ), LTE (2600MHZ)
 TDMA (800MHZ), GSM/GPRS/EDGE (850/1900MHZ), UMTS/HSDPA (850MHZ soon 1900)
 TDMA (800MHZ), GSM/GPRS/EDGE (850MHZ soon 1900), UMTS/HSDPA (850MHZ soon 1900)
 GSM/GPRS/EDGE (850/1900MHZ), UMTS/HSDPA (850MHZ soon 1900)
 CDMA (1900MHZ), GSM/GPRS/EDGE (850/1900MHZ), UMTS/HSDPA (850MHZ soon 1900)
 GSM/GPRS/EDGE (1900MHZ), UMTS/HSDPA (1900MHZ)
 GSM/GPRS/EDGE (1900MHZ), UMTS/HSDPA (1900MHZ)



 CDMA (800/1900MHZ), GSM/GPRS/EDGE (850/1900MHZ), UMTS/HSDPA (850MHZ soon 1900)
 CDMA (800/1900MHZ), GSM/GPRS/EDGE (850/1900MHZ), UMTS/HSDPA (850MHZ soon 1900)
 CDMA (800MHZ), GSM/GPRS/EDGE (850MHZ), UMTS/HSDPA (850MHZ)



 CDMA (1900MHZ), GSM/GPRS/EDGE (900/1900MHZ), UMTS/HSPA (1900MHZ) first HSPA (High-Speed Packet Access) live by Am rica M vil
 GSM/GPRS/EDGE (1900MHZ), UMTS/HSDPA (1900MHZ)
 GSM/GPRS/EDGE (1900MHZ), UMTS/HSDPA (1900MHZ)
 GSM/GPRS/EDGE (1900MHZ), UMTS/HSDPA (850MHZ)
 GSM/GPRS/EDGE (1900MHZ), UMTS/HSDPA (850MHZ)



 CDMA (800/1900MHZ), GSM/GPRS/EDGE (850/1900MHZ) (MVNO)
 TDMA (800MHZ), GSM/GPRS/EDGE (850/1900MHZ), UMTS/HSDPA (850/1900MHZ), LTE (1700MHZ)



 GSM/GPRS/EDGE (900/1800MHZ), UMTS/HSDPA (900/2100MHZ), LTE (800/1800/2600MHZ)
 GSM/GPRS/EDGE (900/1800MHZ), UMTS/HSDPA (2100MHZ)
 GSM/GPRS/EDGE (900/1800MHZ), UMTS/HSDPA (2100MHZ)
 GSM/GPRS/EDGE (900MHZ), UMTS/HSDPA (2100MHZ), LTE (800/1800MHZ)
 GSM/GPRS/EDGE (900/1800MHZ), UMTS/HSDPA (900/2100MHZ), LTE (800/1800/2600MHZ)
 GSM/GPRS/EDGE (900/1800MHZ), UMTS/HSDPA (2100MHZ), LTE (1800MHZ)
 GSM/GPRS/EDGE (900/1800MHZ), UMTS/HSDPA (2100MHZ), LTE (800/1800/2600MHZ)
 GSM/GPRS/EDGE (900/1800MHZ), UMTS/HSDPA (2100MHZ), LTE (800MHZ)



In early August 2013, Am rica M vil offered to take over the remaining 70% stake of the Dutch telecommunications company KPN for 7.2 billion Euros ($9.49 billion). Am rica M vil currently owns close to 30% of KPN. The Dutch government has warned against this acquisition quoting it as a threat to national security. The Dutch government s intervention comes after the council representing employees of KPN urged authorities to halt Am rica M vil s planned bid.



Key Highlights
Highlights
THE AMERICA MOVIL GROUP
Worldwide operations
Company ownership (Q1 2015);
World s largest pay-TV operators, by group (Q3 2014)
Am rica M vil s global pay-TV operations
AMX s PAY-TV OPERATIONS IN LATIN AMERICA / BUSINESS STRATEGY
Revenue growth and ARPU (USD; 2009-Q3 2014)
AMX pay-TV revenue by country (%; Jan-Sep 2014)
Triple-play service offering
AMX vs. DirecTV in Latam (pay-TV subs; ARPU)
HD adoption rate (key markets, 2009-Q3 2014)
Content strategy (TV rights, own content, HD lineup, OTT)


