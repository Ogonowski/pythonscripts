This article lists universities in Switzerland.
The Swiss law on publicly financed universities, the Swiss University Conference and its accreditation body the CRUS-OAQ recognizes: 12 Swiss universities (10 cantonal universities and two federal institutes of technology) and a number of public Swiss Universities of Applied Sciences and other education institutions, as Higher Education institutions.



University of Basel (Basel), German-speaking
University of Bern (Bern), German-speaking
University of Fribourg (Fribourg), French- and German-speaking
University of Geneva (Geneva), French-speaking
University of Neuch tel (Neuch tel), French-speaking
University of Lausanne (Lausanne), French-speaking
University of Lucerne (Lucerne), German/English-speaking
University of Lugano (Lugano), Italian/English-speaking
University of St. Gallen (HSG) (St. Gallen), German/English-speaking
University of Zurich (Z rich), German/English-speaking
Swiss Federal Institute of Technology Lausanne (EPFL) (Lausanne), French/English-speaking
Swiss Federal Institute of Technology Zurich (ETH) (Zurich), German/English-speaking



according to the Fachhochschulgesetz (FHSG) / Loi f d rale sur les hautes  coles sp cialis es (LHES):
Berner Fachhochschule, BFH
Ecole d'ing nieurs et d'architectes de Fribourg | Hochschule f r Technik und Architektur Freiburg (HES-SO), French and German-speaking
Fachhochschule Nordwestschweiz, FHNW (German: Fachhochschule Nordwestschweiz)
Fachhochschule Ostschweiz, FHO (German: Fachhochschule Ostschweiz)
Haute  cole sp cialis e de Suisse occidentale, HES-SO, University of Applied Sciences of Western Switzerland, French-speaking
Hochschule Luzern, HSLU, German-speaking
Scuola Universitaria Professionale della Svizzera Italiana, (SUPSI), Italian-speaking
Z rcher Fachhochschule, ZFH
Kalaidos Fachhochschule
University of Human Sciences and Technology of Lugano



Conservatorio della Svizzera italiana, Lugano,
Conservatoire de Musique de Gen ve, Geneva,
Dipartimento Ambiente, Costruzioni e Design, Canobbio,
Ecole cantonale d'art du Valais, Sierre,
Haute  cole d'arts appliqu s Arc, La Chaux-de-Fonds, (part of the HES-SO),
 cole cantonale d'art de Lausanne (ECAL), Lausanne (part of the HES-SO)
Haute  cole d'arts et de design, Geneva, (part of the HES-SO),
Haute  cole de musique de Lausanne - HEMU] (part of the HES-SO),
Hochschule der K nste Berne, Bern, (part of the BFH),
Hochschule f r Gestaltung und Kunst], Basel (part of the FHNW),
Hochschule f r Musik (Basel) und Schola Cantorum Basiliensis (part of the FHNW),
Hochschule Luzern   Design & Kunst, Musik (part of the HSLU),
Scuola Teatro Dimitri, Verscio (part of the SUPSI),
Z rcher Hochschule der K nste (part of the ZFH)



according to diploma recognition by the EDK / CDIP:
The following cantonal schools are recognized: Graub nden, Bern Jura and Neuch tel, Fribourg, Nordwestschweiz (Teilschule der FHNW), Rorschach, St. Gallen, Schaffhausen, Thurgau, Vaud, Wallis, Zentralschweiz, Z rich (Teilschule der ZFH)
Alta Scuola Pedagogica Ticino (SUPSI)
Interkantonale Hochschule f r Heilp dagogik Z rich
Schweize Hochschule f r Logop die Rorschach SHLR
Eidgen ssisches Hochschulinstitut gem ss Berufsbildungsgesetz (BBG), Art. 48
Eidgen ssisches Hochschulinstitut f r Berufsbildung, EHB



according to the Berufsbildungsgesetz (BBG), Art. 48 / Loi f d rale sur la formation (LFPr), l'art. 48
Swiss Federal Institute for Vocational Education and Training, SFIVET



Facolt  di Teologia di Lugano
Theologische Hochschule Chur (THC)
Franklin University Switzerland, formerly Franklin College Switzerland (program accreditation since 2005, full university institution accreditation since 2013)
The Graduate Institute of International and Development Studies (Institut universitaire de hautes  tudes internationales et du d veloppement IHEID) in Geneva



Stiftung Fernstudien Schweiz, Brig
the Institut Universitaire Kurt B sch (Institut Universitaire Kurt B sch, IUKB) in Sion






According to the THE QS World University Rankings:
NR = no ranking or ranking outside top 200.



In relation to its population size, Switzerland is the country with the highest number of universities among the 100 best of the Academic Ranking of World Universities (2014-2015).



See Category:Business schools in Switzerland , Category: Hospitality schools in Switzerland and Category:Private schools in Switzerland



Category:Business schools in Switzerland
Category:Private schools in Switzerland
Swiss Association for Private Schools and Universities
List of largest universities by enrollment in Switzerland
Education in Switzerland
Science and technology in Switzerland
List of colleges and universities by country
List of colleges and universities
myScience.ch - a Swiss portal for research and innovation






All Swiss university programmes
Official portal of Swiss universities
The State Secretariat for Education, Research and Innovation
Results of University Rankings