The United Nations Institute for Disarmament Research (UNIDIR) was established in 1980 by the United Nations General Assembly to inform States and the global community on questions of international security, and to assist with disarmament efforts so as to facilitate progress toward greater security and economic and social development for all.
Recognizing the need for objective, empirical and comprehensive research on disarmament and security, the General Assembly specified that UNIDIR would be an autonomous entity within the United Nations structure, so that its work could be conducted in scientific independence.



Through its research projects, publications, small meetings and expert networks, UNIDIR promotes creative thinking and dialogue on the disarmament and security challenges of today and of tomorrow.
UNIDIR explores both current and future security issues, examining topics as varied as tactical nuclear weapons, refugee security, computer warfare, regional confidence-building measures, and small arms.
Working with researchers, diplomats, government officials, NGOs and other institutions, UNIDIR acts as a bridge between the research community and United Nations Member States. UNIDIR's work is funded by contributions from governments and donor foundations.
UNIDIR is based in Geneva, the primary centre for security and disarmament negotiations, home of the Conference on Disarmament, and global focal point for humanitarian concerns such as human rights, refugees, migration, health and labour issues.



UNIDIR works on the basis of the provisions of the Final Document of the First Special Session of the UN General Assembly Devoted to Disarmament and also takes into account relevant General Assembly recommendations. The work programme is reviewed annually and is subject to approval by the UN Secretary-General's Advisory Board on Disarmament Matters, which also functions as UNIDIR's Board of Trustees. The Director reports yearly to the General Assembly on the activities of the Institute.
UNIDIR's mandate is as follows:

The work of the Institute shall aim at:

(a) Providing the international community with more diversified and complete data on problems relating to international security, the armaments race and disarmament in all fields, particularly in the nuclear field, so as to facilitate progress, through negotiations, towards greater security for all States and towards the economic and social development of all peoples;

(b) Promoting informed participation by all States in disarmament efforts;

(c) Assisting ongoing negotiations on disarmament and continuing efforts to ensure greater international security at a progressively lower level of armaments, particularly nuclear armaments, by means of objective and factual studies and analyses;

(d) Carrying out more in-depth, forward-looking and long-term research on disarmament, so as to provide a general insight to the problems involved and stimulating new initiatives for new negotiations.

The Mandate is from Article II, Paragraph 2 of the Institute's Statute, which may be found here.



All inner workings and research done by UNIDIR is supervised by a Board of Trustees and the Director. The Board also serves as the Secretary-General's Advisory Board on Disarmament Matters. Each member must be knowledgeable in security, arms control and disarmament. Each member is elected by the Secretary General to serve a two-year term.



The Director of UNIDIR as of 2015 is Jarmo Sareva, former Deputy Secretary-General of the Conference on Disarmament and Director of the Conference on Disarmament Secretariat and Conference Support Branch (Geneva Branch) of the UN Office for Disarmament Affairs, who succeeded Theresa Hitchens



As of 2014 the Board of Trustees members were:
Wael Al-Assad (Jordan)
Mely Caballero Anthony (Singapore)
Choi Sung-joo (Republic of Korea)
Rut Diamint (Argentina)
Trevor Findlay (Australia)
Anita Friedt (United States)
Vicente Garrido Rebolledo (Spain)
Camille Grand (France)
Istv n Gyarmati (Hungary)
Pervez Hoodbhoy (Pakistan)
Eboe Hutchful (Ghana)
Togzhan Kassenova (Kazakhstan)
Vladimir Orlov (Russian Federation)
Fred Tanner (Switzerland)
Wu Haitao (China)



International Atomic Energy Agency
Nuclear Non-Proliferation Treaty
UN System
Insecurity Insight



UNIDIR Homepage