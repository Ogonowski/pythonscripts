HS/Link is a file transfer protocol developed by Samuel H. Smith in 1991-1992. HS/Link is a high speed, full streaming, bidirectional, batch file transfer protocol with advanced Full-Streaming-Error-Correction. Each side of the link is allowed to provide a list of files to be sent. Files will be sent in both directions until both sides of the link are satisfied.
HS/Link is also a very fast protocol for normal downloading and uploading, incorporating some new ideas (such as Full-Streaming-Error-Correction and Dynamic-Code-Substitution) to improve speed and provide greater reliability. HS/Link operates at or very near peak efficiency, often reaching 98% or more with pre-compressed files and non-buffered modems. Even higher speeds are possible with buffered or error correcting modems. A number of features, such as 32bit CRC protection, Full-Streaming-Error-Recovery and Dynamic-Code-Substitution, contribute to performance and security.
HS/Link can resume an aborted transfer, verifying all existing data blocks to insure the resumed file completely matches the file being transmitted. This function can also update a file that has only a small number of changed, added, or deleted blocks. An additional feature allowed both remote and local user to chat depending on the file transfer bandwidth(s).



Samuel H. Smith's page (which gives registration codes and source code)
HS/Link 1.12 program
HS/LINK documentation