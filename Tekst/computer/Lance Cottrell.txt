Lance Cottrell develops Internet privacy systems. Cottrell launched the commercial privacy service, Anonymizer.com, in 1995 while studying towards a PhD in Astrophysics at the University of California, San Diego. Cottrell was the original coder for the Mixmaster anonymous remailer and designed version 1 Mixmaster Protocol. As president of Anonymizer.com and in conjunction with the EFF, Cottrell established the Kosovo Privacy Project which, using Anonymizer.com services, allowed individuals to report from the 1999 Kosovo war zone without fear of retaliation.
Cottrell's company Anonymizer Inc. was acquired by Abraxas Corporation in April 2008 and Cottrell now serves as Chief Scientist for the company.
Cottrell is a noted privacy advocate and has appeared in dozens of interviews on radio, TV, and in print, as well as speaking at numerous conferences.



Ph.D. in Physics at University of California, San Diego in 2008
MS Physics at University of California, San Diego in 1995
BA Physics at University of California, Santa Cruz in 1991



Data privacy
Information privacy



Lance Cottrell bio on an Anonymizer-run site
[4]