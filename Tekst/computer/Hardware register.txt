In digital electronics, especially computing, a hardware register stores bits of information in such a way that systems can write to or read out all the bits simultaneously.



Signals from a state machine to the register control when registers transmit to or accept information from other registers.
Sometimes the state machine routes information from one register through a functional transform, such as an adder unit, and then to another register that stores the results.
Typical uses of hardware registers include:
configuration and start-up of certain features, especially during initialization
buffer storage e.g. video memory for graphics cards
input/output (I/O) of different kinds
status reporting such as whether a certain event has occurred in the hardware unit.(Note for example a modem status register (MSR) or a line status register (LSR).)
Reading a hardware register in "peripheral units"   computer hardware outside the CPU   involves accessing its memory-mapped I/O address or port-mapped I/O address with a "load" or "store" instruction, issued by the processor. Hardware registers are addressed in words, but sometimes only use a few bits of the word read in to, or written out to the register.
Commercial design tools simplify and automate memory-mapped register specification and code generation for hardware, firmware, hardware verification, testing and documentation.
Because write-only registers make debugging almost impossible, lead to the read-modify-write problem, and also make it unnecessarily difficult for the Advanced Configuration and Power Interface (ACPI) to determine the device's state when entering sleep mode in order to restore that state when exiting sleep mode, many programmers tell hardware designers to make sure that all writable registers are also readable. However, there are some cases when reading certain types of hardware registers is useless. For example, a strobe register bit that generates a one cycle pulse into specialized hardware will always read logic 0.



The hardware registers inside a central processing unit (CPU) are called processor registers.
Strobe registers have the same interface as normal hardware registers, but instead of storing data, they trigger an action each time they are written to (or, in rare cases, read from). They are a means of signaling.
Registers are normally measured by the number of bits they can hold, for example, an "8-bit register" or a "32-bit register".
Designers can implement registers in a wide variety of ways, including:
register files
standard SRAM
individual flip-flops
high-speed core memory
In addition to the "programmer-visible" registers that can be read and written with software, many chips have internal microarchitectural registers that are used for state machines and pipelining; for example, registered memory.



SPIRIT IP-XACT and DITA SIDSC XML define standard XML formats for memory-mapped registers.



Register-transfer level (RTL)
Input/output base address


