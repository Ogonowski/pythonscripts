Amongst the menu of options that agents choose from, the default option is the option the chooser will obtain if she does nothing. Experiments and observational studies show that setting a default has a significant effect on how people choose; this is called the default effect. Different causes for this effect have been discussed. Setting or changing defaults therefore has been proposed as an effective way of influencing behavior for example, with respect to deciding whether to become an organ donor, giving consent to receive e-mail marketing, or choosing the level of one s retirement contributions.



In a choice context, a default refers to that option which choosers end up with if they do not make an active choice. This notion is similar to the one in computer science where defaults are settings or values that are automatically assigned outside of user intervention. Setting the default affects how likely people end up with an option. This is called the default effect. More precisely, it refers to changes in the probability that an agent chooses a particular option when it is set as a default as opposed to the situation where this option has not been set as default.

For example, different countries have different rules on how to become an organ donor. In countries with the so-called opt-in policy, all citizens are automatically considered as non-donors unless they actively register as donors. In countries with the so-called opt-out policy, all citizens are automatically considered as donors unless they actively seek to be struck from the register. It has been argued that this difference in policy is the main cause of the significant difference in donor rates across the respective countries.



A number of different explanations have been offered for how default setting causes a change in the choice distribution. These include Cognitive Effort, Switching Costs, Loss Aversion, Recommendation and Change of Meaning.



If an agent is indifferent or conflicted between options, it may involve too much cognitive effort to base a choice on explicit evaluations. In that case, she might disregard the evaluations and choose according to the default heuristic instead, which simply states  if there is a default, do nothing about it .



If an agent faces costs when diverging from a default that surmount the possible benefits from switching to another option, then it is rational to stick with the default option. Costs of diverging from the default might involve costs for the search of information (time, consultancy fees) and/or costs for registering the choice (time, postage, lawyer fees). This amounts to a standard transaction cost explanation from rational choice theory.



If an agent evaluates options on multiple dimensions, then the default functions as a reference point from which some dimensions are interpreted as losses and therefore becomes more important for the choice. This loss aversion explanation of the default effect can be illustrated with the following example. Let A, B and C be three different pension saving plans. The agent evaluates them (in terms of time-discounted utilities) on the two following dimensions, current consumption and future savings which obviously trade off: the more one saves for the future, the less one can consume in the present.
Seen from plan A being the default, plans B and C constitute losses in the savings dimension. Seen from plan C being the default, however, plans A and B constitute losses in the consumption dimension. According to the theory of loss aversion, that dimension which is considered a loss influences the decision stronger than that which is considered a gain. Hence for either default A or C, the loss-averse agent would choose sticking with the default.



If an agent interprets the default as a signal from the policy maker, whom she sufficiently trusts, she might rationally decide to stick with this default. That the policy maker sets a default is interpreted as an implicit recommendation to choose that default option. The information taken from this recommendation might be sufficient to change some people s preferences.



Defaults also might affect the meaning of choice options and thus agents' choices over them. For example, it has been shown that under an opt-in policy in organ donation choosing not to become an organ donor is perceived as a choice of little moral failing. Under an opt-out policy, in contrast, choosing not to be an organ donor is perceived as morally more deficient. These differences in evaluation might affect the rational choice over these options.



Setting or changing defaults has been proposed as an effective way of influencing behaviour for example, with respect to deciding whether to become an organ donor, giving consent to receive e-mail marketing, choosing car insurance plans, selecting which car options to purchase, choosing between different energy providers, or choosing the level of one s retirement contributions. Setting defaults are an important example of nudges or soft paternalist policies.


