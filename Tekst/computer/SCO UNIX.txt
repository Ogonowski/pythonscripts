SCO OpenServer, previously SCO UNIX and SCO Open Desktop (SCO ODT), is a closed source version of the Unix computer operating system developed by Santa Cruz Operation (SCO), later acquired by SCO Group, and now owned by Xinuos.






SCO UNIX was the successor to SCO variant of Microsoft Xenix, derived from UNIX System V Release 3.2 with an infusion of Xenix device drivers and utilities. SCO UNIX System V/386 Release 3.2.0 was released in 1989 as the commercial successor to SCO Xenix. The base operating system did not include TCP/IP networking or X Window System graphics; these were available as optional extra-cost add-on packages. Shortly after the release of this bare OS, SCO shipped an integrated product under the name of SCO Open Desktop, or ODT. 1994 saw the release of SCO MPX, an add-on SMP package.
At the same time, AT&T completed its merge of Xenix, BSD, SunOS, and UNIX System V Release 3 features into UNIX System V Release 4. SCO UNIX remained based on System V Release 3, but eventually added home-grown versions of most of the features of Release 4.
The 1992 releases of SCO UNIX 3.2v4.0 and Open Desktop 2.0 added support for long file names and symbolic links. The next major version, OpenServer Release 5.0.0, released in 1995, added support for ELF executables and dynamically linked shared objects, and made many kernel structures dynamic.



SCO OpenServer 5, released in 1995, would become SCO's primary product and serve as the basis for products like PizzaNet (the first Internet-based food delivery system done in partnership with Pizza Hut) and SCO Global Access, an Internet gateway server based on Open Desktop Lite. Due to its large installed base, SCO OpenServer 5 continues to be actively maintained by SCO with major updates having occurred as recently as April 2009.
SCO OpenServer 6, an AT&T UNIX System V Release 4.2MP-based operating system, was initially released by The SCO Group in 2005. It includes support for large files, increased memory, and multi-threaded kernel (light-weight processes) and is referred to as SVR5. SCO OpenServer 6 contains the UnixWare 7 SVR5 kernel integrated with SCO OpenServer 5 application and binary compatibility, OpenServer 5 system administration, and OpenServer 5 user environments.
SCO OpenServer has primarily been sold into the small and medium business (SMB) market. It is widely used in small offices, point of sale (POS) systems, replicated sites, and backoffice database server deployments. Prominent SCO OpenServer customers include McDonalds, Taco Bell, Big O Tires, Pizza Hut, Costco pharmacy, NASDAQ, The Toronto Stock Exchange, Banco do Brasil, many banks in Russia and China, and the railway system of India.



SCO purchased the right to distribute the UnixWare system and its System V Release 4 code base from Novell in 1995. SCO was eventually able to re-use some code from that version of UnixWare in later releases of OpenServer. Until Release 6, this came primarily in the compilation system and the UDI driver framework and the USB subsystem written to it.
By the end of the 1990s, there were around 15,000 value-added resellers (VARs) around the world who provided solutions for customers of SCO's Unix systems.
SCO announced on August 2, 2000 that it would sell its Server Software and Services Divisions, as well as UnixWare and OpenServer technologies, to Caldera Systems, Inc. The purchase was completed in May 2001. The remaining part of the SCO company, the Tarantella Division, changed its name to Tarantella, Inc., while Caldera Systems became Caldera International, and subsequently in 2002 the SCO Group.



The SCO Group continued the development and maintenance of OpenServer. They currently continue to maintain the now obsoleted 5.0.x branch derived from 3.2v5.0.x; the most recent of these is 5.0.7.
On June 22, 2005, OpenServer 6.0 was released, codenamed "Legend", the first release in the new 6.0.x branch. SCO OpenServer 6 is based upon the System V Release 5 UNIX kernel and features multi-threading application support for C, C++, and Java applications through the POSIX interface. OpenServer 6 features kernel-level threading (not found in 5.0.x).
Some improvements over OpenServer 5 include improved SMP support (support for up to 32 processors), support for files over 1 terabyte on a partition (larger network files supported through NFSv3), better file system performance, and support for up to 64GB of memory.
OpenServer 6.0 maintains backward-compatibility for applications developed for Xenix 286 onwards.
The SCO Group went bankrupt in 2011, after a long series of legal battles.



UnXis renamed into Xinuos.









The Santa Cruz Operation and UNIX
SCO v. Novell
The SCO Group



SCO OpenServer 6.0 home page
SCO OpenServer 5.0.7 (deprecated) home page
Milestones in the History of The SCO Group: Celebrating Thirty Years of SCO
SCO OS FAQ (3.2v4.2 and 3.2v5.0.x)
Review in Linux Journal