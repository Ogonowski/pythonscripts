Bedford is a town in Middlesex County, Massachusetts, United States. It is within the Greater Boston area, 15 miles (24 km) north-west of the city of Boston. The population of Bedford was 13,320 at the 2010 census.



The following compilation comes from Ellen Abrams (1999) based on information from Abram English Brown s History of the Town of Bedford (1891), as well as other sources such as The Bedford Sampler Bicentennial Edition containing Daisy Pickman Oakley s articles, Bedford Vital Records, New England Historical and Genealogical Register, Town Directories, and other publications from the Bedford Historical Society.
The land now within the boundaries of Bedford was first settled by Europeans around 1640. In 1729 it was incorporated from a portion of Concord (about 3/5 of Bedford) and a portion of Billerica (about 2/5 of Bedford).
In 1630 came the arrival of John Winthrop and Thomas Dudley of the Massachusetts Bay Company. Aboard the Arabella from Yarmouth, England, Winthrop and Dudley sailed, and after a difficult ten-week voyage, they landed on the shores of the New World, with Salem and Boston Harbor being the Arabella's earliest destinations. In 1637, the General Court of Massachusetts granted some 2,200 acres (9 km ) of land, including Huckins Farm land to the first Governor, John Winthrop, and to Deputy Governor Thomas Dudley. The following year, the two men agreed to divide the land so that the parcel south of the two large boulders by the Concord River (Brothers Rocks) belonged to Governor Winthrop and north of the Rocks was to belong to Deputy Governor Dudley. Later, Dudley became governor. Dudley s son Rev. Samuel Dudley and Winthrop s daughter Mary were married, thus Brothers Rocks were so named because of this marriage of families.



Governor Winthrop s grandson, Fitz John Winthrop, in 1664, sold 1,200 acres (5 km ) of this land (including what is present day Huckins Farm) to Job Lane (1), a skilled artisan and house builder, in exchange for a house that Lane built for him in Connecticut. (Note: The numbers appended to the names of Lane family members indicate the generation number beginning with Job Lane (1), who immigrated from Mill End, Rickmansworth, England.) Upon his death, he passed all of this land to his son, John Lane (2), who left it to his three sons, John Lane (3), Job Lane (3), and James Lane (3). John Lane and his wife, Catherine (Whiting), lived on the site, and after she died, he married Hannah Abbott. Upon his death in 1763, their son, Samuel Lane, inherited the land we know as Huckins Farm. Some time after Samuel Lane died in 1802, the house was removed and Peter Farmer built the present farmhouse in the 1840s. We know that Peter and Dorcas Farmer had two children in the late 1820s and 1830s. Later, Banfield succeeded Farmer as the owner.
Samuel W. Huckins, born in 1817, settled on the land about 1870. Huckins was respected for his good judgment and was honored with various offices in town. Maps circa 1875 indicate that what we know as Dudley Road was called Huckins Street. Samuel Huckins lived there until his death in 1892. He had a son, Henry, who was born in 1849, and was living in Bedford in 1910.
In the late 19th century, Dudley Leavitt Pickman, descendant of an old Salem merchant family, and his wife Ellen fell in love with the land. They bought a substantial parcel (mostly Winthrop s land and a portion of Dudley s grant). Huckins Farm was a part of this purchase. A direct descendant of both Winthrop and Dudley, Pickman bought the land without knowledge of the Winthrop-Dudley grant. He discovered later that he had purchased his ancestors' lands. The land was used as a dairy farm and apple orchard, in addition to the fields, pasture land, bog garden, and ponds. Chestnut trees lined the old road between the fields. A portion of Dudley Road was named Chestnut Avenue around that time. (Today's Dudley Road and Winthrop Avenue in Bedford, as well as Pickman Drive, are named for these families.)
A large portion of the Pickman land, Huckins Farm, was sold to a developer for condominium development in 1987, and other parcels including the large Pickman house (Stearns Farm) were sold to private parties.






"By the rude bridge that arched the flood, their flag to April's breeze unfurled - here once the embattled farmers stood, and fired the shot heard 'round the world." ~ Ralph Waldo Emerson
The Bedford flag on display at the Bedford Free Public Library is the oldest known surviving intact battle flag in the United States. It is celebrated for having been the first U.S. flag flown during the American Revolutionary War, as it is believed to have been carried by Nathaniel Page's outfit of Minutemen to the Old North Bridge in Concord for the Battle of Concord on April 19, 1775.
Though the flag previously had a border of silver tassels, the tassels were cut from the flag to adorn the dress of Page's daughter.
The Latin motto on the flag, "Vince Aut Morire", means "Conquer or Die."



When Governor Winthrop and his Deputy Thomas Dudley viewed their lands in early 1638, they decided to use two great stones on the eastern bank of the Concord River to divide the property. Winthrop claimed the land to one side of one rock; Dudley claimed the land on the other side of the other rock. They named the rocks "The Two Brothers." Over the years, the two men had many differences; however they learned to work together and even considered themselves "brothers" by their children's marriage. The rocks have come to symbolize the men's spirit of cooperation and democracy. The Two Brothers Rocks can still be seen near the banks of the Concord River in the Great Meadows National Wildlife Refuge. This site has recently been restored for an Eagle Scout project by Dennis Warner in collaboration with the U.S. Fish and Wildlife service, and the Bedford Historic Preservation Commission.



The early settlers called this area along the Concord River, "Great River Meadow" because they could harvest hay along the grass banks when the water retreated each summer. Today, this 12-mile (19 km) stretch of freshwater wetlands is a sanctuary for migratory birds and wildlife. There are deer, cottontail rabbit, fox, raccoon, muskrat, beaver, weasel and over 200 species of birds seen here.



This traditional saltbox-style home at 295 North Road dates back to the early 18th century and was built by Job Lane (3), the grandson of one of Bedford's earliest settlers, Job Lane (1), a master carpenter. Job Lane (3) was a church deacon and also a town officer. His son Job Lane (4) was a Minuteman; he was wounded in the battle of Concord. The house and grounds, not far from Huckins Farm, has been restored and is open to the public from 2-4 PM on the second and fourth Sunday of the month, May through October.



Early on the morning of April 19, 1775, an alarm sounded warning the people of Bedford that British soldiers were marching from Boston to Concord. Their captain, Jonathan Willson, told them, "It is a cold breakfast boys, but we'll give them a hot dinner." The Fitch Tavern is located in Bedford center, a little over a mile from Huckins Farm.



The ruins of this old mill over Vine Brook (on Wilson and Old Burlington Road) was added to the national historical register in 2003 (see photo). A 1972 "Bedford Landmark Tour" says, "Site of the Wilson mills dating from about 1685; mills, dam, and pond passed from the Wilson family about 1770 to Oliver Bacon, then bought by Jonas Gleason (1782) and by Simeon Blodgett (1816); through the years, the site was operated as a grist mill, a saw mill, and later a cider mill."



According to the United States Census Bureau, the town has a total area of 13.9 square miles (36 km2), of which 13.7 square miles (35 km2) is land and 0.1 square miles (0.26 km2), or 0.94%, is water. Bedford is approximately 15 miles (24 km) away from the coast.
Bedford is a relatively circular town. Its neighbors, clockwise, starting from 12 o'clock, are: Billerica, Burlington, Lexington, Lincoln, Concord, and Carlisle.
In addition to the Concord River which forms part of the town's borders, the Shawsheen River flows through town. Vine Brook flows from Lexington, Massachusetts, through Burlington, Massachusetts, and into the Shawsheen in Bedford. In the 1840s, a large paper mill was built on Vine Brook, that supplied many of the jobs in town.




As of the census of 2000, there were 12,595 people, 4,621 households, and 3,419 families residing in the town. The population density was 916.7 people per square mile (353.9/km ). There were 4,708 housing units at an average density of 342.7 per square mile (132.3/km ). The racial makeup of the town was 91.19% White, 1.65% African American, 0.22% Native American, 5.40% Asian, 0.34% from other races, and 1.19% from two or more races. Hispanic or Latino of any race were 1.80% of the population.
There were 4,621 households out of which 34.1% had children under the age of 18 living with them, 64.4% were married couples living together, 7.3% had a female householder with no husband present, and 26.0% were non-families. 21.8% of all households were made up of individuals and 10.2% had someone living alone who was 65 years of age or older. The average household size was 2.60 and the average family size was 3.04.
In the town the population was spread out with 23.6% under the age of 18, 3.9% from 18 to 24, 27.8% from 25 to 44, 26.3% from 45 to 64, and 18.3% who were 65 years of age or older. The median age was 42 years. For every 100 females there were 99.3 males. For every 100 females age 18 and over, there were 96.3 males.
The median income for a household was $87,962, and the median income for a family was $101,081. Males had a median income of $65,697 versus $45,181 for females. The per capita income for the town was $39,212. About 1.4% of families and 2.5% of the population were below the poverty line, including 3.1% of those under age 18 and 4.0% of those age 65 or over.



The town uses an open town meeting as its legislature. The executive branch consists of a Board of Selectmen who oversee a town Administrator.
Bedford was the home of a Consolidated Mail Outpatient Pharmacy (CMOP). It was the part of an initiative by the Department of Veterans Affairs to provide mail order prescriptions to veterans using computerization at strategic locations throughout the United States. It has moved to the Lowell area as a result of the Veterans Administrations Cares Mission and is no longer in Bedford, Mass.
As part of the Middlesex 21st District, Bedford is represented by Ken Gordon in the Massachusetts House of Representatives.



Bedford Public Schools operate Bedford's public school system. It consists of four buildings: Lt. Eleazer Davis Elementary (K 2), Lt. Job Lane Elementary (3 5), John Glenn Middle School (6 8), and Bedford High School (9 12). Some students from Hanscom Air Force Base, which is partially located in Bedford, join Bedford residents at Bedford High for 9th grade and beyond. There is a METCO program, where students from Boston come to the Bedford schools, starting in kindergarten and stay with the class until graduation. Bedford is also part of the school district of Shawsheen Valley Technical High School which is in nearby Billerica.
The former Center School was deactivated in the 1970s. Center School is today the Town Center and Recreation Department. Nathaniel Page School was similarly deactivated in about 1982 and today is a condominium community. Davis, Lane, and Page elementary schools were all (k 6) at one time. John Glenn Middle School (originally called Bedford Junior High School) is named for John Glenn, formerly the Superintendent of Schools in Bedford, not for the U.S. Senator and astronaut. The Davis and Lane (and former Page) schools are named for local officers who took part in the Battle of Concord on April 19, 1775.



Joe Bellino, Heisman Trophy - 1960, Naval Academy
Doug Ardito, rock musician
Doug Coombs, professional skier
Helen Ramsay, singer
Taecyeon, member of South Korean pop group, 2PM
Wilbur Wood, pitcher for the Chicago White Sox and other MLB teams
Kaan Zoroglu, Internet Mogul and Owner of Bigfoot Marketing, and Smart Connect




Bedford is located slightly northwest of the intersection of I-95 (also known as MA-128) and MA-4/MA-225 (which actually cross in Lexington). Important roadways through town include US-3 (an expressway) and MA-62. It is serviced by the 62 and 62/76 lines of the MBTA's bus service. Bedford is also served by Hanscom Field (IATA: BED, ICAO: KBED), a civilian airport, which is located adjacent to Hanscom Air Force Base.
Additionally, the MBTA operates the Route 351 express bus service, from Alewife. The bus terminates at Oak Park Drive, Bedford Woods, and EMD Serono. This service operates only on the morning and evening weekday rush hour times. The bus connects to the Red Line at Alewife.
A snowstorm on January 10, 1977, prompted the end of passenger service on the Lexington Branch of the Boston & Maine Railroad (see additional notes under Boston and Lowell Railroad). The line was embargoed four years later. In 1991, the branch was railbanked by the Interstate Commerce Commission. It is now used for the Minuteman Bikeway. In the early 20th century, the Middlesex & Boston Street Railway line ran generally down Great Road (Routes 4 and 225), with lines from as far west as Hudson running into Lexington and beyond.
Other historic transportation systems through Bedford included the narrow-gauge Billerica and Bedford Railroad and the Middlesex Turnpike.






Boston.com profile of Bedford
1871 Atlas of Massachusetts. by Wall & Gray.Map of Massachusetts. Map of Middlesex County.
History of Middlesex County, Massachusetts, Volume 1 (A-H), Volume 2 (L-W) compiled by Samuel Adams Drake, published 1879 and 1880. 572 and 505 pages. Section on Bedford in Volume 2 page 241 by Josiah A. Stearns.
History of the Town of Bedford, Middlesex County, Massachusetts, by Abram English Brown, published 1891, 158 pages.



Town of Bedford official website
Bedford Chamber of Commerce
Bedford is Business Commercial Economic Development Resource Center
Bedford Historical Society
Friends of the Job Lane House
Bedford Depot Park
Bedford Fire Department
Bedford Public Schools
Bedford Conservation Land Stewards
Bedford Flag history on the town's site and information on seeing the flag at the Bedford Free Public Library
Great Meadows National Wildlife Refuge
Hanscom Air Force Base official web site
The Melting Pot - A fondue restaurant located in Bedford, near the Burlington border.
Descendants of Michael Bacon, 1640 Dedham, MA