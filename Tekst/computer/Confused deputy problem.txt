A confused deputy is a computer program that is innocently fooled by some other party into misusing its authority. It is a specific type of privilege escalation. In information security, the confused deputy problem is often cited as an example of why capability-based security is important, as capability systems protect against this whereas access control list-based systems do not.




In the original example of a confused deputy, there is a program that provides compilation services to other programs. Normally, the client program specifies the name of the input and output files, and the server is given the same access to those files that the client has.
The compiler service is pay-per-use, and the compiler service stores its billing information in a file (dubbed BILL) that only it has access to.
Now suppose a client calls the service and names its output file BILL. The service opens the output file. Even though the client did not have access to that file, the service does, so the open succeeds, and the server writes the compilation output to the file, overwriting it, and thus destroying the billing information.



In this example, the compilation service is the deputy because it is acting at the request of the client. It is confused because it was tricked into overwriting its billing file.
Whenever a program tries to access a file, the operating system needs to know two things: which file the program is asking for, and whether the program has permission to access the file. In the example, the file is designated by its name,  BILL . The server receives the file name from the client, but does not know whether the client had permission to write the file. When the server opens the file, the system uses the server s permission, not the client s. When the file name was passed from the client to the server, the permission did not go along with it; the permission was increased by the system silently and automatically.
It is not essential to the attack that the billing file is designated by a name represented as a string. The essential points are that:
the designator for the file does not carry the full authority needed to access the file;
the server's own permission to the file is used implicitly.



Confidence trick based scams are based on gaining the trust of a victim in order for an attacker to use them as a confused deputy. For example, in Salting, an attacker presents a victim with what appears to be a mineral-rich mine. In this case an attacker is using a victim's greed to persuade them to perform an action that the victim would not normally do.
When checking out at a grocery store, the cashier will scan the barcode of each item to determine the total cost. A thief could replace barcodes on his items with those of cheaper items. In this attack the cashier is a confused deputy that is using seemingly valid barcodes to determine the total cost.



A cross-site request forgery (CSRF) is an example of a confused deputy attack that uses the web browser to perform sensitive actions against a web application. A common form of this attack occurs when a web application uses a cookie to authenticate all requests transmitted by a browser. Using JavaScript an attacker can force a browser into transmitting authenticated HTTP requests.
The Samy computer worm used Cross-Site Scripting (XSS) to turn the browser's authenticated MySpace session into a confused deputy. Using XSS the worm forced the browser into posting an executable copy of the worm as a MySpace message which was then viewed and executed by friends of the infected user.
Clickjacking is an attack where the user acts as the confused deputy. In this attack a user thinks they are harmlessly browsing a website (an attacker-controlled website) but they are in fact tricked into performing sensitive actions on another website.
An FTP bounce attack can allow an attacker to indirectly connect to TCP ports that the attacker's machine has no access to, using a remote FTP server as the confused deputy.
Another example relates to personal firewall software. It can restrict internet access for specific applications. Some applications circumvent this by starting a browser with instructions to access a specific URL. The browser has authority to open a network connection, even though the application does not. Firewall software can attempt to address this by prompting the user in cases where one program starts another which then accesses the network. However, the user frequently does not have sufficient information to determine whether such an access is legitimate false positives are common, and there is a substantial risk that even sophisticated users will become habituated to clicking 'OK' to these prompts.
Not every program that misuses authority is a confused deputy. Sometimes misuse of authority is simply a result of a program error. The confused deputy problem occurs when the designation of an object is passed from one program to another, and the associated permission changes unintentionally, without any explicit action by either party. It is insidious because neither party did anything explicit to change the authority.



In some systems it is possible to ask the operating system to open a file using the permissions of another client. This solution has some drawbacks:
It requires explicit attention to security by the server. A naive or careless server might not take this extra step.
It becomes more difficult to identify the correct permission if the server is in turn the client of another service and wants to pass along access to the file.
It requires the client to trust the server to not abuse the borrowed permissions. Note that intersecting the server and client's permissions does not solve the problem either, because the server may then have to be given very wide permissions (all of the time, rather than those needed for a given request) in order to act for arbitrary clients.
The simplest way to solve the confused deputy problem is to bundle together the designation of an object and the permission to access that object. This is exactly what a capability is.
Using capability security in the compiler example, the client would pass to the server a capability to the output file, not the name of the file. Since it lacks a capability to the billing file, it cannot designate that file for output. In the cross-site request forgery example, a URL supplied "cross"-site would include its own authority independent of that of the client of the web browser.




Setuid executables in Unix
Ambient authority






Norman Hardy, The Confused Deputy: (or why capabilities might have been invented), ACM SIGOPS Operating Systems Review, Volume 22, Issue 4 (October 1988).
ACM published document.
Document text on Norm Hardy's website.
Document text on University of Pennsylvania's website.
Citeseer cross reference.

Capability Theory Notes from several sources (collated by Norm Hardy).
Everything2: Confused Deputy (some introductory level text).