The National Physical Laboratory (NPL) is the national measurement standards laboratory for the United Kingdom, based at Bushy Park in Teddington, London, England. It is the largest applied physics organisation in the UK.




NPL is an internationally respected centre of excellence in measurement and materials science. Since 1900, when Bushy House was selected as the site of NPL, it has developed and maintained the primary national measurement standards. Today it provides the scientific resources for the National Measurement System financed by the Department for Business, Innovation and Skills. NPL also offers a range of commercial services, applying scientific skills to industrial measurement problems, and manages the MSF time signal. The NPL cooperates with professional networks such as those of the IET to support scientists and engineers concerned with areas of work in which it has expertise.
NPL is mainly based on the Teddington site but also has a site in Huddersfield for dimensional metrology and an underwater acoustics facility at Wraysbury Reservoir.
Teddington was also home to the UK National Chemical Laboratory but this was closed in 1965 and some of its work was transferred to NPL.



The laboratory was initially run by the UK government, with members of staff being part of the civil service. Administration of the NPL was contracted out in 1995 under a GOCO model, with Serco winning the bid and all staff transferred to their employ. Under this regime, overhead costs halved, third party revenues grew by 16% per annum, and the number of peer-reviewed research papers published doubled. It was decided in 2012 to change the operating model for NPL from 2014 onward to include academic partners and to establish a postgraduate teaching institute on site. The date of the changeover was later postponed for up to a year. The candidates for lead academic partner were the Universities of Edinburgh, Southampton, Strathclyde and Surrey with an alliance of the Universities of Strathclyde and Surrey chosen as preferred partners. The laboratory transferred back to Department for Business, Innovation and Skills ownership on 1 January 2015.



NPL was initially sited entirely within Bushy House but grew to fill a large selection of buildings on the Teddington site. Many of these buildings were demolished and the work moved to a large state-of-the-art laboratory for NPL that was built between 1998 and 2007. The construction and operation of this new building were initially being run under a PFI scheme, before transferring back to the DTI in 2004 after the private sector companies involved made losses of over  100m.
In January 2013 funding for a new  25m Advanced Metrology Laboratory was announced that will be built on the footprint of an existing unused building.
NPL buildings



Notable researchers at NPL

Researchers who have worked at NPL include Donald Davies, who was one of two independent inventors of packet switching in the early 1960s; D. W. Dye who did important work in developing the technology of quartz clocks; Louis Essen, who invented a more accurate atomic clock than those first built in America. Others who have spent time at NPL include Harry Huskey, a computer pioneer; Alan Turing, one of the fathers of modern digital computing who was largely responsible for the early ACE computer design; Robert Watson-Watt, generally considered the inventor of radar, Oswald Kubaschewski, the father of computational materials thermodynamics and the numerical analyst James Wilkinson. H.J. Gough one of the pioneers of research into metal fatigue worked at NPL for 19 years from 1914-38. The inventor Sir Barnes Wallis did early development work there on the "Bouncing Bomb" used in the "Dam Busters" wartime raids. Sydney Goldstein and Sir James Lighthill worked in NPL's aerodynamics division during WW2 researching boundary layer theory and supersonic aerodynamics respectively. Dr Clifford Hodge also worked there and was engaged in research on semiconductors



Directors of NPL

Sir Richard Tetley Glazebrook, 1900 1919
Sir Joseph Ernest Petavel, 1919 1936
Sir Frank Edward Smith, 1936-1937 (acting)
Sir William Lawrence Bragg, 1937 1938
Sir Charles Galton Darwin, 1938 1949
Sir Edward Victor Appleton, 1941 (acting)
Sir Edward Crisp Bullard, 1948 1955
Dr Reginald Leslie Smith-Rose, 1955-1956 (acting)
Sir Gordon Brims Black McIvor Sutherland, 1956 1964
Dr John Vernon Dunworth, 1964 1977
Dr Paul Dean, 1977 1990
Dr Peter Clapham, 1990 1995
Managing Directors
Dr John Rae, 1995 2000
Dr Bob McGuiness, 2000 2005
Steve McQuillan, 2005 2008
Dr Martyn Sen , 2008-2009, 2015 (acting)
Dr Brian Bowsher, 2009 2015
Chief Executive Officers
Dr Peter Thompson, 2015 present



NPL network






NPL Website
NPL Video Podcast
Second Health in Second Life
NMS Home Page
NPL YouTube Channel
NPL Sports and Social Club
Ethos Journal profile of the National Physical Laboratory