The Czech Republic has one of the most developed industrialized economic system of the countries in Central and Eastern Europe. The country has quickly become the most stable and prosperous of the post-Communist states of Europe.
In 2014, the Czech GDP per capita at purchasing power parity was $29,925., and $19,563 at nominal value.
The principal industries are heavy and general machine-building, iron and steel production, metalworking, chemical production, electronics, transportation equipment, textiles, glass, brewing, china, ceramics, and pharmaceuticals. Its main agricultural products are sugar beets, fodder roots, potatoes, wheat, and hops.




The country's strong industrial tradition dates back to the 19th century, when Bohemia and Moravia were the economic heartland of the Austro-Hungarian Empire. The Czech lands produced a majority (about 70%) of all industrial goods in the Austro-Hungarian Empire, some of which were almost monopolistic. The Czechoslovak crown was introduced in April 1919. Introduced at a 1:1 ratio to the Austro-Hungarian currency, it became one of the most stable currencies in Europe. The First Republic became one of the 10 most developed countries of the world (behind the U.S., Canada, Australia, Switzerland, Argentina, Britain, France, Sweden and Belgium).
The consequences of the Munich Agreement were disastrous for the economy. After the occupation and forced subordination of the economy to German economic interests, the crown was officially pegged to the mark at a ratio of 1:10, even though the unofficial exchange rate was 1 to 6-7 and Germans immediately started buying Czech goods in large quantities.
In accordance with Stalin's development policy of planned interdependence, all the economies of the socialist countries were tightly linked to that of the Soviet Union. Czechoslovakia was the most prosperous country in the Eastern Bloc, however it continued to lag further behind the rest of the developed world. With the disintegration of the communist economic alliance in 1991, Czech manufacturers lost their traditional markets among former communist countries in the east.
Today, this heritage is both an asset and a liability. The Czech Republic has a well-educated population and a well-developed infrastructure.



The "Velvet Revolution" in 1989, offered a chance for profound and sustained political and economic reform. Signs of economic resurgence began to appear in the wake of the shock therapy that the International Monetary Fund (IMF) labelled the "big bang" of January 1991. Since then, consistent liberalization and astute economic management has led to the removal of 95% of all price controls, low unemployment, a positive balance of payments position, a stable exchange rate, a shift of exports from former communist economic bloc markets to Western Europe, and relatively low foreign debt. Inflation has been higher than in some other countries   mostly in the 10% range   and the government has run consistent modest budget deficits.
Two government priorities have been strict fiscal policies and creating a good climate for incoming investment in the republic. Following a series of currency devaluations, the crown has remained stable in relation to the US$. The Czech crown became fully convertible for most business purposes in late 1995.
In order to stimulate the economy and attract foreign partners, the government has revamped the legal and administrative structure governing investment. With the breakup of the Soviet Union, the country, till that point highly dependent on exports to the USSR, had to make a radical shift in economic outlook: away from the East, and towards the West. This necessitated the restructuring of existing banking and telecommunications facilities, as well as adjusting commercial laws and practices to fit Western standards. Further minimizing reliance on a single major partner, successive Czech governments have welcomed U.S. investment (amongst others) as a counterbalance to the strong economic influence of Western European partners, especially of their powerful neighbour, Germany. Although foreign direct investment (FDI) runs in uneven cycles, with a 12.9% share of total FDI between 1990 and March 1998, the U.S. was the third-largest foreign investor in the Czech economy, behind Germany and the Netherlands.
Progress toward creating a stable investment climate was recognized when the Czech Republic became the first post-communist country to receive an investment-grade credit rating by international credit institutions.
The republic boasts a flourishing consumer production sector and has privatized most state-owned heavy industries through the voucher privatization system. Under the system, every citizen was given the opportunity to buy, for a moderate price, a book of vouchers that represents potential shares in any state-owned company. The voucher holders could then invest their vouchers, increasing the capital base of the chosen company, and creating a nation of citizen share-holders. This is in contrast to Russian privatization, which consisted of sales of communal assets to private companies rather than share-transfer to citizens. The effect of this policy has been dramatic. Under communism, state ownership of businesses was estimated to be 97%. Privatization through restitution of real estate to the former owners was largely completed in 1992. By 1998, more than 80% of enterprises were in private hands. Now completed, the program has made Czechs, who own shares of each of the Czech companies, one of the highest per-capita share owners in the world.



The republic's economic transformation was far from complete. Political and financial crises in 1997, shattered the Czech Republic's image as one of the most stable and prosperous of post-Communist states. Delays in enterprise restructuring and failure to develop a well-functioning capital market played major roles in Czech economic troubles, which culminated in a currency crisis in May. The formerly pegged currency was forced into a floating system as investors sold their Korunas faster than the government could buy them. This followed a worldwide trend to divest from developing countries that year. Investors also worried the republic's economic transformation was far from complete. Another complicating factor was the current account deficit, which reached nearly 8% of GDP.
In response to the crisis, two austerity packages were introduced later in the spring (called vernacularly "The Packages"), which cut government spending by 2.5% of GDP. Growth dropped to 0.3% in 1997,  2.3% in 1998, and  0.5% in 1999. The government established a restructuring agency in 1999 and launched a revitalization program   to spur the sale of firms to foreign companies. Key priorities included accelerating legislative convergence with EU norms, restructuring enterprises, and privatising banks and utilities. The economy, fueled by increased export growth and investment, was expected to recover by 2000.



Growth in 2000 05 was supported by exports to the EU, primarily to Germany, and a strong recovery of foreign and domestic investment. Domestic demand is playing an ever more important role in underpinning growth as interest rates drop and the availability of credit cards and mortgages increases. Current account deficits of around 5% of GDP are beginning to decline as demand for Czech products in the European Union increases. Inflation is under control. Recent accession to the EU gives further impetus and direction to structural reform. In early 2004 the government passed increases in the Value Added Tax (VAT) and tightened eligibility for social benefits with the intention to bring the public finance gap down to 4% of GDP by 2006, but more difficult pension and healthcare reforms will have to wait until after the next elections. Privatization of the state-owned telecommunications firm  esk  Telecom took place in 2005. Intensified restructuring among large enterprises, improvements in the financial sector, and effective use of available EU funds should strengthen output growth.




Growth continued in the first years of the EU membership. The credit portion of the Financial crisis of 2007 2010 did not affect the Czech Republic much, mostly due to its stable banking sector which has learned its lessons during a smaller crisis in the late 1990s and became much more cautious. As a fraction of the GDP, the Czech public debt is among the smallest ones in Central and Eastern Europe. Moreover, unlike many other post-communist countries, an overwhelming majority of the household debt   over 99%   is denominated in the local Czech currency. That's why the country wasn't affected by the shrunken money supply in the U.S. dollars.
However, as a large exporter, the economy was sensitive to the decrease of the demand in Germany and other trading partners. In the middle of 2009, the annual drop of the GDP for 2009 was estimated around 3% or 4.3%, a relatively modest decrease. The impact of the economic crisis may have been limited by the existence of the national currency that temporarily weakened in H1 of 2009, simplifying the life of the exporters.




From the financial crisis of 2007 2010, Czech Republic is in stagnation or decreasing of GDP. Some commenters and economists criticising fiscally conservative policy of Petr Ne as' right-wing government, especially criticising ex-minister of finance, Miroslav Kalousek. Miroslav Kalousek in a 2008 interview, as minister of finance in the center-right government of Mirek Topol nek, said "Czech Republic will not suffer by financial crisis". In September 2008, Miroslav Kalousek formed state budget with projection of 5% GDP increase in 2009. In 2009 and 2010, Czech Republic suffered strong economical crisis and GDP decreased by 4,5%. From 2009 to 2012, Czech Republic suffered highest state budget deficits in history of independent Czech Republic. From 2008 to 2012, the public debt of Czech Republic increased by 18,9%. Most decrease of industrial output was in construction industry (-25% in 2009, -15,5% in 2013). From 4Q 2009 to 1Q 2013, GDP decreased by 7,8%.
In 2012, Czech government increased VAT. Basic VAT was increased from 20% in 2012 to 21% in 2013 and reduced VAT increased from 14% to 15% in 2013. Small enterprises sales decreased by 21% from 2012 to 2013 as result of increasing VAT. Patria.cz predicting sales stagnation and mild increase in 2013. Another problem is foreign trade. The Czech Republic is considered an export economy (the Czech Republic has strong machinery and automobile industries), however in 2013, foreign trade rapidly decreased which led to many other problems and increase of state budget deficit.
In 2013, Czech National Bank, central bank, implemented controversial monetary step. To increase export and employment, CNB wilfully deflated Czech Crown (CZK), which inflation increased from 0.2% in November 2013, to 1.3% in 1Q 2014.
In 2014, GDP in the Czech Republic increased by 2% and is predicted to increase by 2.7% in 2015.
In 2015, Czech Republic's economy grew by 4,2% and it's the fastest growing economy in the European Union. On 29 May 2015, it was announced that growth of the Czech economy has increased from calculated 3,9% to 4,2%.



On August 2015, Czech GDP growth was 4,4%, making the Czech economy the highest growing in Europe.



The Czech Republic is reducing its dependence on highly polluting low-grade brown coal as a source of energy. Nuclear power currently provides about 30% of total power needs, and its share is projected to increase to 40%. Natural gas is procured from Russian Gazprom (roughly three-fourths of domestic consumption) and from Norwegian companies (most of the remaining one-fourth). Russian gas is imported via Ukraine (Friendship pipeline), Norwegian gas is transported through Germany. The gas consumption (approx. 100 TWh in 2003-5) is almost two times higher than the electricity consumption. South Moravia has a small amount of oil and gas deposits.



From the CIA World Factbook 2015




GDP (pp.): $299.7 billion (2014)
GDP Growth: 2.5% (2014)
GDP per capita (pp.): $28,400 (2014)
GDP per capita (nom.): $19,563 (2014)
GDP by sector: Agriculture: 2.9% Industry: 38.7% Services: 60% (2014)
Inflation: 0.5% (2014)
Labour Force: 5.416 million (2014)
Unemployment: 6.7% (May 2015)
Industrial production growth rate: 4% (2014)
Household income or consumption by percentage share: (2012)
lowest 10%: 1.5%
highest 10%: 29.1%
Public Debt: 43.5% GDP (2014)




Exports: $147.3 billion Export goods: machinery and transport equipment, raw materials, fuel, chemicals (2014)
Imports: $135.1 billion Import goods: machinery and transport equipment, raw materials and fuels, chemicals (2014)
Current Account balance: -$4,533,000 (2007)
Export partners: Germany 31.7%, Slovakia 9%, Poland 6%, France 5%, UK 4.9%, Austria 4.6% (2013)
Import partners: Germany 30.3%, Poland 8.1%, Slovakia 7.2%, China 5.8%, Netherlands 5.5%, Russia 5.1%, Austria 4.1% (2013)
Reserves: $59.15 billion (31 December 2014)
Foreign Direct Investment: $86.75 billion (2007)
Czech Investment Abroad: $6.058 billion (2007)
External debt: $116.1 billion (31 December 2014)
Value of Publicly Traded Shares: $48.6 billion (2006)
Exchange rates:
koruny (K ) per US$1   24.67 (May 2015), 18.75 (December 2010), 18.277 (2007), 23.957 (2005), 25.7 (2004), 28.2 (2003), 32.7 (2002), 38.0 (2001), 38.6 (2001), 34.6 (1999), 32.3 (1998), 31.7 (1997), 27.1 (1996), 26.5 (1995)
koruny (K ) per EUR 1   27.33 (May 2015), 25.06 (December 2010)



Electricity production: 81.71 billion kWh (2012) Electricity   production by source:
fossil fuel: 75.54%
hydro: 2.55%
nuclear: 20.37%
other: 1.54% (1998)
Electricity   consumption: 70.45 billion kWh (2012)
Electricity   exports: 27.46 billion kWh (2013)
Electricity   imports: 10.57 billion kWh (2013)
Oil   production: 18,030 bbl/d (2,867 m3/d) (2005)
Oil   consumption: 213,000 bbl/d (33,900 m3/d) (2005 est.)
Oil   exports: 20,930 bbl/d (3,328 m3/d) (2004)
Oil   imports: 203,700 bbl/d (32,390 m3/d) (2004)
Oil   proved reserves: 15,000,000 bbl (2,400,000 m3) (1 January 2006)
Natural gas   production: 252 million m  (2013 est.)
Natural gas   consumption: 8.477 billion m  (2013 est.)
Natural gas   exports: 8 million m  (2013 est.)
Natural gas   imports: 8.479 billion m  (2013 est.)
Natural gas   proved reserves: 4.276 billion m  (1 January 2014 est.)
Natural resources: coal, timber, lignite, uranium, magnesite.
Agriculture   products: wheat, rye, oats, corn, barley, potatoes, sugar beets, hops, fruit; pigs, cattle, poultry, horses; forest products



Households with access to fixed and mobile telephone access
landline telephone   25% (2009)
according to the Czech Statistical Office: 55,2% (2005); 31,1% (2008); 27,6% (2009); 24,2% (2010); 23,4% (2011); 21,8% (2012)

mobile telephone   94% (2009)
according to the Czech Statistical Office: 81,2% (2005); 92,4% (2008); 94,6% (2009); 95,6% (2010); 96,2% (2011); 97,0% (2012)

Individuals with mobile telephone access
according to the Czech Statistical Office: 75,8% (2005); 90,6% (2009); 93,9% (2011); 96,0% (2012); 96,0% (2013)
Broadband penetration rate
fixed broadband   19.1% (2010)
mobile broadband   3.5% (2010)
Individuals using computer and internet
computer   67% (2009)
according to the Czech Statistical Office: 42,0% (2005); 59,2% (2009); 64,1% (2010); 67,1% (2011); 69,5% (2012); 70,2% (2013)

internet   64% (2009)
according to the Czech Statistical Office: 32,1% (2005); 55,9% (2009); 61,8% (2010); 65,5% (2011); 69,5% (2012); 70,4% (2013)


