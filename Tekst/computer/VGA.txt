Video Graphics Array (VGA) refers specifically to the display hardware first introduced with the IBM PS/2 line of computers in 1987, but through its widespread adoption has also come to mean either an Amplitude Modulated computer display standard, the 15-pin D-subminiature VGA connector or the 640x480 resolution itself.
VGA was the last IBM graphics standard to which the majority of PC clone manufacturers conformed, making it the lowest common denominator that virtually all post-1990 PC graphics hardware can be expected to implement. It was officially followed by IBM's Extended Graphics Array (XGA) standard, but was effectively superseded by numerous slightly different extensions to VGA made by clone manufacturers, collectively known as Super VGA.
Today, the VGA analog interface is used for high definition video, including resolutions of 1080p and higher. While the transmission bandwidth of VGA is high enough to support even higher resolution playback, there can be picture quality degradation depending on cable quality and length. How discernible this degradation is depends on the individual's eyesight and the display, though it is more noticeable when switching to and from digital inputs like HDMI or DVI.




The VGA supports both All Points Addressable graphics modes, and alphanumeric text modes.



Standard graphics modes are:
640x480 in 16 colors or monochrome  (the latter matching IBM's lesser Multi-Color Graphics Array standard)
640x350 or 640x200 in 16 colors or monochrome (EGA compatibility mode)
320x200 in 4 or 16 colors
320x200 in 256 colors (Mode 13h)
The 640x480 16-color and 320x200 256-color modes had fully redefinable palettes, with each entry selectable from within an 18-bit (262,144-color) RGB table, although the high resolution mode is most commonly familiar from its use with a fixed palette under Microsoft Windows. The other color modes defaulted to standard EGA or CGA compatible palettes (including the ability for programs to redefine the 16-color EGA palette from a master 64-color table), but could still be redefined if desired using VGA-specific commands.



Higher-resolution and other display modes are also achievable, even with standard cards and most standard monitors - on the whole, a typical VGA system can produce displays with any combination of:
512 to 800 pixels wide, in 16 colors (including 640, 704, 720, 736, 768...), or
256 to 400 pixels wide, in 256 colors (including 320, 360, 384...)
with
200, or 350 to 410 lines (including 400-line) at 70 Hz refresh rate, or
224 to 256, or 448 to 512 lines (including 240 or 480-line) at 60 Hz refresh rate
512 to 600 lines at reduced vertical refresh rates (down to 50 Hz, and including e.g. 528, 544, 552, 560, 576-line), depending on individual monitor compatibility.
(175 to 205 line modes may be possible at 70 Hz, and 256 to 300 lines in the 50 to 60 Hz refresh rate range, as well as horizontal widths below 256/512, but these are of little practical use)
For example, high resolution modes with square pixels are available at 768x576 or 704x528 in 16 colors, or medium-low resolution at 320x240 with 256 colors; alternatively, extended resolution is available with "fat" pixels and 256 colors using, e.g. 400x600 (50 Hz) or 360x480 (60 Hz), and "thin" pixels, 16 colors and the 70 Hz refresh rate with e.g. 736x410 mode.
"Narrow" modes such as 256x224 tend to preserve the same pixel ratio as in e.g. 320x240 mode unless the monitor is adjusted to stretch the image out to fill the screen, as they are derived simply by masking down the wider mode instead of altering pixel or line timings, but can be useful for reducing memory requirements and pixel addressing calculations for arcade game conversions or console emulators.



Standard text modes:
80x25 character display, rendered with a 9x16 pixel font, with an effective resolution of 720x400 in either 16 colors or monochrome, the latter being compatible with legacy MDA-based applications.
40x25, using the same font grid, for an effective resolution of 360x400
80x43 or 80x50 (8x8 font grid) in 16-colors, with an effective resolution of 640x344 (EGA-compatible) or 640x400 pixels.
As with the pixel-based graphics modes, additional text modes are technically possible (as VGA resolution settings are notionally calculated from character-grid dimensions) with an overall maximum of about 100x80 cells and an active area spanning about 88x64 cells, but these are rarely used as it usually makes much more sense to just use a graphics mode - with a small, perhaps proportional font - if a larger text display is required. One variant that is sometimes seen is 80x30 or 80x60, using an 8x16 or 8x8 font and an effective 640x480 pixel display, which trades use of the more flickery 60 Hz mode for an additional 5 or 10 lines of text and square character blocks (or, at 80x30, square half-blocks).







VGA is referred to as an "Array" instead of an "adapter" because it was implemented from the start as a single chip an application-specific integrated circuit which replaced both the Motorola 6845 video address generator as well as dozens of discrete logic chips that covered the full-length ISA boards of the MDA, CGA, and EGA. Its single-chip implementation allowed the VGA to be placed directly on a PC s motherboard with a minimum of difficulty, since it only required video memory, timing crystals and an external RAMDAC. As a result, the first IBM PS/2 models were equipped with VGA on the motherboard, in contrast to all of the "family one" IBM PC desktop models the PC, PC/XT, and PC AT which required a display adapter installed in a slot in order to connect a monitor.



The original VGA specifications are as follows:
256 kB Video RAM (The very first cards could be ordered with 64 kB or 128 kB of RAM, at the cost of losing some or all high-resolution 16-color modes.)
16-color and 256-color paletted display modes.
262,144-color global palette (6 bits, and therefore 64 possible levels, for each of the red, green, and blue channels via the RAMDAC)
Selectable 25.175 MHz or 28.322 MHz master pixel clock
Usual line rate fixed at 31.469 kHz
Maximum of 800 horizontal pixels
Maximum of 600 lines
Refresh rates at up to 70 Hz
Vertical blank interrupt (Not all clone cards support this.)
Planar mode: up to 16 colors (4-bit planes)
Packed-pixel mode: 256 colors (Mode 13h)
Hardware smooth scrolling support
No hardware sprites,
No Blitter, but supports very fast data transfers via "VGA latch" registers.
Some "Raster Ops" support
Barrel shifter
Split screen support
0.7 V peak-to-peak
75 ohm double-terminated impedance (18.7 mA, 13 mW)
As well as the standard modes, VGA can be configured to emulate many of the modes of its predecessors (EGA, CGA, and MDA), including their reduced global color palettes (with particular pre-set colors chosen from the VGA palette for text and 4- or 16-color, 200-line modes) and coarser text font grids. Compatibility is almost full at BIOS level, but even at register level, a very high value of compatibility is reached. VGA is not directly compatible with the special IBM PCjr or HGC video modes, despite having sufficient resolution, color, refresh rate and memory capabilities; any emulation of these modes has to be performed in software instead.



The intended standard value for the horizontal frequency of VGA is exactly double the value used in the NTSC-M video system, as this made it much easier to offer optional TV-out solutions or external VGA-to-TV converter boxes at the time of VGA's development. The formula for the VGA horizontal frequency is thus (60   1001)   525 kHz = 4500   143 kHz   31.4685 kHz. All other frequencies used by the VGA card are derived from this value by integer multiplication or division. Since the exactness of quartz oscillators is limited, real cards will have slightly higher or lower frequency.
All derived VGA timings (i.e. those which still use the master 25.2 and 28.3 MHz crystals and 31.5 kHz line rate) can be varied widely by software that bypasses the VGA firmware interface and communicates directly with the VGA hardware, as many MS-DOS based games did. However, only the standard modes, or modes that at least use exactly the same H-sync and near-exact V-sync timings as one of the standard modes, can be expected to work with the original late-1980s and early-1990s VGA monitors. The use of other timings may in fact damage such monitors and thus was usually avoided by software publishers. Later "multisync" CRT monitors were usually much more flexible, and in combination with later SVGA graphics cards, could display a much wider range of resolutions and refresh rates at wholly arbitrary sync frequencies and pixel clock rates (or at least, within a particular lower/upper range, depending on model, which enfolded at least the normal (S)VGA options, often XGA as well, and far more besides in more upmarket examples).
For the most common VGA mode (640x480 "60 Hz" non-interlaced), the horizontal timings are:
(Total horizontal sync and blanking time = 6.356  s; equivalent to pixel widths of A = 16, B = 96, C = 48, D = 640 and each complete line = 800)

NB. The figures shown in this image may be slightly inaccurate and not match the above table exactly. The same general layout applies, merely at a lower frequency, for the vertical timings.
These timings are the same in the higher frequency mode, but all pixel counts are correspondingly multiplied by 9/8ths - thus, 720 active pixels, 900 total per line, and a 54 pixel back porch.
The vertical timings are:
(Total vertical sync and blanking time 1.43 ms; equivalent to line periods of A = 10, B = 2, C = 33, D = 480 and each complete frame = 525)
These timings are somewhat altered in "70Hz" mode, as although it uses the same line rate, its frame rate is not quite exactly 7/6ths that of "60Hz", despite 525 dividing cleanly into 7 - and, of course, 480/400 is itself a larger 6:5 ratio. Instead, it compromises on a 449-line frame (instead of the expected 450), with the back porch extended to 34 lines, and the front porch to 13, with an unaltered 2-line sync pulse - and the active image taking up 89% of the total scan period rather than 91%. The monitor is triggered into synchronising at the higher frame scan rate (and, with digital displays such as LCDs, the higher horizontal pixel density) by use of a positive-polarity VSync pulse, versus the negative pulse of 60 Hz mode.
Depending on manufacturer, the exact details of active period and front/back porch widths, particularly in the horizontal domain, may vary slightly. This does not usually cause a problem as the porches are merely intended to act as blanked-video buffers offering a little overscan space between the active area and the sync pulse (which triggers, in traditional CRT monitors, the phosphor beam deflection "flyback" to the upper or left hand side of the tube) and thus can be safely overrun into by a certain amount when everything else is operating correctly. The relationship between the front and back porches can also be altered within certain limits, which makes possible special features such as software-based image alignment with certain graphics cards (centering the image within the monitor frame by adjusting the location of the active screen area between the horizontal and vertical porches, rather than relying wholly upon the adjustment range offered by the monitor's own controls which can sometimes be less than satisfactory).
This buffer zone is typically what is exploited to achieve higher active resolutions in the various custom screen modes, by deliberately reducing porch widths and using the freed-up scan time for active pixels instead. This technique can achieve an absolute maximum of 704 pixels horizontally in 25 MHz mode and 792 at 28 MHz without altering the actual sync width (in real-world cases, e.g. with 800 pixel wide mode, the sync pulse would be shortened and a small porch area left in place to prevent obvious visual artefacting), and as much as 523 or 447 lines at the standard 60 and 70 Hz refresh rates (again, it is usually necessary to leave SOME porch lines intact, hence the usual maximum of 410 or 512 lines at these rates, and the 50 Hz maximum being 600 lines rather than 626). Conveniently, the practical limits of these techniques are not quite high enough to overflow the available memory capacity of typical 256KB cards (800x600 consuming 235KB, and even the theoretical 832x624 requiring "only" 254KB), so the only concerns remain those of monitor compatibility.



640x400 @ 70 Hz is traditionally the video mode used for booting most VGA-compatible x86 personal computers which show a graphical boot screen (text-mode boot uses 720x400 @ 70 Hz).
640x480 @ 60 Hz is the default Windows graphics mode (usually with 16 colors), up to Windows 2000. It remains an option in XP and later versions via the boot menu "low resolution video" option and per-application compatibility mode settings.
320x200 @ 70 Hz is the most common mode for VGA-era PC games, using exactly the same timings as the 640x400 mode, but halving the pixel rate (and, in 256 colour mode, doubling the bit-depth of each pixel) and displaying each line of pixels twice.
The actual timings vary slightly from the defined standard. For example, for 640x480 @ 60 Hz, a 25.17  s active video time with a pixel frequency of 25.174 MHz gives 634 pixels, rather than the expected 640.




VGA uses a DE-15 connector. This connector fits on the mounting tab of an ISA expansion card.

An alternative method of connecting VGA devices that maintains very high signal quality is the BNC connector, typically used as a group of five connectors, one each for Red, Green, Blue, Horizontal Sync, and Vertical Sync. With BNC, the coaxial wires are fully shielded end-to-end and through the interconnect so that no crosstalk or external interference is possible.
However, BNC connectors are relatively large compared to the DE 15, and some attention is needed to make sure each cable goes to the correct socket. Additionally, extra signal lines such as +5 V, DDC, and DDC2 are not supported using BNC connectors.




The BIOS offers some text modes for a VGA adapter, which have 80x25, 40x25, 80x43 or 80x50 text grid. Each cell may choose from one of 16 available colors for its foreground and eight colors for the background; the eight background colors allowed are the ones without the high-intensity bit set. Each character may also be made to blink; all that are set to blink will blink in unison. The blinking option for the entire screen can be exchanged for the ability to use all 16 colors for background. All of these options are the same as those on the CGA adapter as introduced by IBM.
Like EGA, VGA supports having up to 512 different simultaneous characters on screen, albeit in only 8 foreground colors, by rededicating one color bit as the highest bit of the character number. The glyphs on 80x25 mode are normally made of 9x16 pixels. Users may define their own character set by loading a custom font onto the card. As character data is only eight bits wide on VGA, just as on all of its predecessors, there is usually a blank pixel column between any two horizontally adjacent glyphs. However, some characters are normally made nine bits wide by repeating their last column instead of inserting a blank column, especially those defining horizontally connected IBM box-drawing characters. This functionality is hard-wired to the character numbers C0hex to DFhex, where all horizontally connecting characters are found in Codepage 437 and its most common derivatives. The same column-repeating trick was already used on the older MDA hardware with its 9x14 pixel glyphs, but on VGA it can be turned off when loading a font in which those character numbers do not represent box drawing characters.



VGA adapters usually support both monochrome and color modes, though the monochrome mode is almost never used, and support for the full set of MDA text mode attributes (intense, underline) is often missing. Black and white text on nearly all modern VGA adapters is drawn by using gray colored text on a black background in color mode. VGA monochrome monitors intended primarily for text were sold, but most of them will work at least adequately with a VGA adapter in color mode. Occasionally, a faulty connection between a modern monitor and video card will cause the VGA part of the card to detect the monitor as monochrome; this will cause the BIOS and initial boot sequence to appear in greyscale. Usually, once the video card's drivers are loaded (for example, by continuing to boot into the operating system), they will override this detection and the monitor will return to color.




The video memory of the VGA is mapped to the PC's memory via a window in the range between segments 0xA0000 and 0xBFFFF in the PC's real mode address space (A000:0000 and B000:FFFF in segment:offset notation). Typically, these starting segments are:
0xA0000 for EGA/VGA graphics modes (64 KB)
0xB0000 for monochrome text mode (32 KB)
0xB8000 for color text mode and CGA-compatible graphics modes (32 KB)
Due to the use of different address mappings for different modes, it is possible to have a monochrome adapter (i.e. MDA or Hercules) and a color adapter such as the VGA, EGA, or CGA installed in the same machine. At the beginning of the 1980s, this was typically used to display Lotus 1-2-3 spreadsheets in high-resolution text on a monochrome display and associated graphics on a low-resolution CGA display simultaneously. Many programmers also used such a setup with the monochrome card displaying debugging information while a program ran in graphics mode on the other card. Several debuggers, like Borland's Turbo Debugger, D86 (by Alan J. Cox) and Microsoft's CodeView could work in a dual monitor setup. Either Turbo Debugger or CodeView could be used to debug Windows. There were also DOS device drivers such as ox.sys, which implemented a serial interface simulation on the monochrome display and, for example, allowed the user to receive crash messages from debugging versions of Windows without using an actual serial terminal. It is also possible to use the "MODE MONO" command at the DOS prompt to redirect the output to the monochrome display. When a monochrome adapter was not present, it was possible to use the 0xB000 0xB7FF address space as additional memory for other programs (for example by adding the line "DEVICE=EMM386.EXE I=B000-B7FF" into config.sys, this memory would be made available to programs that can be "loaded high", that is loaded into high memory.)




The VGA color system is backward compatible with the EGA and CGA adapters, and adds another level of indirection to support 256 18-bit-colors.
CGA was able to display 16 fixed colors, and EGA extended this by using 16 palette registers, each containing a color value from a 64-color palette. The default EGA palette values were chosen to look like the CGA colors, but it's possible to remap each color. (Note: This works in graphics and text modes.) The signals from the EGA palette entries will drive a set of six signal lines on the EGA output, with two lines corresponding to one color (off, dark, normal and bright, for red, green and blue).
VGA further extends this scheme by adding 256 color registers, 3 6 bit each for a total of 262,144 colors to chose from. These color registers are by default set to match the 64 default EGA colors. The values from these registers drive a DAC, which in turn drives three signal lines, one for red, green and blue. (Using analog signals allows a theoretically unlimited number of color values on a default VGA cable.)
Like EGA uses the CGA color value to address a palette entry, the VGA hardware will also use the palette entries not directly as signal levels but as indexes to the color registers. Therefore, in the 16-color-modes, the color value from the RAM will reference a palette register and that palette register will select a color register. E.g. the default palette entry for brown (on CGA: 4 (red) + 2(green), contains 0x14 (dark green + normal red) on the EGA palette. The corresponding VGA color register 0x14 is preset to (42,21,0, or #aa5500, also and off cause brown).
For 16-color-modes, using the 6-bit palette registers allows to use a block of up to 64 color registers, usually 0-63.
In the 256-color-modes, the palette registers are set to have no effect and the DAC is set to combine two 4-bit color values into the original 8-bit-value; thereby the 8-bit color number in the video RAM is exactly the color register index to be used. Since the colors 0 ... 15 are still supposed to result in the CGA colors, the color registers are not preset to contain the EGA palette, instead it contains the 16 CGA colors in the first entries. The other entries are 16 gray levels from black to white and 9 groups of 24 color values. The remaining 8 entries were black (see picture). 



A documented but less well-known technique nicknamed Mode X (first coined by Michael Abrash) or "tweaked VGA" was used to make programming techniques and graphics resolutions available that were not otherwise possible in the standard Mode 13h. This was done by "unchaining" the 256 KB VGA memory into four separate "planes", which would make all of VGA's 256 KB of RAM available in 256-color modes. There was a trade-off for extra complexity and performance loss in some types of graphics operations, but this was mitigated by other operations becoming faster in certain situations:
Single-color polygon filling could be accelerated due to the ability to set four pixels with a single write to the hardware.
The video adapter could assist in copying video RAM regions, which was sometimes faster than doing this with the relatively slow CPU-to-VGA interface.
The use of multiple video pages in hardware allowed the programmer to perform double buffering, triple buffering or split screens, which, while available in VGA's 320x200 16-color mode, was not possible using stock Mode 13h.
Most particularly, several higher, arbitrary-resolution display modes were possible, all the way up to the programmable limit of 800x600 with 16 colors (or 400x600 with 256 colors), as well as other custom modes using unusual combinations of horizontal and vertical pixel counts in either color mode.
Software such as Fractint, Xlib and ColoRIX also supported tweaked 256-color modes on standard adaptors using freely-combinable widths of 256, 320, and 360 pixels and heights of 200, 240 and 256 (or 400, 480 and 512) lines, extending still further to 384 or 400 pixel columns and 576 or 600 (or 288, 300) lines in rare cases, as well as other arbitrary sizes in-between. However, 320x240 was the best known and most-frequently used, as it offered a standard 40-column resolution and 4:3 aspect ratio with square pixels - so much so that "Mode X" itself became generally synonymous with "320x240x8".
The highest resolution modes were only used in special, opt-in cases rather than as standard, especially where high line counts were involved. Standard VGA monitors had a fixed line scan (H-scan) rate - "multisync" monitors being, at the time, expensive exotica - and so the vertical/frame (V-scan) refresh rate had to be reduced in order to accommodate them, which increased visible flicker and thus eye strain. For example, the highest 800x600 mode, being otherwise based off the matching SVGA resolution (with 628 total lines), reduced the refresh rate from 60 Hz to about 50 Hz (and 832x624, the theoretical maximum resolution achievable with 256kb at 16 colors, would have reduced it to about 48 Hz, barely higher than the rate at which XGA monitors employed a double-frequency interlacing technique to mitigate full-frame flicker).
These modes were also outright incompatible with some monitors, producing display problems such as picture detail disappearing into overscan (especially in the horizontal dimension), vertical roll, poor horizontal sync or even a complete lack of picture depending on the exact mode attempted. Due to these potential issues, most VGA tweaks used in commercial products were limited to more standards-compliant, "monitor-safe" combinations, such as 320x240 (square pixels, three video pages, 60 Hz), 320x400 (double resolution, two video pages, 70 Hz), and 360x480 (highest resolution compatible with both standard VGA monitors and cards, one video page, 60 Hz) in 256 colors, or double the H-rez in 16-color mode.



Several companies produced VGA compatible graphic board models.
ATI (acquired by AMD): Graphics Solution Plus, Wonder series, Mach series
S3 Graphics: S3 911, 911A, 924, 801, 805, 805i, 928, 805p, 928p, S3 Vision series, S3 Trio series
Matrox: MAGIC RGB
Plantronics: Colorplus
Paradise Systems (defunct): PEGA 1, PEGA 1a, PEGA 2a
Tseng labs: ET3000, ET4000, ET6000
Cirrus Logic: CL-GD400, CL-GD500 and CL-GD5000 series
Trident Microsystems: TVGA 8000 series, TVGA 9000 series, TGUI9000 series
IIT
NEC
Chips and Technologies
SiS
Tamerack
Weston Digital
Realtek
Oak Technology
LSI
Hualon
Cornerstone Imaging
Winbond
AMD
Western Digital
Intergraph
Texas Instruments
Gemini (defunct)
Genoa (defunct)


