The University of Georgia Press or UGA Press is a scholarly publishing house for the University System of Georgia. It is the oldest and largest publishing house in Georgia and a member of the Association of American University Presses.



Founded in 1938, the UGA Press is a publishing division of the University of Georgia and is located on the North Campus in Athens, Georgia, USA. It is the oldest and largest publishing house in the state of Georgia and one of the largest in the South. UGA Press has been one of 130 full members of the prestigious Association of American University Presses since 1940. The University of Georgia and Mercer University are the only member presses in the state of Georgia.
The Press employs 24 full-time publishing professionals, the Press currently publishes 80-85 new books a year and has more than 1500 titles in print. The Press is the only scholarly publisher within the University System of Georgia serving all 31 institutions of higher education in the state.
In 2008 the Press received the Governor s Award in the Humanities.



The UGA Press publishes 70-80 titles each year of scholarly and academic, regional, and literary works with a focus on American and Southern studies. It is also a leading publisher of African-American studies, civil rights history and environmental studies.
The Flannery O'Connor Award for Short Fiction was established by Charles East, then the editor-in-chief of the UGA Press, in 1983 to recognize gifted young writers. The Press is also a long-time publisher of creative writing through books published in conjunction with the Flannery O Connor Award for Short Fiction, the Association of Writers & Writing Programs|Associated Writers and Writing Programs Award for Creative Nonfiction, and other literary competitions and series. The publishing program has been nationally recognized, and in recent years a number of books published by the Press have won major awards.
In conjunction with the Georgia Humanities Council and GALILEO, the UGA Press created the New Georgia Encyclopedia, an online resource of Georgia history.
The UGA Press has successfully published original novels and works by writers such as Rick Bass, Erskine Caldwell, Terry Kay, Jim Kilgo, Barry Lopez, Judith Ortiz Cofer, Mary Hood, Harry Crews, Tom Wicker, Calvin Trillin, Roy Blount, Jr., Eugene Genovese, Rebecca Solnit, David Carkeet (of Campus Sexpot fame), and Catherine Clinton.



The Press has been the subject of several scandals. Documents uncovered by the website Foetry.com revealed that the 1999 University of Georgia Contemporary Poetry series prize to Peter M. Sacks had been judged by Jorie Graham, a colleague of Sacks at Harvard who subsequently married him. Throughout the course of the controversy, series editor Bin Ramke had insisted that judges of the contest be kept secret, and until Foetry.com obtained the names of judges via The Open Records Act, the conflict of interest had been undisclosed. As a result of the critical coverage from Foetry.com and elsewhere, Ramke resigned from the editorship of the series. The University of Georgia Press now discloses the names of its poetry judges, who "are instructed to avoid conflicts of interest of all kinds."
On October 27, 2005, the University of Georgia Press rescinded author Brad Vice's Flannery O'Connor Award for Short Fiction and recalled copies of his collection The Bear Bryant Funeral Train. Vice was alleged to have plagiarized sections of one story from Carl Carmer's book Stars Fell on Alabama (1934) (a charge that Vice and others dispute).



Flannery O'Connor Award for Short Fiction
University of Georgia
University System of Georgia






Official site
UGA Press blog
UGA Press celebrates 75 years of publishing, UGA News, February 11, 2013
UGA Press names 15 members to its new Advisory Council, UGA Columns faculty staff newspaper, March 20, 2006
UGA Public Affairs Press Release, Friday, March 23, 2001
The New Georgia Encyclopedia