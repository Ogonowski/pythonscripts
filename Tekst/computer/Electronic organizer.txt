An electronic organizer (or electric organizer) is a small calculator-sized computer, often with an in-built diary application but few other functions such as an address book and calendar. It normally has a small alphanumeric keypad and an LCD screen of one, two or three lines.

Because of the advent of personal digital assistants, and later smartphones in the 2000s and 2010s respectively, both of which have a larger set of features, electronic organizers are rarely seen today.



Casio digital diaries were produced by Casio in the early and mid 1990s, but have since been entirely superseded by Mobile Phones and PDA's.



Telephone directory
Schedule keeper: Keep track of appointments.
Memo function: Store text data such as price lists, airplane schedules, and more.
To do list: Keep track of daily tasks, checking off items as you complete them.
World time: Find out the current time in virtually any location on the globe.
Secret memory area: The secret memory area keeps personal data private. Once a password is registered, data is locked away until the password is used to access the secret area.
Alarm
Metric conversion function: Conversion between metric units and another measurement unit.
Currency conversion function



PDA
Smartphone
Pocket computer


