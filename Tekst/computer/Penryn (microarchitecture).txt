In Intel's Tick-Tock cycle, the 2007/2008 "Tick" was the shrink of the Core microarchitecture to 45 nanometers as CPUID model 23. In Core 2 processors, it is used with the code names Penryn (Socket P), Wolfdale (LGA 775) and Yorkfield (MCM, LGA 775), some of which are also sold as Celeron, Pentium and Xeon processors. In the Xeon brand, the Wolfdale-DP and Harpertown code names are used for LGA 771 based MCMs with two or four active Wolfdale cores.
The chips come in two sizes, with 6 MB and 3 MB L2 cache. The smaller version is commonly called Penryn-3M and Wolfdale-3M as well as Yorkfield-6M, respectively. The single-core version of Penryn, listed as Penryn-L here, is not a separate model like Merom-L but a version of the Penryn-3M model with only one active core.






The processors of the Core microarchitecture can be categorized by number of cores, cache size, and socket; each combination of these has a unique code name and product code that is used across a number of brands. For instance, code name "Allendale" with product code 80557 has two cores, 2 MB L2 cache and uses the desktop socket 775, but has been marketed as Celeron, Pentium, Core 2 and Xeon, each with different sets of features enabled. Most of the mobile and desktop processors come in two variants that differ in the size of the L2 cache, but the specific amount of L2 cache in a product can also be reduced by disabling parts at production time. Wolfdale-DP and all quad-core processors except Dunnington QC are multi-chip modules combining two dies. For the 65 nm processors, the same product code can be shared by processors with different dies, but the specific information about which one is used can be derived from the stepping.



In the model 23 (cpuid 01067xh), Intel started marketing stepping with full (6 MiB) and reduced (3 MiB) L2 cache at the same time, and giving them identical cpuid values. All steppings have the new SSE4.1 instructions. Stepping C1/M1 was a bug fix version of C0/M0 specifically for quad core processors and only used in those. Stepping E0/R0 adds two new instructions (XSAVE/XRSTOR) and replaces all earlier steppings.
In mobile processors, stepping C0/M0 is only used in the Intel Mobile 965 Express (Santa Rosa refresh) platform, whereas stepping E0/R0 supports the later Intel Mobile 4 Express (Montevina) platform.
Model 30 stepping A1 (cpuid 106d1h) adds an L3 cache as well as six instead of the usual two cores, which leads to an unusually large die size of 503 mm . As of February 2008, it has only found its way into the very high-end Xeon 7400 series (Dunnington).






x86 architecture
List of Intel CPU microarchitectures


