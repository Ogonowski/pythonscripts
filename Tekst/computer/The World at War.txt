The World at War (1973 74) is a 26-episode British television documentary series chronicling the events of the Second World War. At the time of its completion in 1973 it was the most expensive series ever made, costing  900,000. It was produced by Jeremy Isaacs, was narrated by Laurence Olivier, and includes a score composed by Carl Davis. A book, The World at War, was written by Mark Arnold-Forster and released in 1973, to accompany the TV series.
Since production was completed, The World at War has attracted acclaim and is now regarded as a landmark in British television history. The producer Jeremy Isaacs was considered ahead of his time in resurrecting studies of military history. The series focused on, among other things, portrayal of the devastating human experiences of the conflict; how life and death throughout the war years affected soldiers, sailors and airmen, civilians, concentration camp inmates and the tragic victims of tyranny.



The World at War was commissioned by Thames Television in 1969. Such was the extent of its research it took four years to produce at a cost of  900,000 (equivalent to  13,229,653 in 2015). At the time this was a record for a British television series. It was first shown in 1973 on ITV.
The series featured interviews with major members of the Allied and Axis campaigns, including eyewitness accounts by civilians, enlisted men, officers and politicians, amongst them Albert Speer, Karl D nitz, Walter Warlimont, James Stewart, Bill Mauldin, W. Averell Harriman, Curtis LeMay, Lord Mountbatten of Burma, Alger Hiss, Toshikazu Kase, Mitsuo Fuchida, Minoru Genda, J. B. Priestley, Brian Horrocks, John J. McCloy, Lawrence Durrell, Sir Arthur Harris, Charles Sweeney, Paul Tibbets, Lord Avon, Traudl Junge, Mark Clark, Adolf Galland, Hasso von Manteuffel, Jock Colville and historian Stephen Ambrose.
In the programme The Making of "The World at War", included in the DVD set, Jeremy Isaacs explains that priority was given to interviews with surviving aides and assistants rather than recognised figures. The most difficult person to locate and persuade to be interviewed was Heinrich Himmler's adjutant Karl Wolff. During the interview he admitted to witnessing a large-scale execution in Himmler's presence. Isaacs later expressed satisfaction with the content of the series, noting that if it had been unclassified knowledge at the time of production, he would have added references to British codebreaking efforts.
In a list of the 100 Greatest British Television Programmes compiled by the British Film Institute during 2000, voted for by industry professionals, The World at War ranked 19th.



The series was originally transmitted on the ITV network in the United Kingdom between 31 October 1973 and 8 May 1974, and has subsequently been shown around the world. It was first shown in the US in syndication on various stations in 1975. WOR in New York aired the series in the mid-1970s, although episodes were edited both for graphic content and to include sufficient commercial breaks. PBS station WGBH broadcast the series unedited and in its entirety in the late 1980s. The Danish channel DR2 also broadcast the series in December 2006 and January 2007. The History Channel in Japan began screening the series in its entirety in April 2007. It repeated the entire series again in August 2011. The Military History Channel in the UK broadcast the series over the weekend of 14 and 15 November 2009. The Military Channel (now American Heroes Channel) in the United States aired the series in January 2010, and has shown it regularly since. BBC Two in the UK transmitted a repeat run of the series since 5 September 1994 at teatime and has been shown continuously to this day at various times. In 2011, the British channel Yesterday started a showing of the series.
Each episode was 52 minutes excluding commercials; as was customary for ITV documentary series at the time, it was originally screened with only one central break. The Genocide episode was screened uninterrupted.
The series was also put on 13 Laservision Longplay videodisks by Video Garant Amsterdam 1980, and included Dutch subtitling for the Dutch television market.



The series has 26 episodes. Producer Jeremy Isaacs asked Noble Frankland, then director of the Imperial War Museum, to list fifteen main campaigns of the war and devoted one episode to each. The remaining eleven episodes are devoted to other matters, such as the rise of the Third Reich, home life in Britain and Germany, the experience of occupation in the Netherlands, and the Nazis' use of genocide. Episode 1 begins with a cold open describing the massacre at the French village of Oradour-sur-Glane by the Waffen SS. The same event is referenced again at the end of Episode 26 and the series ends with Laurence Olivier uttering the poignant word, "Remember".



Some footage and interviews which were not used in the original series were later made into additional hour or half-hour documentaries narrated by Eric Porter. These were released as a bonus to the VHS version and are included in the DVD set of the series, first released in 2002.
"Hitler's Germany: The People's Community (1933 1939)"
"Hitler's Germany: Total War (1939 1945)"
"The Two Deaths of Adolf Hitler"
"Secretary to Hitler"
"Warrior - Reflections of Men at War"
"From War to Peace"
"The Final Solution: Part One"
"The Final Solution: Part Two"



The original book The World at War, which accompanied the series, was written by Mark Arnold-Forster in 1973. In October 2007 Ebury Press published The World at War, a new book by Richard Holmes, an oral history of the Second World War drawn from the interviews conducted for the TV series. The programme's producers committed hundreds of interview-hours to tape in its creation, but only a fraction of that recorded material was used for the final version of the series. A selection of the rest of this material was published in this book, which included interviews with Albert Speer, Karl Wolff (Himmler's adjutant), Traudl Junge (Hitler's secretary), James Stewart (USAAF bomber pilot and Hollywood star), Anthony Eden, John Colville (Private Secretary to Winston Churchill), Averell Harriman (US Ambassador to the Soviet Union) and Arthur "Bomber" Harris (Head of RAF Bomber Command).


