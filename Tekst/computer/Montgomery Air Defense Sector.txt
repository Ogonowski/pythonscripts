The Southeast Air Defense Sector (SEADS), was a unit of the US Air Force located at Tyndall Air Force Base near Panama City, Florida. It provided air defense and surveillance of the southeastern region of the US. SEADS closed in winter 2006, giving up surveillance and control of their airspace to the Eastern Air Defense Sector (EADS) and the former Northeast Air Defense Sector (NEADS).






The origins of the Southeast Air Defense Sector (SEADS) are in September 1957 with the formation of its predecessor organization, the Montgomery Air Defense Sector (MoADS) by Air Defense Command (ADC). It was established in September 1957 with a mission to train and maintain tactical flying units in state of readiness in order to defend the Southeastern United States, assuming control of former ADC Central Air Defense Force units. Its original region consisted of ADC atmospheric forces (fighter-interceptor and radar units) located east of the Mississippi River, south of the 34th parallel north and east to a designated line west of the 86th meridian west, southeast to the southernmost point of Key Largo Island, Florida.
It was consolidated on 1 January 1959 with the Shreveport and Miami Air Defense Sectors, defining a region south of 34th parallel north, bordering on the east along the intersection of the parallel southeast along the Georgia South Carolina border to the Atlantic coastline. In the west, the sector was responsible for most of Eastern Texas south of the 34th parallel north, including the state of Louisiana, eastwards. It operated a Manual Air Direction Center (MDC) at Dobbins AFB, Georgia. The sector's mission was to train and maintain tactical flying units in state of readiness in order to defend the northeast United States while initially continuing to operate the MDC.
Beginning on 1 July 1958, it began operations of a SAGE (Semi-Automatic Ground Environment) Direction Center DC-09 32 45 15 N 086 14 29 W at Gunter AFB, Alabama.
During the Cuban Missile Crisis, MoADS was the forward command center for Continental Air Defense Command, remaining on heightened alert for 36 days as part of Task Force 32. This period of constant alert was the longest alert period for any organization during the Cold War
On 1 April 1966, MoADS was inactivated, as were the other 22 sectors in the country. Most of its assets were assumed by the 32d Air Division; the 33d Air Division assumed assets in eastern North and South Carolina. The DC-09 SAGE Direction Center was assigned to the 32d Air Division, remaining in operation until 31 December 1969. Today it is used as offices by Air University, Air Education and Training Command at Gunter AFB.



On 1 July 1987, the Montgomery Air Defense Sector (MOADS) was reactivated, and co-located with the 23d Air Division. The 23d Air Division was inactivated and all atmospheric defense assets of the Division were transferred to the MOADS, re-designated the Southeast Air Defense Sector (SEADS).
SEADS was responsible for the atmospheric defense of approximately 1,000,000 square miles (2,600,000 km2) of airspace and 3,000 miles (4,800 km) of coastline extending from Virginia to Texas. It was the busiest of the air defense sectors comprising the NORAD Continental United States North American Aerospace Defense Command Region. It operated a Sector Operations Control Center (SOCC) at Tyndall AFB, part of the Joint Surveillance System (JSS) which had replaced SAGE in 1983. This system, using the latest advances in computerized airspace control, relied on digitized radar inputs from Air Route Surveillance Radar (ARSR) sites jointly operated by the Federal Aviation Administration and the Air Force, and tethered aerostat radar balloons. More than 2,000 aircraft were detected and identified each day by SEADS technicians and operators.
On 1 October 1995, the Southeast Air Defense Sector was reassigned to the Florida Air National Guard; SEADS re-designated Southeast Air Defense Sector (ANG). It came under the Continental NORAD Region (CONR) Headquarters at Tyndall AFB, Florida.
On 1 November 2005, SEADS ceased air defense operations and its duties were absorbed into the Northeast Air Defense Sector which is now known as the Eastern Air Defense Sector. The SEADS transformed into the 601st Air and Space Operations Center and currently performs the duties as the Air Operations Center for AFNORTH.
Known Air National Guard units with an air defense mission under EADS were:
187th Fighter Wing (F-16), Alabama ANG, Montgomery, Alabama
125th Fighter Wing (F-16), Florida ANG, Jacksonville, Florida
183d Fighter Wing (F-16), Illinois ANG, Springfield, Illinois
159th Fighter Wing, (F-15), Louisiana ANG, New Orleans, Louisiana
169th Fighter Wing (F-16), South Carolina ANG, Eastover, South Carolina



Designated as Montgomery Air Defense Sector and organized on 8 September 1957
Discontinued on 1 April 1966
Redesignated as Southeast Air Defense Sector and activated on 1 July 1987
Inactivated on 1 November 2005



35th Air Division, 8 September 1957
32d Air Division, 15 November 1958
26th Air Division, 1 July 1963
73d Air Division, 1 October 1964 - 1 April 1966
23d Air Division, 1 July 1987
Florida Air National Guard, 1 October 1996 - 1 November 2005



Gunter AFB, Alabama, 8 September 1957 - 1 April 1966
Tyndall AFB, Florida, 1 July 1987 - 1 November 2005






4751st Air Defense Wing (Missile)
Eglin AFB Auxiliary Field #9, Florida, 1 October 1959 - 1 July 1962



48th Fighter-Interceptor Squadron
Langley AFB, Virginia, 1987-1991
319th Fighter Interceptor Training Squadron
Homestead AFB, Florida, 1 March 1963 - 1 April 1966
482d Fighter-Interceptor Squadron
Seymour-Johnson AFB, North Carolina, 1 July 1965 - 1 April 1966



4751st Air Defense Squadron (Missile)
Eglin AFB Auxiliary Field #9, Florida, 1 July 1962 - 1 July 1963







List of MAJCOM wings of the United States Air Force
List of USAF Aerospace Defense Command General Surveillance Radar Stations
Aerospace Defense Command Fighter Squadrons
Alaskan Air Defense Sector (176th Air Control Squadron)
Hawaii Region Air Operations Center (169th Aircraft Control and Warning Squadron)
North American Aerospace Defense Command
Western Air Defense Sector
Eastern Air Defense Sector
Southwest Air Defense Sector









 This article incorporates public domain material from websites or documents of the Air Force Historical Research Agency.
Cornett, Lloyd H.; Johnson, Mildred W. (1980). A Handbook of Aerospace Defense Organization, 1946 1980 (PDF). Peterson AFB, CO: Office of History, Aerospace Defense Center. 
Mueller, Robert (1989). Air Force Bases, Vol. I, Active Air Force Bases Within the United States of America on 17 September 1982 (PDF). Washington, DC: Office of Air Force History. ISBN 0-912799-53-6. 
NORAD/CONAD Participation in the Cuban Missile Crisis, Historical Reference Paper No. 8, Directorate of Command History Continental Air Defense Command, Ent AFB, CO, 1 Feb 63 (Top Secret NOFORN declassified 9 March 1996)
Further reading
Leonard, Barry (2009). History of Strategic Air and Ballistic Missile Defense (PDF). Vol II, 1955-1972. Fort McNair, DC: Center for Military History. ISBN 978-1-43792-131-1. 
Maurer, Maurer, ed. (1982) [1969]. Combat Squadrons of the Air Force, World War II (PDF) (reprint ed.). Washington, DC: Office of Air Force History. ISBN 0-405-12194-6. LCCN 70605402. OCLC 72556. 
McMullen, Richard F. (1964) The Fighter Interceptor Force 1962-1964, ADC Historical Study No. 27 (Confidential, declassified 22 March 2000)
Redmond, Kent C.; Smith, Thomas M. (2000). From Whirlwind to MITRE: The R&D Story of The SAGE Air Defense Computer. Cambridge, MA: MIT Press. ISBN 978-0-262-18201-0. 
Winkler, David F.; Webster, Julie L (1997). Searching the skies: The legacy of the United States Cold War Defense Radar Program (PDF). Champaign, IL: US Army Construction Engineering Research Laboratories. LCCN 97020912. 



Radomes.org Montgomery Air Defense Sector
First Air Force Units
Photos of Montgomery Air Defense Sector SAGE facilities
History of Tyndall AFB
Air Force Historical Research Agency
SEADS mission overview