The Japanese invasion of Manchuria began on September 18, 1931, when the Kwantung Army of the Empire of Japan invaded Manchuria immediately following the Mukden Incident. The Japanese established a puppet state called Manchukuo, and their occupation lasted until the end of World War II.



The Chinese-Japanese dispute in July 1931 (the Wanpaoshan Incident) was followed by the Mukden Incident, on September 18, 1931. The same day as the Mukden Incident, the Japanese Imperial General Headquarters, which had decided upon a policy of localizing the incident, communicated its decision to the Kwantung Army command. However, Kwantung Army commander-in-chief General Shigeru Honj  instead ordered his forces to proceed to expand operations all along the South Manchurian Railway. Under orders from Lieutenant General Jir  Tamon, troops of the 2nd Division moved up the rail line and captured virtually every city along its 730-mile length in a matter of days, occupying Anshan, Haicheng, Kaiyuan, Tiehling, Fushun, Szeping-chieh, Changchun, Kuanchengtzu, Yingkou, Antung, and Penhsihu.
Likewise on September 19, in response to General Honj 's request, the Chosun Army in Korea under General Senjuro Hayashi ordered the 20th Infantry Division to split its force, forming the 39th Mixed Brigade, which departed on that day for Manchuria without authorization from the Emperor.
Between September 20 and September 25, Japanese forces took Hsiungyueh, Changtu, Liaoyang, Tungliao, Tiaonan, Kirin, Chiaoho, Huangkutun and Hsin-min. This effectively secured control of Liaoning and Kirin provinces and the main line of rail communications to Korea.
Tokyo was shocked by the news of the Army acting without orders from the central government. The Japanese civilian government was thrown into disarray by this act of "gekokujo" insubordination, but as reports of one quick victory after another began to arrive, it felt powerless to oppose the Army, and its decision was to immediately send three more infantry divisions from Japan, beginning with the 14th Mixed Brigade of the IJA 7th Division. During this era, the elected government could be held hostage by the Army and Navy, since Army and Navy members were constitutionally necessary for the formation of cabinets. Without their support, the government would collapse.



After the Liaoning Provincial government fled Mukden, it was replaced by a "Peoples Preservation Committee" which declared the secession of Liaoning province from the Republic of China. Other secessionist movements were organized in Japanese-occupied Kirin by General Xi Qia head of the "New Kirin" Army, and at Harbin, by General Chang Ching-hui. In early October, at Taonan in northwest Liaoning province, General Chang Hai-peng declared his district independent of China, in return for a shipment of a large quantity of military supplies by the Japanese Army.
On October 13, General Chang Hai-peng ordered three regiments of the Hsingan Reclamation Army under General Xu Jinglong north to take the capital of Heilongjiang province at Tsitsihar. Some elements in the city offered to peacefully surrender the old walled town, and Chang advanced cautiously to accept. However his advance guard was attacked by General Dou Lianfang's troops, and in a savage fight with an engineer company defending the north bank, were sent fleeing with heavy losses. During this fight the Nenjiang railroad bridge was dynamited by troops loyal to General Ma Zhanshan to prevent its use.




Using the repair of the Nen River Bridge as the pretext, the Japanese sent a repair party in early November under the protection of Japanese troops. Fighting erupted between the Japanese forces and troops loyal to the acting governor of Heilongjiang province Muslim General Ma Zhanshan, who chose to disobey the Kuomintang government's ban on further resistance to the Japanese invasion.
Despite his failure to hold the bridge, General Ma Zhanshan became a national hero in China for his resistance at Nenjiang Bridge, which was widely reported in the Chinese and international press. The publicity inspired more volunteers to enlist in the Anti-Japanese Volunteer Armies.
The repaired bridge made possible the further advance of Japanese forces and their armored trains. Additional troops from Japan, notably the 4th Mixed Brigade from the 8th Division, were sent in November.
On November 15, 1931, despite having lost more than 400 men and 300 left wounded since November 5, General Ma declined a Japanese ultimatum to surrender Tsitsihar. On November 17, in subzero weather, 3,500 Japanese troops, under the command of General Jir  Tamon, mounted an attack, forcing General Ma from Tsitsihar by November 19.




In late November 1931, General Honj  dispatched 10,000 soldiers in 13 armored trains, escorted by a squadron of bombers, in an advance on Chinchow from Mukden. This force had advanced to within 30 kilometres (19 mi) of Chinchow, when it received an order to withdraw. The operation was cancelled by Japanese War Minister General Jir  Minami, due to the acceptance of modified form of a League of Nations proposal for a "neutral zone" to be established as a buffer zone between China proper and Manchuria pending a future Chinese-Japanese peace conference by the civilian government of Prime Minister Wakatsuki Reijiro in Tokyo.
However, the two sides failed to reach a lasting agreement. The Wakatsuki government soon fell and was replaced by a new cabinet led by Prime Minister Inukai Tsuyoshi. Further negotiations with the Kuomintang government failed, the Japanese government authorized the reinforcement of troops in Manchuria. In December, the rest of 20th Infantry Division, along with the 38th Mixed Brigade from the 19th Infantry Division were sent into Manchuria from Korea while the 8th Mixed Brigade from the 10th Infantry Division was sent from Japan. The total strength of the Kwantung Army was thus increased to around 60,450 men.
With this stronger force the Japanese Army announced on December 21 the beginning of large scale anti-bandit operations in Manchuria to quell a growing resistance movement by the local Chinese population in Liaoning and Kirin provinces.
On December 28, a new government was formed in China after all members of the old Nanjing government resigned. This threw the military command into turmoil, and the Chinese army retreated to the south of the Great Wall into Hebei province, a humiliating move which lowered China's international image. Japanese forces occupied Chinchow on January 3, 1932, after the Chinese defenders retreated without giving combat. The following day the Japanese occupied Shanhaiguan completing their military takeover of southern Manchuria.




With southern Manchuria secure, the Japanese turned north to complete the occupation of Manchuria. As negotiations with Generals Ma Zanshan and Ting Chao to defect to the pro-Japanese side had failed, in early January Colonel Kenji Doihara requested collaborationist General Qia Xi to advance his forces and take Harbin.
The last major Chinese regular force in northern Manchuria was led by General Ting Chao who organized the defense of Harbin successfully against General Xi until the arrival of the IJA 2nd Division under General Jir  Tamon. Japanese forces took Harbin on February 4, 1932.
By the end of February Ma had sought terms and joined the newly formed Manchukuo government as governor of Heilongjiang province and Minister of War.
On February 27, 1932, Ting offered to cease hostilities, ending official Chinese resistance in Manchuria, although combat by guerilla and irregular forces continued as Japan spent many years in their campaign to pacify Manchukuo.



Western media reported on the events with accounts of atrocities such as bombing civilians, or firing upon shell-shocked survivors. It aroused considerable antipathy to Japan, which lasted until the end of World War II.
When the Lytton Commission issued a report on the invasion, despite its statements that China had to a certain extent provoked Japan, and China's sovereignty over Manchuria was not absolute, Japan took it as an unacceptable rebuke and withdrew from the League of Nations, which also helped create international isolation.
The Manchurian Crisis had a significant negative impact on the moral strength and influence of the League of Nations. As critics had predicted, the League was powerless if a strong nation decided to pursue an aggressive policy against other countries, allowing a country such as Japan to commit blatant aggression without serious consequences. Adolf Hitler and Benito Mussolini were also aware of this, and within three years would both follow Japan's example in aggrandisation against their neighbors, in the case of Italy, against Abyssinia, and Hitler, against Czechoslovakia and Poland.


