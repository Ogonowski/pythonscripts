BMI Healthcare is an independent provider of private healthcare, offering treatment to private patients, medically insured patients, and NHS patients. It is a subsidiary of General Healthcare Group (GHG), forming the majority of the operations (OpCo) side of GHG, which owns and operates the hospital business. It has 66 hospitals and healthcare facilities across the UK with headquarters based in London.




BMI Healthcare was formed in 1970, when US hospital group AMI acquired its first hospital in the UK, the Harley Street Clinic. By 1983 the AMI group had grown to 13 hospitals and by 1988 had created a psychiatric division, health services division and floated on the London Stock Exchange.
In 1990 AMI was purchased by then Generale des Eaux. AMI was renamed BMI Healthcare in 1993 and its new corporate group named General Healthcare Group. In 1997 funds managed by Cinven Ltd acquired GHG. After a further period of expansion GHG merged with Amicus Healthcare group in 1998 and the group grew its portfolio to over 40 hospitals.
In 2000 Cinven Ltd sold its investment to funds advised by BC Partners and in 2005 BMI Healthcare s health services, Occupational Health division was sold to the Capita Group Plc. GHG was bought from BC Partners by a consortium led by Netcare (Network Healthcare Holdings Limited), a large South African Healthcare company, in 2006.
In 2011 Stephen Collier was appointed chief executive. BMI showed a strong performance in FY2013, with revenue up by 2.1% (to  851.3 million). The firm has developed its delivery channels, and in 2013 began work with Medopad to pilot an integrated mobile health solution.
As of 2013, BMI Healthcare has 66 hospitals and healthcare facilities throughout the UK.






As a private healthcare provider, the bulk of BMI Healthcare s revenue is generated through the treatment of insured and self-pay patients, with many being surgical in nature. BMI Healthcare has over 100 different specialties and services, but has a particular focus on Women s Health, Orthopaedics, Fertility, Oncology, Weight Loss and Cosmetic Surgery. As with other private healthcare providers in 2012/13, recessionary pressures have led to declining numbers of private patients, leading to an increased push towards securing NHS funded business.
BMI Healthcare also offers health services to corporate clients, providing health assessments, on-site education days, physiotherapy, GP Services, occupational health services and flu clinics to company employees.



In May 2014 Brent Clinical Commissioning Group awarded a  5.8m ophthalmology contract to BMI Healthcare after a two-year hold up caused by complaints about the CCG s procurement procedure.
The Woodlands Hospital in Darlington is used by South Tees Hospitals NHS Foundation Trust to help with elective surgery capacity problems, usually in the winter. This often involves the same surgeon working on a Sunday. 70% of the hospital's work is NHS funded.



BMI Healthcare has seen increased numbers of international patients in recent years, with most international patients receiving treatment at London-based hospitals. The majority of patients BMI Healthcare treats are government sponsored from the Gulf regions of the Middle East. Insured patients living or travelling in the UK and self-paying patients from abroad make up the remainder of the international market BMI Healthcare treats.
Plymouth Hospitals NHS Trust has a contract for cardiac surgery performed by the trust s own surgeons at the London Independent Hospital. This is mainly valve replacements and coronary artery bypass grafts. Patients usually spend 5 8 days in hospital and are admitted the night before to take account of travelling.






In 2013, BMI Healthcare launched the Big Health Pledge to raise awareness of the UK s five biggest preventable killers: lung disease, heart disease, cancer, liver disease and stroke. It sought to promote healthier living through a range of initiatives in order to lessen the impact these diseases have on the British population.
From the 9th September to the 27th September 2013 BMI Healthcare supported participants taking part in Ask Italian s Grand tour, raising money for Great Ormond Street Hospital Children s Charity. BMI Healthcare provided nurses and physiotherapists to ensure those taking part remained injury free for the length of the tour.



At the 2013 Independent Healthcare Awards run by Laing & Buisson, consultants from BMI Healthcare were shortlisted in the Innovation, Outstanding Contribution, Risk Management and Nursing Practice categories.
Annie Ollivierre-Smith, Cath lab manager and lead cardiac nurse at BMI The London Independent Hospital, won Nurse of the year 2013, at the Nursing Times awards. She was the first nurse ever from the private sector to win the award.
In 2011, BMI Healthcare became the first private healthcare provider in the UK to be awarded VTE (Venous Thromboembolism) exemplar status across all of its sites, recognising its efforts to reduce the health risks associated with Deep Vein Thrombosis and Pulmonary embolism. BMI Healthcare benefited greatly from the expertise of King s Thrombosis Centre, leading to the creation of a National Thrombosis Team tasked with the implementation of VTE prevention policies across BMI Healthcare Hospitals.


