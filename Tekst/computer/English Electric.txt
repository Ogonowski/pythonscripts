The English Electric Company Limited was a British industrial manufacturer formed after the armistice of World War I at the end of 1918. It was created to make one of Britain's three principal electrical manufacturing concerns by amalgamating five businesses which, during the war, had been making munitions armaments and aeroplanes.
It initially specialised in industrial electric motors and transformers, railway locomotives and traction equipment, diesel motors and steam turbines. In the end its activities expanded to include consumer electronics, guided missiles, military aircraft and mainframe computers.
English Electric's operations were merged with GEC's in 1968, the combined business employing more than 250,000 people.
Two English Electric aircraft designs became landmarks in British aeronautical engineering; the Canberra or B-57 and the Lightning. In 1960, long before the merger with GEC, English Electric Aircraft (40%) with Vickers (40%) and Bristol (20%) formed British Aircraft Corporation.



Aiming to turn their employees and other assets to peaceful productive purposes those in control of them made an arrangement to group their businesses together and to carry that out formed The English Electric Company Limited in December 1918



English Electric was formed to acquire ownership of:
Coventry Ordnance Works of Coventry which retained a separate identity and Scotstoun sold by April 1920
Phoenix Dynamo Manufacturing Company of Bradford
Dick, Kerr & Co. of Preston founded 1880 and its subsidiaries:
United Electric Car Company of Preston
Willans & Robinson of Rugby which retained a separate identity not wholly owned.

The owners of the component companies took up the shares in English Electric.



John Pybus was appointed managing director in March 1921 and chairman in April 1926. Initially J H Mansell of Coventry Ordnance Works, John Pybus of Phoenix Dynamo Manufacturing and W Rutherford of Dick Kerr were joint managing directors.
The five previously independent major operations under their control had these principal capabilities:
Coventry Ordnance Works: the plant was built for the production of heavy armaments but was suitable for the manufacture of large generating units
Phoenix Dynamo Works: during the war production was shells and aeroplanes but by July 1919 had been returned to electric motors
Dick Kerr and United Electric Car: special war work munitions, aeroplanes and metallic filament lamps, prior to the war locomotives and tram cars
Willans & Robinson: made steam turbines, condensers and Diesel motors, there was a foundry
Together these businesses covered the whole field of electrical machinery from the smallest fan motor to the largest turbo-generator.
In November 1919 English Electric bought the Stafford works of Siemens Brothers Dynamo Works Ltd. In 1931 Stafford became English Electric's centre.
However there was no post-war boom in electrical generation. Though English Electric products were indeed in heavy demand potential buyers were unable to raise the necessary capital funds. In 1922 a drastic reorganisation of the works was carried through and that managed to halve overheads. The Coventry Ordnance Works was practically closed down. Cables lamps and wireless equipment were then in buoyant demand but that would have been a new field for the company to enter. English Electric's business was in heavy electrical and mechanical plant. Both the 1926 general strike and the miners strike caused heavy losses. In 1929 part of the Coventry Ordnance Works was sold and the pattern shop at Preston, neither of which was required.
By the end of 1929 it was clear the only solution to English Electric's financial difficulties was a financial restructure. The restructure acknowledged the loss of much of the shareholders' capital and brought in new capital to re-equip with new plant and machinery. In the event an American syndicate fronted by Lazard Brothers and Co. bankers came up with the new capital but left control in the hands of the previous shareholders.
In June 1930 four fresh directors were appointed filling four new vacancies.
Ten days later there was a formal announcement of an American Arrangement. "English Electric with works at Preston, Stafford, Rugby, Bradford and Coventry had entered into a comprehensive arrangement" with Westinghouse Electric International Company of New York and Westinghouse Electric & Manufacturing Company of East Pittsburgh, Pennsylvania USA whereby there would be an exchange of technical information between the two organisations on steam turbines and electrical apparatus. It was made clear that this technical and manufacturing link did not carry with it any control from America. In recognition of the exchange arrangement Westinghouse had offered to provide further capital which would be less than 10% of the total including that new capital organised earlier by Lazard Brothers.



Seven weeks later the chairman, W L Hichens, who had temporarily replaced P J Pybus in 1927 retired at the end of July 1930 and was replaced by Sir Holberry Mensforth as a director and as chairman. It was then announced that a Mr George H Nelson had been appointed to the board and would take up the position of managing director early in October. Mensforth had been taken away from his position as general manager of American Westinghouse Trafford Park Manchester  where George Nelson had been his apprentice  in 1919 by the Minister of Transport. The Minister had given Mensforth the responsibility of easing the transition of the nation's munitions businesses back into peacetime industry. It was Mensforth had arranged the technical exchange agreement and extra capital with Westinghouse. They began to reorganise.



The main base of the company's operation was moved from London to Stafford including the sales departments, general and factory accounts and the principal executives previously in London. The managing director was to divide his time between the various works but would be mainly in Stafford or in London
On 30 December 1930 the engineering shops at Preston closed leaving the following distribution:
Preston: specialists in high-tension direct-current railway electrification, rolling stock and trolley buses Dick Kerr
Stafford: medium-sized electrical plant, transformers and switchgear and (from Preston) large turbo-alternator work Siemens
Rugby: prime movers, steam turbines and condensing plant, Fullagar and Diesel engines and (from Preston) water turbine plant Willans & Robinson
Bradford: small motors and control gear and (from Preston) traction motor and traction control work Phoenix
Coventry: engineers small tools (stopped in 1931), zed fuse (cartridge type) transferred to Stafford in 1931 C.O.W.



Manufacture of domestic apparatus got under way at both Stafford and Bradford during 1931. They were followed in 1934 by a range of household meters of various kinds. In the same report to shareholders the chairman pointed out that every day 330 more homes adopted electricity for heating cooking and lighting and between 1929 and 1935 the production of electricity in Britain had increased by 70 per cent.



1933 proved to be the first of four years of real achievement. At the beginning of July 1933 Mensforth stepped down and George Nelson took up the post of chairman. Nelson remained managing director. Sir Holberry kept a seat on the board from which he retired at the end of 1936.
English Electric's recovery was noted by commentators as remarkable. During 1936 passed preference dividends had been brought up to date, they were English Electric's first dividend since a 1924 dividend on ordinary shares. The balance sheet at the end of 1936 showed liquidity was in a strong position and the chairman told shareholders the rate of production in the factories for the last three months of the year was double the rate of production in the first three months.
During 1938 the first dividend was paid on ordinary shares since 1924.
In the summer of 1938 a large display advertisement confidently declared:
ENGLISH ELECTRIC PLANT AND EQUIPMENT in operation throughout the world.
With its historical achievements and the wealth of experience of its several Associated Companies the English Electric Company
continues to maintain its reputation as Manufacturers and Suppliers of electrical and allied products for Home and Overseas markets:

STEAM TURBINES
WATER TURBINES
OIL ENGINES
GENERATORS
SWITCHGEAR
TRANSFORMERS
RECTIFIERS
ELECTRIC MOTORS
ELECTRIC AND DIESEL-ELECTRIC TRACTION EQUIPMENTS
MARINE PROPULSION EQUIPMENT
DOMESTIC APPLIANCES
Complete Electrification Schemes Undertaken



Airframes
The first steps to strengthen the Royal Air Force had been taken in May 1935 and English Electric was brought into the scheme for making airframes working in conjunction with Handley Page. The chairman reported to shareholders that though both Dick Kerr and Phoenix were involved in the aircraft business during and shortly after the previous war the problems had so changed they were now completely new to the company. He also noted as he ended his address that the demand for domestic appliances including cookers, breakfast cookers, washing machines and water heaters was growing progressively.
The Preston works without subcontracting made more than 3,000 Hampden and Halifax aircraft.

Aero engines
In December 1942 English Electric bought the ordinary shares of D. Napier & Son Limited. Mr H G Nelson, son of English Electric chairman George H Nelson, was appointed managing director.
Napier's Sabre engines were used in Typhoon and Tempest aircraft and Lion engines in Motor Torpedo Boats
Tanks, locomotives, submarines, ships, power generation
The Stafford works made thousands of Covenanter Centaur Cromwell tanks as well as precision instruments for aircraft, electric propulsion and electrical equipment.
The Rugby works made Diesel engines for ships, submarines and locomotives, steam turbines for ships and turbo-alternator sets for power stations.
Bradford made electric generators for ships' auxiliaries and a wide variety of other naval and aviation material.

Employees
In April 1945 English Electric employed 25,000 persons in its four main works. Subsequently the chairman revealed that the peak employment number during wartime had been 45,000 when including Napier's people.
de Havilland Vampire
In September 1945 details were released of the Vampire jet, the world's fastest aircraft, which could exceed 500mph by a considerable margin. The aircraft was built by English Electric at its Preston works, the Frank Halford designed Goblin jet engine, the world's most powerful, by de Havilland in London.






From 1912 to 1924, United Electric and English Electric (with assistance from Hong Kong and Whampoa Dock) supplied second- and third-series tramcars for Hong Kong Tramways. These cars were eventually retired from 1924 to 1930 as the fourth Generation cars were being introduced.




In 1923 English Electric supplied electric locomotives for the New Zealand Railways for use between Arthurs Pass and Otira, in the Southern Alps. Between 1924 and 1926 they delivered nine box-cab electric (B+B) locomotives to the Harbour Commissioners of Montreal (later the National Harbours Board). In 1927 English Electric delivered 20 electric motor cars for Warsaw's Warszawska Kolej Dojazdowa. During the 1930s equipment was supplied for the electrification of the Southern Railway system, reinforcing EE's position in the traction market, and it continued to provide traction motors to them for many years. In 1936, production of diesel locomotives began in the former tramworks in Preston. Between the late 1930s and the 1950s English Electric supplied electric multiple unit trains for the electrified network in and around Wellington, New Zealand. Between 1951 and 1959, English Electric supplied the National Coal Board with five 51-ton, 400 hp electric shunting locomotives for use on the former Harton Coal Company System at South Shields (which had been electrified by Siemens in 1908) to supplement the existing fleet of ten ageing Siemens and AEG locomotives. English Electric took over Vulcan Foundry and Robert Stephenson and Hawthorns, both with substantial railway engineering pedigrees, in 1955.
English Electric produced nearly 1000 diesel and electric locomotives, of nine different classes, for British Rail as part of the Modernisation Plan in the 1950s and 1960s. Most of these classes of locomotive gave long service to British Rail and its successor train operating companies, some still being active well into the 21st century.




Both Dick, Kerr & Co. and the Phoenix Dynamo Manufacturing Company built aircraft in the First World War, including flying boats designed by the Seaplane Experimental Station at Felixstowe, 62 Short Type 184 and 6 Short Bombers designed by Short Brothers. Aircraft manufacture under the English Electric name began in Bradford in 1922 with the Wren but lasted only until 1926 after the last Kingston flying boat was built.
With War in Europe looming, English Electric was instructed by the Air Ministry to construct a "shadow factory" at Samlesbury Aerodrome in Lancashire to build Handley Page Hampden bombers. Starting with Flight Shed Number 1, the first Hampden built by English Electric made its maiden flight on 22 February 1940 and, by 1942, 770 Hampdens had been delivered   more than half of all the Hampdens produced. In 1940, a second factory was built on the site and the runway was extended to allow for construction of the Handley Page Halifax four-engined heavy bomber to begin. By 1945, five main hangars and three runways had been built at the site, which was also home to No. 9 Group RAF. By the end of the war, over 2,000 Halifaxes had been built and flown from Samlesbury.
In 1942, English Electric took over Napier & Son, an aero-engine manufacturer. Along with the shadow factory, this helped to re-establish the company's aeronautical engineering division. Post-war, English Electric invested heavily in this sector, moving design and experimental facilities to the former RAF Warton near Preston in 1947. This investment led to major successes with the Lightning and Canberra, the latter serving in a multitude of roles from 1951 until mid-2006 with the Royal Air Force.
At the end of the war, English Electric started production under licence of the second British jet fighter, the de Havilland Vampire, with 1,300 plus built at Samlesbury. Their own design work took off after the Second World War under W. E. W. Petter, formerly of Westland Aircraft. Although English Electric produced only two aircraft designs before their activities became part of BAC, the design team put forward suggestions for many Air Ministry projects.

The aircraft division was formed into the subsidiary English Electric Aviation Ltd. in 1958, becoming a founding constituent of the new British Aircraft Corporation (BAC) in 1960; English Electric having a 40% stake in the latter company. The guided weapons division was added to BAC in 1963.



The Industrial Electronics Division was established at Stafford. One of the products produced at this branch was the Igniscope, a revolutionary design of ignition tester for petrol engines. This was invented by Napiers and supplied as Type UED for military use during World War 2. After the war, it was marketed commercially as type ZWA.



In 1946, English Electric took over the Marconi Company, a foray into the domestic consumer electronic market. English Electric tried to take over one of the other major British electrical companies, the General Electric Company (GEC), in 1960 and, in 1963, English Electric and J. Lyons and Co. formed a jointly owned company   English Electric LEO Company   to manufacture the LEO Computer developed by Lyons. English Electric took over Lyons' half-stake in 1964 and merged it with Marconi's computer interests to form English Electric Leo Marconi (English Electric LM). The latter was merged with Elliott Automation and International Computers and Tabulators (ICT) to form International Computers Limited (ICL) in 1967. In 1968 GEC, recently merged with Associated Electrical Industries (AEI), merged with English Electric; the former being the dominant parter, the English Electric name was then lost.







Complete electrification schemes
Polish State Railways
London Post Office Railway, London Post Office Railway 1927 Stock and London Post Office Railway 1962 Stock
Wellington N.Z. suburban railway system
Steam turbines
Munmorah Power Station
Churchill-class submarines
St. Laurent-class destroyers
Restigouche-class destroyers
Hinkley Point A nuclear power station, Hartlepool Nuclear Power Station, Wylfa Nuclear Power Station, Sizewell nuclear power stations
Water turbines
Queen Elizabeth Power Station
Oil engines

Generators
Ultimo Power Station
Tallawarra Power Station
Monowai Power Station
White Bay Power Station
Blyth Power Station
Switchgear, transformers, rectifiers
Drax power station
HVDC Kingsnorth
Nelson River DC Transmission System
Electric motors
British Porpoise-class submarine
Electric and Diesel-electric traction equipment
Blackpool tramway, English Electric Balloon tram
New Zealand Railways Department see Diesel Traction Group (NZ)
Marine Propulsion equipment
Oberon-class submarines
HMAS Oxley (S 57), HMAS Orion
GMV Aranui
Domestic appliances



Aircraft

English Electric P.5 Phoenix "Cork" (1918)
Wren (1923)
Ayr (1923)
Kingston (1924)
Canberra (1949)
English Electric P1A (Lightning prototype)
Lightning (1954)
English Electric P.10 (unbuilt supersonic bomber to OR.330/R.156).
Manned spacecraft
MUSTARD

Guided weapons
Thunderbird (1959)   surface-to-air missile
Blue Water (cancelled 1962)   short-range ballistic missile
Tanks
A13 Covenanter
A33 Excelsior


==