HomeBank is a personal accounting software package that runs on OpenBSD, Linux, FreeBSD, Microsoft Windows, Mac OS (via macports or homebrew) and AmigaOS. Released under the GNU General Public License, HomeBank is free software. HomeBank can be found in the software repositories of Linux distributions such as Debian, Fedora, Mandriva, openSUSE, Gentoo Linux, Arch Linux and Ubuntu.



HomeBank development started in 1995 on an Amiga computer. Version 1.0 was released in January 1998 under a shareware license. In May 2003, version 3.0 was released as freeware and a complete rewrite was started on GNU/Linux using GTK+. In September 2006, version 3.2 was released for GNU/Linux systems only. HomeBank became available on Macintosh systems in August 2007, via MacPorts. In May 2008, version 3.8 was released and became available for Microsoft Windows platforms.




Comparison of accounting software






Official website