Casemate Publishers, also known as Casemate, is a Philadelphia area based publishing company that specializes in producing printed military history books. They have published over 200 books on military history. Many of their books are memoirs and historical overviews of specific military events. They also distribute books for other publishing companies and market their products to enthusiasts, hobbyists, students, instructors and researchers of military history, as well as members of the armed forces and military organizations.



Casemate Publishers was established in 2001, when Combined Publishing s US operations were sold to Perseus Books Group and rolled into its Da Capo imprint. The running of their distribution operation was taken over by Combined Publishing s sales director David Farnsworth, who created new distribution company Casemate Publishers and Book Distributors LLC. Casemate began distributing books in North America for publishers, specializing in military history. The company also began to publish its own military history books and since 2001 has expanded its publishing output to 30 book titles a year.




In July 2007, Casemate supported a management buyout of the Greenhill Books/Chatham Publishing distribution operation and sales team from Lionel Leventhal Ltd and set up a new company, Casemate UK Ltd. As a result, Casemate UK became responsible for the sales, marketing and distribution of a number of military history and art book publishers.



In December 2011, Casemate UK's parent company, Casemate Publishers Ltd bought leading Archaeology Publisher and Bookseller Oxbow Books, together with its US operation, The David Brown Book Company from its founder, David Brown on his retirement. Oxbow was founded and is still based in Oxford, UK in 1983 .



In January 2009, Casemate Publishers launched brand Casemate Athena their new distribution initiative targeted at non-military history enthusiasts including travel, cooking, reference, nostalgia, heritage, sports and gardening. On January 1, 2015 Casemate acquired IPM, International Publisher's Marketing, from Books International and merged its Athena brand into a new division, Casemate IPM.
With the acquisition of Oxbow Books, Casemate also acquired Oxbow's north American distribution arm, The David Brown Company which was founded in 1991. In 2013 The David Brown Book Company was rebranded as Casemate Academic. Casemate Academic provides specialist sales, marketing and distribution to leading institutions and publishers from around the world in the US market.
Casemate also has a specialist brand for selling and marketing art books, Casemate Art.



^ Casemate Publishers, 2010. About Casemate
^ Perseus Books Group website
^ Da Capo website
^ "Military publisher makes advances", The Bookseller, 24 October, 2007.
^ "Casemate adds U.K. division", Publishers Weekly, 8 October 2007.
^ Oxbow Books Website
^ "News from Casemate-Athena", Dane of War, 26 January, 2009.
^ Books International website
^ "Casemate Acquires International Publishers Marketing", The Bookseller, 19 November, 2012.



Casemate website
Casemate UK website
Casemate Athena website
Casemate Athena website
Oxbow Books website
Casemate Academic website