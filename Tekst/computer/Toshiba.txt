Toshiba Corporation ( , Kabushiki-gaisha T shiba, English /t i b /) (commonly referred to as Toshiba, stylized as TOSHIBA) is a Japanese multinational conglomerate corporation headquartered in Tokyo, Japan. Its diversified products and services include information technology and communications equipment and systems, electronic components and materials, power systems, industrial and social infrastructure systems, consumer electronics, household appliances, medical equipment, office equipment, lighting and logistics.
Toshiba was founded in 1938 as Tokyo Shibaura Electric K.K. through the merger of Shibaura Seisaku-sho (founded in 1875) and Tokyo Denki (founded in 1890). The company name was officially changed to Toshiba Corporation in 1978. Toshiba has made numerous corporate acquisitions during its history, including of Semp in 1977, of Westinghouse Electric LLC, a nuclear energy company in 2006, of Landis+Gyr in 2011, and of IBM's point-of-sale business in 2012.
Toshiba is organised into four business groupings: the Digital Products Group, the Electronic Devices Group, the Home Appliances Group and the Social Infrastructure Group. In 2010, Toshiba was the world's fifth-largest personal computer vendor measured by revenues (after Hewlett-Packard, Dell, Acer and Lenovo). In the same year, it was also the world's fourth-largest manufacturer of semiconductors by revenues (after Intel Corporation, Samsung Electronics and Texas Instruments).
Toshiba is listed on the Tokyo Stock Exchange, where it is a constituent of the Nikkei 225 and TOPIX indices, the Osaka Securities Exchange and the Nagoya Stock Exchange .






Toshiba was founded in 1939 by the merger of Shibaura Seisakusho (Shibaura Engineering Works) and Tokyo Denki (Tokyo Electric). Shibaura Seisakusho had been founded as Tanaka Seisakusho by Tanaka Hisashige in 1875 as Japan's first manufacturer of telegraph equipment. In 1904, it was renamed Shibaura Seisakusho. Through the first decades of the 20th century, Shibaura Seisakusho had become a major manufacturer of heavy electrical machinery as Japan modernized during the Meiji Era and became a world industrial power. Tokyo Denki was founded as Hakunetsusha in 1890 and had been Japan's first producer of incandescent electric lamps. It later diversified into the manufacture of other consumer products and in 1899 had been renamed Tokyo Denki. The merger of Shibaura and Tokyo Denki created a new company called Tokyo Shibaura Denki (Tokyo Shibaura Electric) ( ). It was soon nicknamed Toshiba, but it was not until 1978 that the company was officially renamed Toshiba Corporation.

The group expanded rapidly, driven by a combination of organic growth and by acquisitions, buying heavy engineering and primary industry firms in the 1940s and 1950s. Groups created include Toshiba Music Industries/Toshiba EMI (1960), Toshiba International Corporation (1970s) Toshiba Electrical Equipment (1974), Toshiba Chemical (1974), Toshiba Lighting and Technology (1989), Toshiba America Information Systems (1989) and Toshiba Carrier Corporation (1999).
Toshiba is responsible for a number of Japanese firsts, including radar (1912), the TAC digital computer (1954), transistor television and microwave oven (1959), color video phone (1971), Japanese word processor (1978), MRI system (1982), laptop personal computer (1986), NAND EEPROM (1991), DVD (1995), the Libretto sub-notebook personal computer (1996) and HD DVD (2005).
In 1977, Toshiba acquired the Brazilian company Semp (Sociedade Eletromercantil Paulista), subsequently forming Semp Toshiba through the combination of the two companies' South American operations.
In 1987, Tocibai Machine, a subsidiary of Toshiba, was accused of illegally selling CNC milling machines used to produce very quiet submarine propellers to the Soviet Union in violation of the CoCom agreement, an international embargo on certain countries to COMECON countries. The Toshiba-Kongsberg scandal involved a subsidiary of Toshiba and the Norwegian company Kongsberg Vaapenfabrikk. The incident strained relations between the United States and Japan, and resulted in the arrest and prosecution of two senior executives, as well as the imposition of sanctions on the company by both countries. Senator John Heinz of Pennsylvania said "What Toshiba and Kongsberg did was ransom the security of the United States for $517 million."



In 2001, Toshiba signed a contract with Orion Electric, one of the world's largest OEM consumer video electronic makers and suppliers, to manufacture and supply finished consumer TV and video products for Toshiba to meet the increasing demand for the North American market. The contract ended in 2008, ending seven years of OEM production with Orion.
In December 2004, Toshiba quietly announced it would discontinue manufacturing traditional in-house cathode ray tube (CRT) televisions. In 2006, Toshiba terminated production of in-house plasma TVs. To ensure its future competitiveness in the flat-panel digital television and display market, Toshiba has made a considerable investment in a new kind of display technology called SED.
Before World War II, Toshiba was a member of the Mitsui Group zaibatsu (family-controlled vertical monopoly). Today Toshiba is a member of the Mitsui keiretsu (a set of companies with interlocking business relationships and shareholdings), and still has preferential arrangements with Mitsui Bank and the other members of the keiretsu. Membership in a keiretsu has traditionally meant loyalty, both corporate and private, to other members of the keiretsu or allied keiretsu. This loyalty can extend as far as the beer the employees consume, which in Toshiba's case is Asahi.
In July 2005, BNFL confirmed it planned to sell Westinghouse Electric Company, then estimated to be worth $1.8 billion ( 1 billion). The bid attracted interest from several companies including Toshiba, General Electric and Mitsubishi Heavy Industries and when the Financial Times reported on January 23, 2006 that Toshiba had won the bid, it valued the company's offer at $5 billion ( 2.8 billion). The sale of Westinghouse by the Government of the United Kingdom surprised many industry experts, who questioned the wisdom of selling one of the world's largest producers of nuclear reactors shortly before the market for nuclear power was expected to grow substantially; China, the United States and the United Kingdom are all expected to invest heavily in nuclear power. The acquisition of Westinghouse for $5.4 billion was completed on October 17, 2006, with Toshiba obtaining a 77% share, and partners The Shaw Group a 20% share and Ishikawajima-Harima Heavy Industries Co. Ltd. a 3% share.
In late 2007, Toshiba took over from Discover Card as the sponsor of the top-most screen of One Times Square in New York City. It displays the iconic 60-second New Year's countdown on its screen, as well as messages, greetings, and advertisements for the company.
In January 2009, Toshiba acquired the HDD business of Fujitsu.



Toshiba announced on May 16, 2011, that it had agreed to acquire all of the shares of the Swiss-based advanced-power-meter maker Landis+Gyr for $2.3 billion.
In April 2012, Toshiba agreed to acquire IBM's point-of-sale business for $850 million, making it the world's largest vendor of point-of-sale systems.
In July 2012, Toshiba was accused of fixing the prices of LCD panels in the United States at a high level. While such claims were denied by Toshiba, they have agreed to settle alongside several other manufacturers for a total of $571 million.
In January 2014, Toshiba completed its acquisition of OCZ Storage Solutions.
In March 2014, Toshiba sued SK Hynix accusing the company for stealing technology of their NAND flash memory.
In October 2014, Toshiba and United Technologies agreed a deal to expand their joint venture outside of Japan.
Toshiba announced that it started investigating an accounting scandal in 2015. Toshiba states that it might need to mark down profits for the past three years.
Toshiba announced in early 2015 that they would stop making televisions in its own factories. From 2015 onward, Toshiba televisions will be made by Compal for the U.S., or by Vestel and other manufacturers for the European market.
On the 21 July 2015, CEO Hisao Tanaka announced his resignation amid an accounting scandal at the company that Tanaka called "the most damaging event for our brand in the company's 140-year history." Profits had been inflated by $1.2 billion over seven years. Eight other senior official also resigned, including the two previous CEOs. Chairman Masashi Muromachi temporarily became CEO until a successor is selected. Following the $1.2 billion accounting scandal, Toshiba Corp. was removed from a stock index showcasing Japan's best companies. This marks the second reshuffle of the index, which picks companies with the best operating income, return on equity and market value
In September 2015, Toshiba sales fell to their lowest point in two and a half years. The firm said in a statement that its net losses for the quarterly period were 12.3 billion yen ($102m;  66m). The company noted poor performances in its televisions, home appliances and personal computer businesses.




Toshiba is headquartered in Minato-ku, Tokyo, Japan and has operations worldwide. It had around 210,000 employees as of 31 March 2012.
Toshiba is organised into four main business groupings: the Digital Products Group, the Electronic Devices Group, the Home Appliances Group and the Social Infrastructure Group. In the year ended 31 March 2012, Toshiba had total revenues of  6,100.3 billion, of which 25.2% was generated by the Digital Products Group, 24.5% by the Electronic Devices Group, 8.7% by the Home Appliances Group, 36.6% by the Social Infrastructure Group and 5% by other activities. In the same year, 45% of Toshiba's sales were generated in Japan and 55% in the rest of the world.
Toshiba has 39 R&D facilities worldwide, which employ around 4,180 people. Toshiba invested a total of  319.9 billion in R&D in the year ended 31 March 2012, equivalent to 5.2% of sales. Toshiba registered a total of 2,483 patents in the United States in 2011, the fifth-largest number of any company (after IBM, Samsung Electronics, Canon and Panasonic).



Toshiba is organised into the following principal business groupings, divisions and subsidiaries:
Digital Products Group

Digital Products and Services Company
Network & Solution Control Center
Toshiba TEC Corporation

Electronic Devices Group

Semiconductor & Storage Products Company

Discrete Semiconductor Division
Analog & Imaging IC Division
Logic LSI Division
Memory Division
Storage Products Division
Center For Semiconductor Research & Development

Optical Disc Drive Division (Formed partnership with optical disc drive division of Samsung Electronics as Toshiba Samsung Storage Technology Corporation (TSST))
Toshiba Mobile Display Co., Ltd. (This company will be merged with Hitachi Displays, Ltd. and Sony Mobile Display Corporation to form Japan Display Inc. in Spring of 2012.)

Social Infrastructure Group

Power Systems Company (Combined-cycle gas power plants, nuclear power plants, hydro-electric power plants, and associated components)

Nuclear Energy Systems & Services Division
Westinghouse Electric Company (Acquired October 2006)
Thermal & Hydro Power Systems & Services Division
Power and Industrial Systems Research and Development Center

Social Infrastructure Systems Company

Transmission & Distribution Systems Division
Railway & Automotive Systems Division
Railway Systems Division
Automotive Systems Division
Motor & Drive Systems Division
Automation Products & Facility Solution Division
Defense & Electronic Systems Division
Environmental Systems Division

Toshiba Elevator and Building Systems Corporation
Toshiba Solutions Corporation
Toshiba Medical Systems Corporation
Toshiba America Information Systems

Home Appliances Group

Toshiba Home Appliances Corporation
Toshiba Lighting & Technology Corporation
Harison Toshiba Lighting Corporation
Toshiba Carrier Corporation

Others

New Lighting Systems Division
Smart Community Division
Materials & Devices Division



Toshiba offers a wide range of products and services, including air conditioners, consumer electronics (including televisions and DVD and Blu-ray players), control systems (including air-traffic control systems, railway systems, security systems and traffic control systems), electronic point of sale equipment, elevators and escalators, home appliances (including refrigerators and washing machines), IT services, lighting, materials and electronic components, medical equipment (including CT and MRI scanners, ultrasound equipment and X-ray equipment), office equipment, business telecommunication equipment personal computers, semiconductors, power systems (including electricity turbines, fuel cells and nuclear reactors) power transmission and distribution systems, and TFT displays.



In October 2010, Toshiba unveiled the Toshiba Regza GL1 21" LED backlit LCD TV glasses-free 3D prototype at CEATEC 2010. This system supports 3D capability without glasses (utilising an integral imaging system of 9 parallax images with vertical lenticular sheet). The retail product was released in December 2010.



4K Ultra HD (3840x2160p) TVs provides four times the resolution of 1080p Full HD TVs resulting in higher quality resolution. Toshiba s 4K HD LED TVs are powered by a CEVO 4K Quad + Dual Core Processor.




On February 19, 2008, Toshiba announced that would be discontinuing its HD DVD storage format following defeat in a format 'war' against Blu-ray. The HD DVD format had failed after most of the major US film studios backed the Blu-ray format, which was developed by Sony, Panasonic, Philips, Pioneer and Toshiba's President, Atsutoshi Nishida. "We concluded that a swift decision would be best [and] if we had continued, that would have created problems for consumers, and we simply had no chance to win".
Toshiba continued to supply retailers with machines until the end of March 2008, and continued to provide technical support to the estimated one million people worldwide who owned HD DVD players and recorders. Toshiba announced a new line of stand-alone Blu-ray players as well as drives for PCs and laptops, and subsequently joined the BDA, the industry body which oversees development of the Blu-ray format.



REGZA (Real Expression Guaranteed by Amazing Architecture) is a unified television brand owned and manufactured by Toshiba. REGZA name was eliminated in 2010 for the North American market citing switch in television production to Compal Electronics.
REGZA is also used in Android-based smartphones that were developed by Fujitsu Toshiba Mobile Communications.



In October 2014, Toshiba released the Chromebook 2, a new version with a thinner profile and a much-improved display. The Chromebook runs exclusively on the Chrome operating system and gives users free Google Drive storage and access to a collection of apps and extensions at the Chrome Web Store.



In March 2015, Toshiba announced the development of the first 48-layer, three-dimensional flash memory. The new flash memory is based on a vertical stacking technology that Toshiba calls BiCS (Bit Cost Scaling), stores two bits of data per transistor and can store 128Gbits (16GB) per chip.



Toshiba has been judged as making 'low' efforts to lessen their impact on the environment. In November 2012, they came second from the bottom in Greenpeace s 18th edition of the Guide to Greener Electronics that ranks electronics companies according to their policies on products, energy and sustainable operations. Toshiba received 2.3 of a possible 10 points, with the top company (WIPRO) receiving 7.1 points. 'Zero' scores were received in the categories 'Clean energy policy advocacy,' 'Use of recycled plastics in products' and 'Policy and practice on sustainable sourcing of fibres for paper.'
In 2010, Toshiba reported that all of its new LCD TVs comply with the Energy Star standards and 34 models exceed the requirements by 30% or more.
Toshiba also partnered with China s Tsinghua University in 2008 in order to form a research facility to focus on energy conservation and the environment. The new Toshiba Energy and Environment Research Center is located in Beijing where forty students from the university will work to research electric power equipment and new technologies that will help stop the global warming process. Through this partnership, Toshiba hopes to develop products that will better protect the environment and save China. This contract between Tsinghua University and Toshiba originally began in October 2007 when they signed an agreement on joint energy and environment research. The projects that they conduct work to reduce car pollution and to create power systems that don t negatively affect the environment.
On December 28, 1970 Toshiba began the construction of unit 3 of the Fukushima Daiichi Nuclear Power Plant which was damaged in the Fukushima I nuclear accidents on March 14, 2011. In April 2011 CEO Norio Sasaki declared nuclear energy would "remain as a strong option" even after the Fukushima I nuclear accidents.
In late 2013, Toshiba (Japan) entered the solar power business in Germany, installing PV systems on apartment buildings.


