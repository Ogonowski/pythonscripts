England Air Force Base is a former United States Air Force base in Louisiana, located 5 miles (8.0 km) Northwest of Alexandria and about 170 miles (270 km) Northwest of New Orleans. Originally known as Alexandria Army Air Base, on 23 June 1955 the facility was renamed England Air Force Base in honor of Lt Col John Brooke England (1923 1954).
The base was closed in 1992. The airfield and buildings are now Alexandria International Airport.



The base was opened on 21 October 1942 and during the Second World War performed Boeing B-17 Flying Fortress aircrew training. It was placed on inactive status on September 23, 1946, although a small cadre of Army and Air Force personnel (331 Composite Squadron) remained assigned to Alexandria Municipal Airport throughout the late 1940s.

On 10 October 1950, the United States Air Force reopened Alexandria Air Force Base during the Korean War. The station's primary mission was tactical fighter operations for Tactical Air Command.
Its major operating units were:
United States Army Air Forces
67th Airdrome Squadron, 1 February 1943   25 March 1943
471st Bombardment Group, 1 May 1943   7 May 1943
469th Bombardment Group, 7 May 1943   1 April 1944
221st AAF Base Unit, 25 March 1944   1 March 1945
329th AAF Base Unit, 1 March 1945   23 September 1946
United States Air Force
4416th Base Complement Sq, 23 October 1950   22 December 1950
137th Fighter-Bomber Wing, 22 December 1950   2 May 1952
Composite Air National Guard wing activated to Federal Service due to Korean War

Flew Republic F-84 "Thunderjets". Three operational squadrons 125th (Oklahoma ANG, F-84B/G, 127th (Kansas ANG F-84C/G), 128th (Georgia ANG, F-84B/G)). ANG personnel trained at Alexandria AFB 1950/51, being replaced by active duty Air Force personnel in 1951/52. Wing reassigned to USAFE at Chaumont-Semoutiers Air Base, France

132d Fighter-Bomber Wing, 2 May 1952   1 January 1953
Iowa ANG Wing activated to Federal Service replacing reassigned 137th FBW

One operational squadron (124th FIS). Flew Republic straight winged F-84s. Returned to state control.(F-84Fs were not assigned to front line units until January 1954-please see pages 42/3 in POST WORLD WAR TWO FIGHTERS 1945-73 U S Government Printing Office)

366th Fighter-Bomber Wing, 1 January 1953   1 April 1959
Active duty Air Force wing activated at Alexandria AFB. Three operational squadrons (389th, 390th, 391st) being initially equipped with North American F-51D Mustang, reequipped with North American F-86F "Sabre" jet aircraft by end of 1953.

Wing became first TAC unit to perform six-month TDY rotations with NATO at Aviano AB, Italy, with rotations continuing to support NATO in France and Italy. Redesignated 366th Tactical Fighter Wing 1 July 1958. Inactivated 1 April 1959 as part of a general budgetary reduction of USAF wings. Aircraft transferred to Air National Guard.

420th Air Refueling Squadron (Tactical), 18 March 1954   4 October 1955
(Activated at England AFB, equipped with Boeing KB-29P.

First unit in TAC of its type. Provided TAC with independent air refueling capability. Reassigned to USAFE at RAF Sculthorpe, England.

622d Air Refueling Squadron (Tactical), 18 July 1955   1 April 1964
Activated at England AFB, assigned to 4505th Air Refueling Wing, Langley AFB, Virginia 1 July 1958.

Equipped with Boeing KB-29P (1955 57), later turbojet-augmented Boeing KB-50J (1957 64) aircraft. Aircraft became obsolescent and unit inactivated 1 April 1964.

401st Tactical Fighter Wing, 1 April 1959   27 April 1966
According to AIR FORCE COMBAT WINGS 1947-1977(GPO) pages 194/5 and 214, the 366th FBW had two FB Groups under it: 366th & 401st. The 366th WING became a TFW in July 1958 and inactivated in April 1959. Meanwhile, the 401st FBG upgraded to a FBW in September 1957 and remained in place with four squadrons of F-100D/Fs. The aircraft and most of the personnel of the 366th Wing were transferred to other units in TAC and overseas. There was no redesignations of any 366th squadrons to 401st squadrons.

Four tactical fighter squadrons (612th, 613th, 614th, 615th) redesignated from former 366th TFW units. Participated in numerous firepower demonstrations, tactical exercises and maneuvers in the United States and overseas, and deployed its tactical squadrons to bases in Europe and the Middle East in support of NATO. Deployed to Homestead AFB, Florida during Cuban Missile Crisis (1962) as tactical airstrike unit. Wing designation along with 613th TFS reassigned to USAFE at Torrejon Air Base, Spain.

3d Tactical Fighter Wing, 9 January 1964   7 November 1965
Reassigned from Yokota AB, Japan, Equipped with F-100 Super Sabres after arrival at England AFB.

Four tactical fighter squadrons (90th , 416th, 510th, 531st)., with the 510th coming from Clark AB, RP and the 416th and 531st moving from Misawa AB Japan, each unit with about 25 F-100D/Fs, then trained and rotated its squadrons in detached status to Southeast Asia for combat duty. Reassigned to Bien Hoa AB, South Vietnam. The 3rd Wing number transferred from Yokota AB Japan along with one squadron number, the 90th "Pair-O-Dice", in the spring of 1964, so the wing had four squadrons assigned.

834th Air Division, 1 July 1964   15 October 1966
Assumed command and control over 3d and 401st Tactical Fighter Wings.

After reassignment of wings, assumed remaining organization and operational squadrons of former 401st TFW. Deployed to South Vietnamese bases (834th AD to Tan Son Nhut AB), (612th, 614th, 615th) TFS to Phan Rang AB, Phu Cat AB).

1st Air Commando/Special Operations Wing 15 January 1966   15 July 1969
317th, 319th, 603rd Air Commando/Special Operations Squadrons

Reassigned from Hurlburt Field Florida. Redesignated 1st SOW 8 July 1968. Reassigned to Hurlburt Field, Florida.

4410th Combat Crew Training Wing/Group 15 July 1969   31 July 1973
6th Special Operations Training Squadron, 427th, 4412th, 4532d Combat Crew Training Squadrons

Wing reassigned from Hurlburt Field Florida. Redesignated as Group, 15 September 1970. Inactivated July 1973 due to US withdrawal from South Vietnam. Upon group inactivation, 6th SOTS reassigned to 1st SOW at Hurlburt Field but remained at England AFB until inactivation, January 1974.

On 15 September 1970, the 4403d Tactical Fighter Wing, a temporary Major Air Command-Controlled (MAJCOM) unit, activated at England. With its subordinate 416th (activated 28 September 1970) and 431st Tactical Fighter Squadrons, it absorbed returning North American F-100 Super Sabre aircraft of 31st TFW from Tuy Hoa Air Base South Vietnam. It transferred aircraft and other assets to the Air National Guard. Acted as holding unit for arriving LTV A-7D Corsair II aircraft starting April 1972. The 4403d TFW and the 416th TFS both inactivated on 1 July 1972.
On 1 July 1972, the 23d Tactical Fighter Wing reactivated at the base, without personnel or equipment, and took over the organization and assets of the temporary 4403d TFW. Three A-7D tactical fighter (74th, 75th, 76th) squadrons formed. The wing re-equipped with Fairchild Republic A-10 Thunderbolt II aircraft in 1981. Redesignated 23d Fighter Wing 1 October 1991. Inactivated 1 June 1992.



Alexandria (and later England) AFB was base for Air Defense Command interceptor and radar units along the Gulf Coast. In 1959, the 332d Fighter-Interceptor Squadron, assigned to the 32d Air Division stationed F-102 Delta Dagger interceptors at the base between 1959-1960.
In addition to the fighter-interceptor squadron, England AFB part of the planned deployment by Air Defense Command of forty-four Mobile radar stations across the United States to support the permanent Radar network established during the Cold War for air defense of the United States. This deployment had been projected to be operational by mid-1952. Funding, constant site changes, construction, and equipment delivery delayed deployment.
On 1 November 1954 the 653d Aircraft Control and Warning Squadron began operations at Alexandria AFB with the activation of AN/MPS-14, AN/TPS-1D, and AN/TPS-10D radars. The site was designated as M-125, and initially the station functioned as a Ground-Control Intercept (GCI) and warning station. As a GCI station, the squadron's role was to guide interceptor aircraft toward unidentified intruders picked up on the unit's radar scopes. The AN/MPS-14 continued to operate until site closure in 1963. In 1958 the site was operating an AN/FPS-20 search set.
In addition to the main facility, Air Defense Command operated three AN/FPS-18 Gap Filler sites:
Delhi, LA (M-125A): 32 19 40 N 091 33 25 W
Weeks Island, LA (M-125C): 29 48 34 N 091 48 22 W
Lake Charles GFA, LA (M-125D): 30 11 05 N 093 10 30 W
England AFB was a planned Semi Automatic Ground Environment (SAGE) Data Center (DC) location, for the also-planned Shreveport Air-Defense Sector. However, in March 1963 Air Defense Command ordered the site to close due to budget reductions and operations ceased on April 23.
The station was picked up by the Federal Aviation Administration (FAA) after the ADC shut down operations. The site is still in operations using the FPS-20A, being known as Alexandria, Louisiana. Virtually all other parts of this old Air Force radar station have been removed. A golf course now occupies part of the old M-125 site.



In October 1990, the 1991 Base Realignment and Closure Commission decided that England Air Force Base would be closed by September 1992. Reduction of equipment and personnel began almost immediately. The 23d Fighter Wing's Fairchild A-10 "Thunderbolt II" aircraft were sent to other units, and the base was closed on 15 December 1992. During the period of its military use, Air Force units from England Air Force Base served in combat in World War II, the Vietnam War and Operation Desert Storm.




Louisiana World War II Army Airfields
List of USAF Aerospace Defense Command General Surveillance Radar Stations



 This article incorporates public domain material from websites or documents of the Air Force Historical Research Agency.



Ravenstein, Charles A. (1984). Air Force Combat Wings Lineage and Honors Histories 1947 1977. Maxwell AFB, Alabama: Office of Air Force History. ISBN 0-912799-12-9.
Mueller, Robert (1989). Active Air Force Bases Within the United States of America on 17 September 1982. USAF Reference Series, Maxwell AFB, Alabama: Office of Air Force History. ISBN 0-912799-53-6
Endicott, Judy G. (1999) Active Air Force wings as of 1 October 1995; USAF active flying, space, and missile squadrons as of 1 October 1995. Maxwell AFB, Alabama: Office of Air Force History. CD-ROM.
Martin, Patrick (1994). Tail Code: The Complete History of USAF Tactical Aircraft Tail Code Markings. Schiffer Military Aviation History. ISBN 0-88740-513-4.
Menard David R. (1998) Republic F-84: Thunderjet, Thunderstreak, & Thunderflash : A Photo Chronicle Schiffer Publishing ISBN 0-7643-0444-5
Rogers, Brian (2005). United States Air Force Unit Designations Since 1978. Hinkley, England: Midland Publications. ISBN 1-85780-197-0.
USAAS-USAAC-USAAF-USAF Aircraft Serial Numbers 1908 to present
A Handbook of Aerospace Defense Organization 1946 - 1980, by Lloyd H. Cornett and Mildred W. Johnson, Office of History, Aerospace Defense Center, Peterson Air Force Base, Colorado
Winkler, David F. (1997), Searching the skies: the legacy of the United States Cold War defense radar program. Prepared for United States Air Force Headquarters Air Combat Command.
England AFB, LA



Memories of England Air Force Base : Closed, But not Forgotten.
The Alexandria Retrospective : England AFB photos, newspaper clippings, and more.