Systems Network Architecture (SNA) is IBM's proprietary networking architecture, created in 1974. It is a complete protocol stack for interconnecting computers and their resources. SNA describes formats and protocols and is, in itself, not a piece of software. The implementation of SNA takes the form of various communications packages, most notably Virtual Telecommunications Access Method (VTAM), the mainframe software package for SNA communications.



SNA was made public as part of IBM's "Advanced Function for Communications" announcement in September, 1974, which included the implementation of the SNA/SDLC (Synchronous Data Link Control) protocols on new communications products:
IBM 3767 communication terminal (printer)
IBM 3770 data communication system
They were supported by IBM 3704/3705 communication controllers and their Network Control Program, and by System/370 and their VTAM and other software such as CICS and IMS. This announcement was followed by another announcement in July, 1975, which introduced the IBM 3760 data entry station, the IBM 3790 communication system, and the new models of the IBM 3270 display system.
SNA was mainly designed by the IBM Systems Development Division laboratory in Research Triangle Park, North Carolina, USA, helped by other laboratories that implemented SNA/SDLC. The details were later made public by IBM's System Reference Library manuals and IBM Systems Journal.
SNA is still used extensively in banks and other financial transaction networks, as well as in many government agencies. While IBM is still providing support for SNA, one of the primary pieces of hardware, the 3745/3746 communications controller, has been withdrawn from the market by IBM. There are an estimated 20,000 of these controllers installed however, and IBM continues to provide hardware maintenance service and microcode features to support users. A robust market of smaller companies continues to provide the 3745/3746, features, parts and service. VTAM is also supported by IBM, as is the IBM Network Control Program (NCP) required by the 3745/3746 controllers.
In 2008 an IBM publication said:

with the popularity and growth of TCP/IP, SNA is changing from being a true network architecture to being what could be termed an "application and application access architecture." In other words, there are many applications that still need to communicate in SNA, but the required SNA protocols are carried over the network by IP.



IBM in the mid-1970s saw itself mainly as a hardware vendor and hence all its innovations in that period aimed to increase hardware sales. SNA's objective was to reduce the costs of operating large numbers of terminals and thus induce customers to develop or expand interactive terminal-based systems as opposed to batch systems. An expansion of interactive terminal-based systems would increase sales of terminals and more importantly of mainframe computers and peripherals - partly because of the simple increase in the volume of work done by the systems and partly because interactive processing requires more computing power per transaction than batch processing.
Hence SNA aimed to reduce the main non-computer costs and other difficulties in operating large networks using earlier communications protocols. The difficulties included:
Often a communications line could not be shared by terminals of different types, as they used different "dialects" of the existing communications protocols. Up to the early 1970s, computer components were so expensive and bulky that it was not feasible to include all-purpose communications interface cards in terminals. Every type of terminal had a hard-wired communications card which supported only the operation of one type of terminal without compatibility with other types of terminals on the same line.
The protocols which the primitive communications cards could handle were not efficient. Each communications line used more time transmitting data than modern lines do.
Telecommunications lines at the time were of much lower quality. For example, it was almost impossible to run a dial-up line at more than 19,200 bits per second because of the overwhelming error rate, as comparing with 56,000 bits per second today on dial-up lines; and in the early 1970s few leased lines were run at more than 2400 bits per second (these low speeds are a consequence of Shannon's Law in a relatively low-technology environment). Telecommunications companies had little incentive to improve line quality or reduce costs, because at the time they were mostly monopolies and sometimes state-owned.
As a result, running a large number of terminals required a lot more communications lines than the number required today, especially if different types of terminals needed to be supported, or the users wanted to use different types of applications (.e.g. under CICS or TSO) from the same location. In purely financial terms SNA's objectives were to increase customers' spending on terminal-based systems and at the same time to increase IBM's share of that spending, mainly at the expense of the telecommunications companies.
SNA also aimed to overcome a limitation of the architecture which IBM's System/370 mainframes inherited from System/360. Each CPU could connect to at most 16 I/O channels and each channel could handle up to 256 peripherals - i.e. there was a maximum of 4096 peripherals per CPU. At the time when SNA was designed, each communications line counted as a peripheral. Thus the number of terminals with which powerful mainframes could otherwise communicate was limited.



Improvements in computer component technology made it feasible to build terminals that included more powerful communications cards which could operate a single standard communications protocol rather than a very stripped-down protocol which suited only a specific type of terminal. As a result, several multi-layer communications protocols were proposed in the 1970s, of which IBM's SNA and ITU-T's X.25 became dominant later.
The most important elements of SNA include:
IBM Network Control Program (NCP) is a communications program running on the 3705 and subsequent 37xx communications processors that, among other things, implements the packet switching protocol defined by SNA. The protocol performed two main functions:
It is a packet forwarding protocol, acting like modern switch - forwarding data packages to the next node, which might be a mainframe, a terminal or another 3705. The communications processors supported only hierarchical networks with a mainframe at the center, unlike modern routers which support peer-to-peer networks in which a machine at the end of the line can be both a client and a server at the same time.
It is a multiplexer that connected multiple terminals into one communication line to the CPU, thus relieved the constraints on the maximum number of communication lines per CPU. A 3705 could support a larger number of lines (352 initially) but only counted as one peripheral by the CPUs and channels. Since the launch of SNA IBM has introduced improved communications processors, of which the latest is the 3745.

Synchronous Data Link Control (SDLC), a protocol which greatly improved the efficiency of data transfer over a single link:SDLC included much more powerful error detection and correction codes than earlier protocols. These codes often enabled the communications cards to correct minor transmission errors without requesting re-transmission, and therefore made it possible to pump data down a line much faster.
It enabled terminals and 3705 communications processors to send "frames" of data one after the other without waiting for an acknowledgement of the previous frame - the communications cards had sufficient memory and processing capacity to "remember" the last 7 frames sent or received, request re-transmission of only those frames which contained errors that the error detection and correction codes could not repair, and slot the re-transmitted frames into the right place in the sequence before forwarding them to the next stage.
These frames all had the same type of "envelope" (frame header and trailer) which contained enough information for data packages from different types of terminal to be sent along the same communications line, leaving the mainframe to deal with any differences in the formatting of the content or in the rules governing dialogs with different types of terminal.

Remote terminals (i.e. those connected to the mainframe by telephone lines) and 3705 communications processors would have SDLC-capable communications cards.
This is the precursor of the so called "packet communication" that eventually evolved into today's TCP/IP technology. SDLC itself evolved into HDLC, one of the base technologies for dedicated telecommunication circuits.
VTAM, a software package to provide log-in, session keeping and routing services within the mainframe. A terminal user would log-in via VTAM to a specific application or application environment (e.g. CICS or TSO). A VTAM device would then route data from that terminal to the appropriate application or application environment until the user logged out and possibly logged into another application. The original versions of IBM hardware could only keep one session per terminal. In the 1980s further software (mainly from third-party vendors) made it possible for a terminal to have simultaneous sessions with different applications or application environments.



SNA removed link control from the application program and placed it in the NCP. This had the following advantages and disadvantages:



Localization of problems in the telecommunications network was easier because a relatively small amount of software actually dealt with communication links. There was a single error reporting system.
Adding communication capability to an application program was much easier because the formidable area of link control software that typically requires interrupt processors and software timers was relegated to system software and NCP.
With the advent of APPN, routing functionality was the responsibility of the computer as opposed to the router (as with TCP/IP networks). Each computer maintained a list of Nodes that defined the forwarding mechanisms. A centralized node type known as a Network Node maintained Global tables of all other node types. APPN stopped the need to maintain APPC routing tables that explicitly defined endpoint to endpoint connectivity. APPN sessions would route to endpoints through other allowed node types until it found the destination. This is similar to the way that routers for the Internet Protocol and the Netware Internetwork Packet Exchange protocol function.



Connection to non-SNA networks was difficult. An application which needed access to some communication scheme, which was not supported in the current version of SNA, faced obstacles. Before IBM included X.25 support (NPSI) in SNA, connecting to an X.25 network would have been awkward. Conversion between X.25 and SNA protocols could have been provided either by NCP software modifications or by an external protocol converter.
A sheaf of alternate pathways between every pair of nodes in a network had to be predesigned and stored centrally. Choice among these pathways by SNA was rigid and did not take advantage of current link loads for optimum speed.
SNA network installation and maintenance are complicated and SNA network products are (or were) expensive. Attempts to reduce SNA network complexity by adding IBM Advanced Peer-to-Peer Networking functionality were not really successful, if only because the migration from traditional SNA to SNA/APPN was very complex, without providing much additional value, at least initially. SNA software licences (VTAM) cost as much as $10000 a month for high-end systems. And SNA IBM 3745 Communications Controllers typically cost over $100K. TCP/IP was still seen as unfit for commercial applications e.g. in the finance industry until the late 1980s, but rapidly took over in the 1990s due to its peer-to-peer networking and packet communication technology.
The design of SNA was in the era when the concept of layered communication was not fully adopted by the computer industry. Applications, databases and communication functions were mingled into the same protocol or product, which made it difficult to maintain and manage. That was very common for the products created in that time. Even after TCP/IP was fully developed, the X Window System was designed with the same model where communication protocols were embedded into graphic display application.
SNA's connection based architecture invoked huge state machine logic to keep track of everything. APPN added a new dimension to state logic with its concept of differing node types. While it was solid when everything was running correctly, there was still a need for manual intervention. Simple things like watching the Control Point sessions had to be done manually. APPN wasn't without issues; in the early days many shops abandoned it due to issues found in APPN support. Over time, however, many of the issues were worked out but not before TCP/IP became increasingly popular in the early 1990s, which marked the beginning of the end for SNA.



Network Addressable Units in a SNA network are any components that can be assigned an address and can send and receive information. They are distinguished further as follows:
a System Services Control Point (SSCP) provides resource management and other session services (such as directory services) for users in a subarea network;
a Physical Unit is a combination of hardware and software components that control the links to other nodes.
a Logical Unit acts as the intermediary between the user and the network.



SNA essentially offers transparent communication: equipment specifics that do not impose any constraints onto LU-LU communication. But eventually it serves a purpose to make a distinction between LU types, as the application must take the functionality of the terminal equipment into account (e.g. screen sizes and layout).
Within SNA there are three types of data stream to connect local display terminals and printers; there is SNA Character String (SCS), used for LU1 terminals and for logging on to an SNA network with Unformatted System Services (USS), there is the 3270 data stream mainly used by mainframes such as the System/370 and successors, including the zSeries family, and the 5250 data stream mainly used by minicomputers/servers such as the System/34, System/36, System/38, and AS/400 and its successors, including System i and IBM Power Systems running IBM i.
SNA defines several kinds of devices, called Logical Unit types:
LU0 provides for undefined devices, or build your own protocol. This is also used for non-SNA 3270 devices supported by TCAM or VTAM.
LU1 devices are printers or combinations of keyboards and printers.
LU2 devices are IBM 3270 display terminals.
LU3 devices are printers using 3270 protocols.
LU4 devices are batch terminals.
LU5 has never been defined.
LU6 provides for protocols between two applications.
LU7 provides for sessions with IBM 5250 terminals.
The primary ones in use are LU1, LU2, and LU6.2 (an advanced protocol for application to application conversations).



PU1 nodes are terminal controllers such as IBM 6670 or IBM 3767
PU2 nodes are cluster controllers running configuration support programs such as IBM 3174, IBM 3274, or the IBM 4701 or IBM 4702 Branch Controller
PU2.1 nodes are peer-to-peer (APPN) nodes
PU3 was never defined
PU4 nodes are front-end processors running the Network Control Program (NCP) such as the IBM 37xx series
PU5 nodes are host computer systems
The term 37xx refers to IBM's family of SNA communications controllers. The 3745 supports up to eight high-speed T1 circuits, the 3725 is a large-scale node and front-end processor for a host, and the 3720 is a remote node that functions as a concentrator and router.



As mainframe-based entities looked for alternatives to their 37XX-based networks, IBM partnered with Cisco in the mid-1990s and together they developed Data Link Switching, or DLSw. DLSw encapsulates SNA packets into IP datagrams, allowing sessions to flow over an IP network. The actual encapsulation and decapsulation takes place in Cisco routers at each end of a DLSw peer connection. At the local, or mainframe site, the router uses Token Ring topology to connect natively to VTAM. At the remote (user) end of the connection, a PU type 2 emulator (such as an SNA gateway server) connects to the peer router via the router's LAN interface. End user terminals are typically PCs with 3270 emulation software that is defined to the SNA gateway. The VTAM/NCP PU type 2 definition becomes a Switched Major Node that can be local to VTAM (without an NCP), and a "Line" connection can be defined using various possible solutions (such as a Token Ring interface on the 3745, a 3172 Lan Channel Station, or a Cisco ESCON-compatible Channel Interface Processor).



The proprietary networking architecture for Honeywell Bull mainframes is Distributed Systems Architecture (DSA). Communications package for DSA is VIP. Like SNA, DSA is also no longer supported for client access. Bull mainframes are fitted with Mainway for translating DSA to TCP/IP and VIP devices are replaced by TNVIP Terminal Emulations (GLink, Winsurf). GCOS 8 supports TNVIP SE over TCP/IP. Other architectures are Unisys   formerly Univac   Distributed Computing Architecture (DCA) and   formerly Burroughs   Burroughs Network Architecture (BNA), both largely obsolete by 2012, and the International Computers Limited (ICL) Information Processing Architecture (IPA). DECnet is a suite of network protocols created by Digital Equipment Corporation, originally released in 1975 to connect two PDP-11 minicomputers. It evolved into one of the first peer-to-peer network architectures, thus transforming DEC into a networking powerhouse in the 1980s.
SNA initially aimed at competing with ISO's Open Systems Interconnection, which was an attempt to create a vendor-neutral network architecture that failed due to the problems of "design by committee". OSI systems are very complex, and the many parties involved required extensive flexibilities that hurt the interoperability of OSI systems, which was the prime objective to start with.
After TCP/IP for many years was not considered a serious alternative due to its "nerdy" image, in the 1990s it was discovered as a very attractive alternative: a truly vendor-neutral alternative that fosters true interoperability due to its flexible RFC process of defining standards. TCP/IP is elegant and simple, in contrast to the former network architectures. IBM SNA "APPN", a later development, was in particular extremely complex, ironically with the purpose of making life simpler.
TN3270 (Telnet 3270) is a TCP/IP Telnet variant that supports direct client-server connections to the mainframe using a TN3270 server on the mainframe, and a TN3270 emulation package on the PC at the end user site. This protocol allows existing VTAM applications (CICS, TSO) to run with little or no change from traditional SNA by supportng traditional 3270 terminal protocol over the TCP/IP session. This protocol is widely used to replace legacy SNA connectivity more than DLSw and other SNA replacement technologies.




Network Data Mover
TN3270






Friend, George E.; John L. Fike, H. Charles Baker, John C. Bellamy (1988). Understanding Data Communications (2nd ed.). Indianapolis: Howard W. Sams & Company. ISBN 0-672-27270-9.  
Pooch, Udo W.; William H. Greene, Gary G. Moss (1983). Telecommunications and Networking. Boston: Little, Brown and Company. ISBN 0-316-71498-4.  
Schatt, Stan (1991). Linking LANs: A Micro Manager's Guide. McGraw-Hill. ISBN 0-8306-3755-9. 
Systems Network Architecture General Information (PDF). First Edition. IBM. January 1975. GA27-3102-0. 
Systems Network Architecture Concepts and Products (PDF). Second Edition. IBM. February 1984. GC30-3072-1. 
Systems Network Architecture Technical Overview. Fifth Edition. IBM. January 1994. GC30-3073-04. 
Systems Network Architecture Guide to SNA Publications. Third Edition. IBM. July 1994. GC30-3438-02. 



Cisco article on SNA
APPN Implementers Workshop Architecture Document repository
SNA protocols quite technical
Related whitepapers sdsusa.com
Advanced Function for Communications System Summary (PDF). Second Edition. IBM. July 1975. GA27-3099-1. Retrieved May 22, 2014. 
Systems Network Architecture Formats. Twenty-first Edition. IBM. March 2004. GA27-3136-20. 
Systems Network Architecture - Sessions Between Logical Units (PDF). Third Edition. IBM. April 1981. GC20-1868-2. 
Systems Network Architecture - Introduction to Sessions between Logical Units (PDF). Third Edition. IBM. December 1979. GC20-1869-2. 
Systems Network Architecture: Transaction Programmer's Reference Manual for LU Type 6.2. Sixth Edition. IBM. June 1993. GC30-3084-05. 
Systems Network Architecture Type 2.1 Node Reference. Fifth Edition. IBM. December 1996. SC30-3422-04. 
Systems Network Architecture LU 6.2 Reference: Peer Protocols. Third Edition. IBM. October 1996. SC31-6808-02.