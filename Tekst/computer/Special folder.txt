On Microsoft Windows, a special folder is a folder which is presented to the user through an interface as an abstract concept instead of an absolute folder path. (The synonymous term shell folder is sometimes used instead.) Special folders make it possible for any application to ask the operating system where an appropriate location for certain kinds of files can be found, regardless of what version or language of Windows is being used.




Windows uses the concept of special folders to present the contents of the storage devices connected to the computer in a fairly consistent way that frees the user from having to deal with absolute file paths, which can (and often do) change between operating system versions, and even individual installations. The idea has evolved over time, with new special folders being added in every version of Windows since their introduction in Windows 95.
Microsoft's "Compatible with Windows" software logo requires that an application make use of special folders locations to locate the appropriate folders in which documents and application settings should be stored.
A special folder can either be a reference to a physical file system directory, or a reference to a "virtual" folder. In the former case, they are analogous to environment variables   in fact, many of the environment variables that are set in a user's session are defined by where the special folders are set to point to.
Virtual folders, however, do not actually exist on the file system; they are instead presented through Windows Explorer as a tree of folders that the user can navigate. This is known as the Shell namespace. On Windows XP systems, the root of this namespace is the Desktop virtual folder, which contains the My Documents, My Computer, My Network Places (Network Neighborhood in Windows 95 and 98) and Recycle Bin virtual folders. Some virtual folders (like Desktop) have an accompanying special folder that is a reference to a directory on the physical file system. Windows Explorer displays the combined contents of a virtual folder and its associated file system folder to the user. This can be seen in Figure 1, which shows the Folder view in Windows XP's Explorer; in the Desktop virtual folder, the four standard virtual folders can be seen, as well as an additional folder, "a folder on the desktop", which is a real folder located in the Desktop directory in the user's profile.
Some third-party programs add their own virtual folders to Windows Explorer.



The following tables list most of the file system and virtual folders that are available as of Windows Vista. The OS version in which each special folder was introduced is given as well. Items like %USERPROFILE% and %windir% are Windows environment variables.



Notes:
The "Desktop" virtual folder is not the same thing as the "Desktop" special folder. The Desktop virtual folder is the root of the Windows Shell namespace, which contains other virtual folders.
"Local Application Data" differs from "Application Data" in that files located in the "Local" variant are also intended to be specific to the machine it is on. This only has relevance if the user's profile is a Roaming Profile in a Windows Server domain environment.
As with Desktop, the "My Documents" virtual folder differs from the "My Documents" special folder. If the virtual folder variant is asked for, it will appear in a file dialog as a sub-directory of the "Desktop" virtual folder, instead of the user's profile directory as it physically exists on the hard drive.
If the "My Documents" folder is moved (e.g., to a network drive), attempting to access it via the shell variable will go to the original, default location.



Notes:
The "Desktop" virtual folder is not the same thing as the "Desktop" special folder. The Desktop virtual folder is the root of the Windows Shell namespace, which contains other virtual folders.
As with Desktop, the "My Documents" virtual folder differs from the "My Documents" special folder. If the virtual folder variant is asked for, it will appear in a file dialog as a sub-directory of the "Desktop" virtual folder, instead of the user's profile directory as it logically exists on the hard drive.



Microsoft Windows
Windows Explorer
Folder redirection
Tweak UI
Unix directory structure   similar standard for Unix
Filesystem Hierarchy Standard   similar standard for Linux



^ Windows Software Logo Program
^ 32-bit executables have this directory mapped as %windir%\System32.
^ The use of %windir%\System32 for 64-bit libraries and executables in a 64-bit Windows OS is intended to not break existing programs that are recompiled without modifications as a 64-bit executable.
^ a b The Shell Namespace
^ Chen, Raymond (2006). "Taxes". The Old New Thing (1st ed.). Pearson Education. p. 451. ISBN 0-321-44030-7. 
^ Paul Thurrott's SuperSite for Windows: Windows 2000 Professional Beta 3 Reviewed



Shell Programmer's Guide - The Shell Namespace   MSDN documentation on Shell namespaces
Environment.SpecialFolder Enumeration   MSDN documentation on accessing special folder values in the .NET Framework on Windows 7 and Windows Server 2008 R2 etc.
CSIDL Values   MSDN documentation containing a complete list of all available special folders and virtual folders
Known Folders   MSDN documentation describing known folders on Vista (replaces CSIDL)
KNOWNFOLDERID Values   MSDN documentation containing a complete list of all known folders (on Vista, replaces CSIDL)
Microsoft PowerToys for Windows XP   TweakUI is available for download on this page
Utility that shows the path of all special folders