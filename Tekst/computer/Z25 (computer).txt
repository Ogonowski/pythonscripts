The Zuse Z25 was a program-controlled electronic computer on the basis of transistors developed by Zuse KG in Bad Hersfeld. It was a "short word" computer with a "single address" instruction set. The word length was 18 bit. The processing of double word lengths was possible (decimal numbers with 10 digits). 32768 addresses could be dialed. On the one hand the computer had a fixed wired memory for standard programs. As main memory a magnetic core memory was used. The maximum size of this memory was 16383 words. A free programmable program memory had a maximum size of 4096 words.
The computer could carry approximately 7100 arithmetic operations per second at a clock rate of 180 kHz. Several Z25 could be connected to a network and the Z25 could be used for the control and data acquisition of external devices by a program interrupt system with up to 32 channels.
In- and output was done by teleprinter, punched tape and punched cards, output also by a type-printer. As mass storage was a drum memory available as well as a magnetic tape memory. The magnetic drum had a storage capacity of 17664 Z25 words. The transmission speed was 6900 words per second. The magnetic tape memory had a capacity of 1 million Z25 words and a transmission speed of approximately 33000 Z25 words per second. The computer was available at the year 1963.



^ Zuse, Konrad (1993). The Computer - My Life. Springer. pp. 140 141. ISBN 978-3-540-56453-9. 
^ a b c Zuse KG: Einf hrung in die Arbeitsweise der Zentraleinheit des Datenverarbeitungssytems Zuse Z 25 (Ausgabe April 1963).
^ Zuse KG: Zuse Z 25 Standard-Grundprogramm Programmieranleitung (Ausgabe Dezember 1963).
^ Zuse Forum 10-1965, p. 34.



"Z25 Data Sheet". Horst Zuse. Retrieved 9 February 2015.