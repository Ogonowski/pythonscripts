QuickBooks is an accounting software package developed and marketed by Intuit. Quickbooks products are geared mainly toward small and medium-sized businesses and offer on-premises accounting applications as well as cloud based versions that accept business payments, manage and pay bills, and payroll functions.



Intuit was founded in 1983 by Scott Cook and Tom Proulx in Mountain View, California, USA. After the success of Quicken for individual financial management, the company developed a similar solution for small business owners.



The initial Quicken software did not function as a "double-entry" accounting package. On September 27, 1994 Intuit purchased the right to use the double-entry accounting program called MoneyCounts developed by Bob Parsons' company Parsons Technology. This new integration of the programs was then named QuickBooks. The software was popular among small business owners who had no formal accounting training. As such, the software soon claimed up to 85 percent of the small business accounting software market. It continues to command the vast majority of this market. Professional accountants, however, were not satisfied with early versions of the system, citing poor security controls such as no audit trail, as well as non-conformity with traditional accounting standards.



Intuit sought to bridge the gap with these accounting professionals, eventually providing full audit trail capabilities, double-entry accounting functions and increased functions. By 2000, Intuit had developed Basic and Pro versions of the software and, in 2003, started offering industry-specific versions, with workflow processes and reports designed for each of these business types along with terminology associated with the trades.
Options now include versions for manufacturers, wholesalers, professional service firms, contractors, non-profit entities and retailers, in addition to one specifically designed for professional accounting firms who service multiple small business clients. In May 2002 Intuit launched QuickBooks Enterprise Solutions for medium-sized businesses.
In September 2005, QuickBooks had 74% of the market. A June 19, 2008 Intuit Press Release said that as of March 2008, QuickBooks' share of retail units in the business accounting category reached 94.2 percent, according to NPD Group. It also says that more than 50,000 accountants, CPAs and independent business consultants are members of the QuickBooks ProAdvisor program. By then Brad Smith was the new CEO, though former CEO Steve Bennett had nearly tripled Intuit revenue and quadrupled earnings in eight years.
On September 22, 2014, Intuit announced the release of QuickBooks 2015 with the features that users have been requesting from the past versions. Improved income tracker, pinned notes, improved registration process and insights on homepage are some of the general changes for all versions of QuickBooks 2015 (Pro, Premier, Enterprise solution and Accountant version).



Versions of this product are available in many different markets. Intuit's Canadian and U.K. divisions offer versions of QuickBooks that support the unique needs of each region, such as Canada's GST, HST or PST sales tax and European VAT for the UK edition. The Quickbooks UK edition makes available support for Irish and South African VAT.
The Mac version is available only in the U.S.



Intuit has integrated several web-based features into QuickBooks, including remote access capabilities, remote payroll assistance and outsourcing, electronic payment functions, online banking and reconciliation, mapping features through integration with Google Maps, marketing options through Google, and improved e-mail functionality through Microsoft Outlook and Outlook Express. For the 2008 version, the company has also added import from Excel spreadsheets, additional employee time tracking options, pre-authorization of electronic funds and new Help functions. In June 2007, Intuit announced that QuickBooks Enterprise Solutions would run on Linux servers, whereas previously it required a Windows server to run.



Intuit also offers a cloud solution called QuickBooks Online (QBO). The user pays a monthly subscription fee rather than an upfront fee and accesses the software exclusively through a secure logon via a web browser. Intuit provides patches, and regularly upgrades the software automatically.
As of May, 2014, QuickBooks Online had the most subscribers for an online accounting platform, with 624,000 subscribers. compared to Xero, which reported 284,000 customers as of July, 2014.
The cloud version is a distinct product from the desktop version of QuickBooks, and has many features that work differently than they do in desktop versions.
In 2013, Intuit announced that it had rebuilt QuickBooks Online "from the ground up" with a platform that allows third parties to create small business applications and gives customers the ability to customize the online version of QuickBooks.
QuickBooks Online is supported on Chrome, Firefox, Internet Explorer 10, Safari 6.1, and also accessible via Chrome on Android and Safari on iOS 7. Internet Explorer. One may also access QuickBooks Online via an iPhone, a BlackBerry, and an Android web app.
In 2011, Intuit introduced a UK-specific version of QuickBooks Online to address the specific VAT and European tax system. There are also versions customized for the Canadian, Indian, and Australian markets, as well as a global version that can be customized by the user.



QuickBooks Point of Sale is software that replaces a retailer's cash register, tracks its inventory, sales, and customer information, and provides reports for managing its business and serving its customers.



Through the Solutions Marketplace, Intuit encouraged third-party software developers to create programs that fill niche areas for specific industries and integrate with QuickBooks. The Intuit Developer Network provides marketing and technical resources, including SDKs for EDI.
Intuit's Lacerte and ProLine tax preparation software for professional accountants who prepare tax returns for a living integrates with QuickBooks in this way. Microsoft Office also integrates with QuickBooks.
Compared to other versions of QuickBooks, fewer third-party add-ons integrate with QuickBooks Online Edition. Although recently, seamless Quickbooks integration is only offered by a handful of vendors.
In addition to add-on products and an online edition, Intuit recently incepted a Commercial Hosting Program, allowing commercial service providers to offer hosted versions of QuickBooks Pro, Premier and Enterprise. Further, the QuickBooks Commercial Hosting Program allows the commercial service providers to offer rental licensing for the Pro and Premier QuickBooks editions. Hosting allows businesses to use all features of the desktop version of QuickBooks, but to run it as an Internet-accessed solution with anytime, anywhere capability and full security and management.



Quickbooks is by far the top accounting software provider among American businesses, with millions of users and estimates of around 85-90% of the business accounting market.



The 1999 version of QuickBooks forced users to pay an additional fee for payroll table updates. The 2000 version of QuickBooks tried to force users to pay an additional fee for payroll tax updates for each company file. After a boycott, this fee was changed to one charge per copy of QuickBooks.
In April 2006, Intuit disabled functionality of add-on services for QuickBooks 2003 in the areas of Bill Pay, Credit Card Download, Do-It-Yourself Payroll, Employee Organizer, Merchant Service, Online Banking, Online Billing, and Support Plans and Services. Since then, Intuit has disabled equivalent functions for all QuickBooks versions approximately 30 months after it has stopped selling them.
On 15 December 2007, the company released a version of its automatic updater for Mac OS X with a serious program error. The company fixed the updater on 17 December, but by then it had caused substantial data loss for a number of users.
QuickBooks Online suffered an extended outage on February 2, 2009, and again due to power failures on June 16 17, 2010. Many customers complained on the company's forum, and threatened to leave the service for a competitor such as Sage. Brad Smith, Intuit's CEO, apologized for the series of outages, totaling around 60 hours, which affected most Intuit online products and tech support. A later July 14 outage resulted in a second Intuit CEO apology, which referred to Intuit's "new online products", "designed for 24/7 availability", and to "aggressively migrating" to two new data centers.
A QuickBooks 2009 online banking module had major problems, which resulted in hundreds of comments in several threads on the Intuit Community discussion forum. The QuickBooks product manager used these as an official thread to discuss problems, which were resolved in March 2009.
As of November 2014, users of QuickBooks for Mac have reported compatibility issues with Apple's new operating system, OS X Yosemite.



QuickBooks were the official kit sponsor for Premier League club West Bromwich Albion for the 2014-15 Premier League season. They have also been the official kit sponsor for Aston Villa since 2015, replacing Dafabet.


