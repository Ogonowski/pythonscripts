The U.S. Army Sniper School is part of the United States Army 197th Infantry Brigade based at Fort Benning, Georgia. The first U.S. Army Sniper School was short lived, being established in 1955, but disbanded in 1956 at Camp Perry, Ohio. The U.S. Army's first permanent school was established in 1987 at Fort Benning, Georgia.



The school trains soldiers in marksmanship, sniper equipment and tactics, collection of battlefield intelligence, stalking and other sniper-related skills. The primary mission of the sniper is to deliver long range, precision fire. Their secondary mission is the collecting and reporting of battlefield information. Soldiers are taught how to blend in with the surrounding environment by wearing a ghillie suit. They must go through a course where they come within approximately 300-800 meters of spotters in the back of an LMTV and take a shot at either the driver or passenger without being noticed. They must also identify a 12" letter held up by the instructor they fired upon. Students are no longer trained on the M24 Sniper Weapon System. They are trained on the M107 rifle, and the M110 Semi-Automatic Sniper System. The U.S. Army issued three XM2010s to snipers at the United States Army Sniper School 18 January 2011.
The school is seven weeks long and is open to Active Duty, Reserves, and National Guard. Students must be in MOS series of 11 (Infantry), 18 (Special Forces), or 19D (Cav Scout).
The United States Army National Guard established a second sniper school in 1993 at Camp Robinson, Arkansas.






197th Infantry Brigade   Sniper School official page
Army Sniper School at About.com.
Sniper students make the grade   The Bayonet
Snipers Bring School to Iraqi Desert   DefenseLink