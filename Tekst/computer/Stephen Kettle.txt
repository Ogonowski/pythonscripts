Stephen Kettle (born 12 July 1966, in Castle Bromwich, Warwickshire, England) is a British sculptor who works exclusively with slate.
His best known works include Supermarine Spitfire designer R. J. Mitchell, commissioned for the Science Museum in London, which was the first statue of its type in the world; and a life size statue of Alan Turing, the founder of computer science and Enigma codebreaker, commissioned by the American philanthropist Sidney E Frank for Bletchley Park in Buckinghamshire.
Kettle lives with his wife and three children in west London.






Stephen Kettle website