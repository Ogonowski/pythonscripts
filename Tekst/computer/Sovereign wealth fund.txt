A sovereign wealth fund (SWF) is a state-owned investment fund investing in real and financial assets such as stocks, bonds, real estate, precious metals, or in alternative investments such as private equity fund or hedge funds. Sovereign wealth funds invest globally. Most SWFs are funded by revenues from commodity exports or from foreign-exchange reserves held by the central bank. By historic convention, the United States' Social Security Trust Fund, with $2.8 trillion of assets in 2014, is not considered a sovereign wealth fund.
Some sovereign wealth funds may be held by a central bank, which accumulates the funds in the course of its management of a nation's banking system; this type of fund is usually of major economic and fiscal importance. Other sovereign wealth funds are simply the state savings that are invested by various entities for the purposes of investment return, and that may not have a significant role in fiscal management.
The accumulated funds may have their origin in, or may represent, foreign currency deposits, gold, special drawing rights (SDRs) and International Monetary Fund (IMF) reserve positions held by central banks and monetary authorities, along with other national assets such as pension investments, oil funds, or other industrial and financial holdings. These are assets of the sovereign nations that are typically held in domestic and different reserve currencies (such as the dollar, euro, pound, and yen). Such investment management entities may be set up as official investment companies, state pension funds, or sovereign oil funds, among others.
There have been attempts to distinguish funds held by sovereign entities from foreign-exchange reserves held by central banks. Sovereign wealth funds can be characterized as maximizing long-term return, with foreign exchange reserves serving short-term "currency stabilization", and liquidity management. Many central banks in recent years possess reserves massively in excess of needs for liquidity or foreign exchange management. Moreover, it is widely believed most have diversified hugely into assets other than short-term, highly liquid monetary ones, though almost no data is publicly available to back up this assertion. Some central banks have even begun buying equities, or derivatives of differing ilk (even if fairly safe ones, like overnight interest rate swaps).



The term "sovereign wealth fund" was first used in 2005 by Andrew Rozanov in an article entitled, "Who holds the wealth of nations?" in the Central Banking Journal. The previous edition of the journal described the shift from traditional reserve management to sovereign wealth management; subsequently the term gained widespread use as the spending power of global officialdom has rocketed upward.
Some of them have grabbed attention making bad investments in several Wall Street financial firms such as Citigroup, Morgan Stanley, and Merrill Lynch. These firms needed a cash infusion due to losses resulting from mismanagement and the subprime mortgage crisis.
SWFs invest in a variety of asset classes such as stocks, bonds, real estate, private equity and hedge funds. Many sovereign funds are directly investing in institutional real estate. According to the Sovereign Wealth Fund Institute's transaction database around US$9.26 billion in direct sovereign wealth fund transactions were recorded in institutional real estate for the last half of 2012. In the first half of 2014, global sovereign wealth fund direct deals amounted to $50.02 bil according to the SWFI.



Sovereign wealth funds have existed for more than a century, but since 2000, the number of sovereign wealth funds has increased dramatically. The first SWFs were non-Federal U.S. state funds established in the mid-19th century to fund specific public services. The U.S. state of Texas was thus the first to establish such a scheme, to fund public education. The Permanent School Fund (PSF) was created in 1854 to benefit primary and secondary schools, with the Permanent University Fund (PUF) following in 1876 to benefit universities. The PUF was endowed with public lands, the ownership of which the state retained by terms of the 1845 annexation treaty between the Republic of Texas and the United States. While the PSF was first funded by an appropriation from the state legislature, it also received public lands at the same time that the PUF was created. The first SWF established for a sovereign state is the Kuwait Investment Authority, a commodity SWF created in 1953 from oil revenues before Kuwait gained independence from the United Kingdom. According to many estimates, Kuwait's fund is now worth approximately US$600 billion.
Another early registered SWFs is the Revenue Equalization Reserve Fund of Kiribati. Created in 1956, when the British administration of the Gilbert Islands in Micronesia put a levy on the export of phosphates used in fertilizer, the fund has since then grown to $520 million.



SWFs are typically created when governments have budgetary surpluses and have little or no international debt. It is not always possible or desirable to hold this excess liquidity as money or to channel it into immediate consumption. This is especially the case when a nation depends on raw material exports like oil, copper or diamonds. In such countries, the main reason for creating a SWF is because of the properties of resource revenue: high volatility of resource prices, unpredictability of extraction, and exhaustibility of resources.
There are two types of funds: saving funds and stabilization funds. Stabilization SWFs are created to reduce the volatility of government revenues, to counter the boom-bust cycles' adverse effect on government spending and the national economy. Savings SWFs build up savings for future generations. One such fund is the Government Pension Fund of Norway. It is believed that SWFs in resource-rich countries can help avoid resource curse, but the literature on this question is controversial. Governments may be able to spend the money immediately, but risk causing the economy to overheat, e.g., in Hugo Ch vez's Venezuela or Shah-era Iran. In such circumstances, saving the money to spend during a period of low inflation is often desirable.
Other reasons for creating SWFs may be economic, or strategic, such as war chests for uncertain times. For example, the Kuwait Investment Authority during the Gulf War managed excess reserves above the level needed for currency reserves (although many central banks do that now). The Government of Singapore Investment Corporation and Temasek Holdings are partially the expression of a desire to bolster Singapore's standing as an international financial centre. The Korea Investment Corporation has since been similarly managed. Sovereign wealth funds invest in all types of companies and assets, including startups like Xiaomi and renewable energy companies like Bloom Energy.



The growth of sovereign wealth funds is attracting close attention because:
As this asset pool continues to expand in size and importance, so does its potential impact on various asset markets.
Some countries worry that foreign investment by SWFs raises national security concerns because the purpose of the investment might be to secure control of strategically important industries for political rather than financial gain.
Former U.S. Secretary of the Treasury Lawrence Summers has argued that the U.S. could potentially lose control of assets to wealthier foreign funds whose emergence  shake[s] [the] capitalist logic  These concerns have led the European Union (EU) to reconsider whether to allow its members to use "golden shares" to block certain foreign acquisitions. This strategy has largely been excluded as a viable option by the EU, for fear it would give rise to a resurgence in international protectionism. In the United States, these concerns are addressed by the Exon Florio Amendment to the Omnibus Trade and Competitiveness Act of 1988, Pub. L. No. 100-418,   5021, 102 Stat. 1107, 1426 (codified as amended at 50 U.S.C. app.   2170 (2000)), as administered by the Committee on Foreign Investment in the United States (CFIUS).
Their inadequate transparency is a concern for investors and regulators: for example, size and source of funds, investment goals, internal checks and balances, disclosure of relationships, and holdings in private equity funds. Many of these concerns have been addressed by the IMF and its Santiago Principles, which set out common standards regarding transparency, independence, and governance.
SWFs are not nearly as homogeneous as central banks or public pension funds.
The governments of SWF's commit to follow certain rules:
Accumulation rule (what portion of revenue can be spent/saved)
Withdraw rule (when the Government can withdraw from the fund)
Investment (where revenue can be invested in foreign or domestic assets)



On 5 March 2008, a joint sub-committee of the U.S. House Financial Services Committee held a hearing to discuss the role of "Foreign Government Investment in the U.S. Economy and Financial Sector". The hearing was attended by representatives of the U.S. Department of Treasury, the U.S. Securities and Exchange Commission, the Federal Reserve Board, Norway's Ministry of Finance, Singapore's Temasek Holdings, and the Canada Pension Plan Investment Board.
On August 20, 2008, Germany approved a law that requires parliamentary approval for foreign investments that endanger national interests. To be specific, it will affect acquisitions of more than 25% of a German company's voting shares by non-European investors; but the economics minister Michael Glos has pledged that investment reviews would be "extremely rare". The legislation is loosely modelled on a similar one by the U.S. Committee on Foreign Investments.
On September 2 3, 2008, at a summit in Chile, the International Working Group of Sovereign Wealth Funds consisting of the world's main SWFs agreed to a voluntary code of conduct first drafted by IMF. They also considered a standing committee to represent them in international policy debates. The 24 principles in the draft (the Santiago Principles) were made public after being presented to the IMF governing council on October 11, 2008.



New SWFs were established in various developed jurisdictions after 2010 following the rise in energy and commodity prices, e.g., the North Dakota Legacy Fund (2011) and the Western Australian Future Fund (2012). The Bank of Israel-managed Israeli sovereign investment fund (to be established end 2014) should start operating in 2016 after more than five years of preparatory work involving veteran American as well as local asset management experts.



Assets under management of SWFs increased for the fifth year running in 2013 to a record $5.78 trillion. There was an additional $7.2 trillion held in other sovereign investment vehicles, such as pension reserve funds, development funds and state-owned corporations' funds and $8.1 trillion in other official foreign exchange reserves. Taken together, governments of SWFs, largely those in emerging economies, have access to a pool of funds totalling $20 trillion. Some of these funds could in future be channelled towards funding development of infrastructure for which there is global demand.
Countries with SWFs funded by oil and gas exports, primarily oil and gas exports, totalled $4.29 trillion as of the end of 2014. Non-oil and gas SWFs totalled $2.82& trillion. Non-commodity SWFs are typically funded by transfer of assets from official foreign exchange reserves, and in some cases from government budget surpluses and privatisation revenue. Asian countries account for the bulk of such funds.
An important point to note is the SWF-to-Foreign Reserve Exchange Ratio, which shows the proportion a government has invested in investments relative to currency reserves. According to the SWF Institute, most oil-producing nations in the Persian Gulf have a higher SWF-to-Foreign Exchange Ratio   for example, the Qatar Investment Authority (5.89 times) compared to the China Investment Corporation (0.12 times)   reflecting a more aggressive stance to seek higher returns.




** This number is a best-guess estimation by the Sovereign Wealth Funds Institute.



Global financial system
National wealth
Sovereign investment fund






Richard Teitelbaum, "The Triple Threat Facing Sovereign Wealth Funds", Institutional Investor, September 2015
Castelli Massimiliano and Fabio Scacciavillani "The New Economics of Sovereign Wealth Funds", John Wiley & Sons, 2012
Saleem H. Ali and Gary Flomenhoft. "Innovating Sovereign Wealth Funds". Policy Innovations, February 17, 2011.
M. Nicolas J. Firzli and Vincent Bazi, World Pensions Council (WPC) Asset Owners Report:  Infrastructure Investments in an Age of Austerity: The Pension and Sovereign Funds Perspective , USAK/JTW July 30, 2011 and Revue Analyse Financi re, Q4 2011
M. Nicolas J. Firzli and Joshua Franzel. "Non-Federal Sovereign Wealth Funds in the United States and Canada". Revue Analyse Financi re, Q3 2014
Xu Yi-chong and Gawdat Bahgat, eds. The Political Economy of Sovereign Wealth Funds (Palgrave Macmillan; 2011) 272 pages; case studies of SWFs in China, Kuwait, Russia, the United Arab Emirates, and other countries.
Lixia, Loh. "Sovereign Wealth Funds: States Buying the World" (Global Professional Publishing: 2010).



SWF Institute Organization dedicated to Studying Sovereign Wealth Funds
International Forum of Sovereign Wealth Funds IFSWF is a voluntary group of SWFs - Set up by IMF
Sovereign Wealth Center Provider of market-leading research on government funds including sovereign wealth funds
The impact of the global economic crisis on sovereign wealth funds
Sovereign Wealth Fund Map This is a map of SWFs and their assets
The so-called "Sovereign Wealth Funds": regulatory issues, financial stability and prudential supervision - European Economy, Economic Papers, April 2009.
The impact of sovereign wealth funds on global financial markets - European Central Bank, Occasional Paper No 91. July 2008.
Linaburg-Maduell Transparency Index - Point system on grading sovereign wealth fund transparency
Santiago Principles