Macmillan Publishers Ltd, also known as The Macmillan Group, is a privately held international publishing company owned by Georg von Holtzbrinck Publishing Group. It has offices in 41 countries worldwide and operates in more than thirty others.




Macmillan was founded in 1843 by Daniel and Alexander Macmillan, two brothers from the Isle of Arran, Scotland. Daniel was the business brain, while Alexander laid the literary foundations, publishing such notable authors as Charles Kingsley (1855), Thomas Hughes (1859), Francis Turner Palgrave (1861), Christina Rossetti (1862), Matthew Arnold (1865) and Lewis Carroll (1865). Alfred Tennyson joined the list in 1884, Thomas Hardy in 1886 and Rudyard Kipling in 1890.
Other major writers published by Macmillan included W. B. Yeats, Rabindranath Tagore, Nirad C. Chaudhuri, Sean O'Casey, John Maynard Keynes, Charles Morgan, Hugh Walpole, Margaret Mitchell, C. P. Snow, Rumer Godden and Ram Sharan Sharma.
Beyond literature, the company created such enduring titles as Nature (1869), the Grove Dictionary of Music and Musicians (1877) and Sir Robert Palgrave's Dictionary of Political Economy (1894 99).
Macmillan established an office in New York City. It sold its American division in 1896, which published as the Macmillan Company.
Macmillan Publishers re-entered the American market in 1954 under the name St. Martin's Press.
After retiring from politics in 1964, former Prime Minister of the United Kingdom Harold Macmillan became chairman of the company, serving until his death in December 1986.
The company was one of the oldest independent publishing houses until 1995, when a 70% share of the company was bought by German media giant Georg von Holtzbrinck Publishing Group (Verlagsgruppe Georg von Holtzbrinck GmbH). Holtzbrinck purchased the remaining shares in 1999, ending the Macmillan family's ownership of the company.
Since 2007 the CEO of Macmillan Publishers Ltd has been Annette Thomas.



George Edward Brett opened the first Macmillan office in the United States in 1869 and Macmillan sold its U.S. operations to the Brett family, George Platt Brett, Sr. and George Platt Brett, Jr. in 1896, resulting in the creation of an American company, Macmillan Publishing, also called the Macmillan Company. Even with the split of the American company from its parent company in England, George Brett, Jr. and Harold Macmillan remained close personal friends.
George P. Brett, Jr. made the following comments in a letter dated 23 January 1947 to Daniel Macmillan about his family's devotion to the American publishing industry:

"For the record my grandfather was employed by Macmillan's of England as a salesman. He came to the United States with his family in the service of Macmillan's of England and built up a business of approximately $50,000 before he died. He was succeeded ... by my father, who eventually incorporated The Macmillan Company of New York and built up business of about $9,000,000. I succeeded my father, and we currently doing a business of approximately $12,000,000. So then, the name of Brett and the name of Macmillan have been and are synonymous in the United States."

Pearson acquired the Macmillan name in America in 1998, following its purchase of the Simon & Schuster educational and professional group (which included various Macmillan properties). Holtzbrinck purchased it from them in 2001. McGraw-Hill continues to market its pre-kindergarten through elementary school titles under its Macmillan/McGraw-Hill brand. The US operations of Georg von Holtzbrinck are now known as Macmillan.
The current CEO of Macmillan U.S. is John Turner Sargent, Jr.



By some estimates, as of 2009 e-books account for three to five per cent of total book sales, and are the fastest growing segment of the market. According to The New York Times, Macmillan and other major publishers "fear that massive discounting [of e-books] by retailers including Amazon, Barnes & Noble and Sony could ultimately devalue what consumers are willing to pay for books." In response, the publisher introduced a new boilerplate contract for its authors that established a royalty of 20 per cent of net proceeds on e-book sales, a rate five per cent lower than most other major publishers.
Following the announcement of the Apple iPad on 27 January 2010 a product that comes with access to the iBookstore Macmillan gave Amazon.com two options: continue to sell e-books based on a price of the retailer's choice (the "wholesale model"), with the e-book edition released several months after the hardcover edition is released, or switch to the agency model introduced to the industry by Apple, in which both are released simultaneously and the price is set by the publisher. In the latter case, Amazon.com would receive a 30 per cent commission.
Amazon responded by pulling all Macmillan books, both electronic and physical, from their website (although affiliates selling the books were still listed). On 31 January 2010, Amazon chose the agency model preferred by Macmillan.
In April 2012, the United States Department of Justice filed United States v. Apple Inc., naming Apple, Macmillan, and four other major publishers as defendants. The suit alleged that they conspired to fix prices for e-books, and weaken Amazon.com's position in the market, in violation of antitrust law.
In December 2013, a federal judge approved a settlement of the antitrust claims, in which Macmillan and the other publishers paid into a fund that provided credits to customers who had overpaid for books due to the price-fixing.



The company comprises divisions operating in five areas of publishing:
Education publishing, including English language teaching (as Macmillan Education)
Academic publishing, including reference (as Palgrave Macmillan)
Science, technological and medical publishing (as Nature Publishing Group), including Nature and other journals
Fiction and non-fiction book publishing (as Pan Macmillan), under the imprints Pan Books, Picador, Macmillan New Writing, Papermac, Kingfisher, Macmillan, Sidgwick & Jackson, Campbell Books, Boxtree Ltd., Macmillan Children's Books, and the newest imprint, Flatiron Books
MacMillan Publishing Solutions (MPS) serves print and digital publishing, advertisement creation, magazine production, and books and journals fulfillment.
MPS Technologies offers content delivery, web analytics, and technology-related services as well as distribution and production.
Macmillan Audio, established as Audio Renaissance in 1987, was acquired by Holtzbrinck in 2001. It publishes abridged and unabridged audiobook editions of books published under Macmillan trade imprints and also acquires some outside titles.



List of largest UK book publishers






Macmillan   A Publishing Tradition by Elizabeth James ISBN 0-333-73517-X
 Chisholm, Hugh, ed. (1911). "Macmillan". Encyclop dia Britannica 17 (11th ed.). Cambridge University Press. p. 264. 




Official website