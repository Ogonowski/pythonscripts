Olympus Corporation ( , Orinpasu Kabushiki-gaisha) is a Japan-based manufacturer of optics and reprography products. Olympus was established on 12 October 1919, initially specializing in microscope and thermometer businesses. Olympus enjoys a majority share of the world market in gastro-intestinal endoscopes. It has a roughly 70% share of the global market whose estimated value is US$2.5 billion. Its global headquarters are in Shinjuku, Tokyo, Japan.
In 2011, the company gained coverage in global media when it fired its newly appointed British chief executive (CEO) Michael Woodford. Woodford, a 30-year Olympus veteran and Olympus' president and chief operating officer (COO) since April that year, had sought to probe financial irregularities and unexplained payments of hundreds of millions of dollars following his appointment as CEO. Although the board initially dismissed Woodford's concerns via mass media as being "disruptive" actions and Woodford as failing to grasp local culture, the matter quickly snowballed into a corporate corruption scandal over concealment (called Tobashi) of more than 117.7 billion Yen ($1.5 billion) of investment losses and other dubious fees and other payments dating back to the late 1980s and suspicion of covert payments to criminal organizations. By 2012 the scandal had developed into one of the biggest and longest-lived loss-concealing financial scandals in the history of corporate Japan; it had wiped 75 80% off the company's stock market valuation, led to the resignation of much of the board, investigations across Japan, the UK and US, the arrest of 11 past or present Japanese directors, senior managers, auditors and bankers of Olympus for alleged criminal activities or cover-up, and raised considerable turmoil and concern over Japan's prevailing corporate governance and transparency  and the Japanese financial markets. Woodford himself, who stated he had received death threats over his exposing of the cover-up, received a reported  10 million ($16 m) in damages from Olympus for defamation and wrongful dismissal in 2012; around the same time, Olympus also announced it would shed 2,700 jobs (7% of its workforce) and around 40 percent of its 30 manufacturing plants by 2015 to reduce its cost base. The company is owned 17% by two large camera makers, Sony and Mitsubishi (Makers of Nikon).






In 1936, Olympus introduced its first camera, the Semi-Olympus I. The first innovative camera series from Olympus was the Pen, launched in 1959. The half-frame format, allowing 72 pictures of 18   24mm format on a standard 36 exposure roll of film, made Pen cameras compact and portable for their time.

The Pen system design team, led by Yoshihisa Maitani, later created the OM system, a full-frame professional 35mm SLR system designed to compete with Nikon and Canon's bestsellers. The OM system introduced a new trend towards more compact cameras and lenses, being much smaller than its competitors and presenting innovative design features such as off-the-film (OTF) metering and OTF flash automation. Eventually the system included 14 different bodies, approximately 60 Zuiko-branded lenses, and numerous camera accessories.

In 1983, Olympus, along with Canon, branded a range of video recording equipment manufactured by JVC, and called it "Olympus Video Photography", even employing renowned photographer Terance Donovan to promote the range. A second version of the system was available the year after, but this was Olympus' last foray into the world of consumer video equipment until digital cameras became popular.
Tsuyoshi Kikukawa, who was later to become President of Olympus, foresaw the demand for the digital SLR, and is credited with the company's strategy in digital photography. He fought for commitment by Olympus to enter the market in high-resolution photographic products. As a result of his efforts, Olympus released an 810,000-pixel digital camera for the mass market in 1996, when the resolution of rivals' offerings were less than half. The very next year, Olympus hit the market with a 1.41 million pixel camera. By 2001, the company's annual turnover from digital photography was in excess of  100 billion. Olympus manufactures compact digital cameras and is the designer of the Four-Thirds System standard for digital single-lens reflex cameras. Olympus' Four Thirds system flagship DSLR camera is the E-5 released in 2010. Olympus is also the largest manufacturer of Four-Thirds lenses, under the Zuiko brand.
At one time Olympus cameras used only the proprietary xD-Picture Card for storage media. This storage solution is less popular than more common formats, and recent cameras can use SD and CompactFlash cards. The most recent development is Olympus' focus on the Micro Four Thirds system.
Olympus first introduced the Microcassette. The Olympus Pearlcorder L400, released in the 1980s, was the smallest and lightest Microcassette Voice recorder ever offered for sale, 2.9 (L)   0.8 (H)   2.0 in. (W) / 73 (L)   20 (H)   52 (W) 3.2 oz (91 g).
In 2012, the company announced that Sony and Fujifilm had offered forming a capital alliance and the company would focus on Mirrorless interchangeable-lens cameras (MILC).



Olympus manufactures endoscopic, ultrasound, electrocautery, endotherapy and cleaning & disinfection equipment. The first flexible Endoscope in the world was co-developed and manufactured by Olympus in Tokyo. Through its comprehensive product range and its reactivity to market innovations, Olympus enjoys a virtual stranglehold of the world market in gastro-intestinal endoscopes. It has roughly 70% share of the global market whose estimated valued at US$2.5 billion. On 28 September 2012, Olympus and Sony announced that the two companies will establish a joint venture to develop new surgical endoscopes with 4K resolution (or higher) and 3D capability.



Since the beginning, the company has also been a manufacturer of microscopes and optics for specialised needs, such as medical use. The first microscope manufactured at Olympus was called the Asahi. Currently, Olympus is a worldwide renowned manufacturer of microscopes. Olympus offers a complete range of microscopes, which covers applications from education and routine studies up to state of the art research imaging systems both in life science and materials science.
Olympus Scientific Solutions Americas Corporation is a Waltham, Massachusetts-based manufacturer, and is a subsidiary of Olympus Corporation. One of its companies, for example, is Olympus Imaging and Measuring Systems, specializing in imaging instruments for testing and measurement during industrial inspections.



Olympus manufactures and sells industrial scanners, flaw detectors, probes and transducers, thickness gages, digital cameras, image analysis software, industrial videoscopes, fiberscopes, light sources, XRF and XRD analyzers, and high-speed video cameras.



1919: The company was founded as Takachiho Seisakusho. In Japanese mythology, deities live on Takamagahara, the peak of Mt. Takachiho. The first corporate logo was TOKIWA, derived from Tokiwa Shokai, the company that the founder, Takeshi Yamashita, had worked for. Tokiwa Shokai held an equity stake in Takachiho Seisakusho and was responsible for marketing Takachiho products. The logo reads "TOKIWA TOKYO". The "G" and "M" marks above are believed to be the initials of Goro Matsukata, the president of Tokiwa Shokai.
1921: The Olympus brand was introduced in February 1921. This logo was used for microscopes and other products. Brochures and newspaper ads for cameras also used this logo. The OLYMPUS TOKYO logo is still in use today. There was a period in which OIC was used instead of TOKYO in the logo. OIC stood for Optical Industrial Company, which was a translation of Olympus' Japanese corporate name at that time. This logo was used for the GT-I and GT-II endoscopes, among others.
1942: The company was renamed to Takachiho Optical Co., Ltd., when optical products became the mainstay of the company.
1949: The name changed to Olympus Optical Co., Ltd. It was named after Mount Olympus, which like Mt. Takachiho is the home of gods, this time of Greek mythology. In the words of the company, they chose the name to "reflect its strong aspiration to create high-quality, world-famous products".
1970: The new logo was designed to give impressions of quality and sophistication.
2001: The yellow line underneath the new logo is called the "Opto-Digital Pattern" and it represents light and boundless possibilities of digital technology. It symbolizes dynamic and innovative nature of Opto-Digital Technology and Olympus Corporation. This logo is called the Communication Symbol of Olympus and it represents Olympus' brand image.
2003: Renamed Olympus Corporation.






Shareholding in Olympus is dispersed, and the company's key institutional investors are largely passive. As of 31 March 2011, investors include Nippon Life Insurance (8.4%), Bank of Tokyo-Mitsubishi (4.98%), and Sumitomo Mitsui Banking (3.13%), and the Government of Singapore Investment Corporation (2.55%). Foreign institutions and individuals speak for 27.71% of Olympus shares. On 28 September 2012, Olympus and Sony announced that Olympus will receive a 50 billion yen capital injection from Sony. On 22 February 2013, Sony became the largest shareholder (11.46%) of Olympus.



According to its 2011 Annual Report, Olympus was governed by a 15-person board of directors, with Tsuyoshi Kikukawa its President and CEO, and Michael C. Woodford as President and chief operating officer. Mr Kikukawa resigned in the following year and was arrested by Tokyo police for alleged criminal offenses during and before his term as president and CEO. The corporation in 2011 had three "outside directors". It has a four-member 'Board of Auditors' which supervises and audits directors' performance. The company's executive committee consists of 28 members, responsible for the day-to-day operations.




On 1 April 2011, Michael Woodford, 51, was named president and chief operating officer   the first ever foreigner to hold the position   replacing Kikukawa, who became chairman. Woodford, an Olympus veteran of 30-years, was previously executive managing director of Olympus Medical Systems Europa. Olympus appointed Woodford its CEO six months later, but the board suddenly removed him as chief executive two weeks into the job, while allowing him to retain his board seat.
Woodford alleged that his removal was linked to several prior acquisitions he questioned, particularly the US$2.2 billion deal in 2008 to acquire British medical equipment maker Gyrus Group. Thomson Reuters reported that US$687 million was paid to a middle-man as a success fee   a sum equal to 31% of the purchase price, and which ranks as the highest ever M&A fee. According to the Daily Telegraph, some of the sums paid out relating to the acquisition of a technology company ITX were also under examination. Woodford noted that an article in Japanese financial magazine Facta in July prompted his suspicion of the transactions. Reports also said the company acquired three other Japanese companies outside its core business, and recognised that the assets were worth US$721 million less than their acquisition value 12 months previously.
Shareholders expressed concern after Olympus' share price nearly halved in value following the Woodford revelations, and asked for "prompt action". Following his dismissal, Woodford passed on information to the British Serious Fraud Office, and requested police protection. He said the payments may have been linked to "forces behind" the Olympus board. Japanese newspaper Sankei suggest that a total of $1.5bn in acquisition-related advisory payments could be linked to the Yakuza.
The company responded on 19 October that "major differences had arisen between Mr. Woodford and other management regarding the direction and conduct of the company s business". On the Gyrus acquisition, it also declared the Audit Board's view that "no dishonesty or illegality is found in the transaction itself, nor any breach of obligation to good management or any systematic errors by the directors recognised." On 26 October, the company announced that in order to assuage shareholders' concerns, Kikukawa resigned as chairman; he was replaced by Shuichi Takayama. Olympus shares rebounded 23 percent.
On 8 November 2011, the company admitted that the money had been used to cover losses on investments dating to the 1990s and that company's accounting practice was "not appropriate", thus coming clean on "one of the biggest and longest-running loss-hiding arrangements in Japanese corporate history", according to the Wall Street Journal. The company laid the blame for the inappropriate accounting on ex-president Tsuyoshi Kikukawa, auditor Hideo Yamada and executive VP Hisashi Mori.
On 21 December 2011, Japanese authorities, including the Tokyo prosecutor's office, the Tokyo Metropolitan Police and the Japanese Securities and Exchange Surveillance Commission, raided the company's offices in Tokyo.
In February 2012, seven Olympus executives were arrested by Japanese police and prosecutors. Ex-president Tsuyoshi Kikukawa, former executive vice president Hisashi Mori and former auditor Hideo Yamada were taken into custody on suspicion of violating the Financial Instruments and Exchange Law, along with former bankers Akio Nakagawa and Nobumasa Yokoo and two others, suspected of having helped the board hide significant losses. On 25 September 2012, the company and Kikukawa, Yamada, and Mori pleaded guilty to collusion to submit false financial reports.




Four Thirds System
Laboratory equipment
List of digital camera brands
List of Olympus products
List of photographic equipment makers
Micro Four Thirds System
Olympus OM system
Variable Control Voice Actuator
xD-Picture Card and SmartMedia






Olympus Global Official Web Site.