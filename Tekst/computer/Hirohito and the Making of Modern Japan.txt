Hirohito and the Making of Modern Japan (2000, ISBN 978-0-06-019314-0) is a book by Herbert P. Bix covering the reign of Emperor Hirohito of Japan from 1926 until his death in 1989. It won the 2001 Pulitzer Prize for General Non-Fiction.


