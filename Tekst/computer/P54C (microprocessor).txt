The Intel Pentium microprocessor was introduced on March 22, 1993. Its microarchitecture, dubbed P5, was Intel's fifth-generation and first superscalar IA-32 microarchitecture. As a direct extension of the 80486 architecture, it included dual integer pipelines, a faster floating-point unit, wider data bus, separate code and data caches and features for further reduced address calculation latency. In 1996, the Pentium with MMX Technology (often simply referred to as Pentium MMX) was introduced with the same basic microarchitecture complemented with an MMX instruction set, larger caches, and some other enhancements.

The P5 Pentium competitors included the Motorola 68060 and the PowerPC 601 as well as the SPARC, MIPS, and Alpha microprocessor families, most of which also used a superscalar in-order dual instruction pipeline configuration at some time.
Intel's Larrabee multicore architecture project uses a processor core derived from a P5 core (P54C), augmented by multithreading, 64-bit instructions, and a 16-wide vector processing unit. Intel's low-powered Bonnell microarchitecture employed in Atom processor cores also uses an in-order dual pipeline similar to P5.



The P5 microarchitecture was designed by the same Santa Clara team which designed the 386 and 486. Design work started in 1989; the team decided to use a superscalar architecture, with on-chip cache, floating-point, and branch prediction. The preliminary design was first successfully simulated in 1990, followed by the laying-out of the design. By this time, the team had several dozen engineers. The design was taped out, or transferred to silicon, in April 1992, at which point beta-testing began. By mid-1992, the P5 team had 200 engineers. Intel at first planned to demonstrate the P5 in June 1992 at the trade show PC Expo, and to formally announce the processor in September 1992, but design problems forced the demo to be cancelled, and the official introduction of the chip was delayed until the spring of 1993.
John H. Crawford, chief architect of the original 386, co-managed the design of the P5, along with Donald Alpert, who managed the architectural team. Dror Avnon managed the design of the FPU. Vinod K. Dham was general manager of the P5 group.



The P5 microarchitecture brings several important advancements over the preceding i486 architecture.
Performance:
Superscalar architecture   The Pentium has two datapaths (pipelines) that allow it to complete two instructions per clock cycle in many cases. The main pipe (U) can handle any instruction, while the other (V) can handle the most common simple instructions. Some RISC proponents had argued that the "complicated" x86 instruction set would probably never be implemented by a tightly pipelined microarchitecture, much less by a dual pipeline design. The 486 and the Pentium demonstrated that this was indeed possible and feasible.
64-bit external databus doubles the amount of information possible to read or write on each memory access and therefore allows the Pentium to load its code cache faster than the 80486; it also allows faster access and storage of 64-bit and 80-bit x87 FPU data.
Separation of code and data caches lessens the fetch and operand read/write conflicts compared to the 486. To reduce access time and implementation cost, both of them are 2-way associative, instead of the single 4-way cache of the 486. A related enhancement in the Pentium is the ability to read a contiguous block from the code cache even when it is split between two cache lines (at least 17 bytes in worst case).
Much faster floating point unit. Some instructions showed an enormous improvement, most notably FMUL, with up to 15 times higher throughput than in the 80486 FPU. The Pentium is also able to execute a FXCH ST(x) instruction in parallel with an ordinary (arithmetical or load/store) FPU instruction.
Four-input address-adders enables the Pentium to further reduce the address calculation latency compared to the 80486. The Pentium can calculate full addressing modes with segment-base + base-register + scaled register + immediate offset in a single cycle; the 486 has a three-input address-adder only, and must therefore divide such calculations between two cycles.
The microcode can employ both pipelines to enable auto-repeating instructions such as rep movsw perform one iteration every clock cycle, while the 80486 needed three clocks per iteration (and the earliest x86-chips significantly more than the 486). Also, optimization of the access to the first microcode words during the decode stages helps in making several frequent instructions execute significantly more quickly, especially in their most common forms, and in typical cases. Some examples are (486 Pentium, in clock cycles): CALL (3 1), RET (5 2), shifts/rotates (2~3 1), etc.
A faster, fully hardware-based multiplier makes instructions such as MUL and IMUL several times as fast (and more predictable) than in the 80486; the execution time is reduced from 13~42 clock cycles down to 10~11 for 32-bit operands.
Virtualized interrupt to speed up virtual 8086 mode.

Other features:
Enhanced debug features with the introduction of the Processor-based debug port (See Pentium Processor Debugging in the Developers Manual, Vol 1).
Enhanced self test features like the L1 cache parity check (see Cache Structure in the Developers Manual, Vol 1).

The later Pentium MMX also added the MMX instruction set, a basic integer SIMD instruction set extension marketed for use in multimedia applications. MMX could not be used simultaneously with the x87 FPU instructions because the registers were reused (to allow for fast context switches). More important enhancements were the doubling of the instruction and data cache sizes and a few microarchitectural changes for better performance.
The Pentium was designed to execute over 100 million instructions per second (MIPS), and the 75 MHz model was able to reach 126.5 MIPS in certain benchmarks. The Pentium architecture typically offered just under twice the performance of a 486 processor per clock cycle in common benchmarks. The fastest 80486 parts (with slightly improved microarchitecture and 100 MHz operation) were almost as powerful as the first-generation Pentiums, and the AMD Am5x86 was roughly equal to the Pentium 75 regarding pure ALU performance.



The early versions of 60 100 MHz P5 Pentiums had a problem in the floating point unit that resulted in incorrect (but predictable) results from some division operations. This bug, discovered in 1994 by professor Thomas Nicely at Lynchburg College, Virginia, became known as the Pentium FDIV bug and caused embarrassment for Intel, which created an exchange program to replace the faulty processors. Soon afterwards, a bug was discovered which could allow a malicious program to crash a system without any special privileges (the "F00F bug"); fortunately, operating systems were able to implement workarounds to prevent crashes.
The 60 and 66 MHz 0.8  m versions of the P5 Pentium processors also had (for the time) high heat production due to their 5 V operation, and were often known colloquially as "coffee warmers" or some similar nickname. The P54C used 3.3 V and had significantly (about 51%) lower power draw (a quadratic relationship). P5 Pentiums used Socket 4, while P54C started out on Socket 5 before moving to Socket 7 in later revisions. All desktop Pentiums from P54CS onwards used Socket 7.



The Pentium was Intel's primary microprocessor for personal computers during the mid-1990s. The original design was reimplemented in newer processes and new features were added to maintain its competitiveness as well as to address specific markets such as portable computers. As a result, there were several variants of the P5 microarchitecture.




The first Pentium microprocessor core was code-named "P5". Its product code was 80501 (80500 for the earliest steppings Q0399). There were two versions, specified to operate at 60 MHz and 66 MHz respectively. This first implementation of the Pentium used a traditional 5 Volt power supply (descended from the usual TTL logic compatibility requirements). It contained 3.1 million transistors and measured 16.7 mm by 17.6 mm for an area of 293.92 mm2. It was fabricated in a 0.8  m BiCMOS process. The five-volt design resulted in relatively high energy consumption for its operating frequency, when compared to the later models.




The P5 was followed by the P54C (80502); there were versions specified to operate at 75, 90, or 100 MHz using a 3.3 volt power supply. This was the first Pentium processor to operate at a 3.3 volts, reducing energy consumption. It employed an internal clock multiplier to let the internal circuitry work at a higher frequency than the external address and data buses, as it is more complicated and cumbersome to increase the external frequency, due to physical constraints. It also allowed two-way multiprocessing and had an integrated local APIC as well as new power management features. It contained 3.3 million transistors and measured 163 mm2. It was fabricated in a BiCMOS process which has been described as both 0.5  m and 0.6  m due to differing definitions.



The P54C was followed by the P54CQS which operated at 120 MHz. It was fabricated in a 0.35  m BiCMOS process and was the first commercial microprocessor to be fabricated in a 0.35  m process. Its transistor count is identical to the P54C and, despite the newer process, it had an identical die area as well. The chip was connected to the package using wire bonding, which only allows connections along the edges of the chip. A smaller chip would have required a redesign of the package, as there is a limit on the length of the wires and the edges of the chip would be further away from the pads on the package. The solution was to keep the chip the same size, retain the existing pad-ring, and only reduce the size of the Pentium's logic circuitry to enable it to achieve higher clock frequencies.



The P54CQS was followed by the P54CS, which operated at 133, 150, 166 and 200 MHz. It contained 3.3 million transistors, measured 90 mm2 and was fabricated in a 0.35  m BiCMOS process with four levels of interconnect.




The P24T Pentium OverDrive for 486 systems were released in 1995, which were based on 3.3 V 0.6  m versions using a 63 or 83 MHz clock. Since these used Socket 2/3, some modifications had to be made to compensate for the 32-bit data bus and slower on-board L2 cache of 486 motherboards. They were therefore equipped with a 32 KB L1 cache (double that of pre-P55C Pentium CPUs).




The P55C (or 80503) was developed by Intel's Research & Development Center in Haifa, Israel. It was sold as Pentium with MMX Technology (usually just called Pentium MMX); although it was based on the P5 core, it featured a new set of 57 "MMX" instructions intended to improve performance on multimedia tasks, such as encoding and decoding digital media data. The Pentium MMX line was introduced on 22 October 1996.
The new instructions worked on new data types: 64-bit packed vectors of either eight 8-bit integers, four 16-bit integers, two 32-bit integers, or one 64-bit integer. So, for example, the PADDUSB (Packed ADD Unsigned Saturated Byte) instruction adds two vectors, each containing eight 8-bit unsigned integers together, pairwise; each addition that would overflow saturates, yielding 255, the maximum unsigned value that can be represented in a byte. These rather specialized instructions generally require special coding by the programmer for them to be used. The performance of the P55C was improved over previous versions by a doubling of the Level 1 CPU cache from 16 KB to 32 KB.
It contained 4.5 million transistors and had an area of 140 mm2. It was fabricated in a 0.28  m CMOS process with the same metal pitches as the previous 0.35  m BiCMOS process, so Intel described it as "0.35  m" because of its similar transistor density. The process has four levels of interconnect.
While the P55C is compatible with the common Socket 7 motherboard configuration, the voltage requirements for powering the chip differ from the standard Socket 7 specifications. Most motherboards manufactured for Socket 7 prior to the establishment of the P55C standard are not compliant with the dual intensity required for proper operation of this chip. Intel temporarily manufactured an upgrade kit called the OverDrive that was designed to correct this lack of planning on the motherboard makers part.



Pentium MMX notebook CPUs used a "mobile module" that held the CPU. This module was a PCB with the CPU directly attached to it in a smaller form factor. The module snapped to the notebook motherboard and typically a heat spreader was installed and made contact with the module. However, with the 0.25  m Tillamook Mobile Pentium MMX (named after a city in Oregon), the module also held the 430TX chipset along with the system's 512 KB SRAM cache memory.






After the introduction of the Pentium, competitors such as Nexgen, AMD, Cyrix, and Texas Instruments announced Pentium-compatible processors in 1994. CIO magazine identified NexGen's Nx586 as the first Pentium-compatible CPU, while PC Magazine described the Cyrix 6x86 as the first. These were followed by the AMD K5, which was delayed due to design difficulties. AMD later bought NexGen in order to help design the AMD K6, and Cyrix was purchased by National Semiconductor. Later processors from AMD and Intel retain compatibility with the original Pentium.


