The Mozilla Firefox project was created by Dave Hyatt and Blake Ross as an experimental branch of the Mozilla browser.
Firefox 1.0 was released on November 9, 2004, Firefox 1.5 was released on November 29, 2005, and version 2.0 was released on October 24, 2006. Firefox 3.0 was released on June 17, 2008, with version 3.5 and version 3.6 released on June 30, 2009 and January 21, 2010, respectively. Version 4.0 was released on March 22, 2011. Since the version 5.0, a rapid release cycle was put into effect, resulting in a new major version release every six weeks on Tuesday.
The latest version, Firefox 41.0.1, was released on September 30, 2015.




The project that became Firefox started as an experimental branch of the Mozilla Suite called m/b (or mozilla/browser). After it had been sufficiently developed, binaries for public testing appeared in September 2002 under the name Phoenix. This name carried the implication of the mythical firebird that rose triumphantly from the ashes of its dead predecessor, in this case from the "ashes" of Netscape Navigator after it had been killed off by Microsoft's Internet Explorer in the "First browser war". The history of the name Mozilla goes all the way back to the internal codename for the original 1994 Netscape Navigator browser, with the name meaning "Mosaic killer" and aiming to some similarity with the building-crushing Godzilla, as the company's goal was to displace NCSA Mosaic as the world's number one web browser. The name Mozilla was revived as the 1998 open sourcing spinoff organization from Netscape.
The Phoenix name was kept until April 14, 2003, when it was changed because of a trademark dispute with the BIOS manufacturer, Phoenix Technologies (which produces a BIOS-based browser called Phoenix FirstWare Connect). The new name, Firebird, met with mixed reactions, particularly as the Firebird database server already carried the name. In response, the Mozilla Foundation stated that the browser should always bear the name Mozilla Firebird to avoid confusion with the database software. Continuing pressure from the Firebird community forced another change, and on February 9, 2004 the project was renamed Mozilla Firefox (or Firefox for short).
The name "Firefox" (a reference to the red panda) was chosen for its similarity to "Firebird", but also for its uniqueness in the computing industry. To ensure that no further name changes would be necessary, the Mozilla Foundation began the process of registering Firefox as a trademark with the United States Patent and Trademark Office in December 2003. This trademark process led to a delay of several months in the release of Firefox 0.8 when the foundation discovered that Firefox had already been registered as a trademark in the UK for Charlton Company software. The situation was resolved when the foundation was given a license to use Charlton's European trademark.




Hyatt, Ross, Hewitt and Chanial developed their browser to combat the software bloat of the Mozilla Suite (codenamed, internally referred to, and continued by the community as SeaMonkey), which integrated features such as IRC, mail and news, and WYSIWYG HTML editing into one internet suite.
Firefox retains the cross-platform nature of the original Mozilla browser, using the XUL user interface markup language. The use of XUL makes it possible to extend the browser's capabilities through the use of extensions and themes. The development and installation processes of these add-ons raised security concerns, and with the release of Firefox 0.9, the Mozilla Foundation opened a Mozilla Update website containing "approved" themes and extensions. The use of XUL sets Firefox apart from other browsers, including other projects based on Mozilla's Gecko layout engine and most other browsers, which use interfaces native to their respective platforms (Galeon and Epiphany use GTK+; K-Meleon uses MFC; and Camino uses Cocoa). Many of these projects started before Firefox, and probably served as inspiration.
On February 5, 2004 AMS, a business and IT consulting company, categorized Mozilla Firefox (then known as Firebird) as a "Tier 1" ("Best of Breed") open-source product, considering it technically strong and virtually risk-free.




Firefox 1.0 was released on November 9, 2004. The launch of version 1.0 was accompanied by "a respectable amount of pre-launch fervor" including a fan-organized campaign to run a full-page ad in The New York Times.
Although the Mozilla Foundation had intended to make the Mozilla Suite obsolete and replace it with Firefox, the Foundation continued to maintain the suite until April 12, 2006 because it had many corporate users and was bundled with other software. The Mozilla community (as opposed to the Foundation) continues to release new versions of the suite, using the product name SeaMonkey to avoid confusion with the original Mozilla Suite.




On June 23, 2005, the Mozilla Foundation announced that Firefox 1.1 (which became Firefox 1.5) and other new Mozilla products would no longer support Mac OS X v10.1, in order to improve the quality of Firefox releases on Mac OS X v10.2 and above. Mac 10.1 users could still use Firefox versions from the 1.0.x branch (e.g. Firefox 1.0.7).

Firefox 1.5 was released on November 30, 2005. While Firefox 1.5 was originally slated to arrive later, the Mozilla Foundation abandoned the 1.1 release plan after the first two 1.1 alpha builds, merging it with the feature set of 1.5, which ended up being released later than the original 1.1 date. The new version resynchronized the code base of the release builds (as opposed to nightly builds) with the core "trunk", which contained additional features not available in 1.0, as it branched from the trunk around the 0.9 release. As such, there was a backlog of bug fixes between 0.9 and the release of 1.0, which were made available in 1.5. Version 1.5 implemented a new Mac-like options interface, the subject of much criticism from Windows and Linux users, with a "Sanitize" action to allow someone to clear their privacy-related information without manually clicking the "Clear All" button. In Firefox 1.5, a user could clear all privacy-related settings simply by exiting the browser or using a keyboard shortcut, depending on their settings. Moreover, the software update system was improved (with binary patches now possible). There were also improvements in the extension management system, with a number of new developer features.
In addition, Firefox 1.5 had preliminary SVG 1.1 support.
Alpha builds of Firefox 1.5 (1.1a1 and 1.1a2) did not carry Firefox branding; they were labeled "Deer Park" (which was Firefox 1.5's internal codename) and contained a different program icon. This was done to dissuade end-users from downloading preview versions, which are intended for developers only.
Firefox 1.5.0.12 was the final version supported on Windows 95.




On October 24, 2006, Mozilla released Firefox 2. This version includes updates to the tabbed browsing environment; the extensions manager; the GUI (Graphical User Interface); and the find, search and software update engines; a new session restore feature; inline spell checking; and an anti-phishing feature which was implemented by Google as an extension, and later merged into the program itself. In December 2007, Firefox Live Chat was launched. It allows users to ask volunteers questions through a system powered by Jive Software, with guaranteed hours of operation and the possibility of help after hours. Firefox 2.0.0.20 is the final version which can run under an unmodified installation of Windows NT 4.0, Windows 98, and Windows ME.
Mozilla Firefox 2.0.0.x was the final version supported on Windows NT 4.0, 98 and Me. Mozilla Corporation announced it would not develop new versions of Firefox 2 after the 2.0.0.20 release, but continued Firefox 2 development as long as other programs, such as Thunderbird mail client, depended on it. The final internal release was 2.0.0.22, released in late April 2009.




Firefox 3 was released on June 17, 2008, by the Mozilla Corporation. Firefox 3 uses version 1.9 of the Mozilla Gecko layout engine for displaying web pages. This version fixes many bugs, improves standard compliance, and implements new web APIs. Other new features include a redesigned download manager, a new "Places" system for storing bookmarks and history, and separate themes for different operating systems. Tabbed browsing was more popularised in this version. The final version under 3.0 is Firefox 3.0.19.
Development stretches back to the first Firefox 3 beta (under the codename 'Gran Paradiso') which had been released several months earlier on November 19, 2007, and was followed by several more beta releases in spring 2008 culminating in the June release. Firefox 3 had more than 8 million unique downloads the day it was released, setting a Guinness World Record.




Version 3.5, codenamed Shiretoko, adds a variety of new features to Firefox. Initially numbered Firefox 3.1, Mozilla developers decided to change the numbering of the release to 3.5, in order to reflect a significantly greater scope of changes than originally planned. The final release was on June 30, 2009. The changes included much faster performance thanks to an upgrade to SpiderMonkey JavaScript engine called TraceMonkey and rendering improvements, and support for the <video> and <audio> tags as defined in the HTML5 specification, with a goal to offer video playback without being encumbered by patent problems associated with many video technologies. Cross-site XMLHttpRequests (XHR), which can allow for more powerful web applications and an easier way to implement mashups, are also implemented in 3.5. A new global JSON object contains native functions to efficiently and safely serialize and deserialize JSON objects, as specified by the ECMAScript 3.1 draft. Full CSS 3 selector support has been added. Firefox 3.5 uses the Gecko 1.9.1 engine, which includes a few features that were not included in the 3.0 release. Multi-touch trackpad support was also added to the release, including gesture support like pinching for zooming and swiping for back and forward. Firefox 3.5 also features an updated logo.




Version 3.6, released on January 21, 2010, uses the Gecko 1.9.2 engine and includes several interface improvements, such as "personas". This release was referred to as 3.2 before 3.1 was changed to 3.5. The codename for this version was Namoroka. This is the last major, official version to run on PowerPC-based Macintoshes.
One minor update to Firefox 3.6, version 3.6.4 (code-named Lorentz) is the first minor update to make non-intrusive changes other than minor stability and security fixes. It adds Out of Process Plugins (OOPP), which runs plugins in a separate process, allowing Firefox to recover from plugin crashes.
Firefox 3.6.6 lengthens the amount of time a plug-in is allowed to be unresponsive before the plug-in quits.
Support for Firefox 3.6 ended on April 24, 2012.




Firefox 3.7 (Gecko 1.9.3) Alpha 1 was released on February 10, 2010. Alpha 2 was released on March 1, 2010, Alpha 3 on March 17, Alpha 4 on April 12, and Alpha 5 on June 16.
The version number was changed to 4.0 (and Gecko's was changed to 2.0) starting with Beta 1, released on July 6, 2010.
Beta 2 was released on July 27, Beta 3 on August 11, Beta 4 on August 24, Beta 5 on September 7, Beta 6 (a chemspill release) on September 14.
After major delays, Beta 7 was finally released on November 10. Beta 7 was followed by Beta 8, released on December 21. Beta 9 was released on January 14, 2011, Beta 10 on January 25, Beta 11 on February 8, and Beta 12 on February 12.
Firefox then moved into the RC stage. The final version of Firefox 4 was released on March 22, 2011.
Version 4 brought a new user interface and is said to be faster. Early mockups of the new interface on Windows, Mac OS X, and Linux were first made available in July 2009. Other new features included improved notifications, tab groups, application tabs, a redesigned add-on manager, integration with Firefox Sync, and support for multitouch displays.
On October 13, 2006, Brendan Eich, Mozilla's Chief Technology Officer, wrote about the plans for "Mozilla 2", referring to the most comprehensive iteration (since its creation) of the overall platform on which Firefox and other Mozilla products run. Most of the objectives were gradually incorporated into Firefox through versions 3.0, 3.5, and 3.6. The largest changes, however, were planned for Firefox 4.
Firefox 4 was based on the Gecko 2.0 engine, which added or improved support for HTML5, CSS3, WebM, and WebGL. It also included a new JavaScript engine (J gerMonkey) and better XPCOM APIs.



In April 2011, the development process was split into several "channels", each working on a build in a different stage of development. The most recent available build is called "Nightly Builds" and offers the latest, untested features and updates. The "Aurora" build is up to six weeks behind "Nightly" and offers functionality that has undergone basic testing. As of version 35, the "Aurora" channel has been renamed to the "Developer Edition" channel. The "Beta" channel is another six weeks away. It provides improved stability over the nightly builds and is the first development milestone that has the "Firefox" logo. "Release" is the current official version of Firefox.
New releases are planned to occur at six-week intervals. The stated aim of this faster-paced process is to get new features to users faster. This accelerated release cycle was met with criticism by users, as it often broke addon compatibility, as well as those who believe Firefox was simply trying to increase its version number to compare with other browsers such as Google Chrome.



Firefox 5 was released on June 21, 2011, three months after the major release of Firefox 4. Firefox 5 is the first release in Mozilla's new rapid release plan, matching Google Chrome's rapid release schedule and rapid version number increments. Version 5 significantly improved the speed of web-related tasks, such as loading pages with combo boxes or MathML. Mozilla also integrated the HTML5 video WebM standard into the browser, allowing playback of WebM video.



Mozilla released its Mozilla Firefox 6.0 on August 16, 2011. The update brought: permissions manager, new address bar highlighting (the domain name is black while the rest of the URL is gray), streamlining the look of the site identity block, quicker startup time, a ScratchPad JavaScript compiler, and many other new features. This update also brought the infamous feature that JavaScript entered in the address bar does not run.



Firefox 7, released September 27, 2011, uses as much as 50% less memory than Firefox 4 as a result of the MemShrink project to reduce Firefox memory usage. Mozilla Firefox 7.0.1 was released a few days later, fixing a rare, but serious, issue with add-ons not being detected by the browser. The "http://" protocol indicator no longer appears in the URL.



Firefox 8 was released on November 8, 2011. Firefox 8 verified that users really wanted any previously installed add-ons. Upon installation, a dialog box prompted users to enable or disable the add-ons. Add-ons installed by third-party programs were disabled by default, but user-installed add-ons were enabled by default. Mozilla judged that third-party-installed add-ons were problematic, taking away user control, lagging behind on compatibility and security updates, slowing down Firefox startup and page loading time, and cluttering the interface with unused toolbars.



Firefox 9 was released on December 20, 2011; version 9.0.1 was released a day later. Firefox 9 includes various new features such as Type Inference, which boosts JavaScript performance up to 30%, improved theme integration for Mac OS X Lion, added two finger swipe navigation for Mac OS X Lion, added support for querying Do Not Track status via JavaScript, added support for font-stretch, improved support for text-overflow, improved standards support for HTML5, MathML, and CSS, and fixed several security problems. It also features a large list of bug fixes.




Firefox 10 and Firefox ESR 10 were released on January 31, 2012. It is the first official extended support release. Firefox 10 hides the forward arrow button until there is a website to go forward to, or it is manually activated. Firefox 10 adds a Full Screen API and improved WebGL performance.
Firefox 10 assumed all add-ons were compatible with version 10, as long as they are written for at least Firefox 4. The add-on developer is able to alert Mozilla that the add-on is incompatible, overriding compatibility with version 10 or later. This new rule also does not apply to themes.
Firefox 10 added the CSS Style Inspector to the Page Inspector, which allow users to check out a site's structure and edit the CSS without leaving the browser.
Firefox 10 added support for CSS 3D Transforms and for anti-aliasing in the WebGL standard for hardware-accelerated 3D graphics. These updates mean that complex site and Web app animations will render more smoothly in Firefox, and that developers can animate 2D objects into 3D without plug-ins.




Firefox 11 was released on March 13, 2012. Firefox 11 introduced many new features, including migration of bookmarks and history from Google Chrome, SPDY integrated services, Page Inspector Tilt (3D View), Add-on Sync, redesigned HTML5 video controls, and the Style Editor (CSS). The update also fixed many bugs, and improved developer tools.



Firefox 12 was released on April 24, 2012. Firefox 12 introduced few new features, but it made many changes and laid the ground work for future releases. Firefox 12 for Windows added the Mozilla Maintenance Service which can update Firefox to a newer version without UAC prompt. It also added line numbers in the "Page Source" and centered find in page results. There were 89 improvements to Web Console, Scratchpad, Style Editor, Page Inspector, Style Inspector, HTML view and Page Inspector 3D view (Tilt). Many bugs were fixed, as well as many other minor under-the-hood changes. Firefox 12 is the final release to support Windows 2000 and Windows XP RTM & SP1.




Firefox 13 was released on June 5, 2012. Firefox 13 adds and updates several features, such as an updated new tab and home tab page. The updated new tab page is a feature similar to the Speed Dial already present in Opera, Google Chrome, Apple Safari, and Windows Internet Explorer. The new tab page will display nine of the user's most visited websites, along with a cached image.
In addition to the updated new tab and home tab page, Mozilla has added a user profile cleaner/reset, reduced hang times, and implemented tabs on demand. The user profile cleaner/reset provides a way for users to fix Firefox errors and glitches that may occur. Mozilla's tabs on demand restores tabs that were open in the previous session, but will keep the tabs unloaded until the user requests to view the page.
Starting with this version, Windows support was exclusively for Windows XP SP2/SP3, Windows Vista, & Windows 7.



Firefox 14.0 for Android was released on June 26, 2012, just outside the regular release schedule of the web browser. In order to sync the version numbers of the desktop and mobile version of Firefox, Mozilla decided to release Firefox 14.0.1 for mobile and desktop on July 17, 2012, instead of Firefox 14.0 for the desktop and Firefox 14.0.1 for mobile clients.
A new hang detector (similar to how Mozilla currently collects other data) allows Mozilla to collect, analyze, and identify the cause of the browser freezing/hanging. Mozilla will use this information to improve the responsiveness of Firefox for future releases.
In addition to tackling freezing and not-responding errors that occur because of Firefox, Mozilla implemented opt-in activation for plugins such as Flash and Java. Mozilla wants to reduce potential problems that could arise through the unwanted use of third-party applications (malware, freezing, etc.).
URL complete will suggest the website that Firefox believes the user plans on visiting. It does this by inserting the remaining characters into the URL form box.
Firefox 14 has an optional GStreamer back-end for HTML5 video tag playback. This allows playback of H.264 if the codec is installed as a GStreamer plugin. GStreamer support is not enabled in the official builds, but can be enabled at compile time.
The first beta version of Firefox 14 was not beta 1, but beta 6 and was released on June 5, 2012.



Firefox 15 was released on August 28, 2012.
This version includes a "Responsive Design View" developer tool, adds support for the Opus audio format and adds preliminary native PDF support (turned off by default).
Silent updates automatically update Firefox to the latest version without notifying the user, a feature that the web browsers Google Chrome and Internet Explorer 8 and above have already implemented, although the user is able to disable that function.
Mozilla improved regular startup time for Windows users.




Firefox 16 was released on October 9, 2012.
Plans for this version called for fixing of bugs still outstanding involving support of new features in Mac OS X Lion, improvements to startup speed when a user wants to restore a previous session, and support for viewing PDF files inline without a plugin.
Opus audio format is now enabled by default. Support for web apps was added.
The roll-out of Firefox 16.0.0 was stopped on October 10, 2012, after Mozilla detected a security flaw and recommended downgrading to 15.0.1 until the issue could be fixed. The security flaw was fixed in version 16.0.1, which was released the following day, October 11, 2012. Version 16.0.2 was released October 26, 2012. It fixed additional security issues, and is also the final release to support Mac OS X Leopard.



Firefox 17 and Firefox ESR 17 were released on November 20, 2012. Firefox 17.0.1, with several bug fixes, launched on November 30, 2012.
Firefox 17 was not planned to bring as many user-facing features as previous releases; it brings improved display of location bar results, improvements to the silent update mechanism for users with incompatible add-ons, and refinements to the Click-To-Play system introduced in Firefox 14. A new feature for developers, an HTML tree editor is also included. Firefox 17 is the first version of the browser that uses SpiderMonkey 17.
Starting with this version, Mac OS X support is exclusively for Snow Leopard, Lion, and Mountain Lion.



Firefox 18 was released on January 8, 2013. A new feature for Firefox 18 is IonMonkey, Mozilla's next generation JavaScript engine; it also uses some functions of WebRTC.
Firefox 18.0.1 was released on January 18, 2013, and added several bug fixes. Firefox 18.0.2 was released on February 5, 2013.




Firefox 19 was released on February 19, 2013. Firefox 19 features a built-in PDF viewer. Firefox 19.0.1 was released on February 27, 2013 to fix stability issues for some AMD Radeon HD graphics cards in Windows 8. Firefox 19.0.2 was released on March 7, 2013 to address a security vulnerability in the HTML editor.



Firefox 20 was released on April 2, 2013. A new feature of Firefox 20 is a panel-based download manager, along with H.264 decoding on the <video> tag (on Windows only), and per-window private browsing (per-tab private browsing on Android). It also includes a new developer toolbox, that combines all developer tools into one panel.
Firefox 20.0.1 was released on April 11, 2013, and included a Windows-only update to handle issues around handling UNC paths.



Firefox 21 was released on May 14, 2013. The Social API now supports multiple providers, enhanced three-state UI for Do Not Track (DNT).



Firefox 22 was released on June 25, 2013. WebRTC is now enabled by default. Partial CSS Flexbox support added (flex-wrap support is currently scheduled for Firefox 28). A new feature for Firefox 22 was OdinMonkey, Mozilla's next generation JavaScript engine.




Firefox 23 was released on August 6, 2013. It includes an updated Firefox logo, mixed content blocking enabled by default to defend against man-in-the-middle attacks, implementation of the <input type="range"> form control attribute in HTML5, dropping support for the <blink> HTML element as well as text-decoration:blink CSS element, the restriction to have to "switch to a different search provider across the entire browser", and a global browser console, a new network monitor among other things. JavaScript is automatically enabled by the update, without regard to the previous setting, and the ability to turn it off has been removed from the interface; the "contentious" change was made because many websites depend on JavaScript and it was felt that users unaware that they had disabled JavaScript were attributing the resulting unpredictable layout to software bugs in Firefox.
The keyword.URL preference now is no longer supported, making it no longer possible to specify the search engine for the URL bar that way. The search engine selected for the search bar on the Navigation Toolbar is now automatically used also for the URL bar and about:home page.
Firefox 23.0.1 was released ten days later, August 16, 2013, to fix the rendering glitches on H.264 video only in FF23 on Windows Vista; it also fixed the spell checking that was broken with non-ASCII characters in profile path, and the audio static/"burble"/breakup in Firefox to Firefox WebRTC calls.



Firefox 24 and Firefox 24 ESR were released on September 17, 2013. The release includes support for the new scrollbar style in Mac OS X 10.7 (and newer), closing tabs to the right, an improved browser console for debugging, and improved SVG rendering, among other things. Firefox 24 is the first version of the browser that uses SpiderMonkey 24.



Firefox 25 was released on October 29, 2013. Firefox 25 Nightly was at one point slated to include the Australis theme, but Australis did not actually land on Nightly until Firefox 28, did not make it to Firefox 28 Aurora channel, and was finally available with Firefox 29. This release added support for <iframe srcdoc> attribute, background-attachment:local in CSS, along with Web audio API support, separate find bar for each tab and many other bug fixes.
Firefox 25.0.1 was released on November 15, 2013, to address issues with pages that sometimes wouldn't load without first moving the cursor.



Firefox 26 was released December 10, 2013. Firefox 26 changed the behavior of Java plugins to "click-to-play" mode instead of automatically running them. It also added support for H.264 on Linux, password manager support for script-generated fields, and the ability for Windows users without advanced write permissions to update Firefox, as well as many bug fixes and developer-related changes.
Firefox 26.0.1 was released only for Android on December 20, just ten days after the desktop release. It fixed screen distortion on some devices after tapping on search suggestion.



Firefox 27 was released on February 4, 2014. It adds improved Social API and SPDY 3.1 support, as well as enabling of TLS 1.1 and 1.2 by default. Also, it brings many bug fixes, security improvements, and developer-related changes.
Firefox 27.0.1 was released on February 13, 2014. It fixed stability issues with Greasemonkey and other JavaScript that used ClearTimeoutOrInterval, as well as JavaScript math correctness issues.



Firefox 28 was released on March 18, 2014. It added support for VP9 video decoding and support for Opus in WebM. For Android, features such as predictive lookup from the address bar, quick share buttons and support for OpenSearch were added.
Firefox 28.0.1 was released only for Android six days later. It fixed H.264 video playback issues on several Galaxy devices and includes a mobile-only security fix for file: URLs.




Firefox 29 was released on April 29, 2014 and includes the Australis interface; it also removes the add-on bar and moves its content to the navigation bar.
Firefox 29.0.1 was released ten days later, fixing a few bugs.



Firefox 30 was released on June 10, 2014. It adds support for GStreamer 1.0 and a new sidebar button, and most plugins are not activated by default.



Firefox 31 and Firefox 31 ESR were released on July 22, 2014. Both versions added search field on the new tab page and were improved to block malware from downloaded files, along with other new features. Firefox 31 ESR is the first ESR to include the Australis interface, unifying the user experience across different Firefox versions. Firefox 24.x.x ESR versions will be automatically updated to ESR version 31 after October 14, 2014.



Firefox 32 was released on September 2, 2014. It shows off HTTP caching improvements, adds HiDPI/Retina support in the Developer Tools UI and widens HTML5 support, among other things. Firefox 32.0.1 was released for mobile only on September 10, fixing the link tap selection that is offset on some Android devices; and for desktop two days later, fixing stability issues. Firefox 32.0.2 was released for desktop only on September 18, fixing the corrupt installations causing Firefox to crash on update. Firefox 32.0.3 was released for desktop and Android on September 24, fixing a security vulnerability.



Firefox 33 was released on October 14, 2014. It now has off-main-thread compositing (OMTC) enabled by default on Windows (which brings responsiveness improvements), OpenH264 support, search suggestions on about:home and about:newtab, address bar search improvements, session restore reliability improvements, and other changes.
Firefox 33.0.1 was released for desktop only on October 24, 2014, fixing displaying of a black screen at startup with certain graphics drivers. Firefox 33.0.2 was released for desktop only on October 28, 2014, fixing a startup crash with some combination of hardware and drivers. Firefox 33.0.3 was released for desktop only on November 6, 2014, fixing several issues related to graphics drivers.
Firefox 33.1 was released on November 10, 2014, celebrating Firefox's 10-year anniversary. Firefox 33.1.1 was released for desktop only on November 14, 2014, fixing a startup crash.



Firefox 34.0 was released on December 1, 2014. It brings Firefox Hello (a WebRTC client for voice and video chat), an improved search bar, and the implementation of HTTP/2 (draft14) and ALPN, together with other features. It also disables SSL v3, and enables the ability to recover from a locked Firefox process and to switch themes and personas directly in the customization mode.
Firefox 34.0.5 was released for desktop only on December 1, 2014, changing the default search engine to Yahoo! for North America.



Firefox 35.0 was released on January 13, 2015. It brings support for a room-based conversations model to the Firefox Hello chat service, and other functions; it includes security fixes.
Firefox 35.0.1 was released first for desktop on January 26, 2015, fixing various issues, and then for Android on February 5, 2015, fixing a crash with video playback on Asus MeMO Pad 10 and 8, Tesco Hudl, Lenovo Lifetab E models, and several other devices running the Rockchip SoC.



Firefox 36.0 was released for desktop on February 24, 2015, bringing full HTTP/2 support and other smaller improvements and fixes. It was also released for Android three days later, adding support for the tablet user interface.
Firefox 36.0.1 was released for desktop on March 5, 2015, and the next day for Android, fixing various issues.
Firefox 36.0.2 was released for Android only on March 16, 2015, fixing a startup crash on HTC One M8 devices (Verizon) with Android 5.0.1 and some potential crashes with Flash videos.
Firefox 36.0.3 was released on March 20, 2015; soon after that, Version 36.0.4 was released on March 21, 2015, for desktop and Android, fixing security issues disclosed at HP Zero Day Initiative's Pwn2Own contest.



Firefox 37 was released on March 31, 2015, bringing a heartbeat user rating system, which provides user feedback about the Firefox, and improved protection against website impersonation via OneCRL centralized certificate revocation. Also, Bing search is changed to use HTTPS for secure searching, and added is support for opportunistic encryption of the HTTP traffic where the server supports HTTP/2's AltSvc feature.
Firefox 37.0.1 was released on April 3, 2015 for desktop and Android, fixing security issues and several crash issues. It also disabled opportunistic encryption of the HTTP traffic introduced in 37.0.
Firefox 37.0.2 was released for Android on April 14, 2015, fixing an issue related to the "request desktop site" feature, and for desktop on April 20, 2015, fixing a Google Maps rendering issue, stability issues for some graphics hardware and feature sets, and certain security issues.



Both Firefox 38 and Firefox 38 ESR were released on May 12, 2015, with new tab-based preferences, Ruby annotation support and availability of WebSockets in web workers, along with the implementation of the BroadcastChannel API and other features and security fixes.
Firefox 38.0.1 and ESR 38.0.1 were released on May 14, 2015 for desktop, fixing a number of stability issues. Firefox 38.0.1 for Android was released on May 15, 2015, fixing a number of stability issues.
Firefox 38.0.5 was released on June 2, 2015 for desktop and Android, fixing bugs and security issues, and adding new functionality that included integration of Pocket and availability of Reader View mode. This was the first release offered to the Release channel users since 38.0.1.



Firefox 39 was released on July 2, 2015 for desktop and Android, disabling insecure SSLv3 and RC4, improving performance for IPv6 fallback to IPv4 and including various security fixes. Firefox 39.0.3 was released on August 6, 2015, to fix a zero-day exploit.



Firefox 40 was released on August 11, 2015 for desktop and Android. On Windows 10, the Australis theme was updated to reflect the overall appearance of Windows 10, and the interface is adapted for usability on touchscreens when used in the operating system's "Tablet mode". Firefox 40 includes additional security features, including the filtering of pages that offer potentially unwanted programs, and warnings during the installation of unsigned extensions; in future versions, signing of extensions will become mandatory, and the browser will refuse to install extensions that have not been signed. Firefox 40 also includes performance improvements, such as off-main-thread compositing on Linux.
Firefox 40.0.2 was released for desktop only two days later on August 13, fixing some stability issues.
Firefox 40.0.3 was released for desktop and Android on August 27, fixing some stability issues and security vulnerabilities.



Firefox 41 was released on September 22, 2015 for desktop and Android. Among many additions are the ability to set a profile picture for a Firefox account, enhanced IME support using Text Services Framework, and instant messaging on Firefox Hello.
Firefox 41.0.1 was released for desktop only on September 30, 2015, fixing some stability issues.




Test builds can be downloaded from the Firefox development channels: "Beta", "Developer Edition" (former Aurora) and "Nightly" (Central). As of September 22, 2015, Firefox 42 beta is in the "Beta" channel, Firefox 43 alpha is in the "Aurora" channel, and Firefox 44 pre-alpha is in the "Nightly" (Central) channel.



In January 2012, the Mozilla Foundation announced the availability of an Extended Support Release (ESR) version of Firefox. In addition to the "release", "beta", and "aurora" update channels the ESR versions form the "esr" update channel.
Firefox ESR is intended for groups who deploy and maintain the desktop environment in large organizations such as universities and other schools, county or city governments and businesses. During the extended cycle, no new features will be added to a Firefox ESR; only high-risk/high-impact security vulnerabilities or major stability fixes will be corrected.
An Extended Support Release includes continuity of support through 9 normal Firefox rapid release cycles (54 weeks), with the final 2 cycles overlapping the next version. ESR versions will jump from 10 to 17, then to 24 etc.
Every six weeks when a new mainstream Firefox release is made under the rapid release cycle, a corresponding security update would also be released for the then-current ESR version. For example, ESR 10.0.1 would be expected to be released at the same time as Firefox 11, ESR 10.0.2 at the same time as Firefox 12. Security updates for ESR versions are also released when out-of-band security updates are made available for mainstream Firefox releases, for example ESR 10.0.10 corresponds with Firefox 16.0.2. At Firefox 17 and Firefox 18, there would be two ESR versions supported. Respectively, ESR 10.0.11 and ESR 17.0.0; ESR 10.0.12 and ESR 17.0.1. Finally, when Firefox reaches 19.0, ESR 10 would go end-of-life alongside the release of ESR 17.0.2. The cycle repeats again.
After the end-of-life with ESR 10.0.12 the Firefox Updater suggested to update to ESR 17.0.x on supported platforms.
The numbering scheme changed somewhat starting with ESR 24.0.0 series. The first minor version number increments on regularly scheduled six-week release cycle, and the second minor version number increments when unscheduled off-cycle releases are necessary. For example, ESR 24.1.0 was released at the same time as 25.0.0, ESR 24.1.1 was released at the same time as 25.0.1, and ESR 24.2.0 was released at the same time as 26.0.0.







Notes
Green color denotes current Firefox versions, while the pink colour is for older versions.
FreeBSD provides a regularly updated port of Firefox.
Firefox 3.5.9 is the last version to work on HP-UX 11i, as packaged by Hewlett-Packard.
Firefox 2.0 has been ported to RISC OS (i.e. not supported by Mozilla).
In March 2014, the Windows Store app version of Firefox was cancelled, although there is a beta release.


