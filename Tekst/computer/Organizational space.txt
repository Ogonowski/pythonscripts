Organizational space describes the influence of the spatial environment on the health, the mind, and the behavior of humans in and around organizations. It is an area of scientific research in which interdisciplinarity is a central perspective. It draws from management, organization and architecture (Dale and Burrell, 2008) added with knowledge from, for instance, environmental psychology (Evans and Mitchell, 1998), social medicine (Macintyre et al., 2002), or spatial science (Festinger et al., 1950). In essence, it may be regarded as a special field of expertise of organization studies and change management applied to architecture. The knowledge area is related to evidence-based design in which the influence of the spatial environment on patient's health, healing, and customer satisfaction are being researched in health care. It is also related to practice-based areas of management such as facility management which is primarily devoted to the maintenance and care of commercial or institutional buildings and to property management in which the operation of real estate is central. Sometimes it is also referred to as organizational architecture. The scientific field of organizational space must be distinguished from social architecture in which the development of information and communication technologies is central and also different from space science which is concerned with the study of the universe.



This research strand distinguishes three different environments: the spatial environment from the physical environment and the built environment.
Spatial environment: the total context in which humans in and around organizations function.
Physical environment: all tangible physical entities in and around organizations.
Built environment: that what is commonly understood as architecture and that what is permanently fixed to it.



The coherence between the organization and its spatial environment may be regarded as an interwoven interdisciplinary cyclical flux from contingencies, intermediates, performances to interventions (Mobach, 2009). The contingencies are the organizational, architectural, technological, and natural conditions under which organization function. In the end they influence the performance of an organization, but first they mix in the intermediates. In this way humans in and around organizations will, for instance, notice these contingencies and will give them meaning (Clegg and Kornberger, 2006; Van Marrewijk and Yanow, 2010). Moreover, the contingencies will also influence social contact (Becker, 1981; Steele, 1973) and the degree to which a spatial environment can be functional (Sharles, 1923). Subsequently, the intermediates influence different performances, for instance, the health, the mind, and the behavior of people in and around organizations. The spatial environment can cause illness, such as with the sick building syndrome (EPA, 1991), but it can also positively influence the vitality of people or the recovery after an operation (Ulrich, 1984). The performances can provoke managerial intervention. In turn, these interventions will change the contingencies, and by doing so, change the elements, relations, and properties of the conditions under which people function.



Becker, F.D. 1981. Workspace: Creating Environments in Organizations. New York: Praeger. ISBN 0-03-059137-6
Clegg, S.R., Kornberger, M. (eds.). 2006. Space, Organizations and Management Theory. Copenhagen: Liber & CBS Press. ISBN 87-630-0164-0
Dale, K., Burrell, G. 2007. The Spaces of Organisation & The Organization of Space -Power, Identity & Materiality at Work. Basingstoke: Palgrave MacMillan. ISBN 0-230-57268-5
EPA (U.S. Environmental Protection Agency). 1991. Office of Air and Radiation. Indoor Air Facts No. 4: Sick Building Syndrome Washington: EPA.
Evans, G.W., Mitchell, J. 1998. When Buildings Don t Work: The Role of Architecture in Human Health. Journal of Environmental Psychology. 18: 85-94.
Festinger, L., Schachter, S., Back, K. 1950. Social Pressures in Informal Groups -A Study of Human Factors in Housing. Stanford: Stanford University Press. ISBN 0-8047-0173-3
Macintyre, S., Ellaway, A., Cummins, S. 2002. Place Effects on Health: How Can We Conceptualise, Operationalise and Measure Them? Social Science & Medicine. 55(1): 125-139.
Marrewijk, A.H. van, Yanow, D. (eds.) 2010. Organizational Spaces. Rematerializing the Workaday World. Cheltenham: Edward Elgar. ISBN 978-1-84844-650-2
Mobach, M.P. 2009. Een organisatie van vlees en steen. Assen: Van Gorcum. ISBN 978-90-232-4531-5
Sharles, F.F. (ed.). 1923. Business Building -A Complete Guide to Business for the Wholesaler, Retailer, Manufacturer, Agent etc. Volume I. London: Pitman.
Steele, F. 1973. Physical Settings and Organization Development. Addison-Wesley: Reading Massachusetts. ISBN 0-201-07211-4
Ulrich, R.S. 1984. View Through a Window May Influence Recovery From Surgery. Science. 224(4647): 420-421.