Xolo (styled as XOLO) is an Indian smart devices brand launched in early 2012 by Lava International. Its head office is located at Noida, Uttar Pradesh, India.



Xolo launched the Xolo X900 in April 2012 as its first model, which is the world's first smartphone with an Intel processor. Xolo was also the first Indian manufacturer to partner with AMD to launch the Xolo Win Tablet.
In December 2013, Xolo announced the first 4G smartphone in India   the LT900.
On 6 January 2014, Liverpool FC announced Xolo as its first regional partnership in India.
In August 2014, Xolo launched its own proprietary user interface - Hive UI.






Play Tegra (tablet, 2014),
Xolo Q700 (smartphone, 2014),
Xolo Q1000 Opus (smartphone, 2014)
Xolo Q2000 (smartphone, 2013)
Xolo Q1010i (smartphone, 2014)
Xolo Omega 5.0 & 5.5 
Xolo 8X-1000 (Xolo HIVE) (2014)
Xolo Play 8X-1100 (December 2014)
Xolo Play 8X-1200 (2015)
Xolo Black (2015)
Xolo One Liverpool FC limited edition Smartphone launched on 14 May 2015. The mobile features exclusive Liverpool FC wall papers and Liverpool FC printed back.
Xolo Cube 5.0 (2015) 



Xolo Win Q900s (smartphone, 2014)
Xolo Win Q1000 (smartphone, 2015)



XOLO introduce with Intel powered smartphone, and makes various Intel devices such as India's first Intel phone XOLO X900 and many more.



XOLO is the India's one and only brand that focused on gamers, and makes various gaming devices dubbed as Play range and create a game store especially for Play devices the 'Play Zone'. such Play devices are XOLO Play Tegra Note, Play Tab 7.0 Tablets, and smartphones are XOLO Play T1000, Play 6X-1000, Play 8X-1100, Play 8X-1200.



In August 2014, XOLO introduces its own in-house design proprietary OS HIVE Inside based on KitKat 4.4.4, and first HIVE powered smartphone released on 6 August'14 the XOLO 8X-1000 at 13999 INR. and later XOLO introduces two other HIVE powered smartphone in December 2014 XOLO Omega 5.0 and Omega 5.5.



In December 2013, XOLO introduces India's first 4G LTE smartphone XOLO LT900 priced at 17999 bucks in India.



In 2013, XOLO signed Bollywood actor Ayushmann Khurrana to endorse various XOLO products.
On 6 January 2014, Liverpool FC announced XOLO as its first regional partnership in India for 3 years, and they've invest 1 billion INR to promote both company in South Asia.



In September 2014, Lava introduced XOLO in the Gulf and its first country was UAE and later to expand other Gulf states such as Oman, Bahrain, Kuwait, Qatar and Iraq.


