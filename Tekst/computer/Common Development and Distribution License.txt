Common Development and Distribution License (CDDL) is a free software license, produced by Sun Microsystems, based on the Mozilla Public License (MPL). Files licensed under the CDDL can be combined with files licensed under other licenses, whether open source or proprietary. In 2005 the Open Source Initiative approved the license. The Free Software Foundation considers it a free software license that is incompatible with the GNU General Public License (GPL).



The previous software license used by Sun for its free software/open source projects was the Sun Public License (SPL), also derived from the Mozilla Public License. The CDDL license is considered by Sun (now Oracle) to be SPL version 2.
The CDDL was developed by a Sun Microsystems team (among them Solaris kernel engineer Andrew Tucker and Claire Giordano), based on the MPL version 1.1. On December 1, 2004 the CDDL was submitted for approval to the Open Source Initiative and was approved as an open source license in mid January 2005. The second CDDL proposal, submitted in early January 2005, includes some corrections that prevent the CDDL from being in conflict with European Copyright law and to allow single developers to use the CDDL for their work.
In 2006 in the first draft of the OSI's license proliferation committee report, the CDDL is one of nine preferred licenses listed as popular, widely used, or with strong communities.
While also the Free Software Foundation considered the CDDL a free software license, they saw some incompatible with their GNU General Public License (GPL).



The question if and when both licenses are incompatible sparked controversial debates in the FOSS domain in 2004 to 2006.
For instance the FSF considered the CDDL incompatible to their GPL license (without going to details).
Some describe the incompatibility as inherited from the MPL 1.1 (fixed with the MPL 2.0 according to the FSF) and as a complex interaction of several clauses.
Some of the CDDL proponents describe the GPL/CDDL compatibility situation from another point of view, they see the problem more on the GPL side than the CDDL side.
Some people argue that Sun (or the Sun engineer) as creator of the license made the CDDL intentionally GPL incompatible. According to Danese Cooper, who is no longer with Sun, one of the reasons for basing the CDDL on the Mozilla license was that the Mozilla license is GPL-incompatible. Cooper stated, at the 6th annual Debian conference, that the engineers who had written the Solaris kernel requested that the license of OpenSolaris be GPL-incompatible. "Mozilla was selected partially because it is GPL incompatible. That was part of the design when they released OpenSolaris. [...] the engineers who wrote Solaris [...] had some biases about how it should be released, and you have to respect that." Simon Phipps (Sun's Chief Open Source Officer at the time), who had introduced Ms. Cooper as "the one who actually wrote the CDDL", did not immediately comment, but later in the same video, he says, referring back to the license issue, "I actually disagree with Danese to some degree", while describing the strong preference among the engineers who wrote the code for a BSD-like license, which was in conflict with Sun's preference for something copyleft, and that waiting for legal clearance to release some parts of the code under the then unreleased GNU GPL v3 would have taken several years, and would probably also have involved massed resignations from engineers (unhappy with either the delay, the GPL, or both this is not clear from the video). Later, in September 2006, Phipps rejected Cooper's assertion in even stronger terms.
The GPL compatibility question was also the source of a controversy behind a partial relicensing of cdrtools to the CDDL which had been previously all GPL. In 2006 the Debian project declared the cdrtools legally undistributable because the build system was licensed under the CDDL, even though the GPL requires that all scripts required to build the work to be licensed freely but not necessarily under the GPL, thus not causing an incompatibility that violates the license. The author J rg Schilling claims smake to be an independent project and therefore not violating the GPLv3. He argues, in "combined works" (in contrast to "derived works") GPL and CDDL licensed code is compatible.



Example products released under CDDL:
OpenSolaris (including DTrace, initially released alone, and ZFS)
NetBeans IDE and RCP
GlassFish
JWSDP
Project DReaM
Bourne shell
cdrtools


