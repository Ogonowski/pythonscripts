The Federal Council is the seven-member executive council which constitutes the federal government of Switzerland and serves as the collective head of government and state of Switzerland.
While the entire council is responsible for leading the federal administration of Switzerland, each Councillor heads one of the seven federal executive departments. The position of Federal President rotates among the seven Councillors on a yearly basis, with the year's Vice President becoming next year's President.



The current members of the Federal Council are, in order of seniority:






The Federal Council was instituted by the 1848 Federal Constitution as the "supreme executive and directorial authority of the Confederation".
When the Constitution was written, constitutional democracy was still in its infancy, and the founding fathers of Switzerland had little in the way of examples. While they drew heavily on the U.S. Constitution for the organisation of the federal state as a whole, they opted for the collegial rather than the presidential system for the executive branch of government. This accommodated the long tradition of the rule of collective bodies in Switzerland. Under the Ancien R gime, the cantons of the Old Swiss Confederacy had been governed by councils of pre-eminent citizens since time immemorial, and the later Helvetic Republic (with its equivalent Directorate) as well as the cantons that had given themselves liberal constitutions since the 1830s had also had good experiences with that mode of governance.
Today, only three other states, Bosnia and Herzegovina, Andorra and San Marino, have collective rather than unitary heads of state. However the collegial system of government has found widespread adoption in modern democracies in the form of cabinet government with collective responsibility.




The 1848 constitutional provision providing for the Federal Council   and indeed the institution of the Council itself   has remained unchanged to this day, even though Swiss society has changed profoundly since. Nonetheless, some significant developments deserve to be mentioned here.







The 1848 Constitution was one of the few successes of the democratic revolutions of 1848. In Switzerland, the democratic movement was led   and the new federal state decisively shaped   by the Radicals (presently the Free Democratic Party, FDP). After winning the Sonderbundskrieg (the Swiss civil war) against the Catholic cantons, the Radicals at first used their majority in the Federal Assembly to fill all the seats on the Federal Council. This made their former war opponents, the Catholic-Conservatives (presently the Christian Democratic People's Party, CVP), the opposition party. Only after Emil Welti's resignation in 1891 after a failed referendum on railway nationalisation did the Radicals decide to co-opt the Conservatives by supporting the election of Josef Zemp.



The process of involving all major political movements of Switzerland into the responsibility of government continued during the first half of the 20th century. It was hastened by the FDP's and CVP's gradually diminishing voter shares, complemented by the rise of new parties of lesser power at the ends of the political spectrum. These were the Social Democratic Party (SP) on the Left and the Party of Farmers, Traders and Independents (BGB; presently the People's Party, SVP) on the Right. In due course, the CVP received its second seat in 1919 with Jean-Marie Musy, while the BGB joined the Council in 1929 with Rudolf Minger. In 1943, during World War II, the Social Democrats were also temporarily included with Ernst Nobs.




The 1959 elections, following the resignation of four Councillors, finally established the Zauberformel, the "magical formula" that determined the Council's composition during the rest of the 20th century and established the long-standing nature of the Council as a permanent, voluntary grand coalition. In approximate relation to the parties' respective strength in the Federal Assembly, the seats were distributed as follows:
Free Democratic Party (FDP): 2 members,
Christian Democratic People's Party (CVP): 2 members,
Social Democratic Party (SP): 2 members, and
Swiss People's Party (SVP): 1 member.
During that time, the FDP and CVP very slowly but steadily kept losing voter share to the SVP and SPS, respectively, which overtook the older parties in popularity during the 1990s.




The governmental balance was changed after the 2003 elections, when the SVP was granted a Council seat for their leader Christoph Blocher that had formerly belonged to the CVP's Ruth Metzler. Due to controversies surrounding his conduct in office, a narrow Assembly majority did not reelect Blocher in 2007 and chose instead Eveline Widmer-Schlumpf, a more moderate SVP politician, against party policy. This led to a split of the SVP in 2008. After liberal regional SVP groups including Federal Councillors Widmer-Schlumpf and Samuel Schmid founded a new Conservative Democratic Party, the SVP was left in opposition for the first time since 1929, but returned into the Council with the election of Ueli Maurer on 10 December 2008, who regained the seat previously held by Schmid, who had resigned.



Women gained suffrage on the federal level in 1971. They remained unrepresented in the Federal Council for three further legislatures, until the 1984 election of Elisabeth Kopp. In 1983, the failed election of the first official female candidate, Lilian Uchtenhagen and again in 1993 the failed election of Christiane Brunner (both SP/PS), was controversial and the Social Democrats each time considered withdrawing from the Council altogether.
There were two female Councillors serving simultaneously for the first time in 2006, and three out of seven Councillors were women from 2007 till 2010, when Simonetta Sommaruga was elected as the fourth woman in government in place of Moritz Leuenberger, putting men in minority for the first time in history. Also remarkable is the fact that the eighth non-voting member of government, the Chancellor who sets the government agenda, is also a woman.
In total, there have been seven female Councillors in the period 1989 to present:
The first woman Councillor, Elisabeth Kopp (FDP/PRD), elected 1984, resigned in 1989.
Ruth Dreifuss (SP/PS), served from 1993 to 2002, was the first woman to become President of the Confederation in 1999.
Ruth Metzler (Metzler-Arnold at the time) (CVP/PDC), served from 1999 to 2003 and was not re-elected to a 2nd term (see above).
Micheline Calmy-Rey (SP/PS), elected in 2003 and Doris Leuthard (CVP/PDC), elected in 2006, were the first two women serving simultaneously. Both were reelected in December 2007 for a four-year term.
Eveline Widmer-Schlumpf was elected in December 2007.
Simonetta Sommaruga was elected in September 2010. Together with Micheline Calmy-Rey, Doris Leuthard and Eveline Widmer-Schlumpf, women had the majority in the Federal Council for the first time, until January 2012, when Alain Berset replaced Micheline Calmy-Rey.



Until 1999, the Constitution mandated that no canton could have more than one representative on the Federal Council. Until 1987, the place of origin was used to determine which canton a Federal Councilor was from. After 1987, the place of residence (or, for councilors who were previously members of the Federal Assembly or of a Canton's legislative or executive body, the canton from which they were elected) became the determinant factor. Nothing prevented candidates from moving to politically expedient cantons, though, and the rule was abandoned in 1999. Since then, the Constitution has mandated an equitable distribution of seats among the cantons and language regions of the country, without setting concrete quotas. Whenever a member resigns, s/he is generally replaced by someone who is not only from the same party, but also the same language region. In 2006, however, Joseph Deiss, a French Swiss, resigned and was succeeded by Doris Leuthard, a German-speaking Swiss.
Historically, at least two Council seats have always been held by French- or Italian-speaking Swiss, and no canton has in fact ever had more than one of its citizens on the Federal Council. From 2003 to 2007, however, two of the members of the Federal Council, Moritz Leuenberger and Christoph Blocher, have resided in the Canton of Z rich. In the 2010 election, the two new Councillors Simonetta Sommaruga and Johann Schneider-Ammann were both from the Canton of Bern.
The current language makeup of the Council is five German speakers and two French speakers; an Italian speaker has not served on the Council since 1999.






Each year, one of the seven Councillors is elected by the United Federal Assembly as President of the Confederation. The Federal Assembly also elects a Vice President. By convention, the positions of President and Vice President rotate annually, each Councillor thus becoming Vice President and then President every seven years while in office.
According to the Swiss order of precedence, the President of the Confederation is the highest-ranking Swiss official. He or she presides over Council meetings and carries out certain representative functions that, in other countries, are the business of a head of state. In urgent situations where a Council decision cannot be made in time, he or she is empowered to act on behalf of the whole Council. Apart from that, though, he or she is a primus inter pares, having no power above and beyond the other six Councillors.
The President is not the Swiss head of state; this function is carried out by the Council in corpore, that is, in its entirety. However, it has recently become usual that the President acts and is recognized as head of state while conducting official visits abroad, as the Council (also by convention) does not leave the country in corpore. More often, though, official visits abroad are carried out by the head of the Federal Department of Foreign Affairs. Visiting heads of state are received by the Federal Council in corpore.



The Federal Council operates mainly through weekly meetings, which are held each Wednesday at the Federal Palace in Bern., the seat of the Swiss federal government.
Apart from the seven Councillors, the following officials also attend the meetings:
Federal Chancellor Corina Casanova. As government chief of staff and head of the Federal Chancellery, she participates in the discussion but has no vote in the Council's decisions. Nonetheless, her influential position is often referred to as that of an "eighth Federal Councillor".
the Vice-Chancellor: Andr  Simonazzi. Simonazzi is the spokesman of the Federal Council and conducts the weekly press briefing after the meeting.
the Vice-Chancellor: Thomas Helbling who is in charge of the Federal Council sector within the Swiss Federal Chancellery.
After the meetings, the Councillors always take lunch together. The Council also meets regularly in conclave to discuss important topics at length, and annually conducts what is colloquially referred to as its "field trip", a day trip to some attractions in the President's home canton. In that and other respects, the Council operates not unlike a board of directors of a major corporation.



Each Federal Councillor heads a government department, much like the ministers in the governments of other countries. Colloquially and by the press, they are often referred to as ministers, e.g. the head of the DDPS as "minister of defence", even though no such post officially exists. However, as Council members, they are not only responsible for their own department, but also for the business of their colleagues' departments as well, and for the conduct of the government and the federal administration as a whole.
Decisions to be taken by the Council are always prepared by the responsible department. For example, a change in the salaries of federal employees would be proposed to the council by the head of the Federal Department of Finance, to whose department the Federal Office of Personnel belongs. Before a vote is taken at a Council meeting, though, all proposals are circulated in writing to the heads of departments, who commission the senior career officials of their department   the heads of the Federal Offices   to prepare a written response to offer criticism and suggestions. This is called the co-report procedure (Mitberichtsverfahren/proc dure de co-rapport), designed to build a wide consensus ahead of a Council meeting.
To prepare for important decisions, an additional public consultation is sometimes conducted, to which the cantons, the political parties and major interest groups are invited, and in which all members of the public can participate. If a change in a federal statute is to be proposed to the Federal Assembly, this step is mandated by law. In such cases, the consultation procedure also serves to identify political concerns that could later be the focus of a popular referendum to stop passage of the bill at issue.
The decisions themselves are formally taken by voice vote by a majority of the Councillors present at a meeting. However, the great majority of decisions are arrived at by consensus; even though lately there is said to be a trend towards more contentious discussions and close votes.



The meetings of the Federal Council and the result of the votes taken are not open to the public, and the records remain sealed for 50 years. This has lately been the subject of some criticism. In particular, the parties at the ends of the political spectrum argue that this secrecy is contrary to the principle of transparency. However, the Council has always maintained that secrecy is necessary to arrive at consensus and to preserve the collegiality and political independence of the individual Councillors.
Despite the secrecy rule, details of the votes and the arguments in Council are sometimes leaked to the press, resulting in (generally fruitless) investigations and criminal prosecutions of the leaking staff member.



Due to the Federal Council's unique nature as a voluntary grand coalition of political opponents, its operation is subject to numerous constitutional conventions. Most notable is the principle of collegiality; that is, the Councillors are not supposed to publicly criticise one another, even though they are often political opponents. In effect, they are expected to publicly support all decisions of the Council, even against their own personal opinion or that of their political party. In the eye of many observers, this convention has become rather strained after the 2003 elections (see below).






The members of the Federal Council are elected for a term of four years by both chambers of the Federal Assembly sitting together as the United Federal Assembly. Each Councillor is elected individually by secret ballot by an absolute majority of votes. Every adult Swiss citizen is eligible, but in practice, only Members of Parliament or more rarely, members of Cantonal governments are nominated by the political parties and receive a substantial number of votes. The voting is conducted in several rounds, under a form of exhaustive ballot: in the first two rounds, anyone can enter their name; but in subsequent rounds, the person receiving the fewest votes is removed from the race until one candidate gains an absolute majority.
With Council seats allocated to parties by unwritten agreement (see above), Federal Council elections generally are unexciting, pleasant affairs. Usually, the party which has a seat to fill presents two candidates with mainstream viewpoints to the United Federal Assembly, which then chooses one. This was not so, however, during the 2003 election, which was the most controversial in recent memory (see also above).
Once elected, Councillors remain members of their political parties, but hold no leading office with them. In fact, they usually maintain a certain political distance from the party leadership, because under the rules of collegiality, they will often have to publicly promote a Council decision which does not match the political conviction of their party (or of themselves).



Once elected for a four-year-term, Federal Councillors can neither be voted out of office by a motion of no confidence nor can they be impeached. Re-election is possible for an indefinite number of terms, and it has historically been extremely rare for Parliament not to re-elect a sitting Councillor. This has only happened four times   to Ulrich Ochsenbein in 1854, to Jean-Jacques Challet-Venel in 1872, to Ruth Metzler-Arnold in 2003 and to Christoph Blocher in 2007. In practice, therefore, Councillors serve until they decide to resign and retire to private life, usually after three to five terms of office.






Unlike most senior members of government in other countries, the Federal Councillors are not entitled to an official residence (however, the Federal Palace houses living apartments for both the Federal Chancellor and President of the Confederation). Mostly, they have chosen to rent apartments or hotel suites in Bern (at their own expense), However, they are entitled to use the Federal Council's country estate, Lohn, for holidays; this estate is also used to host official guests of the Swiss Confederation.
While Councillors can draw on an Army security detail if they need personal protection (in particular during official events), it is more usual to encounter them without any escort at all in the streets, restaurants and tramways of Bern. Councillors are also entitled to a personal bailiff (Weibel) who accompanies them, in a colourful uniform, to official events. This tradition is directly traceable   through the republican governments of the ancient Swiss cantons   back to the lictors of the ancient Roman Republic.
The spouses of Councillors do not play an official part in the business of government, apart from accompanying the Councillors to official receptions.



Federal councillors receive an annual salary of CHF 445,000 (about EUR 416,000 / USD 451,000), plus another CHF 30,000 annually for expenses. The councillors do pay tax on this income.
Former counsillors with at least four years of service receive a pension equivalent to half the salary of Federal Council members in office. If a counsillor leaves office for health reasons, he or she may receive this pension even if his or her length of service was less than three years. Counsillors who leave offices for less than four years may also receive a partial pension. After leaving office, "former federal councillors frequently pursue some other lucrative activity," but "their earnings, when added to the pension they receive as an ex-federal councillor, may not exceed the salary of a federal councillor in office, otherwise their pension is reduced accordingly."
Serving federal councillors "enjoy a certain number of special benefits, from free telephone contracts to a chauffeur-driven car for official business, a courtesy car for personal use or the use of federal planes and helicopters for official business trips. Each member of the Federal Council also has the right to a first-class SBB GA travelcard (also in retirement). They are also given personal security, which is often very discreet."



Federal Councillors, like members of parliament, enjoy absolute legal immunity for all statements made in their official capacity.
Prosecution for crimes and misdemeanors that relate to the Councillors' official capacity requires the assent of the immunity commissions of the Federal Assembly. In such cases, Parliament can also suspend the Councillor in office (but not actually remove her or him).
According to statements to the media by a Federal Chancellory official, in none of the few cases of accusations against a Federal Councillor has the permission to prosecute ever been granted. Such cases usually involved statements considered offensive by members of the public. However, one unnamed Councillor involved in a traffic accident immediately prior to his date of resignation was reported to have voluntarily waived his immunity, and Councillor Elisabeth Kopp decided to resign upon facing an inquiry over allegations of secrecy violations.



Historically, the collegial government of Switzerland has been assessed both internationally and nationally as exceptionally competent and stable. The Federal Council as a whole (although not individual members) has consistently maintained public approval and confidence rates in excess of sixty percent, possibly also because under the Swiss system of direct democracy, voters can vent their displeasure with government decisions when deciding individual issues at the ballot box.
However, lately there has been a growing contention that the Federal Council is often too slow to respond to the needs of the moment, too resistant to change and too weak to lead the powerful federal bureaucracy. Various changes have been proposed to address these issues, including expanding the powers of the presidency, expanding the Federal Council itself or adding a second layer of ministers between the Council and the departments. However, none of these proposals have developed much further.




1848: The first seven members elected: Ulrich Ochsenbein, Jonas Furrer, Martin J. Munzinger, Henri Druey, Friedrich Frey-Heros , Wilhelm Matthias Naeff and Stefano Franscini.
1854: First (of only four so far) sitting Federal Councillors not to be reelected, Ulrich Ochsenbein.
1891: First Councillor of the Christian Democratic People's Party of Switzerland, Josef Zemp.
1893: First member whose father was a member of the Council: Eug ne Ruffy, son of Victor Ruffy. In 2007, the 2nd is elected: Eveline Widmer-Schlumpf, the daughter of Leon Schlumpf.
1911: First (and only) octogenarian in office, Adolf Deucher (FDP).
1913: First (and only) native Romansh speaker, Felix Calonder (FDP).
1917: First (and only) Councillor of the Liberal Party elected, Gustave Ador.
1930: First Councillor of the Party of Farmers, Traders and Independents (BGB/PAI; now the Swiss People's Party), Rudolf Minger.
1943: First Councillor of the Social Democratic Party, Ernst Nobs.
1983: First female candidate for the Council from a government party, Lilian Uchtenhagen (SP).
1984: First woman Councillor, Elisabeth Kopp (FDP).
1993: First Councillor of Jewish origin, Ruth Dreifuss (SP).
1995: First Councillor living in a domestic partnership, Moritz Leuenberger (SP) (with architect Gret Loewensberg, whom he later married).
1999: First woman President of the Confederation, Ruth Dreifuss (SP).
2010: First Majority of women in the Swiss Federal Council with the election of Simonetta Sommaruga (SP).


