The Penguin Group is a trade book publisher, part of Penguin Random House. It is owned by Pearson PLC, the global education and publishing company, and Bertelsmann, the German media conglomerate. The new company was created by a merger that was finalized on July 1, 2013, with Bertelsmann owning 53% of the joint venture, and Pearson controlling the remaining 47%.
Penguin Books has its registered office in City of Westminster, London.
Its British division is Penguin Books Ltd. Other separate divisions can be found in the United States, Ireland, New Zealand, India, Australia, Canada, China, and South Africa.



Penguin Books Ltd. (est. 1935) of the United Kingdom was bought over by Pearson Longman in 1970.
Penguin Group (USA) Inc. was formed in 1996 as a result of the merger between Penguin Books USA and the Putnam Berkley Group. The newly formed company was originally called Penguin Putnam Inc., but, in 2003, it changed its name to Penguin Group (USA) Inc. to reflect the parent Pearson PLC's grouping of all the Penguin companies worldwide under the supervisory umbrella of Pearson's own Penguin Group division.
The different Penguin companies use many imprints, many of which used to be independent publishers. Penguin Group (USA) Inc. also operates its own speaker's bureau that books speaking engagements for many of the publisher's authors. In 2011, the online writing and publishing community Book Country was launched as a subsidiary of Penguin Group USA.
In April 2012, the United States Department of Justice filed United States v. Apple Inc., naming Apple, Penguin, and four other major publishers as defendants. The suit alleged that they conspired to fix prices for e-books, and weaken Amazon.com's position in the market, in violation of antitrust law.
In October 2012, Pearson entered into talks with rival conglomerate Bertelsmann, over the possibility of combining their respective publishing companies, Penguin Group and Random House. The houses are considered two of the Big-Six publishing companies. The European Union approved of the Penguin Random House merger on April 5, 2013; Pearson controls 47% of the publisher.
In December 2013, a federal judge approved a settlement of the antitrust claims, in which Penguin and the other publishers paid into a fund that provided credits to customers who had overpaid for books due to the price-fixing.



Penguin Group imprints include the following:


