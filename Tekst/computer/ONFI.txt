This is a list of the world's largest technology companies by revenue. The list includes companies whose primary business activities are associated with technology industry which includes computer hardware, software, electronics, semiconductor, internet, telecom equipment, e-commerce and computer services. Note: The list is limited to companies with annual revenues exceeding 50 billion USD.






(All monetary values are in billions of dollars)


