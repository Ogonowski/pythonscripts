Succession of states is a theory and practice in international relations regarding the recognition and acceptance of a newly created sovereign state by other states, based on a perceived historical relationship the new state has with a prior state. The theory has its root in 20th century diplomacy. The term succession in international law does not have its normal meaning in English, which would imply automatic inheritance by the new state of the rights and obligations of the prior state. On the contrary, a successor state is a totally new state. This is distinct from a continuing state, also known as a continuator, which despite change to its borders maintains the same legal personality and possess all its existing rights and obligations.



Succession may refer to the transfer of rights, obligations, and/or property from a previously well-established prior state (the predecessor state) to the new one (the successor state). Transfer of rights, obligations, and property can include overseas assets (embassies, monetary reserves, museum artifacts), participation in treaties, membership in international organizations, and debts. Often a state chooses piecemeal whether or not it wants to be considered the successor state. A special case arises, however, when the predecessor state was signatory to a human rights treaty, since it would be desirable to hold the successor state accountable to the terms of that treaty, regardless of the successor state's desires.
In an attempt to codify the rules of succession of states the Vienna Convention on Succession of States in respect of Treaties was drafted in 1978. It entered into force on November 6, 1996.
Legal difficulties can arise at the dissolution of a larger territory into a number of independent states, when there is political contention about the outcome. One successor state may seek either to continue to be recognised under the same federal name of that of its predecessor or to assume the privileged position in international organisations held by the preceding federation. Such a state is then claiming to be a continuator state, when other states regard it as a successor state. When states are divided into two or more states, the question arises whether there is or is not a "continuator state". In the case of the split of Bangladesh from Pakistan, there was no challenge to Pakistan's claim to continue to exist and to retain its membership of the United Nations: it was a continuator and not a successor. Bangladesh eventually was recognised as a new state: it was a successor and had to apply for UN membership. On the other hand, when Czechoslovakia was dissolved, neither part claimed any continuity: both the Czech Republic and Slovakia were new successor states.



There are several recent examples where succession of states, as described above, has not been entirely adhered to. This is only a list of the exceptions that have occurred since the creation of the United Nations. In previous historical periods, the exceptions would be too many to list.



The Taliban state in Afghanistan (the Islamic Emirate of Afghanistan) became the de facto government of nearly all the country in the mid-1990s, but the Afghan Northern Alliance was still recognised by many nations and retained the UN seat.



The People's Republic of China (PRC) was established in 1949 in Mainland China, claiming  succession  from the Republic of China (ROC). However, the succession of the PRC as the state of "China" was not initially recognized by many states because the ROC continued to exist on the island of Taiwan and other islands, such as Penghu, Kinmen, and Matsu. Despite this situation, the ROC in Taiwan maintained their membership as  China  in the United Nations, and a permanent seat on the UN Security Council. However, the PRC was granted the seat of "China" in the United Nations and Security Council in 1971, in place of the ROC which was expelled through the adoption of General Assembly Resolution 2758, and was recognized as the successor state of the ROC. As of 2014, the PRC exercises sovereignty over mainland China, while the ROC exercises sovereignty over Taiwan Area and some minor islands, with both claiming to be the sole legitimate government of both the mainland and Taiwan.



When the Democratic Kampuchea led by Pol Pot was militarily displaced by the Vietnamese-backed People's Republic of Kampuchea, the country's United Nations seat continued to be held by Democratic Kampuchea for many years.



There is some debate over whether the modern Republic of Turkey was the continuing state to the Ottoman Empire or a successor. The two entities fought on opposing sides in the Turkish War of Independence (1919 1922), and even briefly co-existed as separate administrative units (whilst at war with one another): Turkey with its capital in Ankara and the Ottoman Empire from Istanbul. The nationalist faction, led by Mustafa Kemal who defected from the Ottoman army, established the modern republic as a nation state by defeating the opposing elements in the independence war.



International convention since the end of the Cold War has come to distinguish two distinct circumstances where such privileges are sought by such a successor state, in only the first of which may such successor states assume the name or privileged international position of their predecessor. The first set of circumstances arose at the dissolution of the Union of Soviet Socialist Republics (USSR) in 1991. One of this federation's constituent republics, the Russian Federation has declared itself to be "the continuator state of the USSR" on the grounds that it contained 51% of the population of the USSR and 77% of its territory. In consequence, Russia agreed that it would acquire the USSR's seat as a permanent member of the United Nations Security Council. This was also accepted by the rest of the former states of the USSR; in a letter dated 24 December 1991, Boris Yeltsin, at the time President of the Russian Federation, informed the Secretary-General that the membership of the Soviet Union in the Security Council and all other United Nations organs was being continued by the Russian Federation with the support of the 11 member countries of the Commonwealth of Independent States. All Soviet embassies became Russian embassies.
On the other hand, the Baltic states represent a special case. An important tenet of the modern states of Estonia, Latvia and Lithuania is that their incorporation into the Soviet Union from 1940 to 1991 amounted to an illegal occupation. In 1991 when each Baltic state regained their independence they claimed continuity directly from their pre-1940 status. Many other states share this view, and as such these states were not considered to be successor states of the Soviet Union. As a consequence, the Baltic states were able to simply re-establish diplomatic relations with many countries, re-affirm pre-1940 treaties still in force, and resume membership to many international organisations.



After four of the six constituent republics of the Socialist Federal Republic of Yugoslavia seceded in 1991 and 1992, the rump state, renamed the Federal Republic of Yugoslavia, claimed to be the continuation of the state of Yugoslavia, against the objections of the newly independent republics. Representatives from Belgrade continued to hold the original Yugoslavian UN seat; however, the United States refused to recognize it. The remaining territory of the federation was less than half of the population and territory of the former federation. In 1992 the Security Council on September 19 (Resolution 777) and the General Assembly on September 22, decided to refuse to allow the new federation to sit in the General Assembly under the name of "Yugoslavia" on the theory that the Socialist Federal Republic of Yugoslavia had dissolved. The Federal Republic of Yugoslavia (later renamed Serbia and Montenegro) was admitted as a new member to the United Nations in 2000; in 2006, Montenegro declared independence and Serbia continued to hold the federation's seat.



Diadochi, the successors to Alexander the Great.
Predecessors of sovereign states in Europe.
Predecessors of sovereign states in South America.
The Republic of Indonesia, the current successor state of the Dutch East Indies.



Comparative history
International law
Translatio imperii
Universal history
Vienna Convention on Succession of States in respect of Treaties






Burgenthal/Doehring/Kokott: Grundz ge des V lkerrechts, 2. Auflage, Heidelberg 2000 (German)



European Journal of International Law   State Succession in Respect of Human Rights Treaties
Wilfried Fiedler: Der Zeitfaktor im Recht der Staatensukzession, in: Staat und Recht. Festschrift f r G nther Winkler, Wien, 1997, S. 217-236. (German)
Wilfried Fiedler: Staatensukzession und Menschenrechte, in: B. Ziemske u.a. (Hrsg.), Festschrift f r Martin Kriele, M nchen 1997, S. 1371-1391 (German)