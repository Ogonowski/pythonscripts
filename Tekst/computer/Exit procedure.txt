Employee exit management is the process used within many businesses to terminate employees in a professional manner. It applies to employees who have resigned and those that have been terminated by the company.
When an employee is terminated there are a number of considerations that an organization needs to make in order to cleanly end the relationship between the company and the employee. The company as a legal entity has a responsibility to the employee which may extend beyond the period of employment and this is the primary focus of the exit procedure.
As part of computer security, the process will also ensure that access privileges are revoked when a person leaves, and may also cover other issues such as the recovery of equipment, keys and credit cards to ensure that security integrity is maintained.



Exit interview
Induction programme
Termination of employment
Up in the Air (2009 film)
Going postal


