The Detroit Air Defense Sector (DEADS) is an inactive United States Air Force organization. Its last assignment was with the Air Defense Command (ADC) 26th Air Division at Custer Air Force Station (AFS), Michigan. It was inactivated on 1 April 1966.



DEADS was originally designated as the 4627th Air Defense Wing, but was redesignated before being organized in January 1957 at Custer AFS, Michigan. It became operational in September 1958, but did not assume control of former ADC Central Air Defense Force units with a mission to provide air defense of lower Michigan, northeast Indiana, and most of Ohio until 1959. The organization provided command and control over several aircraft, missile and radar squadrons.
On 1 September 1959 the new Semi Automatic Ground Environment (SAGE) Direction Center (DC-06) and Combat Center (CC-01) became operational. 42 19 18 N 085 16 00 W DC-06 was equipped with dual AN/FSQ-7 Computers. The day-to-day operations of the command were to train and maintain tactical units flying jet interceptor aircraft (F-86 Sabre, F-89 Scorpion, F-101 Voodoo, F-102 Delta Dagger, F-104 Starfighter, F-106 Delta Dart) and operating interceptor missiles (CIM-10 Bomarc) and radar squadrons in a state of readiness with training missions and series of exercises with Strategic Air Command and other units simulating interceptions of incoming enemy aircraft.
The Sector was inactivated 1 April 1966 as part of ADC reorganization and consolidation and replaced at Custer AFS by the 34th Air Division. Most of its units were reassigned to 34th or the 29th Air Division.



Designated as 4627th Air Defense Wing, SAGE
Redesignated as Detroit Air Defense Sector on 8 January 1957 and organized
Discontinued and inactivated on 1 April 1966.



30th Air Division, 8 January 1957
26th Air Division, 4 September 1963   1 April 1966



Custer AFS, Michigan, 8 January 1957   1 April 1966






1st Fighter Wing (Air Defense)
Selfridge AFB, Michigan, 1 April 1959 - 1 April 1966



79th Fighter Group (Air Defense)
Youngstown Airport, Ohio, 1 April 1959 - 1 March 1960






35th Air Defense Missile Squadron (BOMARC)
Niagara Falls Air Force Missile Site, New York, 4 September 1963 - 1 April 1966






F-86D, 1959-1960
F-86L, 1959-1960
F-89J, 1959-1960
F-101B, 1960-1966
F-102A, 1959-1960
F-104A, 1959-1960
F-106A, 1960-1966
CIM-10 Bomarc, 1963-1966




List of USAF Aerospace Defense Command General Surveillance Radar Stations
Aerospace Defense Command Fighter Squadrons
List of United States Air Force aircraft control and warning squadrons






 This article incorporates public domain material from websites or documents of the Air Force Historical Research Agency.
Leonard, Barry (2009). History of Strategic Air and Ballistic Missile Defense (PDF). , Vol II, 1955-1972. Fort McNair, DC: Center for Military History. ISBN 978-1-4379-2131-1. 
Redmond, Kent C.; Smith, Thomas M. (2000). From Whirlwind to MITRE: The R&D Story of The SAGE Air Defense Computer. Cambridge, MA: MIT Press. ISBN 978-0-262-18201-0. 
Winkler, David F.; Webster, Julie L (1997). Searching the skies : the legacy of the United States Cold War defense Radar Program (PDF). Champaign, IL: US Army Construction Engineering Research Laboratories. LCCN 97020912. 
Radomes.org Detroit Air Defense Sector