TEMPEST is a National Security Agency specification and NATO certification referring to spying on information systems through leaking emanations, including unintentional radio or electrical signals, sounds, and vibrations. TEMPEST covers both methods to spy upon others and also how to shield equipment against such spying. The protection efforts are also known as emission security (EMSEC), which is a subset of communications security (COMSEC).
The NSA methods for spying upon computer emissions are classified, but some of the protection standards have been released by either the NSA or the Department of Defense. Protecting equipment from spying is done with distance, shielding, filtering and masking. The TEMPEST standards mandate elements such as equipment distance from walls, amount of shielding in buildings and equipment, and distance separating wires carrying classified vs. unclassified materials, filters on cables, and even distance and shielding between wires/equipment and building pipes. Noise can also protect information by masking the actual data.
While much of TEMPEST is about leaking electromagnetic emanations, it also encompasses sounds or mechanical vibrations. For example, it is possible to log a user's keystrokes using the motion sensor inside smartphones. Compromising emissions are defined as unintentional intelligence-bearing signals which, if intercepted and analyzed, may disclose the information transmitted, received, handled, or otherwise processed by any information-processing equipment.



Many specifics of the TEMPEST standards are classified, but some elements are public. Current United States and NATO Tempest standards define three levels of protection requirements:
NATO SDIP-27 Level A (formerly AMSG 720B) and USA NSTISSAM Level I
"Compromising Emanations Laboratory Test Standard"
This is the strictest standard for devices that will be operated in NATO Zone 0 environments, where it is assumed that an attacker has almost immediate access (e.g. neighbouring room, 1 m distance).
NATO SDIP-27 Level B (formerly AMSG 788A) and USA NSTISSAM Level II
"Laboratory Test Standard for Protected Facility Equipment"
This is a slightly relaxed standard for devices that are operated in NATO Zone 1 environments, where it is assumed that an attacker cannot get closer than about 20 m (or where building materials ensure an attenuation equivalent to the free-space attenuation of this distance).
NATO SDIP-27 Level C (formerly AMSG 784) and USA NSTISSAM Level III
"Laboratory Test Standard for Tactical Mobile Equipment/Systems"
An even more relaxed standard for devices operated in NATO Zone 2 environments, where attackers have to deal with about 100 m worth of free-space attenuation (or equivalent attenuation through building materials).
Additional standards include:
NATO SDIP-29 (formerly AMSG 719G)
"Installation of Electrical Equipment for the Processing of Classified Information"
This standard defines installation requirements, for example in respect to grounding and cable distances.
AMSG 799B
"NATO Zoning Procedures"
Defines an attenuation measurement procedure, according to which individual rooms within a security perimeter can be classified into Zone 0, Zone 1, Zone 2, or Zone 3, which then determines what shielding test standard is required for equipment that processes secret data in these rooms.
The NSA and Department of Defense have declassified some TEMPEST elements after Freedom of Information Act requests, but the documents black out many key values and descriptions. The declassified version of the TEMPEST test standard is heavily redacted, with emanation limits and test procedures blacked out. A redacted version of the introductory Tempest handbook NACSIM 5000 was publicly released in December 2000. Additionally, the current NATO standard SDIP-27 (before 2006 known as AMSG 720B, AMSG 788A, and AMSG 784) is still classified.

Despite this, some declassified documents give information on the shielding required by TEMPEST standards. For example, Military Handbook 1195 includes the chart at the right, showing electromagnetic shielding requirements at different frequencies. A declassified NSA specification for shielded enclosures offers similar shielding values, requiring, "a minimum of 100 dB insertion loss from 1 KHz to 10 GHz." Since much of the current requirements are still classified, there are no publicly available correlations between this 100 dB shielding requirement and the newer zone-based shielding standards.
In addition, many separation distance requirements and other elements are provided by the declassified NSA red-black installation guidance, NSTISSAM TEMPEST/2-95.
The United States Air Force has published a few mandatory standards regarding Emission Security:
Instruction AFI 33-203 Vol 1, Emission Security (Soon to be AFSSI 7700)
Instruction AFI 33-203 Vol 3, EMSEC Countermeasures Reviews (Soon to be AFSSI 7702)
Instruction AFI 33-201 Vol 8, Protected Distributed Systems (Soon to be AFSSI 7703)



The information-security agencies of several NATO countries publish lists of accredited testing labs and of equipment that has passed these tests:
In Canada: Canadian Industrial TEMPEST Program
In Germany: BSI German Zoned Products List
In the UK: UK CESG Directory of Infosec Assured Products, Section 12
In the U.S.: NSA Tempest Endorsement Program
The United States Army also has a Tempest testing facility, as part of the U.S. Army Information Systems Engineering Command, at Fort Huachuca, Arizona. Similar lists and facilities exist in other NATO countries.
Tempest certification must apply to entire systems, not just to individual components, since connecting a single unshielded component (such as a cable) to an otherwise secure system could dramatically alter the system RF characteristics.



TEMPEST standards require "RED/BLACK separation", i.e., maintaining distance or installing shielding between circuits and equipment used to handle plaintext classified or sensitive information that is not encrypted (RED) and secured circuits and equipment (BLACK), the latter including those carrying encrypted signals. Manufacture of TEMPEST-approved equipment must be done under careful quality control to ensure that additional units are built exactly the same as the units that were tested. Changing even a single wire can invalidate the tests.



One aspect of Tempest testing that distinguishes it from limits on spurious emissions (e.g., FCC Part 15) is a requirement of absolute minimal correlation between radiated energy or detectable emissions and any plaintext data that are being processed.



In 1985, Wim van Eck published the first unclassified technical analysis of the security risks of emanations from computer monitors. This paper caused some consternation in the security community, which had previously believed that such monitoring was a highly sophisticated attack available only to governments; van Eck successfully eavesdropped on a real system, at a range of hundreds of metres, using just $15 worth of equipment plus a television set.
In consequence of this research such emanations are sometimes called "van Eck radiation", and the eavesdropping technique van Eck phreaking, although government researchers were already aware of the danger, as Bell Labs noted this vulnerability to secure teleprinter communications during World War II and was able to produce 75% of the plaintext being processed in a secure facility from a distance of 80 feet. Additionally the NSA published Tempest Fundamentals, NSA-82-89, NACSIM 5000, National Security Agency (Classified) on February 1, 1982. In addition, the van Eck technique was successfully demonstrated to non-TEMPEST personnel in Korea during the Korean War in the 1950s.
Markus Kuhn has discovered several low-cost techniques for reducing the chances that emanations from computer displays can be monitored remotely. With CRT displays and analog video cables, filtering out high-frequency components from fonts before rendering them on a computer screen will attenuate the energy at which text characters are broadcast. With modern flat panel displays, the high-speed digital serial interface (DVI) cables from the graphics controller are a main source of compromising emanations. Adding random noise to the less significant bits of pixel values may render the emanations from flat-panel displays unintelligible to eavesdroppers but is not a secure method. Since DVI uses a certain bit code scheme for trying to transport an evenly balanced signal of 0 and 1 bits there may not be much difference between two pixel colours that differ very much in their colour or intensity. It may also be that the generated emanations may differ totally even if only the last bit of a pixel's colour is changed. The signal received by the eavesdropper does also depend on the frequency where the emanations are detected. The signal can be received on many frequencies at once and each frequency's signal differs in contrast and brightness related to a certain colour on the screen. Usually, the technique of smothering the RED signal with noise is not effective unless the power of the noise is sufficient to drive the eavesdropper's receiver into saturation and thus overwhelming the receiver input.
LED indicators on computer equipment can be a source of compromising optical emanations. One such technique involves the monitoring of the lights on a dial-up modem. Almost all modems flash an LED to show activity, and it is common for the flashes to be directly taken from the data line. As such, a fast optical system can easily see the changes in the flickers from the data being transmitted down the wire.
Further, recent research has shown it is possible to detect the radiation corresponding to a keypress event from not only wireless (radio) keyboards, but also from traditional wired keyboards, and even from laptop keyboards.
In 2014, researchers introduced  AirHopper , a bifurcated attack pattern showing the feasibility of data exfiltration from an isolated computer to a nearby mobile phone, using FM frequency signals.
In 2015, "BitWhisper", a Covert Signaling Channel between Air-Gapped Computers using Thermal Manipulations was introduced. "BitWhisper" supports bidirectional communication and requires no additional dedicated peripheral hardware.
Later in 2015, researchers introduced GSMem, a method for exfiltrating data from air-gapped computers over cellular frequencies. The transmission - generated by a standard internal bus - renders the computer into a small cellular transmitter antenna.



In the television series Numb3rs, season 1 episode "Sacrifice", a wire connected to a high gain antenna was used to "read" from a computer monitor.
In the television series Spooks, season 4 episode "The Sting", a failed attempt to read information from a computer that has no network link is described.
In the novel Cryptonomicon by Neal Stephenson, characters use Van Eck phreaking to likewise read information from a computer monitor in a neighboring room.
In the television series Agents of S.H.I.E.L.D., season 1 episode "Ragtag", an office is scanned for digital signatures in the UHF spectrum.
In the video game Tom Clancy's Splinter Cell: Chaos Theory, part of the final mission involves spying on a meeting in a Tempest hardened war room. Throughout the entire Splinter Cell series, a laser microphone is used as well.


