Exabyte Corp. was a manufacturer of magnetic tape data storage products headquartered in Boulder, Colorado, United States. Exabyte Corp. is now defunct, but the company's technology is sold by Tandberg Data under both brand names. Prior to the 2006 demise, Exabyte offered tape storage and automation solutions for servers, workstations, LANs and SANs. Exabyte is best known for introducing the Data8 (8mm) magnetic tape format in 1987. At the time of its demise, Exabyte manufactured VXA and LTO based products. The company controlled VXA technology but did not play a large role in the LTO community.



The company was formed in 1985 by Juan Rodriguez, Harry Hinz, and Kelly Beavers, and a group of ex-StorageTek engineers who were interested in using consumer videotape technology for data storage. The company advanced technology for computer backups in 1987 when they introduced the Data8 magnetic tape format. The company's follow-up technologies, including Mammoth and Mammoth-2, were less successful.
Exabyte went public on the NASDAQ in 1989 under the symbol EXBT.



Exabyte's history of acquisitions includes:
1992 - R-Byte, Inc., a maker of 4mm tape systems.
1993 - Tallgrass Technologies of Lenexa, KS. Tallgrass manufactured 4mm DDS drives, backup software, and had a significant distribution channel. 
1993 - Everex's Mass Storage Division (MSD). Everex did its research and development in Ann Arbor, MI and manufactured its products in Fremont, CA. Everex MSD made QIC products.
October 1994 - Grundig Data Scanner GmbH, for $2.9 million and renamed Exabyte Magnetics GmbH. This subsidiary designed and manufactured helical scan tape heads. 



Ecrix was a magnetic tape data storage company founded in 1996 in Boulder, Colorado. The founders, Kelly Beavers and Juan Rodriguez, were two of the three founders of Exabyte. The research and development done by Ecrix focused on making a cheaper 8mm tape drive. In 1999, Ecrix released their first product, the VXA tape drive.  In 2001, Ecrix and Exabyte merged, giving Exabyte access to Ecrix's VXA Packet Technology tape drive format.



On 30 June 2006, Exabyte announced that they are looking for a buyer. 
On 30 August 2006, Tandberg Data announced that they were buying Exabyte's assets for 28 million USD.  
The acquisition was completed on 20 November 2006.



Tandberg Tape Backup Drives
Company history, 1985-2000


