This is a list of Neolithic cultures of China that have been discovered by archaeologists. They are sorted in chronological order from the earliest founding to the latest and are followed by a schematic visualization of these cultures.
It would seem that the definition of Neolithic in China is undergoing changes. The discovery in 2012 of pottery circa 20,000 years BP indicates that this measure alone can no longer be used to define the period.  It will fall to the more difficult task of determining when cereal domestication started.






These cultures are brought together schematically for the period 8500 to 1500 BC. Neolithic cultures remain unmarked and Bronze Age cultures (from 2000 BC) are marked with *. There are many differences in opinion by dating these cultures, so the dates chosen here are tentative:
For this schematic outline of its neolithic cultures China has been divided into the following nine parts:
Northeast China: Inner Mongolia, Heilongjiang, Jilin and Liaoning.
Northwest China (Upper Yellow River): Gansu, Qinghai and western part of Shaanxi.
North-central China (Middle Yellow River): Shanxi, Hebei, western part of Henan and eastern part of Shaanxi. This is called the North China Plain, until recently seen as where Chinese civilization originated from and spread out along the country.
Eastern China (lower Yellow River): Shandong, Anhui, northern part of Jiangsu and eastern part Henan.
East-south-eastern China (lower Yangtze): Zhejiang and biggest part of Jiangsu.
South-central China (middle Yangtze): Hubei and northern part of Hunan.
Sichuan and upper Yangtze.
Southeast China: Fujian, Jiangxi, Guangdong, Guangxi, southern part of Hunan, lower Red River in the northern part of Vietnam and the island of Taiwan.
Southwest China: Yunnan and Guizhou.



History of China
List of Bronze Age sites in China
List of Palaeolithic sites in China
Prehistoric Asia
Prehistoric Beifudi site
Neolithic signs in China






Chang Kwang-chih, The Archaeology of Ancient China, Yale University Press: New Haven, 1986 (Fourth Edition Revised and Enlarged), ISBN 0-300-03784-8.
Loewe, Michael en Edward L. Shaughnessy (ed.), The Cambridge History of Ancient China. From the Origins of Civilization to 221 B.C., Cambridge University Press: Cambridge 1999, ISBN 0-521-47030-7.
He, Zhonghu, and Alain P.A. Bonjean, Cereals in China, The International Maize and Wheat Improvement Center, 2010, ISBN 978-970-648-177-1
Higham, Charles, The Bronze Age of Southeast Asia, Cambridge University Press: Cambridge 1996, ISBN 0-521-49660-8.
Li Liu,The Chinese Neolithic. Trajectories to Early States, Cambridge University Press: Cambridge 2004, ISBN 0-521-81184-8.
Maisels, Charles Keith, Early Civilizations of the Old World. The Formative Histories of Egypt, The Levant, Mesopotamia, India and China, Routledge: Londen 1999, ISBN 0-415-10976-0.
Scarre, Chris (ed.), The Human Past. World Prehistory & the Development of Human Societies, Thames & Hudson: Londen 2005, ISBN 0-500-28531-4.

chapter 7, Higham, Charles, 'East Asian Agriculture and Its Impact', p.234-264.
chapter 15,Higham, Charles, 'Complex Societies of East and Southeast Asia', p.552-594