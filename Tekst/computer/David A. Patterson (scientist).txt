David Andrew Patterson (born November 16, 1947) is an American computer pioneer and academic who has held the position of Professor of Computer Science at the University of California, Berkeley since 1976.
Patterson is noted for his pioneering contributions to RISC processor design, having coined the term RISC, and by leading the Berkeley RISC project. He is also noted for his research on RAID disks.
His books on computer architecture (co-authored with John L. Hennessy) are widely used in computer science education. Patterson is a Fellow of the American Association for the Advancement of Science.



A native of Evergreen Park, Illinois, David Patterson attended UCLA, receiving his B.A. in 1969, M.S. in 1970 and Ph.D. (advised by David F. Martin and Gerald Estrin) in 1976.



He is an important proponent of the concept of Reduced Instruction Set Computer and coined the term "RISC". He led the Berkeley RISC project from 1980 and onwards along with Carlo H. Sequin, where the technique of register windows was introduced. He is also one of the innovators of the Redundant Arrays of Independent Disks (RAID) (in collaboration with Randy Katz and Garth Gibson), and Network of Workstations (NOW) (in collaboration with Eric Brewer and David Culler).



Past chair of the Computer Science Department at U.C. Berkeley and the Computing Research Association, he served on the Information Technology Advisory Committee for the U.S. President (PITAC) during 2003 05 and was elected president of the Association for Computing Machinery (ACM) for 2004 06.






He co-authored six books, including two with John L. Hennessy on computer architecture: Computer Architecture: A Quantitative Approach (5 editions latest is ISBN 0-12-383872-X) and Computer Organization and Design: the Hardware/Software Interface (5 editions latest is ISBN 978-0-12-407726-3). They have been widely used as textbooks for graduate and undergraduate courses since 1990. His most recent book is with Armando Fox on software engineering: Engineering Software as a Service: An Agile Approach Using Cloud Computing (1st Edition) (ISBN 0-98-488124-7).



Michael Stonebraker; Randy Katz,David Patterson, John Ousterhout (1988). "THE DESIGN OF XPRS" (PDF). VLDB: 318 330. Retrieved 25 March 2015. 



His work has been recognized by about 35 awards for research, teaching, and service, including Fellow of the Association for Computing Machinery (ACM) and the Institute of Electrical and Electronics Engineers (IEEE) as well as by election to the National Academy of Engineering, National Academy of Sciences, and the Silicon Valley Engineering Hall of Fame. In 2005 he and Hennessy shared Japan's Computer & Communication award and, in 2006, was elected to the American Academy of Arts and Sciences and the National Academy of Sciences and received the Distinguished Service Award from the Computing Research Association.  In 2007 he was named a Fellow of the Computer History Museum "for fundamental contributions to engineering education, advances in computer architecture, and the integration of leading-edge research with education." That same year he was also named a Fellow of the American Association for the Advancement of Science. In 2008, won the ACM Distinguished Service Award, the ACM-IEEE Eckert-Mauchly Award, and was recognized by the School of Engineering at UCLA for Alumni Achievement in Academia. Since then he has won the ACM-SIGARCH Distinguished Service Award, ACM-SIGOPS Hall of Fame Award, and the 2012 Jean-Claude Laprie Award in Dependable Computing from IFIP Working Group 10.4 on Dependable Computing and Fault Tolerance.
In 2013 he set the American Powerlifting Record for the state of California for his weight class and age group in bench press, dead lift, squat, and all three combined lifts.
On February 12, 2015, IEEE installed a plaque at UC Berkeley to commemorate the contribution of RISC-I in Soda Hall at UC Berkeley. The plaque reads:
IEEE Milestone in Electrical and Computer Engineering
First RISC (Reduced Instruction Set Computing) Microprocessor
UC Berkeley students designed and built the first VLSI reduced instruction-set computer in 1981. The simplified instructions of RISC-I reduced the hardware for instruction decode and control, which enabled a flat 32-bit address space, a large set of registers, and pipelined execution. A good match to C programs and the Unix operating system, RISC-I influenced instruction sets widely used today, including those for game consoles, smartphones and tablets.
February 2015



From 2003 to 2012 he rode in the annual Waves to Wine MS charity event as part of Bike MS; a 2-day cycling adventure. He was the top fundraiser in 2006, 2007, 2008, 2009, 2010, 2011, and 2012. In total, he has raised more than $200,000 for Multiple Sclerosis.



David Patterson's recent projects have been the RAD Lab: Reliable Adaptive Distributed systems, the Par Lab: Parallel Computing Laboratory, the AMP Lab: Algorithms, Machines, and People Laboratory, and the ASPIRE Lab: Algorithms and Specializers for Provably optimal Implementations with Resilience and Efficiency Laboratory.



He has advised a number of notable Ph.D. candidates, including:
Michael Armbrust, software engineer at DataBricks
Remzi Arpaci-Dusseau, professor at the University of Wisconsin Madison
Sarah Bird, researcher at Microsoft Research
Peter Bodik, researcher at Microsoft Research
Peter Chen, professor at the University of Michigan
Mike Dahlin, Google engineer and Adjunct Professor of Computer Science at the University of Texas
David Ditzel, founder and former president of Transmeta
Archana Ganapathi, research engineer at Splunk
Garth A. Gibson, co-inventor of RAID, founder and CTO of Panasas, and professor at Carnegie Mellon University
Mark Hill, Amdahl & Morgridge Professor and currently Chair of Computer Science at the University of Wisconsin Madison
Manolis Katevenis, pioneer in RISC VLSI implementation and high-speed network switches
Kim Keeton, researcher at Hewlett Packard Labs
Christos Kozyrakis, associate professor at Stanford University
Corinna Lee, architect at ATI Technologies
Nisha Talagala, Fellow at SanDisk
David Ungar, designer of the Self programming language
Robert Yung, CTO of PMC-Sierra
Wei Xu, Assistant Professor at Tsinghua University






Who's Who in America 2008. New Providence, New Jersey: Marquis Who's Who. ISBN 978-0-8379-7010-3



David A. Patterson page at the website of the University of California, Berkeley
List of David A. Patterson's students who graduated with PhD