In software engineering, the adapter pattern is a software design pattern that allows the interface of an existing class to be used from another interface. It is often used to make existing classes work with others without modifying their source code.



An adapter helps two incompatible interfaces to work together. This is the real world definition for an adapter. Interfaces may be incompatible but the inner functionality should suit the need. The Adapter design pattern allows otherwise incompatible classes to work together by converting the interface of one class into an interface expected by the clients.



There are two types of adapter patterns:



In this type of adapter pattern, the adapter contains an instance of the class it wraps. In this situation, the adapter makes calls to the instance of the wrapped object.



This type of adapter uses multiple polymorphic interfaces implementing or inheriting both the interface that is expected and the interface that is pre-existing. It is typical for the expected interface to be created as a pure interface class, especially in languages such as Java (before jdk 1.8) that do not support multiple inheritance of classes.

The adapter pattern is useful in situations where an already existing class provides some or all of the services you need but does not use the interface you need. A good real life example is an adapter that converts the interface of a Document Object Model of an XML document into a tree structure that can be displayed.



There is a further form of runtime adapter pattern as follows:
It is desired for classA to supply classB with some data, let us suppose some String data. A compile time solution is:

However, suppose that the format of the string data must be varied. A compile time solution is to use inheritance:

and perhaps create the correctly "formatting" object at runtime by means of the Factory pattern.
A solution using "adapters" proceeds as follows:
(i) define an intermediary "Provider" interface, and write an implementation of that Provider interface that wraps the source of the data, ClassA in this example, and outputs the data formatted as appropriate:

(ii) Write an Adapter class that returns the specific implementation of the Provider:

(iii) Register the Adapter with a global registry, so that the Adapter can be looked up at runtime:

(iv) In your code, when you wish to transfer data from ClassA to ClassB, write:

or more concisely:

(v) The advantage can be seen in that, if it is desired to transfer the data in a second format, then look up the different adapter/provider:

(vi) And if it is desired to output the data from ClassA as, say, image data in Class C:

(vii) In this way, the use of adapters and providers allows multiple "views" by ClassB and ClassC into ClassA without having to alter the class hierarchy. In general, it permits a mechanism for arbitrary data flows between objects that can be retrofitted to an existing object hierarchy.



When implementing the adapter pattern, for clarity one can apply the class name [ClassName]To[Interface]Adapter to the provider implementation, for example DAOToProviderAdapter. It should have a constructor method with an adaptee class variable as a parameter. This parameter will be passed to an instance member of [ClassName]To[Interface]Adapter. When the clientMethod is called it will have access to the adaptee instance which allows for accessing the required data of the adaptee and performing operations on that data that generates the desired output.

And a Scala implementation



The term glue code is sometimes used to describe implementations of the adapter pattern. It does not serve any use in calculation or computation. Rather it serves as a proxy between otherwise incompatible parts of software, to make them compatible. The standard practice is to keep logic out of the glue code and leave that to the code blocks it connects to.



Delegation, strongly relevant to the object adapter pattern.
Dependency inversion principle, which can be thought of as applying the Adapter pattern, when the high-level class defines their own (adapter) interface to the low-level module (implemented by an Adaptee class).
Shim
Wrapper function
Wrapper library


