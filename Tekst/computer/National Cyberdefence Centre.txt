The National Cyberdefence Centre is a German government agency established to respond to attacks on government computers in Germany.
The interior ministry's spokesman announced in December 2010 that the agency would be established; the German government reports that in the first 9 months of 2010, there were 1600 attacks on computer systems used by local and federal government in Germany. A large proportion of attacks come from China.



The new National Cyberdefence Centre pools the cyberdefense resources of the Federal Office for Information Security, the Federal Office for the Protection of the Constitution, the Federal Intelligence Service, the Federal Police, the Customs Criminal Investigation Office, the German Military, the Federal Office of Civil Protection and Disaster Assistance, and the Federal Criminal Police Office; it will cooperate with ISPs.



CCDCOE
Cyber Command
ENISA
Federal Office for Information Security






National Cyber Security Framework Manual