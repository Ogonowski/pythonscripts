The Battle of Rehe (simplified Chinese:  ; traditional Chinese:  ; pinyin: R h  zh ny , sometimes called the Battle of Jehol) was the second part of Operation Nekka, a campaign by which the Empire of Japan successfully captured the Inner Mongolian province of Rehe from the Chinese warlord Zhang Xueliang and annexed it to the new state of Manchukuo. The battle was fought from February 21 to March 1, 1933.



Following the establishment of Manchukuo, the Kwantung Army launched an operation to secure its southern frontier with China by attacking and capturing Shanhaiguan Pass at the Great Wall on 3 January 1933. The province of Rehe, on the northern side of the Great Wall was the next target. Declaring the province to be historically a portion of Manchuria, the Japanese Army initially hoped to secure it through the defection of General Tang Yulin to the Manchukuo cause. When this failed, the military option was placed into action. Assigned to this operation were the Japanese 6th Division and 8th Division and 14th and 33rd Mixed Brigades of infantry, 4th Cavalry Brigade with Type 92 Heavy Armored Cars and the 1st Special Tank Company.
The Japanese army's Chief of Staff requested Emperor Hirohito's sanction for the 'strategic operation' against Chinese forces in Rehe. Hoping that it was the last of the army's operations in the area and that it would bring an end to the Manchurian matter, the Emperor approved, while stating explicitly that the army was not to go beyond China's Great Wall.



On February 23, 1933, the offensive was launched. On February 25, Chaoyang and Kailu were taken. On March 2, the Japanese 4th Cavalry Brigade encountered resistance from the forces of Sun Dianying, and after days of fighting, took over Chifeng. Sun Dianying mounted a counterattack against the Japanese 6th Division on the same day, and at one time penetrated to near the Japanese headquarters. On March 4, Japanese cavalry and the 1st Special Tank Company with Type 89 Tanks. took Chengde the capital of Rehe.



Rehe was subsequently annexed to Manchukuo. Zhang Xueliang was forced by the Kuomintang government to relinquish his posts for  medical reasons . Chinese forces fell back in disarray to the Great Wall, where after a series of battles and skirmishes, the Japanese Army seized a number of strategic points, and then agreed to a cease fire and a negotiated settlement (the Tangku Truce) whereby a demilitarized zone would be established between the Great Wall and Beijing. However, this would prove to be only a temporary respite before the full scale combat of the Second Sino-Japanese War erupted in earnest in 1937.



Order of battle Operation Jehol






Fenby, Jonathan (2003). Chiang Kai-shek: China's Generalissimo and the Nation He Lost. Carroll & Graf Publishers. isbn = 0-7867-1318-6. 
Hsu Long-hsuen and Chang Ming-kai, History of The Sino-Japanese War (1937 1945) 2nd Ed., 1971. Translated by Wen Ha-hsiung, Chung Wu Publishing; 33, 140th Lane, Tung-hwa Street, Taipei, Taiwan Republic of China. Pg. 159-161.
  (China's Anti-Japanese War Combat Operations), Guo Rugui, editor-in-chief Huang Yuzhang, Jiangsu People's Publishing House, Date published : 2005-7-1, ISBN 7-214-03034-9
Jowett, Philipp (2005). Rays of the Rising Sun, Volume 1: Japan's Asian Allies 1931 45, China and Manchukuo. Helion and Company Ltd. ISBN 1-874622-21-3. 



Battles of the Great Wall
THE HISTORY OF BATTLES OF IMPERIAL JAPANESE TANKS
Jan. 23, 1933 issue of TIME magazine, "On Bended Knee"
Feb. 27, 1933 issue of TIME magazine, Bumps & Blood
Mar. 6, 1933 issue of TIME magazine War of Jehol
Mar. 6, 1933 issue of TIME magazine, Two-Gun Tang
Mar. 13, 1933 issue of TIME magazine Glorious 16th
Dec. 11, 1933 issue of TIME magazine, Generalissimo's Last Straw