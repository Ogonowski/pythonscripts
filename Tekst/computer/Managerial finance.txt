Managerial finance is the branch of finance that concerns itself with the managerial significance of finance techniques. It is focused on assessment rather than technique.
The difference between a managerial and a technical approach can be seen in the questions one might ask of annual reports. The concern of a technical approach is primarily measurement. It asks: is money being assigned to the right categories? Were generally accepted accounting principles (GAAP) followed?
The purpose of a managerial approach, however, is to understand what the figures mean.
Someone using such an approach might compare the returns to other businesses in their industry and ask: are we performing better or worse than our peers? If we are performing worse, what is the source of the problem? Do we have the same profit margins? If not, why? Do we have the same expenses? Are we paying more for something than our peers?
They may look at changes in asset balances or red flags that indicate problems with bill collection or bad debt.
They will analyze working capital to anticipate future cash flow problems.
Managerial finance is an interdisciplinary approach that borrows from both managerial accounting and corporate finance.
Sound financial management creates value and organizational agility through the allocation of scarce resources amongst competing business opportunities. It is an aid to the implementation and monitoring of business strategies and helps achieve business objectives.



To interpret financial results in the manner described above, managers use Financial analysis techniques.
Managers also need to look at how resources are allocated within an organization. They need to know what each activity costs and why. These questions require managerial accounting techniques such as activity based costing.
Managers also need to anticipate future expenses. To get a better understanding of the accuracy of the budgeting process, they may use variable budgeting.



Managerial finance is also interested in determining the best way to use money to improve future opportunities to earn money and minimize the impact of financial shocks. To accomplish these goals managerial finance uses the following techniques borrowed from Corporate finance:
Valuation
Dividend policy
Working capital management
Capital structure



List of finance topics
Category:Finance




Financial Management Notes
Mastering Financial Management, Clive Marsh, Financial Times Prentice Hall, ISBN 978-0-273-72454-4
MIT Open Courseware - 15.414 Financial Management, Summer 2003.
Gitman, Lawrence (2003), Principles of Managerial Finance, 10th edition, Addison-Wesley Publishing, 2003, ISBN 0-201-78479-3. [1]
Weston, Fred and Brigham, Eugene (1972), Managerial Finance, Dryden Press, Hinsdale Illinois, 1972
Chen, Henry editor, (1967), Frontiers of Managerial Finance, Gulf Publishing, Houston Texas, 1967
Brigham, Eugene and Johnson, Ramon (1980), Issues in Managerial Finance, Holt Rinehart and Winston Publishers, Hindale Illinois, 1908