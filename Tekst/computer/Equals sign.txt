The equals sign or equality sign (=) is a mathematical symbol used to indicate equality. It was invented in 1557 by Robert Recorde. In an equation, the equals sign is placed between two expressions that have the same value. It is assigned to the Unicode and ASCII character 003D in hexadecimal, 0061 in decimal.



The etymology of the word "equal" is from the Latin word "aequalis" as meaning "uniform", "identical", or "equal", from aequus ("level", "even", or "just").

The "=" symbol that is now universally accepted in mathematics for equality was first recorded by Welsh mathematician Robert Recorde in The Whetstone of Witte (1557). The original form of the symbol was much wider than the present form. In his book Recorde explains his design of the "Gemowe lines" (meaning twin lines, from the Latin gemellus):

  to auoide the tediou e repetition of the e woordes : is equalle to : I will  ette as I doe often in woorke v e, a paire of paralleles, or Gemowe lines of one lengthe, thus: =, bicau e noe .2. thynges, can be moare equalle.

  to avoid the tedious repetition of these words: "is equal to", I will set (as I do often in work use) a pair of parallels, or Gemowe lines, of one length (thus =), because no two things can be more equal.

According to Scotland's University of St Andrews History of Mathematics website:

The symbol '=' was not immediately popular. The symbol || was used by some and   (or  ), from the Latin word aequalis meaning equal, was widely used into the 1700s.



In mathematics, the equals sign can be used as a simple statement of fact in a specific case (x = 2), or to create definitions (let x = 2), conditional statements (if x = 2, then  ), or to express a universal equivalence (x + 1)2 = x2 + 2x + 1.
The first important computer programming language to use the equals sign was the original version of Fortran, FORTRAN I, designed in 1954 and implemented in 1957. In Fortran, "=" serves as an assignment operator: X = 2 sets the value of X to 2. This somewhat resembles the use of "=" in a mathematical definition, but with different semantics: the expression following "=" is evaluated first and may refer to a previous value of X. For example, the assignment X = X + 2 increases the value of X by 2.
A rival programming-language usage was pioneered by the original version of ALGOL, which was designed in 1958 and implemented in 1960. ALGOL included a relational operator that tested for equality, allowing constructions like if x = 2 with essentially the same meaning of "=" as the conditional usage in mathematics. The equals sign was reserved for this usage.
Both usages have remained common in different programming languages into the early 21st century. As well as Fortran, "=" is used for assignment in such languages as C, Perl, Python, awk, and their descendants. But "=" is used for equality and not assignment in the Pascal family, Ada, Eiffel, APL, and other languages.
A few languages, such as BASIC and PL/I, have used the equals sign to mean both assignment and equality, distinguished by context. However, in most languages where "=" has one of these meanings, a different character or, more often, a sequence of characters is used for the other meaning. Following ALGOL, most languages that use "=" for equality use ":=" for assignment, although APL, with its special character set, uses a left-pointing arrow.
Fortran did not have an equality operator (it was only possible to compare an expression to zero, using the arithmetic IF statement) until FORTRAN IV was released in 1962, since when it has used the four characters ".EQ." to test for equality. The language B introduced the use of "==" with this meaning, which has been copied by its descendant C and most later languages where "=" means assignment.



In PHP, the triple equals sign ( false is not, because the number 0 is an integer value whereas false is a Boolean value.
JavaScript has the same semantics for .
In Ruby, equality under .



The equals sign is also used in defining attribute value pairs, in which an attribute is assigned a value.



The equals sign is also used as a grammatical tone letter in the orthographies of Budu in the Congo-Kinshasa, in Krumen, Mwan and Dan in the Ivory Coast. The Unicode character used for the tone letter (U+A78A) is different from the mathematical symbol (U+003D).







Symbols used to denote items that are approximately equal include the following:
  (U+2248, LaTeX \approx)
  (U+2243, LaTeX \simeq), a combination of   and =, also used to indicate asymptotic equality
  (U+2245, LaTeX \cong), another combination of   and =, which is also sometimes used to indicate isomorphism or congruence
  (U+223C), which is also sometimes used to indicate proportionality, being related by an equivalence relation, or to indicate that a random variable is distributed according to a specific probability distribution
  (U+223D), which is also used to indicate proportionality
  (U+2250, LaTeX \doteq), which can also be used to represent the approach of a variable to a limit
  (U+2252), commonly used in Japanese, Taiwanese and Korean
  (U+2253)



The symbol used to denote inequation (when items are not equal) is a slashed equals sign " " (U+2260; 2260,Alt+X in Microsoft Windows). In LaTeX, this is done with the "\neq" command.
Most programming languages, limiting themselves to the ASCII character set and typeable characters, use ~=, !=, /=, =/=, or <> to represent their Boolean inequality operator.



The triple bar symbol " " (U+2261, Latex \equiv) is often used to indicate an identity, a definition (which can also be represented by U+225D " " or U+2254 " "), or a congruence relation in modular arithmetic. The symbol " " can be used to express that an item corresponds to another.



The symbol " " is often used to indicate isomorphic algebraic structures or congruent geometric figures.



Equality of truth values, i.e. bi-implication or logical equivalence, may be denoted by various symbols including =, ~, and  .



A possibly unique case of the equals sign of European usage in a person's name, specifically in a double-barreled name, was by pioneer aviator Alberto Santos=Dumont, as he is also known not only to have often used an equals sign (=) between his two surnames in place of a hyphen, but also seems to have personally preferred that practice, to display equal respect for his father's French ethnicity and the Brazilian ethnicity of his mother.
The equals sign is sometimes used in Japanese as a separator between names.



Additional symbols in Unicode related to the equals sign include:
  (U+224C   all equal to)
  (U+2254   colon equals)
  (U+2255   equals colon)
  (U+2256   ring in equal to)
  (U+2257   ring equal to)
  (U+2259   estimates)
  (U+225A   equiangular to)
  (U+225B   star equals)
  (U+225C   delta equal to)
  (U+225E   measured by)
  (U+225F   questioned equal to).



The equals sign is sometimes used incorrectly within a mathematical argument to connect math steps in a non-standard way, rather than to show equality (especially by early mathematics students).
For example, if one were finding the sum, step by step, of the numbers 1, 2, 3, 4, and 5, one might write
1 + 2 = 3 + 3 = 6 + 4 = 10 + 5 = 15.
Structurally, this is shorthand for
([(1 + 2 = 3) + 3 = 6] + 4 = 10) + 5 = 15,
but the notation is incorrect, because each part of the equality has a different value. If interpreted strictly as it says, it implies
3 = 6 = 10 = 15 = 15.
A correct version of the argument would be
1 + 2 = 3, 3 + 3 = 6, 6 + 4 = 10, 10 + 5 = 15.



2 + 2 = 5
Double hyphen
Equality (mathematics)
Logical equality
Plus and minus signs






Cajori, Florian (1993). A History of Mathematical Notations. New York: Dover (reprint). ISBN 0-486-67766-4. 
Boyer, C. B.: A History of Mathematics, 2nd ed. rev. by Uta C. Merzbach. New York: Wiley, 1989 ISBN 0-471-09763-2 (1991 pbk ed. ISBN 0-471-54397-7)



Earliest Uses of Symbols of Relation
Image of the page of The Whetstone of Witte on which the equals sign is introduced
Scientific Symbols, Icons, Mathematical Symbols
Robert Recorde invents the equals sign