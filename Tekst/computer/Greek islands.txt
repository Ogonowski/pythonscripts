Greece has an extremely large number of islands, with estimates ranging from somewhere around 1,200 to 6,000, depending on the minimum size to take into account. The number of inhabited islands is variously cited as between 166 and 227.
The largest Greek island by area is Crete, located at the southern edge of the Aegean Sea. The second largest island is Euboea, which is separated from the mainland by the 60m-wide Euripus Strait, and is administered as part of the Central Greece region. After the third and fourth largest Greek Islands, Lesbos and Rhodes, the rest of the islands are two-thirds of the area of Rhodes, or smaller.
The Greek islands are traditionally grouped into the following clusters: The Argo-Saronic Islands in the Saronic gulf near Athens, the Cyclades, a large but dense collection occupying the central part of the Aegean Sea, the North Aegean islands, a loose grouping off the west coast of Turkey, the Dodecanese, another loose collection in the southeast between Crete and Turkey, the Sporades, a small tight group off the coast of Euboea, and the Ionian Islands, located to the west of the mainland in the Ionian Sea (one of these islands, Kythira, is off the southern tip of the Peloponnese Peninsula and part of the Attica region, but still considered part of the Ionian Islands, mainly because of historical reasons). There are also many islands, islets and rocks that surround the coast of Crete.



The following are the largest Greek islands listed by surface area. The table includes all islands of over 75 square miles (190 km2).









This list includes islands that may have been inhabited in the past but are now uninhabited:






















164 total islands of which 26 are inhabited.






Agios Efstratios
Chios
Antipsara
Oinousses
Psara

Fournoi Korseon
Agios Minas
Thymaina

Ikaria
Lesbos
Lemnos
Samos
Samiopoula



Samothraki
Thasos




Cyclades comprise around 220 islands









This is a list of islands, islets, and rocks that surround the island of Crete.















List of Aegean Islands
List of islands
List of islands in the Mediterranean



^ The Greek claim is disputed by Turkey (Ministry of Foreign Affairs of Republic of Turkey : "The Kardak Dispute" retrieved 30 November 2012).


