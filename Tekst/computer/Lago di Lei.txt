Lago di Lei is a reservoir in the Valle di Lei. The reservoir is almost entirely located in Italy, but the barrage was built on territory later assigned to Switzerland (municipality of Innerferrera, Grisons), while an equivalent territory north of the lake was assigned to Italy. The dam is operated by Kraftwerke Hinterrhein. The waters of the lake are the only waters in Italian territory which end up in the North Sea, being part of the Rhine's drainage basin. Other waters of Italy that don't flow to the Mediterranean Sea are found in the valley of Livigno, valley of Sexten, Puster Valley east of Innichen, and most of the waters of the municipality of Tarvisio east of Sella Nevea: all these waters flow to the Black Sea.






 Media related to Lago di Lei at Wikimedia Commons
Valle di Lei in German, French and Italian in the online Historical Dictionary of Switzerland.
KHR: Valle di Lei: Der Rhein entspringt auch in Italien (German)
KHR: Valle di Lei: il Reno   anche un po' italiano (Italian)