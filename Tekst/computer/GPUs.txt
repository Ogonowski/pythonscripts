The Green Party of the United States (GPUS) is a green, left-wing political party in the United States.
The party, which is the country's fourth-largest by membership, promotes environmentalism, nonviolence, social justice, participatory grassroots democracy, feminism, LGBT rights, and anti-racism.
The GPUS was founded in 2001 as the evolution of the Association of State Green Parties (ASGP), which was formed in 1996. After its founding, the GPUS soon became the primary national green organization in the country, eclipsing the Greens/Green Party USA (G/GPUSA), formed in 1991 out of the Green Committees of Correspondence (CoC), a collection of local green groups active since 1984. The ASGP had increasingly distanced itself frm the G/GPUSA in the late 1990s.
The Greens gained widespread public attention during the 2000 presidential election, when the ticket composed of Ralph Nader and Winona LaDuke, won 2.7% of the popular vote. Nader was vilified by some Democrats, who accused him of spoiling the election for Al Gore. Nader's impact on the 2000 election has remained controversial.
The party had several members elected in state legislatures, including in California, Maine and Arkansas. A number of Greens around the United States hold positions on the municipal level, including on school boards, on city councils and as mayors.



The GPUS follows the ideals of green politics, which are based on the Four Pillars of the Green Party: Ecological wisdom, Social justice, Grassroots democracy and Nonviolence. The "Ten Key Values," which expand upon the four pillars, are as follows:
Grassroots democracy
Social justice
Ecological wisdom
Nonviolence
Decentralization
Community-based economics
Feminism
Respect for diversity
Global responsibility
Future focus
The Green Party does not accept donations from corporations, political action committees (PACs), 527(c) organizations or soft money. The party's platforms and rhetoric harshly criticize any corporate influence and control over government, media, and society at large.






The political movement that began in 1984 as the decentralized Committees of Correspondence evolved into a more centralized structure by 1990, opening a national clearinghouse, and forming governing bodies, bylaws, and a platform as the Green Committees of Correspondence (GCoC), and by 1990, simply, The Greens. The organization conducted grassroots organizing efforts, educational activities, and electoral campaigns.
Internal divisions arose between members who saw electoral politics as ultimately corrupting and supported the notion of an "anti-party party" formed by Petra Kelly and other leaders of Die Gr nen in Germany, vs. those who saw electoral strategies as a crucial engine of social change. A struggle for the direction of the organization culminated a "compromise agreement," ratified in 1990 at the Greens National Congress in Elkins, West Virginia   in which both strategies would be accommodated within the same 527 political organization renamed the Greens/Green Party USA (G/GPUSA). The G/GPUSA was recognized by the FEC as a national political party in 1991.
The compromise agreement subsequently collapsed and two Green Party organizations have co-existed in the United States since. The Green Politics Network was organized in 1990 and The National Association of Statewide Green Parties formed by 1994. Divisions between those pressing to break onto the national political stage and those aiming to grow roots at the local level continued to widen during the 1990s. The Association of State Green Parties (ASGP) encouraged and backed Nader's presidential runs in 1996 and 2000. By 2001, the push to separate electoral activity from the G/GPUA issue-based organizing led to the Boston Proposal and subsequent rise of the Green Party of the United States. The G/GPUSA lost most of its affiliates in the following months, and dropped its FEC national party status in 2005.



In the early decades of Green organizing in the United States, the prevailing U.S. system of money-dominated elections was universally rejected by Greens, so that some Greens were reluctant to have Greens participate in the election system at all, because they deemed the campaign finance system inherently corrupt. Other Greens felt strongly that the Green Party should develop in the electoral arena; many of these Greens felt that adopting an alternative model of campaign finance, emphasizing self-imposed contribution limits, would present a wholesome and attractive contrast to the odious campaign finance practices of the money-dominated major parties.
Over the years, some state Green parties have come to place less emphasis on the principle of self-imposed limits than they did in the past. Nevertheless, it is safe to say that Green Party fundraising (for candidates' campaigns and for the party itself) still tends to rely on relatively small contributions, and that Greens generally decry not only the rise of the Super PACs but also the big-money system, which some Greens criticize as plutocracy.
Some Greens feel that the Green Party's position should be simply to follow the laws and regulations of campaign finance. Other Greens argue that it would injure the Green Party not to practice a principled stand against the anti-democratic influence of money in the political process.
Candidates for office, like Jill Stein, the 2012 Green Party nominee for the President of the United States, typically rely on smaller donations to fund their campaigns.






The Green Party has two national committees recognized by the Federal Election Commission:
the Green National Committee (GNC)
the Green Senatorial Campaign Committee (GSCC)




The GNC is made up of delegates elected by affiliated state parties. The state parties also appoint delegates to serve on the various standing committees of the GNC. The National Committee elects a Steering Committee of seven Co-chairs, a Secretary and a Treasurer, to oversee daily operations. The National Committee performs most of its business online, but also holds an Annual National Meeting to conduct business in person.



Five identity caucuses have achieved representation on the GNC:
Black Caucus
Latinx Caucus
Lavender Greens Caucus (LGBTIQ)
National Women's Caucus
Young Greens Caucus
Other caucuses have worked toward formal recognition by the GNC:
Disability Caucus
Labor Caucus



The following is a list of accredited state parties which comprise the Green Party of the United States.
In addition, the Green Party has a chapter in the US Virgin Islands. The Green Party does not currently have active state chapters in North Dakota, South Dakota, Utah, or Vermont.



The Green Party has its strongest popular support on the Pacific Coast, Upper Great Lakes, and Northeast, as reflected in the geographical distribution of Green candidates elected. Californians have elected 55 of the 226 office-holding Greens nationwide as of June 2007. Other states with high numbers of Green elected officials include Pennsylvania (31), Wisconsin (23), Massachusetts (18), and Maine (17). Maine has the highest per capita number of Green elected officials in the country, and the largest Green registration percentage with more than 29,273 Greens comprising 2.95% of the electorate as of November 2006. Madison, Wisconsin, is the city with the most Green elected officials (8) followed by Portland, Maine (7).
In 2005, the Green Party had 305,000 registered members in states allowing party registration, and tens of thousands of members and contributors in the rest of the country. One challenge that the Green Party (as well as other third parties) faces is the difficulty of overcoming ballot access laws in many states.



















As of October 18, 2012, there were 134 elected Greens across the United States. Positions held varied greatly, from mayor to city council, school board to sanitation district. Twenty-three states had Greens elected at the municipal level, representing every region of the country except for East South Central. Greens held mayorships in California and New York, and positions on city, neighborhood, or common councils in the West, South, Midwest, and Northeast. Major cities with a Green presence were spread throughout the country and included Los Angeles, Minneapolis, Milwaukee, Cleveland, Oklahoma City, and Washington, DC.
The Green Party in the United States has won elected office at the local level; most winners of public office in the United States who are considered Greens have won nonpartisan elections. The highest-ranking Greens ever elected in the nation were: John Eder, a member of the Maine House of Representatives until his defeat in November 2006; Audie Bock, elected to the California State Assembly in 1999 but switched her registration to Independent seven months later running as an independent in the 2000 election; Richard Carroll, elected to the Arkansas House of Representatives in 2008 but switched parties to become a Democrat five months after his election; and Fredrick Smith, elected to the Arkansas House of Representatives in 2012, but re-registered as a Democrat in 2014.
In November 2010, Ben Chipman, a former Green Party leader, ran for Maine House of Representatives as an unenrolled candidate and was elected. Chipman was re-elected in 2012 and 2014.
As of 2014, Mayor Gayle McLaughlin is the most notable Green elected official in the United States. McLaughlin is serving her second term as mayor of Richmond, California. McLaughlin defeated two Democrats in 2006 to become mayor, and was reelected in 2010. Richmond, with a population of over 100,000 people, is the largest city in the country with a Green mayor.
Fairfax, California, Arcata, California, Sebastopol, California, and New Paltz, New York are the only towns in the United States to ever hold a Green Party majority in their town councils. Twin Ridges Elementary in Nevada County, California held the first Green Party majority school board in the United States.



1996: Ralph Nader and Winona LaDuke 685,128 votes (Ralph Nader presidential campaign, 1996)
2000: Ralph Nader and Winona LaDuke 2,882,000 votes (Ralph Nader presidential campaign, 2000)
2004: David Cobb and Pat LaMarche 119,859 votes (David Cobb presidential campaign, 2004)
2008: Cynthia McKinney and Rosa Clemente 161,603 votes (Cynthia McKinney presidential campaign, 2008)
2012: Jill Stein and Cheri Honkala 469,627 votes (Jill Stein presidential campaign, 2012)



The Green National Convention is scheduled in presidential election years, and the Annual National Meeting is scheduled in other years. The Green National Committee conducts business online between these in person meetings.
1996   Los Angeles, California
2000   Denver, Colorado
2001   Santa Barbara, California
2002   Philadelphia, Pennsylvania
2003   Washington, D.C.
2004   Milwaukee, Wisconsin
2005   Tulsa, Oklahoma
2006   Tucson, Arizona
2007   Reading, Pennsylvania
2008   Chicago, Illinois
2009   Durham, North Carolina
2010   Detroit, Michigan
2011 - Alfred, New York
2012 - Baltimore, Maryland
2013 - Iowa City, Iowa
2014 - Saint Paul, Minnesota
2015 - St. Louis, Missouri
2016 - Houston, TX









Official website
Green Senatorial Campaign Committee (GSCC)
Green Party Archives Project
2012 Green Party Platform
Ten key values
Ten Key Values of the Green Party U.S.
California Green Party's Ten Key Values
Early Ten Key Values statement
Six Principles of the Global Greens
National Green Party Caucuses
Black Caucus
Latino Caucus
Lavender Greens Caucus (GLBTIQ)
National Women's Caucus (NWC)
Young Greens Caucus
Disability Caucus (inactive)