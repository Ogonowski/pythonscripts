import os


DIR = '/home/mateuszogonowski/Documents/PythonScripts/Tekst/computer'
os.chdir(DIR)

infile = "+41.txt"
outfile = "+412.txt"

delete_list = ['telephone', 'numbers', 'in', 'switzerland']

with open(infile) as fin, open(outfile, "w+") as fout:
    for line in fin:
        splitLine = line.split()
        resultwords  = [word for word in splitLine if word.lower() not in delete_list]
        result = ' '.join(resultwords)+'\n'
        fout.write(result)
