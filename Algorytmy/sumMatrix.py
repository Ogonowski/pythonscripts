def addM(a, b):
    res = []
    for i in range(len(a)):
        row = []
        for j in range(len(a[0])):
            row.append(a[i][j]+b[i][j])
        res.append(row)
    return res



import numpy as np
import numpy
matrix1 = np.random.randint(5, size=(128, 128))
matrix2 = np.random.randint(5, size=(128, 128))
sumNumpy  = matrix1 + matrix2
sumImpl = numpy.asarray(addM(matrix1, matrix2))
comparison = sumNumpy == sumImpl
equal_arrays = comparison.all()
print("Both arrays are equal: ", equal_arrays)

