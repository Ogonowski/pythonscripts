def sort(array):

    less = []
    equal = []
    greater = []

    if len(array) > 1:
        pivot = array[0]
        for x in array:
            if x < pivot:
                less.append(x)
            elif x == pivot:
                equal.append(x)
            elif x > pivot:
                greater.append(x)
        return sort(less)+equal+sort(greater)  
    else:  
        return array

import random

arr=random.sample(range(1000),50)
n = len(arr)

arrQsort = sort(arr)
arrSortBuildin = sorted(arr)
if arrQsort == arrSortBuildin:
    print("Function works properly")
else:
    print("something goes wrong")
