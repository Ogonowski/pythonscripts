import numpy as np
X = np.random.randint(10, size=(8, 8))
Y = np.random.randint(10, size=(8, 8))

mulImpl = np.zeros([8,8],dtype = int)


for i in range(len(X)):
  
   for j in range(len(Y[0])):
       
       for k in range(len(Y)):
           mulImpl[i][j] += X[i][k] * Y[k][j]

mulNumpy = np.matmul(X, Y)
comparison = mulNumpy == mulImpl
equal_arrays = comparison.all()
print("Both arrays are equal: ", equal_arrays)

