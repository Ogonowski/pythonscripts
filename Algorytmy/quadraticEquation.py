import cmath

a = 1
b = 2
c = 1

delta = (b**2) - (4 * a*c)

# find two results
ans1 = (-b-cmath.sqrt(delta))/(2 * a)
ans2 = (-b + cmath.sqrt(delta))/(2 * a)

print('The roots are')
print(ans1)
print(ans2)
