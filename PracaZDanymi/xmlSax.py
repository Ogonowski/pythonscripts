#!/usr/bin/python3

import xml.sax
from xml.dom import minidom


class MovieHandler(xml.sax.ContentHandler):
   def __init__(self):
      self.CurrentData = ""
      self.type = ""
      self.format = ""
      self.year = ""
      self.description = ""

   # Call when an element starts
   def startElement(self, tag, attributes):
      self.CurrentData = tag
      if tag == "movie":
         print("*****Cinema*****")
         title = attributes["title"]
         print("Title:", title)

   # Call when an elements ends
   def endElement(self, tag):
      if self.CurrentData == "type":
         print("Type:", self.type)
      elif self.CurrentData == "format":
         print("Format:", self.format)
      elif self.CurrentData == "year":
         if(int(self.year) < 2000):
             print("Not long supported", self.year)
         else:
            print("Year:", self.year)
      elif self.CurrentData == "description":
         print("Description:", self.description)
      self.CurrentData = ""

   # Call when a character is read
   def characters(self, content):
      if self.CurrentData == "type":
         self.type = content
      elif self.CurrentData == "format":
         self.format = content 
      elif self.CurrentData == "year":
         self.year = content
      elif self.CurrentData == "description":
         self.description = content


if (__name__ == "__main__"):
   sax = MovieHandler()
   parser = xml.sax.make_parser()
   parser.setContentHandler(sax)
   parser.parse("/home/mateuszogonowski/Documents/PythonScripts/PracaZDanymi/cinema.xml")

   file = minidom.parse("/home/mateuszogonowski/Documents/PythonScripts/PracaZDanymi/cinema.xml")
   movies = file.getElementsByTagName("movie")

   for movie in movies:
      if(int(movie.getElementsByTagName("year")[0].childNodes[0].data)<2000):
         movie.getElementsByTagName("year")[0].childNodes[0].data = "No longer supported"

   with open("/home/mateuszogonowski/Documents/PythonScripts/PracaZDanymi/onlyNew.xml", 'w+') as f:
      f.write(file.toxml())

